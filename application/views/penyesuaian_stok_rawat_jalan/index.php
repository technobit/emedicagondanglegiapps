<section class="content-header">
    <h1 class="title"><?=$title?></h1>
    <?=$breadcrumbs?>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Data Penerimaan</h3>
                    </div>
                    <div class="box-body">
                        <?php getview('layouts/partials/message') ?>    
                        <div class="toolbar">
                            <a href="<?= base_url('apotik/rawat_jalan/penyesuaian/stok/tambah') ?>" class="btn btn-primary"><i class="fa fa-plus"></i> Buat Penerimaan Baru</a>
                        </div><br>
                        <table id="dataTable" class="table table-bordered table-condensed table-hover" data-apotik="<?= $this->uri->segment(2) ?>">
                            <thead>
                                <tr>
                                    <th>Tanggal</th>
                                    <th>No Penerimaan</th>                                    
                                    <th>Disetujui Oleh</th>
                                    <th width="220px" class="text-center">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>