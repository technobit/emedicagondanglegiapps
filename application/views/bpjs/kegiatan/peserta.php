<section class="content-header">
  <h1 class="title"><?=$title?></h1>
  <?=$breadcrumbs?>
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
    <!-- Hasil Pencarian Data Pasien -->
    <?php
        $alert = $this->session->flashdata("alert_message");
        if(isset($alert) && !empty($alert)):
        $message = $alert['message'];
        $status = ucwords($alert['status']);
        $class_status = ($alert['status'] == true) ? 'success' : 'danger';
        $icon = ($alert['status'] == true) ? 'check' : 'ban';
        ?>
        <div class="alert alert-<?=$class_status?> alert-dismissable">
          <i class="fa fa-<?=$icon?>"></i>
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          <?=$message?>
       </div>
        <?php
        endif;
      ?>
    <div class="box box-success">
       <div class="box-header">
    <h3 class="box-title">Form Tambah Kegiatan</h3>
    </div>
    <!-- form start -->
        <form action="" method="POST" class="form-horizontal" id="frm-kegiatan">
          <div class="box-body">
            <input type="text" style="display: none;" name="id" id="id" value="<?=$eduID?>">
            <?=$frm_tanggal?>
            <?=$jenis_kelompok?>
            <?=$club?>
            <?=$jenis_kegiatan?>
            <?=$frm_materi?>
            <?=$frm_pembicara?>
            <?=$frm_lokasi?>
            <?=$frm_biaya?>
            <?=$frm_ket?>
            <div class="col-sm-9 col-sm-offset-3" style="padding-left: 8px;">
                <button class="btn btn-primary btn-flat" id="saveBtn" type="button"><i class="fa fa-save"></i> Simpan</button>
                
            </div>
          </div>
        </form>
    </div>
      <div class="box box-primary">
        <!-- /.box-header -->
        <div class="box-header">
          <div class="box-tools">
           
          </div>
        </div>
        <div class="box-body">
           <div class="box-header">
    <h3 class="box-title">Data Peserta</h3>
      <div class="box-tools">
            <button class="btn btn-info btn-flat" onclick="getForm()" id="BtnAddpeserta" type="button" ><i class="fa fa-plus"></i> Tambah Peserta</button>
          </div>
    </div>
          <table class="table table-hover" id="table-peserta" >
                <thead>
                  <tr>
                    <th>No</th>
                    <th>No Kartu</th>
                    <th>Nama Peserta</th>
                    <th>Jenis Kelamin</th>
                    <th>Jenis Peserta</th>
                    <th>Tanggal lahir</th>
                    <th>Usia</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  
                </tbody>
              </table>
          
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>