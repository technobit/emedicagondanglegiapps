<!--?php 
    $arrData = array(
        "intIDAdmin", 
        "txtUsername",
        "txtEmail",
        "bitActive"
    );

    foreach($arrData as $rowData) {
        $$rowData = isset($data[$rowData]) ? $data[$rowData] : "";
    }
?-->
<?=form_open("" , "id='form-peserta' class='form-horizontal' ")?>
    <?=$this->form_builder->inputHidden("intIDAdmin"  )?>
  
					<div class="form-group">
              <label class="col-sm-3 control-label" for="selectJamkes">No BPJS</label>
              <div class="col-sm-9">
               
								<div class="input-group input-group-sm">
                <?=form_input("inputBPJS" , "" , "class='form-control' id='inputBPJS' placeholder='No BPJS' ");?>
               
                    <span class="input-group-btn">
                      <button id="checkBPJSkegiatan" type="button" class="btn btn-info btn-flat">Check BPJS</button>
                    </span>
              </div>
              </div>
            </div>
  
    <?=$this->form_builder->inputText("Nama" , "nama", "" , "col-sm-3" , array("class"=> "form-control", "readonly"=>"readonly")  )?>
    <?=$this->form_builder->inputText("Status Peserta" , "status_peserta", "" , "col-sm-3" , array("class"=> "form-control", "readonly"=>"readonly") )?>
    <?=$this->form_builder->inputText("Jenis Peserta" , "jenis_peserta"  , "" , "col-sm-3" , array("class"=> "form-control", "readonly"=>"readonly"))?>
    <?=$this->form_builder->inputText("Tanggal Lahir" , "tanggal_lahir"  , "" , "col-sm-3" , array("class"=> "form-control", "readonly"=>"readonly"))?>
    <?=$this->form_builder->inputText("Jenis Kelamin" , "jenis_kelamin"  , "" , "col-sm-3" , array("class"=> "form-control", "readonly"=>"readonly"))?>
    <?=$this->form_builder->inputText("PPK" , "ppk"  , "" , "col-sm-3" , array("class"=> "form-control", "readonly"=>"readonly"))?>
    <?=$this->form_builder->inputText("Status" , "status"  , "" , "col-sm-3" , array("class"=> "form-control", "readonly"=>"readonly"))?>
  
    

    <div class="box-footer text-center">
        <button type="button" id="btnSimpan" class="btn btn-primary">Simpan</button>
    </div>
<?=form_close()?>
<script>

$(function(){
  
    $('#checkBPJSkegiatan').click(function(){
      checkBPJS(2);
    });

});


function checkBPJS(mode){
    //loadingDialog();
   
    var noBPJS = $('#inputBPJS').val();

   
        if(noBPJS==''){
            alert("Isikan Nomor Terlebih Dahulu");
        }else{
        $.ajax({
            url : global_url + "bpjs/peserta/getPesertaByNoBPJS/",
            data : "txtNoAnggota="+noBPJS,
            type : "POST",
            success : function msg(response){
               
                var resVal = jQuery.parseJSON(response);
                var status = resVal['status'];
                var message = resVal['message'];
                var data = resVal['data'];
                
                if(status==true){
                    /// Form Pendaftaran pasien
                    
                   
                        $('#nama').val(data['nama']);
                        $('#jenis_kelamin').val(data['sex']);
                        $('#tanggal_lahir').val(convertDigitIn(data['tglLahir']));
                    
                    
             
                    
                }else{
                    alertPopUp(status , message , "");
                }
                console.log(data);
            }
        });
        }
 
    
}

</script>