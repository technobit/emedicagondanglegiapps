<section class="content-header">
  <h1 class="title"><?=$title?></h1>
  <?=$breadcrumbs?>
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
    <!-- Hasil Pencarian Data Pasien -->
    <div class="box box-success">
    <!-- form start -->
        <form class="form-horizontal" id="frm-poli-pasien">
          <div class="box-body">
            <?=$this->form_builder->inputText("Tanggal","txtTanggal" , date("Y-m-d"))?>
            <?=$this->form_builder->inputDropdown("Pelayanan","intIdJenisPelayanan" , "" , $arrPelayanan)?>
            <?=$this->form_builder->inputDropdown("Jenis Kunjungan","kunjSakit" , "" , $arrJenisKunjungan)?>
            <?=$this->form_builder->inputDropdown("Status Data","bitStatus" , "" , $arrServer)?>
            <div class="col-sm-9 col-sm-offset-3" style="padding-left: 8px;">
                <button class="btn btn-primary btn-flat" id="searchPeserta" type="button"><i class="fa fa-search"></i> Cari</button>
            </div>
          </div>
        </form>
    </div>
      <div class="box box-primary">
        <div class="box-body">
          <table class="table table-hover" id="table-data">
                <thead>
                  <tr>
                    <th>Tanggal Pendaftaran</th>
                    <th>No Anggota</th>
                    <th>Nama Pengguna Layanan</th>
                    <th>Jenis Jaminan Kesehatan</th>
                    <th>Ruangan / Kegiatan</th>
                    <th>Status Upload</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  
                </tbody>
              </table>
          
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>