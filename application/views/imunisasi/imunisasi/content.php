<section class="content-header">
  <h1 class="title"><?=$title?></h1>
  <?=$breadcrumbs?>
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
    <!-- Hasil Pencarian Data Pasien -->
    <?php
        $alert = $this->session->flashdata("alert_message");
        if(isset($alert) && !empty($alert)):
        $message = $alert['message'];
        $status = ucwords($alert['status']);
        $class_status = ($alert['status'] == true) ? 'success' : 'danger';
        $icon = ($alert['status'] == true) ? 'check' : 'ban';
        ?>
        <div class="alert alert-<?=$class_status?> alert-dismissable">
          <i class="fa fa-<?=$icon?>"></i>
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          <?=$message?>
       </div>
        <?php
        endif;
      ?>
    <div class="box box-success">
    <!-- form start -->
        <form class="form-horizontal" id="frm-filter">
          <div class="box-body">
            <div class="form-group">
                    <label class="col-sm-3 control-label form-label">Tanggal</label>
                    <div class="col-sm-3">
                        <div class="input-group date">
                            <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                            </div>
                            <input value="<?=date("Y-m-")?>01" type="text" name="start_date" class="form-control pull-right" id="start_date">
                        </div>
                    </div>
                    <div class="col-sm-1">
                        <label class="control-label form-label">s.d.</label>
                    </div>
                    <div class="col-sm-3">
                        <div class="input-group date">
                            <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                            </div>
                            <input value="<?=date("Y-m-d")?>" type="text" name="end_date" class="form-control pull-left" id="end_date">
                        </div>
                    </div>
                </div>
           
            <?=$kelurahan?>
           <?=$jenis_kegiatan?>
            <div class="col-sm-9 col-sm-offset-3" style="padding-left: 8px;">
                <button class="btn btn-primary btn-flat" id="searchBtn" type="button"><i class="fa fa-search"></i> Cari</button>
                <a href="<?=$link_tambah?>"><button class="btn btn-info btn-flat" id="addBtn" type="button" ><i class="fa fa-plus"></i> Tambah</button></a>
            </div>
          </div>
        </form>
    </div>
      <div class="box box-primary">
        <!-- /.box-header -->
        <div class="box-header">
          <div class="box-tools">
          
          </div>
        </div>
        <div class="box-body">
          <table class="table table-hover" id="table-imunisasi">
                <thead>
                  <tr>
                    <th>Nama Kegiatan</th>
                    <th>Kelurahan</th>
                    <th>Tanggal Kegiatan</th>
                    <th>Jenis Kegiatan</th>
                    <th>Jumlah Petugas</th>
                    <th>Jumlah Pengguna Layanan</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  
                </tbody>
              </table>
          
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>