
<?=form_open("" , "id='form-peserta' class='form-horizontal' ")?>
   <?=$txtIdPasien?>
   <?=$txtNoAnggota?>
   <?=$txtNamaPasien?>
   <?=$dtTanggalLahir?>
   <?=$charJkPasien?>
   <div class="form-group">
    <label class="col-sm-4"><h4>Form Imunisasi</h4></label>
   </div>
   <?=$id_imunisasi_pasien?>
   <?=$id_jenis_pasien_imunisasi?>
   <?=$status_nikah?>
   <?=$nama_suami?>
   <?=$nama_ibu?>
   <?=$hpht?>
   <?=$kehamilan_ke?>
    <?=$jarak_kehamilan?>
    <div id="jenis">
   <div class="form-group">
    <label class="col-sm-4"><h4>Jenis Imunisasi</h4></label>
   </div>
     <table id="tbl" class="table table-bordered table-condensed table-hover">
              <thead>
                <tr>
                  <th>Nama Imunisasi</th>                  
                                 
                  <th width="20px" class="text-center">Aksi</th>
                </tr>
              </thead>
              <tbody>
               <tbody id="cont-list-imun">

          <?php 
            if(empty($data_imun)) : 
          ?>
            <tr id="row-form-imun-1" class="row-form-imun">
              <td>
                <?=form_dropdown('listImunisasi[]' , $listJenisImun , '' , 'class="form-control" id="listImunisasi1"')?>
              </td>
              <td>

              </td>
            </tr>
            <?php 
            else : 
            $number = 1;
           // echopre($data_imun);
            foreach($data_imun as $row_imun) : 
            ?>
            
            <tr id="row-form-imun-<?=$number?>" class="row-form-imun">
              <td>
                <?=form_dropdown('listImunisasi[]' , $listJenisImun , $row_imun['id_jenis_imunisasi'] , 'class="form-control" id="listImunisasi<?=$number?>"')?>
              </td>
              <td>
                <?=$number==1 ? "" : form_button('btnHapus' . $number , '<i class="fa fa-trash"></i>' , 'class="btn btn-danger btn-flat" onclick="hapusImun('.$number.')"')?>
              </td>
            </tr>
            <?php 
            $number++;
            endforeach;
            endif;
            ?>
          </tbody>
              <tfoot>
                <tr>
                  <td colspan="3" class="text-right">
               <?=form_button('btnAddimun' , '<i class="fa fa-plus"></i> Tambah' , 'class="btn btn-primary btn-flat" id="btnAddimun"')?>
          </td>
                </tr>
              </tfoot>
            </table>
   </div>
       <div class="box-footer text-center">
         	<button id="btn" onClick="savePasien()" class="btn btn-success btn-lg" type="button" style=""><i class="fa fa-save"></i> Simpan</button>
			    <button id="cancel" onClick="cancel_modal()"  class="btn btn-warning btn-lg" type="button" style=""><i class="fa fa-ban"></i> Batal</button>
		
    </div>
<?=form_close()?>
<script>
var rowss = <?= isset($rowss) ? $rowss : 0 ?>;
$(function(){
   hide_form();
    $('#btnAddimun').click(function(){
    var num = $('.row-form-imun').length;
    var newNum = new Number(num + 1);
    var newSection = $('#row-form-imun-' + num).clone().attr("id" , "row-form-imun-" + newNum);
    newSection.children(":first").children(":first").attr("id","listPegawai"+newNum).val("");
    newSection.children(":nth-child(2)").html('<button type="button" onclick="hapusImun('+newNum+')" class="btn btn-danger btn-flat" id="btnHapusimun'+newNum+'"><i class="fa fa-trash"></i></button>');
    //console.log(newSection);
    $('#cont-list-imun').last().append(newSection);
  });
  
  
});




</script>