<div class="form-group">
    <label class="col-sm-3 control-label" for="selectJamkes">No Anggota / Nama / NIK</label>
              <div class="col-sm-9">
               
								<div class="input-group input-group-sm">
               
                <?= $this->form->text('txtCariPasien', $this->input->get('key'), 'placeholder="Cari berdasarkan No Anggota / Nama / NIK " id="txtCariPasien" class="form-control"') ?>
                    <span class="input-group-btn">
                      <button id="cari" type="button" class="btn btn-success btn-flat">Cari</button>
                       <button id="tambah" onClick="tambah()" type="button" class="btn btn-warning btn-flat">Tambah Pengguna Layanan</button>
                    </span>
              </div>
    
</div>
<table id="dataTable" class="table table-bordered table-condensed table-hover">
    <thead>
        <tr>
            <th>No Anggota</th>            
            <th>Nama Pengguna Layanan</th>
            
            <th>Tanggal Lahir</th>
            <th>Jenis Kelamin</th>
            <th width="1px"></th>
        </tr>
    </thead>
</table>

<script>
$(function() {
    var searchTimer;
    var dataTable;

    dataTable = $('#dataTable').dataTable({
        lengthChange: false,
		searching: false,
		ordering : true,
		info: false,
        processing : true,
        serverSide : true,
        ajax : {
            url : '<?= base_url('kegiatan_luar/imunisasi/get_cari_Pasien?key='.$this->input->get('key')) ?>',
            type : 'post'
        },
        columns : [
            {data : 'txtNoAnggota'},
            {data : 'txtNamaPasien'},
            {data : 'dtTanggalLahir'},
            {data : 'charJkPasien'},            
            {data : 'txtIdPasien', orderable : false, render : function(data, type, row) {
                    return '<button type="button" class="btn btn-xs btn-primary" onclick="pilihPasien(\''+row['txtIdPasien']+'\')"><i class="fa fa-plus"></i></button>'
                }
            }
        ],
        autoWidth : false,
        filter : false
    });    

    $('#txtCariPasien').keyup(function() {
        clearTimeout(searchTimer);
        searchTimer = setTimeout(function() {
            dataTable.api().ajax.url('<?= base_url('kegiatan_luar/imunisasi/get_cari_Pasien?key='.$this->input->get('key')) ?>'+$('#txtCariPasien').val()).load();
        }, 500)
    });
      $('#cari').click(function() {
            dataTable.api().ajax.url('<?= base_url('kegiatan_luar/imunisasi/get_cari_Pasien?key='.$this->input->get('key')) ?>'+$('#txtCariPasien').val()).load();
        
    });
     $('#cari').click(function() {
            dataTable.api().ajax.url('<?= base_url('kegiatan_luar/imunisasi/get_cari_Pasien?key='.$this->input->get('key')) ?>'+$('#txtCariPasien').val()).load();
        
    });
});

function pilihPasien(id) {
    getForm(id);
    $('.bootbox').modal('hide');
}
function tambah() {
    getFormTambah();
    $('.bootbox').modal('hide');
}
</script>