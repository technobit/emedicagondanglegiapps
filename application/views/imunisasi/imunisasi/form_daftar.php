<section class="content-header">
  <h1 class="title"><?=$title?></h1>
  <?=$breadcrumbs?>
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
    <!-- Hasil Pencarian Data Pasien -->
    <?php
        $alert = $this->session->flashdata("alert_message");
        if(isset($alert) && !empty($alert)):
        $message = $alert['message'];
        $status = ucwords($alert['status']);
        $class_status = ($alert['status'] == true) ? 'success' : 'danger';
        $icon = ($alert['status'] == true) ? 'check' : 'ban';
        ?>
        <div class="alert alert-<?=$class_status?> alert-dismissable">
          <i class="fa fa-<?=$icon?>"></i>
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          <?=$message?>
       </div>
        <?php
        endif;
      ?>
    <div class="box box-success">
    <!-- form start -->
        <form class="form-horizontal" id="frm-filter">
          <div class="box-body">
            <input type="text" name="id_kegiatan" style="display: none;" id="id_kegiatan" value="<?=$id_kegiatan?>">
           <?=$tanggal_kegiatan?>
						<?=$id_kegiatan_imunisasi?>
						<?=$jenisLokasi?>
						<?=$namaLokasi?>
						<?=$alamat?>
						<?=$kecamatan?>
						<?=$kelurahan?>
            <div class="col-sm-9 col-sm-offset-3" style="padding-left: 8px;">
                <a href="<?=$redirect_edit?>" class="btn btn-primary btn-flat" id="EditBtn"><i class="fa fa-edit"></i> Edit</a>
            </div>
          </div>
        </form>
    </div>
      <div class="box box-primary">
        <!-- /.box-header -->
        <div class="box-header">
          <div class="box-tools">
                 <button class="btn btn-info btn-flat" id ="addPasien" type="button" ><i class="fa fa-plus"></i> Tambah Peserta</button>
         
          </div>
        </div>
        <div class="box-body">
          <table class="table table-hover" id="table-pasien">
                <thead>
                  <tr>
                      
                    <th>No Anggota</th>
                    <th>Nama</th>
                    <th>Jenis Pengguna Layanan</th>
                    <th>Jumlah Imunisasi</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  
                </tbody>
              </table>
          
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>