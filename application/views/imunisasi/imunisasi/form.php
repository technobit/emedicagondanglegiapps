<section class="content-header">
  <h1 class="title"><?=$title_form?></h1>
  <?=$breadcrumbs?>
</section>
<section class="content">
	<form action="<?=$link_save?>" class="form-horizontal" method="POSt" id="frm-input">
  <div class="row">
    <div class="col-xs-12">
    <!-- Hasil Pencarian Data Pasien -->
	  <div class="alert alert-danger alert-dismissable" id="alert_message" style="display: none;">
		  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
		  <h4><i class="icon fa fa-ban" id="alert_message_icon"></i> Peringatan!</h4>
		  <div id="alert_message_content"></div>
      </div>
      
    <!-- Hasil Pencarian Data Pasien -->
    <div class="box box-success">
			  <div class="box-header">
          <h3 class="box-title">Form Kegiatan</h3>
		  <div class="box-tools">
		  </div>
        </div>
    <!-- form start -->
        
          <div class="box-body">
            <?=$tanggal_kegiatan?>
						<?=$id_kegiatan_imunisasi?>
						<?=$jenisLokasi?>
						<?=$namaLokasi?>
						<?=$alamat?>
						<?=$kecamatan?>
						<?=$kelurahan?>
               </div>
          </div>
					<div class="box box-primary">
			  <div class="box-header">
          <h3 class="box-title">Form Petugas Kesehatan</h3>
		  <div class="box-tools">
		  </div>
        </div>
    <!-- form start -->
        
          <div class="box-body">
						  <?php getview('layouts/partials/message') ?>   
            <?php getview('layouts/partials/validation') ?>   
            <div class="form-group">
              <div class="col-md-8">
                     
              </div>
            </div>
            <table id="tbl" class="table table-bordered table-condensed table-hover">
              <thead>
                <tr>
                  <th>Nama Petugas</th>                  
                                 
                  <th width="20px" class="text-center">Aksi</th>
                </tr>
              </thead>
           <tbody id="cont-list-petugas">

          <?php 
            if(empty($data_petugas)) : 
          ?>
            <tr id="row-form-petugas-1" class="row-form-petugas">
              <td>
                <?=form_dropdown('listPegawai[]' , $listPegawai , '' , 'class="form-control" id="listPegawai1" style="width: 95%"')?>
              </td>
              <td>

              </td>
            </tr>
            <?php 
            else : 
            $number = 1;
            foreach($data_petugas as $row_petugas) : 
            ?>
            <tr id="row-form-petugas-<?=$number?>" class="row-form-petugas">
              <td>
                <?=form_dropdown('listPegawai[]' , $listPegawai , $row_petugas['intIdPegawai'] , 'class="form-control" id="listPegawai<?=$number?>" style="width: 95%"')?>
              </td>
              <td>
                <?=$number==1 ? "" : form_button('btnHapus' . $number , '<i class="fa fa-trash"></i>' , 'class="btn btn-danger btn-flat" onclick="hapusPetugas('.$number.')"')?>
              </td>
            </tr>
            <?php 
            $number++;
            endforeach;
            endif;
            ?>
          </tbody>
              <tfoot>
                <tr>
                  <td colspan="3" class="text-right">
                 
        <?=form_button('btnAddPetugas' , '<i class="fa fa-plus"></i> Tambah' , 'onclick="add()" class="btn btn-primary btn-flat" id="btnAddPetugas"')?>
      </td>
                </tr>
              </tfoot>
            </table>
            <br>          
            <div class="form-group text-center">
              </div>
          </div>
           <div class="box-footer text-center">
         	<button type="submit" id="saveKegiatan" class="btn btn-success btn-lg" style=""><i class="fa fa-save"></i> Simpan</button>
			    <a href="<?=$link_home?>" id="cancelEditPasien"  class="btn btn-warning btn-lg" style=""><i class="fa fa-ban"></i> Batal</a>
			</div>
               </div>
               
          </div>
        
    </div>
      
		
      <!-- /.box -->
    </div>
	<!-- /.col -->
</div>
</form>
<!-- /.row -->
</section>
