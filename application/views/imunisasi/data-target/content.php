<section class="content-header">
  <h1 class="title"><?=$title?></h1>
  <?=$breadcrumbs?>
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
    <!-- Hasil Pencarian Data Pasien -->
    <?php
        $alert = $this->session->flashdata("alert_message");
        if(isset($alert) && !empty($alert)):
        $message = $alert['message'];
        $status = ucwords($alert['status']);
        $class_status = ($alert['status'] == true) ? 'success' : 'danger';
        $icon = ($alert['status'] == true) ? 'check' : 'ban';
        ?>
        <div class="alert alert-<?=$class_status?> alert-dismissable">
          <i class="fa fa-<?=$icon?>"></i>
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          <?=$message?>
       </div>
        <?php
        endif;
      ?>
    <div class="box box-success">
    <!-- form start -->
        <form class="form-horizontal" id="frm-filter">
          <div class="box-body">
            <?=$tahun?>
            <?=$kelurahan?>
            <div class="col-sm-9 col-sm-offset-3" style="padding-left: 8px;">
                <button class="btn btn-primary btn-flat" id="searchBtn" type="button"><i class="fa fa-search"></i> Cari</button>
                <a href="<?=$link_tambah?>"><button class="btn btn-info btn-flat" id="addBtn" type="button" ><i class="fa fa-plus"></i> Tambah Data Target</button></a>
            </div>
          </div>
        </form>
    </div>
      <div class="box box-primary">
        <!-- /.box-header -->
        <div class="box-header">
          <div class="box-tools">
          
          </div>
        </div>
        <div class="box-body">
          <table class="table table-hover" id="table-dasar">
                <thead>
                  <tr>
                    <th>Kelurahan</th>
                    <th>Tahun</th>
                    <th>Jumlah Bayi</th>
                    <th>Jumlah Balita</th>
                    <th>Jumlah Anak</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  
                </tbody>
              </table>
          
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>