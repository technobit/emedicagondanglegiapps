<section class="content-header">
  <h1 class="title"><?=$title_form?></h1>
  <?=$breadcrumbs?>
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
    <!-- Hasil Pencarian Data Pasien -->
	  <div class="alert alert-danger alert-dismissable" id="alert_message" style="display: none;">
		  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
		  <h4><i class="icon fa fa-ban" id="alert_message_icon"></i> Peringatan!</h4>
		  <div id="alert_message_content"></div>
      </div>
      <div class="box box-primary">
        <div class="box-header">
          <h3 class="box-title">Form Tambah Data Dasar Target</h3>
		  <div class="box-tools">
		  </div>
        </div>
        <!-- /.box-header -->
      <div class="box-body">
		  <form action = "<?=$link_save?>" class="form-horizontal" method = "POST"  id="form-input-pasien">
          <div class="col-sm-6">
						
						<?=$id_target_imunisasi?>
						<?=$frmListKelurahan?>
            <?=$tahun?>
            <?=$jumlah_bayi?>
            <?=$jumlah_balita?>
            
						<?=$jumlah_anak?>
						<?=$jumlah_caten?>
						<?=$jumlah_wus_hamil?>
						</div>			
             <div class="col-sm-6">
					
            <?=$jumlah_wus_tidak_hamil?>
            <?=$jumlah_sd?>
            <?=$jumlah_posyandu?>
						<?=$jumlah_ups_bds?>
						<?=$jumlah_pembina_wil?>
						<?=$jumlah_waktu_tempuh?>
					</div>	
			<div class="col-sm-12 text-center">
			<div class="form-group text-center">
              </div>
          </div>
			</div>
			<div class="box-footer clearfix text-right">
		  <div class="col-sm-12 text-center">
			  
         
			<button id="savePasien" class="btn btn-success btn-lg"><i class="fa fa-save"></i> Simpan</button>
			<a href="<?=$link_index?>" id="backPasien" class="btn btn-danger btn-lg"><i class="fa fa-reply"></i> Kembali</a>
			</div>
			</div>
		  </div>
		 </form>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
	<!-- /.col -->
</div>
<!-- /.row -->
</section>