<section class="content-header">
    <h1 class="title"><?=$title?></h1>
    <?=$breadcrumbs?>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Item Keuangan</h3>
                </div>
                <div class="box-body">
                    <?php getview('layouts/partials/message') ?>    
                    <div class="toolbar">
                        <a href="<?= base_url('keuangan/item_keuangan/create') ?>" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Item Keuangan</a>
                    </div><br>
                    <table id="dataTable" class="table table-bordered table-condensed table-hover">
                        <thead>
                            <tr>
                                <th>Nama</th>
                                <th>Jasa Sarana</th>
                                <th>Jasa Pelayanan</th>
                                <th>Tarif</th>
                                <th>Diinput Oleh</th>
                                <th>Teerahir Dirubah</th>
                                <th class="text-center">Aktif</th>
                                <th width="220px" class="text-center">Aksi</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

