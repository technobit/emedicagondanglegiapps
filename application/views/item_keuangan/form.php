<section class="content-header">
    <h1 class="title"><?=$title?></h1>
    <?=$breadcrumbs?>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Form Obat</h3>
                </div>
                <div class="box-body">
                  <?php getview('layouts/partials/validation') ?>        
                  <?= $this->form->open(null, 'class="form-horizontal"') ?>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Jasa Pelayanan</label>
                        <div class="col-md-10">
                            <?= $this->form->text('nama_item', null, 'class="form-control"') ?>
                        </div> 
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Deskripsi Pelayanan</label>
                        <div class="col-md-10">
                            <?= $this->form->textarea('keterangan', null, 'class="form-control"') ?>
                        </div> 
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Jasa Sarana</label>
                        <div class="col-md-10">
                            <div class="input-group">
                                <?= $this->form->text('jasa_sarana', null, 'id="jasa_saranan" class="form-control text-right"') ?>
                                <span id="jasa_sarana_currency" class="input-group-addon" style="min-width: 300px;text-align: right">
                                    
                                </span>
                            </div>
                        </div> 
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Jasa Pelayanan</label>
                        <div class="col-md-10">
                            <div class="input-group">
                            <?= $this->form->text('jasa_pelayanan', null, 'id="jasa_pelayanan" class="form-control text-right"') ?>
                                <span id="jasa_pelayanan_currency" class="input-group-addon" style="min-width: 300px;text-align: right">
                                    
                                </span>
                            </div>
                        </div> 
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Tarif</label>
                        <div class="col-md-10">
                            <div class="input-group">
                                <?= $this->form->text('tarif', null, 'id="tarif" class="form-control text-right" readonly') ?>
                                <span id="tarif_currency" class="input-group-addon" style="min-width: 300px;text-align: right">
                                    
                                </span>
                            </div>
                        </div> 
                    </div>                    
                    <div class="form-group text-center">
                        <button type="submit" class="btn btn-primary btn-lg"><i class="fa fa-print"></i> Simpan</button>   
                        <a href="<?= base_url('keuangan/item_keuangan') ?>" class="btn btn-danger btn-lg"><i class="fa fa-ban"></i> Batal</a>             
                    </div>
                  <?= $this->form->close() ?>
                </div>
            </div>
        </div>
    </div>
</section>