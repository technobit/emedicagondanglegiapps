<section class="content-header">
  <h1 class="title"><?=$title?></h1>
  <!--<?=$breadcrumbs?>-->
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
    <!-- Cari Data Pasien -->
    <!-- Hasil Pencarian Data Pasien -->
      <div class="box box-success">
        <!-- /.box-header -->
        <div class="box-body">
          <table class="table table-hover" id="table-antrian">
            <thead>
              <tr>
                <th>No Registrasi</th>
                <th>Nama Pengguna Layanan</th>
                <th>Usia</th>
                <th>Tanggal Masuk</th>
                <th>Ruang Kamar</th>
              </tr>
            </thead>
            <tbody id="result-search-pasien">
              
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>