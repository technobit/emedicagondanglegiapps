<section class="content">
    <div class="row">
        <div class="col-md-4">
            <div class="box box-success box-solid">
                <div class="box-body">
                    <?=$date_input?>
                    <div class="row">
                        <div class="col-sm-12">
                            <!-- small box -->
                            <div class="small-box bg-navy">
                                <div class="inner">
                                    <p><?=$tglAntrian?></p>
                                    <h3 id="jam-sekarang"></h3>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-clock-o"></i>
                                </div>
                            </div>
                        </div>
                        <div class="list-dashboard-pelayanan">
                        <?php 
                            $no = 1;
                            foreach($data_pelayanan as $row_pelayanan) :
                        ?>
                        <div class="col-md-12 cont-dashboard-pelayanan">
                            <?=form_hidden('id_pelayanan_'.$no, $row_pelayanan['intIdPelayanan']);?>
                            <div class="info-box bg-<?=$row_pelayanan['txtColorButton']?>">
                                <span class="info-box-icon"><i class="fa fa-user-md"></i></span>
                                <div class="info-box-content">
                                    <span class="info-box-text"><?=$row_pelayanan['txtNama']?></span>
                                    <span class="info-box-number" id="count_<?=$row_pelayanan['intIdPelayanan']?>">-</span>
                                </div>
                            <!-- /.info-box-content -->
                            </div>
                        <!-- /.info-box -->
                        </div>

                        <?php 
                            $no++;
                            endforeach;
                        ?>
                        </div>
                        <div class="col-sm-12">
                            <!-- small box -->
                            <div class="small-box bg-green">
                                <div class="inner">
                                    <p>Jumlah Kunjungan</p>
                                    <h3 id="kunjungan-total">0</h3>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-pie-chart"></i>
                                </div>
                            </div>
                        </div>
                    </div>                
                </div>
            <!-- /.box-body -->
            </div>
        </div>

        <div class="col-md-8">
            <div class="box box-danger box-solid">
                <div class="box-header with-border cursor-pointer" data-widget="collapse" >
                <i class="fa fa-minus pull-right"></i>
                <h3 class="box-title">Daftar Antrian</h3>
                <div class="box-tools pull-right">
                    <button data-widget="collapse" class="btn btn-box-tool" type="button"><i class="fa fa-minus"></i>
                    </button>
                </div>
                <!-- /.box-tools -->
                </div>
                <div class="box-body table-responsive">
                  <table class="table table-hover">
                    <thead style="font-size:20px; font-weight:lighter;">
                      <tr>
                        <th >No. Antrian</th>
                        <th >Loket</th>
                      </tr>
                    </thead>

                   <tbody id="result-register-pasien" style="font-size:20px;">
                      
                    </tbody>
                  </table>
                </div>
            <!-- /.box-body -->
            </div>
        </div>
    </div>
</section>