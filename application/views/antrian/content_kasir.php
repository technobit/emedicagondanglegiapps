<section class="content-header">
  <h1 class="title"><?=$title?></h1>
  <!--<?=$breadcrumbs?>-->
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
    <div class="box box-success">
        <div class="box-header">
          <h3 class="box-title">Filter</h3>
          <div class="box-tools pull-right">
                <button data-widget="collapse" class="btn btn-box-tool" type="button"><i class="fa fa-minus"></i>
                </button>
              </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <form class="form-horizontal" method="POST" id="form-filter">
                <?=$txtTanggal?>
            </form>
        </div>
        </div>
    </div>
    <div class="col-xs-12">
    <!-- Cari Data Pasien -->
    <!-- Hasil Pencarian Data Pasien -->
      <div class="box box-success">
        <div class="box-header">
          <h3 class="box-title">Data Antrian Kasir</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table class="table table-hover" id="table-antrian">
            <thead>
              <tr>
                <th>No Antrian</th>
                <th>Nama Pengguna Layanan</th>
                <th>No Registrasi</th>
                <th>Usia</th>
                <th>Ruangan / Pelayanan</th>
                <th>Status</th>
              </tr>
            </thead>
            <tbody id="result-search-pasien">
              
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>