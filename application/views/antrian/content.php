<section class="content-header">
  <h1 class="title"><?=$title?></h1>
  <!--<?=$breadcrumbs?>-->
</section>
<section class="content">
  <div class="row">
    <!--<div class="col-xs-12">
    <div class="box box-success">
        <div class="box-header">
          <h3 class="box-title">Antrian Pasien Rawat Jalan</h3>
          <div class="box-tools pull-right">
                <button data-widget="collapse" class="btn btn-box-tool" type="button"><i class="fa fa-minus"></i>
                </button>
              </div>
        </div>-->
        <!-- /.box-header
        <div class="box-body">
            <form class="form-horizontal" method="POST" id="form-filter">
                <?=$txtTanggal?>
                <?=$intIdPoli?>
            </form>
        </div>
        </div> -->
    </div>
    <div class="col-sm-3 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-green">
            <div class="inner">
              <div class="box-body">
                <div class="form-group">
                  <?=$txtTanggal?>
                  <?=$intIdPoli?>
                </div>                    
              </div>
            </div>
	        <div class="small-box-footer"></div>
        </div>
    </div>
    <div class="col-sm-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-blue">
        <div class="inner text-center">
          <h3 id="jam-sekarang">0</h3>
          <p id="waiting_list_umum_1"><?=$tglAntrian?></p>
        </div>
        <div class="icon">
          <i class="fa fa-clock-o"></i>
        </div>
        <span class="small-box-footer text-left"></span>
      </div>
    </div>
    <div class="col-sm-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-yellow">
        <div class="inner text-center">
          <p>Nomor Antrian yang Dilayani</p>
          <h3 id="kunjungan_antrian">-</h3>
        </div>
        <div class="icon">
          <i class="fa fa-user-md"></i>
        </div>
        <span class="small-box-footer"></span>
      </div>
    </div>
    <div class="col-sm-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-red">
        <div class="inner text-center">
          <p>Jumlah Antrian Menunggu</p>
          <h3 id="waiting_kunjungan_list">0</h3>
        </div>
        <div class="icon">
          <i class="fa fa-users"></i>
        </div>
        <span class="small-box-footer"></span>
      </div>
    </div>
     <!--<div class="col-sm-3 col-xs-6">
      <div class="small-box bg-aqua">
        <div class="inner text-center">
          <p>Jumlah Kunjungan</p>
          <h3 id="kunjungan-total">0</h3>
        </div>
        <div class="icon">
          <i class="fa fa-pie-chart"></i>
        </div>
        <span class="small-box-footer"></span>
      </div>
    </div>-->
    <div class="col-xs-12">
    <!-- Cari Data Pasien -->
    <!-- Hasil Pencarian Data Pasien -->
      <div class="box box-success">
        <div class="box-header">
          <h3 class="box-title">Data Antrian Pengguna Layanan</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table class="table table-hover" id="table-antrian">
            <thead>
              <tr>
                <th>No Antrian</th>
                <th>Loket</th>
                <th>Status</th>
              </tr>
            </thead>
            <tbody>
              
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>