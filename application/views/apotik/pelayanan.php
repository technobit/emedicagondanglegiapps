<section class="content-header">
  <h1 class="title"><?=$title?></h1>
  <?=$breadcrumbs?>
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
    <!-- Hasil Pencarian Data Pasien -->
    <div class="box box-success">
    <!-- form start -->
        <form class="form-horizontal" id="frm-poli-pasien">
          <div class="box-body">
            <?=$frm_tanggal?>
            <?=$frm_jenis_apotik?>
            <div class="col-sm-10 col-sm-offset-2" style="padding-left: 3px;">
                <button class="btn btn-primary btn-flat" id="getDataPasien" type="button"><i class="fa fa-search"></i>Cari</button>
            </div>
          </div>
        </form>
    </div>
      <div class="box box-primary">
        <!-- /.box-header -->
        <div class="box-body table-responsive">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a data-toggle="tab" href="#tab_1">Daftar Antrian</a></li>
              <li ><a data-toggle="tab" href="#tab_2">Sedang Di Layani</a></li>
              <li ><a onclick="refreshTableDone()" data-toggle="tab" href="#tab_3">Telah Di Layani</a></li>
            </ul>
            <div class="tab-content">
              <!-- /.tab-pane Form Rawat Jalan-->
              <div id="tab_1" class="tab-pane active">
                <table class="table table-hover" id="table-apotik-list1">
                <thead>
                  <tr>
                    <th>No Antrian</th>
                    <th>Nama Pengguna Layanan</th>
                    <th>Ruangan</th>
                    <th>Usia</th>
                    <th>Status Pelayanan</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  
                </tbody>
              </table>
              </div>
              <!-- /.tab-pane -->
              <div id="tab_2" class="tab-pane">
                <table class="table table-hover" id="table-apotik-list2">
                <thead>
                  <tr>
                    <th>No Antrian</th>
                    <th>Nama Pengguna Layanan</th>
                    <th>Ruangan</th>
                    <th>Usia</th>
                    <th>Status Pelayanan</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  
                </tbody>
              </table>
              </div>
              <div id="tab_3" class="tab-pane">
                <table class="table table-hover" id="table-apotik-list3">
                <thead>
                  <tr>
                    <th>No Antrian</th>
                    <th>Nama Pengguna Layanan</th>
                    <th>Ruangan</th>
                    <th>Usia</th>
                    <th>Status Pelayanan</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  
                </tbody>
              </table>
              </div>
            </div>
            <!-- /.tab-content -->
          </div>
          
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>