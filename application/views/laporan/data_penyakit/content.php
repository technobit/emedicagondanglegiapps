<section class="content-header">
  <h1 class="title"><?=$title?></h1>
  <?=$breadcrumbs?>
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
    <!-- Cari Data Pasien -->
    <div class="box box-success">
      <div class="box-header with-border">
      <h3 class="box-title">Filter</h3>
      </div>
    <!-- /.box-header -->
    <!-- form start -->
        <form class="form-horizontal" id="frm-rekap-pengunjung">
          <div class="box-body">
                <?=$frmPelayanan?>
                <?=$frmTahun?>
                <?=$frmBulanan?>
                <div class="col-sm-offset-3 col-sm-7">
                    <button type="button" id="sendBtn" class="btn btn-success btn-flat"><i class="fa fa-send"></i> Cari</button>
                </div>
          </div>
        </form>
    </div>
      <div class="box box-primary">
        <div class="box-header">
          <h3 class="box-title">Data Pasien</h3>
          <div class="box-tools pull-right">
          <button class="btn btn-warning" id="btnExportExcel"><i class="fa fa-download"></i> Download Excel</button>          
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive">
          <table class="table table-hover" id="table-data-laporan">
            <thead>
              <tr>
                <th rowspan="3">Nama Penyakit</th>
                <th rowspan="3">Kode ICD - X</th>
                <th colspan="13">Jumlah Penderita Menurut Golongan Umur</th>                            
              </tr>
              <tr>
                <th colspan="2">0 - 7 Hari</th>
                <th colspan="2">8 - 28 Hari</th>
                <th colspan="2">28 - 60 Hari</th>
                <th colspan="2">60 - 1 Tahun</th>
                <th colspan="2">1 - 4 Tahun</th>
                <th colspan="2">5 - 9 Tahun</th>
                <th colspan="2">10 - 14 Tahun</th>
                <th colspan="2">15 - 19 Tahun</th>
                <th colspan="2">20 - 44 Tahun</th>
                <th colspan="2">45 - 54 Tahun</th>
                <th colspan="2">55 - 59 Tahun</th>
                <th colspan="2">60 - 69 Tahun</th>
                <th colspan="2">> 70 Tahun</th>
              </tr>
              <tr>
                <th>B</th>
                <th>L</th>
                <th>B</th>
                <th>L</th>
                <th>B</th>
                <th>L</th>
                <th>B</th>
                <th>L</th>
                <th>B</th>
                <th>L</th>
                <th>B</th>
                <th>L</th>
                <th>B</th>
                <th>L</th>
                <th>B</th>
                <th>L</th>
                <th>B</th>
                <th>L</th>
                <th>B</th>
                <th>L</th>
                <th>B</th>
                <th>L</th>
                <th>B</th>
                <th>L</th>
                <th>B</th>
                <th>L</th>
              </tr>
            </thead>
            <tbody>
              
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>