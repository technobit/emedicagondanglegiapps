<section class="content-header">
  <h1 class="title"><?=$title?></h1>
  <?=$breadcrumbs?>
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
    <!-- Hasil Pencarian Data Pasien -->
      <div class="box box-primary">
        <!-- /.box-header -->
        <div class="box-header">
          <h3 class="box-title">Laporan Harian</h3>
        </div>
        <div class="box-body">
          <?php 
            foreach ($laporan_harian as $key => $value) {
           ?>
              <div class="col-sm-4">
              <a href="<?=$value['link']?>" class="btn btn-lg btn-default">
                <i class="<?=$value['icon']?>"></i> </br> <?=$value['title']?>
              </a>
              </div>
          <?php
            }
          ?>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>