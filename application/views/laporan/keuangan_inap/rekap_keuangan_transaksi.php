<section class="content-header">
    <h1 class="title"><?=$title?></h1>
    <?=$breadcrumbs?>
</section>
<section class="content">
	<div class="box box-primary">
		<div class="box-body">
			<form method="post" class="form-horizontal" action="<?= base_url('laporan/keuangan_inap/excel_rekap_keuangan_transaksi') ?>">
				<div class="form-group">
					<label class="col-md-2 control-label">Tanggal</label>
					<div class="col-md-10">
						<div class="input-group">
							<?= $this->form->text('tgl_awal', date('Y-m-01'), 'id="tgl_awal" class="form-control datepicker"') ?>
							<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
							<?= $this->form->text('tgl_akhir', date('Y-m-d'), 'id="tgl_akhir" class="form-control datepicker"') ?>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-offset-2 col-md-10">
						<button id="preview" type="button" class="btn btn-primary">Preview</button>
						<button type="submit" class="btn btn-success">Excel</button>
					</div>
				</div>
			</form>
		</div>
	</div>
	<div class="box box-primary">
		<div class="box-body">
			<table id="dtTable" class="table">
				<thead>
					<tr>
						<th>Item Layanan</th>
						<th>Total</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
	</div>
</section>