<section class="content-header">
    <h1 class="title"><?=$title?></h1>
    <?=$breadcrumbs?>
</section>
<section class="content">
	<div class="box box-primary">
		<div class="box-body">
			<form method="post" class="form-horizontal" action="<?= base_url('laporan/obat/excel_permintaan_pemakaian') ?>">
				<div class="form-group">
					<label class="col-md-2 control-label">Tanggal / Bulan</label>
					<div class="col-md-2">
						<?= $this->form->select('tahun', selectTahun(), date('Y'), 'id="tahun" class="form-control input-sm"') ?>
					</div>
					<div class="col-md-2">
						<?= $this->form->select('bulan', lang('_month'), date('m'), 'id="bulan" class="form-control input-sm"') ?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-2 control-label">Apotik</label>
					<div class="col-md-2">
						<?= $this->form->select('apotik', selectApotik('Gudang'), null, 'id="apotik" class="form-control input-sm"') ?>
					</div>					
				</div>
				<div class="form-group">
					<label class="col-md-2 control-label">Jenis Obat</label>
					<div class="col-md-2">
						<?= $this->form->select('jenis_obat', selectJenisObat('Semua'), null, 'id="jenis_obat" class="form-control input-sm"') ?>
					</div>					
				</div>
				<div class="form-group">
					<div class="col-md-offset-2 col-md-10">
						<button id="preview" type="button" class="btn btn-primary">Preview</button>
						<button type="submit" class="btn btn-success">Excel</button>
					</div>
				</div>
			</form>
		</div>
	</div>
	<div class="box box-primary">
		<div class="box-body">
			<table id="dtTable" class="table">
				<thead>
					<tr>
						<th>Nama Obat</th>
						<th>Kemasan</th>
						<th>Stok Awal</th>
						<th>Penerimaan</th>
						<th>Persediaan</th>
						<th>Pemakaian</th>
						<th>Sisa Stok</th>						
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
	</div>
</section>