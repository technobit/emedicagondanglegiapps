<section class="content-header">
  <h1 class="title"><?=$title?></h1>
  <?=$breadcrumbs?>
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
    <!-- Hasil Pencarian Data Pasien -->
    <form class="form-horizontal" action="<?=base_url()."data_rujukan/saveData/"?>" method="POST" id="frm-input">
     <div class="box box-success">
        <div class="box-header">
            <h3 class="box-title">Data Rujukan</h3>
         </div>
    <!-- form start -->
          <div class="box-body">
          <?=$txtIdKunjungan?>
          <?=$selectMode?>
          <?=$selectLayananLanjut?>
          <?=$poliLanjut?>
          <?=$txtKeteranganLanjut?>
          <div class="col-sm-offset-3 col-sm-9">
                <button class="btn btn-primary btn-flat"><i class="fa fa-save"></i> Simpan</button>
          </div>    
          </div>
    </div>
    <div class="box box-success">
        <div class="box-header">
            <h3 class="box-title">Data Pengguna Layanan</h3>
            
         </div>
    <!-- form start -->
          <div class="box-body">
            
            <?=$txtNoAnggota?>
            <?=$txtNamaPasien?>
            <?=$txtUsiaPasienKunjungan?>
          </div>
    </div>
   
    <div class="box box-success">
        <div class="box-header">
            <h3 class="box-title">Data Diagnosa</h3>
            <div class="box-tools">
                <?=anchor($link_edit_diagnosa , "Edit Diagnosa" , 'class="btn btn-primary"')?>
            </div>
         </div>
    <!-- form start -->
          <div class="box-body">
                <?=$txtPemeriksaan?>
                <?=$txtDiagnosa?>
          </div>
    </div>
    </form>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>