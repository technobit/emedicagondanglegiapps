<section class="content-header">
  <h1 class="title"><?=$title?></h1>
  <?=$breadcrumbs?>
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
    <!-- Hasil Pencarian Data Pasien -->
      <div class="box box-primary">
        <!-- /.box-header -->
        <div class="box-header">
          <div class="box-tools">
            <a href="<?=$link_add?>" class="btn btn-success"><i class="fa fa-plus"></i> Tambah Data</a>
          </div>
        </div>
        <div class="box-body">
          <table class="table table-hover" id="table-data">
                <thead>
                  <tr>
                    <th>Kelas</th>
                    <th>Status</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                      foreach($data_kelas as $row) : 
                  ?>
                    <tr>
                      <td><?=$row['txtKelasKamar']?></td>
                      <td><?=$row['bitStatusKelasKamar']==1 ? "Aktif" : "Non Aktif"?></td>
                      <td>
                        <a class="btn btn-primary" href="<?=base_url().'master/kelas_kamar/form/'.$row['intIdKelasKamar']?>"><i class="fa fa-edit"></i> Detail</a>
                        <button class="btn btn-warning" onclick="hapusData(<?=$row['intIdKelasKamar']?>)"><i class="fa fa-trash"></i> Hapus</button>
                      </td>
                    </tr>
                  <?php 
                      endforeach;
                  ?>
                </tbody>
              </table>
          
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>