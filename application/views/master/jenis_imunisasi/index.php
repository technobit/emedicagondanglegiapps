<section class="content-header">
    <h1 class="title"><?=$title?></h1>
    <?=$breadcrumbs?>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Data Jenis Imunisasi</h3>
                </div>
                <div class="box-body"> 
                    <?php getview('layouts/partials/message') ?>           
                    <div class="toolbar">
                        <a href="<?= base_url('master/jenis_imunisasi/create') ?>" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Jenis Imunisasi</a>
                    </div><br>
                    <table class="table table-bordered table-condensed table-hover">
                        <thead>
                            <tr>
                                <th>Jenis Imunisasi</th>
                                <th class="text-center" width="170">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($rsjenis_imunisasi->result() as $jenis_imunisasi) { ?>
                                <tr>
                                    <td><?= $jenis_imunisasi->jenis_imunisasi ?></td>
                                    <td class="text-center">
                                        <a href="<?= base_url('master/jenis_imunisasi/edit/' . $jenis_imunisasi->id) ?>" class="btn btn-info btn-sm"><i class="fa fa-pencil"></i> Ubah</a>
                                        <a onclick="return confirm('Apakah anda yakin akan menghapus data ini?')" href="<?= base_url('master/jenis_imunisasi/delete/' . $jenis_imunisasi->id) ?>" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> Hapus</a>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>