<section class="content-header">
  <h1 class="title">Master Data - Ruangan Luar</h1>
  <?=$breadcrumbs?>
</section>
<section class="content">
<div class="row">
        <div class="col-md-4">
            <div class="box box-success">
                <div class="box-header">
                  <h3 class="box-title" id="form-head">Tambah Ruangan Luar</h3>
                </div>
                <div class="box-body">
                  <?=form_open($linkForm , "id='form-jenis' ")?>
                    <?=$intIDPoliLuar?>
                    <?=$txtPoliLuar?>
                    <?=$txtKeterangan?>
                    <?=$bitStatus?>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                        
                    </div>
                  <?=form_close()?>
                </div><!-- /.box-body -->
            </div>  
        </div>
        <div class="col-md-8">
            <div class="box box-warning">
                <div class="box-header">
                  <h3 class="box-title">Data Ruangan Luar</h3>
                </div>
                <div class="box-body">
                    <table class="table table-hover table-striped" id="table-data">
                    <thead>
                      <tr>
                        <th>Ruangan</th>
                        <th>Keterangan</th>
                        <th>Status</th>
                        <th>Aksi</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php 
                        foreach($data as $rows) : 
                        $ID = $rows['intIDPoliLuar'];
                      ?>
                      <tr>
                        <td><?=$rows['txtPoliLuar']?></td>
                        <td><?=$rows['txtKeterangan']?></td>
                        <td><?=$rows['bitStatus']==1 ? "Aktif" : "Non Aktif"?></td>
                        <td>
                          <?=form_button("btnEdit" , "Edit" , 'class="btn btn-primary btn-flat" onclick="getDetail('.$ID.')"')?>
                          <?=form_button("btnHapus" , "Hapus" , 'class="btn btn-danger btn-flat" onclick="deleteDetail('.$ID.')"')?>
                        </td>
                      </tr>
                      <?php 
                        endforeach;
                      ?>
                    </tbody>   
                    </table>
                </div>
            </div>
        </div>
      </div>
      <?=html_alert("insert_alert")?>
</section>
