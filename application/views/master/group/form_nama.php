<div class="clearfix">
<form class="form-horizontal" id="form-input-nama">
    <?=$intIdGroup?>
    <?=$frmPegawai?>
    <div id="msg-response">

    </div>
    <div class="col-sm-12 text-center">
        <button class="btn btn-success btn-flat" type="button" id="btnTambahNama"><i class="fa fa-plus"></i> Tambahkan</button>
    </div>
</form>
</div>

<script>
    $('#btnTambahNama').click(function(){
        $.ajax({
            url : global_url+"master/group/simpanPegawai/",
            type : "POST",
            data : $('#form-input-nama').serialize(),
            success : function(response){
                var data = jQuery.parseJSON(response);
                var status = data['status'];
                var message = data['message'];
                var htmlRep = alertMessage(message , status);
                $('#msg-response').html(htmlRep);
                refreshTable();
                if(status==true){
                    $('.bootbox').modal("hide");
                }
            }
        });
    });
</script>