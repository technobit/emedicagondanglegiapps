<section class="content-header">
  <h1 class="title"><?=$title?></h1>
  <?=$breadcrumbs?>
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-4">
    <div class="box box-primary">
        <!-- /.box-header -->
        <div class="box-header">
          <h3 class="box-title">Detail Group <?=$nameGroup?></h3>
        </div>
        <div class="box-body">
            <form class="form-vertical" id="frm-group">
                <?=$intIdGroup?>
                <div class="form-group">
                    <label class="form-label control-label">Group Name</label>
                    <?=$txtGroupName?>
                </div>
                <div class="form-group">
                    <label class="form-label control-label">Group Status</label>
                    <?=$bitGroupStatus?>
                </div>                
            </form>
        </div>
        <!-- /.box-body -->
      </div>
    </div>
    <div class="col-xs-8">
    <?php
        $alert = $this->session->flashdata("alert_message");
        if(isset($alert) && !empty($alert)):
        $message = $alert['message'];
        $status = ucwords($alert['status']);
        $class_status = ($alert['status'] == true) ? 'success' : 'danger';
        $icon = ($alert['status'] == true) ? 'check' : 'ban';
     ?>
        <div class="alert alert-<?=$class_status?> alert-dismissable">
          <i class="fa fa-<?=$icon?>"></i>
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          <?=$message?>
       </div>
      <?php
        endif;
      ?>
      <div class="box box-primary">
        <!-- /.box-header -->
        <div class="box-header">
          <h3 class="box-title">Data Group <?=$nameGroup?></h3>
          <div class="box-tools">
            <button id="btnAddPegawai" class="btn btn-success btn-flat">Tambah Data</button>
            <button id="btnRefresh" class="btn btn-success btn-flat">Refresh</button>
          </div>          
        </div>

        <div class="box-body">
          <table class="table table-hover" id="table-data">
                <thead>
                  <tr>
                    <th>Nama Pegawai</th>
                    <th>No Handphone</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>