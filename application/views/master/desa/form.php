<section class="content-header">
  <h1 class="title"><?=$title?></h1>
  <?=$breadcrumbs?>
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
    <form class="form-horizontal" id="frm-input">
    <!-- Hasil Pencarian Data Pasien -->
    <div class="box box-success">
          <div class="box-body">
          <?=$frmid?>
          <?=$frmid_pelayanan?>
          <?=$frmnama_desa?>
          <?=$frmketerangan?>
          <?=$frmListKelurahan?>
          <?=$frmStatusdesa?>
          </div>
    </div>
    <div class="box box-success">
      <div class="box-header">
        <h3 class="box-title">List Petugas Pegguna</h3>
      </div>
      <div class="box-body">
        <table class="table">
          <thead>
          <tr>
            <th>Nama Petugas</th>
            <th>Aksi</th>
          </tr>
          </thead>
          <tbody id="cont-list-petugas">

          <?php 
            if(empty($data_petugas)) : 
          ?>
            <tr id="row-form-petugas-1" class="row-form-petugas">
              <td>
                <?=form_dropdown('listPegawai[]' , $listPegawai , '' , 'class="form-control" id="listPegawai1"')?>
              </td>
              <td>

              </td>
            </tr>
            <?php 
            else : 
            $number = 1;
            foreach($data_petugas as $row_petugas) : 
            ?>
            <tr id="row-form-petugas-<?=$number?>" class="row-form-petugas">
              <td>
                <?=form_dropdown('listPegawai[]' , $listPegawai , $row_petugas['intIdUser'] , 'class="form-control" id="listPegawai<?=$number?>"')?>
              </td>
              <td>
                <?=$number==1 ? "" : form_button('btnHapus' . $number , '<i class="fa fa-trash"></i> Hapus' , 'class="btn btn-danger btn-flat" onclick="hapusPetugas('.$number.')"')?>
              </td>
            </tr>
            <?php 
            $number++;
            endforeach;
            endif;
            ?>
          </tbody>
        </table>
      </div>
      <div class="box-footer clearfix text-right">
        <?=form_button('btnAddPetugas' , '<i class="fa fa-plus"></i> Tambah' , 'class="btn btn-primary btn-flat" id="btnAddPetugas"')?>
      </div>
    </div>    
    <div class="col-sm-12 text-center">
          <button class="btn btn-primary btn-success" id="saveBtn" type="button"><i class="fa fa-save"></i> Simpan</button>
          <a href="<?=$link_index?>" class="btn btn-danger btn-flat" id="cancelButton"><i class="fa fa-ban"></i> Batal</a>
    </div>
      <!-- /.box -->
      </form>
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>