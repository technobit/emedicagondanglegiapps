<section class="content-header">
  <h1 class="title"><?=$title?></h1>
  <?=$breadcrumbs?>
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
    <!-- Cari Data Pasien -->
    <!-- Hasil Pencarian Data Pasien -->
      <div class="box box-primary">
        <div class="box-header">
          <h3 class="box-title">Data Antrian</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <form method="POST" class="form-horizontal" action="" id="form-detail-loket">
            <?=form_hidden('id_jenis_loket', $detail_antrian['intIdPelayanan']);?>
            <?=form_hidden('id_kunjungan_loket', $detail_antrian['intIdKunjunganLoket']);?>
            <div class="form-group">
              <label class="col-sm-3 control-label" for="inputNoId">Tanggal</label>
              <div class="col-sm-9">
                <?=
                form_input('tanggal', indonesian_date($detail_antrian['dtTanggalKunjungan']), 'class="form-control" id="tanggal" readonly=""');
                ?>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-3 control-label" for="inputNoId">No Antrian</label>
              <div class="col-sm-9">
                <?=
                form_input('no_antrian', $detail_antrian['intNoAntri'], 'class="form-control" id="tanggal" readonly=""');
                ?>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-3 control-label" for="inputNoId">Keterangan Kunjungan</label>
              <div class="col-sm-9">
                <?=
                form_textarea('txtKeterangan', $detail_antrian['txtKeterangan'], 'class="form-control" id="txtKeterangan" ');
                ?>
              </div>
            </div>

            <div class="col-md-12 text-center">
              <?=
              form_button('btnSelesai' , '<i class="fa fa-check"></i> Selesai' , 'class="btn btn-success btn-flat" onclick="selesai_antrian('.$detail_antrian['intIdKunjunganLoket'].')"').
              form_button('btnHapus' , '<i class="fa fa-trash"></i> Hapus' , 'class="btn btn-danger btn-flat" onclick="hapus_antrian('.$detail_antrian['intIdKunjunganLoket'].')"').
              form_button('btnPanggilAntrian' , '<i class="fa fa-bullhorn"></i> Panggil' , 'class="btn btn-default btn-flat" onclick="panggil_antrian('.$detail_antrian['intIdKunjunganLoket'].')"').
              anchor($base_url, '<i class="fa fa-undo"></i> Batal', 'class="btn btn-warning btn-flat"')
              ?>
            </div>
            </form>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>