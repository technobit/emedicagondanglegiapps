<section class="content-header">
    <h1 class="title"><?=$title?></h1>
    <?=$breadcrumbs?>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Form Kemasan</h3>
                </div>
                <div class="box-body">
                  <?php getview('layouts/partials/validation') ?>        
                  <?= $this->form->open(null, 'class="form-horizontal"') ?>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Kemasan</label>
                        <div class="col-md-10">
                            <?= $this->form->text('kemasan', null, 'class="form-control"') ?>
                        </div> 
                    </div>
                    <div class="form-group text-center">
                        <button type="submit" class="btn btn-primary btn-lg"><i class="fa fa-print"></i> Simpan</button>   
                        <a href="<?= base_url('apotik/kemasan') ?>" class="btn btn-danger btn-lg"><i class="fa fa-ban"></i> Batal</a>             
                    </div>
                  <?= $this->form->close() ?>
                </div>
            </div>            
        </div>
    </div>
</section>