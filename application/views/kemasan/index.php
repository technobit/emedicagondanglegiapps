<section class="content-header">
    <h1 class="title"><?=$title?></h1>
    <?=$breadcrumbs?>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Data Kemasan</h3>
                </div>
                <div class="box-body"> 
                    <?php getview('layouts/partials/message') ?>           
                    <div class="toolbar">
                        <a href="<?= base_url('data-kemasan/create') ?>" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Kemasan</a>
                    </div><br>
                    <table class="table table-bordered table-condensed table-hover">
                        <thead>
                            <tr>
                                <th>Kemasan</th>
                                <th class="text-center" width="170">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($rsKemasan->result() as $kemasan) { ?>
                                <tr>
                                    <td><?= $kemasan->kemasan ?></td>
                                    <td class="text-center">
                                        <a href="<?= base_url('gudang_apotik/kemasan/edit/' . $kemasan->id) ?>" class="btn btn-info btn-sm"><i class="fa fa-pencil"></i> Ubah</a>
                                        <a onclick="return confirm('Apakah anda yakin akan menghapus data ini?')" href="<?= base_url('gudang_apotik/kemasan/delete/' . $kemasan->id) ?>" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> Hapus</a>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>