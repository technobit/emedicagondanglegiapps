<section class="content">
  <div class="info-box bg-red">
    <span class="info-box-icon"><i class="fa fa-warning"></i></span>

    <div class="info-box-content">
      <h1>Akses Ditolak!</h1>
    </div>
  </div>
 <div class="box box-danger box-solid">
        <div class="box-body text-center">
            <h1 style="color:#DD4B39">Maaf, Anda tidak dapat membuka menu atau halaman ini!<h1>
            <h3>Anda tidak memiliki hak akses untuk membuka menu atau halaman <?=$meta_title?>. Untuk bisa mendapatkan hak akses silahkan hubungi Administrator.<h3>
        </div>
  </div>
</section>