<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title><?=$meta_title?> | eMedica - Sistem Informasi Fasilitas Pelayanan Kesehatan Terpadu</title>
    <link href="<?=ASSETS_URL?>/img/icon.png" rel="shortcut icon" />
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.2 -->
    <link href="<?=ASSETS_URL?>bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css" />
    <?php
      echo $custom_css;
    ?>
    <!-- Theme style -->
    <link href="<?=ASSETS_URL?>dist/css/AdminLTE.css" rel="stylesheet" type="text/css" />
    <link href="<?=ASSETS_URL?>plugins/font-awesome-4.6.3/css/font-awesome.min.css" rel="stylesheet" >
    <link href="<?=ASSETS_URL?>dist/css/skins/_all-skins.css" rel="stylesheet" type="text/css" />
    <link href="<?=ASSETS_URL?>plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
    <!-- new post -->
    <!-- datepicker -->
    <link href="<?=ASSETS_URL?>plugins/datepicker/datepicker3.css" rel="stylesheet" type="text/css" />
    <link href="<?=ASSETS_URL?>custom/css/global.css" rel="stylesheet" type="text/css" />
    <!-- jQuery 2.1.3 -->
    <script src="<?=ASSETS_URL?>plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <script src='http://localhost/emedica-bululawang/assets/plugins/playsound/jquery.playSound.js'></script>
    <!-- Bootstrap datetime Picker -->
    <!-- audio player-->
    <script>
      var global_url = '<?=base_url();?>';
    </script>
  </head>
  <body class="skin-green-light layout-top-nav">
    <div class="wrapper">
    <header class="main-header">
    <!-- Header Navbar: style can be found in header.less -->
    <nav role="navigation" class="navbar navbar-static-top text-center">
      <a class="logo" href="<?=base_url()?>self-service" style="float:left;height: 70px;padding: 0px 10px;">
		<img height="100%" src="<?=$logo?>">
      </a>
      <a class="logo" href="<?=base_url()?>self-service" style="float:right;height: 60px;padding: 10px;">
		<img height="100%" src="<?=base_url()?>assets/img/emedica_white.png">
      </a>
    </nav>
</header>
      <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <?=$main_content?>
    </div><!-- /.content-wrapper -->
    <?=$main_footer?>
    </div><!-- ./wrapper -->
    
<!-- Bootstrap 3.3.2 JS -->
<script src="<?=ASSETS_URL?>bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<!-- Slimscroll -->
<script src="<?=ASSETS_URL?>plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<!-- FastClick -->
<script src='<?=ASSETS_URL?>plugins/fastclick/fastclick.min.js'></script>
<!-- DataTables -->
<script src='<?=ASSETS_URL?>plugins/datatables/jquery.dataTables.min.js'></script>
<script src='<?=ASSETS_URL?>plugins/datatables/dataTables.bootstrap.min.js'></script>
<!-- Date Picker-->
<script src='<?=ASSETS_URL?>plugins/datepicker/bootstrap-datepicker.js'></script>
<script src='<?=ASSETS_URL?>plugins/moment/moment.min.js'></script>
<!-- bootbox-->
<script src='<?=ASSETS_URL?>plugins/bootbox/bootbox.js'></script>
<!-- AdminLTE App -->
<script src="<?=ASSETS_URL?>dist/js/app.min.js" type="text/javascript"></script>
<!-- Globak App -->
<script src="<?=ASSETS_JS_URL?>global.js" type="text/javascript"></script>
<?=$custom_js?>
  </body>
</html>