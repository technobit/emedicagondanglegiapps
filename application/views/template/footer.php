<footer class="main-footer no-print">
    <div class="pull-right hidden-xs" style="display:inline-flex">
      <span style="margin-top:-5px">
        <img src="<?=ASSETS_URL?>img/emedica.png" height="25px">
      </span>
      <span style="margin:0 10px">
      <b>Version</b> <?=APP_VERSION?>
      </span>
    </div>
    <strong>Copyright &copy; 2015-2016 <a href="http://technobit.id">Technobit Indonesia</a>.</strong> All rights reserved.
</footer>