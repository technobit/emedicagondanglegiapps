<header class="main-header">
    <!--<a href="#" class="menu-bar-button" data-toggle="offcanvas" role="button"> <span class="fa fa-bars"></span></a>-->
    <span class="menu-bars" data-toggle="offcanvas" role="button">
        <span class="menu-bars-mini">
            <span class="fa fa-bars"></span>
        </span>
        <span class="menu-bars-lg">
            <span class="fa fa-bars"></span>
        </span>
    </span>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="<?=base_url()?>" class="logo">
		<img src="<?=$logo?>" height="100%">
      </a>
      <a href="<?=base_url()?>loket/antrian/78" class="sidebar-toggle"><i class="fa fa-folder"> </i> <span>Loket Umum</span></a>
      <a href="<?=base_url()?>loket/antrian/79" class="sidebar-toggle"><i class="fa fa-folder"> </i> <span>Loket BPJS</span></a>
      <a href="<?=base_url()?>loket/antrian/80" class="sidebar-toggle"><i class="fa fa-folder"> </i> <span>Loket Rawat Inap</span></a>
      <!--<a href="<?=base_url()?>ugd/6" class="sidebar-toggle"><i class="fa fa-warning"> </i> <span>UGD</span></a>-->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <span class="hidden-xs"><i class="fa fa-user"> </i> <?=$username?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <p>
                  <?=$username?> - Admin
                  <small><?=$useremail?></small>
                </p>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="<?=$link_edit_profil?>" class="btn btn-default btn-flat">Edit Profile</a>
                </div>
                <div class="pull-right">
                  <a href="<?=$link_logout?>" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
</header>