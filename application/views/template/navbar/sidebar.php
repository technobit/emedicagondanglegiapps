<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
        <!--<li class="header">MAIN MENU</li>-->
			<?php foreach($sidebar_nav as $index  => $val): ?>
				<?php if(isset($val['sub_menu']) && ($val['sub_menu']=="")): ?>
                	<li>
                    	<a href="<?=base_url().$val['main_url']?>">
                        	<i class="<?=$val['class']?>"></i>
                            <span><?=$val['desc']?></span>
                            </a>
                    </li>
                <?php else : ?>
                    <li class="treeview">
                        <a href="#">
                        	<i class="<?=$val['class']?>"></i>
                            <span><?=$val['desc']?></span>
                            <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">
                            <?php if(isset($submenu_side[$val["sub_menu"]])) :
                                $subMenu = $submenu_side[$val["sub_menu"]];
                                foreach($subMenu as $indexSub => $row) : ?>
                                    <?php if(isset($row['subsub_menu']) && ($row['subsub_menu']=="")): ?>
                                        <li>
                                            <a href="<?=base_url().$row['url']?>"><span class="<?=$row['class']?>"></span> <?=$row['desc']?></a>
                                        </li>
                                    <?php else : ?>
                                        <li class="treeview">
                                            <a href="#">
                                            	<i class="<?=$row["class"]?>"></i>
                                                <span><?=$row["desc"]?> </span>
                            					<i class="fa fa-angle-left pull-right"></i>
                                            </a>
                                            <ul class="treeview-menu">
                                                <?php if(isset($subsubmenu_side[$row["subsub_menu"]])) :
                                                $subSubMenu = $subsubmenu_side[$row["subsub_menu"]];
                                                foreach($subSubMenu as $indexSubSub => $rows) : ?>
                                                    <li><a href="<?=base_url().$rows['url']?>"><span class="<?=$rows['class']?>"></span> <?=$rows['desc']?></a></li>	
                                                <?php endforeach ?>
                                                <?php endif ?>
                                            </ul>
                                        </li>
                                    <?php endif;
                                 endforeach;
                            endif; ?>
                        </ul>
                    </li>
                <?php endif; ?>
            <?php endforeach; ?>
        </ul>
        <!-- Sidebar user panel -->
    </section>
    <!-- /.sidebar -->
</aside>