<header class="main-header">
    <!--<a href="#" class="menu-bar-button" data-toggle="offcanvas" role="button"> <span class="fa fa-bars"></span></a>-->
    <span role="button" data-toggle="offcanvas" class="menu-bars">
        <span class="menu-bars-mini">
            <span class="fa fa-bars"></span>
        </span>
        <span class="menu-bars-lg">
            <span class="fa fa-bars"></span>
        </span>
    </span>
    <!-- Header Navbar: style can be found in header.less -->
    <nav role="navigation" class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a class="logo" href="<?=base_url()?>">
		<img height="100%" src="<?=$logo?>">
      </a>
      <a class="sidebar-toggle" href="<?=base_url()?>antrian/dashboard/"><i class="fa fa-desktop"> </i> <span>Dashboard</span></a>
    </nav>
</header>