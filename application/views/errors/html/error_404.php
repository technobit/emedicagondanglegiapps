<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Halaman tidak dapat diakses</title>
    <link href="<?=ASSETS_URL?>/img/icon.png" rel="shortcut icon" />
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.2 -->
    <link href="<?=ASSETS_URL?>bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="<?=ASSETS_URL?>dist/css/AdminLTE.css" rel="stylesheet" type="text/css" />
    <link href="<?=ASSETS_URL?>plugins/font-awesome-4.6.3/css/font-awesome.min.css" rel="stylesheet" >
  </head>
  <body class="skin-green-light sidebar-mini sidebar-collapse">
    <!-- Content Header (Page header) -->
    <section class="content">
  <div class="info-box bg-yellow">
    <span class="info-box-icon"><i class="fa fa-warning"></i></span>

    <div class="info-box-content">
      <h1>Halaman Tidak Ditemukan!</h1>
    </div>
  </div>
  <div class="box box-warning box-solid">
        <div class="box-body text-center">
            <h1 style="color:#F39C12">Maaf, halaman yang Anda akses tidak ditemukan atau tidak tersedia<h1>
            <h3>Silahkan <a class="btn btn-success" href="<?=BASE_URL?>" style="font-size:20px;margin-top:-5px">Klik Disini !</a> untuk kembali ke halaman utama. <h3>
        </div>
  </div>
    </section> 
    <!-- Bootstrap 3.3.2 JS 
    <script src="<?=ASSETS_URL?>bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- AdminLTE App 
    <script src="<?=ASSETS_URL?>dist/js/app.min.js" type="text/javascript"></script>--?
</body>
</html>