<section class="content-header">
  <h1 class="title"><?=$title?></h1>
  <?=$breadcrumbs?>
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
    <!-- Hasil Pencarian Data Pasien -->
        <div class="box box-success">
        <!-- form start -->
            <div class="box-header">
                <h3 class="box-title">Informasi Pengguna Layanan</h3>
            </div>
            <form class="form-horizontal" id="frm-detail-register">
            <div class="box-body">
                <div class="col-md-6">
                    <?=$intIdRekamMedisDetail?>
                    <?=$txtIdRekamMedis?>
                    <?=$txtIdKunjungan?>
                    <?=$txtIdPasien?>
                    <?=$txtNamaPasien?>
                    <?=$txtNoRekamMedis?>
                    <?=$txtKamarPasien?>
                    <?=$txtTanggalMasuk?>
                </div>
                <div class="col-md-6">
                    <?=$txtUsiaPasien?>
                    <?=$txtJenisKelamin?>
                    <?=$txtJaminanKesehatan?>
                    <?=$txtNoJaminanKesehatan?>
                </div>                
            <div>
                <a href="<?=$link_detail_register?>" class="btn btn-primary"><i class="fa fa-list"></i> Detail Administrasi</a>
            </div>
            </div>
            </form>
        </div>
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Detail Rawat Inap</h3>
                <div class="box-tools">
                    <button class="btn btn-primary" type="button" id="btnResumeMedik"><i class="fa fa-calendar-check-o"></i> Selesai Rawat Inap</button>
                </div>
            </div>
            <div class="box-body">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab_1" data-toggle="tab">Anamnesis</a></li>
                    <li><a href="#tab_3" data-toggle="tab">Pemeriksaan Fisik Dan Lanjut</a></li>
                    <li><a href="#tab_8" data-toggle="tab">Diagnosis Dan Terapi</a></li>
                    <li><a href="#tab_12" data-toggle="tab">Asuhan Keperawatan</a></li>
                    <li><a href="#tab_6" data-toggle="tab">Perkembangan</a></li>
                    <li><a href="#tab_5" data-toggle="tab">Pengobatan</a></li>
                    <li><a href="#tab_7" data-toggle="tab">Pemberian Cairan</a></li>
                    <li><a href="#tab_9" data-toggle="tab">Tindakan</a></li>
                    <li><a href="#tab_10" data-toggle="tab">Pemeriksaan Lab</a></li>
                    <li><a href="#tab_11" data-toggle="tab">Resume Medis</a></li>
                    <li><a href="#tab_2" data-toggle="tab">Riwayat Rekam Medis</a></li>
                    <li><a href="#tab_4" data-toggle="tab">Rujukan / Pemeriksaan Lanjut</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <div id="form-diagnosa"></div>
                        </div>
                        <div class="tab-pane" id="tab_2">
                        <table class="table table-hover" id="table-rekam-medis">
                        <thead>
                            <tr>
                            <th>Tanggal</th>
                            <th>Poli</th>
                            <th>Pemeriksaan</th>
                            <th>Diagnosa</th>
                            <th>Kode ICD-X</th>
                            <th>Terapi / Pengobatan</th>
                            <th>Dokter</th>
                            <th>Tanda Tangan</th>
                            <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                        </tbody>
                        </table>
                        </div>
                        
                        <div class="tab-pane" id="tab_3">
                            <div id="form-pemeriksaan"></div>
                        </div>
                        <div class="tab-pane" id="tab_4">
                            <div id="form-periksa-lanjut"></div>
                        </div>
                        <div class="tab-pane" id="tab_5">
                        <div class="pull-right">
                                <button class="btn btn-success" id="btnGetFormAntrian" type="button"><i class="fa fa-edit"></i> Buat Antrian Resep</button>
                                <button class="btn btn-warning" id="btnRefreshPengobatan" type="button"><i class="fa fa-refresh"></i> Refresh</button>
                                <button class="btn btn-primary" id="btnAddFormPengobatan" type="button"><i class="fa fa-plus"></i> Tambah Data</button>
                                
                            </div>
                            <table class="table table-hover" id="table-pengobatan">
                            <thead>
                                <tr>
                                <th>Tanggal</th>
                                <th>Nama Obat</th>
                                <th>Dosis</th>
                                <th>Waktu Pemberian</th>
                                <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                            </tbody>
                            </table>
                        </div>
                        <div class="tab-pane" id="tab_6">
                            <div class="pull-right">
                                <button class="btn btn-warning" id="btnRefreshPerkembangan" type="button"><i class="fa fa-refresh"></i> Refresh</button>
                                <button class="btn btn-primary" id="btnAddFormPerkembangan" type="button"><i class="fa fa-plus"></i> Tambah Catatan</button>
                            </div>
                            <table class="table table-hover" id="table-perkembangan">
                            <thead>
                                <tr>
                                <th>Tanggal</th>
                                <th>Instruksi Dokter</th>
                                <th>Catatan Perawat</th>
                                <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                            </tbody>
                            </table>
                        </div>
                        <div class="tab-pane" id="tab_7">
                            <div class="pull-right">
                                <button class="btn btn-warning" id="btnRefreshCairan" type="button"><i class="fa fa-refresh"></i> Refresh</button>
                                <button class="btn btn-primary" id="btnAddFormCairan" type="button"><i class="fa fa-plus"></i> Tambah</button>
                            </div>
                            <table class="table table-hover" id="table-cairan">
                            <thead>
                                <tr>
                                <th>Tanggal</th>
                                <th>Jam</th>
                                <th>Jenis Cairan</th>
                                <th>Dosis</th>
                                <th>Jumlah Urin</th>
                                <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                            </tbody>
                            </table>
                        </div>
                        <div class="tab-pane" id="tab_12">
                            <div class="pull-right">
                                <button class="btn btn-warning" id="btnRefreshAsuhan" type="button"><i class="fa fa-refresh"></i> Refresh</button>
                                <button class="btn btn-primary" id="btnAddFormAsuhan" type="button"><i class="fa fa-plus"></i> Tambah</button>
                            </div>
                            <table class="table table-hover" id="table-asuhan">
                            <thead>
                                <tr>
                                <th>Tanggal</th>
                                <th>Diagnosa Keperawatan</th>
                                <th>Intervensi</th>
                                <th>Implementasi</th>
                                <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                            </tbody>
                            </table>
                        </div>
                        <div class="tab-pane" id="tab_8">
                            <div id="form-assesment"></div>
                        </div>
                        <div class="tab-pane" id="tab_9">
                            <div class="pull-right">
                                <button class="btn btn-warning" id="btnRefreshTindakan" type="button"><i class="fa fa-refresh"></i> Refresh</button>
                                <button class="btn btn-primary" id="btnAddFormTindakan" type="button"><i class="fa fa-plus"></i> Tambah</button>
                            </div>
                            <table class="table table-hover" id="table-tindakan">
                            <thead>
                                <tr>
                                <th>Tanggal</th>
                                <th>Tindakan</th>
                                <th>Deskripsi Tindakan</th>
                                <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                            </tbody>
                            </table>
                        </div>
                        <div class="tab-pane" id="tab_10">
                        <table class="table table-hover" id="tbl-detail-lab">
                        <thead>
                            <tr>
                            <th>Tanggal Pemeriksaan</th>
                            <th>Jenis Pemeriksaan</th>
                            <th>Hasil Pemeriksaan</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                        </tbody>
                        </table>
                        </div>
                        <div class="tab-pane" id="tab_11">
                            <div id="form-resume-medis"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>