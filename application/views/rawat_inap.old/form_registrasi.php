<section class="content-header">
  <h1 class="title"><?=$title?></h1>
  <?=$breadcrumbs?>
</section>
<section class="content">
  <div class="row">
    <div class="col-md-12">
    <form method="POST" action="?" class="" id="frm-registrasi-pasien">
        <?=$txtIdKunjungan?>
        
        <div class="box box-success">
            <div class="box-header">
                <h3 class="box-title">Identitas Pengguna Layanan</h3>
            </div>
            <div class="box-body">
                <div class="col-md-6">
                    <?=$frm_id_pasien?>
                    <?=$frm_id_rekam_medis?>
                    <?=$intIdRekamMedisRawatInap?>
                    <?=$frm_id_pelayanan?>
                    <?=$frm_status_pasien?>
                    <?=$frm_nama_pasien?>
                    <?=$frm_umur_pasien?>
                    <?=$frm_jk_pasien?>
                    <?=$frm_alamat?>
                    <?=$frm_telpon_pasien?>
                    <?=$frm_agama?>
                    <?=$frm_pendidikan?>
                </div>
                <div class="col-md-6">
                    <?=$intIdKeluarga?>
                    <?=$frm_nama_keluarga?>
                    <?=$frm_usia_keluarga?>
                    <?=$frm_jk_keluarga?>
                    <?=$frm_alamat_keluarga?>
                    <?=$frm_telpon_keluarga?>
                    <?=$frm_agama_keluarga?>
                    <?=$frm_pendidikan_keluarga?>
                    <?=$frm_hubungan_keluarga?>
                </div>
            </div>
        </div>
        </form>
        <form method="POST" action="?" class="form-horizontal" id="frm-ruangan-pasien">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Ruangan Pengguna Layanan</h3>
            </div>
            <div class="box-body">
                <div class="form-group">
                <label class="col-sm-3 control-label" for="selectJamkes">Jaminan Kesehatan</label>
                <div class="col-sm-9">
                    <?=$selectJamkes?>
                    <?=$inputNoJamkes?>
                </div>
                </div>
                <?=$tglMasuk?>
                <?=$selectKamar?>
            </div>
        </div>
        <div class="text-center col-md-12">
                <button class="btn btn-success btn-flat" type="button" id="simpanDataIdentitas"><i class="fa fa-save"></i> <?=$buttonLabel?></button>
        </div>
    </div>
  </div>
</section>