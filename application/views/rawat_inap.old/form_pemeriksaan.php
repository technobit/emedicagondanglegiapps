<form class="form-horizontal clearfix" method="POST" action="?" id="frm-pemeriksaan-rawat-inap">
    <?=$txtIdRekmedDetail?>
    <?=$txtKeadaanUmum?>
    <div class="form-group">
        <label class="col-sm-3 control-label form-label" for="txtPemeriksaan">Pemeriksaan Fisik</label>
        <div class="col-sm-3">
            <label class="control-label form-label">TD</label>
            <div class="input-group">
                <?=$frm_rekmed_td?>
                <span class="input-group-addon">mmHg</span>
            </div>
        </div>
        <div class="col-sm-3">
            <label class="control-label form-label">Nadi</label>
            <div class="input-group">
                <?=$frm_rekmed_nadi?>
                <span class="input-group-addon">X/Menit</span>
            </div>
        </div>
        <div class="col-sm-3 col-sm-offset-3">
            <label class="control-label form-label">Temp</label>
            <div class="input-group">
                <?=$frm_rekmed_temp?>
                <span class="input-group-addon">C</span>
            </div>
        </div>
        <div class="col-sm-3">
            <label class="control-label form-label">RR</label>
            <div class="input-group">
                <?=$frm_rekmed_rr?>
                <span class="input-group-addon">X/Menit</span>
            </div>
        </div>
        <div class="col-sm-3 col-sm-offset-3">
            <label class="control-label form-label">Gizi</label>
            <?=$frm_rekmed_gizi?>
        </div>
        <div class="col-sm-3">
            <label class="control-label form-label">Berat Badan</label>
            <div class="input-group">
                <?=$frm_rekmed_berat_badan?>
                <span class="input-group-addon">kg</span>
            </div>
        </div>
        <div class="col-sm-3 col-sm-offset-3">
            <label class="control-label form-label">Tinggi Badan</label>
            <div class="input-group">
                <?=$frm_rekmed_tinggi_badan?>
                <span class="input-group-addon">cm</span>
            </div>
        </div>
        <div class="col-sm-3">
            <label class="control-label form-label">Kesadaran</label>
            <?=$frm_rekmed_kesadaran?>
        </div>
    </div>
    <?=$txtKepala?>
    <?=$txtThorax?>
    <?=$txtAbdomen?>
    <?=$txtEkstrimitas?>
    <?=$txtLaboratorium?>
    <?=$txtECG?>
    <?=$txtLainLain?>
    <div class="col-md-12 text-center">
        <button type="button" id="btnSimpan" class="btn btn-primary"><i class="fa fa-save"></i> <?=$buttonLabel?></button>
           <?php  if ($buttonLabel =="Update") :?>
                    <a href="<?=$link_cetak?>" target="_blank" id="" class="btn btn-warning" type="button"><i class="fa fa-print"></i> Cetak</a>
     
                 <?php endif; ?>
                        <a href="<?=$link_cetak?>" target="_blank" style="display:none;" id="btnCetakPemeriksaan" class="btn btn-warning" type="button"><i class="fa fa-print"></i> Cetak</a>
     
    </div>
</form>
<script>
$(function(){
    $('#frm-pemeriksaan-rawat-inap').validate({
        ignore : "",
        rules : {
            "txtKeadaanUmum" : {
                required : true,
            }
        }
    });

    $('#btnSimpan').click(function(){
        if($('#frm-pemeriksaan-rawat-inap').valid()){
            saveDataPemeriksaan();
        }
    });
});

function saveDataPemeriksaan(){
    $.ajax({
        url : global_url + "rawat-inap/save-pemeriksaan",
        type : "POST",
        data : $('#frm-pemeriksaan-rawat-inap').serialize(),
        dataType : "html",
        success : function msg(result){
            var data = jQuery.parseJSON(result);
            var status = data['status'];
            var message = data['message'];
             var id = data['id'];
            alertPopUp(status , message , "");
              $('#btnCetakPemeriksaan').css('display','inline');
           // $("#btnCetakPemeriksaan").attr('href', '<?=$link_cetak?>'+id);
        }
    });
}
</script>