<form class="form-horizontal clearfix" method="POST" action="?" id="frm-assesment-planning">
    <?=$txtIdRekmedDetail?>
    <?=$txtDiagnosis?>
    <?=$txtTerapi?>
    <div class="col-md-12 text-center">
        <button id="btnSimpanAssesment" class="btn btn-primary" type="button"><i class="fa fa-save"></i> <?=$buttonLabel?></button>
              <?php  if ($buttonLabel =="Update") :?>
                    <a href="<?=$link_cetak?>" target="_blank" id="" class="btn btn-warning" type="button"><i class="fa fa-print"></i> Cetak</a>
     
              <?php endif; ?>
              
             <a href="<?=$link_cetak?>" target="_blank" style="display:none;" id="btnCetakAss" class="btn btn-warning" type="button"><i class="fa fa-print"></i> Cetak</a>
     
     
</form>

<script>
$(function(){
    $('#frm-assesment-planning').validate({
        ignore : "",
        rules : {
            "txtAssesment" : {
                required : true,
            }
        }
    });
    
    $('#btnSimpanAssesment').click(function(){
        if($('#frm-assesment-planning').valid()){
            saveDataAssesment();
        }
    });
});

function saveDataAssesment(){
    $.ajax({
        url : global_url + "rawat-inap/save-assesment",
        type : "POST",
        data : $('#frm-assesment-planning').serialize(),
        dataType : "html",
        success : function msg(result){
            var data = jQuery.parseJSON(result);
            var status = data['status'];
            var message = data['message'];
            var id = data['id'];
            alertPopUp(status , message , "");
            $('#btnCetakAss').css('display','inline');
            //$("#btnCetakAss").attr('href', '<?=$link_cetak?>'+id);
        }
    });
}
</script>