<div class="clearfix">
    <div class="col-md-12">
    <h4 class="">Form Diagnosa</h4>
        <form method="POST" action="?" class="form-horizontal" id="frm-diagnosis-rawat-inap">
            <?=$frm_id_detail_rekmed?>
            <!--Form Diagnosa Sekarang-->
            <?=$frm_rekmed_diagnosa?>
            <?=$frm_rekmed_detail_diagnosa?>
            <?=$frm_no_diagosa?>
            <div id="additional-rekmed"><?=$frm_add_diagosa?></div>
            <div class="col-sm-9 col-sm-offset-3">
            <div class="form-group text-right">
                <button onclick="addRekmedAdd(1)" class="btn btn-success btn-flat" id="addRekmed" type="button"><i class="fa fa-plus"></i> Tambah Diagnosa</button>
            </div>
            </div>
            <!--Form Diagnosa Keluarga-->
            <?=$frm_rekmed_diagnosa_keluarga?>
            <?=$frm_rekmed_detail_diagnosa_keluarga?>
            <?=$frm_no_diagosa_keluarga?>
            <div id="additional-rekmed-keluarga"><?=$frm_add_diagosa_keluarga?></div>
            <div class="col-sm-9 col-sm-offset-3">
            <div class="form-group text-right">
                <button onclick="addRekmedAdd(2)" class="btn btn-success btn-flat" id="addRekmed" type="button"><i class="fa fa-plus"></i> Tambah Diagnosa</button>
            </div>
            </div>
            <!--Form Diagnosa Sosial / Lingkungan-->
            <?=$frm_rekmed_diagnosa_sosial?>
            <?=$frm_rekmed_detail_diagnosa_sosial?>
            <?=$frm_no_diagosa_sosial?>
            <div id="additional-rekmed-sosial"><?=$frm_button_add_diagnosa_sosial?></div>
            <div class="col-sm-9 col-sm-offset-3">
            <div class="form-group text-right">
                <button onclick="addRekmedAdd(3)" class="btn btn-success btn-flat" id="addRekmed" type="button"><i class="fa fa-plus"></i> Tambah Diagnosa</button>
            </div>
            </div>
            <div class="col-md-12 text-center">
                <button type="button" class="btn btn-primary" id="simpanDataDiagnosis"><i class="fa fa-save"></i> <?=$buttonLabel?></button>
            </div>
        </form>
    </div>
</div>
<script>
var base_url_rekmed = global_url+'rekam-medis/';
var base_url_penyakit = global_url+"data-penyakit/";
var number = 1;
var number2 = 1;
var number3 = 1;
var numberTindakan = 1;

function addRekmedAdd (type) {
    //// Diagnosa Biasa
    if(type==1){
        number = parseInt($('input[name=noDiagnosa]').val());
        var htmlRes = '<div class="form-group" id="contSelect'+number+'">'+
                        '<label class="col-sm-3 control-label form-label">Diagnosa Penyakit '+(number + 1)+'</label>'+
                        '<div class="col-sm-9"><select name="selectDiagnosa[]" id="selectDiagnosa'+number+'" class="form-control select-diagnosa">'+
                        '</select></div>'+
                        '</div>';
                
            htmlRes += '<div class="form-group" id="contText'+number+'">'+
                        '<label for="txtDetailDiagnosa'+number+'" class="col-sm-3 control-label form-label">Detail Diagnosa</label>'+
                        '<div class="col-sm-9"><textarea name="txtDetailDiagnosa[]" cols="3" rows="2" id="txtDetailDiagnosa'+number+'" class="form-control"></textarea>'+
                        '</div></div>';

            htmlRes += '<div class="col-sm-9 col-sm-offset-3"><div class="form-group text-left" id="contBtn'+number+'"><button class="btn  btn-default btn-flat" style="margin-left:10px;" onclick="hapusForm(\'1\','+number+')" type="button">Hapus Diagnosa '+number+'</button></div></div>';
        $('#additional-rekmed').append(htmlRes);
        initiateSelect('selectDiagnosa'+number);
        number++;
    }else if(type==2){
        number2 = parseInt($('input[name=noDiagnosaKeluarga]').val());
        var htmlRes2 = '<div class="form-group" id="contSelectKeluarga'+number2+'">'+
                        '<label class="col-sm-3 control-label form-label">Diagnosa Penyakit Keluarga '+(number2 + 1)+'</label>'+
                        '<div class="col-sm-9"><select name="selectDiagnosaKeluarga[]" id="selectDiagnosaKeluarga'+number2+'" class="form-control select-diagnosa">'+
                        '</select></div>'+
                        '</div>';
                
            htmlRes2 += '<div class="form-group" id="contTextKeluarga'+number2+'">'+
                        '<label for="txtDetailDiagnosaKeluarga'+number2+'" class="col-sm-3 control-label form-label">Detail Diagnosa Keluarga</label>'+
                        '<div class="col-sm-9"><textarea name="txtDetailDiagnosaKeluarga[]" cols="3" rows="2" id="txtDetailDiagnosaKeluarga'+number2+'" class="form-control"></textarea>'+
                        '</div></div>';

            htmlRes2 += '<div class="col-sm-9 col-sm-offset-3"><div class="form-group text-left" id="contBtnKeluarga'+number2+'"><button type="button" class="btn btn-default btn-flat" style="margin-left:10px;" onclick="hapusForm(\'2\','+number2+')">Hapus Diagnosa Keluarga '+number2+'</button></div></div>';
        $('#additional-rekmed-keluarga').append(htmlRes2);
        initiateSelect('selectDiagnosaKeluarga'+number2);
        number2++;
    }else if(type==3){
        number3 = parseInt($('input[name=noDiagnosaSosial]').val());
        var htmlRes3 = '<div class="form-group" id="contSelectSosial'+number3+'">'+
                        '<label class="col-sm-3 control-label form-label">Diagnosa Penyakit Lingkungan '+(number3 + 1)+'</label>'+
                        '<div class="col-sm-9"><select name="selectDiagnosaSosial[]" id="selectDiagnosaSosial'+number3+'" class="form-control select-diagnosa">'+
                        '</select></div>'+
                        '</div>';
                
            htmlRes3 += '<div class="form-group" id="contTextSosial'+number3+'">'+
                        '<label for="txtDetailDiagnosaSosial'+number3+'" class="col-sm-3 control-label form-label">Detail Diagnosa Lingkungan</label>'+
                        '<div class="col-sm-9"><textarea name="txtDetailDiagnosaSosial[]" cols="3" rows="2" id="txtDetailDiagnosaSosial'+number3+'" class="form-control"></textarea>'+
                        '</div></div>';

            htmlRes3 += '<div class="col-sm-9 col-sm-offset-3"><div class="form-group text-left" id="contBtnSosial'+number3+'"><button type="button" class="btn btn-default btn-flat" style="margin-left:10px;" onclick="hapusForm(\'3\','+number3+')">Hapus Diagnosa Sosial '+number3+'</button></div></div>';
        $('#additional-rekmed-sosial').append(htmlRes3);
        initiateSelect('selectDiagnosaSosial'+number3);
        number3++;
    }
}


$(function(){
 $('.select-diagnosa').select2({
        ajax: {
        url: base_url_penyakit+"get-list-penyakit/",
        dataType: 'json',
        delay: 250,
        data: function (params) {
            return {
              q: params.term, // search term
              page: params.page
            };
        },
        processResults: function (data, params) {
        params.page = params.page || 1;
        return {
          results: data.items,
          pagination: {
            more: (params.page * 20) < data.total_count
        }
        };
        },
        cache : true,
  },
  escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
  minimumInputLength: 1,
  templateResult: formatRepo, // omitted for brevity, see the source of this page
  templateSelection: formatRepoSelection, // omitted for brevity, see the source of this page
  placeholder: 'Ketikan Nama Penyakit / Kode ICD',
});

$('#frm-diagnosis-rawat-inap').validate({
    ignore : "",
    rules : {
        "txtDetailDiagnosa[]" : {
            required : true,
        }
    }
});

$('#simpanDataDiagnosis').click(function(){
    if($('#frm-diagnosis-rawat-inap').valid()){
        ///alert("Simpan Data Berhasil");
        saveDataDiagnosis();
    }
});

    
});

function saveDataDiagnosis(){
    $.ajax({
       url : global_url+"rawat-inap/diagnosis/save-diagnosis/",
       type : "POST",
       data : $('#frm-diagnosis-rawat-inap').serialize(),
       dataType : "html",
       success: function msg(res){
            var data = jQuery.parseJSON(res);            
            var status = data['status'];
            var message = data['message'];
            alertPopUp(status , message , "");    
       }
    });
}

function initiateSelect(id) {
    //code
    $('#'+id).select2({
        ajax: {
        url: base_url_penyakit+"get-list-penyakit/",
        dataType: 'json',
        delay: 250,
        data: function (params) {
            return {
              q: params.term, // search term
              page: params.page
            };
        },
        processResults: function (data, params) {
        params.page = params.page || 1;
        return {
          results: data.items,
          pagination: {
            more: (params.page * 20) < data.total_count
        }
        };
        },
        cache : true,
  },
  escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
  minimumInputLength: 1,
  templateResult: formatRepo, // omitted for brevity, see the source of this page
  templateSelection: formatRepoSelection, // omitted for brevity, see the source of this page
  placeholder: 'Ketikan Nama Penyakit / Kode ICD',
});
}

function hapusForm(type,numberSelect) {
    //code
    if(type==1){
        $('#contSelect'+numberSelect).remove();
        $('#contText'+numberSelect).remove();
        $('#contBtn'+numberSelect).remove();
        number--;
    }else if(type==2){
        $('#contSelectKeluarga'+numberSelect).remove();
        $('#contTextKeluarga'+numberSelect).remove();
        $('#contBtnKeluarga'+numberSelect).remove();
        number2--;
    }else if(type==3){
        $('#contSelectSosial'+numberSelect).remove();
        $('#contTextSosial'+numberSelect).remove();
        $('#contBtnSosial'+numberSelect).remove();
        number3--;
    }
    
}
</script>