<section class="content-header">
    <h1 class="title"><?=$title?></h1>
    <?=$breadcrumbs?>
</section>
<section class="content">
<div class="row"> 
  <div class="col-md-12 text-right no-print">    
    <?=form_button("btnCetak" , '<i class="fa fa-print"></i> Cetak' , 'onclick="cetak()" class="btn btn-default btn-flat"')?>
   </div>
</div>
<section class="invoice">
<!-- title row -->

<div class="row">
  <div class="col-xs-12">
    <center>
    <h2 class="page-header">
      <img src="<?=ASSETS_IMAGE_URL?>pkm_bll_black.png" height="50px">
     
    </h2>
    </center>
  </div>
  <!-- /.col -->
</div>
<div class="text-center">
  <h4>Data Pengguna Layanan</h4>
</div>
<div class="row invoice-info">
        <div class="col-sm-6 invoice-col">
          <address>
            <strong>Nama Pengguna Layanan</strong> <?=$detailPasien['txtNamaPasien']?><br>
            <strong>No Rekam Medis</strong> <?=$detailPasien['txtNoRekamMedis']?> <br>
            <strong>Kamar Pengguna Layanan</strong> <?=$detailPasien['txtKamar']?> <br>
            <strong>Tanggal Masuk</strong> <?=$tanggalKunjungan?> <br>
          </address>
        </div>
     
        <div class="col-sm-2 invoice-col">
        </div>
    
        <div class="col-sm-4 invoice-col">
          <b>Usia Pengguna Layanan </b><?=$detailPasien['txtUsiaPasienKunjungan']?><br>
          <b>Jenis Kelamin  </b> <?=$jenisKelamin?><br>
          <b>Jaminan Kesehatan  </b> <?=$detailPasien['txtNamaJaminan']?><br>
          <b>No Jaminan Kesehatan  </b> <?=$detailPasien['txtNoIdJaminanKesehatan']?><br>
        </div>
       
      </div>
<div class="text-center">
  <h4>Diagnonis Dan Terapi</h4>
</div>

<div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          <address>
            <strong>Terapi</strong> 
            </address>
        </div>
     <div class="col-sm-8 invoice-col">
          <?=': '.$detailData['txtTerapi']?><br>
          </div>
</div>
<div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          <address>
            <strong>Diagnosis</strong> 
            </address>
        </div>
     <div class="col-sm-8 invoice-col">
          <?=': '.$detailData['txtDiagnosis']?><br>
          </div>
</div>


<!-- /.row -->    
</section>
</section>