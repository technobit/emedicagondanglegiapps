<section class="content-header">
    <h1 class="title"><?=$title?></h1>
    <?=$breadcrumbs?>
</section>
<section class="content">
<div class="row"> 
  <div class="col-md-12 text-right no-print">    
    <?=form_button("btnCetak" , '<i class="fa fa-print"></i> Cetak' , 'onclick="cetak()" class="btn btn-default btn-flat"')?>
   </div>
</div>
<section class="invoice">
<!-- title row -->

<div class="row">
  <div class="col-xs-12">
    <center>
    <h2 class="page-header">
      <img src="<?=ASSETS_IMAGE_URL?>pkm_bll_black.png" height="50px">
     
    </h2>
    </center>
  </div>
  <!-- /.col -->
</div>
<div class="text-center">
  <h4>Data Pengguna Layanan</h4>
</div>
<div class="row invoice-info">
        <div class="col-sm-6 invoice-col">
          <address>
            <strong>Nama Pengguna Layanan </strong> <?=': '.$detailPasien['txtNamaPasien']?><br>
            <strong>No Rekam Medis </strong> <?=': '.$detailPasien['txtNoRekamMedis']?> <br>
            <strong>Kamar Pengguna Layanan </strong> <?=': '.$detailPasien['txtKamar']?> <br>
            <strong>Tanggal Masuk </strong> <?=': '.indonesian_date($tanggalKunjungan[0]).' '.$tanggalKunjungan[1]?> <br>
          </address>
        </div>
     
        <div class="col-sm-2 invoice-col">
        </div>
    
        <div class="col-sm-4 invoice-col">
          <b>Usia Pengguna Layanan  </b><?=': '.$detailPasien['txtUsiaPasienKunjungan']?><br>
          <b>Jenis Kelamin   </b> <?=$jenisKelamin?><br>
          <b>Jaminan Kesehatan   </b> <?=': '.$detailPasien['txtNamaJaminan']?><br>
          <b>No Jaminan Kesehatan   </b> <?=': '.$detailPasien['txtNoIdJaminanKesehatan']?><br>
        </div>
       
      </div>
<div class="text-center">
  <h4>Resume Medis</h4>
</div>
<div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          <address>
            <strong>Tanggal Keluar</strong> 
            </address>
        </div>
     <div class="col-sm-8 invoice-col">
          <?=': '.indonesian_date($detailData['dtKeluar'])?><br>
          </div>
</div>
<div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          <address>
            <strong>Dokter</strong> 
            </address>
        </div>
     <div class="col-sm-8 invoice-col">
          <?=': '.$arrPegawai[$detailData['intIdPegawai']]?><br>
          </div>
</div>

<div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          <address>
            <strong>Anamnesis</strong> 
            </address>
        </div>
     <div class="col-sm-8 invoice-col">
          <?=': '.$detailData['txtAnamnesis']?><br>
          </div>
</div>
<div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          <address>
            <strong>Periksa Fisik</strong> 
            </address>
        </div>
     <div class="col-sm-8 invoice-col">
          <?=': '.$detailData['txtPeriksaFisik']?><br>
          </div>
</div>
<div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          <address>
            <strong>
Periksa Pengunjung
</strong> 
            </address>
        </div>
     <div class="col-sm-8 invoice-col">
          <?=': '.$detailData['txtPeriksaPenunjang']?><br>
          </div>
</div>
<div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          <address>
            <strong>Diagnosa Penyakit
</strong> 
            </address>
        </div>
     <div class="col-sm-8 invoice-col">
          <?=': '.$rekmed_diagnosa?><br>
          </div>
</div>
<div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          <address>
            <strong>
Detail Diagnosa

</strong> 
            </address>
        </div>
     <div class="col-sm-8 invoice-col">
          <?=': '.$rekmed_detail_diagnosa?><br>
          </div>
</div>
<div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          <address>
            <strong>
Terapi

</strong> 
            </address>
        </div>
     <div class="col-sm-8 invoice-col">
          <?=': '.$detailData['txtTerapi']?><br>
          </div>
</div>
<div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          <address>
            <strong>
Prognosis

</strong> 
            </address>
        </div>
     <div class="col-sm-8 invoice-col">
          <?=': '.$detailData['txtPrognosis']?><br>
          </div>
</div>
<div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          <address>
            <strong>
Jaminan Kesehatan


</strong> 
            </address>
        </div>
     <div class="col-sm-8 invoice-col">
          <?=': '.$arrKeadaan[$detailData['intKeadaanKeluar']]?><br>
          <?php if ($detailData['intKeadaanKeluar'] == 4) {
           echo !empty($detailData['intIdLayananRujukan'])?': '.$arrPelayanan[$detailData['intIdLayananRujukan']]: "";
            
           } ?>
          </div>
</div>
<div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          <address>
            <strong>
Saran


</strong> 
            </address>
        </div>
     <div class="col-sm-8 invoice-col">
          <?=': '.$detailData['txtSaran']?><br>
          </div>
</div>
<!-- /.row -->    
</section>
</section>