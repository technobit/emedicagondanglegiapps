<form class="form-horizontal clearfix" method="POST" action="?" id="frm-tindakan">
    <?=$txtIdRekmedDetail?>
    <?=$intIdRekmedTindakanInap?>
    <?=$dtTanggal?>
    <?=$intIdTindakan?>
    <?=$txtDeskripsiTindakan?>
    <div class="col-md-12 text-center">
        <button id="btnSimpanTindakan" class="btn btn-primary" type="button"><i class="fa fa-save"></i> <?=$buttonLabel?></button>
    </div>
</form>
<script>

function formatRepo (repo) {
      if (repo.loading) return repo.text;
      var markups = "<div class='select2-result-repository clearfix'>" +
          "<div class='select2-result-repository__meta'>" +
          "<div class='select2-result-repository__title'>" + repo.txtTindakan + "</div>"+
          "</div></div>";
      ///markups = repo.txtIndonesianName;
      return markups;
}
    
function formatRepoSelection (repo) {
      return repo.txtTindakan || repo.text;
}

$(function(){
    $.fn.modal.Constructor.prototype.enforceFocus = function() {};
    $('#frm-tindakan').validate({
        ignore : "",
        rules : {
            "dtTanggal" : {
                required : true,
            },
            "intIdTindakan" : {
                required : true,
            },
        }
    });

    $('#dtTanggal').datetimepicker({
        format : "YYYY-MM-DD HH:mm:ss"
    });

    $('#btnSimpanTindakan').click(function(){
        if($('#frm-tindakan').valid()){
            saveDataTindakan();
        }
    });

    $('.select-tindakan').select2({
            ajax: {
            url: global_url+"data-tindakan/get-list-tindakan/",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                q: params.term, // search term
                page: params.page
                };
            },
            processResults: function (data, params) {
            params.page = params.page || 1;
            return {
            results: data.items,
            pagination: {
                more: (params.page * 20) < data.total_count
            }
            };
            },
            cache : true,
    },
    escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
    minimumInputLength: 1,
    templateResult: formatRepo, // omitted for brevity, see the source of this page
    templateSelection: formatRepoSelection, // omitted for brevity, see the source of this page
    placeholder: 'Ketikan Nama Tindakan',
    });
});

function saveDataTindakan(){
    $.ajax({
        url : global_url + "rawat-inap/save-tindakan",
        type : "POST",
        data : $('#frm-tindakan').serialize(),
        dataType : "html",
        success : function msg(result){
            var data = jQuery.parseJSON(result);
            var status = data['status'];
            var message = data['message'];
            alertPopUp(status , message , "");
            getDataTindakan();
            $('.bootbox').modal("hide");
            
        }
    });
}
</script>