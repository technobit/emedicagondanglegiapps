<section class="content-header">
  <h1 class="title"><?=$title?></h1>
  <?=$breadcrumbs?>
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
    <!-- Hasil Pencarian Data Pasien -->
    <div class="box box-success">
    <!-- form start -->
        <form class="form-horizontal" id="frm-detail-register">
          <div class="box-body">
            <?=$frm_id_rekam_medis?>
            <?=$frm_id_pasien?>
            <?=$frm_id_pelayanan?>
            <?=$frm_id_kunjungan?>
            <?=$frm_tanggal?>
            <?=$frm_no_anggota_pasien?>
            <?=$frm_nama_pasien?>
            <?=$frm_usia_pasien?>
            <?=$frm_jaminan_kesehatan?>
            <?=$frm_no_jaminan_kesehatan?>
            <?=$frm_no_rekam_medis?>
          </div>
        </form>
    </div>
        <?=$frm_rekmed_poli?>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>