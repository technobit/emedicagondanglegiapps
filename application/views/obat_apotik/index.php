<section class="content-header">
    <h1 class="title"><?=$title?></h1>
    <?=$breadcrumbs?>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="control-label col-md-2">Jenis Obat</label>
                            <div class="col-md-4">
                                <?= $this->form->select('filter_jenis_obat', selectJenisObat('Semua'), null, 'id="filter_jenis_obat" class="form-control input-sm"') ?>
                            </div>
                            <label class="control-label col-md-2">Nama Obat</label>
                            <div class="col-md-4">
                                <?= $this->form->text('filter_nama_obat', null, 'id="filter_nama_obat" class="form-control input-sm"') ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-2 col-md-10">
                                <button id="btnCari" class="btn btn-primary btn-sm">Cari</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Data Obat</h3>
                </div>
                <div class="box-body">
                    <?php getview('layouts/partials/message') ?>    
                    <div class="toolbar">
                        <a href="<?= base_url('apotik/'.$this->uri->segment(2).'/permintaan_obat/create') ?>" class="btn btn-primary"><i class="fa fa-plus"></i> Permintaan Obat</a>
                    </div><br>
                    <table id="dataTable" class="table table-bordered table-condensed table-hover" data-apotik="<?= $this->uri->segment(2) ?>">
                        <thead>
                            <tr>
                                <th>Nama Obat</th>
                                <th>Kode Obat</th>
                                <th>Kemasan</th>
                                <th>Jenis Obat</th>
                                <th>Stok</th>                                                      
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
