<section class="content-header">
  <h1 class="title"><?=$title?></h1>
  <?=$breadcrumbs?>
</section>
<section class="content">
   <div class="row">
	<div class="col-sm-3">
      <!-- small box -->
      <div class="small-box bg-blue">
        <div class="inner">
          <p>Jam Operasional</p>
          <h3 id="jam-sekarang"></h3>
        </div>
        <div class="icon">
          <i class="fa fa-clock-o"></i>
        </div>
        <div class="small-box-footer"></div>
      </div>
    </div>
	<div class="col-sm-2">
      <!-- small box -->
      <div class="small-box bg-red">
        <div class="inner">
          <p>Jumlah Antrian</p>
          <h3 id="jumlah-pengunjung-antrian">0</h3>
        </div>
        <div class="icon">
          <i class="fa fa-users"></i>
        </div>
        <div class="small-box-footer"></div>
      </div>
    </div>
	<div class="col-sm-2">
      <!-- small box -->
      <div class="small-box bg-yellow">
        <div class="inner">
          <p>Sedang Dilayani</p>
          <h3 id="jumlah-pengunjung-sedang">0</h3>
        </div>
        <div class="icon">
          <i class="fa fa-user-md"></i>
        </div>
        <div class="small-box-footer"></div>
      </div>
    </div>
	<div class="col-sm-2">
      <!-- small box -->
      <div class="small-box bg-green">
        <div class="inner">
          <p>Selesai Dilayani</p>
          <h3 id="jumlah-pengunjung-selesai">0</h3>
        </div>
        <div class="icon">
          <i class="fa fa-flag"></i>
        </div>
        <div class="small-box-footer"></div>
      </div>
    </div>
	<div class="col-sm-3">
      <!-- small box -->
      <div class="small-box bg-aqua">
        <div class="inner">
          <p>Total Pengunjung</p>
          <h3 id="jumlah-pengunjung">0</h3>
        </div>
        <div class="icon">
          <i class="fa fa-bar-chart"></i>
        </div>
        <div class="small-box-footer"></div>
      </div>
    </div>
    <div class="col-xs-12">
    <!-- Hasil Pencarian Data Pasien -->
      <div class="box box-primary">
        <!-- /.box-header -->
        <div class="box-body table-responsive">
          <table class="table table-hover">
            <thead>
              <tr>
                <th>No Antrian</th>
                <th>Nama Pengguna Layanan</th>
                <th>No Registrasi</th>
                <th>Usia</th>
                <th>Ruangan / Layanan</th>
                <th>Status Pelayanan</th>
              </tr>
            </thead>
            <tbody id="result-register-pasien">
              
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>