<section class="content-header">
  <h1 class="title"><?=$title?></h1>
  <?=$breadcrumbs?>
</section>
<section class="content">
  <div class="row">
      <div class="col-xs-12">
      <div class="box box-success">
        <form class="form-horizontal" id="frm-loket-search" action="" method="POST">
          <div class="box-body">
            <?=form_hidden('id_jenis_loket', $id_jenis_loket);?>
            <div class="form-group">
              <label class="col-sm-3 control-label" for="inputNoId">Tanggal</label>
              <div class="col-sm-9">
                <input type="text" name="txtDate" id="txtDate" class="form-control" value="<?=date("Y-m-d")?>">
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-3 control-label" for="inputNoId">Status Pelayanan</label>
              <div class="col-sm-9"> 
                <?=form_dropdown("intIdStatus" , $list_status , "" , 'class="form-control" id="intIdStatus"')?>
              </div>
            </div>
          <div class="col-sm-9 col-sm-offset-3" style="padding-left: 8px;">
                <button class="btn btn-primary btn-flat" id="searchRegister" type="button"><i class="fa fa-search"></i> Cari</button>
          </div>  
          </div>
     </form>      
     </div>
     </div>
    <div class="col-xs-12">
    <!-- Hasil Pencarian Data Pasien -->
      <div class="box box-primary">
        <!-- /.box-header -->
        <div class="box-body">
          <table class="table table-hover" id="table-data">
            <thead>
              <tr>
                <th>No Antrian</th>
                <th>Loket Antrian</th>
                <th>Status Pelayanan</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody id="result-history-register-pasien">
              
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>