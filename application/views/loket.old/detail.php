<section class="content-header">
  <h1 class="title"><?=$title_section?></h1>
  <?=$breadcrumbs?>
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
    <!-- Cari Data Pasien -->
    <!-- Hasil Pencarian Data Pasien -->
    <div class="alert alert-danger alert-dismissable" id="alert_message" style="display: none;">
		  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
		  <h4><i class="icon fa fa-ban" id="alert_message_icon"></i> Peringatan!</h4>
		  <div id="alert_message_content">Danger alert preview. This alert is dismissable. A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart.</div>
      </div>
      <div class="box box-primary">
        <div class="box-header">
          <h3 class="box-title">Form Register Pengguna Layanan</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <form method="POST" class="form-horizontal" action="" id="form-detail-pelayanan">
                <div class="row">
                        <div class="col-sm-6">
                            <?=$inputIdKunjungan?>
                            <?=$inputIdLoket?>
                            <?=$inputTanggalLahir?>
                            <?=$inputIdPasien?>
                            <?=$inputNoAnggota?>
                            <?=$inputNamaPasien?>
                          <div class="form-group">
                          <label class="col-sm-3 control-label">No KTP / NIK</label>
                            <div class="col-sm-9">
                              <div class="input-group input-group">
                                <?=$inputNoIdentitas?>
                                    <span class="input-group-btn">
                                      <button id="checkBPJSByNIK" type="button" class="btn btn-info btn-flat">Check BPJS By NIK</button>
                                    </span>
                              </div>
                            </div>
                          </div>
                            <?=$inputJenisKelamin?>
                            <?=$inputUsia?>
                            <?=$inputUsiaHari?>
                        </div>
                        <div class="col-sm-6">
                        <?=$inputTanggal?>
                          <div class="form-group">
                          <label class="col-sm-3 control-label" for="selectJamkes">Jaminan Kesehatan</label>
                            <div class="col-sm-9">
                              <?=$selectJamkes?>
                              <div class="input-group input-group-sm">
                                <?=$inputNoJamkes?>
                                    <span class="input-group-btn">
                                      <button id="checkBPJS" type="button" class="btn btn-info btn-flat">Check BPJS</button>
                                    </span>
                              </div>
                            </div>
                          </div>
                            <?=$selectLayanan?>
                            <?=$txtKeterangan?>
                        </div>
                        <div class="col-sm-6">
                          <div class="progress active" id="progressBar" style="display:none;margin:20px 0 0 0">
                            <div style="width:100%" aria-valuemax="30" aria-valuemin="0" aria-valuenow="40" role="progressbar" class="progress-bar progress-bar-success progress-bar-striped">
                              <span style="text-weight:bold">Sedang mencetak nomor antrian</span>
                            </div>
                          </div>
                        </div>
                        <div class="col-sm-6 text-right">
							          <a href="<?=$link_loket?>" class="btn btn-warning btn-flat btn-lg" id="cancelButton"><i class="fa fa-mail-reply"></i> Kembali</a>
                        <button class="btn btn-primary btn-lg btn-flat" id="registerButton"><i class="fa fa-save"></i> Simpan</button>
                        </div>
                </div>          
            </form>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>