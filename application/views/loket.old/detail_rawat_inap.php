<section class="content-header">
  <h1 class="title"><?=$title_section?></h1>
  <?=$breadcrumbs?>
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
    <div class="alert alert-danger alert-dismissable" id="alert_message" style="display: none;">
		  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
		  <h4><i class="icon fa fa-ban" id="alert_message_icon"></i> Peringatan!</h4>
		  <div id="alert_message_content">Danger alert preview. This alert is dismissable. A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart.</div>
      </div>
      <div class="box box-primary">
        <div class="box-header">
          <h3 class="box-title">Form Register Pengguna Layanan Rawat Inap</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <form method="POST" class="form-horizontal" action="" id="form-detail-pelayanan">
                <div class="row">
                        <div class="col-sm-6">
                            <?=$inputIdKunjungan?>
                            <?=$inputIdPasien?>
                            <?=$inputNoAnggota?>
                            <?=$inputNamaPasien?>
                            <?=$inputNoIdentitas?>
                            <?=$inputJenisKelamin?>
                            <?=$inputUsia?>
                        </div>
                        <div class="col-sm-6">
                          <div class="form-group">
                          <label class="col-sm-3 control-label" for="selectJamkes">Jaminan Kesehatan</label>
                            <div class="col-sm-9">
                              <?=$selectJamkes?>
                              <?=$inputNoJamkes?>
                            </div>
                          </div>
                            <?=$selectLayanan?>
                            <?=$selectKamar?>
                        </div>    
                        <div class="col-sm-12 text-right">
							          <a href="<?=$link_loket?>" class="btn btn-warning btn-flat btn-lg" id="cancelButton"><i class="fa fa-ban"></i> Batalkan</a>
                        <button class="btn btn-primary btn-lg btn-flat" id="registerButton"><i class="fa fa-save"></i> Daftarkan</button>
                        </div>
                </div>          
            </form>
        </div>        
      </div>      
    </div>
  </div>
</section>