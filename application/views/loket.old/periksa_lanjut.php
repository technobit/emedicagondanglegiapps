<div class="alert alert-danger alert-dismissable" id="alert_message_periksa" style="display: none;">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <h4><i class="icon fa fa-ban" id="alert_message_periksa_icon"></i> Peringatan!</h4>
        <div id="alert_message_periksa_content"></div>
</div>
<form class="form-horizontal clearfix" id="form-pemeriksaan-lanjut">
        <?=$txtIdKunjungan?>
        <?=$bitIsPeriksaFrm?>
        <?=$selectMode?>
        <?=$selectLayananLanjut?>
        <?=$poliLanjut?>
        <?=$txtIdKunjunganEdit?>
        <div class="form-group rujukan-bpjs" <?=$arrListRujukBPJS?>>
            <label for="kdProvider" class="col-sm-3 control-label form-label" id="lblkdProvider">Provider / Faskes</label>
            <div class="col-sm-9">
                <?=$listProvider?>
                <?=$txtProvider?>
            </div>
        </div>
        <div class="form-group rujukan-bpjs" <?=$arrListRujukBPJS?>>
            <label for="kdProvider" class="col-sm-3 control-label form-label" id="lblkdProvider">Poli / Pelayanan</label>
            <div class="col-sm-9">
                <?=$listFKTL?>
                <?=$txtFKTL?>
            </div>
        </div>
        <?=$txtKeteranganLanjut?>
        <div class="col-sm-9 col-sm-offset-3">
            <button class="btn btn-primary" type="button" id="btnPeriksaLanjut">Periksa</button>
               <button class="btn btn-success" type="button" style="display:none;" id="btnCancel">Cancel</button>
             <?=form_hidden('hidden','','id="hidden"')?>
            <?php 
            
                if($bitIsPeriksa==true) : 
            ?>  
                <button class="btn btn-success" type="button" id="btnEdit">Edit</button>
                <button class="btn btn-success" type="button" style="display:none;" id="btnCancel">Cancel</button>
                <button class="btn btn-danger" type="button" id="btnDelete">Batal</button>
            <?php 
                endif;
            ?>
        </div>
</form>
<div >
   
<table class="table table-hover" id="rujuk">
                <thead>
                  <tr>
                    <th>No Antrian</th>
                    <th>Nama Pengguna Layanan</th>
                    <th>No Registrasi</th>
                    <th>Usia</th>
                    <th>Status Pemeriksaan</th>
                    <th>Fasilitas</th>
                    
                    <th>Hasil Rujukan</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  
                </tbody>
              </table>
              </div>
<script>
    $(function(){
        $('#btnEdit').click(function(){
          /*  $('#form-pemeriksaan-lanjut :input').removeAttr("disabled");
            $('#btnCancel').css("display" , "inline");
            $('#btnEdit').css("display" , "none");*/
            
        });

        $('#btnCancel').click(function(){
            getFormRujukan();
        });
         var data_grid = $('#rujuk').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": false,
          "info": false,
          "autoWidth": true,
          "processing" : true,
          "ajax" : {
            "url" : global_url + 'loket/getDtRujukan/',
            "type" : "POST",
            "data" : function (postParams) {
                postParams.pasien = $('input[name=txtIdPasien]').val();
                
            }
          }
    });

        $('#btnDelete').click(function(){
            var idKunjungan = $('input[name=txtIdKunjungan]').val();
            var conf = confirm("Apakah Rujukan Batal?");
            if(conf==true){
                $.ajax({
                    url : global_url+"loket/hapusDataRujukan/",
                    type : "POST",
                    data : $('#form-pemeriksaan-lanjut').serialize(),
                    dataType : "html",
                    success: function msg(res){
                        var data = jQuery.parseJSON(res);
                        var status = data['status'];
                        var message = data['message'];
                        alertPopUp(status , message , "");
                        getFormRujukan();
                    }
                }); 
            }
        });

        $('#kdProvider').change(function(){
            var providerTxt = $('#kdProvider option:selected').text();
            $('input[name=txtProvider]').val(providerTxt);
        });

        $('#kdFKTL').change(function(){
            var txtFKTL = $('#kdFKTL option:selected').text();
            $('input[name=txtFKTL]').val(txtFKTL);
        });

        $('#lblpoliLanjut').attr('style' , 'display:none');
        $('#btnPeriksaLanjut').click(function(){
            if($('#form-pemeriksaan-lanjut').valid()){
                var r = confirm("Apakah Pengguna Layanan Di Rujuk?");
                if(r==true){
                    saveReferral();    
                }
            }
        });

        $('#form-pemeriksaan-lanjut').validate({
            ignore : "",
            rules : {
                txtKeteranganPemeriksaan : {
                    required : false,
                },
            }
        });
    
      /*  $('#selectModeRujukan1').click(function(){
            if($('#selectModeRujukan1').is(':checked')){
                getListFasilitasRujuk("FASILITAS");
                enablePoliLuar(false);
                enablePoliBPJS(false);
                enableStatus(false);
            }    
        });*/
        $('#selectModeRujukan1').click(function(){
            if($('#selectModeRujukan1').is(':checked')){
                getListFasilitasRujuk("POLI");
                enablePoliLuar(false);
                enablePoliBPJS(false);
                enableStatus(true);
            }    
        });
        $('#selectModeRujukan2').click(function(){
            if($('#selectModeRujukan2').is(':checked')){
                getListFasilitasRujuk("RUJUKAN_LUAR");
                enablePoliLuar(true);
                enablePoliBPJS(false);
                enableStatus(false);
            }    
        });
        $('#selectModeRujukan3').click(function(){
            if($('#selectModeRujukan3').is(':checked')){
                enablePoliLuar(false);
                enablePoliBPJS(true);
                enableStatus(false);
            }    
        });
    });


function getListFasilitasRujuk(jenisFasilitas){
   ///$('#selectLayananLanjut').select2('destroy');
   //$('#poliLanjut').select2('destroy');
   $.ajax({
       url : global_url+"data-pelayanan/get-list-pelayanan/",
       type : "POST",
       data : "jenisFasilitas="+jenisFasilitas,
       dataType : "html",
       success: function msg(res){
            var data = jQuery.parseJSON(res);
            var status = data['status'];
            var dataFasilitas = data['data'];
            var htmlRes = "";
           
            for(i = 0; i < dataFasilitas.length; i++){
              
                htmlRes += '<option  value="'+dataFasilitas[i]['intIdPelayanan']+'">'+dataFasilitas[i]['txtNama']+'</option>';
            }
            $('#selectLayananLanjut').html(htmlRes);
       }
    }); 
}
function getListFasilitasRujuk2(jenisFasilitas, pel){
   ///$('#selectLayananLanjut').select2('destroy');
   //$('#poliLanjut').select2('destroy');
   $.ajax({
       url : global_url+"data-pelayanan/get-list-pelayanan/",
       type : "POST",
       data : "jenisFasilitas="+jenisFasilitas,
       dataType : "html",
       success: function msg(res){
            var data = jQuery.parseJSON(res);
            var status = data['status'];
            var dataFasilitas = data['data'];
            var htmlRes = "";
            var selected = "";
            for(i = 0; i < dataFasilitas.length; i++){
                if (pel != "") {
                    if(dataFasilitas[i]['intIdPelayanan'] == pel){
                        selected = 'selected="selected"';
                        
                    }else{
                        selected ="";
                    }
                }
                htmlRes += '<option '+selected+' value="'+dataFasilitas[i]['intIdPelayanan']+'">'+dataFasilitas[i]['txtNama']+'</option>';
            }
            $('#selectLayananLanjut').html(htmlRes);
       }
    }); 
}


function saveReferral(){
    $.ajax({
       url : global_url+"loket/save-referral-antrian/",
       type : "POST",
       data : $('#form-pemeriksaan-lanjut').serialize(),
       dataType : "html",
       success: function msg(res){
            var data = jQuery.parseJSON(res);
            var status = data['status'];
            var message = data['message'];
            ////messageBox(status , message , 'alert_message_periksa');
            alertPopUp(status,message , "");
            ///window.location = base_url_poli;
            getFormRujukan();
       }
    }); 
}

function enablePoliLuar(status){
    if(status==true){
        $('#lblpoliLanjut').removeAttr('style');
        $('#poliLanjut').removeAttr('style');
    }else{
        $('#poliLanjut').attr('style' , 'display:none');
        $('#lblpoliLanjut').attr('style' , 'display:none')
    }
    
}

function enablePoliBPJS(status){
    if(status==true){
        $('#cont-selectLayananLanjut').attr('style' , 'display:none');
        $('.rujukan-bpjs').removeAttr('style');
    }else{
        $('.rujukan-bpjs').attr('style' , 'display:none');
        $('#cont-selectLayananLanjut').removeAttr('style');
    }
}
function enableStatus(status){
    if(status==true){
        $('#cont-txtKeteranganPemeriksaan').attr('style' , 'display:none');
    }else{
        $('#cont-txtKeteranganPemeriksaan').removeAttr('style');
    }
}

function editdata(id){
    $('input[name="txtIdKunjunganEdit"]').val(id);
    $.ajax({
       url : global_url+"loket/getDetailLoket/"+id,
       type : "POST",
       success: function msg(data){
       //var data = jQuery.parseJSON(res);
         var mode = data.bitPeriksaLanjut;
         var pelayanan = data.intIdPelayanan;
         var poliLanjut = data.intIdPoliLuar;
         var keterangan = data.txtKeteranganKunjungan;
         var fktp = data.kdPoliRujukLanjut;
         var bpjs = data.txtKodePelayananBPJS;
         var kunjungan = data.txtIdKunjungan;
        $('input[name="bitIsPeriksa"]').val('true');
         $('#btnCancel').css("display" , "inline");
         $('#btnEdit').css("display" , "inline");
         
         if (mode == 2) {
             $('#selectModeRujukan1').attr('checked',true);
                getListFasilitasRujuk2("POLI",pelayanan);
                enablePoliLuar(false);
                enablePoliBPJS(false);
                enableStatus(true);
                //$('#selectLayananLanjut').val(pelayanan);
         }else if (mode == 3) {
             $('#selectModeRujukan2').attr('checked',true);
                getListFasilitasRujuk2("RUJUKAN_LUAR",pelayanan);
                enablePoliLuar(true);
                enablePoliBPJS(false);
                enableStatus(false);
                //$('#selectLayananLanjut').val(pelayanan);
                $('#poliLanjut').val(poliLanjut);
                $('#txtKeteranganPemeriksaan').val(keterangan);
         }else if (mode == 4) {
             $('#selectModeRujukan3').attr('checked',true);
                enablePoliLuar(false);
                enablePoliBPJS(true);
                enableStatus(false);
               // $('#selectLayananLanjut').val(pelayanan);
                $('#kdProvider').val(bpjs);
                $('#kdFKTL').val(fktp);
                $('#txtKeteranganPemeriksaan').val(keterangan); 
        }
               
       }
    }); 
}

</script>