<section class="content-header">
  <h1 class="title"><?=$title?></h1>
  <?=$breadcrumbs?>
</section>

<section class="content">
  <div class="row">
    <div class="col-xs-12">
    <!-- Cari Data Pasien -->
    <div class="box box-success">
      <div class="box-header with-border">
        <h3 class="box-title">Cari Data Pengguna Layanan</h3>
      </div>
    <!-- /.box-header -->
    <!-- form start -->
        <form class="form-horizontal" id="frm-search-pasien" >
          <div class="box-body">
            <div class="form-group">
              <label class="col-sm-3 control-label" for="inputNoId">No. Anggota / No. Identitas/ Nama Pengguna Layanan</label>
              <div class="col-sm-9">
                <div class="input-group">
                    <input type="text" name="id_pasien" id="id_pasien" class="form-control">
                    <span class="input-group-btn">
                      <button class="btn btn-success btn-flat" id="searchPasien" type="button"><i class="fa fa-search"></i> Cari</button>
                    </span>
                </div>
              </div>
            </div>
          </div>
        </form>
    </div>
    <!-- Hasil Pencarian Data Pasien -->
      <div class="box box-primary">
        <div class="box-header">
          <h3 class="box-title">Data Pengguna Layanan</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive">
          <a href="<?=$link_form_register?>" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Pasien Baru</a>
          <table class="table table-hover">
            <thead>
              <tr>
                <th>No KTP</th>
                <th>No Anggota</th>
                <th>Nama Pengguna Layanan</th>
                <th>Tanggal Lahir</th>
                <th>Jenis Kelamin</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody id="result-search-pasien">
              
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>