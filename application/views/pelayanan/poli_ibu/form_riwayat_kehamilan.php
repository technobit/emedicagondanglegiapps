<div class="alert alert-danger alert-dismissable" id="alert_message" style="display: none;">
    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
    <h4><i class="icon fa fa-ban" id="alert_message_icon"></i> Peringatan!</h4>
    <div id="alert_message_content"></div>
</div>
<form method="POST" action="?" class="form-horizontal" id="frm-riwayat-kehamilan">
    
    <?=$intIdRiwayatKehamilan?>
    <?=$intIdRekamMedisIbu?>
    <?=$intKehamilanKe?>
    <?=$txtKomplikasi?>
    <?=$txtKondisiPersalinan?>
    <div class="form-group">
    <label class="col-sm-3 control-label" for="selectJamkes">Tempat Persalinan</label>
      <div class="col-sm-9">
        <?=$txtTempatPersalinan?>
        <?=$txtTempatPersalinanLain?>
      </div>
    </div>
    <?=$txtKomplikasiPersalinan?>
    <div class="form-group">
    <label class="col-sm-3 control-label" for="selectJamkes">Penolong</label>
      <div class="col-sm-9">
        <?=$txtPenolong?>
        <?=$txtPenolongLain?>
      </div>
    </div>    
    <?=$txtJenisKelamin?>
    <?=$txtBeratBadan?>
    <?=$txtKeadaanBBL?>
    <?=$txtKeadaanSekarang?>
    <?=$txtISKb?>
    <div class="text-center">
        <button class="btn btn-success btn-flat" type="button" id="simpanData"><i class="fa fa-save"></i> Simpan</button>
    </div>
</form>
<script>
    $(function(){
        $('#frm-riwayat-kehamilan').validate({
            ignore : "",
            rules : {
                intKehamilanKe : {
                    required : true,
                },
                txtBeratBadanBBL : {
                    required : true,
                }
            }
        });
        
        $('#simpanData').click(function(){
            if ($('#frm-riwayat-kehamilan').valid()) {
                //code
                saveDataRiwayatKehamilan();
            }
            
        });
    });
    
function saveDataRiwayatKehamilan() {
    //code
    $.ajax({
       url : global_url+"rekam-medis-ibu/save-data-riwayat-kehamilan-ibu/",
       type : "POST",
       data : $('#frm-riwayat-kehamilan').serialize(),
       dataType : "html",
       success: function msg(res){
            var data = jQuery.parseJSON(res);
            var status = data['status'];
            var message = data['message'];
            messageBox(status , message , 'alert_message');
            if (status==true) {
             //code
             getDataRiwayatKehamilan();
             $('.bootbox').modal("hide");
            }
       }
    }); 

}
</script>