<div class="clearfix">
<div class="alert alert-danger alert-dismissable" id="alert_message3" style="display: none;">
    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
    <h4><i class="icon fa fa-ban" id="alert_message3_icon"></i> Peringatan!</h4>
    <div id="alert_message3_content"></div>
</div>
<div class="col-md-12">
<h4 class="">Form Pemeriksaan</h4>    
</div>
<form method="POST" action="?" class="form-horizontal" id="frm-pemeriksaan">
    <div class="col-md-6">
    <?=$intIdRekamMedisIbu?>
    <?=$txtTanggalUpdate?>
    <?=$txtFormMode?>
    <?=$txtTB?>
    <?=$txtLila?>
    <?=$txtIMT?>
    <?=$txtBentukTubuh?>
    <?=$txtKesadaran?>
    <?=$txtMuka?>
    <?=$txtKulit?>
    <?=$txtMata?>
    </div>
    <div class="col-md-6">
    <?=$txtMulut?>
    <?=$txtGigi?>
    <?=$txtPembesaranKel?>
    <?=$txtDada?>
    <?=$txtParuJantung?>
    <?=$txtJantung?>
    <?=$txtRefleks?>
    <?=$txtPayudara?>
    <?=$txtTungkai?>
    </div>
    <div class="col-md-12 text-center">
        <button class="btn btn-success btn-flat" type="button" id="simpanDataPemeriksaan"><i class="fa fa-save"></i> <?=$buttonLabel?></button>
    </div>
</form>
</div>
<script>
    $(function(){
        $('#frm-pemeriksaan').validate({
            ignore : "",
            rules : {
                txtTB : {
                    required : true,
                },
                txtLila : {
                    required : true,
                },
            }
        });
        
        $('#simpanDataPemeriksaan').click(function(){
            if ($('#frm-pemeriksaan').valid()) {
                //code
                saveDataPemeriksaan();
            }
            
        });
    });
    
function saveDataPemeriksaan() {
    //code
    $.ajax({
       url : global_url+"rekam-medis-ibu/save-data-pemeriksaan-ibu/",
       type : "POST",
       data : $('#frm-pemeriksaan').serialize(),
       dataType : "html",
       success: function msg(res){
            var data = jQuery.parseJSON(res);
            var status = data['status'];
            var message = data['message'];
            messageBox(status , message , 'alert_message3');
       }
    }); 

}
</script>