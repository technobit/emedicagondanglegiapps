<div class="clearfix">
<div class="alert alert-danger alert-dismissable" id="alert_pelayanan" style="display: none;">
    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
    <h4><i class="icon fa fa-ban" id="alert_pelayanan_icon"></i> Peringatan!</h4>
    <div id="alert_pelayanan_content"></div>
</div>
<form method="POST" action="?" class="form-horizontal" id="frm-periksa-lanjut">
    <div class="col-md-12">
        <?=$selectMode?>
        <?=$selectLayanan?>
        <?=$txtKeteranganPemeriksaan?>
    </div>
    <div class="text-center col-md-12">
        <button class="btn btn-success btn-flat" type="button" id="btnPeriksaLanjut"><i class="fa fa-save"></i> Periksa</button>
    </div>
</form>
</div>
<script>
    var idKunjungan = $('input[name=txtIdKunjungan]').val();
    var idPelayanan = $('input[name=txtIdPelayanan]').val();
    $(function(){
        $('#btnPeriksaLanjut').click(function(){
        if($('#frm-periksa-lanjut').valid()){
            var r = confirm("Apakah Pasien Di Rujuk?");
            if(r==true){
                saveReferral();    
            }
        }
    });
    $('#frm-periksa-lanjut').validate({
            ignore : "",
            rules : {
                txtKeteranganPemeriksaan : {
                    required : true,
                },
                selectModeRujukan : {
                    required : true,
                }
            }   
        });
});    
function saveReferral(){
    $.ajax({
       url : global_url+"loket/save-referral-antrian/",
       type : "POST",
       data : $('#frm-periksa-lanjut').serialize()+"&idKunjungan="+idKunjungan,
       dataType : "html",
       success: function msg(res){
            var data = jQuery.parseJSON(res);
            var status = data['status'];
            var message = data['message'];
            messageBox(status , message , 'alert_message_periksa');
            window.location = global_url+"pelayanan/poli-ibu/"+idPelayanan;
       }
    }); 
}
</script>
