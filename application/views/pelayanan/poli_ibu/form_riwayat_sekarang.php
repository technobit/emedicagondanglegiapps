<div class="clearfix">
<div class="alert alert-danger alert-dismissable" id="alert_message2" style="display: none;">
    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
    <h4><i class="icon fa fa-ban" id="alert_message2_icon"></i> Peringatan!</h4>
    <div id="alert_message2_content"></div>
</div>
<div class="col-md-12">
<h4 class="">Form Riwayat Kehamilan Sekarang</h4>    
</div>

<form method="POST" action="?" class="form-horizontal" id="frm-riwayat-sekarang">
    <div class="col-md-6">
    <?=$intIdRekamMedisIbu?>
    <?=$txtTanggalUpdate?>
    <?=$txtFormMode?>
    <?=$txtHaid?>
    <?=$txtBBHamil?>
    <?=$txtMual?>
    <?=$txtPusing?>
    <?=$txtNyeriPerut?>
    <?=$txtGerakJanin?>
    <?=$txtOedema?>
    <?=$txtNafsuMakan?>
    <?=$txtPendarahan?>
    <?=$txtPenyakitDiDerita?>
    
    </div>
    <div class="col-md-6">
    <?=$txtRiwayatPenyakitKeluarga?>
    <?=$txtKebiasaanIbu?>
    <?=$txtStatusTT?>
    <?=$txtFaktorResikoAids?>
    <?=$txtHPL?>
    <?=$txtSkorKSPR?>
    <?=$txtKeluhanPasien?>
    <?=$txtRujukanKe?>
    </div>
    <div class="col-md-12 text-center">
        <button class="btn btn-success btn-flat" type="button" id="simpanDataRiwayatSekarang"><i class="fa fa-save"></i> <?=$buttonLabel?></button>
    </div>
</form>
</div>
<script>
    $(function(){
        $('#frm-riwayat-sekarang').validate({
            ignore : "",
            rules : {
                txtBBHamil : {
                    required : true,
                },
            }
        });
        
        $('#simpanDataRiwayatSekarang').click(function(){
            if ($('#frm-riwayat-sekarang').valid()) {
                //code
                saveDataRiwayatSekarang();
            }
            
        });
    });
    
function saveDataRiwayatSekarang() {
    //code
    $.ajax({
       url : global_url+"rekam-medis-ibu/save-data-riwayat-sekarang-ibu/",
       type : "POST",
       data : $('#frm-riwayat-sekarang').serialize(),
       dataType : "html",
       success: function msg(res){
            var data = jQuery.parseJSON(res);
            var status = data['status'];
            var message = data['message'];
            messageBox(status , message , 'alert_message2');
       }
    }); 

}
</script>