<section class="content-header">
  <h1 class="title"><?=$title?></h1>
  <?=$breadcrumbs?>
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
    <!-- Hasil Pencarian Data Pasien -->
    <div class="box box-success collapsed-box">
    <div class="box-header with-border">
        <h3 class="box-title">Data Pasien</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
          </button>
        </div>
              <!-- /.box-tools -->
      </div>
    <!-- form start -->
        <form class="form-horizontal" id="frm-detail-register">
          <div class="box-body">
            <?=$frm_id_rekam_medis?>
            <?=$frm_id_pasien?>
            <?=$frm_id_pelayanan?>
            <?=$frm_id_kunjungan?>
            <?=$frm_id_rujukan?>
            <div class="col-sm-6">
            <?=$frm_tanggal?>
            <?=$frm_no_anggota_pasien?>
            <?=$frm_nama_pasien?>
            <?=$frm_usia_pasien?>
            <?=$frm_alamat_pasien?>
            </div>
            
            <div class="col-sm-6">
            <?=$frm_jaminan_kesehatan?>
            <?=$frm_no_jaminan_kesehatan?>
           
            <?=$frm_txt_kunjungan?>
            </div>
            
          </div>
        </form>
    </div>
        <?=$frm_rekmed_poli?>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>