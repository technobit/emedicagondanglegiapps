
<section class="content-header">
  <h1 class="title"><?=$title?></h1>
  <?=$breadcrumbs?>
</section>
<section class="content">
  <div class="row">
    <div class="col-sm-4">
        <!-- small box -->
        <div class="small-box bg-green">
            <div class="inner">
              <form class="form-horizontal" id="frm-poli-pasien" style="margin:-4px 0;">
                  <div class="box-body">
                    <?=$frm_tanggal?>
                    <?=$frm_id_poliumum?>
                    <div class="col-sm-10 col-sm-offset-2" style="padding-left:5px;">
                        <button type="button" class="btn btn-default btn-flat" id="registerButton" >Kirim</button>
                    </div>
                  </div>
              </form>
            </div>
            <div class="icon">
              <i class="fa fa-calendar"></i>
            </div>
	        <div class="small-box-footer"></div>
        </div>
    </div>
    <div class="col-sm-4">
      <!-- small box -->
      <div class="small-box bg-red">
        <div class="inner">
          <p>Jumlah Antrian</p>
          <h3 id="jumlah-pengunjung">0</h3>
        </div>
        <div class="icon">
          <i class="fa fa-users"></i>
        </div>
        <div class="small-box-footer"></div>
      </div>
    </div>
    <div class="col-sm-4">
      <!-- small box -->
      <div class="small-box bg-blue">
        <div class="inner">
          <p>Jam Operasional</p>
          <h3 id="jam-sekarang"></h3>
        </div>
        <div class="icon">
          <i class="fa fa-clock-o"></i>
        </div>
        <div class="small-box-footer"></div>
      </div>
    </div>
    <div class="col-xs-12">
    <!-- Hasil Pencarian Data Pasien -->
     <div class="box box-primary">
        <!-- /.box-header -->
        <div class="box-body table-responsive">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a data-toggle="tab" href="#tab_1">Daftar Antrian</a></li>
              <li onclick="getDataSedangDiLayani()"><a data-toggle="tab" href="#tab_2">Sedang Dilayani</a></li>
              <li onclick="getDataSelesai()"><a data-toggle="tab" href="#tab_3">Telah Dilayani</a></li>
              <li onclick="getDataRujukan()"><a data-toggle="tab" href="#tab_4">Pemeriksaan Lanjut / Rujukan Dalam</a></li>
            </ul>
            <div class="tab-content">
              <!-- /.tab-pane Form Rawat Jalan-->
              <div id="tab_1" class="tab-pane active">
                <table class="table table-hover">
                <thead>
                  <tr>
                    <th>No Antrian</th>
                    <th>Nama Pasien</th>
                    <th>No Registrasi</th>
                    <th>Usia</th>
                    <th>Status Pelayanan</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody id="result-register-pasien">
                  
                </tbody>
              </table>
              </div>
              <!-- /.tab-pane -->
              <div id="tab_2" class="tab-pane">
                <table class="table table-hover">
                <thead>
                  <tr>
                    <th>No Antrian</th>
                    <th>Nama Pasien</th>
                    <th>No Registrasi</th>
                    <th>Usia</th>
                    <th>Status Pelayanan</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody id="result-register-sedang">
                  
                </tbody>
              </table>
              </div>
              <div id="tab_3" class="tab-pane">
                <table class="table table-hover">
                <thead>
                  <tr>
                    <th>No Antrian</th>
                    <th>Nama Pasien</th>
                    <th>No Registrasi</th>
                    <th>Usia</th>
                    <th>Status Pelayanan</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody id="result-register-selesai">
                  
                </tbody>
              </table>
              </div>
              <div id="tab_4" class="tab-pane">
                <table class="table table-hover">
                <thead>
                  <tr>
                    <th>No Antrian</th>
                    <th>Nama Pasien</th>
                    <th>No Registrasi</th>
                    <th>Usia</th>
                    <th>Status Pemeriksaan</th>
                    <th>Fasilitas</th>
                    <th>Status Pelayanan</th>
                  </tr>
                </thead>
                <tbody id="result-register-rujuk">
                  
                </tbody>
              </table>
              </div>

            </div>
            <!-- /.tab-content -->
          </div>
          
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>