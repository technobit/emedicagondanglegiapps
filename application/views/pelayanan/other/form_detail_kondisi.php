<?= $this->form->open(null, 'class="form-horizontal"') ?>
<div class="row">
<div class="col-md-4">
    <div class="box box-primary">
        <div class="box box-header">
            <h4 class="box-title">Tanda Vital</h4>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-4 control-label">Tekanan Darah</label>
        <div class="col-md-8">
          <?= $this->form->text('txtNoAnggota', null, 'class="form-control"') ?>
          <span class="help-block">Cth : 10 mmHg</span>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-4 control-label">Frekuensi Nadi</label>
        <div class="col-md-8">
          <?= $this->form->text('txtNoAnggota', null, 'class="form-control"') ?>
          <span class="help-block">Kali / Menit</span>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-4 control-label">Suhu</label>
        <div class="col-md-8">
          <?= $this->form->text('txtNoAnggota', null, 'class="form-control"') ?>
          <span class="help-block">Suhu (Celcius)</span>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-4 control-label">Frekuensi Nafas</label>
        <div class="col-md-8">
          <?= $this->form->text('txtNoAnggota', null, 'class="form-control"') ?>
          <span class="help-block">Frekuensi / Menit</span>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-4 control-label">Skor Nyeri</label>
        <div class="col-md-8">
          <?= $this->form->text('txtNoAnggota', null, 'class="form-control"') ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-4 control-label">Skor Jatuh</label>
        <div class="col-md-8">
          <?= $this->form->text('txtNoAnggota', null, 'class="form-control"') ?>
        </div>
    </div>
</div>
<div class="col-md-4">
    <div class="box box-primary">
        <div class="box box-header">
            <h4 class="box-title">Apnometri</h4>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-4 control-label">Berat Badan</label>
        <div class="col-md-8">
          <?= $this->form->text('txtNoAnggota', null, 'class="form-control"') ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-4 control-label">Tinggi Badan</label>
        <div class="col-md-8">
          <?= $this->form->text('txtNoAnggota', null, 'class="form-control"') ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-4 control-label">Lingkar Kepala</label>
        <div class="col-md-8">
          <?= $this->form->text('txtNoAnggota', null, 'class="form-control"') ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-4 control-label">IMT</label>
        <div class="col-md-8">
          <?= $this->form->text('txtNoAnggota', null, 'class="form-control"') ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-4 control-label">Lingkar Lengan Atas</label>
        <div class="col-md-8">
          <?= $this->form->text('txtNoAnggota', null, 'class="form-control"') ?>
        </div>
    </div>
</div>
<div class="col-md-4">
    <div class="form-group">
        <label class="col-md-4 control-label">Fungsional</label>
        <div class="col-md-8">
          <?= $this->form->text('txtNoAnggota', null, 'class="form-control"') ?>
        </div>
    </div>
</div>
</div>
<div>
    
</div>
<?= $this->form->close() ?>