<div class="form-group">
    <?= $this->form->text('txtCariLayanan', $this->input->get('key'), 'placeholder="Cari berdasarkan nama layanan" id="txtCariLayanan" class="form-control"') ?>
</div>
<table id="dataTable" class="table table-bordered table-condensed table-hover">
    <thead>
        <tr>
            <th>Nama Layanan</th>            
            <th>Jasa Sarana</th>
            <th>Jasa Pelayanan</th>
            <th>Tarif</th>
            <th width="1px"></th>
        </tr>
    </thead>
</table>

<script>
$(function() {
    var searchTimer;
    var dataTable;

    dataTable = $('#dataTable').dataTable({
        processing : true,
        serverSide : true,
        ajax : {
            url : '<?= base_url('keuangan/rawat_inap/pelayanan/get_cari_layanan?key='.$this->input->get('key')) ?>',
            type : 'post'
        },
        columns : [
            {data : 'nama_item'},
            {data : 'jasa_sarana', class : 'text-right'},
            {data : 'jasa_pelayanan', class : 'text-right'},
            {data : 'tarif', class : 'text-right'},            
            {data : 'id', orderable : false, render : function(data, type, row) {
                    return '<button type="button" class="btn btn-xs btn-primary" onclick="pilihLayanan(\''+row['id']+'\')"><i class="fa fa-check"></i></button>'
                }
            }
        ],
        autoWidth : false,
        filter : false
    });    

    $('#txtCariLayanan').keyup(function() {
        clearTimeout(searchTimer);
        searchTimer = setTimeout(function() {
            dataTable.api().ajax.url('<?= base_url('keuangan/rawat_jalan/pelayanan/get_cari_layanan?key='.$this->input->get('key')) ?>'+$('#txtCariLayanan').val()).load();
        }, 500)
    });

});

function pilihLayanan(id) {
    tambahLayanan(id);
    $('.bootbox').modal('hide');
}
</script>