<section class="content-header">
    <h1 class="title"><?=$title?></h1>
    <?=$breadcrumbs?>
</section>
<section class="content">
  <div class="row"> 
    <div class="col-md-12 text-right">
      <a href="<?= base_url('keuangan/rawat_inap/pelayanan/' . formValue('txtIdKunjungan')) ?>" class="btn btn-primary"><i class="fa fa-plus"></i> Pembayaran Baru</a>
      <a href="<?= base_url('keuangan/rawat_inap/riwayat/' . formValue('txtIdKunjungan')) ?>" class="btn btn-success"><i class="fa fa-list"></i> Riwayat Pembayaran</a>
      <a href="<?= base_url('keuangan/rawat_inap/total/' . formValue('txtIdKunjungan')) ?>" class="btn btn-info"><i class="fa fa-list"></i> Total Pembayaran</a>
    </div>
  </div>
  <br>
  <?= $this->form->open(null, 'id="form-pelayanan" class="form-horizontal" data-apotik="'.$this->uri->segment(2).'"') ?>
    <div class="row">
      <div class="col-md-8">        
        <div class="box box-primary">
          <div class="box-header">
            <h3 class="box-title">Tindakan</h3>
          </div>
          <div class="box-body">
            <table class="table table-condensed">              
              <thead>
                <tr>
                  <th>Tanggal</th>
                  <th>Tindakan</th>
                  <th>Keterangan</th>
                </tr>
              </thead>          
              <tbody>
                <?php foreach ($rsTindakan as $tindakan) { ?>
                  <tr>
                    <td><?= indonesian_date($tindakan->dtTanggal) ?></td>
                    <td><?= $tindakan->txtTindakan ?></td>
                    <td><?= $tindakan->txtDeskripsiTindakan ?></td>
                  </tr>
                <?php } ?>
              </tbody>    
            </table>
          </div>
        </div>
        <div class="box box-primary">
          <div class="box-header">
            <h3 class="box-title">Riwayat Pembayaran</h3>
          </div>
          <div class="box-body">                                                                        
            <table id="tblLayanan" class="table table-bordered table-condensed table-hover">
              <thead>
                <tr>
                  <th>Tanggal</th>
                  <th width="100px" class="text-right">Total Tagihan</th>
                  <th width="110px" class="text-right">Total Bayar</th>
                  <th width="90px" class="text-center">Bebas Biaya?</th>                  
                  <th width="100px" class="text-center">Aksi</th>
                </tr>
              </thead>
              <tbody>     
                 <?php $total_tagihan = 0 ?>
                 <?php foreach($rsRiwayat as $riwayat) { ?>
                  <tr>
                    <td><?= indonesian_date($riwayat->tgl_bayar) ?></td>
                    <td class="text-right"><?= toCurrency($riwayat->total_tagihan) ?></td>
                    <td class="text-right"><?= toCurrency($riwayat->total_bayar) ?></td>
                    <td class="text-center">
                      <?php if ($riwayat->bitBebasBiaya == 1) { ?>
                        <label class="label label-success">Ya</label>
                      <?php } else { ?>
                        <label class="label label-danger">Tidak</label>
                      <?php } ?>
                    </td>
                    <td class="text-center">
                      <a href="<?= base_url('keuangan/rawat_inap/total_partial/'.$idKunjungan."/". $riwayat->id) ?>" class="btn btn-default btn-xs"><i class="fa fa-print"></i></a>
                      <a href="<?= base_url('keuangan/rawat_inap/edit/' . $riwayat->id) ?>" class="btn btn-warning btn-xs"><i class="fa fa-pencil"></i></a>
                      <a href="<?= base_url('keuangan/rawat_inap/delete/' . $riwayat->id) ?>" class="btn btn-danger btn-xs" onclick="return confirm('Apakah anda yakin akan menghapus data ini ?')"><i class="fa fa-trash"></i></a>
                    </td>
                  </tr>
                  <?php $total_tagihan+=$riwayat->total_tagihan ?>
                <?php } ?>       
              </tbody>
            </table>
            <br>
            <div class="form-group text-center">                
                <a href="<?= base_url('keuangan/rawat_inap') ?>" class="btn btn-danger btn-lg"><i class="fa fa-ban"></i> Batal</a>             
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="box box-primary">
          <div class="box-body">
            <div class="text-right">
              <input type="hidden" name="total_tagihan" id="grandTotal" value="<?= $total_tagihan ?>">
              <span style="font-size: 60px" id="grandTotalLabel"><?= toCurrency($total_tagihan) ?></span>
            </div>
          </div>
        </div>
        <div class="box box-primary">
          <div class="box-header">
              <h4 class="box-title">Data Pengguna Layanan</h4>
          </div>
          <div class="box-body">              
            <div class="form-group">
              <label class="col-md-4 control-label">No. Anggota</label>
              <div class="col-md-8">
                <input type="hidden" name="kunjungan" id="kunjungan" value="<?= $this->uri->segment(4) ?>"/>
                <?= $this->form->text('txtNoAnggota', null, 'class="form-control" readonly') ?>
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-4 control-label">Nama Pengguna Layanan</label>
              <div class="col-md-8">
                <?= $this->form->text('txtNamaPasien', null, 'class="form-control" readonly') ?>
              </div>
            </div> 
            <div class="form-group">
              <label class="col-md-4 control-label">No. KTP/NIK</label>
              <div class="col-md-8">
                <?= $this->form->text('txtNoKtp', null, 'class="form-control" readonly') ?>
              </div>
            </div> 
            <div class="form-group">
              <label class="col-md-4 control-label">Jenis Kelamin</label>
              <div class="col-md-8">
                <?= $this->form->select('charJkPasien', selectJenisKelamin(), null, 'class="form-control" disabled') ?>
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-4 control-label">Jaminan Kesehatan</label>
              <div class="col-md-8">                
                <?= $this->form->select('intIdJaminanKesehatan', selectJamkes(), null, 'class="form-control" disabled') ?>
              </div>
            </div>           
          </div>                          
        </div>
        <div class="box box-primary">
          <div class="box-header">
            <h3 class="box-title">Data layanan</h3>
          </div>
          <div class="box-body">
            <div class="form-group">
              <label class="col-md-4 control-label">Dari</label>
              <div class="col-md-8">
                <?= $this->form->text('txtNama', null, 'class="form-control" readonly') ?>
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-4 control-label">Kamar</label>
              <div class="col-md-8">
                <?= $this->form->text('txtKamar', null, 'class="form-control" readonly') ?>
              </div>
            </div>           
          </div>
        </div>
      </div>    
    </div>    
    <div class="modal fade" id="frmBayar" tabindex="-1" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Bayar</h4>
          </div>
          <div class="modal-body">              
            <div class="form-horizontal">
              <div class="form-group">                                
                <div class="col-md-12">
                  <input type="text" name="total_tagihan2" id="tagihan" class="form-control text-right" style="font-size:100px;height: 120px;" readonly>   
                </div>
              </div>
              <div class="form-group">                                
                <div class="col-md-12">
                  <input type="text" name="total_bayar" id="bayar" class="form-control text-right" style="font-size:100px;height: 120px;">   
                </div>
              </div>
              <div class="form-group">
                <div class="col-md-4">
                  <?= $this->form->checkbox('bitBebasBiaya', '1') ?>
                  <span>Bebas Biaya?</span>
                </div>
                <label class="col-md-2 control-label">Kembalian</label>
                <div class="col-md-6">
                  <input type="text" id="kembalian" class="form-control text-right" readonly>
                </div>                
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-success" onclick="return simpanPembayaran()">Simpan</button>
          </div>
        </div>
      </div>
    </div>
  <?= $this->form->close() ?>
</section>