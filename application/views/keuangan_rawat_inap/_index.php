<section class="content-header">
    <h1 class="title"><?=$title?></h1>
    <?=$breadcrumbs?>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-6">
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3 id="antrian_hari_ini"></h3>
                    <p>Antrian Hari Ini</p>
                </div>
                <div class="icon">
                    <i class="fa fa-users"></i>
                </div>
            </div>
        </div>
        <div class="col-xs-6">
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3 id="uang_masuk_hari_ini"></h3>
                    <p>Uang Masuk Hari ini</p>
                </div>
                <div class="icon">
                    <i class="fa fa-money"></i>
                </div>
            </div>
        </div>        
    </div>
    <div class="row">
        <!--<div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-body">
                   <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-md-2 control-label">Tanggal</label>
                            <div class="col-md-10">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input type="text" value="<?= date('Y-m-d') ?>" id="filterTanggal" class="form-control" />
                                </div>
                            </div>                           
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-2 col-md-10">
                                <button id="btnTampilkan" class="btn btn-primary">Tampilkan</button>
                            </div>
                        </div>
                   </div>
                </div>
            </div>-->
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li class="active"><a data-toggle="tab" href="#tab_1">Daftar Antrian</a></li>
                  <li ><a data-toggle="tab" href="#tab_2">Sedang Di Layani</a></li>
                  <li ><a data-toggle="tab" href="#tab_3">Telah Di Layani</a></li>
                </ul>
                <div class="tab-content">
                    <div id="tab_1" class="tab-pane active">
                        <table id="tableAntrian" class="table table-bordered table-condensed table-hover"> 
                            <thead>
                                <tr>
                                    <th>No.Antrian</th>
                                    <th>Nama Pengguna Layanan</th>
                                    <th>Ruangan</th>
                                    <th>Usia</th>                                                                                                
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    <div id="tab_2" class="tab-pane">
                        <table id="tableDiLayani" class="table table-bordered table-condensed table-hover"> 
                            <thead>
                                <tr>
                                    <th>No.Antrian</th>
                                    <th>Nama Pengguna Layanan</th>
                                    <th>Ruangan</th>
                                    <th>Usia</th>                                                                  
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    <div id="tab_3" class="tab-pane">
                    <table id="tableSelesai" class="table table-bordered table-condensed table-hover"> 
                            <thead>
                                <tr>
                                    <th>No.Antrian</th>
                                    <th>Nama Pengguna Layanan</th>
                                    <th>Ruangan</th>
                                    <th>Usia</th>    
                                    <th>Total Tagihan</th>        
                                    <th>Petugas</th>                        
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
