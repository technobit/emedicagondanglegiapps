<section class="content-header">
    <h1 class="title"><?=$title?></h1>
    <?=$breadcrumbs?>
</section>
<section class="content">  
    <div class="row">
        <div class="col-xs-12">            
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li class="active"><a data-toggle="tab" href="#tab_1">Sedang Di Rawat</a></li>
                  <li ><a data-toggle="tab" href="#tab_2">Riwayat Rawat Inap</a></li>                  
                </ul>
                <div class="tab-content">
                    <div id="tab_1" class="tab-pane active">
                        <table id="tableDirawat" class="table table-bordered table-condensed table-hover"> 
                            <thead>
                                <tr>
                                    <th>No.Antrian</th>
                                    <th>Nama Pengguna Layanan</th>
                                    <th>Jenis Pembayaran</th>
                                    <th>Kamar</th>
                                    <th>Tanggal Masuk</th> 
                                    <th>Tanggal Pulang</th> 
                                    <th>Status</th>
                                    <th></th>                                                                                                                                  
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    <div id="tab_2" class="tab-pane">                        
                        <div class="form-group">                                                                
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input type="text" value="<?= date('Y-m-d') ?>" placeholder="Tanggal Pulang" id="filterTanggal" class="form-control" />
                                <div class="input-group-btn">
                                    <button id="btnTampilkan" class="btn btn-primary">Tampilkan</button>
                                </div>
                            </div>                                          
                        </div>                        
                        <table id="tableRiwayat" class="table table-bordered table-condensed table-hover"> 
                            <thead>
                                <tr>
                                    <th>No.Antrian</th>
                                    <th>Nama Pengguna Layanan</th>
                                    <th>Jenis Pembayaran</th>
                                    <th>Kamar</th>
                                    <th>Tanggal Masuk</th> 
                                    <th>Tanggal Pulang</th> 
                                    <th>Status</th>
                                    <th></th>                                                                                                                                  
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>                   
                </div>
            </div>
        </div>
    </div>
</section>
