<section class="content-header">
  <h1 class="title"><?=$title_form?></h1>
  <?=$breadcrumbs?>
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
    <!-- Hasil Pencarian Data Pasien -->
	  <div class="alert alert-danger alert-dismissable" id="alert_message" style="display: none;">
		  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
		  <h4><i class="icon fa fa-ban" id="alert_message_icon"></i> Peringatan!</h4>
		  <div id="alert_message_content"></div>
      </div>
      <div class="box box-primary">
        <div class="box-header">
          <h3 class="box-title">Form Registrasi Pengguna Layanan</h3>
		  <div class="box-tools">
			<?php
			if($mode_form=='detail') { 
			?>
			<a href="<?=$link_input_rekam_medis?>" id="moveKunjunganPasien" class="btn btn-success btn-flat"><i class="fa fa-file-text"></i> Data Rekam Medis</a>
			<?php
			if($status_sidik=='Belum') : 
			?>
				<a onclick="checkFingerRegistration()" href="<?=$linkRegisterFinger?>" class="btn btn-success"><i class="fa fa-hand-pointer-o"></i> Registrasi Sidik Jari</a>
			<?php
			else: 
			?>
				<button class="btn btn-danger" onclick="hapusSidikJari()" type="button"><i class="fa fa-hand-pointer-o"></i> Hapus Sidik Jari</button>
			<?php 
			endif;
			?>
			<?php

			}
			if($mode_form!="detail") : 
			?>
				<a onclick="checkFingerRegistration()" href="<?=$linkRegisterFinger?>" class="btn btn-success"><i class="fa fa-hand-pointer-o"></i> Registrasi Sidik Jari</a>
			<?php 
			endif;
			?>
		  </div>
        </div>
        <!-- /.box-header -->
      <div class="box-body">
		  <form class="form-horizontal" id="form-input-pasien">
          <div class="col-sm-6">
						<?=$inputMode?>
						<?=$inputIdPasien?>
						<div class="form-group">
						<label class="col-sm-3 control-label" for="inputTanggalLahir">No Anggota</label>
              <div class="col-sm-9">
          		<?=$inputNoAnggota?>
							<span class="input-group-addon">
                          <?=$bitSementara?> Sementara? (* Centang Jika Pengguna Layanan Sementara)
              </span>
							<p class="help-block">No Tidak Perlu Di Isi Dan Akan Di Generate Oleh System</p>
              </div>
            </div>
            <?=$inputNamaPasien?>
            <?=$inputNoIdentitas?>
            <?=$selectJenisKelamin?>
            <?=$inputAgama?>
            <?=$inputTempatLahir?>			
            <div class="form-group">
			<label class="col-sm-3 control-label" for="inputTanggalLahir">Tanggal Lahir</label>
              <div class="col-sm-9">
          		<?=$inputTanggalLahir?>
              </div>
            </div>
			<div class="form-group">
                  <label class="col-sm-3 control-label form-label">No. Handphone</label>
                  <div class="col-sm-9">
                    <div class="input-group">
                    <span class="input-group-addon">+62 / 0</span>
                    <?=$txtNoHandphone?>
                    </div>
                  </div>
                </div>
			    <?=$inputPekerjaan?>
          </div>
          <div class="col-sm-6">
					<div class="form-group">
              <label class="col-sm-3 control-label" for="selectJamkes">Jaminan Kesehatan</label>
              <div class="col-sm-9">
                <?=$selectJamkes?>
								<div class="input-group input-group-sm">
                <?=$inputNoJamkes?>
                    <span class="input-group-btn">
                      <button id="checkBPJS" type="button" class="btn btn-info btn-flat">Check BPJS</button>
                    </span>
              </div>
              </div>
            </div>
            <?=$inputAlamat?>
            <?=$selectKecamatan?>
            <?=$selectKelurahan?>
            <?=$selectWilayah?>
            <?=$selectStatus?>
            <?=$inputFinger?>
		<div class="form-group">
        <label class="control-label col-sm-3 form-label" for="txtPemeriksaan">Tanda Tangan Pengguna Layanan</label>
        <div class="col-sm-9">
        <a onclick="checkHandSign()" href="<?=$linkTandaTanganPasien?>" class="btn btn-info btn-flat">Tanda Tangan</a>
        <?php 
            if(!empty($txtTandaTangan)) : 
        ?>
            <div id="previewDiv" class="text-center">
						<img src="<?=$txtTandaTangan?>">
						</div>
        <?php
            else : 
        ?>
            <div id="previewDiv"></div>
        <?php
            endif;
        ?>
            
        </div>
    </div>
         </div>
			<div class="col-sm-12 text-center">
			
			</div>
		  <div class="col-sm-12 text-center">
			<?php
			if($mode_form=='loket') { 
			?>
			<button id="savePasien" class="btn btn-success btn-lg"><i class="fa fa-save"></i> Simpan</button>
			<a href="<?=$link_loket?>" id="backPasien" class="btn btn-danger btn-lg"><i class="fa fa-reply"></i> Kembali</a>
			<a href="javascript:void(0)" id="moveKunjunganPasien" onclick="moveToKunjungan()" style="display: none;" class="btn btn-primary btn-lg"><i class="fa fa-sign-in"></i> Daftarkan Kunjungan</a>
			<?php
			}else if($mode_form=='insert') { 
			?>
			<button id="savePasien" class="btn btn-success btn-lg"><i class="fa fa-save"></i> Simpan</button>
			<button id="editPasien" type="button" onclick="enableForm()" class="btn btn-warning btn-lg" style="display: none;"><i class="fa fa-edit"></i> Edit</button>
			<a href="<?=$link_pasien?>" id="backPasien" class="btn btn-danger btn-lg"><i class="fa fa-reply"></i> Kembali</a>
			<a href="<?=$link_input_rekam_medis?>" id="moveKunjunganPasien" style="display: none;" class="btn btn-success btn-lg"><i class="fa fa-file-text"></i> Data Rekam Medis</a>
			<?php
			}else if($mode_form=='detail'){
			?>
            <a href="<?=$link_ke_pendaftaran?>" id="moveKunjunganPasienDetil" onclick="moveToKunjungan()" class="btn btn-primary btn-lg"><i class="fa fa-sign-in"></i> Daftarkan Kunjungan</a>
			<button id="editPasien" type="button" onclick="enableForm()" class="btn btn-warning btn-lg"><i class="fa fa-edit"></i> Ubah</button>
			<button id="savePasien" class="btn btn-success btn-lg" style="display: none;"><i class="fa fa-save"></i> Simpan</button>
			<button id="cancelEditPasien" onclick="disableForm()" class="btn btn-warning btn-lg" type="button" style="display: none;"><i class="fa fa-ban"></i> Batal</button>
			<button id="deletePasien" onclick="hapusData()" type="button" class="btn btn-danger btn-lg"><i class="fa fa-trash"></i> Hapus</button>
			<a href="<?=$link_pasien?>" id="backPasien" class="btn btn-info btn-lg"><i class="fa fa-reply"></i> Kembali</a>
			<?php
			}else if($mode_form=='ugd'){
			?>
			<button id="savePasien" class="btn btn-success btn-lg"><i class="fa fa-save"></i> Simpan</button>
			<a href="<?=$link_loket?>" id="backPasien" class="btn btn-danger btn-lg"><i class="fa fa-reply"></i> Kembali Ke Pendaftaran</a>
			<a href="javascript:void(0)" id="moveKunjunganPasien" onclick="moveToKunjungan()" style="display: none;" class="btn btn-primary btn-lg"><i class="fa fa-sign-in"></i> Daftarkan Kunjungan</a>
			<?php
			}else if($mode_form=='rawat-inap'){
			?>
			<button id="savePasien" class="btn btn-success btn-lg"><i class="fa fa-save"></i> Simpan</button>
			<a href="<?=$link_loket?>" id="backPasien" class="btn btn-danger btn-lg"><i class="fa fa-reply"></i> Kembali Ke Pendaftaran</a>
			<a href="javascript:void(0)" id="moveKunjunganPasien" onclick="moveToKunjungan()" style="display: none;" class="btn btn-primary btn-lg"><i class="fa fa-sign-in"></i> Daftarkan Kunjungan</a>
			<?php
			}
			?>
		  </div>
		 </form>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
	<!-- /.col -->
</div>
<!-- /.row -->
</section>