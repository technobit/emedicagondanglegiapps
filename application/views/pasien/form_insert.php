<section class="content-header">
  <h1 class="title"><?=$title_form?></h1>
  <?=$breadcrumbs?>
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <!-- Hasil Pencarian Data Pasien -->
	  <div class="alert alert-danger alert-dismissable" id="alert_message" style="display: none;">
		  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
		  <h4><i class="icon fa fa-ban" id="alert_message_icon"></i> Peringatan!</h4>
		  <div id="alert_message_content">Danger alert preview. This alert is dismissable. A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart.</div>
      </div>
      <div class="box box-primary">
        <div class="box-header">
          <h3 class="box-title">Form Tambah Pengguna Layanan</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
		  <form class="form-horizontal" id="form-input-pasien">
          <div class="col-sm-6">
            <?=$inputMode?>
            <?=$inputIdPasien?>
            <?=$inputNoAnggota?>
            <?=$inputNamaPasien?>
            <?=$selectJenisKelamin?>
            <?=$inputNamaKK?>
          </div>
          <div class="col-sm-6" id="detail-form-quick">
            <?=$inputAgama?>
            <?=$inputUsia?>
            <?=$inputPekerjaan?>
            <?=$inputAlamat?>
          </div>
          <div class="col-sm-12 text-center">
			<button id="savePasien" class="btn btn-primary btn-lg">Simpan</button>
			<button id="cancelPasien" class="btn btn-danger btn-lg">Batal</button>
			<button id="editPasien" class="btn btn-info btn-lg" style="display: none;">Edit</button>
			<button id="cancelEditPasien" class="btn btn-danger btn-lg" style="display: none;">Cancel</button>
			<a href="<?=$link_rekam_medis?>" id="linkAkses" class="btn btn-success" style="display: none;">Rekam Medis</a>
		  </div>
		 </form>
        </div>
        <!-- /.box-body -->
      </div>
	  <div class="box box-default collapsed-box">
            <div class="box-header with-border">
              <h3 class="box-title">Data Kelengkapan Pengguna Layanan</h3>
              <div class="box-tools pull-right">
                <button data-widget="collapse" class="btn btn-box-tool" type="button"><i class="fa fa-plus"></i>
                </button>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body" style="display: none;">
              <form class="form-horizontal" id="form-kelengkapan-pasien">
				<div class="col-sm-6">
				<?=$inputNoKK?>
				<div class="form-group">
				  <label class="col-sm-3 control-label" for="inputTanggalLahir">Tanggal Lahir</label>
				  <div class="col-sm-9">
				  <div class="input-group">
					  <div class="input-group-addon">
						<i class="fa fa-calendar"></i>
					  </div>
					  <?=$inputTanggalLahir?>
					</div>
				  </div>
				</div>
				<?=$inputTempatLahir?>  
				</div>
				<div class="col-sm-6" >
				  <div class="form-group">
					<label class="col-sm-3 control-label" for="selectJamkes">Jaminan Kesehatan</label>
					<div class="col-sm-9">
					  <?=$selectJamkes?>
					  <?=$inputNoJamkes?>
					</div>
				  </div>
				  <?=$selectKecamatan?>
				  <?=$selectKelurahan?>
				</div>
				<div class="col-sm-12 text-center">
				  <button id="savePasienFull" class="btn btn-primary btn-lg">Simpan</button>
				  <button id="editPasienFull" class="btn btn-info btn-lg" style="display: none;">Edit</button>
				  <button id="cancelEditPasienFull" class="btn btn-danger btn-lg" style="display: none;">Cancel</button>
				</div>
			   </form>
            </div>
            <!-- /.box-body -->
          </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>