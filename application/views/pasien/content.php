<section class="content-header">
  <h1 class="title"><?=$title?></h1>
  <?=$breadcrumbs?>
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
    <!-- Hasil Pencarian Data Pasien -->
    <div class="box box-success">
    <!-- form start -->
        <form class="form-horizontal" id="frm-poli-pasien">
          <div class="box-body">
            <?=$frm_tanggal?>
            <div class="col-sm-9 col-sm-offset-3" style="padding-left: 8px;">
                <button class="btn btn-primary btn-flat" id="searchPasien" type="button"><i class="fa fa-search"></i> Cari</button>
                <button class="btn btn-warning btn-flat" id="refreshPasien" type="button" ><i class="fa fa-undo"></i> Reset</button>
                <a href="<?=$link_tambah_pasien?>" class="btn btn-success btn-flat pull-right"><i class="fa fa-plus"></i> Tambah Pasien</a>
            </div>
          </div>
        </form>
    </div>
      <div class="box box-primary">
        <!-- /.box-header -->
        <!--<div class="box-header">
          <div class="box-tools">
            <a href="<?=$link_tambah_pasien?>" class="btn btn-success"><i class="fa fa-plus"></i> Tambah Pasien</a>
          </div>
        </div>-->
        <div class="box-body">
          <table class="table table-hover" id="table-data-pasien">
                <thead>
                  <tr>
                    <th>No Anggota</th>
                    <th>Nama Pengguna Layanan</th>
                    <th>Tanggal Lahir</th>
                    <th>Jenis Kelamin</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody id="result-register-pasien">
                  
                </tbody>
              </table>
          
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>