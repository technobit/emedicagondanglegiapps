<div class="row">
    <div class="col-xs-12">
    <!-- Hasil Pencarian Data Pasien -->
	  <div class="alert alert-danger alert-dismissable" id="alert_message" style="display: none;">
		  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
		  <h4><i class="icon fa fa-ban" id="alert_message_icon"></i> Peringatan!</h4>
		  <div id="alert_message_content"></div>
      </div>
      
		
		  </div>
        </div>
        <!-- /.box-header -->
      <div class="box-body">
		  <form class="form-horizontal" id="form-input-pasien">
          <div class="col-sm-6">
						<?=$inputMode?>
						<?=$inputIdPasien?>
						<div class="form-group">
						<label class="col-sm-3 control-label" for="inputTanggalLahir">No Anggota</label>
              <div class="col-sm-9">
          		<?=$inputNoAnggota?>
				<p class="help-block">No Di Generate Oleh System</p>
              </div>
            </div>
            <?=$inputNamaPasien?>
            
            <?=$inputNoIdentitas?>
            <?=$selectJenisKelamin?>
            <?=$inputAgama?>
            <?=$inputTempatLahir?>			
            <div class="form-group">
			<label class="col-sm-3 control-label" for="inputTanggalLahir">Tanggal Lahir</label>
              <div class="col-sm-9">
          		<?=$inputTanggalLahir?>
              </div>
            </div>
			<div class="form-group">
                  <label class="col-sm-3 control-label form-label">No. Handphone</label>
                  <div class="col-sm-9">
                    <div class="input-group">
                    <span class="input-group-addon">+62 / 0</span>
                    <?=$txtNoHandphone?>
                    </div>
                  </div>
                </div>
			    <?=$inputPekerjaan?>
                <?=$txtPekerjaan?>
          </div>
          <div class="col-sm-6">
					<div class="form-group">
              <label class="col-sm-3 control-label" for="selectJamkes">Jaminan Kesehatan</label>
              <div class="col-sm-9">
                <?=$selectJamkes?>
								<div class="input-group input-group-sm">
                <?=$inputNoJamkes?>
                    <span class="input-group-btn">
                      <button id="checkBPJS" type="button" class="btn btn-info btn-flat">Check BPJS</button>
                    </span>
              </div>
              </div>
            </div>
            <?=$inputAlamat?>
            <?=$selectKecamatan?>
            <?=$selectKelurahan?>
            <?=$selectWilayah?>
            <?=$selectStatus?>
           
			<div class="col-sm-12 text-center">
			
			</div>
		  <div class="col-sm-12 text-center">
			<?php
			if($mode_form=='insert') { 
			?>
			<button id="savePasien" class="btn btn-success btn-lg"><i class="fa fa-save"></i> Simpan</button>
			<button id="editPasien" type="button" onclick="enableForm()" class="btn btn-warning btn-lg" style="display: none;"><i class="fa fa-edit"></i> Edit</button>
			<button id="backPasien" onclick="kembali()" class="btn btn-danger btn-lg"><i class="fa fa-reply"></i> Kembali</button>
			<a href="<?=$link_input_rekam_medis?>" id="moveKunjunganPasien" style="display: none;" class="btn btn-success btn-lg"><i class="fa fa-file-text"></i> Data Rekam Medis</a>
			<?php
			}
			?>
		  </div>
		 </form>
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
	<!-- /.col -->
			
				
			
<script>
	var base_url_pasien = global_url+'pasien/';
$( document ).ready(function() {
 $('.select2').select2();
   $('#inputPekerjaan').select2();
  // Handler for .ready() called.
  $('#savePasien').click(function(e){
    e.preventDefault();
    if ($('#form-input-pasien').valid()) {
        //code
        saveDataPasien(1);
    }
  });
  
  $('#inputTanggalLahir').change(function(e){
    var tanggalLahir = $('#inputTanggalLahir').val();
    if (tanggalLahir!='') {
        //code
        var age = moment().diff(tanggalLahir, 'years');
        if (age==0) {
            //code
            var age = moment().diff(tanggalLahir, 'months');
        }
        console.log(age);
    }
    
  });
  
 
  
  $('#form-input-pasien').validate({
    ignore : "",
    rules : {
           inputNamaPasien : {
                  required : true,
           },
           inputTanggalLahir : {
                  required : true,
           },
    },messages : {
      
    }
  });

  //// Check No bpjsapi

  var $signatureDiv = $('#signatureDiv');
        $signatureDiv.jSignature();
  var inputMode = $('input[name=inputMode]').val();
  if(inputMode=="detail"){
        $('#signatureDiv').css('display' , 'none');
        $('#btnClearSign').attr("disabled" , "disabled");
    }
    

    $('#btnClearSign').click(function(){
        if($('#signatureDiv').css('display') == 'none'){
            $('#signatureDiv').css('display' , 'inline');
            $('#btnSaveSign').css('display' , 'inline');
            $('#previewDiv').empty();
            ////$signatureDiv.jSignature('init');
        }
        $signatureDiv.jSignature('reset');
    });

    $('#btnSaveSign').click(function(){
        var txtIdKunjungan = $('input[name=inputIdPasien]').val();
        var datapair = $signatureDiv.jSignature("getData" , "base30");
        ///$('input[name=inputTTD]').val(datapair);
        $.ajax({
            url : global_url + 'rekam_medis/convertToImage/',
            data : "imageString="+datapair+"&idKunjungan="+txtIdKunjungan,
            type : "POST",
            success : function msg(response){
                $('#signatureDiv').css('display' , 'none');
                $('#previewDiv').html('<img src="'+response+'">');
                $('#btnSaveSign').css('display' , 'none');
                $('input[name=txtTtdPasien]').val(response);
            }
        })
    });
});

function pilihPasien(id) {
    getForm(id);
    $('.bootbox').modal('hide');
}
function kembali() {
   tambahims();
    $('.bootbox').modal('hide');
}
function saveDataPasien(mode) {
    //code
    //var mode = $('#inputMode').val();
    if (mode==1) {
        //code
      var dataPost = $('#form-input-pasien').serialize() ;  
    }
    $.ajax({
            url : base_url_pasien+"save-pasien/",
            type : "POST",
            data : dataPost,
            dataType : "html",
            success: function msg(res){
                 var data = jQuery.parseJSON(res);
                 var status = data['status'];
                 var message = data['message'];
                 var mode = data['mode'];
								 var idPasien = data['idPasien'];
                if (status==true) {
									pilihPasien(idPasien);
								}
                
            }
    });
}
function enableForm(){
  $('#form-input-pasien input').removeAttr("disabled");
  $('#form-input-pasien select').removeAttr("disabled");
  $('#form-input-pasien textarea').removeAttr("disabled");
  $('#form-input-pasien button').removeAttr("disabled");
  $('#form-kelengkapan-pasien input').removeAttr("disabled");
  $('#form-kelengkapan-pasien select').removeAttr("disabled");
  $('#form-kelengkapan-pasien textarea').removeAttr("disabled");
  $('#form-kelengkapan-pasien button').removeAttr("disabled");
  $('#btnClearSign').removeAttr("disabled");
  $('#savePasien').css("display" , "inline");
  $('#cancelEditPasien').css("display" , "inline");
  $('#deletePasien').css("display","none");
  $('#backPasien').css("display","none");
  $('#editPasien').css("display" , "none");
  $('#moveKunjunganPasien').css("display" , "none");
  $('#moveKunjunganPasienDetil').css("display" , "none");
}

function disableForm(){
  $('#form-input-pasien input').attr("disabled", "disabled");
  $('#form-input-pasien select').attr("disabled", "disabled");
  $('#form-input-pasien textarea').attr("disabled", "disabled");
  $('#form-kelengkapan-pasien input').attr("disabled", "disabled");
  $('#form-kelengkapan-pasien select').attr("disabled", "disabled");
  $('#form-kelengkapan-pasien textarea').attr("disabled", "disabled");
  $('#btnClearSign').attr("disabled", "disabled");
  $('#savePasien').css("display" , "none");
  $('#cancelPasien').css("display" , "none");
  $('#cancelEditPasien').css("display" , "none");
  $('#editPasien').css("display" , "inline");
  $('#backPasien').css("display","inline");
  $('#moveKunjunganPasien').css("display" , "inline");
  $('#moveKunjunganPasienDetil').css("display" , "inline");
  $('#deletePasien').css("display","inline");
}

function setNoJaminanKesehatan() {
    //code
    if ($('#selectJamkes').val()!='') {
        //code
        if ($('#inputNoJamkes').is(":disabled")) {
            //code
            $('#inputNoJamkes').removeAttr("disabled");
        }else{
            $('#inputNoJamkes').removeAttr("readonly");
        }
        
    }else{
        $('#inputNoJamkes').attr("readonly" , "readonly");
        
    }
}

function hapusData() {
    //code
    var r = confirm("Apakah Anda Akan Menghapus Data Ini??")
    if (r==true) {
        //code
        var id = $('input[name=inputIdPasien]').val();
    $.ajax({
      url : base_url_pasien+"delete-data-pasien/",
      type : "POST",
      data : "id="+id,
      dataType : "html",
      success: function msg(res){
           var data = jQuery.parseJSON(res);
           var status = data['status'];
           var resHtml = data['html'];
           if (status==true) {
            //code
            window.location = base_url_pasien;
           }
      }
    });
    }
    
}

$('.datepicker').datepicker({
    "format": 'yyyy-mm-dd',
    "locale" : 'id',
});

function getKelurahanModal() {
    //code
    $.ajax({
      url : base_url_pasien+"getDataKelurahan/",
      type : "POST",
      data : "idKecamatan="+$('#selectKecamatan').val(),
      dataType : "html",
      success: function msg(res){
           var data = jQuery.parseJSON(res);
        
           var resHtml = data['html'];
           $('#selectKelurahan').html(resHtml);
      }
    });
}
function pekerjaan(){
    var dropPekerjaan =  $('#inputPekerjaan').val();
    if (dropPekerjaan == "0") {
        $('#txtPekerjaan').removeAttr('disabled');
    }else{
        $('#txtPekerjaan').attr('disabled','disabled');
    }
}
</script>
				
 
