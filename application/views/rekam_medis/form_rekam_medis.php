<form class="form-horizontal clearfix" method="POST" action="?" id="frm-detail-rekmed">
    <?=$frm_rekmed_id_detail?>
    <?=$frm_tgl_rekam_medis?>
    <?=$frm_rekmed_pelayanan?>
    <?=$frm_rekmed_keterangan?>
    <?=$frm_rekmed_pemeriksaan?>
    <?=$frm_rekmed_diagnosa?>
    <?=$frm_rekmed_detail_diagnosa?>
    <?=$frm_no_diagosa?>
    <div id="additional-rekmed"><?=$frm_add_diagosa?></div>
    <div class="col-sm-9 col-sm-offset-3">
    <div class="form-group text-right">
        <button <?=$frm_button_add_diagnosa?> class="btn btn-success btn-flat" id="addRekmed" type="button"><i class="fa fa-plus"></i> Add Diagnosa</button>
    </div>    
    </div>
    <?=$frm_no_tindakan?>
    <?=$frm_rekmed_tindakan?>
    <?=$frm_rekmed_detail_tindakan?>
    <div id="additional-tindakan"><?=$frm_add_tindakan?></div>
    <div class="col-sm-9 col-sm-offset-3">
    <div class="form-group text-right">
        <button <?=$frm_button_add_tindakan?> class="btn btn-success btn-flat" id="addTindakan" type="button"><i class="fa fa-plus"></i> Add Tindakan</button>
    </div>    
    </div>
    <?=$frm_rekmed_pengobatan?>
    <div class="col-sm-9 col-sm-offset-3">
      <?php
      if($frm_rekmed_mode=="insert") : 
      ?>
      <button class="btn btn-primary" id="btnSaveRekmed" type="button"><i class="fa fa-save"></i> Simpan</button>
      <a href="<?=$link_rekmed?>" class="btn btn-warning" id="btnCancelRekmed"><i class="fa fa-ban"></i> Batal</a>
      <?php
      else : 
      ?>
      <button class="btn btn-success" id="btnSaveRekmed" type="button" style="display: none;"><i class="fa fa-save"></i> Simpan</button>
      <button class="btn btn-warning" onclick="disableForm()" id="btnCancelEditRekmed" style="display: none;" type="button"><i class="fa fa-save"></i> Batal</button>
      <button class="btn btn-info" onclick="enableForm()" id="btnEditRekmed" type="button"><i class="fa fa-pencil"></i> Edit</button>
      <a href="<?=$link_rekmed?>" class="btn btn-warning" id="btnCancelRekmed"><i class="fa fa-ban"></i> Batal</a>
      <button class="btn btn-danger" id="btnDeleteRekmed" type="button" onclick="hapusRekamMedis()"><i class="fa fa-trash"></i> Hapus</button>
      <?php
      endif;
      ?>
    </div>
</form>
<script>
var base_url_rekmed = global_url+'rekam-medis/';
var base_url_penyakit = global_url+"data-penyakit/";
var number = 1;
var numberTindakan = 1;
$('#add-rekam-medis').click(function(){
    $('#frm-detail-rekmed')[0].reset();
    $('#modal-frm-rekam-medis').modal('show');
});


function formatListTindakan (repo) {
      if (repo.loading) return repo.text;
      var markups = "<div class='select2-result-repository clearfix'>" +
          "<div class='select2-result-repository__meta'>" +
          "<div class='select2-result-repository__title'>" + repo.txtTindakan  + "</div>"+
          "<div class='select2-result-repository__description'>" + repo.txtDeskripsi + "</div>"
          +"</div></div>";
      return markups;
}

function formatListTindakanSelection (repo) {
      return repo.txtTindakan || repo.text;
}

function formatRepo (repo) {
      if (repo.loading) return repo.text;
      var markups = "<div class='select2-result-repository clearfix'>" +
          "<div class='select2-result-repository__meta'>" +
          "<div class='select2-result-repository__title'>" + repo.txtIndonesianName + "</div>"+
          "<div class='select2-result-repository__description'>" + repo.txtCategory + repo.txtSubCategory + "</div>"
          +"</div></div>";
      ///markups = repo.txtIndonesianName;
      return markups;
}
    
function formatRepoSelection (repo) {
      return repo.txtIndonesianName || repo.text;
}

$( document ).ready(function() {
    $('.select-diagnosa').select2({
        ajax: {
        url: base_url_penyakit+"get-list-penyakit/",
        dataType: 'json',
        delay: 250,
        data: function (params) {
            return {
              q: params.term, // search term
              page: params.page
            };
        },
        processResults: function (data, params) {
        params.page = params.page || 1;
        return {
          results: data.items,
          pagination: {
            more: (params.page * 20) < data.total_count
        }
        };
        },
        cache : true,
  },
  escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
  minimumInputLength: 1,
  templateResult: formatRepo, // omitted for brevity, see the source of this page
  templateSelection: formatRepoSelection, // omitted for brevity, see the source of this page
  placeholder: 'Ketikan Nama Penyakit / Kode ICD',
});
    initiateSelectTindakan('select-tindakan');
    $('#btnSaveRekmed').click(function(){
      if ($('#frm-detail-rekmed').valid()) {
          //code
          saveRekmed();
        }
    });
    $('#frm-detail-rekmed').validate({
      ignore : "",
      rules : {
            txtTanggalPemeriksaan : {
                  required : true,  
            },
            txtPemeriksaan : {
                  required : true,
            },
            "txtDetailDiagnosa[]" : {
                  required : true,
            },
            txtPengobatan : {
                  required : true,
            }
      }
    });
   var number = parseInt($('input[name=noDiagnosa]').val());
   
   $('#addRekmed').click(function(){  
    var htmlRes = '<div class="form-group" id="contSelect'+number+'">'+
                    '<label class="col-sm-3 control-label form-label">Diagnosa Penyakit '+(number + 1)+'</label>'+
                    '<div class="col-sm-9"><select name="selectDiagnosa[]" id="selectDiagnosa'+number+'" class="form-control select-diagnosa">'+
                    '</select></div>'+
                    '</div>';
               
        htmlRes += '<div class="form-group" id="contText'+number+'">'+
                    '<label for="txtDetailDiagnosa'+number+'" class="col-sm-3 control-label form-label">Detail Diagnosa</label>'+
                    '<div class="col-sm-9"><textarea name="txtDetailDiagnosa[]" cols="3" rows="2" id="txtDetailDiagnosa'+number+'" class="form-control"></textarea>'+
                    '</div></div>';

        htmlRes += '<div class="col-sm-9 col-sm-offset-3"><div class="form-group text-left" id="contBtn'+number+'"><button class="btn btn-default btn-flat" style="margin-left:10px;" onclick="hapusForm('+number+')">Hapus Diagnosa '+number+'</button></div></div>';
      $('#additional-rekmed').append(htmlRes);
      initiateSelect('selectDiagnosa'+number);
      number++;
    });
   var numberTindakan = parseInt($('input[name=noTindakan]').val());
   $('#addTindakan').click(function(){
      var htmlRes = '<div class="form-group" id="contTindakanSelect'+numberTindakan+'">'+
                    '<label class="col-sm-3 control-label form-label">Tindakan '+(numberTindakan + 1)+'</label>'+
                    '<div class="col-sm-9"><select name="selectTindakan[]" id="select-tindakan'+numberTindakan+'" class="form-control">'+
                    '</select></div>'+
                    '</div>';
               
        htmlRes += '<div class="form-group" id="contTindakanText'+numberTindakan+'">'+
                    '<label for="txtDetailTindakan'+numberTindakan+'" class="col-sm-3 control-label form-label">Detail Tindakan</label>'+
                    '<div class="col-sm-9"><textarea name="txtDetailTindakan[]" cols="3" rows="2" id="txtDetailTindakan'+numberTindakan+'" class="form-control"></textarea>'+
                    '</div></div>';
                    htmlRes += '<div class="col-sm-9 col-sm-offset-3"><div class="form-group text-left" id="contTindakanBtn'+numberTindakan+'"><button class="btn btn-default btn-flat" style="margin-left:10px;" onclick="hapusTindakanForm('+numberTindakan+')">Hapus Tindakan '+numberTindakan+'</button></div></div>';
      $('#additional-tindakan').append(htmlRes);
      initiateSelectTindakan('select-tindakan'+numberTindakan);
      numberTindakan++;
    });
});


function hapusRekamMedis() {
    //code
    var r=confirm("Apakah Anda Akan Menghapus Data?? ");
    if (r==true) {
        //code
        var idRekamMedis =$('input[name=txtIdRekamMedis]').val();
        var idDetailRekamMedis = $('input[name=txtIdRekmedDetail]').val();
        var postData = 'txtIdRekamMedis='+idRekamMedis+"&txtIdDetailRekamMedis="+idDetailRekamMedis;
        $.ajax({
           url : base_url_rekmed+"hapus-rekam-medis/",
           type : "POST",
           data : postData,
           dataType : "html",
           success: function msg(res){
                var data = jQuery.parseJSON(res);
                var status = data['status'];
                if (status==true) {
                 //code
                 window.location = base_url_rekmed+"detail-rekam-medis/"+idRekamMedis;
                }else{
                 alert("Data Gagal Di hapus");
                }
           }
        }); 
    }
}

function enableForm(){
  $('#frm-detail-rekmed input').removeAttr("disabled");
  $('#frm-detail-rekmed select').removeAttr("disabled");
  $('#frm-detail-rekmed textarea').removeAttr("disabled");
  $('#frm-detail-rekmed button').removeAttr("disabled");
  $('#btnSaveRekmed').css("display" , "inline");
  $('#btnCancelEditRekmed').css("display" , "inline");
  $('#btnEditRekmed').css("display" , "none");
  $('#btnCancelRekmed').css("display" , "none");
}

function disableForm(){
  $('#frm-detail-rekmed input').attr("disabled", "disabled");
  $('#frm-detail-rekmed select').attr("disabled", "disabled");
  $('#frm-detail-rekmed textarea').attr("disabled", "disabled");
  $('#btnSaveRekmed').css("display" , "none");
  $('#btnCancelEditRekmed').css("display" , "none");
  $('#btnEditRekmed').css("display" , "inline");
  $('#btnCancelRekmed').css("display" , "inline");
}

$('#txtTanggalPemeriksaan').datepicker({
    "format": 'yyyy-mm-dd',
    "locale" : 'id',
});

function hapusForm(numberSelect) {
    //code
    $('#contSelect'+numberSelect).remove();
    $('#contText'+numberSelect).remove();
    $('#contBtn'+numberSelect).remove();
    number--;
}


function hapusTindakanForm(numberSelect) {
    //code
    $('#contTindakanSelect'+numberSelect).remove();
    $('#contTindakanText'+numberSelect).remove();
    $('#contTindakanBtn'+numberSelect).remove();
    numberTindakan--;
}

function resetForms() {
    //code
    $('#frm-detail-rekmed')[0].reset();
    $('#additional-rekmed').empty();
    $('#additional-tindakan').empty();
    number = 1;
}

function initiateSelect(id) {
    //code
    $('#'+id).select2({
        ajax: {
        url: base_url_penyakit+"get-list-penyakit/",
        dataType: 'json',
        delay: 250,
        data: function (params) {
            return {
              q: params.term, // search term
              page: params.page
            };
        },
        processResults: function (data, params) {
        params.page = params.page || 1;
        return {
          results: data.items,
          pagination: {
            more: (params.page * 20) < data.total_count
        }
        };
        },
        cache : true,
  },
  escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
  minimumInputLength: 1,
  templateResult: formatRepo, // omitted for brevity, see the source of this page
  templateSelection: formatRepoSelection, // omitted for brevity, see the source of this page
  placeholder: 'Ketikan Nama Penyakit / Kode ICD',
});
}

function initiateSelectTindakan(id) {
    //code
    $('#'+id).select2({
        ajax: {
        url: global_url+"data-tindakan/get-list-tindakan/",
        dataType: 'json',
        delay: 250,
        data: function (params) {
            return {
              q: params.term, // search term
              page: params.page,
              idPelayanan: $('input[name=txtIdPelayanan]').val()
            };
        },
        processResults: function (data, params) {
        params.page = params.page || 1;
        return {
          results: data.items,
          pagination: {
            more: (params.page * 20) < data.total_count
        }
        };
        },
        cache : true,
  },
  escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
  minimumInputLength: 1,
  templateResult: formatListTindakan, // omitted for brevity, see the source of this page
  templateSelection: formatListTindakanSelection, // omitted for brevity, see the source of this page
  placeholder: 'Ketikan Tindakan Yang Di Lakukan',
});
}


function saveRekmed() {
    //code
    $.ajax({
       url : base_url_rekmed+"save-rekam-medis/",
       type : "POST",
       data : $('#frm-detail-register').serialize() +"&"+ $('#frm-detail-rekmed').serialize(),
       dataType : "html",
       success: function msg(res){
            var data = jQuery.parseJSON(res);            
            var status = data['status'];
            var message = data['message'];
            messageBox(status , message , 'alert_message');
            if (status==true) {
             //code
            var idRekamMedis = $('input[name=txtIdRekamMedis]').val();
            window.location = base_url_rekmed+"detail-rekam-medis/"+idRekamMedis;
            }
       }
    }); 
}

function getDetailRekamMedis() {
    //code
    $.ajax({
       url : base_url_rekmed+"get-detail-poli-umum/",
       type : "POST",
       data : $('#frm-detail-register').serialize()+"&page="+$('#page-detail-rekmed').val()+"&mode_form=true",
       dataType : "html",
       success: function msg(res){
            var data = jQuery.parseJSON(res);
            var status = data['status'];
            if (status==true) {
             //code
             var page_next = data['page'];
             var message = data['html'];
             $('#page-detail-rekmed').val(page_next);
             $('#accordion-rekmed').append(message);
            }else{
                  
            }
       }
    }); 
}
</script>