    <div class="row">
        <div class="col-md-4">
                <div class="box box-primary">
                        <div class="box-header">
                                <h3 class="box-title" id="form-head">Tambah Baru</h3>
                        </div>
                        <div class="box-body">
                                <form action="?" method="POST" id="frm-add-rekam-medis-ibu">
                                <?=$intIdRekamMedisIbu?>
                                <?=$txtTanggalPemeriksaan?>
                                <?=$txtKehamilan?>
                                <?=$txtNoIndeks?>
                                <?=$txtStatusPemeriksaan?>
                                </form>        
                        </div>
                        <div class="box-footer">
                        <button type="button" id="saveDataKehamilan" class="btn btn-primary pull-right"><i class="fa fa-save"></i> Submit</button>
                    </div>
                </div>
        </div>
        <div class="col-md-8">
                <div class="alert alert-danger alert-dismissable" id="alert_message" style="display: none;">
                  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                  <h4><i class="icon fa fa-ban" id="alert_message_icon"></i> Peringatan!</h4>
                  <div id="alert_message_content"></div>
                </div>
              <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title" id="form-head">Data Kartu Ibu / Riwayat Pemeriksaan</h3>
                </div>
                <div class="box-body">
                <table class="table" id="table-riwayat-kehamilan">
                    <thead>
                        <tr>
                            <th>Tanggal Pemeriksaan</th>
                            <th>Kehamilan Ke</th>
                            <th>No Indeks</th>
                            <th>Status Pemeriksaan</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                </table>        
                </div>
              </div>  
        </div>
    </div>
    <script>

var base_url_rekam_medis_ibu = global_url+"rekam-medis-ibu/";
var base_url_detail_rekmed_ibu = base_url_rekam_medis_ibu+"get-detail-rekmed-ibu/";
var number = 1;
var statusQueue = 2;
var statusDone = 3;
var data_grid_kehamilan ="";

$(function() {
    ///getDataTables(); 
    $('#txtTanggalPemeriksaanIbu').datepicker({
      format : 'yyyy-mm-dd', 
    });
    
    $('#add-rekam-medis').click(function(){
       $('#modal-ibu').modal('show');
    });
    
    $('#saveDataKehamilan').click(function(){
       if($('#frm-add-rekam-medis-ibu').valid()) {
            saveFormKehamilan();
       }
    });
    
    $('#frm-add-rekam-medis-ibu').validate({
        ignore : "",
        rules : {
            txtKehamilanKe : {
                required : true,
            },
            txtNoIndeks : {
                required : true,
            },
        }
    });
    data_grid_kehamilan = $('#table-riwayat-kehamilan').DataTable({
          "paging": true,
          "lengthChange": false,
          "searching": false,
          "ordering": false,
          "info": false,
          "autoWidth": false,
          "iDisplayLength": 20,
          serverSide: true,
          ajax : {
            url : base_url_detail_rekmed_ibu,
            type : "POST",
            data : function(d){
                d.txtIdRekamMedis = $('input[name=txtIdRekamMedis]').val();
                d.txtIdKunjungan = $('input[name=txtIdKunjungan]').val();
                d.jenisPelayanan = 'pelayanan';
            }
          }
    });
});

function saveFormKehamilan() {
    //code
    $.ajax({
       url : base_url_rekam_medis_ibu+"save-rekam-medis/",
       type : "POST",
       data : $('#frm-detail-register').serialize() +"&"+ $('#frm-add-rekam-medis-ibu').serialize(),
       dataType : "html",
       success: function msg(res){
            var data = jQuery.parseJSON(res);
            var status = data['status'];
            var message = data['message'];
            ///messageBox(status , message , 'alert_message');
            alertPopUp(status , message , '');
            if (status==true) {
             //code
             ///getDataTables();
             data_grid_kehamilan.ajax.reload();
            }
       }
    }); 
}

function hapusDataRiwayatKehamilan(intIdKehamilan){
    bootbox.confirm({
        title : "Peringatan",
        message : "Apakah Data akan Di Hapus? Jika Data Di Hapus maka seluruh data pemeriksaan kehamilan akan terhapus",
        callback : function(result){
            if(result==true){
                //code
                $.ajax({
                url : base_url_rekam_medis_ibu+"hapus-rekam-medis/",
                type : "POST",
                data : 'id='+intIdKehamilan,
                dataType : "html",
                success: function msg(res){
                        var data = jQuery.parseJSON(res);
                        var status = data['status'];
                        var message = data['message'];
                        alertPopUp(status , message , '');
                        if (status==true) {
                        //code
                        ///getDataTables();
                            data_grid_kehamilan.ajax.reload();
                        }
                }
                }); 
            }
        }

    });
}
</script>