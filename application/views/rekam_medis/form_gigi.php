 <form class="form-horizontal clearfix" method="POST" action="?" id="frm-detail-rekmed">
                <?=$frm_rekmed_id_detail?>
                <?=$frm_tgl_rekam_medis?>
                <?=$frm_rekmed_pelayanan?>
                <?=$frm_rekmed_keterangan?>
                <?=$frm_rekmed_pemeriksaan?>
                <?=$frm_rekmed_diagnosa?>
                <?=$frm_rekmed_detail_diagnosa?>
                <?=$frm_no_diagosa?>
                <div id="additional-rekmed"><?=$frm_add_diagosa?></div>
                <div class="col-sm-9 col-sm-offset-3">
                <div class="form-group text-right">
                    <button <?=$frm_button_add_diagnosa?> class="btn btn-success btn-flat" id="addRekmed" type="button"><i class="fa fa-plus"></i> Add Diagnosa</button>
                </div>    
                </div>
                <?=$frm_rekmed_tindakan?>
                <?=$frm_rekmed_pengobatan?>
                <div class="col-sm-9 col-sm-offset-3">
                  <?php
                  if($frm_rekmed_mode=="insert") : 
                  ?>
                  <button class="btn btn-primary" id="btnSaveRekmed" type="button"><i class="fa fa-save"></i> Simpan</button>
                  <a href="<?=$link_rekmed?>" class="btn btn-warning" id="btnCancelRekmed"><i class="fa fa-ban"></i> Batal</a>
                  <?php
                  else : 
                  ?>
                  <button class="btn btn-success" id="btnSaveRekmed" type="button" style="display: none;"><i class="fa fa-save"></i> Simpan</button>
                  
                  <button class="btn btn-warning" onclick="disableForm()" id="btnCancelEditRekmed" style="display: none;" type="button"><i class="fa fa-save"></i> Batal</button>
                  <button class="btn btn-info" onclick="enableForm()" id="btnEditRekmed" type="button"><i class="fa fa-pencil"></i> Edit</button>
                  <a href="<?=$link_rekmed?>" class="btn btn-warning" id="btnCancelRekmed"><i class="fa fa-ban"></i> Batal</a>
                  <button class="btn btn-danger" id="btnDeleteRekmed" type="button" onclick="hapusRekamMedis()"><i class="fa fa-trash"></i> Hapus</button>
                  <?php
                  endif;
                  ?>
                </div>
                </form>