<form class="form-horizontal clearfix" method="POST" action="?" id="frm-detail-rekmed">
    <?=$frm_rekmed_id_detail?>
    <?=$frm_no_kunjungan?>
    <?=$frm_tgl_rekam_medis?>
    <?=$frm_tanggal_lahir?>
    <?=$frm_rekmed_pelayanan?>
    <?=$frm_rekmed_anamnesa?>
    <?=$rujukan?>
    <div class="form-group">
        <label class="col-sm-3 control-label form-label" for="txtPemeriksaan">Pemeriksaan Fisik</label>
        <div class="col-sm-3">
            <label class="control-label form-label">Tinggi Badan</label>
            <div class="input-group">
                <?=$frm_rekmed_tinggi_badan?>
                <span class="input-group-addon">cm</span>
            </div>
        </div>
        <div class="col-sm-3">
            <label class="control-label form-label">Berat Badan</label>
            <div class="input-group">
                <?=$frm_rekmed_berat_badan?>
                <span class="input-group-addon">kg</span>
            </div>
        </div>
        <div class="col-sm-3 col-sm-offset-3">
            <label class="control-label form-label">Tekanan Darah - Sistole</label>
            <div class="input-group">
                <?=$frm_rekmed_sistole?>
                <span class="input-group-addon">mmHg</span>
            </div>
        </div>
        <div class="col-sm-3">
            <label class="control-label form-label">Tekanan Darah - Diastole</label>
            <div class="input-group">
                <?=$frm_rekmed_diastole?>
                <span class="input-group-addon">mmHg</span>
            </div>
        </div>
        <div class="col-sm-3 col-sm-offset-3">
            <label class="control-label form-label">Respiratory Rate</label>
            <div class="input-group">
                <?=$frm_rekmed_respiratory_rate?>
                <span class="input-group-addon">per menit</span>
            </div>
        </div>
        <div class="col-sm-3">
            <label class="control-label form-label">Heart Rate</label>
            <div class="input-group">
                <?=$frm_rekmed_heart_rate?>
                <span class="input-group-addon">bpm</span>
            </div>
        </div>
        <div class="col-sm-3 col-sm-offset-3">
            <label class="control-label form-label">Kesadaran</label>
            <?=$frm_rekmed_kesadaran?>
        </div>
    </div>
    <?=$frm_rekmed_pemeriksaan?>
    <?=$frm_add_others?>
    <div class="form-group">
        <label for="selectDiagnosa[]" class="col-sm-3 control-label form-label" id="lblselectDiagnosa[]">Diagnosa Penyakit</label>
        <div class="col-sm-9">
          <?=$frm_rekmed_diagnosa?>  
          <?=$frm_rekmed_bpjs?>  
        </div>
    </div>
    <?=$frm_no_diagosa?>
    <div id="additional-rekmed"><?=$frm_add_diagosa?></div>
    
    <div class="col-sm-9 col-sm-offset-3">
    <div class="form-group text-right">
        <button <?=$frm_button_add_diagnosa?> class="btn btn-success btn-flat" id="addRekmed" type="button"><i class="fa fa-plus"></i> <Tambah></Tambah> Diagnosa</button>
    </div>    
    </div>
    <?=$frm_rencana_tindakan?>
    <?=$frm_no_tindakan?>
    <?=$frm_rekmed_tindakan?>
    <!--?=$frm_rekmed_detail_tindakan?-->
    <div id="additional-tindakan"><?=$frm_add_tindakan?></div>
    <div class="col-sm-9 col-sm-offset-3">
    <div class="form-group text-right">
        <button <?=$frm_button_add_tindakan?> class="btn btn-success btn-flat" id="addTindakan" type="button"><i class="fa fa-plus"></i> Tambah Tindakan</button>
    </div>    
    </div>
    
    <?php if ($pengobatan_lama=='kosong') :?>
    <?=$frm_rekmed_pengobatan_lama?>
    <?php else :?>
     
    <div class="col-sm-3 text-right">
          <label class="control-label form-label">Pengobatan</label>
        </div>
    <div class="col-sm-9 col-sm-offset-3">
       <table id="tblObat" class="table table-bordered table-condensed table-hover">
              <thead>
                <tr>
                  <th>Obat</th>                  
                  <th width="70px">Jumlah</th>
                  <th width="150px">Dosis</th>                
                  <th width="1px" class="text-center">Aksi</th>
                </tr>
              </thead>
              <tbody>
                 <?php if (!empty($dataObat)) : ?>
                
                <?php 
                $rowsObat = 1;
                foreach($dataObat as $obat) { ?>
                    <tr data-row="<?= $rowsObat; ?>">
                      <td>
                        <?= $this->form->select('selectPengobatan[]', selectObat('--Pilih--'), $obat['intIdObat'], 'class="form-control select2" disabled="disabled"') ?>
                      </td>
                      <td>
                        <input type="text" name="txtPengobatan[]" value="<?= $obat['jumlah'] ?>" disabled="disabled" class="form-control text-center input-sm"/>
                      </td>
                                <td>
                        <input type="text" name="txtDosis[]" value="<?= $obat['dosis'] ?>" disabled="disabled" class="form-control text-center input-sm"/>
                      </td>
        
                      <td>
                        <button type="button" disabled="disabled" onclick="hapusObat(<?= $rowsObat ?>)" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button>
                      </td>
                    </tr>
                    <?php $rowsObat++ ?>
                  <?php } ?>
                  
                <?php else :?>
                             
                
                    
                <?php endif; ?>
              </tbody>
              <tfoot>
                <tr>
                  <td colspan="3" class="text-right">
                    <button <?=$frm_button_add_tindakan?> type="button" class="btn btn-success btn-sm btn-flat" onclick="tambahObat()"><i class="fa fa-plus"></i> Tambah Obat</button>
                  </td>
                </tr>
              </tfoot>
            </table>
            </div>
    <?php endif;?>
    
    
    <!--?=$frm_rekmed_pengobatan?-->
    
    <?=$frm_pegawai?>
  
    <?=$frm_rekmed_rujukan?>
    <?=$frm_rekmed_keterangan?>
    
    <div class="col-sm-3 text-center">
    </div>
    <div class="col-sm-9">
       <br><br><h4> <?=$this->config->item("pernyataan_puskesmas")?></h4><br><br>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label form-label" ></label>
  <div class="form-group">
      <?=form_hidden('pernyataan', '0','id="pernyataan"');?>
                            <div class="col-sm-3">
                                <div class="checkbox">
                                <label>
                                    <?=form_radio("persetujuan" , "setuju", $setuju, "id='frm_setuju'".$styleDisabled);?>
                                    Setuju
                                </label>
                                </div>
                            </div>
                            <div class="form-group">
                            <div class="col-sm-3">
                                <div class="checkbox">
                                <label>
                                    <?=form_radio("persetujuan" , "setuju", $tidak_setuju, "id='frm_tidak_setuju'".$styleDisabled);?>
                                    Tidak Setuju
                                </label>
                                </div>
                            </div>
</div>
    
    <?=$frm_apotik_list?>
    <div class="col-sm-9 col-sm-offset-3">
      <?php
      if($frm_rekmed_mode=="insert") : 
      ?>
      <button class="btn btn-primary" id="btnSaveRekmed" type="button"><i class="fa fa-save"></i> Simpan</button>
      <button class="btn btn-info" onclick="enableForm()" style="display: none;" id="btnEditRekmed" type="button"><i class="fa fa-pencil"></i> Edit</button>
      <a class="btn btn-default" href="" target="_blank" style="display: none;" id="btnCetakRekmed"><i class="fa fa-print"></i> Print</a>
      <button class="btn btn-warning" onclick="disableForm()" id="btnCancelEditRekmed" style="display: none;" type="button"><i class="fa fa-save"></i> Batal</button>
       <a href="" class="btn btn-danger" id="btnDownload" style="display: none;"><i class="fa fa-download"></i> Download Surat Pernyataan</a>
      <?php
      else : 
      ?>
       <button class="btn btn-info" onclick="enableForm()"  id="btnEditRekmed" type="button"><i class="fa fa-pencil"></i> Edit</button>
     
      <button class="btn btn-success" id="btnSaveRekmed" type="button" style="display: none;"><i class="fa fa-save"></i> Simpan</button>
      <button class="btn btn-warning" onclick="disableForm()" id="btnCancelEditRekmed" style="display: none;" type="button"><i class="fa fa-save"></i> Batal</button>
      <button class="btn btn-info" onclick="enableForm()" style="display:none;" id="btnEditRekmed" type="button"><i class="fa fa-pencil"></i> Edit</button>
      <a class="btn btn-default" target="_blank" href="<?=$link_cetak_detail?>" id="btnCetakRekmed"><i class="fa fa-print"></i> Print</a>
      <a href="<?=$link_pernyataan?>" target="_blank" class="btn btn-primary" id="btnDownloadi"><i class="fa fa-download"></i> Download Surat Pernyataan</a>
      <a href="<?=$link_rekmed?>" class="btn btn-warning" id="btnCancelRekmed"><i class="fa fa-ban"></i> Batal</a>
      <button class="btn btn-danger" id="btnDeleteRekmed" type="button" onclick="hapusRekamMedis()"><i class="fa fa-trash"></i> Hapus</button>
      <?php
      endif;
      ?>
    </div>
</form>
<script>
var base_url_rekmed = global_url+'rekam-medis/';
var base_url_penyakit = global_url+"data-penyakit/";
var number = 1;
var numberTindakan = 1;
var rowsObat = $('#tblObat tb tr').length;
function checkOdontogram(){
    var selectedOdontogram = $('input[name=selectOdontogram]').val();
    $.ajax({
        url : global_url + 'rekam_medis/getFormOdontogram/'+selectedOdontogram,
        success : function msg(response){
            bootbox.dialog({
                title : "Form Odontogram",
                message : response,
                size: "large"
            });            
        }
    });
}

 $(function(){
 
 var rujukan = $('input[name="rujukan"]').val();
 if(rujukan == ""){
     $('#cont-txtHasilRujukan').css('display','none');
 }  
$('#add-rekam-medis').click(function(){
    $('#frm-detail-rekmed')[0].reset();
    $('#modal-frm-rekam-medis').modal('show');
});
 $('#frm_tidak_setuju').click(function(){
            if($('#frm_tidak_setuju').is(':checked')){
                 $('input[name=pernyataan]').val("0");
            }    
        });
        $('#frm_setuju').click(function(){
            if($('#frm_setuju').is(':checked')){
                 $('input[name=pernyataan]').val("1");
            }    
        });
$('.select2').select2();
});


function formatListTindakan (repo) {
      if (repo.loading) return repo.text;
      var markups = "<div class='select2-result-repository clearfix'>" +
          "<div class='select2-result-repository__meta'>" +
          "<div class='select2-result-repository__title'>" + repo.txtTindakan  + "</div>"+
          "<div class='select2-result-repository__description'>" + repo.txtDeskripsi + "</div>"
          +"</div></div>";
      return markups;
}

function formatListTindakanSelection (repo) {
      return repo.txtTindakan || repo.text;
}



$( document ).ready(function() {
    ///$('#selectOdontogram').select2();
    $('.select-diagnosa').select2({
        ajax: {
        url: base_url_penyakit+"get-list-penyakit/",
        dataType: 'json',
        delay: 250,
        data: function (params) {
            return {
              q: params.term, // search term
              page: params.page
            };
        },
        processResults: function (data, params) {
        params.page = params.page || 1;
        return {
          results: data.items,
          pagination: {
            more: (params.page * 20) < data.total_count
        }
        };
        },
        cache : true,
  },
  escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
  minimumInputLength: 1,
  templateResult: formatRepo, // omitted for brevity, see the source of this page
  templateSelection: formatRepoSelection, // omitted for brevity, see the source of this page
  placeholder: 'Ketikan Nama Penyakit / Kode ICD',
});
    initiateSelectTindakan('select-tindakan');
   
    $('#btnSaveRekmed').click(function(){
      if ($('#frm-detail-rekmed').valid()) {
          //code
          saveRekmed();
        }
    });
    $('#frm-detail-rekmed').validate({
      ignore : "",
      rules : {
            txtTanggalPemeriksaan : {
                  required : true,  
            },
            txtAnamnesa : {
                  required : true,
            },
      }
    });
   var number = parseInt($('input[name=noDiagnosa]').val());
   
   $('#addRekmed').click(function(){  
    var htmlRes = '<div class="form-group" id="contSelect'+number+'">'+
                    '<label class="col-sm-3 control-label form-label">Diagnosa Penyakit '+(number + 1)+'</label>'+
                    '<div class="col-sm-9"><select name="selectDiagnosa[]" onchange="checkDiagnosaBPJS('+(number + 1)+')" id="selectDiagnosa'+(number + 1)+'" class="form-control select-diagnosa"></select>'+
                    '<input type="text" name="diagnosaBPJS[]" class="form-control" readonly="" placeholder="Kode DiagnosaBPJS" id="diagnosaBPJS_'+(number + 1)+'">'+
                    '</div>'+
                    '</div>';
               
    htmlRes += '<div class="col-sm-9 col-sm-offset-3"><div class="form-group text-left" id="contBtn'+number+'"><button class="btn btn-default btn-flat" style="margin-left:10px;" onclick="hapusForm('+number+')">Hapus Diagnosa '+number+'</button></div></div>';
    if(number < 3){
        
      $('#additional-rekmed').append(htmlRes);
      initiateSelect('selectDiagnosa'+(number + 1));
      number++;
    }
    });
   numberTindakan = parseInt($('input[name=noTindakan]').val());
   $('#addTindakan').click(function(){
      var htmlRes = '<div class="form-group" id="contTindakanSelect'+numberTindakan+'">'+
                    '<label class="col-sm-3 control-label form-label">Tindakan '+(numberTindakan + 1)+'</label>'+
                    '<div class="col-sm-9"><select name="selectTindakan[]" id="select-tindakan'+numberTindakan+'" class="form-control">'+
                    '</select></div>'+
                    '</div>';
               
        /*htmlRes += '<div class="form-group" id="contTindakanText'+numberTindakan+'">'+
                    '<label for="txtDetailTindakan'+numberTindakan+'" class="col-sm-3 control-label form-label">Detail Tindakan</label>'+
                    '<div class="col-sm-9"><textarea name="txtDetailTindakan[]" cols="3" rows="2" id="txtDetailTindakan'+numberTindakan+'" class="form-control"></textarea>'+
                    '</div></div>';*/
                    htmlRes += '<div class="col-sm-9 col-sm-offset-3"><div class="form-group text-left" id="contTindakanBtn'+numberTindakan+'"><button class="btn btn-default btn-flat" style="margin-left:10px;" onclick="hapusTindakanForm('+numberTindakan+')">Hapus Tindakan '+(numberTindakan + 1)+'</button></div></div>';
      $('#additional-tindakan').append(htmlRes);
      initiateSelectTindakan('select-tindakan'+numberTindakan);
      numberTindakan++;
      $('input[name=noTindakan]').val(numberTindakan);
    });
     
    var $signatureDiv = $('#signatureDiv');
        $signatureDiv.jSignature();
    var txtIdRekmedDetail = $('input[name=txtIdRekmedDetail]').val();

    if(txtIdRekmedDetail!=""){
        $('#signatureDiv').css('display' , 'none');
        $('#btnClearSign').attr("disabled" , "disabled");
    }
    

    $('#btnClearSign').click(function(){
        if($('#signatureDiv').css('display') == 'none'){
            $('#signatureDiv').css('display' , 'inline');
            $('#btnSaveSign').css('display' , 'inline');
            $('#previewDiv').empty();
            ////$signatureDiv.jSignature('init');
        }
        $signatureDiv.jSignature('reset');
    });

    $('#btnSaveSign').click(function(){
        var txtIdKunjungan = $('input[name=txtIdKunjungan]').val();
        var datapair = $signatureDiv.jSignature("getData" , "base30");
        ///$('input[name=inputTTD]').val(datapair);
        $.ajax({
            url : global_url + 'rekam_medis/convertToImage/',
            data : "imageString="+datapair+"&idKunjungan="+txtIdKunjungan,
            type : "POST",
            success : function msg(response){
                $('#signatureDiv').css('display' , 'none');
                $('#previewDiv').html('<img src="'+response+'">');
                $('#btnSaveSign').css('display' , 'none');
                $('input[name=inputTTD]').val(response);
            }
        })
    });
});

function hapusRekamMedis() {
    //code
    var r=confirm("Apakah Anda Akan Menghapus Data?? ");
    if (r==true) {
        //code
        var idRekamMedis =$('input[name=txtIdRekamMedis]').val();
        var idDetailRekamMedis = $('input[name=txtIdRekmedDetail]').val();
        var postData = 'txtIdRekamMedis='+idRekamMedis+"&txtIdDetailRekamMedis="+idDetailRekamMedis;
        $.ajax({
           url : base_url_rekmed+"hapus-rekam-medis/",
           type : "POST",
           data : postData,
           dataType : "html",
           success: function msg(res){
                var data = jQuery.parseJSON(res);
                var status = data['status'];
                if (status==true) {
                 //code
                 window.location = base_url_rekmed+"detail-rekam-medis/"+idRekamMedis;
                }else{
                 alert("Data Gagal Di hapus");
                }
           }
        }); 
    }
}

function enableForm(){
  $('#frm-detail-rekmed input').removeAttr("disabled");
  $('#frm-detail-rekmed select').removeAttr("disabled");
  $('#frm-detail-rekmed textarea').removeAttr("disabled");
  $('#frm-detail-rekmed button').removeAttr("disabled");
  $('#btnSaveRekmed').css("display" , "inline");
  $('#btnCancelEditRekmed').css("display" , "inline");
  $('#btnEditRekmed').css("display" , "none");
  $('#btnCetakRekmed').css("display" , "none");
  $('#btnCancelRekmed').css("display" , "none");
}

function disableForm(){
    var pernyataan = $('input[name=pernyataan]').val();
  $('#frm-detail-rekmed input').attr("disabled", "disabled");
  $('#frm-detail-rekmed select').attr("disabled", "disabled");
  $('#frm-detail-rekmed textarea').attr("disabled", "disabled");
  $('#frm-detail-rekmed button').attr("disabled", "disabled");
  $('#btnSaveRekmed').css("display" , "none");
  $('#btnCancelEditRekmed').css("display" , "none");
  $('#btnEditRekmed').css("display" , "inline");
  $('#btnCetakRekmed').css("display" , "inline");
  $('#btnCancelRekmed').css("display" , "inline");
  $('#btnEditRekmed').removeAttr("disabled");
  $('#btnCetakRekmed').removeAttr("disabled");
  
}

$('#txtTanggalPemeriksaan').datepicker({
    "format": 'yyyy-mm-dd',
    "locale" : 'id',
});

function hapusForm(numberSelect) {
    //code
    $('#contSelect'+numberSelect).remove();
    ///$('#contText'+numberSelect).remove();
    $('#contBtn'+numberSelect).remove();
    number--;
}


function hapusTindakanForm(numberSelect) {
    //code
    $('#contTindakanSelect'+numberSelect).remove();
    ///$('#contTindakanText'+numberSelect).remove();
    $('#contTindakanBtn'+numberSelect).remove();
    numberTindakan--;
    $('input[name=noTindakan]').val(numberTindakan);
}


function resetForms() {
    //code
    $('#frm-detail-rekmed')[0].reset();
    $('#additional-rekmed').empty();
    $('#additional-tindakan').empty();
    number = 1;
}

function initiateSelect(id) {
    //code
    $('#'+id).select2({
        ajax: {
        url: base_url_penyakit+"get-list-penyakit/",
        dataType: 'json',
        delay: 250,
        data: function (params) {
            return {
              q: params.term, // search term
              page: params.page
            };
        },
        processResults: function (data, params) {
        params.page = params.page || 1;
        return {
          results: data.items,
          pagination: {
            more: (params.page * 20) < data.total_count
        }
        };
        },
        cache : true,
  },
  escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
  minimumInputLength: 1,
  templateResult: formatRepo, // omitted for brevity, see the source of this page
  templateSelection: formatRepoSelection, // omitted for brevity, see the source of this page
  placeholder: 'Ketikan Nama Penyakit / Kode ICD',
});
}

function initiateSelectTindakan(id) {
    //code
    $('#'+id).select2({
        ajax: {
        url: global_url+"data-tindakan/get-list-tindakan/",
        dataType: 'json',
        delay: 250,
        data: function (params) {
            return {
              q: params.term, // search term
              page: params.page,
              idPelayanan: $('input[name=txtIdPelayanan]').val()
            };
        },
        processResults: function (data, params) {
        params.page = params.page || 1;
        return {
          results: data.items,
          pagination: {
            more: (params.page * 20) < data.total_count
        }
        };
        },
        cache : true,
  },
  escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
  minimumInputLength: 1,
  templateResult: formatListTindakan, // omitted for brevity, see the source of this page
  templateSelection: formatListTindakanSelection, // omitted for brevity, see the source of this page
  placeholder: 'Ketikan Tindakan Yang Di Lakukan',
});
}


function saveRekmed() {
      bootbox.confirm({
        size : "small",
        title : "Konfirmasi",
        message : "Rekam medis tidak dapat diubah jika telah disimpan !!!",
        callback : function(response){
            if(response==true){
               $.ajax({
       url : base_url_rekmed+"save-rekam-medis/",
       type : "POST",
       data : $('#frm-detail-register').serialize() +"&"+ $('#frm-detail-rekmed').serialize(),
       dataType : "html",
       success: function msg(res){
            var data = jQuery.parseJSON(res);            
            console.log(data);
            var status = data['status'];
            var message = data['message'];
            var id = data['id_rekmed_detail'];
            messageBox(status , message , 'alert_message');
            if (status==true) {
             //code
            $("#btnCetakRekmed").attr('href', '<?=$link_cetak?>/'+id);
            $("#btnDownload").attr('href', '<?=$link_pernyataan?>/'+id);
            var idRekamMedis = $('input[name=txtIdRekamMedis]').val();
            $('input[name=txtIdRekmedDetail]').val(id);
           $('#btnDownload').css("display" , "inline");
           disableForm()
            }
       }
    });               
            }
            
        }
    });
    
}

function checkDiagnosaBPJS(number){
    var idDiagnosaPenyakit = $('#selectDiagnosa'+number).val();
    if(!idDiagnosaPenyakit){
        alert("Diagnosa Utama Belum Di Pilih");
    }else{
        $.ajax({
            url : global_url + 'bpjs/diagnosa/getDiagnosaUmum/',
            type : 'POST',
            data : 'idDiagnosa='+idDiagnosaPenyakit+"&number="+number,
            success : function msg(response){
                var results = jQuery.parseJSON(response);
                var status = results['status'];
                var message = results['message'];
                var jumlahDiagnosa = results['jumlahDiagnosa'];
                if(status){
                    var dataDiagnosa = results['diagnosa'];
                    if(jumlahDiagnosa==1){
                        var strKodeDiagnosa = dataDiagnosa.kdDiagnosaBPJS+'-'+dataDiagnosa.nmDiagnosaBPJS;
                        $('#diagnosaBPJS_'+number).val(strKodeDiagnosa);
                    }else{
                        bootbox.dialog({
                            title : "List Data Penyakit Yang Sama",
                            message : results['htmlVal']
                        });
                    }
                }else{
                    alertPopUp(status , message , "");
                }
            }
        });
    }
}

function tambahObat() {  
  rowsObat++;
  html = '<tr data-row="'+rowsObat+'">';
    html +='<td>';
      html +='<select id="rowObat'+rowsObat+'" name="selectPengobatan[]" class="form-control select2">';
        <?php foreach(selectObat('--Pilih--') as $key => $row) { ?>
          html +='<option value="<?= $key ?>"><?= $row ?></option>';
        <?php } ?>
      html +='</select>';
    html +='</td>';    
    html +='<td><input type="text" name="txtPengobatan[]" value="1" class="form-control text-center input-sm"/></td>';      
     html +='<td><input type="text" name="txtDosis[]"  class="form-control text-center input-sm"/></td>';      
   
    html +='<td><button type="button" onclick="hapusObat('+rowsObat+')" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button></td>';
  html += '</tr>';
  $('#tblObat tbody').append(html);
  $('#rowObat'+rowsObat).select2();
}
function hapusObat(id) {
  $('#tblObat tbody [data-row="'+id+'"]').remove();
}

</script>