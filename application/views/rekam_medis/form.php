<section class="content-header">
  <h1 class="title"><?=$title?></h1>
  <?=$breadcrumbs?>
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
    <!-- Cari Data Pasien -->
    <div class="box box-success">
      <div class="box-header with-border">
        <h3 class="box-title">Data Pengguna Layanan</h3>
      </div>
        <!-- /.box-header -->
        <!-- form start -->
        
          <div class="box-body">
          <form class="form-horizontal" id="frm-detail-register">
          <div class="box-body">
            <?=$frm_id_rekam_medis?>
            
            <div class="col-xs-12">
            <?=$frm_no_rekam_medis?>
            <?=$frm_nama_pasien?>
            </div>
          </div>
          </form>
          </div>
    </div>
    <!-- Hasil Pencarian Data Pasien -->
      <div class="box box-primary">
        <div class="box-header">
          <h3 class="box-title">Detail Rekam Medis</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
        <form class="form-horizontal clearfix" method="POST" action="?" id="frm-detail-rekmed">
    <?=$frm_id_pasien?>
    <?=$frm_rekmed_id_detail?>
    <?=$frm_no_kunjungan?>
    <?=$frm_id_kunjungan?>
    <?=$link_pernyataan_frm?>
    <?=$link_cetak_frm?>
    <?=$mode_form?>
    <?=$frm_tgl_rekam_medis?>
    <?=$frm_tanggal_lahir?>
    <?=$frm_rekmed_pelayanan?>
    <?=$frm_rekmed_anamnesa?>
    <?=$rujukan?>
    
   
    <div class="form-group">
        <label class="col-sm-3 control-label form-label" for="txtPemeriksaan">Pemeriksaan Fisik</label>
        <div class="col-sm-3">
            <label class="control-label form-label">Tinggi Badan</label>
            <div class="input-group">
                <?=$frm_rekmed_tinggi_badan?>
                <span class="input-group-addon">cm</span>
            </div>
        </div>
        <div class="col-sm-3">
            <label class="control-label form-label">Berat Badan</label>
            <div class="input-group">
                <?=$frm_rekmed_berat_badan?>
                <span class="input-group-addon">kg</span>
            </div>
        </div>
        <div class="col-sm-3 col-sm-offset-3">
            <label class="control-label form-label">Tekanan Darah - Sistole</label>
            <div class="input-group">
                <?=$frm_rekmed_sistole?>
                <span class="input-group-addon">mmHg</span>
            </div>
        </div>
        <div class="col-sm-3">
            <label class="control-label form-label">Tekanan Darah - Diastole</label>
            <div class="input-group">
                <?=$frm_rekmed_diastole?>
                <span class="input-group-addon">mmHg</span>
            </div>
        </div>
        <div class="col-sm-3 col-sm-offset-3">
            <label class="control-label form-label">Respiratory Rate</label>
            <div class="input-group">
                <?=$frm_rekmed_respiratory_rate?>
                <span class="input-group-addon">per menit</span>
            </div>
        </div>
        <div class="col-sm-3">
            <label class="control-label form-label">Heart Rate</label>
            <div class="input-group">
                <?=$frm_rekmed_heart_rate?>
                <span class="input-group-addon">bpm</span>
            </div>
        </div>
        <div class="col-sm-3 col-sm-offset-3">
            <label class="control-label form-label">Kesadaran</label>
            <?=$frm_rekmed_kesadaran?>
        </div>
    </div>
    <?=$frm_rekmed_pemeriksaan?>
    <?=$frm_add_others?>
    <div class="form-group">
        <label for="selectDiagnosa[]" class="col-sm-3 control-label form-label" id="lblselectDiagnosa[]">Diagnosa Penyakit</label>
        <div class="col-sm-9">
          <?=$frm_rekmed_diagnosa?>  
          <?=$frm_rekmed_bpjs?>  
        </div>
    </div>
    <?=$frm_no_diagosa?>
    <div id="additional-rekmed"><?=$frm_add_diagosa?></div>
    
    <div class="col-sm-9 col-sm-offset-3">
    <div class="form-group text-right">
        <button <?=$frm_button_add_diagnosa?> class="btn btn-success btn-flat" id="addRekmed" type="button"><i class="fa fa-plus"></i> <Tambah></Tambah> Diagnosa</button>
    </div>    
    </div>
    <?=$frm_rencana_tindakan?>
    <?=$frm_no_tindakan?>
    <?=$frm_rekmed_tindakan?>
    <!--?=$frm_rekmed_detail_tindakan?-->
    <div id="additional-tindakan"><?=$frm_add_tindakan?></div>
    <div class="col-sm-9 col-sm-offset-3">
    <div class="form-group text-right">
        <button <?=$frm_button_add_tindakan?> class="btn btn-success btn-flat" id="addTindakan" type="button"><i class="fa fa-plus"></i> Tambah Tindakan</button>
    </div>    
    </div>
    
    <?php if ($pengobatan_lama=='kosong') :?>
    <?=$frm_rekmed_pengobatan_lama?>
    <?php else :?>
     
    <div class="col-sm-3 text-right">
          <label class="control-label form-label">Pengobatan</label>
        </div>
    <div class="col-sm-9 col-sm-offset-3">
       <table id="tblObat" class="table table-bordered table-condensed table-hover">
              <thead>
                <tr>
                  <th>Obat</th>                  
                  <th width="70px">Jumlah</th>
                  <th width="150px">Dosis</th>                
                  <th width="1px" class="text-center">Aksi</th>
                </tr>
              </thead>
              <tbody>
                 <?php if (!empty($dataObat)) : ?>
                
                <?php 
                $rowsObat = 1;
                foreach($dataObat as $obat) { ?>
                    <tr data-row="<?= $rowsObat; ?>">
                      <td>
                        <?= $this->form->select('selectPengobatan[]', selectObat('--Pilih--'), $obat['intIdObat'], 'class="form-control select2" disabled="disabled"') ?>
                      </td>
                      <td>
                        <input type="text" name="txtPengobatan[]" value="<?= $obat['jumlah'] ?>" disabled="disabled" class="form-control text-center input-sm"/>
                      </td>
                                <td>
                        <input type="text" name="txtDosis[]" value="<?= $obat['dosis'] ?>" disabled="disabled" class="form-control text-center input-sm"/>
                      </td>
        
                      <td>
                        <button type="button" disabled="disabled" onclick="hapusObat(<?= $rowsObat ?>)" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button>
                      </td>
                    </tr>
                    <?php $rowsObat++ ?>
                  <?php } ?>
                  
                <?php else :?>
                             
                
                    
                <?php endif; ?>
              </tbody>
              <tfoot>
                <tr>
                  <td colspan="3" class="text-right">
                    <button <?=$frm_button_add_tindakan?> type="button" class="btn btn-success btn-sm btn-flat" onclick="tambahObat()"><i class="fa fa-plus"></i> Tambah Obat</button>
                  </td>
                </tr>
              </tfoot>
            </table>
            </div>
    <?php endif;?>
    
    
    <!--?=$frm_rekmed_pengobatan?-->
    
    <?=$frm_pegawai?>
  
    <?=$frm_rekmed_rujukan?>
    <?=$frm_rekmed_keterangan?>
    
    <div class="col-sm-3 text-center">
    </div>
    <div class="col-sm-9">
       <br><br><h4> Pengguna layanan telah diberikan penjelasan oleh pihak puskesmas dan menyetujui segala resiko atau akibat, efek samping dari tindakan yang dimaksud.</h4><br><br>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label form-label" >Persetujuan</label>
  <div class="form-group">
      <?=form_hidden('pernyataan', '0','id="pernyataan"');?>
                            <div class="col-sm-3">
                                <div class="checkbox">
                                <label>
                                    <?=form_radio("persetujuan" , "setuju", $setuju, "id='frm_setuju'".$styleDisabled);?>
                                    Setuju
                                </label>
                                </div>
                            </div>
                            <div class="form-group">
                            <div class="col-sm-3">
                                <div class="checkbox">
                                <label>
                                    <?=form_radio("persetujuan" , "setuju", $tidak_setuju, "id='frm_tidak_setuju'".$styleDisabled);?>
                                    Tidak Setuju
                                </label>
                                </div>
                            </div>
</div>
    <div class="form-group">
        <label class="col-sm-3 control-label form-label" for="txtPemeriksaan">Tanda Tangan</label>
        <div class="col-sm-9">
        <?php 
            if(!empty($txtTandaTangan)) : 
        ?>
            <div id="signatureDiv"></div>
            <div id="previewDiv"><img src="<?=BASE_URL.$txtTandaTangan?>"></div>
            <div class="input-group">
                <?=$frm_ttd?>
                <span class="input-group-btn text-center">
                        <button type="button" class="btn btn-info btn-flat" id="btnClearSign"><i class="fa fa-pencil"></i> Ulang TTD</button>
                        <button style="display:none;" type="button" class="btn btn-danger btn-flat" id="btnSaveSign"><i class="fa fa-pencil"></i> Simpan TTD</button>
                </span>
            </div>
        <?php
            else : 
        ?>
            <div id="signatureDiv"></div>
            <div id="previewDiv"></div>
            <div class="input-group">
                <?=$frm_ttd?>
                <span class="input-group-btn text-center">
                        <button type="button" class="btn btn-info btn-flat" id="btnClearSign"><i class="fa fa-pencil"></i> Ulang TTD</button>
                        <button type="button" class="btn btn-danger btn-flat" id="btnSaveSign"><i class="fa fa-pencil"></i> Simpan TTD</button>
                </span>
            </div>
        <?php
            endif;
        ?>
            
        </div>
    </div>
    <?=$frm_apotik_list?>
    <div class="col-sm-9 col-sm-offset-3">
      <?php if($frm_rekmed_mode=="insert") :?>
      <button class="btn btn-primary" id="btnSaveRekmed" type="button"><i class="fa fa-save"></i> Simpan</button>
      <button class="btn btn-info" onclick="enableForm()" style="display: none;" id="btnEditRekmed" type="button"><i class="fa fa-pencil"></i> Edit</button>
      <a class="btn btn-default" href="" target="_blank" style="display: none;" id="btnCetakRekmed"><i class="fa fa-print"></i> Print</a>
      <button class="btn btn-warning" onclick="disableForm()" id="btnCancelEditRekmed" style="display: none;" type="button"><i class="fa fa-save"></i> Batal</button>
      <a href="" class="btn btn-danger" id="btnDownload" style="display: none;"><i class="fa fa-download"></i> Download Surat Pernataan</a>
      <?php else : ?>
      <button class="btn btn-info" onclick="enableForm()"  id="btnEditRekmed" type="button"><i class="fa fa-pencil"></i> Edit</button>
     <button class="btn btn-success" id="btnSaveRekmed" type="button" style="display: none;"><i class="fa fa-save"></i> Simpan</button>
      <button class="btn btn-warning" onclick="disableForm()" id="btnCancelEditRekmed" style="display: none;" type="button"><i class="fa fa-save"></i> Batal</button>
      <button class="btn btn-info" onclick="enableForm()" style="display:none;" id="btnEditRekmed" type="button"><i class="fa fa-pencil"></i> Edit</button>
      <a class="btn btn-default" target="_blank" href="<?=$link_cetak_detail?>" id="btnCetakRekmed"><i class="fa fa-print"></i> Print</a>
     
     <a href="<?=$link_pernyataan?>" target="_blank" class="btn btn-primary" id="btnDownloadi"><i class="fa fa-download"></i> Download Surat Pernyataan</a>
     <a href="<?=$link_rekmed?>" class="btn btn-warning" id="btnCancelRekmed"><i class="fa fa-ban"></i> Batal</a>
     <button class="btn btn-danger" id="btnDeleteRekmed" type="button" onclick="hapusRekamMedis()"><i class="fa fa-trash"></i> Hapus</button>
      <?php endif;?>
    </div>
</form>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<script>
  var rowsObat = $('#tblObat tb tr').length;
function tambahObat() {  
  rowsObat++;
  html = '<tr data-row="'+rowsObat+'">';
    html +='<td>';
      html +='<select id="rowObat'+rowsObat+'" name="selectPengobatan[]" class="form-control select2">';
        <?php foreach(selectObat('--Pilih--') as $key => $row) { ?>
          html +='<option value="<?= $key ?>"><?= $row ?></option>';
        <?php } ?>
      html +='</select>';
    html +='</td>';    
    html +='<td><input type="text" name="txtPengobatan[]" value="1" class="form-control text-center input-sm"/></td>';      
     html +='<td><input type="text" name="txtDosis[]"  class="form-control text-center input-sm"/></td>';      
   
    html +='<td><button type="button" onclick="hapusObat('+rowsObat+')" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button></td>';
  html += '</tr>';
  $('#tblObat tbody').append(html);
  $('#rowObat'+rowsObat).select2();
}
function hapusObat(id) {
  $('#tblObat tbody [data-row="'+id+'"]').remove();
}

</script>