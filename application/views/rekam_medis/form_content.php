<section class="content-header">
  <h1 class="title"><?=$title?></h1>
  <?=$breadcrumbs?>
</section>
<section class="content">
  <div class="row">
    
    <div class="col-xs-12">
      <div class="alert alert-danger alert-dismissable" id="alert_message" style="display: none;">
		  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
		  <h4><i class="icon fa fa-ban" id="alert_message_icon"></i> Peringatan!</h4>
		  <div id="alert_message_content">Data Berhasil Disimpan</div>
      </div>
    <!-- Cari Data Pasien -->
    <div class="box box-success">
      <div class="box-header with-border">
        <h3 class="box-title">Form Tambah Rekam Medis</h3>
      </div>
        <!-- /.box-header -->
        <!-- form start -->
          <div class="box-body">
          <form class="form-horizontal" id="frm-add-rekam-medis" method="POST">
          <div class="box-body">
            <div class="col-xs-12">
            <?=$frm_mode_rekam_medis?>
            <?=$frm_nama_pasien?>
            <?=$frm_no_rekam_medis?>
            <div class="col-sm-9 col-sm-offset-3">
              <button class="btn btn-success btn-flat" id="saveRekamMedis" type="button"><i class="fa fa-save"></i> Simpan</button>
              <a href="<?=$link_rekmed?>" class="btn btn-warning btn-flat" id="cancelRekamMedis"><i class="fa fa-ban">Batal</i></a>
            </div>
            </div>
          </div>
          </form>
          </div>
    </div>
    <!-- Hasil Pencarian Data Pasien -->
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>