<style>
    .odontogram_box{
        border: 1px solid #000;
        padding : 15px;
        font-size : 15px;
        display : inline;
    }

    .odontogram_box:hover{
        background : blue;
        color : white;
        cursor : pointer;
    }

    .odontogram_box.selected{
        background : blue;
        color : white;
    }

    .odontogram_row{
        margin : 0 auto;
        text-align : center;
        padding : 25px 0px;
    }
    .odontogram_container{
        min-height: 400px;
        padding-top : 30px;
    }
</style>    
<div class="odontogram_container">
<div class="odontogram_row">
    <?php 
        $index_utama = "a_ka";
        for($i=1; $i<=8 ; $i++) :
    ?>
        <div class="text-center odontogram_box" id="<?=$index_utama?>_<?=$i?>" onclick="selectedOdontogram('<?=$index_utama?>_<?=$i?>' , '<?=$status_odontogram[$index_utama]?> <?=$i?>')" onmouseover="odontogram_hover('<?=$status_odontogram[$index_utama]?> <?=$i?>')" data-text="<?=$status_odontogram[$index_utama]?> <?=$i?>">
            2<?=$i?>
        </div>
    <?php
        endfor;
    ?>
    <?php 
        $index_utama = "a_ki";
        for($i=8; $i>=1 ; $i--) :
    ?>
        <div class="text-center odontogram_box" id="<?=$index_utama?>_<?=$i?>" onclick="selectedOdontogram('<?=$index_utama?>_<?=$i?>' , '<?=$status_odontogram[$index_utama]?> <?=$i?>')" onmouseover="odontogram_hover('<?=$status_odontogram[$index_utama]?> <?=$i?>')" data-text="<?=$status_odontogram[$index_utama]?> <?=$i?>">
            1<?=$i?>
        </div>
    <?php
        endfor;
    ?>
</div>


<div class="odontogram_row">
    <?php 
        $index_utama = "a_ka";
        for($i=1; $i<=5 ; $i++) :
    ?>
        <div class="text-center odontogram_box" id="<?=$index_utama?>_<?=$status_odontogram_rome[$i]?>" onclick="selectedOdontogram('<?=$index_utama?>_<?=$status_odontogram_rome[$i]?>' , '<?=$status_odontogram[$index_utama]?> <?=$status_odontogram_rome[$i]?>')" onmouseover="odontogram_hover('<?=$status_odontogram[$index_utama]?> <?=$status_odontogram_rome[$i]?>')" data-text="<?=$status_odontogram[$index_utama]?> <?=$status_odontogram_rome[$i]?>">
            6<?=$i?>
        </div>
    <?php
        endfor;
    ?>
    <?php 
        $index_utama = "a_ki";
        for($i=5; $i>=1 ; $i--) :
    ?>
        <div class="text-center odontogram_box" id="<?=$index_utama?>_<?=$status_odontogram_rome[$i]?>" onclick="selectedOdontogram('<?=$index_utama?>_<?=$status_odontogram_rome[$i]?>' , '<?=$status_odontogram[$index_utama]?> <?=$status_odontogram_rome[$i]?>')" onmouseover="odontogram_hover('<?=$status_odontogram[$index_utama]?> <?=$status_odontogram_rome[$i]?>')">
            5<?=$i?>
        </div>
    <?php
        endfor;
    ?>
</div>

<div class="odontogram_row">
    
    <?php 
        $index_utama = "b_ka";
        for($i=1; $i<=5 ; $i++) :
    ?>
        <div class="text-center odontogram_box" id="<?=$index_utama?>_<?=$status_odontogram_rome[$i]?>" onclick="selectedOdontogram('<?=$index_utama?>_<?=$status_odontogram_rome[$i]?>' , '<?=$status_odontogram[$index_utama]?> <?=$status_odontogram_rome[$i]?>')" onmouseover="odontogram_hover('<?=$status_odontogram[$index_utama]?> <?=$status_odontogram_rome[$i]?>')" data-text="<?=$status_odontogram[$index_utama]?> <?=$status_odontogram_rome[$i]?>">
            7<?=$i?>
        </div>
    <?php
        endfor;
    ?>
    <?php 
        $index_utama = "b_ki";
        for($i=5; $i>=1 ; $i--) :
    ?>
        <div class="text-center odontogram_box" id="<?=$index_utama?>_<?=$status_odontogram_rome[$i]?>" onclick="selectedOdontogram('<?=$index_utama?>_<?=$status_odontogram_rome[$i]?>' , '<?=$status_odontogram[$index_utama]?> <?=$status_odontogram_rome[$i]?>')" onmouseover="odontogram_hover('<?=$status_odontogram[$index_utama]?> <?=$status_odontogram_rome[$i]?>')" data-text="<?=$status_odontogram[$index_utama]?> <?=$status_odontogram_rome[$i]?>">
            8<?=$i?>
        </div>
    <?php
        endfor;
    ?>
</div>

<div class="odontogram_row">
    <?php 
        $index_utama = "b_ka";
        for($i=1; $i<=8 ; $i++) :
    ?>
        <div class="text-center odontogram_box" id="<?=$index_utama?>_<?=$i?>" onclick="selectedOdontogram('<?=$index_utama?>_<?=$i?>' , '<?=$status_odontogram[$index_utama]?> <?=$i?>')" data-text="<?=$status_odontogram[$index_utama]?> <?=$i?>" onmouseover="odontogram_hover('<?=$status_odontogram[$index_utama]?> <?=$i?>')">
            3<?=$i?>
        </div>
    <?php
        endfor;
    ?>
    <?php 
        $index_utama = "b_ki";
        for($i=8; $i>=1 ; $i--) :
    ?>
        <div class="text-center odontogram_box" id="<?=$index_utama?>_<?=$i?>" onclick="selectedOdontogram('<?=$index_utama?>_<?=$i?>' , '<?=$status_odontogram[$index_utama]?> <?=$i?>')" data-text="<?=$status_odontogram[$index_utama]?> <?=$i?>" onmouseover="odontogram_hover('<?=$status_odontogram[$index_utama]?> <?=$i?>')">
            4<?=$i?>
        </div>
    <?php
        endfor;
    ?>
    
</div>
</div>
<div class="text-center clearfix">
    <h5 id="hover-text"></h5>
    <h5>Odontogram Yang Di Pilih : <label for="" id="result-text"></label> </h5>
</div>

<div class="text-center">
    <?=form_hidden("txtSelectedOdontogram" , $selected_odontogram)?>
    <?=form_button('btnSimpan' , 'Simpan' , 'class="btn btn-primary btn-flat" onclick="updateOdontogram()" ')?>
</div>

<script>
    $(function(){
        var selectedValOdontogram = $('input[name=txtSelectedOdontogram]').val();
        if(selectedValOdontogram!=""){
            ///$('.odontogram_box#'+selectedOdontogram).addClass('selected');                
            console.log(selectedValOdontogram);
            selectedOdontogram(selectedValOdontogram , '');
        }
        $('.odontogram_box').mouseout(function(){
            $('#hover-text').empty();    
        });
    });
    function selectedOdontogram(id , text){
        $('.odontogram_box').removeClass('selected');
        $('#result-text').html(text);
        $('input[name=txtSelectedOdontogram]').val(id);
        $('#'+id).addClass('selected');
        var txtResult = $('#'+id).attr('data-text');
        $('#result-text').html(txtResult);
    }

    function odontogram_hover(text){
        $('#hover-text').html(text);
    }

    function updateOdontogram(){
        var result = $('input[name=txtSelectedOdontogram]').val();
        var txtResult = $('#result-text').html();
        console.log(txtResult);
        $('input[name=selectOdontogram]').val(result);
        $('#txtOdontogram').val(txtResult);
        $('.bootbox').modal('hide');
    }
</script>