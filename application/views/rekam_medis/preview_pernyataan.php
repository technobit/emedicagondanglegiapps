<section class="content-header">
    <h1 class="title"><?=$title?></h1>
    <?=$breadcrumbs?>
</section>
<section class="content">
<div class="row">
  <div class="col-md-12 text-right no-print">
    <?=form_button("btnCetak" , '<i class="fa fa-print"></i> Cetak' , 'onclick="cetak()" class="btn btn-default btn-flat"')?>
   
  </div>
</div>
<section class="invoice">
<!-- title row -->

<div class="row">
  <div class="col-xs-12">
 <center>
    <h2 class="page-header">
      <img src="<?=ASSETS_IMAGE_URL?>pkm_bll_black.png" height="50px">
     </h2>
     </center>
  </div>
  <!-- /.col -->
</div>
<div class="text-center">
  <h2>LEMBAR <?=$detailRekamMedis['bitSetuju']==0?' PENOLAKAN':' PERSETUJUAN'?></h2><br><br><br>
</div>
<div class="row ">
        <div class="col-sm-12">
         Yang ber tanda tangan dibawah ini :
         </div>
         </div>
         <div class="row invoice-info">
        <div class="col-sm-3 invoice-col">
         
            <strong>Nama</strong><br> 
            <strong>Umur</strong><br> 
            <strong>Jenis Kelamin</strong><br> 
         
            <strong>Alamat</strong>  <br>
          
        </div>
        <!-- /.col -->
        <!-- /.col -->
        <div class="col-sm-9 invoice-col">
           
         : <?=$detailPasien['txtNamaPasien']?> <br>
         : <?=personAge($detailPasien['dtTanggalLahir'])?> <br>
         : <?=$detailPasien['charJkPasien']=="L" ? "Laki-Laki" : "Perempuan"?><br>
         : Kec. <?=$nama_kecamatan?>, Kel/Desa. <?=$detailPasien['Nama']?> <?=$detailPasien['txtAlamatPasien']?>
          
          </div>
          </div>
          
           <div class="row">
          <div class="col-sm-12">
       Pengguna layanan telah diberikan penjelasan oleh pihak puskesmas dan menyetujui segala resiko atau akibat, efek samping dari tindakan yang dimaksud  <div>
        <!-- /.col -->
      </div>
      <div class="text-center">
  <h2><?=$detailRekamMedis['bitSetuju']==0?'TIDAK SETUJU':'SETUJU'?></h2>
</div>
<div class="row">
        <div class="col-sm-12">
         Untuk dilakukan :
         <div>
        <div class="col-sm-12">
          <address>
        <?php 
        $no = 0;
        foreach ($detailTindakan as $value) :
          $no ++?>
          <?=$no.'. '.$value['txtTindakan'].'<br>'?>
          <?php endforeach; ?>
          </address>
        </div>
        <!-- /.col -->
      </div>
      <div class="row">
         <div class="col-sm-12">
        Demikian surat pernyataan ini Saya buat dalam kondisi sadar dan tanpa ada paksaan dari pihak manapun.
        <br><br><br>
         <div>
        </div>
        
   <div class="row  invoice-info">
 
    <div class="col-sm-4 text-center pull-right invoice-col">
  
   <b>Bululawang, <?=indonesian_date(date('Y-m-d'))?></b><br>
    <center>Pengguna Layanan / Keluarga Pengguna Layanan<br>
    <br>
    </center>
     <?php 
    
     if (!empty($detailRekamMedis['txtTandaTangan'])) :?>
     <br>
    
      <img src="<?=BASE_URL.$detailRekamMedis['txtTandaTangan']?>" width="250" alt="">  
     <?php else : ?>
     
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <?php endif; ?>
    <b><u> <?=strtoupper($detailPasien['txtNamaPasien'])?> </b></u><br>
   
     </div> 
     <div class="col-sm-4 text-center pull-right invoice-col">
        </div>
     <div class="col-sm-4 text-center pull-left invoice-col">
  
     <br>
     <BR>
    <center>Petugas Kesehatan<br>
    <br>
    </center>
    <br>
    <br>
    <br>
    <br>
    <b><u><?=strtoupper($detailRekamMedis["namaPegawai"])?></u></b><br>
    
    
      </div>
            </div>
</section>
</section>