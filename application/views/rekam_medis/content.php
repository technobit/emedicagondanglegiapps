<section class="content-header">
  <h1 class="title"><?=$title?></h1>
  <?=$breadcrumbs?>
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
    <!-- Cari Data Pasien -->
    <div class="box box-success">
      <div class="box-header with-border">
        <h3 class="box-title">Cari Data Rekam Medis</h3>
      </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form class="form-horizontal" id="frm-search-pasien">
          <div class="box-body">
            <div class="form-group">
              <label class="col-sm-3 control-label" for="inputNoId">Nama / No. Anggota/ Nomor Rekam Medis</label>
              <div class="col-sm-9">
                <input type="text" name="id_pasien" id="id_pasien" class="form-control">
              </div>
              <div class="col-sm-9 col-sm-offset-3" style="margin-top: 10px;">
                <button class="btn btn-success btn-flat" id="searchPasien" type="button"><i class="fa fa-search"></i> Cari</button>
                <button class="btn btn-info btn-flat" id="refreshPasien" type="button"><i class="fa fa-repeat"></i> Refresh</button>
              </div>
            </div>
          </div>
        </form>
    </div>
    <!-- Hasil Pencarian Data Pasien -->
      
      <div class="box box-primary">
        <div class="box-header">
          <h3 class="box-title">Data Rekam Medis</h3>
          <div class="box-tools">
            <a href="<?=$link_add_rekam_medis?>" class="btn btn-info btn-flat"><i class="fa fa-plus"></i>  Tambah Rekam Medis</a>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table class="table table-hover" id="table-rekam-medis">
            <thead>
              <tr>
                <th>No Anggota</th>
                <th>No Rekam Medis</th>
                <th>Nama Pengguna Layanan</th>
                <th>Tanggal Lahir</th>
                <th>Jenis Kelamin</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
                
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
