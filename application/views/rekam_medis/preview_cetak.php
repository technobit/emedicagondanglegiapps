<section class="content-header">
    <h1 class="title"><?=$title?></h1>
    <?=$breadcrumbs?>
</section>
<section class="content">
<div class="row"> 
  <div class="col-md-12 text-right no-print">    
    <?=form_button("btnCetak" , '<i class="fa fa-print"></i> Cetak' , 'onclick="cetak()" class="btn btn-default btn-flat"')?>
    <?=anchor($link_cetakForm , '<i class="fa fa-print"></i> Cetak Form' , 'target="_blank" class="btn btn-warning btn-flat"')?>
   
    <?php 
      if($id_detail) : 
    ?>
    <?=anchor($link_cetakRekamMedis , '<i class="fa fa-print"></i> Cetak Semua' , 'class="btn btn-primary btn-flat"')?>
    <?php 
      endif;
    ?>
  </div>
</div>
<section class="invoice">
<!-- title row -->

<div class="row">
  <div class="col-xs-12">
    <h2 class="page-header">
      <img src="<?=ASSETS_IMAGE_URL?>pkm_bll_black.png" height="50px">
      <small class="pull-right">Date : 25/04/2017 03:14:48</small>
    </h2>
  </div>
  <!-- /.col -->
</div>
<div class="text-center">
  <h4>Data Pengguna Layanan</h4>
</div>
<div class="row invoice-info">
        <div class="col-sm-6 invoice-col">
          <address>
            <strong>No Rekam Medis :</strong> <?=$detailPasien['txtNoAnggota']?><br>
            <strong>Nama Pengguna Layanan :</strong> <?=$detailPasien['txtNamaPasien']?> <br>
            <strong>Jenis Kelamin :</strong> <?=$detailPasien['charJkPasien']=="L" ? "Laki-Laki" : "Perempuan"?> <br>
            <strong>Tanggal Lahir :</strong> <?=indonesian_date($detailPasien['dtTanggalLahir']) ." - ".personAge($detailPasien['dtTanggalLahir'])?> <br>
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-2 invoice-col">
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          <b>Pekerjaan : </b><?=$detailPasien['txtPekerjaan']?><br>
          <b>Alamat   :</b> <?=$detailPasien['Nama']?><br>
          <b>Jaminan Kesehatan   :</b> <?=$detailPasien['txtNamaJaminan']?><br>
          <b>No Jaminan Kesehatan   :</b> <?=$detailPasien['txtNoIdJaminanKesehatan']?><br>
        </div>
        <!-- /.col -->
      </div>
<div class="text-center">
  <h4>Rekam Medis</h4>
</div>
<div class="row table-responsive">
    <table class="table">
      <thead>
        <tr>
          <th>Tanggal</th>  
          <th>Ruangan</th>
          <th>Pemeriksaan</th>
          <th>Diagnosis</th>
          <th>Kode ICD-X</th>
          <th>Terapi/Pengobatan</th>
          <th>Petugas</th>
          <th>TTD</th>
        </tr>  
      <thead>
      <tbody>
        <?php 
          foreach($detailRekamMedis as $rowRekamMedis) : 
          
        ?>
        <tr>
          <td><?=$rowRekamMedis['tanggal']?></td>
          <td><?=$rowRekamMedis['nama_poli']?></td>
          <td><?=$rowRekamMedis['tindakan']?></td>
          <td><?=$rowRekamMedis['diagnosa']?></td>
          <td><br><?=$rowRekamMedis['kode_icd']?></td>
          <td><?=$rowRekamMedis['pengobatan']?></td>
          <td><?=$rowRekamMedis['nama_pegawai']?></td>
          <td><?=$rowRekamMedis['tanda_tangan']?></td>
        </tr>
        <?php 
          endforeach;
        ?>
      </tbody>
    </table>
</div>

<!-- /.row -->    
</section>
</section>