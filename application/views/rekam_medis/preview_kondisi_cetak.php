<section class="content-header">
    <h1 class="title"><?=$title?></h1>
    <?=$breadcrumbs?>
</section>
<section class="content">
<div class="row">
  <div class="col-md-12 text-right no-print">
    <?=form_button("btnCetak" , '<i class="fa fa-print"></i> Cetak' , 'onclick="cetak()" class="btn btn-default btn-flat"')?>
   
  </div>
</div>
<section class="invoice">
<!-- title row -->

<div class="row">
  <div class="col-xs-12">
 <center>
    <h2 class="page-header">
      <img src="<?=ASSETS_IMAGE_URL?>pkm_bll_black.png" height="50px">
     </h2>
     </center>
  </div>
  <!-- /.col -->
</div>
<div class="text-center">
  <h4>Data Pengguna Layanan</h4>
</div>
<div class="row invoice-info">
        <div class="col-sm-6 invoice-col">
          <address>
            <strong>No Rekam Medis :</strong> <?=$detailPasien['txtNoAnggota']?><br>
            <strong>Nama Pengguna Layanan :</strong> <?=$detailPasien['txtNamaPasien']?> <br>
            <strong>Jenis Kelamin :</strong> <?=$detailPasien['charJkPasien']=="L" ? "Laki-Laki" : "Perempuan"?> <br>
            <strong>Tanggal Lahir :</strong> <?=indonesian_date($detailPasien['dtTanggalLahir']) ." - ".personAge($detailPasien['dtTanggalLahir'])?> <br>
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-2 invoice-col">
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          <b>Pekerjaan : </b><?=$detailPasien['txtPekerjaan']?><br>
          <b>Alamat   :</b> <?=$detailPasien['Nama']?><br>
          <b>Jaminan Kesehatan   :</b> <?=$detailPasien['txtNamaJaminan']?><br>
          <b>No Jaminan Kesehatan   :</b> <?=$detailPasien['txtNoIdJaminanKesehatan']?><br>
        </div>
        <!-- /.col -->
      </div>
<div class="text-center">
  <h4>Rekam Medis</h4>
</div>
<div class="text-left">
  <h4>Tanda Vital</h4>
</div>
<div class="row invoice-info">
      <div class="col-sm-3 invoice-col">
          <address>
            <strong>Tekanan Darah</strong><br>
            <strong>Frekwensi Nadi</strong><br>
            <strong>Suhu</strong><br>
            <strong>Frekwensi Nafas</strong><br>
            <strong>Skor Nyeri</strong><br>
            <strong>Skor Jatuh</strong><br>
          </address>
        </div>
        <div class="col-sm-3 invoice-col">
          : <?=$detailKondisi['intTekananDarah']?> <br>
          : <?=$detailKondisi['intNadi']?> <br>
          : <?=$detailKondisi['intSuhu']?> <br>
          : <?=$detailKondisi['intNafas']?> <br>
          : <?=$detailKondisi['intSkorNyeri']?> <br>
          : <?=$detailKondisi['intSkorJatuh']?> <br>
        </div>
        
        <!-- /.col -->
</div>
  <div class="text-left">
  <h4>Riwayat Penyakit Dan Alergi</h4>
</div>
<div class="row invoice-info">
      <div class="col-sm-5 invoice-col">
          <address>
            <strong>Riwayat Penyakit Sekarang</strong><br>
            <strong>Riwayat Penyakit Dulu</strong><br>
            <strong>Riwayat Penyakit Dalam</strong><br>
            <strong>Alergi</strong><br>
            
          </address>
        </div>
        <div class="col-sm-7 invoice-col">
           : <?=$detailKondisi['txtRiwayatPenyakitSekarang']?> <br>
          : <?=$detailKondisi['txtRiwayatPenyakitDulu']?> <br>
          : <?=$detailKondisi['txtRiwayatPenyakitDalam']?> <br>
         
        </div>
        <!-- /.col -->
</div>
 <div class="text-left">
  <h4>Antropometri</h4>
</div>
<div class="row invoice-info">
      <div class="col-sm-5 invoice-col">
          <address>
            <strong>Berat Badan</strong><br>
            <strong>Tinggi Badan</strong><br>
            <strong>Lingkar Kepala</strong><br>
            <strong>Lingkar Lengan Atas</strong><br>
            <strong>Fungsional</strong><br>
            
            
          </address>
        </div>
        <div class="col-sm-7 invoice-col">
          : <?=$detailKondisi['intBerat']?> <br>
          : <?=$detailKondisi['intTinggi']?> <br>
          : <?=$detailKondisi['intLingkarKepala']?> <br>
          : <?=$detailKondisi['intLenganAtas']?> <br>
          : <?=$detailKondisi['intFungsional']?> <br>
         
        </div>
        <!-- /.col -->
</div>
<div class="row invoice-info">
      <div class="col-sm-5 invoice-col">
          <address>
            <strong>Status Psikologis</strong><br>
           
            
            
          </address>
        </div>
        <div class="col-sm-7 invoice-col">
          : <?=str_replace("|","<br> : ",$detailKondisi['txtStatusPsikologi'])?> <br><br>
         
        </div>
        <!-- /.col -->
</div>
<div class="row invoice-info">
      <div class="col-sm-5 invoice-col">
          <address>
            <strong>Hambatan Edukasi</strong><br>
           
            
            
          </address>
        </div>
        <div class="col-sm-2 invoice-col">
           : <?=str_replace("|","<br> : ",$detailKondisi['txtHambatanEdukasi'])?> <br><br>
        </div>
        <div class="col-sm-5 invoice-col">
           <?=!empty($detailKondisi['txtKeteranganHambatan1'])?': '.$detailKondisi['txtKeteranganHambatan1'] : ""?> <br>
           <?=!empty($detailKondisi['txtKeteranganHambatan2'])?': '.$detailKondisi['txtKeteranganHambatan2'] : ""?> <br>
        </div>
        <!-- /.col -->
</div>
<div class="row invoice-info">
      <div class="col-sm-5 invoice-col">
          <address>
            <strong>Status Obsetri</strong><br>
              <strong>HPHT</strong><br>
                <strong>TP</strong><br>
                 <strong>Keterangan Obsetri / Ginekologi/ Laktasi / KB</strong><br>
                
                
           
            
            
          </address>
        </div>
        <div class="col-sm-7 invoice-col">
          : <?=$detailKondisi['txtStatusObsetri']?> <br>
          : <?=$detailKondisi['txtHPHT']?> <br>
          : <?=$detailKondisi['txtTP']?> <br>
          : <?=$detailKondisi['txtKeterangan']?> <br>
        </div>
        <!-- /.col -->
</div>
</section>
</section>