<section class="content-header">
    <h1 class="title"><?=$title?></h1>
    <?=$breadcrumbs?>
</section>
<section class="content">
<div class="row">
  <div class="col-md-12 text-right no-print">
    <?=form_button("btnCetak" , '<i class="fa fa-print"></i> Cetak' , 'onclick="cetak()" class="btn btn-default btn-flat"')?>
    <?php
      if($id_detail) :
    ?>
    <?=anchor($link_cetak_kondisi , '<i class="fa fa-print"></i> Cetak Kondisi' , 'class="btn btn-primary btn-flat" target="_blank"')?>
    <?php
      endif;
    ?>
  </div>
</div>
<section class="invoice">
<!-- title row -->

<div class="row">
  <div class="col-xs-12">
    <h2 class="page-header">
      <center>
      <img src="<?=ASSETS_IMAGE_URL?>pkm_bll_black.png" height="50px"><br><br>
       <b><?=strtoupper($pelayanan[$detailRekamMedis['intIdPelayanan']])?></b><br>
      </center>
    </h2>
  </div>
  <!-- /.col -->
</div>
<div class="text-center">
  <h4>Data Pengguna Layanan</h4>
</div>
<div class="row invoice-info">
        <div class="col-sm-6 invoice-col">
          <address>
            <strong>No Rekam Medis :</strong> <?=$detailPasien['txtNoAnggota']?><br>
            <strong>Nama Pengguna Layanan :</strong> <?=$detailPasien['txtNamaPasien']?> <br>
            <strong>Jenis Kelamin :</strong> <?=$detailPasien['charJkPasien']=="L" ? "Laki-Laki" : "Perempuan"?> <br>
            <strong>Tanggal Lahir :</strong> <?=indonesian_date($detailPasien['dtTanggalLahir']) ." - ".personAge($detailPasien['dtTanggalLahir'])?> <br>
          </address>
        </div>
       
        <!-- /.col -->
        <div class="col-sm-2 invoice-col">
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          <b>Pekerjaan : </b><?=$detailPasien['txtPekerjaan']?><br>
          <b>Alamat   :</b> <?=$detailPasien['Nama']?><br>
          <b>Jaminan Kesehatan   :</b> <?=$detailPasien['txtNamaJaminan']?><br>
          <b>No Jaminan Kesehatan   :</b> <?=$detailPasien['txtNoIdJaminanKesehatan']?><br>
         
        </div>
        <!-- /.col -->
      </div>
<div class="text-center">
  <h4>Rekam Medis</h4>
</div>

<div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          <address>
            <strong>Anamnesis </strong><br>
            </address>
        </div>

        <div class="col-sm-8 invoice-col">
         <?=$detailRekamMedis['txtAnamnesa']?><br>
         <br>
        </div>
        </div>
        
        <?php 
       
        if($detailRekamMedis['intIdPelayanan']==3):?>
        <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          <address>
            <strong>Status Odontogram </strong><br>
            </address>
        </div>

        <div class="col-sm-8 invoice-col">
         : <?=$odontogram[$detailRekamMedis['txtOdontogram']]?><br>
         <br>
        </div>
        </div>
        <?php endif;?>
        <!-- /.col -->
        <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          <address>
            <strong>Pemeriksaan Fisik </strong><br>
            </address>
        </div>

        <div class="col-sm-4 invoice-col">
         <address>
          <strong>Tinggi Badan </strong> <?=$detailRekamMedis['txtTinggi']?><br>
            <strong>Tekanan Darah - Sistole </strong> <?=$detailRekamMedis['txtSistole']?> <br>
            <strong>Respiratory Rate </strong> <?=$detailRekamMedis['txtRespiratoryRate']?> <br>
            <strong>Kesadaran </strong> <?=$detailRekamMedis['txtKesadaran']== 0 ? '-':$kesadaran[$detailRekamMedis['txtKesadaran']]?> <br>
           </address>
        </div>
        <div class="col-sm-4 invoice-col">
          <address>
            <strong>Berat Badan </strong> <?=$detailRekamMedis['txtBerat']?><br>
            <strong>Tekanan Darah - Diastole </strong> <?=$detailRekamMedis['txtDiastole']?> <br>
            <strong>Heart Rate </strong> <?=$detailRekamMedis['txtHeartRate']?> <br>
             <strong></strong><br>

          </address>
        </div>
        <!-- /.col -->
        </div>
        <!-- /.col -->
        <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          <address>
            <strong>Pemeriksaan Umum </strong><br>
            </address>
        </div>

        <div class="col-sm-8 invoice-col">
         : <?=$detailRekamMedis['txtPemeriksaan']?><br>
         <br>
        </div>
        <!-- /.col -->
        </div>
        <!-- /.col -->
        
        <?php $no = 0; foreach ($detailDiagnosa as $value): $no++;?>
<div class="row invoice-info">
          
        <div class="col-sm-4 invoice-col">
          <address>
            <strong>Diagnosa Penyakit <?=$no==1?"":$no?></strong><br>
            </address>
        </div>

        <div class="col-sm-8 invoice-col">
         : <?=$value['txtCategory']." - ".$value['txtIndonesianName']?><br>

         <br>
        </div>
        </div>
        <?php endforeach;?>
        
        <!-- /.col -->

        <!-- /.col -->
        
         <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          <address>
            <strong>Rencana Tindakan</strong><br>
            </address>
        </div>

        <div class="col-sm-8 invoice-col">
         : <?=$detailRekamMedis['txtRencanaTindakan']?><br>
         <br>
        </div>
        <!-- /.col -->
        </div>
        <!-- /.col -->
        <?php $no = 0; foreach ($detailTindakan as $key): $no++; ?>
        <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          <address>
            <strong>Tindakan <?=$no==1?"":$no?> </strong><br>
            
            </address>
        </div>

        <div class="col-sm-8 invoice-col">
          <address>
         : <?=$key['txtTindakan']?><br>
       
       </address>

        </div>
        </div>
        <?php endforeach;?>
        
        <!-- /.col -->
        <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          <address>
            <strong>Pengobatan </strong><br>
            </address>
        </div>

        <div class="col-sm-8 invoice-col">
          <?php 
          if ((empty($detailPengobatan)) && (!empty($detailRekamMedis['txtPengobatan']))) :
         ?>
          : <?=$detailRekamMedis['txtPengobatan']?><br>
          <?php else :
          
          
            foreach ($detailPengobatan as  $value) :
          ?>
         <?=$value['nama_obat']?><br>
           <?=!empty($value['jumlah'])?'<b>Jumlah</b> : '.$value['jumlah']:''?><br>
           <?=!empty($value['dosis'])? '<b>Dosis</b>  : '.$value['dosis']:''?> <br>
         
         
         <?php  endforeach; endif;?>
         <br>
        </div>
        </div>
        <?php if (!empty($detailRekamMedis['txtRefIdKunjungan'])) :?>
        <div class="row invoice-info">
         <div class="col-sm-4 invoice-col">
          <address>
            <strong>Hasil Rujukan</strong><br>
            </address>
        </div>

        <div class="col-sm-8 invoice-col">
         : <?=$detailRekamMedis['txtHasilRujukan']?><br>
         <br>
        </div>
        </div>
        <?php endif;?>
        <!-- /.col -->
        <div class="row invoice-info">
         <div class="col-sm-4 invoice-col">
          <address>
            <strong>Keterangan / Tindak lanjut</strong><br>
            </address>
        </div>

        <div class="col-sm-8 invoice-col">
         : <?=$detailRekamMedis['txtKeterangan']?><br>
         <br>
        </div>
        </div>

    <div class="col-sm-12">
    <br><br><h4> <?=$this->config->item("pernyataan_puskesmas")?></h4><br><br>
    </div>
    <div class="row">
 
    <div class="col-sm-4 text-center pull-right invoice-col">
  
   <b>Malang, <?=indonesian_date($detailRekamMedis['dtTanggalKunjungan'])?></b><br>
    <center><b>PETUGAS</b>
    </center>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <b><u><?=strtoupper($detailRekamMedis['namaPegawai'])?> </b></u><br>
    
  
     </div> 
     <div class="col-sm-4 text-center pull-right invoice-col">
        </div>
     <div class="col-sm-4 text-center pull-right invoice-col">
  
       
    <center><br>
    Pengguna Layanan<br>
    </center>
    <br>
    <br>
    <?php if (!empty($detailRekamMedis['txtTandaTangan'])) {
     ?>
    <img src="<?=BASE_URL.$detailRekamMedis['txtTandaTangan']?>" width="250" alt="">
    <?php } else {?>
    <br>
    <br>
    <br>
    <?php }?>
    <br>
    <br>
    <b><u><?=strtoupper($detailPasien['txtNamaPasien'])?> </b></u><br>
       
      </div>
            </div>
          
        <!-- /.col -->
</div>
<!-- /.row -->
</section>
</section>