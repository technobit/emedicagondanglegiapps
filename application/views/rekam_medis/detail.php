<section class="content-header">
  <h1 class="title"><?=$title?></h1>
  <?=$breadcrumbs?>
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
    <!-- Cari Data Pasien -->
    <div class="box box-success">
      <div class="box-header with-border">
        <h3 class="box-title">Data Pengguna Layanan</h3>
      </div>
        <!-- /.box-header -->
        <!-- form start -->
          <div class="box-body">
          <form class="form-horizontal" id="frm-detail-register">
          <div class="box-body">
            <?=$frm_id_rekam_medis?>
            <?=$frm_id_pasien?>
            <div class="col-xs-6">
            <?=$frm_no_rekam_medis?>
            <?=$frm_no_anggota_pasien?>
            <?=$frm_nama_pasien?>
            <?=$frm_jenis_kelamin?>
            </div>
            <div class="col-xs-6">
            <?=$frm_pekerjaan?>
            <?=$frm_agama?>
            <?=$frm_alamat?>
            </div>
          </div>
        </form>
          </div>
    </div>
    <!-- Hasil Pencarian Data Pasien -->
      <?php
        $alert = $this->session->flashdata("alert_rekam_medis");
        if(isset($alert) && !empty($alert)):
        $message = $alert['message'];
        $status = ucwords($alert['status']);
        $class_status = ($alert['status'] == true) ? 'success' : 'danger';
        $icon = ($alert['status'] == true) ? 'check' : 'ban';
        ?>
        <div class="alert alert-<?=$class_status?> alert-dismissable">
          <i class="fa fa-<?=$icon?>"></i>
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          <?=$message?>
       </div>
        <?php
        endif;
      ?>
      <div class="box box-primary">
        <div class="box-header">
          <h3 class="box-title">Detail Rekam Medis</h3>
          <div class="box-tools">
            
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a data-toggle="tab" href="#tab_1">Rekam Medis Umum</a></li>
              <li><a data-toggle="tab" href="#tab_2">Rekam Medis Rawat Inap</a></li>
              <li><a data-toggle="tab" href="#tab_3">Pemeriksaan Laboratorium</a></li>
            </ul>
            <div class="tab-content">
              <div id="tab_1" class="tab-pane active">
              <div class="pull-right">
                <a href="<?=$link_add_rekam_medis_umum?>" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Rekam Medis Rawat Jalan</a>
              </div>
              <table class="table table-hover" id="table-rekam-medis">
              <thead>
                <tr>
                  <th>Tanggal</th>
                  <th>Poli</th>
                  <th>Pemeriksaan</th>
                  <th>Diagnosa</th>
                  <th>Kode ICD 10</th>
                  <th>Terapi / Pengobatan</th>
                  <th>Petugas</th>
                  <th>Tanda Tangan</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                  
              </tbody>
              </table>
              </div>
              <div id="tab_2" class="tab-pane">
                <table class="table table-hover" id="table-rekam-medis-inap">
              <thead>
                <tr>
                  <th>Tanggal Masuk</th>
                  <th>Diagnosa</th>
                  <th>Kamar</th>
                  <th>Tanggal Pulang</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                  
              </tbody>
              </table>
              </div>
              <div id="tab_3" class="tab-pane">
              <table class="table table-hover" id="table-lab">
              <thead>
                <tr>
                  <th>Tanggal Pemeriksaan</th>
                  <th>Jenis Pemeriksaan</th>
                  <th>Detail Pemeriksaan</th>
                </tr>
              </thead>
              <tbody>
                  
              </tbody>
              </table>
              </div>

            </div>
          </div>
          
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>