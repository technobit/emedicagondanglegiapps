<form class="clearfix" method="POST" action="?" id="frm-detail-kondisi-pasien">
<?=$frmIdRekamMedis?>
<?=$frmModeRekamMedis?>
<div class="col-md-12">
<div class="col-sm-6">
    <div class="text-center">
    <label>Tanda Vital</label>
    </div>
    <div class="form-group">
        <label class="control-label form-label">Tekanan Darah</label>
        <div class="input-group">
            <?=form_input("intTekananDarah" , $intTekananDarah , 'class="form-control" id="intTekananDarah"')?>
            <span class="input-group-addon">mmHG</span>
        </div> 
    </div>
    <div class="form-group">
        <label class="control-label form-label">Frekwensi Nadi</label>
        <div class="input-group">
            <?=form_input("intNadi" , $intNadi , 'class="form-control" id="intNadi"')?>
            <span class="input-group-addon">x/menit</span>
        </div> 
    </div>
    <div class="form-group">
        <label class="control-label form-label">Suhu</label>
        <div class="input-group">
            <?=form_input("intSuhu" , $intSuhu , 'class="form-control" id="intSuhu"')?>
            <span class="input-group-addon">C</span>
        </div> 
    </div>
    <div class="form-group">
        <label class="control-label form-label">Frekwensi Nafas</label>
        <div class="input-group">
            <?=form_input("intNafas" , $intNafas , 'class="form-control" id="intNafas"')?>
            <span class="input-group-addon">x/menit</span>
        </div> 
    </div>
    <div class="form-group">
        <label class="control-label form-label">Skor Nyeri</label>
        <?=form_input("intSkorNyeri" , $intSkorNyeri , 'class="form-control" id="intSkorNyeri"')?>
    </div>
    <div class="form-group">
        <label class="control-label form-label">Skor Jatuh</label>
            <?=form_input("intSkorJatuh" , $intSkorJatuh , 'class="form-control" id="intSkorJatuh"')?> 
    </div>
    <div class="text-center">
    <label>Riwayat Penyakit Dan Alergi</label>
    </div>
    <div class="form-group">
        <label class="control-label form-label">Riwayat Penyakit Sekarang</label>
        <?=form_textarea("txtRiwayatPenyakitSekarang" , $txtRiwayatPenyakitSekarang , 'class="form-control" id="txtRiwayatPenyakitSekarang"')?>
    </div>
    <div class="form-group">
        <label class="control-label form-label">Riwayat Penyakit Dulu</label>
        <?=form_textarea("txtRiwayatPenyakitDulu" , $txtRiwayatPenyakitDulu , 'class="form-control" id="txtRiwayatPenyakitDulu"')?>
    </div>
    <div class="form-group">
        <label class="control-label form-label">Riwayat Penyakit Dalam</label>
        <?=form_textarea("txtRiwayatPenyakitDalam" , $txtRiwayatPenyakitDalam , 'class="form-control" id="txtRiwayatPenyakitDalam"')?>
    </div>
    <div class="form-group">
        <label class="control-label form-label">Alergi</label>
        <?=form_textarea("txtAlergi" , $txtAlergi , 'class="form-control" id="txtAlergi"')?>
    </div>
    
</div>

<div class="col-sm-6">
<div class="text-center">
    <label>Antropometri</label>
    </div>
    <div class="form-group">
        <label class="control-label form-label">Berat Badan</label>
        <div class="input-group">
        <?=form_input("intBerat" , $intBerat , 'class="form-control" id="intBerat"')?>
        <span class="input-group-addon">kg</span>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label form-label">Tinggi Badan</label>
        <div class="input-group">
        <?=form_input("intTinggi" , $intTinggi , 'class="form-control" id="intTinggi"')?>
        <span class="input-group-addon">cm</span>
        </div>         
    </div>
    <div class="form-group">
        <label class="control-label form-label">Lingkar Kepala</label>
        <div class="input-group">
        <?=form_input("intLingkarKepala" , $intLingkarKepala , 'class="form-control" id="intLingkarKepala"')?>
        <span class="input-group-addon">cm</span>
        </div>          
    </div>
    <label class="control-label form-label">IMT</label>
    <div class="input-group">
        
        <?=form_input("intIMT" , $intIMT , 'class="form-control" id="intIMT"')?>
        <span class="input-group-btn">
            <button type="button" onclick="hitungIMT()" class="btn btn-info btn-flat">Hitung IMT</button>
        </span> 
    </div>
    <div class="form-group">
        <label class="control-label form-label">Lingkar Lengan Atas</label>
        <div class="input-group">
        <?=form_input("intLenganAtas" , $intLenganAtas , 'class="form-control" id="intLenganAtas"')?>
        <span class="input-group-addon">cm</span>
        </div>          
    </div>
    <div class="form-group">
        <label class="control-label form-label">Fungsional</label>
        <?=form_dropdown("intFungsional" , $arrIntFungsional, $intFungsional , 'class="form-control" id="intFungsional"')?>
    </div>
    <?php 
    $arrPsikologi = explode("|" , $txtStatusPsikologi);
    ?>
    <div class="form-group">
        <label class="control-label form-label">Status Psikologis</label>
        <div class="checkbox">

            <label>
                <?=form_checkbox("txtStatusPsikologi[]" , "Depresi", in_array("Depresi" , $arrPsikologi) , 'id="txtStatusPsikologi1"')?>
                Depresi
            </label>
        </div>
        <div class="checkbox">
            <label>
                <?=form_checkbox("txtStatusPsikologi[]" , "Takut", in_array("Takut" , $arrPsikologi) , 'id="txtStatusPsikologi2"')?>
                Takut
            </label>
        </div>
        <div class="checkbox">
            <label>
                <?=form_checkbox("txtStatusPsikologi[]" , "Agresif", in_array("Agresif" , $arrPsikologi) , 'id="txtStatusPsikologi3"')?>
                Agresif
            </label>
        </div>
        <div class="checkbox">
            <label>
                <?=form_checkbox("txtStatusPsikologi[]" , "Melukai", in_array("Melukai" , $arrPsikologi) , 'id="txtStatusPsikologi4"')?>
                Melukai Diri Sendiri
            </label>
        </div>
    </div>
    <?php 
    $arrHambatanEdukasi = array();
    if(!empty($txtHambatanEdukasi)){
        $arrHambatanEdukasi = explode("|",$txtHambatanEdukasi);
    }
    ?>
    <div class="form-group">
        <label class="control-label form-label">Hambatan Edukasi</label>
        <div class="checkbox">
            <label>
                <?=form_checkbox("txtHambatanEdukasi[]" , "Bahasa", in_array("Bahasa" , $arrHambatanEdukasi) , 'id="txtHambatanEdukasi1"')?>
                Bahasa
            </label>
            <?=form_input("txtKeteranganHambatan[]" , $txtKeteranganHambatan1 , 'id="txtKeteranganHambatan1"')?>
        </div>
        <div class="checkbox">
            <label>
                <?=form_checkbox("txtHambatanEdukasi[]" , "Cacat", in_array("Cacat" , $arrHambatanEdukasi) , 'id="txtHambatanEdukasi1"')?>
                Cacat/ Kognitif/ Fisik
            </label>
            <?=form_input("txtKeteranganHambatan[]" , $txtKeteranganHambatan2 , 'id="txtKeteranganHambatan2"')?>
        </div>
    </div>
    
    
    <div class="form-group">
        <label class="control-label form-label">Status Obsetri</label>
        <?=form_input("txtStatusObsetri" , $txtStatusObsetri , 'class="form-control" id="txtStatusObsetri"')?> 
    </div>
    <div class="form-group">
        <label class="control-label form-label">HPHT</label>
        <?=form_input("txtHPHT" , $txtHPHT , 'class="form-control" id="txtStatusObsetri"')?>
    </div>
    <div class="form-group">
        <label class="control-label form-label">TP</label>
        <?=form_input("txtTP" , $txtTP , 'class="form-control" id="txtTP"')?> 
    </div>
    <div class="form-group">
        <label class="control-label form-label">Keterangan Obsetri / Ginekologi/ Laktasi / KB</label>
        <?=form_input("txtKeterangan" , $txtKeterangan , 'class="form-control" id="txtKeterangan"')?> 
    </div>
</div>
</div>
<div class="col-md-12 text-center">
     <button type="button" id="btnSimpan" class="btn btn-success btn-flat"><i class="fa fa-save"></i> Simpan</button>
     <a class="btn btn-default" href="<?=$link_cetak?>" <?=$mode=="update" ? '' : 'style="display:none;"' ?> target="_blank" id="btnCetakKondisi"><i class="fa fa-print"></i> Print</a>
</div>
</form>
<script>
    $(function(){
        $('#frm-detail-kondisi-pasien').validate({
            ignore : "",
            rules : {
                intTekananDarah : {
                    required : true,
                },
            }
        });

        $('#btnSimpan').click(function(){
            if($('#frm-detail-kondisi-pasien').valid()){
                saveDataKondisi();
            }
        });
    });

    function saveDataKondisi(){
        $.ajax({
            url : global_url+"rekam-medis/save-kondisi-pasien/",
            type : "POST",
            data : $('#frm-detail-kondisi-pasien').serialize(),
            dataType : "html",
            success : function msg(response){
                var data = jQuery.parseJSON(response);
                var status = data['status'];
                var message = data['message'];
                ////var message = data['id'];
                alertPopUp(status , message , "");
                if(status==true){
                    $('input[name=txtMode]').val("update"); 
                    $('#btnCetakKondisi').css("display" , "inline");
                }
            }
        });
    }

    function hitungIMT(){
        var intBerat = $('#intBerat').val();
        var intTinggi = $('#intTinggi').val();
        if((intBerat!="") && (intTinggi!="")){
            
            var IMTValue = intBerat / Math.pow((intTinggi / 100) , 2);
            var kondisiGizi = "";
            console.log(IMTValue);
            if(IMTValue < 17.0){
                kondisiGizi = "Gizi Kurang - Sangat Kurus";
            }else if(IMTValue < 18.5){
                kondisiGizi = "Gizi Kurang - Kurus";
            }else if(IMTValue < 25.0){
                kondisiGizi = "Gizi Baik - Normal";
            }else if(IMTValue < 27.0){
                kondisiGizi = "Gizi Lebih - Gemuk";
            }else{
                kondisiGizi = "Gizi Lebih - Sangat Gemuk";
            }
            $('#intIMT').val(IMTValue.toFixed(2) + ' - ('+kondisiGizi+')');
        }else{
            alert("Mohon Isi Berat Dan Tinggi Badan Terlebih Dahulu");
        }
        
    }
    
</script>