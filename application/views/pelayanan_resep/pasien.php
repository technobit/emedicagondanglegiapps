<?php section('pluginsCSS') ?>
<link href="<?= base_url('assets/plugins/datatables/dataTables.bootstrap.css') ?>" rel="stylesheet" type="text/css" />
<?php endsection() ?>

<?php section('pluginsJS') ?>
<script src="<?= base_url('assets/plugins/datatables/jquery.dataTables.min.js') ?>"></script>
<script src="<?= base_url('assets/plugins/datatables/dataTables.bootstrap.min.js') ?>"></script>
<?php endsection() ?>

<?php section('siteTitle') ?>
    Pelayanan Resep
<?php endsection() ?>

<?php section('breadcrumb') ?>
    <li><a href="#">Pelayanan Resep</a></li>
<?php endsection() ?>

<?php section('content') ?>
<?= $this->form->open(null, 'class="form-horizontal"') ?>
  <div class="row">
    <div class="col-md-8">
      <div class="box box-primary">
        <div class="box-header">
          <h3 class="box-title">Resep</h3>
        </div>
        <div class="box-body">
          <div class="form-group">            
            <div class="col-md-12">
              <?= $this->form->textarea('txtResep', null, 'class="form-control" style="height:150px;" readonly') ?>
            </div>
          </div> 
        </div>
      </div>
      <div class="box box-primary">
        <div class="box-header">
          <h3 class="box-title">Pemberian Obat</h3>
        </div>
        <div class="box-body">
          <div class="form-group">
            <div class="col-md-8">
              <div class="input-group">
                <?= $this->form->text('obat', null, 'placeholder="Nama / Kode Obat" id="txtObat" class="form-control input-lg"') ?>   
                <div class="input-group-btn">           
                  <button type="button" id="btnTambahObat" class="btn btn-success btn-lg btn-flat"><i class="fa fa-plus"></i></button>
                  <button type="button" id="btnCariObat" class="btn btn-primary btn-lg btn-flat"><i class="fa fa-search"></i> Cari Obat</button>
                </div>
              </div>
            </div>
          </div>
          <table id="tblObat" class="table table-bordered table-condensed table-hover">
            <thead>
              <tr>
                <th>Obat</th>
                <th>Kemasan</th>
                <th width="70px">Jumlah</th>
                <th width="125px">Keterangan</th>
                <th width="1px" class="text-center">Aksi</th>
              </tr>
            </thead>
            <tbody>              
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="box box-primary">
        <div class="box-header">
            <h4 class="box-title">Data Pengguna Layanan</h4>
        </div>
        <div class="box-body">          
          <div class="form-group">
            <label class="col-md-4 control-label">No. Anggota</label>
            <div class="col-md-8">
              <?= $this->form->text('txtNoAnggota', null, 'class="form-control" readonly') ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-4 control-label">Nama Pengguna Layanan</label>
            <div class="col-md-8">
              <?= $this->form->text('txtNamaPasien', null, 'class="form-control" readonly') ?>
            </div>
          </div> 
          <div class="form-group">
            <label class="col-md-4 control-label">No. KTP/NIK</label>
            <div class="col-md-8">
              <?= $this->form->text('txtNoKTP', null, 'class="form-control" readonly') ?>
            </div>
          </div> 
          <div class="form-group">
            <label class="col-md-4 control-label">Jenis Kelamin</label>
            <div class="col-md-8">
              <?= $this->form->select('charJkPasien', selectJenisKelamin(), null, 'class="form-control" disabled') ?>
            </div>
          </div> 
        </div>                          
      </div>
      <div class="box box-primary">
        <div class="box-header">
          <h3 class="box-title">Data layanan</h3>
        </div>
        <div class="box-body">
          <div class="form-group">
            <label class="col-md-4 control-label">Dari</label>
            <div class="col-md-8">
              <?= $this->form->text('txtDariLayanan', null, 'class="form-control" readonly') ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-4 control-label">Dokter</label>
            <div class="col-md-8">
              <?= $this->form->text('txtNamaDokter', null, 'class="form-control" readonly') ?>
            </div>
          </div>           
        </div>
      </div>
    </div>    
  </div>    
<?= $this->form->close() ?>
<?php endsection() ?>

<?php section('script') ?>
<script>
var dataObat = new Array;

$(function() {
  getDataObat();

  $('#txtObat').keypress(function(e) {
    if (e.keyCode == 13) {
      $('#btnTambahObat').click();
      return false;
    }
  });

  $('#btnCariObat').click(function() {
    $.ajax({
      url : '<?= base_url('apotik/pelayanan_resep/cari_obat') ?>?key=' + $('#txtObat').val(),
      success : function(response) {
         bootbox.dialog({
          title : 'Cari Obat',
          message : response
        });
      }
    });   
  });
  $('#btnTambahObat').click(function() {
    tambahObat($('#txtObat').val());
  });
});

function getDataObat() {
  var html = '';
  $.each(dataObat, function(key, row) {
    if (row['jumlah']) { var jumlah = row['jumlah']; } else { var jumlah = 1 }
    if (row['aturan_minum']) { var aturan_minum = row['aturan_minum']; } else { var aturan_minum = ''}    
    html += '<tr>';
      html +='<td><input type="hidden" name="obat['+key+'][kode_obat]" value="'+row['kode_obat']+'">'+row['kode_obat']+' - '+row['nama_obat']+'</td>';
      html +='<td>'+row['kemasan']+'</td>';
      html +='<td><input type="text" name="obat['+key+'][jumlah]" value="'+jumlah+'" class="form-control text-center input-sm"/></td>';
      html +='<td><input type="text" name="obat['+key+'][aturan_minum]" value="'+aturan_minum+'" class="form-control input-sm"/></td>';
      html +='<td><button type="button" onclick="hapusObat('+key+')" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button></td>';
    html += '</tr>';
  });
  $('#tblObat tbody').html(html);
}


function tambahObat(obat) {  
  var parse = obat.split('*');
  if (parse.length == 2) {
    var jumlah = parse[0];
    obat = parse[1];
  }  
  $('#txtObat').val('');
  $.ajax({
    url : '<?= base_url('apotik/pelayanan_resep/tambah_obat') ?>',
    type : 'post',
    data : 'obat=' + obat,
    dataType : 'json',
    success : function(response) {
      if (response.length > 1) {
        $('#btnCariObat').click();
      } else {
        if (response[0]) {
          if (jumlah) {
            response[0]['jumlah'] = jumlah;
          }
          dataObat.push(response[0]);
          getDataObat();
        } else {
          alert('Obat tidak ditemukan');
        }
      }
    }
  });
}

function hapusObat(key) {
  dataObat.splice(key, 1);  
  getDataObat();
}
</script>
<?php endsection() ?>

<?php getview('layouts/layout') ?>