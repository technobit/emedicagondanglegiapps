<?php section('pluginsCSS') ?>
<link href="<?= base_url('assets/plugins/datatables/dataTables.bootstrap.css') ?>" rel="stylesheet" type="text/css" />
<?php endsection() ?>

<?php section('pluginsJS') ?>
<script src="<?= base_url('assets/plugins/datatables/jquery.dataTables.min.js') ?>"></script>
<script src="<?= base_url('assets/plugins/datatables/dataTables.bootstrap.min.js') ?>"></script>
<?php endsection() ?>

<?php section('siteTitle') ?>
    Layanan
<?php endsection() ?>

<?php section('breadcrumb') ?>
    <li><a href="#">Layanan</a></li>
<?php endsection() ?>

<?php section('content') ?>
<div class="row">
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Data Antrian Pengguna Layanan</h3>
            </div>
            <div class="box-body">
                <?php getview('layouts/partials/message') ?>    
                <div style="height: 400px; overflow-y: auto">
                    <table class="table table-bordered table-condensed table-hover">
                        <thead>
                            <tr>
                                <th>Nama Pengguna Layanan</th>
                                <th>No. Anggota</th>    
                                <th class="text-center">No. Urut</th>                
                                <th width="1px" class="text-center">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Agus</td>
                                <td>100021</td>
                                <td class="text-center">1</td>
                                <td>
                                    <a href="<?= base_url('apotik/pelayanan_resep/pasien') ?>" class="btn btn-info">Layani</a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Yang Sudah Dilayani</h3>
            </div>
            <div class="box-body">
                <?php getview('layouts/partials/message') ?>                    
                <div style="height: 400px; overflow-y: auto">
                    <table class="table table-bordered table-condensed table-hover">
                        <thead>
                            <tr>
                                <th>Nama Pasien</th>
                                <th>No. Anggota</th>                    
                                <th width="220px" class="text-center">Aksi</th>
                            </tr>
                        </thead>
                    </table>
                </div>                 
            </div>
        </div>
    </div>
</div>
<?php endsection() ?>

<?php getview('layouts/layout') ?>