<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Selamat Datang di eMedica - Sistem Informasi Fasilitas Pelayanan Kesehatan Terpadu</title>
    <link href="<?=ASSETS_URL?>/img/icon.png" rel="shortcut icon" />
    <!-- Bootstrap 3.3.2 -->
    <link href="<?=ASSETS_URL?>bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="<?=ASSETS_URL?>dist/css/AdminLTE.css" rel="stylesheet" type="text/css" />
  </head>
  <body class="login-page">
    <div class="login-box">
      <div class="login-box-body">
          <div class="login-logo">
            <img src="<?=ASSETS_URL?>img/emedica_white.png" height="50px">
			      <h4>Sistem Informasi<br>Fasilitas Pelayanan Kesehatan Terpadu</h4>
            <!--<span> Puskesmas <?=APP_KELURAHAN?></span>-->
          </div><!-- /.login-logo -->
        <p class="login-box-msg" id="msg-box">Masukkan <i>Username/Email</i> dan <i>Password</i> Anda</p>
        <form action="#" method="post">
          <div class="form-group has-feedback">
            <input type="text" class="form-control" placeholder="<?=label_lang('username_login' , $this)?>" name="email_user">
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="password" class="form-control" placeholder="<?=label_lang('password_login' , $this)?>" name="password_user">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="row">
            <div class="col-xs-12">  
              <button type="submit" name="submit" class="btn btn-default btn-block btn-flat">Masuk</button>
              
              <a class="btn btn-primary btn-block btn-flat" href="<?=base_url()."antrian/"?>">Antrian</a>
              
              
              <a class="btn btn-warning btn-block btn-flat" href="<?=base_url()."self-service/"?>">Self Service</a>
              
            </div><!-- /.col -->
          </div>
        </form>
        <div class="row text-right">
        	<span class="col-xs-12" style="margin-top:40px;">Powered by. <a href="http://technobit.id"><img src="<?=ASSETS_URL?>img/technobit_white.png" height="31px" style="margin-top:-20px;"></a></span>
            
        </div>
      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->
    
    <!-- alert -->
    <?php
      if(isset($alert) && !empty($alert)):
        $message = $alert['message'];
        $status = ($alert['status'] == "success") ? 'success' : 'Warning';
        $class_status = ($alert['status'] == "success") ? 'success' : 'danger';
        $icon = ($alert['status'] == "success") ? 'check' : 'ban';
    ?>
    <div class="modal modal-<?php echo $class_status ?> fade" id="myModal" >
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
            <h4 class="modal-title"><span class="icon fa fa-<?php echo $icon ?>"></span> <?php echo $status?></h4>
          </div>
          <div class="modal-body">
            <p><?php echo $message ?></p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-outline" data-dismiss="modal">OK</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->    
    <?php endif; ?>
    <!-- jQuery 2.1.3 -->
    <script src="<?=ASSETS_URL?>plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="<?=ASSETS_URL?>bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?=ASSETS_URL?>custom/js/login.js"></script>
  </body>
</html>