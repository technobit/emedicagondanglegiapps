<!-- Start Page Header -->
<section class="content-header">
      <h1>
        Dashboard
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
</section>
  <!-- End Page Header -->
<section class="content">
 <div class="row">
  <!--<div class="col-md-12">
      <div class="box box-success box-solid">
            <div class="box-header with-border cursor-pointer" data-widget="collapse" >
              <i class="fa fa-minus pull-right"></i>
              <h3 class="box-title">Selamat Datang di<img src="<?=ASSETS_URL?>img/emedica_white.png" height="20px" style="margin: -4px 0 0 5px"></h3>
            </div>
            <div class="box-body text-center">
                <h4>Sistem Informasi Fasilitas Pelayanan Kesehatan Terpadu </h4>
                <img src="<?=ASSETS_URL?>img/puskesmas_bululawang.png">
            </div>
      </div>
  </div>-->
  <div class="col-md-12">
      <div class="box box-success box-solid">
            <div class="box-header with-border cursor-pointer" data-widget="collapse" >
              <i class="fa fa-minus pull-right"></i>
              <h3 class="box-title">Data Kunjungan</h3>
              <div class="box-tools pull-right">
                <button data-widget="collapse" class="btn btn-box-tool" type="button"><i class="fa fa-minus"></i>
                </button>
              </div>
              <!-- /.box-tools -->
            </div>
            <div class="box-body">
              <!--<h2>Selamat Datang di</h2>
              <h1><b>Sistem Informasi Pelayanan Terpadu</b></h1>
              <h3>Puskesmas <?=APP_KELURAHAN?>, Kabupaten <?=APP_KOTA?> <h3>-->
              <div class="row">
                  <div class="col-sm-3 col-xs-6">
                      <!-- small box -->
                      <div class="small-box bg-red">
                        <div class="inner">
                          <h3 id="kunjungan-harian">0</h3>
                          <p>Kunjungan Hari Ini</p>
                        </div>
                        <div class="icon">
                          <i class="fa fa-users"></i>
                        </div>
                        <span class="small-box-footer text-left" id="detil_harian">Tidak ada data kunjungan</span>
                      </div>
                    </div>
                  <div class="col-sm-3 col-xs-6">
                      <!-- small box -->
                      <div class="small-box bg-yellow">
                        <div class="inner">
                          <h3 id="kunjungan-mingguan">0</h3>
                          <p>Kunjungan Minggu Ini</p>
                        </div>
                        <div class="icon">
                          <i class="fa fa-calendar-o"></i>
                        </div>
                        <span class="small-box-footer text-left" id="detil_mingguan">Tidak ada data kunjungan</span>
                      </div>
                    </div>
                  <div class="col-sm-3 col-xs-6">
                      <!-- small box -->
                      <div class="small-box bg-blue">
                        <div class="inner">
                          <h3 id="kunjungan-bulanan">0</h3>
                          <p>Kunjungan Bulan Ini</p>
                        </div>
                        <div class="icon">
                          <i class="fa fa-calendar"></i>
                        </div>
                        <span class="small-box-footer text-left" id="detil_bulanan">Tidak ada data kunjungan</span>
                      </div>
                    </div>
                  <div class="col-sm-3 col-xs-6">
                      <!-- small box -->
                      <div class="small-box bg-green">
                        <div class="inner">
                          <p>Jam Operasional</p>
                          <h3 id="jam-sekarang"></h3>
                        </div>
                        <div class="icon">
                          <i class="fa fa-clock-o"></i>
                        </div>
                        <span class="small-box-footer">
                        <i class="fa fa-clock-o"></i><span id="tanggal-sekarang"></span>
                        </span>
                      </div>
                    </div>
                </div>
                <div class="row chart-responsive">
                    
                </div>
            </div>
            <!-- /.box-body -->
        </div>
    </div>
    
    <!--<div class="col-md-6">
        <div class="box box-primary box-solid">
            <div class="box-header with-border cursor-pointer" data-widget="collapse" >
              <i class="fa fa-minus pull-right"></i>
              <h3 class="box-title">Data Jaminan Kesehatan Bulan Ini</h3>
            </div>
            <div class="box-body text-center">
	            <canvas id="barChartResult" height="75"></canvas>
            </div>
            
        </div>-->
        
	
  <div class="col-md-8">
    
  <div class="col-md-4">
  

  </div>
  </div>
  </div>
</section>