<section class="content-header">
  <h1 class="title"><?=$title?></h1>
  <?=$breadcrumbs?>
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
    <!-- Cari Data Pasien -->
	<div class="box box-primary box-solid">
        <div class="box-header with-border cursor-pointer" data-widget="collapse" >
         
          <h3 class="box-title">Data Filter</h3>
          <div class="box-tools pull-right">
            <button data-widget="collapse" class="btn btn-box-tool" type="button"><i class="fa fa-minus"></i>
            </button>
          </div>
         </div>
    <!-- /.box-header -->
    <!-- form start -->
        <form class="form-horizontal" id="frm-penyakit">
          <div class="box-body">
                <?=$frmPelayanan?>
                <div class="form-group">
                    <label class="col-sm-3 control-label form-label">Tanggal</label>
                    <div class="col-sm-3">
                        <div class="input-group date">
                            <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                            </div>
                            <input value="<?=date("Y-m-").'01'?>" type="text" name="start_date" class="form-control datepicker pull-right" id="datepicker1">
                        </div>
                    </div>
                    <div class="col-sm-1">
                        <label class="control-label form-label">s.d.</label>
                    </div>
                    <div class="col-sm-3">
                        <div class="input-group date">
                            <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                            </div>
                            <input value="<?=date("Y-m-d")?>" type="text" name="end_date" class="form-control datepicker pull-left" id="datepicker2">
                        </div>
                    </div>
                </div>
                <div class="col-sm-offset-3 col-sm-7">
                    <button type="button" id="sendBtn" class="btn btn-success btn-flat"><i class="fa fa-send"></i> Cari</button>
                </div>
          </div>
        </form>
    </div>
	<div class="box box-success box-solid" id="barChart" >
        <div class="box-header with-border cursor-pointer" data-widget="collapse" >
         
          <h3 class="box-title">Grafik Penyakit</h3>
          <div class="box-tools pull-right">
            <button data-widget="collapse" class="btn btn-box-tool" type="button"><i class="fa fa-minus"></i>
            </button>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive">
            <!-- bar chart canvas element -->
            <div class="chart" id="data_penyakit" style="height: 300px;"></div>
            </div>
        
            <table class="table table-hover" id="table-data">
            <thead>
              <tr>
                  <th rowspan  = '2' align="center">No</th>
                  <th rowspan = '2' align="center">Nama Penyakit</th>
                  <th colspan = '3' align="center">Jumlah</th>
              </tr>
              <tr>
                  <th>Jenis Kasus Lama</th>
                  <th>Jenis Kasus Baru</th>
                  <th>Total</th>
              </tr>
            </thead>
            <tbody id="results-data">
              
            </tbody>
          </table>
        
        </div>
        
        
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>