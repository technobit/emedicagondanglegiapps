<section class="content-header">
  <h1 class="title"><?=$title?></h1>
  <?=$breadcrumbs?>
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
    <!-- Cari Data Pasien -->
    <div class="box box-success box-solid">
        <div class="box-header with-border cursor-pointer" data-widget="collapse" >
          <i class="fa fa-minus pull-right"></i>
          <h3 class="box-title">Data Filter</h3>
          <div class="box-tools pull-right">
            <button data-widget="collapse" class="btn btn-box-tool" type="button"><i class="fa fa-minus"></i>
            </button>
          </div>
          <!-- /.box-tools -->
        </div>
    <!-- /.box-header -->
    <!-- form start -->
        <form class="form-horizontal" id="frm-rekap-pengunjung">
          <div class="box-body">
                <?=$frmPelayanan?>
                <div class="form-group">
                    <label class="col-sm-3 control-label form-label">Tanggal</label>
                    <div class="col-sm-3">
                        <div class="input-group date">
                            <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                            </div>
                            <input value="<?=date("Y-m-d")?>" type="text" name="date1" class="form-control datepicker pull-right" id="datepicker1">
                        </div>
                    </div>
                    <div class="col-sm-1">
                        <label class="control-label form-label">s.d.</label>
                    </div>
                    <div class="col-sm-3">
                        <div class="input-group date">
                            <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                            </div>
                            <input value="<?=date("Y-m-d")?>" type="text" name="date2" class="form-control datepicker pull-left" id="datepicker2">
                        </div>
                    </div>
                </div>
                <div class="col-sm-offset-3 col-sm-7">
                    <button type="button" id="sendBtn" class="btn btn-success btn-flat"><i class="fa fa-send"></i> Cari</button>
                </div>
          </div>
        </form>
    </div>
	<div class="box box-primary box-solid">
        <div class="box-header with-border cursor-pointer" data-widget="collapse" >
          <i class="fa fa-minus pull-right"></i>
          <h3 class="box-title">Data Kunjungan</h3>
          <div class="box-tools pull-right">
            <button data-widget="collapse" class="btn btn-box-tool" type="button"><i class="fa fa-minus"></i>
            </button>
          </div>
          <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table class="table table-hover" id="table-rekap-pengunjung">
            <thead>
              <tr>
                <th>No Anggota</th>
                <th>Nama Pengguna Layanan</th>
                <th>Usia Pengguna Layanan</th>
                <th>Jenis Kelamin</th>
                <th>Alamat</th>
                <th>Jenis Pembayaran</th>
                <th>Jenis Kunjungan</th>
                <th>Jenis Wilayah Kunjungan</th>
                <th>Jenis Kasus</th>
                <th>Kode Penyakit - ICD X</th>
                <th>Pengobatan</th>
                <th>Keterangan</th>
                <th>Pelayanan</th>                
              </tr>
            </thead>
            <tbody>
              
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>