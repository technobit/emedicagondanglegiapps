<section class="content-header text-center">
  <h1 class="title"style="font-size:35px;">Layanan Pendaftaran Mandiri</h1>
</section>
<section class="content">
  <div class="row">
      <div class="col-xs-12">
    <!-- Cari Data Pasien -->
      <div class="box box-success">
        <div class="box-header">
          <h1 class="box-title">Silahkan Pilih Loket</h1>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
        <?php 
            foreach($list_pelayanan as $row_poli) : 
        ?>
        <div class="col-sm-12"><a href="#" onclick="daftarkan('<?=$row_poli['intIdPelayanan']?>')">
          <div class="small-box bg-<?=$row_poli['txtColorButton']?>">
              <div class="inner">
                <p>Loket</p>
                <h4 style="font-size: 40px; font-weight: bold; padding:0;"><?=$row_poli['txtNama']?></h4>
              </div>
              <div class="icon">
                <i class="fa fa-desktop"></i>
              </div>
              <div class="small-box-footer"></div>
            </div>
        </a></div>
        <?php 
            endforeach;
        ?>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
      </div>
      
    </div>
  <!-- /.row -->
</section>