<section class="content-header text-center">
<h1 class="title"style="font-size:35px;">Layanan Pendaftaran Mandiri</h1>
</section>
<section class="content">
  <div class="row">
    <div class="col-sm-12">
      <div class="col-sm-12">
          <!-- small box -->
          <div class="small-box bg-green">
              <div class="inner">
                <form id="frm-search-pasien" action="" method="POST">
                  <?=$this->form_builder->inputHidden("uniqueID" , $uniqueID)?>
                  <?=$this->form_builder->inputHidden("uniqueIDPegawai" , $uniqueIDPegawai)?>
                    <div class="box-body text-center" style="margin:-1px 0">
                      <div class="form-group">
                        <label for="inputNoId" style="font-size:18px;">Masukkan No. Anggota / Nama Pengguna Layanan</label>
                          <div class="input-group col-md-12">
                              <?=form_input("txtNoAnggota" , "" , 'id="txtNoAnggota" class="form-control" style="height:50px; font-size:38px;text-align:center;"')?>
                              <span class="input-group-btn">
                              </span>
                          </div>
                      </div>
                      <div class="col-md-12 text-center">
                      <?=anchor(base_url() . 'self-service/pendaftaran-loket/' , '<i class="fa fa-check"></i>  Loket' , 'class="btn btn-primary btn-flat" style="height:50px;font-size:25px"')?>
                      <button class="btn btn-warning btn-flat" id="searchPasien" type="button" style="height:50px;font-size:25px"><i class="fa fa-search"></i> Cari</button>
                      </div>
                    </div>
                  </form>
              </div>
              <div class="small-box-footer"></div>
          </div>
      </div>
    </div>
    <div class="col-sm-12">
    <div class="col-sm-12" id="data-pasien">
      <!-- Cari Data Pasien -->
        <div class="box box-success" id="hidden-table" >
          <!-- /.box-header -->
          <div class="box-body table-responsive">
            <table class="table table-hover">
              <thead>
                <tr>
                  <th style="font-size:19px;text-align:right">Nomor<br>Anggota</th>
                  <th style="font-size:19px;text-align:left">Nama Pengguna Layanan<br></th>
                  <th style="font-size:19px;text-align:left">Tanggal<br>Lahir</th>
                  <th style="font-size:19px;text-align:left">Jenis<br>Kelamin</th>
                  <th style="font-size:19px;text-align:left">Daftarkan<br>Kunjungan</th>
                </tr>
              </thead>
              <tbody id="result-search-pasien">
                
              </tbody>
            </table>
          </div>
          <!-- /.box-body -->
        </div>
        </div>
        <!-- /.box -->
      </div>
    </div>
  </div>

</section>