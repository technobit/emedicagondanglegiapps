<section class="content-header">
  <h1 class="title"style="font-size:35px;">Layanan Pendaftaran Mandiri</h1>
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
        <div class="alert alert-success"> 
                <div class="text-center">
                  <h3><i class="icon fa fa-check"></i>Presensi Berhasil</h3>
                  <br>
                  <p><h3>Terima Kasih , <?=$dataPegawai['txtNamaPegawai']?></h3></p>
                  <br>
                  <a style="text-decoration: none;" class="btn btn-primary btn-flat btn-lg" href="<?=$link_service?>">Selesai</a>
                </div>
              </div>
    </div>  
    </div>
  <!-- /.row -->
</section>
