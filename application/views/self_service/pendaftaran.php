<section class="content-header text-center">
  <h1 class="title"style="font-size:35px;">Layanan Pendaftaran Mandiri</h1>
</section>
<section class="content">
  <div class="row">
  <div class="col-xs-4">
    <!-- Cari Data Pasien -->
      <div class="box box-success">
        <div class="box-header">
          <h3 class="box-title">Data Pengguna Layanan</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <form method="POST" class="form-vertical" action="" id="form-self-registration">
            <?php 
            $this->form_builder->form_type = "vertical";
            ?>
            <?=$this->form_builder->inputHidden("inputIdPasien" , $data_pasien['txtIdPasien']);?>
            <?=$this->form_builder->inputText("Nama" , "inputNamaPasien" , $data_pasien['txtNamaPasien'] , "col-sm-3"  , array("readonly" => "readonly"))?>
            <?=$this->form_builder->inputText("No KTP / NIK" , "inputNoIdentitas" , $data_pasien['txtNoKtp'] , "col-sm-3"  , array("readonly" => "readonly"))?>

            <?php 
            $listJenisKelamin = $this->config->item("jk_list");
		        $jenis_kelamin = $listJenisKelamin[$data_pasien['charJkPasien']];
            $tanggalLahir = $data_pasien['dtTanggalLahir'];
            $tanggal = explode(" " , $tanggalLahir);
            $usia = personAge($tanggal[0]);	
            $usiaHari = getIntervalDays($tanggal[0]);
            $jamkes_list = $this->jaminan_kesehatan->getListDataJaminan();
            $jamkes_list["0"] = "Umum / Tidak Ada";
            $jenisJamkes = $jamkes_list[$data_pasien['intIdJaminanKesehatan']];
            $noJenisJamkes = $data_pasien['txtNoIdJaminanKesehatan'];
            echo $this->form_builder->inputText("Jenis Kelamin" , "inputJenisKelamin" , $jenis_kelamin , "col-sm-3"  , array("readonly" => "readonly"));
            echo $this->form_builder->inputText("Usia" , "inputUsia" , $usia , "col-sm-3"  , array("readonly" => "readonly"));
            echo $this->form_builder->inputHidden("intUsiaPasienHari" , $usiaHari);
            echo $this->form_builder->inputText("Jaminan Kesehatan" , "txtJamkes" , $jenisJamkes , "col-sm-3"  , array("readonly" => "readonly"));
            echo $this->form_builder->inputText("No Jaminan Kesehatan" , "inputNoJamkes" , $noJenisJamkes , "col-sm-3"  , array("readonly" => "readonly"));
            echo $this->form_builder->inputHidden("idJamkes" , $data_pasien['intIdJaminanKesehatan']);
            ?>
          </form>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
      </div>
      <div class="col-xs-8">
    <!-- Cari Data Pasien -->
      <div class="box box-success">
        <div class="box-header">
          <h1 class="box-title">Silahkan Pilih Pelayanan</h1>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
        <?php 
            foreach($arr_poli as $row_poli) : 
        ?>
        <!--<div class="col-sm-6"><a onclick="daftarkan('<?=$data_pasien['txtIdPasien']?>', '<?=$row_poli['intIDPoli']?>')" class="btn btn-lg btn-block <?=$row_poli['class']?> btn-flat" style="height : 120px; padding:49px;">
        <i class="<?=$row_poli['icon']?>"></i> <?=$row_poli['description']?>-->

        <div class="col-sm-6"><a href="#" onclick="daftarkan('<?=$data_pasien['txtIdPasien']?>', '<?=$row_poli['intIDPoli']?>')">
        <!--<i class="<?=$row_poli['icon']?>"></i> <?=$row_poli['description']?>-->
          <div class="small-box bg-<?=$row_poli['color']?>">
              <div class="inner">
                <p>Pendaftaran</p>
                <h4 style="font-size: 40px; font-weight: bold; padding:0;"><?=$row_poli['description']?></h4>
              </div>
              <div class="icon">
                <i class="<?=$row_poli['icon']?>"></i>
              </div>
              <div class="small-box-footer"></div>
            </div>
        </a></div>
        <?php 
            endforeach;
        ?>
        <div class="col-sm-6"><a href="<?=base_url()?>self-service">
          <div class="small-box bg-red">
              <div class="inner">
                <h4 style="font-size: 50px; font-weight: bold; padding:0;">Batalkan</h4>
              </div>
              <div class="icon">
                <i class="fa fa-mail-reply"></i>
              </div>
              <div class="small-box-footer"></div>
            </div>
        </a></div>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
      </div>
      
    </div>
  <!-- /.row -->
</section>