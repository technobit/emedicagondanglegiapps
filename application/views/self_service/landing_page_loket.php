<section class="content-header">
  <h1 class="title"style="font-size:35px;">Layanan Pendaftaran Mandiri</h1>
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
        <div class="alert alert-success"> 
                <div class="text-center">
                  <h3><i class="icon fa fa-check"></i>Pendaftaran Berhasil</h3>
                  <p style="font-size:32px">Anda Telah Terdaftar Di <b><?=$data['txtNama']?></b></p>
                  <p style="font-size:36px">Dengan No Antrian <b style="font-size:50px"><?=$data['intNoAntri']?></b></p>
                  <p style="font-size:25px">Silahkan ambil nomor antrian Anda dan tunggu panggilan Anda di Ruang Tunggu.</p>
                  <br>
                  <p><h3>Terima Kasih</h3></p>
                  <br>
                  <a style="text-decoration: none;" class="btn btn-primary btn-flat btn-lg" href="<?=$link_service?>">Selesai</a>
                </div>
              </div>
    </div>  
    </div>
  <!-- /.row -->
</section>
