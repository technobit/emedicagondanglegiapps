<section class="content-header">
    <h1 class="title"><?=$title?></h1>
    <?=$breadcrumbs?>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Data Jenis Obat</h3>
                </div>
                <div class="box-body"> 
                    <?php getview('layouts/partials/message') ?>           
                    <div class="toolbar">
                        <a href="<?= base_url('data-jenis-obat/create') ?>" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Jenis Obat</a>
                    </div><br>
                    <table class="table table-bordered table-condensed table-hover">
                        <thead>
                            <tr>
                                <th>Jenis Obat</th>
                                <th class="text-center" width="170">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($rsJenisObat->result() as $jenis_obat) { ?>
                                <tr>
                                    <td><?= $jenis_obat->jenis_obat ?></td>
                                    <td class="text-center">
                                        <a href="<?= base_url('data-jenis-obat/edit/' . $jenis_obat->id) ?>" class="btn btn-info btn-sm"><i class="fa fa-pencil"></i> Ubah</a>
                                        <a onclick="return confirm('Apakah anda yakin akan menghapus data ini?')" href="<?= base_url('data-jenis-obat/delete/' . $jenis_obat->id) ?>" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> Hapus</a>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>