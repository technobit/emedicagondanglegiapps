<section class="content-header">
    <h1 class="title"><?=$title?></h1>
    <?=$breadcrumbs?>
</section>
<section class="content">
  <?= $this->form->open(null, 'class="form-horizontal"') ?>
    <div class="row">
      <div class="col-md-8">            
        <div class="box box-primary">
          <div class="box-header">
            <h3 class="box-title">Daftar Obat</h3>
          </div>
          <div class="box-body">
            <?php getview('layouts/partials/message') ?>   
            <?php getview('layouts/partials/validation') ?>      
            <table id="tblObat" class="table table-bordered table-condensed table-hover">
              <thead>
                <tr>
                  <th>Obat</th>
                  <th>Kemasan</th>
                  <th width="70px">Jumlah</th>                
                  <th width="70px">Jumlah</th>                                  
                </tr>
              </thead>
              <tbody>
                <?php if (formValue('obat')) { ?>    
                  <?php $rowsObat = 1; ?>          
                  <?php foreach(formValue('obat') as $obat) { ?>
                    <tr data-row="<?= $rowsObat ?>">
                      <td>
                        <input type="hidden" name="obat[<?= $rowsObat ?>][id_obat]" value="<?= $obat['id_obat'] ?>">
                        <input type="hidden" name="obat[<?= $rowsObat ?>][kode_obat]" value="<?= $obat['kode_obat'] ?>">
                        <input type="hidden" name="obat[<?= $rowsObat ?>][nama_obat]" value="<?= $obat['nama_obat'] ?>">
                        <?= $obat['kode_obat'] ?> - <?= $obat['nama_obat'] ?>
                      </td>
                      <td>
                        <input type="hidden" name="obat[<?= $rowsObat ?>][id_kemasan]" value="<?= $obat['id_kemasan'] ?>">
                        <input type="hidden" name="obat[<?= $rowsObat ?>][kemasan]" value="<?= $obat['kemasan'] ?>">
                        <?= $obat['kemasan'] ?>
                      </td>
                      <td class="text-center">
                        <input type="hidden" name="obat[<?= $rowsObat ?>][jml]" value="<?= $obat['jml'] ?>">
                        <?= $obat['jml'] ?>
                      </td>
                      <td>
                        <input type="text" name="obat[<?= $rowsObat ?>][jml_disetujui]" value="<?= $obat['jml_disetujui'] ?>" class="form-control text-center input-sm"/>
                      </td>                      
                    </tr>
                    <?php $rowsObat++ ?>
                  <?php } ?>
                <?php } ?>
              </tbody>
            </table>
            <br>          
            <div class="form-group text-center">
                <button type="submit" class="btn btn-success btn-lg"><i class="fa fa-check"></i> Setujui</button>   
                <a href="<?= base_url('gudang_apotik/permintaan_obat') ?>" class="btn btn-danger btn-lg"><i class="fa fa-ban"></i> Batal</a>             
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="box box-primary">
          <div class="box-header">
              <h4 class="box-title">Data Permintaan</h4>
          </div>
          <div class="box-body">          
            <div class="form-group">
              <label class="col-md-4 control-label">Tanggal Permintaan</label>
              <div class="col-md-8">
                <div class="input-group">
                  <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                  <?= $this->form->date('tgl_permintaan', date('Y-m-d'), 'class="form-control"') ?>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-4 control-label">No. PO</label>
              <div class="col-md-8">
                <?= $this->form->text('no_permintaan', null, 'placeholder="Auto Generate" class="form-control" readonly') ?>
              </div>
            </div> 
            <div class="form-group">
              <label class="col-md-4 control-label">Tanggal Disetujui</label>
              <div class="col-md-8">
                <div class="input-group">
                  <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                  <?= $this->form->date('tgl_disetujui', date('Y-m-d'), 'id="tgl_disetujui" class="form-control"') ?>
                </div>
              </div>
            </div>             
          </div>                          
        </div>      
      </div>    
    </div>    
  <?= $this->form->close() ?>
</section>