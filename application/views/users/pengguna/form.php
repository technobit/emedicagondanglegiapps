<section class="content-header">
  <h1 class="title"><?=$title?></h1>
  <?=$breadcrumbs?>
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
    <!-- Hasil Pencarian Data Pasien -->
    <div class="box box-success">
    <!-- form start -->
        <form class="form-horizontal" id="frm-input">
          <div class="box-body">
                <?=$intIdUser?>
                <?=$intIdPegawai?>
                <?=$txtUserName?>
                <?=$txtPassword?>
                <?=$txtRePassword?>
                <?=$txtEmail?>
                <?=$intIdLevel?>
                <?=$bitStatusUser?>
                <div class="col-sm-6 col-sm-offset-3">
                <button class="btn btn-primary btn-success" id="saveBtn" type="button"><i class="fa fa-save"></i> Simpan</button>
                <a href="<?=$link_index?>" class="btn btn-danger btn-flat" id="cancelButton"><i class="fa fa-ban"></i> Batal</a>
                <a href="<?=$link_ganti_password?>" class="btn btn-warning btn-flat" id="cancelButton"><i class="fa fa-key"></i> Ganti Password</a>
            </div>
          </div>
        </form>
    </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>