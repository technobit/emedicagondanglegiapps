<section class="content-header">
  <h1 class="title"><?=$title?></h1>
  <?=$breadcrumbs?>
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
    <!-- Hasil Pencarian Data Pasien -->
    <?php
        $alert = $this->session->flashdata("alert_message");
        if(isset($alert) && !empty($alert)):
        $message = $alert['message'];
        $status = ucwords($alert['status']);
        $class_status = ($alert['status'] == true) ? 'success' : 'danger';
        $icon = ($alert['status'] == true) ? 'check' : 'ban';
        ?>
        <div class="alert alert-<?=$class_status?> alert-dismissable">
          <i class="fa fa-<?=$icon?>"></i>
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          <?=$message?>
       </div>
        <?php
        endif;
      ?>
      <div class="box box-primary">
        <!-- /.box-header -->
        <div class="box-header">
          <div class="box-tools">
            <a href="<?=$link_add?>" class="btn btn-success"><i class="fa fa-plus"></i> Tambah Data</a>
          </div>
        </div>
        <div class="box-body">
          <table class="table table-hover" id="table-data">
                <thead>
                  <tr>
                    <th>Username</th>
                    <th>Nama Pegawai</th>
                    <th>Email</th>
                    <th>Level</th>
                    <th>Status</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  
                </tbody>
              </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>