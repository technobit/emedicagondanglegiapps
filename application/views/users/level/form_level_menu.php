<section class="content-header">
  <h1 class="title"><?=$title?></h1>
  <?=$breadcrumbs?>
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
    <!-- Hasil Pencarian Data Pasien -->
    <div class="box box-success">
    <!-- form start -->
        <form class="form-horizontal" id="frm-input">
          <div class="box-body">
                <?=$intIdLevel?>
                <?=$txtLevel?>
                <?=$bitStatusLevel?>
                
          </div>
        </form>
    </div>
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Detail Akses Menu</h3>
            <div class="box-tools">
                <button class="btn btn-success" id="btnCheckAll">Check All</button>
            </div>
        </div>
        <div class="box-body">
            <form class="form-horizontal" id="frm-level-akses">
            <?php 
            $no = 1;
            foreach($config_menu as $indexMenu => $rowMenu) :
            $classGrid= !empty($rowMenu['sub_menu']) ? "col-sm-12" : "col-sm-6";
            $frmCheckbox = !empty($rowMenu['sub_menu']) ? "" : form_checkbox("menu[".$indexMenu."][is_view]" , $indexMenu, isset($data_menu[$indexMenu]['is_view']) , "id='frm_checkbox_".$no."' onclick='checkForm(\"".$no."\")'");
            ?>
            <div class="form-group">
            <div class="<?=$classGrid?>">
                <div class="checkbox">
                <label>
                    <?=$frmCheckbox?>
                    <?=$rowMenu['desc']?>
                </label>
                </div>
            </div>
            <?php 
            if(empty($rowMenu['sub_menu'])) : 
            ?>
            <!--div class="col-sm-2">
                <div class="checkbox">
                <label>
                    <?=form_checkbox("menu[".$indexMenu."][is_insert]" , 1, isset($data_menu[$indexMenu]['is_insert']), "id='frm_checkbox_ins_".$no."'")?>
                    Insert
                </label>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="checkbox">
                <label>
                <?=form_checkbox("menu[".$indexMenu."][is_update]" , 1, isset($data_menu[$indexMenu]['is_update']), "id='frm_checkbox_upd_".$no."'")?>
                    Update
                </label>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="checkbox">
                <label>
                    <?=form_checkbox("menu[".$indexMenu."][is_delete]" , 1, isset($data_menu[$indexMenu]['is_delete']), "id='frm_checkbox_del_".$no."'")?>
                    Delete
                </label>
                </div>
            </div-->
            <?php
            endif;
            ?>
            </div>
                <?php 
                if((!empty($rowMenu['sub_menu'])) && ( isset($config_submenu[$rowMenu['sub_menu']]))) :
                $noSub = 1;
                foreach($config_submenu[$rowMenu['sub_menu']] as $indexSubMenu => $rowSubMenu) :
                $classGrid= !empty($rowSubMenu['subsub_menu']) ? "col-sm-11" : "col-sm-5";
                $frmCheckbox = !empty($rowSubMenu['subsub_menu']) ? "" : form_checkbox("menu[".$indexSubMenu."][is_view]" , $indexSubMenu, isset($data_menu[$indexSubMenu]['is_view']), "id='frm_checkbox_".$no."_".$noSub."' onclick='checkForm(\"".$no."_".$noSub."\")'");
                ///$frmCheckbox = !empty($rowMenu['sub_menu']) ? "" : form_checkbox("menu[]" , $indexMenu, FALSE ); 
                ?>
                <div class="form-group">
                <div class="col-sm-offset-1 <?=$classGrid?>">
                    <div class="checkbox">
                    <label>
                        <?=$frmCheckbox?>
                        <?=$rowSubMenu['desc']?>
                    </label>
                    </div>
                </div>
                <?php 
                if(empty($rowSubMenu['subsub_menu'])) : 
                ?>
                <!--div class="col-sm-2">
                <div class="checkbox">
                <label>
                    <?=form_checkbox("menu[".$indexSubMenu."][is_insert]" , 1, isset($data_menu[$indexSubMenu]['is_insert']), "id='frm_checkbox_ins_".$no."_".$noSub."'")?>
                    Insert
                </label>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="checkbox">
                <label>
                <?=form_checkbox("menu[".$indexSubMenu."][is_update]" , 1, isset($data_menu[$indexSubMenu]['is_update']), "id='frm_checkbox_upd_".$no."_".$noSub."'")?>
                    Update
                </label>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="checkbox">
                <label>
                    <?=form_checkbox("menu[".$indexSubMenu."][is_delete]" , 1, isset($data_menu[$indexSubMenu]['is_delete']), "id='frm_checkbox_del_".$no."_".$noSub."'")?>
                    Delete
                </label>
                </div>
            </div-->
                <?php
                endif;
                ?>
                </div>
                        <?php 
                        if((!empty($rowSubMenu['subsub_menu'])) && ( isset($config_subsubmenu[$rowSubMenu['subsub_menu']]))) :
                        $noSubSub = 1;
                        foreach($config_subsubmenu[$rowSubMenu['subsub_menu']] as $indexSubSubMenu => $rowSubSubMenu) :
                        $classGrid= "col-sm-4"; 
                        ?>
                        <div class="form-group">
                            <div class="col-sm-offset-2 <?=$classGrid?>">
                                <div class="checkbox">
                                <label>
                                    <?=form_checkbox("menu[".$indexSubSubMenu."][is_view]" , $indexSubSubMenu, isset($data_menu[$indexSubSubMenu]['is_view']), "id='frm_checkbox_".$no."_".$noSub."_".$noSubSub."' onclick='checkForm(\"".$no."_".$noSub."_".$noSubSub."\")'");?>
                                    <?=$rowSubSubMenu['desc']?>
                                </label>
                                </div>
                            </div>
                             <!--div class="col-sm-2">
                                <div class="checkbox">
                                <label>
                                <?=form_checkbox("menu[".$indexSubSubMenu."][is_insert]" , 1, isset($data_menu[$indexSubSubMenu]['is_insert']), "id='frm_checkbox_ins_".$no."_".$noSub."_".$noSubSub."'")?>
                                    Insert
                                </label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="checkbox">
                                <label>
                                <?=form_checkbox("menu[".$indexSubSubMenu."][is_update]" , 1, isset($data_menu[$indexSubSubMenu]['is_update']) , "id='frm_checkbox_upd_".$no."_".$noSub."_".$noSubSub."'")?>
                                    Update
                                </label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="checkbox">
                                <label>
                                <?=form_checkbox("menu[".$indexSubSubMenu."][is_delete]" , 1, isset($data_menu[$indexSubSubMenu]['is_delete']),  "id='frm_checkbox_del_".$no."_".$noSub."_".$noSubSub."'")?>
                                    Delete
                                </label>
                                </div>
                            </div-->
                        </div>  
                        <?php
                        $noSubSub++;
                        endforeach;
                        endif;
                        ?>
                <?php
                $noSub++;
                endforeach;
                endif;
                ?>
            <?php
            $no++;
            endforeach; 
            ?>
            <div class="col-md-12 text-center">
            <button class="btn btn-primary btn-success" id="saveLevelBtn" type="button"><i class="fa fa-save"></i> Simpan</button>
            <a href="<?=$link_index?>" class="btn btn-danger btn-flat" id="cancelButton"><i class="fa fa-ban"></i> Batal</a>
            </div>
            </form>
        </div>
        
        
       </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>