<dl class="dl-horizontal">
                <dt>No. Kartu BPJS</dt>
                <dd><?=$noKartu?></dd>
                <dt>Nama</dt>
                <dd><?=$nama?></dd>
                <dt>Status Peserta</dt>
                <dd><?=$hubunganKeluarga?></dd>
                <dt>Jenis Peserta</dt>
                <dd><?=$jnsPeserta['nama']?></dd>
                <dt>Tanggal Lahir </dt>
                <dd><?=str_replace("-","/",$tglLahir)?> (dd/MM/YY)</dd>
                <dt>Jenis Kelamin</dt>
                <dd><?=$sex=="P" ? "Perempuan" : "Laki-Laki"?></dd>
                <dt>PPK</dt>
                <dd><?=$kdProviderPst['kdProvider']." - ".$kdProviderPst['nmProvider']?></dd>
                <dt>Status</dt>
                <dd><?=$ketAktif?></dd>
</dl>