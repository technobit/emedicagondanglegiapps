
<div class="alert alert-info alert-dismissible">
    <h4><i class="icon fa fa-info"></i> Peringatan</h4>
    Diagnosa Dengan Kode ICD-X Yang Sama Tidak Tersedia Pada Data Diagnosa BPJS, Silahkan Pilih Diagnosa Yang sesuai
</div>
<div class="box box-success">
    <div class="box-header">
        <h3 class="box-title">Data Diagnosa Yang Sejenis : <?=$jumlahDiagnosa?></h3>
        <?=form_hidden("numberDiagnosa" , $number)?>
    </div>
    <div class="box-body">
        <table class="table">
            <thead>
                <tr>
                    <td>No</td>
                    <td>Kode ICD-X</td>
                    <td>Nama Diagnosa</td>
                    <td>Aksi</td>
                </tr>
            </thead>
            <tbody>
                <?php
                    $no = 1;
                    foreach($dataDiagnosa as $rowDiagnosa) : 
                ?>
                    <tr>
                        <td><?=$no?></td>
                        <td><?=$rowDiagnosa['kdDiag']?></td>
                        <td><?=$rowDiagnosa['nmDiag']?></td>
                        <td><button class="btn btn-primary btn-flat" type="button" onclick="getDataDiagnosaBPJS('<?=$rowDiagnosa['kdDiag']?>' , '<?=$rowDiagnosa['nmDiag']?>')"><i class="fa fa-pencil"></i> Pilih</button></td>
                    </tr>
                <?php 
                    $no++;
                    endforeach;
                ?>
            </tbody>
        </table>
    </div>
</div>

<script>
    function getDataDiagnosaBPJS(kdDiagnosaBPJS , nmDiagnosaBPJS){
        var numberDiagnosa = $('input[name=numberDiagnosa]').val();
        var strKode = kdDiagnosaBPJS+"-"+nmDiagnosaBPJS;
        $('#diagnosaBPJS_'+numberDiagnosa).val(strKode);
        $('.bootbox').modal('hide');
    }
</script>