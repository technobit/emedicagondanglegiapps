<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Penerimaan_obat_m extends Base_m {
    protected $table = 'penerimaan_obat';
    protected $fillable = array('id_apotik', 'tgl_penerimaan:date', 'no_penerimaan', 'tgl_disetujui', 'disetujui_oleh');
    protected $timestamp = true;

    public function getObat($id) {
        return $this->db->select('penerimaan_obat.*, penerimaan_obat_detail.*, obat.id as id_obat, obat.kode_obat, obat.nama_obat, kemasan.id as id_kemasan, kemasan.kemasan')        
        ->join('penerimaan_obat_detail', 'penerimaan_obat.id = penerimaan_obat_detail.id_penerimaan_obat')
        ->join('obat', 'obat.id = penerimaan_obat_detail.id_obat')
        ->join('kemasan', 'kemasan.id = obat.id_kemasan', 'left')
        ->where('penerimaan_obat.id', $id)
        ->get('penerimaan_obat');
    }

    public function createPO($id_apotik, $record) {
        $record['id_apotik'] = $id_apotik;         
        $record['no_penerimaan'] = $this->generateID();
    	$this->insert($record);
    	$id_penerimaan_obat = $this->db->insert_id();
        $this->load->model('obat_m');
    	foreach ($record['obat'] as $row) {
    		$this->db->insert('penerimaan_obat_detail', array(
    			'id_penerimaan_obat' => $id_penerimaan_obat,
    			'id_obat' => $row['id_obat'],
    			'jml' => $row['jml']
    		));
    		$this->db->where('id', $row['id_obat'])->set('stok', 'stok + '.$row['jml'], false)->update('obat');
            $this->obat_m->update_stok_periode($row['id_obat']);
    	}
    	return true;
    }

    public function deletePO($id) {
        $rsObat = $this->db->where('id_penerimaan_obat', $id)->get('penerimaan_obat_detail')->result();
        $this->load->model('obat_m');
        foreach ($rsObat as $obat) {
            $this->db->set('stok', 'stok - '.$obat->jml, false)->where('id', $obat->id_obat)->update('obat');
            $this->db->where('id', $obat->id)->delete('penerimaan_obat_detail');
            $this->obat_m->update_stok_periode($obat->id);
        }
        $this->db->where('id', $id)->delete('penerimaan_obat');
    }

    public function generateID() {
        $prefix = 'PO/'. date('m') . '/' . date('Y') . '/';
        $digit = '000';
        $lastID =  $this->db->select_max('no_penerimaan')
        ->where('left(no_penerimaan,'.(strlen($prefix)).')', $prefix)
        ->get($this->table)->row()->no_penerimaan;
        if ($lastID) {            
            $counter = substr($lastID, -strlen($digit)) + 1;
            return $prefix.substr($digit.$counter, -strlen($digit));
        } else {
            return $prefix.substr($digit.'1', -strlen($digit));
        }
    }

}