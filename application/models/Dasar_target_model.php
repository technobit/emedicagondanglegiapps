<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dasar_target_model extends CI_Model {

	var $table = "data_target_imunisasi"; 
	var $primary_key = "id_target_imunisasi";
	var $foreign_key = "id_kelurahan";
    
    public function __construct(){
        parent::__construct();
        
    }
    
	
    public function getData($IDKelurahan = "", $tahun = ""){
        if(!empty($IDKelurahan)){
        	$this->db->where("id_kelurahan" , $IDKelurahan);
            
        }
		 if($tahun != ""){
        	$this->db->where("tahun" , $tahun);
            
        }
		$this->db->select($this->table.
			'.*, kelurahan.Nama as nama_kelurahan,'
		);
		$this->db->join('kelurahan' , 'kelurahan.IDKelurahan = '.$this->table.'.id_kelurahan');
		$this->db->from($this->table);
        $data = $this->db->get();
        return $data->result_array();
    }
	

	
	public function getDetail($id){
		
		$this->db->where($this->primary_key , $id);
		
		$query = $this->db->get($this->table);
		$resVal = !empty($query) ? $query->row_array() : false;	
		return $resVal;
	}
	
	public function saveData($arrData = array(),$debug=false){
		
		$this->db->set($arrData);
		if($debug){
			$retVal = $this->db->get_compiled_insert($this->table);
		}else{
			$res = $this->db->insert($this->table);
			if(!$res){
				$retVal['error_stat'] = "Data Gagal Di Simpan";
				$retVal['status'] = false;
			}else{
				$retVal['error_stat'] = "Data Berhasil Di Simpan";
				$retVal['status'] = true;
				$retVal['id'] = $this->db->insert_id();
			}
			
		}
		return $retVal;
	}
	
	public function update($array , $id){
		
		$this->db->where($this->primary_key, $id);
		$query = $this->db->update($this->table, $array);
		if(!$query){
			
			$retVal['error_stat'] = "Data Gagal Di perbarui";
			$retVal['status'] = false;
		}else{
			$retVal['error_stat'] = "Data Berhasil Di Perbarui";
			$retVal['status'] = true;
			$retVal['id'] = $id;
		}
		
		return $retVal;
	}
	
	public function delete($id){
		$this->db->where($this->primary_key , $id);
		$q = $this->db->delete($this->table);
		
		if(!$q){
			$retVal['error_stat'] = "Data Gagal Di Hapus";
			$retVal['status'] = false;
		}else{
			$retVal['error_stat'] = "Data Berhasil Di Hapus";
			$retVal['status'] = true;
		}
		
		return $retVal;
	}
	
}

