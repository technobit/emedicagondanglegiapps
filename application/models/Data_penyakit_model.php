<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data_Penyakit_model extends CI_Model {

	var $table = "data_penyakit";
	var $primary_key = "intIdPenyakit";

    public function __construct(){
        parent::__construct();

    }

	/// For List Data / Option Select 2
	public function listDataDiagnosa($limit , $offset){
		$this->db->select('intIdPenyakit as id, txtCategory');
		$this->db->from($this->table);
        $this->db->limit($limit);
        $this->db->offset($offset);
        $data = $this->db->get();
        return $data->result_array();
	}

	public function listDiagnosaRename($limit , $offset){
		$this->db->select('intIdPenyakit as id, 
		txtCategory,
		txtEnglishName,
		txtIndonesianName
		');
		$this->db->from($this->table);
        $this->db->limit($limit);
        $this->db->offset($offset);
        $data = $this->db->get();
        return $data->result_array();
	}

    public function getListDataPenyakit($where = "" , $offset , $limit){
        if($where!=""){
            $this->db->like("txtIndonesianName" , $where);
						$this->db->or_like("txtEnglishName" , $where);
            $this->db->or_like("txtCategory" , $where);
        }

        $this->db->select('intIdPenyakit as id, txtCategory,txtSubCategory,txtIndonesianName , txtEnglishName');
		$this->db->from($this->table);
        $this->db->limit($limit);
        $this->db->offset($offset);
        $data = $this->db->get();
        return $data->result_array();
    }

    function getCountListDataPenyakit($where = ""){
        if($where==""){
            $this->db->like("txtIndonesianName" , $where);
            $this->db->or_like("txtCategory" , $where);
        }

        $this->db->select($this->table.'.* ');
		$this->db->from($this->table);
        $data = $this->db->count_all_results();
        return $data;
    }

	/// For Index Data
    public function getData($offset = 0 , $limit = 10 , $search=""){

		if(!empty($search)){
			$this->db->like("txtIndonesianName" , $search);
			$this->db->or_like("txtCategory" , $search);
		}

        $this->db->select($this->table.'.*');
		$this->db->from($this->table);
		$this->db->offset($offset);
		$this->db->limit($limit);
        $data = $this->db->get();
        return $data->result_array();
    }

	public function getCountData($search=""){

		if(!empty($search)){
			$this->db->like("txtIndonesianName" , $search);
			$this->db->or_like("txtCategory" , $search);
		}

        $this->db->select($this->table.'.*');
				$this->db->from($this->table);
        $data = $this->db->count_all_results();
        return $data;
    }

	public function countAllDiagnosis(){
		$this->db->from($this->table);
		$data = $this->db->count_all_results();
		return $data;
	}

	public function getListDataJaminan(){
        $this->db->select('intIdJaminanKesehatan, txtNamaJaminan ,bitStatusJaminan');
		$this->db->from($this->table);
		$this->db->where('bitStatusJaminan' , '1');
        $data = $this->db->get();
		$resVal = $data->result_array();
		$arrVal = array();
		foreach($resVal as $val){
			$arrVal[$val['intIdJaminanKesehatan']] = $val['txtNamaJaminan'];
		}
        return $arrVal;
    }

    public function getCountDataIndex($search=""){
    	if(!empty($search)){
			$this->db->where($search);
		}
		$this->db->select($this->table.'.* ');
		$this->db->from($this->table);
		$data = $this->db->count_all_results();
        return $data;
    }


	public function getDetail($id){
		$this->db->where($this->primary_key , $id);
		$query = $this->db->get($this->table,1);
		$resVal = "";
		if($query->num_rows() > 0){
			$resVal = $query->row_array();
		}else{
			$resVal = false;
		}
		return $resVal;
	}

	public function saveData($arrData = array(),$debug=false){

		$this->db->set($arrData);
		if($debug){
			$retVal = $this->db->get_compiled_insert($this->table);
		}else{
			$res = $this->db->insert($this->table);
			if(!$res){
				$retVal['error_stat'] = "Data Gagal Di Simpan";
				$retVal['status'] = false;
			}else{
				$retVal['error_stat'] = "Data Berhasil Di Simpan";
				$retVal['status'] = true;
				$retVal['id'] = $this->db->insert_id();
			}

		}
		return $retVal;
	}

	public function update($array , $id){

		$this->db->where($this->primary_key , $id);
		$query = $this->db->update($this->table, $array);
		if(!$query){

			$retVal['error_stat'] = "Data Gagal Di perbarui";
			$retVal['status'] = false;
		}else{
			$retVal['error_stat'] = "Data Berhasil Di Perbarui";
			$retVal['status'] = true;
			$retVal['id'] = $id;
		}

		return $retVal;
	}

	public function delete($id){
		$this->db->where($this->primary_key , $id);
		$q = $this->db->delete($this->table);

		if(!$q){
			$retVal['error_stat'] = "Data Gagal Di Hapus";
			$retVal['status'] = false;
		}else{
			$retVal['error_stat'] = "Data Berhasil Di Hapus";
			$retVal['status'] = true;
		}

		return $retVal;
	}

	//// Laporan LB-1

	public function getDataLB1($idPelayanan , $idBulan){

		$stringQuery = "CALL `get_report_data_kesakitan`(".$idPelayanan.",".$idBulan.")";
		$data = $this->db->query($stringQuery);
		$resVal = $data->result_array();
		return $resVal;
	}

	public function getDataLB1V2($idPelayanan , $idBulan , $idYear){
		$stringQuery = "CALL get_report_data_kesakitan_v2(".$idPelayanan.",".$idBulan." , ".$idYear.")";
		$data = $this->db->query($stringQuery);
		///// To Execute FUckn Procedure You Have TO Execute This SHit
		$res = $data->next_result();
		$resVal =$data->result_array();
		return $resVal;
	}

	/// For Migration Data 

	public function getDiagnosaBaru($limit, $offset){
		echo $limit;
		$data = $this->db->order_by("txtKodeICD")
		->limit($limit)
		->offset($offset)
		->get("data_diagnosa_bpjs")
		->result_array();
		return $data;
	}

	public function countDiagnosaBPJSBaru(){
		$data = $this->db->order_by("txtKodeICD")
		->from("data_diagnosa_bpjs");
		return $data->count_all_results();
	}

	public function countDiagnosaBaru(){
		$data = $this->db->from("data_penyakit_baru");
		return $data->count_all_results();
	}

	public function saveDiagnosaBaru($arrInsert , $is_batch = true){
		if($is_batch){
			$res = $this->db->insert_batch("data_penyakit_baru" , $arrInsert);
		}else{
			$res = $this->db->insert("data_penyakit_baru" , $arrInsert);
		}
		$retVal['status'] = $res;
		return $retVal;
	}

	

	public function getAllUsedDiagnosa($limit = 10, $offset = 0){
		
		$limitStr = " LIMIT ".$limit;
		$query = "
SELECT `rekam_medis_diagnosa`.`intIdDiagnosaPenyakit`,`data_penyakit`.`txtCategory`, `data_penyakit`.`txtEnglishName`,`data_penyakit`.`txtIndonesianName`, `data_penyakit_baru`.`intIdPenyakit`, COUNT(`data_penyakit`.`intIdPenyakit`) AS jumlah_penyakit, COUNT(`data_penyakit_baru`.`intIdPenyakit`) AS jumlah_diagnosa_baru
FROM `rekam_medis_diagnosa`
INNER JOIN data_penyakit ON `data_penyakit`.`intIdPenyakit` =  `rekam_medis_diagnosa`.`intIdDiagnosaPenyakit`
LEFT JOIN `data_penyakit_baru` ON `data_penyakit_baru`.`intIdPenyakit` = `data_penyakit`.`txtCategory`
WHERE `data_penyakit_baru`.`intIdPenyakit` IS NULL
GROUP BY rekam_medis_diagnosa.`intIdDiagnosaPenyakit`
ORDER BY `jumlah_penyakit` DESC

		".$limitStr;


		$res = $this->db->query($query);
		return $res->result_array();
	}

	public function getAllUsedDiagnosaBaru($limit){
		///$limitStr = " LIMIT ".$limit." OFFSET ".$offset."";
		$limitStr = " LIMIT ".$limit;
		$query = "
		SELECT `rekam_medis_diagnosa_baru`.`intIdDiagnosaPenyakit`, `data_penyakit`.`txtCategory`, `data_penyakit_baru`.`txtCategory` FROM `rekam_medis_diagnosa_baru` 
		INNER JOIN `data_penyakit` ON `data_penyakit`.`intIdPenyakit` = `rekam_medis_diagnosa_baru`.`intIdDiagnosaPenyakit`
		INNER JOIN `data_penyakit_baru` ON `data_penyakit_baru`.`intIdPenyakit` = data_penyakit.`txtCategory`
		GROUP BY `rekam_medis_diagnosa_baru`.`intIdDiagnosaPenyakit`
		".$limitStr;

		$res = $this->db->query($query);
		return $res->result_array();
	}

	public function getCountAllUsedDiagnosaBaru(){
		
		$query = "
		SELECT `rekam_medis_diagnosa_baru`.`intIdDiagnosaPenyakit`, `data_penyakit`.`txtCategory`, `data_penyakit_baru`.`txtCategory` FROM `rekam_medis_diagnosa_baru` 
		INNER JOIN `data_penyakit` ON `data_penyakit`.`intIdPenyakit` = `rekam_medis_diagnosa_baru`.`intIdDiagnosaPenyakit`
		INNER JOIN `data_penyakit_baru` ON `data_penyakit_baru`.`intIdPenyakit` = data_penyakit.`txtCategory`
		GROUP BY `rekam_medis_diagnosa_baru`.`intIdDiagnosaPenyakit`
		";

		$res = $this->db->query($query);
		return $res->result_id->num_rows;
	}

	public function getCountAllUsedDiagnosa(){
		$query = "
SELECT `rekam_medis_diagnosa`.`intIdDiagnosaPenyakit`,`data_penyakit`.`txtCategory`, `data_penyakit`.`txtEnglishName`,`data_penyakit`.`txtIndonesianName`, `data_penyakit_baru`.`intIdPenyakit`, COUNT(`data_penyakit`.`intIdPenyakit`) AS jumlah_penyakit, COUNT(`data_penyakit_baru`.`intIdPenyakit`) AS jumlah_diagnosa_baru
FROM `rekam_medis_diagnosa`
INNER JOIN data_penyakit ON `data_penyakit`.`intIdPenyakit` =  `rekam_medis_diagnosa`.`intIdDiagnosaPenyakit`
LEFT JOIN `data_penyakit_baru` ON `data_penyakit_baru`.`intIdPenyakit` = `data_penyakit`.`txtCategory`
WHERE `data_penyakit_baru`.`intIdPenyakit` IS NULL
GROUP BY rekam_medis_diagnosa.`intIdDiagnosaPenyakit`
ORDER BY `jumlah_penyakit` DESC
		";
		$res = $this->db->query($query);
		return $res->result_id->num_rows;
	}

	public function updateBatchRekmedDiagnosaBaru($array , $id){

		$this->db->where("intIdDiagnosaPenyakit" , $id);
		$query = $this->db->update("rekam_medis_diagnosa_baru", $array);
		if(!$query){
			$retVal['error_stat'] = "Data Gagal Di perbarui";
			$retVal['status'] = false;
		}else{
			$retVal['error_stat'] = "Data Berhasil Di Perbarui";
			$retVal['status'] = true;
		}

		return $retVal;	
	}

	public function updateBatchDiagnosa($array){

		$query = $this->db->update_batch("data_penyakit", $array , $this->primary_key);
		if(!$query){
			$retVal['error_stat'] = "Data Gagal Di perbarui";
			$retVal['status'] = false;
		}else{
			$retVal['error_stat'] = "Data Berhasil Di Perbarui";
			$retVal['status'] = true;
		}

		return $retVal;	
	}


	

}
