<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Desa_m extends Base_m {
    protected $table = 'desa';
    protected $fillable = array('kode_desa', 'nama_desa');
}