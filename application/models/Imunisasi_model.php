<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Imunisasi_model extends CI_Model {

	var $tableData = "kegiatan_imunisasi"; 
	var $primary_key = "id_kegiatan_imunisasi";
	var $foreign_key = "id_kelurahan";
    
    public function __construct(){
        parent::__construct();
        
    }
    
	
    public function getData($IDKelurahan = "", $jenis_kegiatan = "", $startDate, $endDate ){
        if(!empty($IDKelurahan)){
        	$this->db->where("id_kelurahan" , $IDKelurahan);
            
        }
		 if($jenis_kegiatan != ""){
        	$this->db->where("jenis_lokasi" , $jenis_kegiatan);
            
        }
		$this->db->select($this->tableData.".*,(SELECT COUNT(*) FROM imunisasi_pasien where id_kegiatan_imunisasi = kegiatan_imunisasi.id_kegiatan_imunisasi ) AS jumlah_pasien, (SELECT COUNT(*) FROM kegiatan_imunisasi_petugas where id_kegiatan_imunisasi = kegiatan_imunisasi.id_kegiatan_imunisasi ) AS jumlah_petugas, kelurahan.Nama");
		$this->db->where($this->tableData.".tanggal_kegiatan BETWEEN '".$startDate."' AND '".$endDate."'");
		$this->db->join('kelurahan' , 'kelurahan.IDKelurahan = '.$this->tableData.'.id_kelurahan');
		$this->db->from($this->tableData);
		$this->db->group_by('id_kegiatan_imunisasi');
        $data = $this->db->get();
        return $data->result_array();
    }
	public function getDataPegawai($IDKegiatan = ""){
        $this->db->select("*");
		$this->db->where('kegiatan_imunisasi_petugas.id_kegiatan_imunisasi = "'.$IDKegiatan.'"');
			$this->db->join('pegawai' , 'pegawai.intIdPegawai = kegiatan_imunisasi_petugas.intIdPegawai');
		
		$this->db->from("kegiatan_imunisasi_petugas");
        $data = $this->db->get();
        return $data->result_array();
    }
		public function getDataImun($IDImun = ""){
        $this->db->select("*");
		$this->db->where('jenis_imunisasi_pasien.id_imunisasi_pasien = "'.$IDImun.'"');
		$this->db->from("jenis_imunisasi_pasien");
        $data = $this->db->get();
        return $data->result_array();
    }
		public function getDataPeserta($IDKegiatan = ""){
        $this->db->select("*,(SELECT COUNT(*) FROM jenis_imunisasi_pasien where id_imunisasi_pasien = imunisasi_pasien.id_imunisasi_pasien ) AS jumlah_imunisasi");
		$this->db->where('imunisasi_pasien.id_kegiatan_imunisasi = "'.$IDKegiatan.'"');
		$this->db->join('pasien' , 'pasien.txtIdPasien = imunisasi_pasien.txtIdPasien');
		$this->db->from("imunisasi_pasien");
        $data = $this->db->get();
        return $data->result_array();
    }
	public function getJenisImunisasi(){
       $this->db->select("*");
		$this->db->from("jenis_imunisasi");
        $data = $this->db->get();
        return $data->result_array();
    }
	

	
	public function getDetail($id){
		
		$this->db->where($this->primary_key , $id);
		
		$query = $this->db->get($this->tableData);
		$resVal = !empty($query) ? $query->row_array() : false;	
		return $resVal;
	}
		public function getDetailPasien($id){
		
		$this->db->where("id_imunisasi_pasien" , $id);
		
		$query = $this->db->get("imunisasi_pasien");
		$resVal = !empty($query) ? $query->row_array() : false;	
		return $resVal;
	}
	
	public function saveData($arrData = array(),$debug=false){
		
		$this->db->set($arrData);
		if($debug){
			$retVal = $this->db->get_compiled_insert($this->tableData);
		}else{
			$res = $this->db->insert($this->tableData);
			if(!$res){
				$retVal['error_stat'] = "Data Gagal Di Simpan";
				$retVal['status'] = false;
			}else{
				$retVal['error_stat'] = "Data Berhasil Di Simpan";
				$retVal['status'] = true;
				$retVal['id'] = $this->db->insert_id();
			}
			
		}
		return $retVal;
	}
	public function saveDataPeserta($arrData = array(),$debug=false){
		
		$this->db->set($arrData);
		if($debug){
			$retVal = $this->db->get_compiled_insert("imunisasi_pasien");
		}else{
			$res = $this->db->insert("imunisasi_pasien");
			if(!$res){
				$retVal['error_stat'] = "Data Gagal Di Simpan";
				$retVal['status'] = false;
			}else{
				$retVal['error_stat'] = "Data Berhasil Di Simpan";
				$retVal['status'] = true;
				$retVal['id'] = $this->db->insert_id();
			}
			
		}
		return $retVal;
	}
		public function saveDataImun($arrData = array(),$debug=false){
		
		$this->db->set($arrData);
		if($debug){
			$retVal = $this->db->get_compiled_insert("jenis_imunisasi_pasien");
		}else{
			$res = $this->db->insert("jenis_imunisasi_pasien");
			if(!$res){
				$retVal['error_stat'] = "Data Gagal Di Simpan";
				$retVal['status'] = false;
			}else{
				$retVal['error_stat'] = "Data Berhasil Di Simpan";
				$retVal['status'] = true;
				$retVal['id'] = $this->db->insert_id();
			}
			
		}
		return $retVal;
	}

	public function cekPetugas($id){
		$this->db->from("kegiatan_imunisasi_petugas");
		$this->db->where("id_kegiatan_imunisasi" , $id);
		$data = $this->db->count_all_results();
		if($data > 0){
			$this->deletePetugas($id);
		}
		return $data;
	}

	public function savePetugas($arrData , $id){
		$this->cekPetugas($id);
		$ret = $this->db->insert_batch("kegiatan_imunisasi_petugas",$arrData);
		return $ret;
	}
	public function deletePetugas($id){
		$this->db->where("id_kegiatan_imunisasi" , $id);
		$data = $this->db->delete("kegiatan_imunisasi_petugas");
		return $data;
	}


public function cekJenisImun($id){
		$this->db->from("jenis_imunisasi_pasien");
		$this->db->where("id_imunisasi_pasien" , $id);
		$data = $this->db->count_all_results();
		if($data > 0){
			$this->deleteJenisImun($id);
		}
		return $data;
	}

	public function saveJenisImun($arrData , $id){
		$this->cekJenisImun($id);
		$ret = $this->db->insert_batch("jenis_imunisasi_pasien",$arrData);
		return $ret;
	}
	public function deleteJenisImun($id){
		$this->db->where("id_imunisasi_pasien" , $id);
		$data = $this->db->delete("jenis_imunisasi_pasien");
		return $data;
	}
	public function update($array , $id){
		
		$this->db->where($this->primary_key, $id);
		$query = $this->db->update($this->tableData, $array);
		if(!$query){
			
			$retVal['error_stat'] = "Data Gagal Di perbarui";
			$retVal['status'] = false;
		}else{
			$retVal['error_stat'] = "Data Berhasil Di Perbarui";
			$retVal['status'] = true;
			$retVal['id'] = $id;
		}
		
		return $retVal;
	}
	public function updatePeserta($array , $id){
		
		$this->db->where("id_imunisasi_pasien", $id);
		$query = $this->db->update("imunisasi_pasien", $array);
		if(!$query){
			
			$retVal['error_stat'] = "Data Gagal Di perbarui";
			$retVal['status'] = false;
		}else{
			$retVal['error_stat'] = "Data Berhasil Di Perbarui";
			$retVal['status'] = true;
			$retVal['id'] = $id;
		}
		
		return $retVal;
	}
	
	public function delete($id){
		
		$this->deletePetugas($id);
		$this->deletePasien($id, "id_kegiatan_imunisasi");
		$this->db->where($this->primary_key , $id);
		$q = $this->db->delete($this->tableData);
		
		if(!$q){
			$retVal['error_stat'] = "Data Gagal Di Hapus";
			$retVal['status'] = false;
		}else{
			$retVal['error_stat'] = "Data Berhasil Di Hapus";
			$retVal['status'] = true;
		}
		
		return $retVal;
	}
	public function deletePasien($id, $primary = ""){
		if ($primary== "id_imunisasi_pasien") {
			$this->deleteJenisImun($id);
		}
		$this->db->where($primary , $id);
		$q = $this->db->delete("imunisasi_pasien");
		
		if(!$q){
			$retVal['error_stat'] = "Data Gagal Di Hapus";
			$retVal['status'] = false;
		}else{
			$retVal['error_stat'] = "Data Berhasil Di Hapus";
			$retVal['status'] = true;
		}
		
		return $retVal;
	}
	
}

