<?php 
class Register_rawat_inap_m extends CI_Model {

    var $table = "register_rawat_inap";
    var $primary_key = "intIdRegisterRawatInap";
    var $secondary_key = "txtIdKunjungan";

    public function __construct(){
        parent::__construct();    
    }

    public function saveRegister($arrData = array() , $id , $mode="insert"){

        if($mode=="insert"){
            $this->db->set($arrData);
            $res = $this->db->insert($this->table);
        }else{
            $this->db->where($this->secondary_key , $id);
            $res = $this->db->update($this->table , $arrData);
        }
        $retVal = array();
        $status = $res;
        $retVal['status'] = $status;
        $retVal['message'] = $status==true ? "Data Berhasil Di Simpan" : "Data Gagal Di Simpan";
        $retVal['id'] = $id;
        return $retVal;
    }

    public function deleteRegister($id){
        $this->db->where($this->secondary_key , $id);
		$q = $this->db->delete($this->table);
		$retVal = array();
		if(!$q){
			$retVal['message'] = "Data Gagal Di Hapus";
			$retVal['status'] = false;
		}else{
			$retVal['message'] = "Data Berhasil Di Hapus";
			$retVal['status'] = true;
		}
		return $retVal;
    }
}