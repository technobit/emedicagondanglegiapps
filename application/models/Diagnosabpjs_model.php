<?php

class Diagnosabpjs_model extends CI_Model{

    var $table = 'data_diagnosa_bpjs';
    var $primary_key = 'txtKodeICD';
    function __construct(){
        parent::__construct();
    }

    function insertDiagnosa($arrPost){

        
        $this->db->set($arrPost);
        $res = $this->db->insert($this->table);
        $retVal['status'] = $res;
        $retVal['message'] = $res!=true ? "Data Gagal Di Simpan" : "Data berhasil di simpan";
        return $retVal;
    }

    function checkDiagnosa($kodeDiagnosa){
        $this->db->where($this->primary_key , $kodeDiagnosa);
        $this->db->from($this->table);
        $res = $this->db->count_all_results();
        return $res;
    }

    function getDiagnosaBPJS($txtInputSearch = ""){
        
        $this->db->select("txtKodeICD as kdDiag, txtEnglishName as nmDiag");
        if(!empty($txtInputSearch)){
            $this->db->like("txtKodeICD" , $txtInputSearch);
            $this->db->or_like("txtEnglishName" , $txtInputSearch);
        }
        $this->db->from($this->table);
        $res = $this->db->get();
        $retVal = $res->result_array();
        return $retVal;
    }

    function getCountDiagnosaBPJS($txtInputSearch = ""){
        
        $this->db->select("txtKodeICD as kdDiag, txtEnglishName as nmDiag");
        if(!empty($txtInputSearch)){
            $this->db->like("txtKodeICD" , $txtInputSearch);
            $this->db->or_like("txtEnglishName" , $txtInputSearch);
        }
        $this->db->from($this->table);
        $count = $this->db->count_all_results();
        $retVal = $count;
        return $retVal;
    }
    

    
}