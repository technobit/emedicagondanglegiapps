<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Level_menu_model extends CI_Model {

	var $table = "users_level_menu";
	var $primary_key = "intIdUsersLevelMenu";
	var $secondary_key = "intIdLevel";
    
    public function __construct(){
        parent::__construct();
        
    }
    
	/// For List Data
    public function getListLevel(){
        $this->db->from($this->table);
        $this->db->where("bitStatusLevel" , 1);
        $data = $this->db->get();
        return $data->result_array();
    }
	
	public function getMenuByIdLevel($id){
		$this->db->from($this->table);
        $this->db->where("intIdLevel" , $id);
		$data = $this->db->get();
        return $data->result_array();
	}
	
	public function checkMenuByIdLevel($id){
		$this->db->from($this->table);
		$this->db->where("intIdLevel" , $id);
		$data = $this->db->count_all_results();
		return $data;
	}
	
	
    
	/// For Index Data
    public function getData($offset = 0 , $limit = 10 , $search=""){
		
		if(!empty($search)){
			$this->db->like("txtTindakan" , $search);
			$this->db->or_like("intIdJenisPelayanan" , $search);
		}
		
        $this->db->select($this->table.'.*');
		$this->db->from($this->table);
		$this->db->offset($offset);
		$this->db->limit($limit);
        $data = $this->db->get();
        return $data->result_array();
    }
	
	public function getCountData($search=""){
		
		if(!empty($search)){
			$this->db->like("txtTindakan" , $search);
		}
		
        $this->db->select($this->table.'.*');
		$this->db->from($this->table);
        $data = $this->db->count_all_results();
        return $data;
    }
	
	public function getDetail($id){
		$this->db->where($this->primary_key , $id);
		$query = $this->db->get($this->table,1);
		$resVal = "";
		if($query->num_rows() > 0){
			$resVal = $query->row_array();	
		}else{
			$resVal = false;
		}
		return $resVal;
	}
	
	public function saveData($arrData = array(),$debug=false){
		
		$this->db->set($arrData);
		if($debug){
			$retVal = $this->db->get_compiled_insert($this->table);
		}else{
			$res = $this->db->insert($this->table);
			if(!$res){
				$retVal['error_stat'] = "Data Gagal Di Simpan";
				$retVal['status'] = false;
			}else{
				$retVal['error_stat'] = "Data Berhasil Di Simpan";
				$retVal['status'] = true;
				$retVal['id'] = $this->db->insert_id();
			}
			
		}
		return $retVal;
	}
	
	public function update($array , $id){
		
		$this->db->where($this->primary_key , $id);
		$query = $this->db->update($this->table, $array);
		if(!$query){
			
			$retVal['error_stat'] = "Data Gagal Di perbarui";
			$retVal['status'] = false;
		}else{
			$retVal['error_stat'] = "Data Berhasil Di Perbarui";
			$retVal['status'] = true;
			$retVal['id'] = $id;
		}
		
		return $retVal;
	}
	
	public function insertBatch($arrPost){
		$q = $this->db->insert_batch($this->table , $arrPost);
		if(!$q){
			$retVal['message'] = "Data Gagal Di Masukan";
			$retVal['status'] = false;
		}else{
			$retVal['message'] = "Data Berhasil Di Masukan";
			$retVal['status'] = true;
		}
		return $retVal;
	}
	
	public function delete($id){
		$this->db->where('intIdLevel' , $id);
		$q = $this->db->delete($this->table);
		
		if(!$q){
			$retVal['error_stat'] = "Data Gagal Di Hapus";
			$retVal['status'] = false;
		}else{
			$retVal['error_stat'] = "Data Berhasil Di Hapus";
			$retVal['status'] = true;
		}
		
		return $retVal;
	}
	
}

