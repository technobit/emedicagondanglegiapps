<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Finger_model extends CI_Model {

	var $table_device = "fingerspot_device";
    var $table_finger = "fingerspot_finger";
    var $table_verifikasi = "finger_verificationmatch";
	var $primary_key = "intIdTindakan";
    
    public function __construct(){
        parent::__construct();
        
    }
    
    function getDeviceBySN($serialNumber){

        $this->db->where("sn" , $serialNumber);
        $this->db->from($this->table_device);
        $data = $this->db->get();
        $resVal = $data->row_array();
        return $resVal;
    }

    function getDeviceByVC($verifyCode){

        $this->db->where("vc" , $verifyCode);
        $this->db->from($this->table_device);
        $data = $this->db->get();
        $resVal = $data->row_array();
        return $resVal;
    }

    function checkNumber($txtIdPasien){
        $this->db->where("txtIdPasien" , $txtIdPasien);
        $this->db->from($this->table_finger);
        $data = $this->db->count_all_results();
        return $data;
    }

    function checkNumberPegawai($intIdPegawai){
        $this->db->where("intIDPegawai" , $intIdPegawai);
        $this->db->from("mabsensi_pegawai");
        $data = $this->db->count_all_results();
        return $data;
    }

    

    function insertFinger($arrPost){
        $this->db->set($arrPost);
        $res = $this->db->insert($this->table_finger);
        if(!$res){
            $retVal['error_stat'] = "Data Gagal Di Simpan";
            $retVal['status'] = false;
        }else{
            $retVal['error_stat'] = "Data Berhasil Di Simpan";
            $retVal['status'] = true;
            $retVal['id'] = $this->db->insert_id();
        }
        return $retVal;
    }

    function insertFingerPegawai($arrPost , $id=""){
        if(!empty($id)){
            $this->db->where("intIDPegawai" , $id);
            $resDelete = $this->db->delete("mabsensi_pegawai");    
        }

        $this->db->set($arrPost);
        $res = $this->db->insert("mabsensi_pegawai");
        if(!$res){
            $retVal['error_stat'] = "Data Gagal Di Simpan";
            $retVal['status'] = false;
        }else{
            $retVal['error_stat'] = "Data Berhasil Di Simpan";
            $retVal['status'] = true;
            $retVal['id'] = $this->db->insert_id();
        }
        return $retVal;
    }

    


    function hapusFinger($txtIdPasien){
        $this->db->where("txtIdPasien" , $txtIdPasien);
        $res = $this->db->delete($this->table_finger);
        if($res){
            $retVal['message'] = "Data Berhasil Di Hapus";
            $retVal['status'] = true;
        }else{
            $retVal['message'] = "Data Gagal Di Hapus";
            $retVal['status'] = false;
        }
        return $retVal;
    }

    function getTxtIdPasienByID($uniqueID){
        $this->db->where("txtUniqueID" , $uniqueID);
        $this->db->from($this->table_verifikasi);
        $data = $this->db->get();
        return $data->row_array();
    }   


    //// Get List Finger

    function getDataFingerPasien($limit="" , $offset = ""){
        $this->db->from("fingerspot_finger");
        if(!empty($limit)){
            $this->db->limit($limit);
        }

        if(!empty($offset)){
            $this->db->limit($offset);
        }

        $retVal = $this->db->get();
        return $retVal->result_array();
        
    }

    function getCountDataFingerPasien(){
        $this->db->from("fingerspot_finger");
        return $this->db->count_all_results();
    }

    function getDataFingerPegawai($limit="" , $offset = ""){
        $this->db->from("mabsensi_pegawai");
        if(!empty($limit)){
            $this->db->limit($limit);
        }

        if(!empty($offset)){
            $this->db->limit($offset);
        }

        $retVal = $this->db->get();
        return $retVal->result_array();        
    }

    function getCountDataFingerPegawai(){
        $this->db->from("mabsensi_pegawai");
        return $this->db->count_all_results();
    }

    function getUpdateLogFingerPasien($date , $offset ="" , $limit = ""){
        $this->db->from("log_finger_pasien");
        if(!empty($limit)){
            $this->db->limit($limit);
        }

        if(!empty($offset)){
            $this->db->limit($offset);
        }

        $this->db->where("DATE(created_date)" , $date);
        $retVal = $this->db->get();
        return $retVal->result_array();        
    }

    function getUpdateLogFingerPegawai($date , $offset ="" , $limit = ""){
        $this->db->from("log_finger_pegawai");
        if(!empty($limit)){
            $this->db->limit($limit);
        }

        if(!empty($offset)){
            $this->db->limit($offset);
        }

        $this->db->where("DATE(created_date)" , $date);
        $retVal = $this->db->get();
        return $retVal->result_array();        
    }
	
}

