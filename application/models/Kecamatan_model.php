<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kecamatan_Model extends MY_Model {

	var $table_profil = "kecamatan";
	var $primary_key = "intIdUsers";
    
    public function __construct(){
        parent::__construct();
        
    }
	
	public function getDataKecamatan($id, $select = '*'){
		$this->db->select($select);
		$this->db->where("IDKabupaten" , $id);		
		$this->db->from($this->table_profil);
		$data = $this->db->get();
        return $data->result_array();
	}
	public function getDataKabupaten($id, $select = '*'){
		$this->db->select($select);
		$this->db->where("IDProvinsi" , $id);		
		$this->db->from('kabupaten');
		$data = $this->db->get();
        return $data->result_array();
	}
		public function getDetailKecamatan($id){
		$this->db->select('*');
		$this->db->where("IDKecamatan" , $id);		
		$this->db->from($this->table_profil);
		$data = $this->db->get();
        return $data->row_array();
	}
	
}

