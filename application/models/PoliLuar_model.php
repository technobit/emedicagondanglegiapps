<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PoliLuar_model extends CI_Model {

	var $table = "data_poli_luar";
	var $primary_key = "intIDPoliLuar";
    
    public function __construct(){
        parent::__construct();
        
    }
    
	/// For List Data / Option Select 2
    public function getListData(){
		$this->db->from($this->table);
		$this->db->where("bitStatus" , 1);	
        
        $data = $this->db->get();
        return $data->result_array();
    }
    
	/// For Index Data
    public function getData(){
		
        $this->db->select($this->table.'.*');
		$this->db->from($this->table);
        $data = $this->db->get();
        return $data->result_array();
    }
	
	public function getDetail($id){
		$this->db->where($this->primary_key , $id);
		$query = $this->db->get($this->table,1);
		$resVal = "";
		if($query->num_rows() > 0){
			$resVal = $query->row_array();	
		}else{
			$resVal = false;
		}
		return $resVal;
	}

    public function saveUpdate($arrData = array() , $id=""){
        
        if(!empty($id)){
            $this->db->where($this->primary_key , $id);
		    $query = $this->db->update($this->table, $arrData);
        }else{
            $this->db->set($arrData);
            $query = $this->db->insert($this->table);
            $id = $this->db->insert_id();
        }
        $retVal['status'] = $query;
        $retVal['message'] = $query==true ? "Data Berhasil Di Simpan" : "Data Gagal Di Simpan" ;
        $retVal['id'] = $this->db->insert_id();
        return $retVal;
    }
	
	public function delete($id){
		$this->db->where($this->primary_key , $id);
		$q = $this->db->delete($this->table);
		
		if(!$q){
			$retVal['message'] = "Data Gagal Di Hapus";
			$retVal['status'] = false;
		}else{
			$retVal['message'] = "Data Berhasil Di Hapus";
			$retVal['status'] = true;
		}
		
		return $retVal;
	}

	public function checkPoliLuar($txtNamaPoli){
		$this->db->where("txtPoliLuar" , $txtNamaPoli);
		$this->db->from($this->table);
		$ret = $this->db->get();
		return $ret->row_array();
	}
	
}

