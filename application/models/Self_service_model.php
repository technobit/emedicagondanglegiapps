<?php
class Self_service_model extends CI_Model{

    var $table = "logabsensi_pegawai";
    var $primary_key = "intIDAbsensi";
    
    function __construct() { 
        parent::__construct();
    }

    function insertUpdate($arrData , $id){
        
        if(!empty($id)) { 
            $this->db->where($this->primary_key , $id);
		    $query = $this->db->update($this->table, $arrData);
            $message = !$query ? "Data Berhasil Di Simpan" : "Data Berhasil Di Simpan";
            
        }else{
            $this->db->set($arrData);
            $query = $this->db->insert($this->table);
            $message = !$query ? "Data Gagal Di Simpan" : "Data Berhasil Di Simpan";
            $id = $this->db->insert_id();
        }
        $retVal =array();
        $retVal['status'] = $query;
        $retVal['message'] = $message;
        $retVal['id'] = $arrData['intIDPegawai'];
        return $retVal;
    }

    function checkPresensiPegawai($intIdPegawai , $date){
        $this->db->from($this->table);
        $this->db->join("pegawai" , "pegawai.intIdPegawai = logabsensi_pegawai.intIDPegawai");
        $this->db->where("logabsensi_pegawai.intIDPegawai" , $intIdPegawai);
        $this->db->like("logabsensi_pegawai.dtAbsensiMasuk" , $date);
        $data = $this->db->get();
        return $data->row_array();
    }

    function getJamByIntDay($intDay) {
        $this->db->where("intDay" , $intDay);
        $ret = $this->db->get("mabsensi_hari");
        return $ret->row_array();
    }

}