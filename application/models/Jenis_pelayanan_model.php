<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Jenis_pelayanan_model extends CI_Model {

	var $table = "jenis_pelayanan";
	var $primary_key = "intIdJenisPelayanan";
    
    public function __construct(){
        parent::__construct();
        
    }
	
	public function getListData(){
		$this->db->select($this->table.'.* ');
		$this->db->from($this->table);
		$this->db->where("bitStatusJenisPelayanan" , 1);
		$data = $this->db->get();
        return $data->result_array();
	}
    
    public function getDataIndex($offset = 0 , $limit = 10, $search = ""){
		
		if(!empty($search)){
			$this->db->where($search);
		}
        
        $this->db->select($this->table.'.* ');
		$this->db->from($this->table);
		$this->db->limit($limit);
		$this->db->offset($offset);
        $data = $this->db->get();
        return $data->result_array();
    }

    public function getCountDataIndex($search=""){
    	if(!empty($search)){
			$this->db->where($search);
		}
		$this->db->select($this->table.'.* ');	
		$this->db->from($this->table);
		$data = $this->db->count_all_results();	
        return $data;
    }
	
	public function getDetail($id){
		$this->db->where($this->primary_key , $id);
		$query = $this->db->get($this->table,1);
		$resVal = "";
		if($query->num_rows() > 0){
			$resVal = $query->row_array();	
		}else{
			$resVal = false;
		}
		return $resVal;
	}
	
	public function saveData($arrData = array(),$debug=false){
		$this->db->set($arrData);
		if($debug){
			$retVal = $this->db->get_compiled_insert($this->table);
		}else{
			$res = $this->db->insert($this->table);
			if(!$res){
				$retVal['error_stat'] = "Failed To Insert";
				$retVal['status'] = false;
			}else{
				$retVal['error_stat'] = "Success To Insert";
				$retVal['status'] = true;
				$retVal['id'] = $this->db->insert_id();
			}
		}
		return $retVal;
	}
	
	public function update($array , $id){
		$this->db->where($this->primary_key , $id);
		$query = $this->db->update($this->table, $array);
		if(!$query){
			
			$retVal['error_stat'] = "Failed To Insert";
			$retVal['status'] = false;
		}else{
			$retVal['error_stat'] = "Success To Update";
			$retVal['status'] = true;
			$retVal['id'] = $id;
		}
		return $retVal;
	}
	
	public function delete($id){
		$this->db->where($this->primary_key , $id);
		$q = $this->db->delete($this->table);
		
		if(!$q){
			$retVal['error_stat'] = "Failed To Delete";
			$retVal['status'] = false;
		}else{
			$retVal['error_stat'] = "Success To Delete";
			$retVal['status'] = true;
		}	
		return $retVal;
	}
}