<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rekam_medis_diagnosa_m extends CI_Model {
	
	/// Master Identifier
	var $table = "rekam_medis_diagnosa_lain";
	var $primary_key = "intIdRekamMedisDiagnosaLain";
	var $secondar_key = "txtIdRekmedDetail";
    
    public function __construct(){
        parent::__construct();
    }
	
	/// Riwayat Kehamilan
	public function saveData($array){
		$query = false;
		
		$this->db->set($array);
		$query = $this->db->insert($this->table);
		
		$retVal = array();
		if(!$query){
			$retVal['message'] = "Data Gagal Di perbarui";
			$retVal['status'] = false;
		}else{
			$retVal['message'] = "Data Berhasil Di Perbarui";
			$retVal['status'] = true;
		}
		
		return $retVal;
	}

    public function checkDataDiagnosa($idRekamMedisDetail , $jenisDiagnosa){
        $this->db->select("*");
		$this->db->from($this->table);
        $this->db->where($this->secondar_key , $idRekamMedisDetail);
        $this->db->where("bitJenisDiagnosa" , $jenisDiagnosa);
        $data = $this->db->count_all_results();
		return $data;
    }
	
	public function delete($id , $jenisDiagnosa){
		$this->db->where($this->secondar_key , $id);
        $this->db->where("bitJenisDiagnosa" , $jenisDiagnosa);
		$q = $this->db->delete($this->table);
		
		if(!$q){
			$retVal['message'] = "Data Gagal Di Hapus";
			$retVal['status'] = false;
		}else{
			$retVal['message'] = "Data Berhasil Di Hapus";
			$retVal['status'] = true;
		}
		return $retVal;
	}
	
	public function getDetail($idRekamMedisDetail , $jenisDiagnosa){
		$this->db->select("
						  data_penyakit.intIdPenyakit as id,
						  data_penyakit.txtCategory,
						  data_penyakit.txtSubCategory,
						  data_penyakit.txtIndonesianName,
						  rekam_medis_diagnosa_lain.txtDetailDiagnosa
						  ");
		$this->db->from($this->table);
        $this->db->join("data_penyakit" , "rekam_medis_diagnosa_lain.intIdDiagnosaPenyakit = data_penyakit.intIdPenyakit","left");
		$this->db->where($this->secondar_key , $idRekamMedisDetail);
        $this->db->where("bitJenisDiagnosa" , $jenisDiagnosa);
		$data = $this->db->get();
		return $data->result_array();
	}
	
	
    
    
}