<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Apotik_model extends CI_Model {

	var $table = "data_apotik";
	var $primary_key = "";
    
    public function __construct(){
        parent::__construct();
        
    }

    public function findByKey($key) {
        return $this->db->select('obat_apotik.*, obat_apotik.stok as stok_apotik, obat.*, kemasan.kemasan')
        ->like('kode_obat', $key)
        ->or_like('nama_obat', $key)
        ->join('obat', 'obat_apotik.id_obat = obat.id')
        ->join('kemasan', 'kemasan.id = obat.id_kemasan')
        ->get('obat_apotik');
    }

}
?>