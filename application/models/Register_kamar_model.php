<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Register_kamar_model extends CI_Model {

	var $table = "register_kamar";
    var $table_kamar = "kamar";
	var $primary_key = "intIdRegisterKamar";
    var $secondary_key = "txtIdKunjungan";
    
    public function __construct(){
        parent::__construct();
        
    }

    function getListDataKamar(){
        $this->db->select("kamar.txtKamar, 
                           kamar.intIdKamar,
                           kelas_kamar.txtKelasKamar");
        $this->db->from($this->table_kamar);
        $this->db->where("bitStatusKamar" , 1);
        $this->db->join("kelas_kamar" , "kamar.intIdKelasKamar = kelas_kamar.intIdKelasKamar" , "left");
        $data = $this->db->get();
        return $data->result_array();
    }

    public function saveData($arrData = array(), $id=""){
		$retVal = array();
        $checkKunjungan = $this->getCountKunjungan($id);
        if(!empty($checkKunjungan)){
            $this->db->where($this->secondary_key , $id);
		    $res = $this->db->update($this->table, $arrData);
        }else{
            $this->db->set($arrData);
            $res = $this->db->insert($this->table);
        }
        $status = $res;
        $message = $status==true ? "Data Berhasil Di Simpan" : "Data Gagal Di Simpan";
        $retVal['message'] = $message;
        $retVal['status'] = $status;
        ///$retVal['id'] = $id;
        return $retVal;
	}

    function getCountKunjungan($txtIdKunjungan){
        $this->db->where("txtIdKunjungan" , $txtIdKunjungan);
        $this->db->from("register_kamar");
        $jumlah = $this->db->count_all_results();
        return $jumlah;
    }

    function updateData($arrData , $id){
        $this->db->where($this->primary_key , $id);
		$query = $this->db->update($this->table, $arrData);
		if(!$query){	
			$retVal['error_stat'] = "Data Gagal Di perbarui";
			$retVal['status'] = false;
		}else{
			$retVal['error_stat'] = "Data Berhasil Di Perbarui";
			$retVal['status'] = true;
			$retVal['id'] = $id;
		}
		return $retVal;
    }




}   