<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Panggilan_model extends CI_Model{


    var $table = 'mpanggilanantrian';
    var $primary_key = 'intIDPanggilanAntrian';

    public function __construct(){
        parent::__construct();
    }

    public function insertData($arrData){
        $res = $this->db->insert($this->table , $arrData);
        $retVal = array();
        $retVal['status'] = $res;
        $retVal['message'] = $res==true ? "Data Berhasil Di Simpan" : "Data Gagal Di Simpan";
        $retVal['id'] = $this->db->insert_id();
        return $retVal;
    }

}