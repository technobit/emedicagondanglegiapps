<?php 
class Rekapitulasi_model extends CI_Model{

    var $table = "register_bpjs";
    var $primary_key = "intIdRegisterBPJS";
    var $secondery_key = "txtIdKunjungan";
    function __construct(){
        parent::__construct();
    }

    function getDataRekapLoket($idPelayanan , $startDate , $endDate , $offset , $limit){
        $startDate .= " 00:00:00";
		$endDate .= " 23:59:00";
		$this->db->select("register.txtIdPasien,
						   register.txtIdKunjungan,
						   register.txtUsiaPasienKunjungan,
						   register.bitIsBaru,
						   register.intUsiaPasienHari,
						   register.bitStatusKunjungan,
						   register.bitIsPoli,
						   register.txtKeteranganKunjungan,
						   pasien.txtNamaPasien,
						   pasien.charJkPasien,
						   pasien.txtNoAnggota,
						   pasien.txtTtdPasien,
						   pasien.txtWilayahPasien,
						   pasien.intIdKelurahan,
						   kelurahan.Nama as NamaKelurahan,
						   jaminan_kesehatan.txtNamaJaminan as txtJaminanKesehatan,
						   pelayanan.txtNama as txtNamaPelayanan,
						   ");
		$this->db->from("register");
		$this->db->where_in("register.bitIsPoli" , 3);
		$this->db->where_in("register.bitStatusKunjungan" , array(1 , 4));
		$this->db->join('pasien', 'pasien.txtIdPasien = register.txtIdPasien');
		$this->db->join('pelayanan', 'pelayanan.IntIdPelayanan = register.IntIdPelayanan');
		$this->db->join('jaminan_kesehatan', 'register.intIdJaminanKesehatan = jaminan_kesehatan.intIdJaminanKesehatan' , 'left');
		$this->db->join('kelurahan', 'kelurahan.IDKelurahan = pasien.intIdKelurahan', 'left');
		if(!empty($idPelayanan)){
			$this->db->where("register.intIdPelayanan" , $idPelayanan);
		}

		$this->db->where("register.dtTanggalKunjungan BETWEEN '".$startDate."' AND '".$endDate."'");
		if($limit!='-1'){
			$this->db->offset($offset);
			$this->db->limit($limit);
		}

		$this->db->group_by("register.txtIdKunjungan");
		$data = $this->db->get();
		
        return $data->result_array();
	
    }

    public function getCountDataRekapLoket($idPelayanan , $startDate , $endDate ){
        $startDate .= " 00:00:00";
		$endDate .= " 23:59:00";
		
		$this->db->from("register");
		$this->db->where("register.bitIsPoli" , 3);
		$this->db->where_in("register.bitStatusKunjungan" , array(1 , 4));
		$this->db->join('pasien', 'pasien.txtIdPasien = register.txtIdPasien');
		$this->db->join('pelayanan', 'pelayanan.IntIdPelayanan = register.IntIdPelayanan');
		$this->db->join('jaminan_kesehatan', 'register.intIdJaminanKesehatan = jaminan_kesehatan.intIdJaminanKesehatan' , 'left');
		$this->db->join('kelurahan', 'kelurahan.IDKelurahan = pasien.intIdKelurahan', 'left');
		if(!empty($idPelayanan)){
			$this->db->where("register.intIdPelayanan" , $idPelayanan);
		}

		$this->db->where("register.dtTanggalKunjungan BETWEEN '".$startDate."' AND '".$endDate."'");
		$this->db->group_by("register.txtIdKunjungan");
		$data = $this->db->count_all_results();
        return $data;
    }

	public function getJumlahRekapLoket($idPelayanan , $startDate , $endDate){
		$startDate .= " 00:00:00";
		$endDate .= " 23:59:00";
		$strQuery = "";
		if(!empty($idPelayanan)){
			$strQuery = " AND p.intIdPelayanan = '".$idPelayanan."'";
		}
		$query = "
		SELECT p.`txtNama`, 
		(SELECT COUNT(R1.`txtIdKunjungan`) FROM register R1 WHERE R1.dtTanggalKunjungan BETWEEN '".$startDate."' AND '".$endDate."'  
		AND R1.intIdPelayanan = p.`intIdPelayanan` AND R1.bitIsBaru = 1) AS jumlah_kunjungan_baru,
		(SELECT COUNT(R1.`txtIdKunjungan`) FROM register R1 WHERE R1.dtTanggalKunjungan BETWEEN '".$startDate."' AND '".$endDate."'  
		AND R1.intIdPelayanan = p.`intIdPelayanan` AND R1.bitIsBaru = 0) AS jumlah_kunjungan_lama,
		(SELECT COUNT(R1.`txtIdKunjungan`) FROM register R1 
		INNER JOIN pasien pa ON R1.txtIdPasien = pa.txtIdPasien
		WHERE R1.dtTanggalKunjungan BETWEEN '".$startDate."' AND '".$endDate."'
		AND R1.intUsiaPasienHari < 20075
		AND pa.charJkPasien = 'L' 
		AND R1.intIdPelayanan = p.`intIdPelayanan` 
		) AS jumlah_kunjungan_laki_muda,
		(SELECT COUNT(R1.`txtIdKunjungan`) FROM register R1 
		INNER JOIN pasien pa ON R1.txtIdPasien = pa.txtIdPasien
		WHERE R1.dtTanggalKunjungan BETWEEN '".$startDate."' AND '".$endDate."'
		AND R1.intUsiaPasienHari > 20075
		AND pa.charJkPasien = 'L' 
		AND R1.intIdPelayanan = p.`intIdPelayanan` 
		) AS jumlah_kunjungan_laki_tua,
		(SELECT COUNT(R1.`txtIdKunjungan`) FROM register R1 
		INNER JOIN pasien pa ON R1.txtIdPasien = pa.txtIdPasien
		WHERE R1.dtTanggalKunjungan BETWEEN '".$startDate."' AND '".$endDate."'
		AND R1.intUsiaPasienHari < 20075
		AND pa.charJkPasien = 'P' 
		AND R1.intIdPelayanan = p.`intIdPelayanan` 
		) AS jumlah_kunjungan_perempuan_muda,
		(SELECT COUNT(R1.`txtIdKunjungan`) FROM register R1 
		INNER JOIN pasien pa ON R1.txtIdPasien = pa.txtIdPasien
		WHERE R1.dtTanggalKunjungan BETWEEN '".$startDate."' AND '".$endDate."'
		AND R1.intUsiaPasienHari > 20075
		AND pa.charJkPasien = 'P' 
		AND R1.intIdPelayanan = p.`intIdPelayanan` 
		) AS jumlah_kunjungan_perempuan_tua
		FROM pelayanan p
		WHERE p.`intIdJenisPelayanan` != '8' AND p.`intIdJenisPelayanan` != '13'
		".$strQuery."
		";		

		$data = $this->db->query($query);
		return $data->result_array();
	}
	public function getRekapDataTindakan($startDate , $endDate , $idPelayanan, $pernyataan = ""){
		$startDate = $startDate." 00:00:00";
		$endDate = $endDate." 23:59:00";
		$this->db->select("*");
		$this->db->from("register");
		$this->db->join('pasien', 'pasien.txtIdPasien = register.txtIdPasien');
		$this->db->join('rekam_medis_detail', 'rekam_medis_detail.txtIdKunjungan = register.txtIdKunjungan' , 'left');
		$this->db->join('pelayanan', 'pelayanan.IntIdPelayanan = register.IntIdPelayanan');
		$this->db->join('rekam_medis_tindakan', 'rekam_medis_tindakan.txtIdRekmedDetail = rekam_medis_detail.txtIdRekmedDetail');
		$this->db->join('tindakan', 'tindakan.intIdTindakan = rekam_medis_tindakan.intIdTindakan');
		$this->db->where("register.dtTanggalKunjungan BETWEEN '".$startDate."' AND '".$endDate."'");

		if($idPelayanan!=0){
			$this->db->where("register.intIdPelayanan" , $idPelayanan);
		}

		if($pernyataan == 0 ){
			$this->db->where("rekam_medis_detail.bitSetuju" , $pernyataan);
		}else{
			$this->db->where("rekam_medis_detail.bitSetuju" , $pernyataan);
		}

		
		$data = $this->db->get();
	   	return $data->result_array();	
	}

}