<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Keuangan_diluar_m extends Base_m {
    protected $table = 'keuangan_diluar';
    protected $fillable = array('no_bukti', 'tanggal:date', 'total_tagihan', 'total_bayar', 'created_by');
    protected $timestamp = true;

    public function getUangMasuk($tanggal = null) {
        if ($tanggal) {
            $this->db->where('LEFT(tanggal, 10) = "'. $tanggal.'"');
        }
        return $this->db->select_sum('total_tagihan')
        ->get($this->table);
    }

    public function getLayanan($id) {
        return $this->db->select('keuangan_diluar_detail.*, item_keuangan.nama_item')        
        ->join('item_keuangan', 'item_keuangan.id = keuangan_diluar_detail.id_item_keuangan')        
        ->where('id_keuangan_diluar', $id)
        ->get('keuangan_diluar_detail');
    }

    public function detailKeuangan($id){
        return $this->db->select('keuangan_diluar.* , pegawai.*')
        ->join("pegawai" , "keuangan_diluar.created_by = pegawai.intIdPegawai" , "left")                
        ->where('id', $id)
        ->get('keuangan_diluar');
    }

    public function createKeuangan($record) {         
        $record['no_bukti'] = $this->generateID();           
    	$this->insert($record);
    	$id_keuangan = $this->db->insert_id();
    	foreach($record['layanan'] as $row => $val) {
            $this->db->insert('keuangan_diluar_detail', array(
                'id_keuangan_diluar' => $id_keuangan,
                'id_item_keuangan' => $val['id'],
                'jasa_sarana' => $val['jasa_sarana'],
                'jasa_pelayanan' => $val['jasa_pelayanan'],
                'jumlah' => $val['jml']
            ));
        }
    	return true;
    }

    public function updateKeuangan($id, $record) {            
        $this->deleteKeuangan($id);
        $this->insert($record);
        $id_keuangan = $this->db->insert_id();
        foreach($record['layanan'] as $row => $val) {
            $this->db->insert('keuangan_diluar_detail', array(
                'id_keuangan_diluar' => $id_keuangan,
                'id_item_keuangan' => $val['id'],
                'jasa_sarana' => $val['jasa_sarana'],
                'jasa_pelayanan' => $val['jasa_pelayanan'],
                'jumlah' => $val['jml']
            ));
        }
        return true;
    }

    public function deleteKeuangan($id) {        
        $this->db->where('id_keuangan_diluar', $id)->delete('keuangan_diluar_detail');
        $this->db->where('id', $id)->delete('keuangan_diluar');
    }

    public function generateID() {
        $prefix = 'PD/'. date('m') . '/' . date('Y') . '/';
        $digit = '000';
        $lastID =  $this->db->select_max('no_bukti')
        ->where('left(no_bukti,'.(strlen($prefix)).')', $prefix)
        ->get($this->table)->row()->no_bukti;
        if ($lastID) {            
            $counter = substr($lastID, -strlen($digit)) + 1;
            return $prefix.substr($digit.$counter, -strlen($digit));
        } else {
            return $prefix.substr($digit.'1', -strlen($digit));
        }
    }

}