<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Kamar_model extends CI_Model {

	var $table = "kamar";
    var $table_kelas = "kelas_kamar";
	var $primary_key = "intIdKamar";
    var $secondary_key = "intIdKelasKamar";
    
    public function __construct(){
        parent::__construct();
        
    }
    //// Kelas Kamar

    //// Kamar
    function getListDataKamar(){
        $this->db->select("kamar.txtKamar, 
                           kamar.intIdKamar,
                           kelas_kamar.txtKelasKamar");
        $this->db->from($this->table);
        $this->db->where("bitStatusKamar" , 1);
        $this->db->join("kelas_kamar" , "kamar.intIdKelasKamar = kelas_kamar.intIdKelasKamar" , "left");
        $data = $this->db->get();
        return $data->result_array();
    }

    public function getDataKelas(){
		
        $this->db->select($this->table_kelas.'.*');
		$this->db->from($this->table_kelas);
        $data = $this->db->get();
        return $data->result_array();
    }

	public function getDataListKelas(){
		$this->db->select($this->table_kelas.'.*');
		$this->db->from($this->table_kelas);
		$this->db->where("bitStatusKelasKamar" , 1);
        $data = $this->db->get();
        return $data->result_array();
	}

    public function saveDataKelas($arrData = array(),$id = ""){
		
        if(!empty($id)){
            $this->db->where($this->secondary_key , $id);
		    $res = $this->db->update($this->table_kelas, $arrData);
        }else{
            $this->db->set($arrData);
		    $res = $this->db->insert($this->table_kelas);
        }
		
		if(!$res){
			$retVal['error_stat'] = "Data Gagal Di Simpan";
			$retVal['status'] = false;
		}else{
			$retVal['error_stat'] = "Data Berhasil Di Simpan";
			$retVal['status'] = true;
			$retVal['id'] = $this->db->insert_id();
		}
		return $retVal;
	}

    public function deleteKelas($id){
		$this->db->where($this->secondary_key , $id);
		$q = $this->db->delete($this->table_kelas);
		
		if(!$q){
			$retVal['error_stat'] = "Data Gagal Di Hapus";
			$retVal['status'] = false;
		}else{
			$retVal['error_stat'] = "Data Berhasil Di Hapus";
			$retVal['status'] = true;
		}
		
		return $retVal;
	}

    public function getDetailKelas($id){
		$this->db->where($this->secondary_key , $id);
		$query = $this->db->get($this->table_kelas,1);
		$resVal = "";
		if($query->num_rows() > 0){
			$resVal = $query->row_array();	
		}else{
			$resVal = false;
		}
		return $resVal;
	}

    public function getDataKamar($intIdKelas = ""){
		
        $this->db->select('*');
		$this->db->from($this->table);
		$this->db->join("kelas_kamar" , "kelas_kamar.intIdKelasKamar = kamar.intIdKelasKamar");
        if(!empty($intIdKelas)){
            $this->db->where("kelas_kamar.intIdKelasKamar" , $intIdKelas);
        }
        $data = $this->db->get();
        return $data->result_array();
    }

    public function saveDataKamar($arrData = array(),$id = ""){
		
        if(!empty($id)){
            $this->db->where($this->primary_key , $id);
		    $res = $this->db->update($this->table, $arrData);
        }else{
            $this->db->set($arrData);
		    $res = $this->db->insert($this->table);
        }
		
		if(!$res){
			$retVal['error_stat'] = "Data Gagal Di Simpan";
			$retVal['status'] = false;
		}else{
			$retVal['error_stat'] = "Data Berhasil Di Simpan";
			$retVal['status'] = true;
			$retVal['id'] = $this->db->insert_id();
		}
		return $retVal;
	}

    public function deleteKamar($id){
		$this->db->where($this->primary_key , $id);
		$q = $this->db->delete($this->table);
		
		if(!$q){
			$retVal['error_stat'] = "Data Gagal Di Hapus";
			$retVal['status'] = false;
		}else{
			$retVal['error_stat'] = "Data Berhasil Di Hapus";
			$retVal['status'] = true;
		}
		
		return $retVal;
	}

    public function getDetailKamar($id){
		$this->db->where($this->primary_key , $id);
		$query = $this->db->get($this->table,1);
		$resVal = "";
		if($query->num_rows() > 0){
			$resVal = $query->row_array();	
		}else{
			$resVal = false;
		}
		return $resVal;
	}

}