<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jaminan_Kesehatan_model extends CI_Model {

	var $table = "jaminan_kesehatan";
	var $primary_key = "intIdJaminanKesehatan";
    
    public function __construct(){
        parent::__construct();
        
    }
    
    public function getAllDataIndex(){
		
        $this->db->select($this->table.'.* ');
		$this->db->from($this->table);
		$this->db->where($search);
        $data = $this->db->get();
        return $data->result_array();
    }
	
	public function getListDataJaminan(){
        $this->db->select('intIdJaminanKesehatan, txtNamaJaminan ,bitStatusJaminan');
		$this->db->from($this->table);
		$this->db->where('bitStatusJaminan' , '1');
        $data = $this->db->get();
		$resVal = $data->result_array();
		$arrVal = array();
		foreach($resVal as $val){
			$arrVal[$val['intIdJaminanKesehatan']] = $val['txtNamaJaminan'];
		}
        return $arrVal;
    }

    public function getCountDataIndex($search=""){
    	if(!empty($search)){
			$this->db->where($search);
		}
		$this->db->select($this->table.'.* ');	
		$this->db->from($this->table);
		$data = $this->db->count_all_results();	
        return $data;
    }
	
	
	public function getDetail($id){
		$this->db->where($this->primary_key , $id);
		$query = $this->db->get($this->table,1);
		$resVal = "";
		if($query->num_rows() > 0){
			$resVal = $query->result_array();	
		}else{
			$resVal = false;
		}
		return $resVal;
	}	
	
	public function saveData($arrData = array(),$debug=false){
		
		$this->db->set($arrData);
		if($debug){
			$retVal = $this->db->get_compiled_insert($this->table);
		}else{
			$res = $this->db->insert($this->table);
			if(!$res){
				$retVal['error_stat'] = "Failed To Insert";
				$retVal['status'] = false;
			}else{
				$retVal['error_stat'] = "Success To Insert";
				$retVal['status'] = true;
				$retVal['id'] = $this->db->insert_id();
			}
			
		}
		return $retVal;
	}
	
	public function update($array , $id){
		
		$this->db->where($this->primary_key , $id);
		$query = $this->db->update($this->table, $array);
		if(!$query){
			
			$retVal['error_stat'] = "Failed To Insert";
			$retVal['status'] = false;
		}else{
			$retVal['error_stat'] = "Success To Update";
			$retVal['status'] = true;
			$retVal['id'] = $id;
		}
		
		return $retVal;
	}
	
	public function delete($id){
		$this->db->where($this->primary_key , $id);
		$q = $this->db->delete($this->table);
		
		if(!$q){
			$retVal['error_stat'] = "Failed To Delete";
			$retVal['status'] = false;
		}else{
			$retVal['error_stat'] = "Success To Delete";
			$retVal['status'] = true;
		}
		
		return $retVal;
	}

	public function find($id) {
		$this->db->where($this->primary_key , $id);
		$query = $this->db->get($this->table,1)->row();
		return $query;
	}
	
}

