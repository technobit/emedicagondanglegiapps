<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rekam_medis_obat_inap extends CI_Model {
	
	/// Master Identifier
	var $table = "rekam_medis_obat_inap";
	var $primary_key = "intIdRekmedObat";
	var $secondar_key = "intIdRekamMedisRawatInap";
    
    public function __construct(){
        parent::__construct();
    }
	
	/// Riwayat Kehamilan
	public function getDataHistory($offset , $limit , $intIdRekamMedisRawatInap){
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->where($this->secondar_key , $intIdRekamMedisRawatInap);
		$this->db->limit($limit);
		$this->db->offset($offset);
        $this->db->order_by("dtTanggal" , "DESC");
		$data = $this->db->get();
		return $data->result_array();
	}
    
	public function getCountDataHistory($intIdRekamMedisRawatInap){
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->where($this->secondar_key , $intIdRekamMedisRawatInap);
		$data = $this->db->count_all_results();
		return $data;
	}
	
	public function saveUpdate($array , $id){
		$query = false;
		if(empty($id)){
			$this->db->set($array);
			$query = $this->db->insert($this->table);
			$id = $this->db->insert_id();
		}else{
			$this->db->where($this->primary_key , $id);
			$query = $this->db->update($this->table, $array);
		}
		$retVal = array();
		if(!$query){
			$retVal['message'] = "Data Gagal Di perbarui";
			$retVal['status'] = false;
			$retVal['id'] = 0;
		}else{
			$retVal['message'] = "Data Berhasil Di Perbarui";
			$retVal['status'] = true;
			$retVal['id'] = $id;
		}
		
		return $retVal;
	}
	
	public function delete($id){
		$this->db->where($this->primary_key , $id);
		$q = $this->db->delete($this->table);
		
		if(!$q){
			$retVal['message'] = "Data Gagal Di Hapus";
			$retVal['status'] = false;
		}else{
			$retVal['message'] = "Data Berhasil Di Hapus";
			$retVal['status'] = true;
		}
		return $retVal;
	}
	
	public function getDetail($id){
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->where($this->primary_key , $id);
		$data = $this->db->get();
		return $data->row_array();
	}
	
	
    
    
}