<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Keuangan_rawat_jalan_m extends Base_m {

    protected $table = 'keuangan_rawat_jalan';
    protected $fillable = array('id', 'id_kunjungan', 'total_tagihan', 'total_bayar', 'tgl_bayar', 'bitBebasBiaya','created_by');
    protected $timestamp = true;

    public function getUangMasuk($tanggal_awal = null, $tanggal_akhir = null) {
        if ($tanggal_awal) {
            $this->db->where('LEFT(register.dtTanggalKunjungan, 10) >= "'. $tanggal_awal.'"');
        }
        if ($tanggal_akhir) {
            $this->db->where('LEFT(register.dtTanggalKunjungan, 10) <= "'. $tanggal_akhir.'"');
        }
        return $this->db->select_sum('total_tagihan')
        ->join('register', 'register.txtIdKunjungan = '.$this->table.'.id_kunjungan')
        ->get($this->table);
    }

    public function getAntrian($tanggal_awal = null, $tanggal_akhir = null) {
        if ($tanggal_awal) {
            $this->db->where('LEFT(register.dtTanggalKunjungan, 10) >= "'. $tanggal_awal.'"');
        }
        if ($tanggal_akhir) {
            $this->db->where('LEFT(register.dtTanggalKunjungan, 10) <= "'. $tanggal_akhir.'"');
        }
        return $this->db->select('count(*) as total_antrian')
        ->join('register', 'register.txtIdKunjungan = '.$this->table.'.id_kunjungan')
        ->where('bitIsPayment', 1)
        ->get($this->table);   
    }

 	public function createKeuangan($id, $layanan) {
 		$this->deleteKeuangan($id);
 		$layanan['id_kunjungan'] = $id;
 		$this->insert($layanan);
 		foreach($layanan['layanan'] as $row => $val) {
 			$this->db->insert('keuangan_rawat_jalan_detail', array(
 				'id_kunjungan' => $id,
 				'id_item_keuangan' => $val['id'],
 				'jasa_sarana' => $val['jasa_sarana'],
 				'jasa_pelayanan' => $val['jasa_pelayanan'],
 				'jumlah' => $val['jml']
 			));
 		}
 	}    

 	public function getKeuangan($id) {
 		return $this->db->join('item_keuangan b', 'b.id = a.id_item_keuangan')
 		->where('id_kunjungan', $id)
 		->get('keuangan_rawat_jalan_detail a');
 	}

 	public function deleteKeuangan($id) {
 		$this->db->where('id_kunjungan', $id)->delete('keuangan_rawat_jalan');
 		$this->db->where('id_kunjungan', $id)->delete('keuangan_rawat_jalan_detail');
 	}

    public function getTindakan($id) {
        return $this->db->join('rekam_medis_tindakan', 'rekam_medis_detail.txtIdRekmedDetail = rekam_medis_tindakan.txtIdRekmedDetail')            
        ->join('tindakan', 'tindakan.intIdTindakan = rekam_medis_tindakan.intIdTindakan')
        ->where('rekam_medis_detail.txtIdKunjungan', $id)
        ->get('rekam_medis_detail');
    }

    public function getDataKeuangan($tanggal_awal , $tanggal_akhir , $status){
        $this->db->select("register.txtIdPasien,register.txtIdKunjungan,register.txtUsiaPasienKunjungan ,register.intNoAntri, pasien.txtNamaPasien,pasien.txtNoAnggota,pasien.dtTanggalLahir,pelayanan.txtNama as txtNamaPelayanan,register.bitStatusKunjungan,register.bitIsPoli,register.bitIsApotik,register.bitIsPayment, keuangan_rawat_jalan.total_tagihan, pegawai.txtNamaPegawai");
        $this->db->from('register');
        $this->db->join("pasien" , "register.txtIdPasien =  pasien.txtIdPasien" , "left");
        $this->db->join("pelayanan" , "register.intIdPelayanan = pelayanan.intIdPelayanan" , "left");
        $this->db->join("jaminan_kesehatan" , "register.intIdJaminanKesehatan = jaminan_kesehatan.intIdJaminanKesehatan" , "left");        
        $this->db->join("keuangan_rawat_jalan", "keuangan_rawat_jalan.id_kunjungan = register.txtIdKunjungan" , "left");
        $this->db->join('users_profil', 'users_profil.intIdUser = keuangan_rawat_jalan.created_by' , "left");
        $this->db->join('pegawai', 'pegawai.intIdPegawai = users_profil.intIdPegawai' , "left");
        $this->db->where("register.bitIsPayment", $status);
        $this->db->where("left(register.dtTanggalKunjungan, 10)>='".$tanggal_awal."'");
        $this->db->where("left(register.dtTanggalKunjungan, 10)<='".$tanggal_akhir."'");
        $ret = $this->db->get();
        return $ret->result_array();
    }

}