<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Group_model extends CI_Model {

	var $table = "mgroup";
	var $primary_key = "intIdGroup";
    
    public function __construct(){
        parent::__construct();
        
    }

    public function getData(){
        $this->db->select("
                        G.intIdGroup,
                        G.txtGroupName,
                        G.bitGroupType,
                        G.`bitGroupStatus`,
                        (SELECT COUNT(PG.`intIdPegawai`) FROM `pegawai_group` PG WHERE PG.`intIdGroup` = G.intIdGroup) AS jumlah_pegawai
        ");
        $this->db->from("mgroup G");
        $this->db->group_by("G.`intIdGroup`");
        $query = $this->db->get();
        $resVal = $query->result_array();
        return $resVal;
    }

    public function checkDataPasien($intIdPegawai , $intIdGroup){
        $this->db->from("pegawai_group");
        $this->db->where("intIdPegawai" , $intIdPegawai);
        $this->db->where("intIdGroup" , $intIdGroup);
        $query = $this->db->count_all_results();
        $status = $query == 0 ? true : false;
        return $status; 
    }

    public function getDataPasien($id){
        $this->db->select("pegawai.intIdPegawai,pegawai.txtNamaPegawai,pegawai.txtNoHandphone");
        $this->db->from("pegawai");
        $this->db->join("pegawai_group" , "pegawai.intIdPegawai = pegawai_group.intIdPegawai");
        $this->db->where("intIdGroup" , $id);
        $query = $this->db->get();
        $resVal = $query->result_array();
        return $resVal;
    }
    
	public function getDetail($id){
		$this->db->where($this->primary_key , $id);
		$query = $this->db->get($this->table,1);
		$resVal = "";
		if($query->num_rows() > 0){
			$resVal = $query->row_array();	
		}else{
			$resVal = false;
		}
		return $resVal;
	}

    public function saveDataPegawai($arrData){
        $this->db->set($arrData);
        $res = $this->db->insert("pegawai_group");
        $status = $res;
        $message = $status==true ? "Data Berhasil Di Simpan" : "Data Gagal Di Simpan";
        $retVal['error_stat'] = $message;
        $retVal['status'] = $status;
        return $retVal;
    }
	
	public function saveData($arrData = array(),$id = ""){
        if(empty($id)){
            $this->db->set($arrData);
            $res = $this->db->insert($this->table);
            $status = $res;
            $message = $status==true ? "Data Berhasil Di Simpan" : "Data Gagal Di Simpan";
            $id = $this->db->insert_id();
        }else{
            $this->db->where($this->primary_key , $id);    
            $res = $this->db->update($this->table, $arrData);
            $status = $res;
            $message = $status==true ? "Data Berhasil Di Update" : "Data Gagal Di Update";
        }
        $retVal['error_stat'] = $message;
        $retVal['status'] = $status;
        $retVal['id'] = $id;
        return $retVal;
	}
	
	public function delete($id){
		$this->db->where($this->primary_key , $id);
		$q = $this->db->delete($this->table);
		
		if(!$q){
			$retVal['error_stat'] = "Data Gagal Di Hapus";
			$retVal['status'] = false;
		}else{
			$retVal['error_stat'] = "Data Berhasil Di Hapus";
			$retVal['status'] = true;
		}
		
		return $retVal;
	}

    public function deletePegawai($id , $idGroup){
        $this->db->where("intIdPegawai" , $id);
        $this->db->where("intIdGroup" , $idGroup);
		$q = $this->db->delete("pegawai_group");
		
		if(!$q){
			$retVal['error_stat'] = "Data Gagal Di Hapus";
			$retVal['status'] = false;
		}else{
			$retVal['error_stat'] = "Data Berhasil Di Hapus";
			$retVal['status'] = true;
		}
		
		return $retVal;
    }

}
