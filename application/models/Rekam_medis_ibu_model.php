<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rekam_medis_ibu_model extends CI_Model {
	
	/// Master Identifier
	var $table = "rekam_medis";
	
	/// Detail Rekam medis
	var $table_detail = "rekam_medis_detail_ibu";
	var $table_riwayat = "rekam_medis_riwayat_kehamilan_ibu";
	
	var $primary_key = "intIdRekamMedisIbu";
	var $secondar_key = "txtIdRekamMedis";
    
    public function __construct(){
        parent::__construct();
    }
	
	public function getDetailRekamMedisIbu($offset , $limit , $idRekamMedis){
		
		$this->db->select("*");
		$this->db->from($this->table_detail);
		$this->db->where($this->secondar_key , $idRekamMedis);
		$this->db->limit($limit);
		$this->db->offset($offset);
		$data = $this->db->get();
		return $data->result_array();
	}
	
	public function getCountDetailRekamMedisIbu($idRekamMedis){
		$this->db->select("*");
		$this->db->from($this->table_detail);
		$this->db->where($this->secondar_key , $idRekamMedis);
		$data = $this->db->count_all_results();
		return $data;
	}
    
    /// Detail
	public function SaveUpdate($array , $id){
		$query = false;
		if(empty($id)){
			$this->db->set($array);
			$query = $this->db->insert($this->table_detail);
			$id = $this->db->insert_id();
		}else{
			$this->db->where($this->primary_key , $id);
			$query = $this->db->update($this->table_detail, $array);
		}
		$retVal = array();
		if(!$query){
			$retVal['message'] = "Data Gagal Di perbarui";
			$retVal['status'] = false;
			$retVal['id'] = 0;
		}else{
			$retVal['message'] = "Data Berhasil Di Perbarui";
			$retVal['status'] = true;
			$retVal['id'] = $id;
		}
		
		return $retVal;
	}
	
	public function delete($id){
		$this->db->where($this->primary_key , $id);
		$q = $this->db->delete($this->table_detail);
		
		if(!$q){
			$retVal['error_stat'] = "Data Gagal Di Hapus";
			$retVal['status'] = false;
		}else{
			$retVal['error_stat'] = "Data Berhasil Di Hapus";
			$retVal['status'] = true;
		}
		
		return $retVal;
	}
	
	public function getDetail($id){
		$this->db->select("*");
		$this->db->from($this->table_detail);
		$this->db->where($this->secondar_key , $id);
		$data = $this->db->get();
		return $data->row_array();
	}
	
	
	/// Riwayat Kehamilan
	public function getDataRiwayatKehamilan($offset , $limit , $idRekamMedis){
		$this->db->select("*");
		$this->db->from($this->table_riwayat);
		$this->db->where($this->primary_key , $idRekamMedis);
		$this->db->limit($limit);
		$this->db->offset($offset);
		$data = $this->db->get();
		return $data->result_array();
	}
    
	public function getCountDataRiwayatKehamilan($idRekamMedis){
		$this->db->select("*");
		$this->db->from($this->table_riwayat);
		$this->db->where($this->primary_key , $idRekamMedis);
		$data = $this->db->count_all_results();
		return $data;
	}
	
	public function saveUpdateRiwayat($array , $id){
		$query = false;
		if(empty($id)){
			$this->db->set($array);
			$query = $this->db->insert($this->table_riwayat);
			$id = $this->db->insert_id();
		}else{
			$this->db->where("intIdRiwayatKehamilan" , $id);
			$query = $this->db->update($this->table_riwayat, $array);
		}
		$retVal = array();
		if(!$query){
			$retVal['message'] = "Data Gagal Di perbarui";
			$retVal['status'] = false;
			$retVal['id'] = 0;
		}else{
			$retVal['message'] = "Data Berhasil Di Perbarui";
			$retVal['status'] = true;
			$retVal['id'] = $id;
		}
		
		return $retVal;
	}
	
	public function deleteRiwayat($id){
		$this->db->where("intIdRiwayatKehamilan" , $id);
		$q = $this->db->delete($this->table_riwayat);
		
		if(!$q){
			$retVal['error_stat'] = "Data Gagal Di Hapus";
			$retVal['status'] = false;
		}else{
			$retVal['error_stat'] = "Data Berhasil Di Hapus";
			$retVal['status'] = true;
		}
		return $retVal;
	}
	
	public function getDetailRiwayatKehamilan($id){
		$this->db->select("*");
		$this->db->from($this->table_riwayat);
		$this->db->where("intIdRiwayatKehamilan" , $id);
		$data = $this->db->get();
		return $data->row_array();
	}
	
	public function deleteAll($intIdRekamMedisIbu){
		$query = $this->db->query("CALL deleteRekamMedisIbu('".$intIdRekamMedisIbu."')");
		$res = $query->next_result();
		$retVal = array();
		$status = $query->result_id;
		if($status){
			$retVal['message'] = "Data Rekam medis berhasil Di Hapus";
		}else{
			$retVal['message'] = "Data Rekam medis Gagal Di Hapus";
		}
		$retVal['status'] = $status;
		return $retVal;
	}
	
    
    
}