<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kelurahan_Model extends MY_Model {

	var $table_profil = "kelurahan";
	var $primary_key = "IDKelurahan";
    
    public function __construct(){
        parent::__construct();
        
    }
	
	public function getDataKelurahan($id, $select = '*'){
		$this->db->select($select);
		$this->db->where("IDKecamatan" , $id);		
		$this->db->from($this->table_profil);
		$data = $this->db->get();
        return $data->result_array();
	}

	public function getDetailKelurahan($id){
		$this->db->where("IDKelurahan" , $id);
		$this->db->from($this->table_profil);
		$data = $this->db->get();
        return $data->row_array();
	}

	
}

