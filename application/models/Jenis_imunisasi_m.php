<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class jenis_imunisasi_m extends Base_m {
    protected $table = 'jenis_imunisasi';
    protected $fillable = array('jenis_imunisasi');
}