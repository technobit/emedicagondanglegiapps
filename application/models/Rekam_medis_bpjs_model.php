<?php 
class Rekam_medis_bpjs_model extends CI_Model{
	
	
	var $table = "rekam_medis_bpjs";
	
	var $primary_key = "intIdRekamMedisBPJS";
	
	var $secondery_key = "txtIdRekamMedisDetail";
	
	function __construct(){
		
		parent::__construct();
		
	}
	
	
	function insert($arrPost , $txtIdRekamMedisDetail = ""){
		
		$countData = $this->check($txtIdRekamMedisDetail);
		
		if($countData > 0){
			
			$this->db->where($this->secondery_key , $txtIdRekamMedisDetail);
			
			$res = $this->db->update($this->table , $arrPost);
			
		}
		else{
			
			$this->db->set($arrPost);
			
			$res = $this->db->insert($this->table);
			
		}
		
		$retVal['status'] = $res;
		
		$retVal['message'] = !empty($res) ? "Data Berhasil Di Simpan" : "Data Gagal Di Simpan";
		
		return $retVal;
		
	}
	
	
	function check($txtIdRekamMedisDetail){
		
		$this->db->where($this->secondery_key , $txtIdRekamMedisDetail);
		
		$this->db->from($this->table);
		
		$res = $this->db->count_all_results();
		
		return $res;
		
	}
	
	
	function hapus($txtIdRekamMedisDetail){
		
		$this->db->where($this->secondery_key , $txtIdRekamMedisDetail);
		
		$ret = $this->db->delete($this->table);
		
		return $ret;
		
	}
	
	
	function detail($txtIdRekamMedisDetail){
		
		$this->db->where($this->secondery_key , $txtIdRekamMedisDetail);
		
		$this->db->from($this->table);
		
		$res = $this->db->get();
		
		return $res->row_array();
		
	}
	
	
	///	/ Select Data
	function getPesertaBPJS($txtTanggal, $idJenisPelayanan , $jenisKunjungan , $bitStatus){
		
		$dtStart = $txtTanggal. " 00:00:00";
		
		$dtEnd = $txtTanggal. " 23:59:00";
		
		$this->db->select("
            R.dtTanggalKunjungan,
            R.txtIdKunjungan,
            RB.bitStatus,
            P.txtNoAnggota, 
            JK.txtNamaJaminan,
            R.txtNoJaminanKesehatan,
            P.txtNamaPasien,
            PE.txtNama as NamaPelayanan,
            RB.bitStatus,
            RB.txtPoliKegiatan,
        ");
		
		$this->db->from("register_bpjs RB");
		
		$this->db->join("register R" , "R.txtIdKunjungan = RB.txtIdKunjungan");
		
		$this->db->join("pasien P" , "P.txtIdPasien = R.txtIdPasien");
		
		$this->db->join("jaminan_kesehatan JK" , "JK.intIdJaminanKesehatan = R.intIdJaminanKesehatan");
		
		$this->db->join("pelayanan PE" , "R.intIdPelayanan = PE.intIdPelayanan");
		
		
		if(!empty($jenisKunjungan)){
			
			$this->db->where("RB.kunjSakit" , $jenisKunjungan);
			
		}
		
		
		if(!empty($bitStatus)){
			
			$this->db->where("RB.bitStatus" , $bitStatus);
			
		}
		
		
		if((!empty($idJenisPelayanan)) && ($jenisKunjungan!=2)){
			
			$this->db->where("R.intIdPelayanan" , $idJenisPelayanan);
			
		}
		
		$this->db->where("R.dtTanggalKunjungan BETWEEN '".$dtStart."' AND '".$dtEnd."'" );
		
		$this->db->order_by("R.dtTanggalKunjungan DESC, RB.bitStatus ASC");
		
		$resQuery = $this->db->get();
		
		return $resQuery->result_array();
		
	}
	
	
	function getDataRekamMedisBPJS($dtStart , $dtEnd , $idJenisPelayanan , $kdStatusPulang , $bitStatusPelayanan){
		
		$this->db->select("
            R.dtTanggalKunjungan,
            R.txtIdKunjungan,
            RMD.txtIdRekmedDetail,
            P.txtNoAnggota, 
            JK.txtNamaJaminan,
            R.txtNoJaminanKesehatan,
            P.txtNamaPasien,
            PE.txtNama as NamaPelayanan,
            RB.rawatInap,
            RB.bitStatus as statusDaftar,
            RMB.bitStatus as statusKunjungan,
            RMB.kdStatusPulang,
            RMB.kdDiag1,
            RMB.kdDiag2,
            RMB.kdDiag3,
            RMB.txtDiag1,
            RMB.txtDiag2,
            RMB.txtDiag3,
            RB.txtPoliKegiatan,
        ");
		
		$this->db->from("rekam_medis_bpjs RMB");
		
		$this->db->join("rekam_medis_detail RMD" , "RMB.txtIdRekamMedisDetail = RMD.txtIdRekmedDetail");
		
		$this->db->join("register R" , "R.txtIdKunjungan = RMD.txtIdKunjungan");
		
		$this->db->join("register_bpjs RB" , "R.txtIdKunjungan = RB.txtIdKunjungan");
		
		$this->db->join("pasien P" , "P.txtIdPasien = R.txtIdPasien");
		
		$this->db->join("jaminan_kesehatan JK" , "JK.intIdJaminanKesehatan = R.intIdJaminanKesehatan");
		
		$this->db->join("pelayanan PE" , "R.intIdPelayanan = PE.intIdPelayanan");
		
		
		if(!empty($idJenisPelayanan)){
			
			$this->db->where("R.intIdPelayanan" , $idJenisPelayanan);
			
		}
		
		
		if(!empty($kdStatusPulang)){
			
			$this->db->where("RMB.kdStatusPulang" , $kdStatusPulang);
			
		}
		
		
		if(!empty($bitStatusPelayanan)){
			
			$this->db->where("RMB.bitStatus" , $bitStatusPelayanan);
			
		}
		
		
		$this->db->where("R.dtTanggalKunjungan BETWEEN '".$dtStart."' AND '".$dtEnd."'" );
		
		$this->db->order_by("R.dtTanggalKunjungan DESC, RB.bitStatus ASC");
		
		$resQuery = $this->db->get();
		
		return $resQuery->result_array();
		
	}
	
	
}
