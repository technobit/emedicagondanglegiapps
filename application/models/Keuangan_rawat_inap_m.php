<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Keuangan_rawat_inap_m extends Base_m {

    protected $table = 'keuangan_rawat_inap';
    protected $fillable = array('id', 'id_kunjungan', 'total_tagihan', 'total_bayar', 'bitBebasBiaya', 'tgl_bayar', 'created_by');
    protected $timestamp = true;

    public function get_riwayat($id) {
        return $this->db->where('id_kunjungan', $id)->order_by('tgl_bayar', 'asc')->get('keuangan_rawat_inap');
    }

    public function get_keuangan_inap($id){
        return $this->db->where('id', $id)
 		->get('keuangan_rawat_inap');
    }

    public function get_detail_keuangan($id){
        return $this->db->join('item_keuangan b', 'b.id = a.id_item_keuangan')
 		->where('id_keuangan_rawat_inap', $id)
 		->get('keuangan_rawat_inap_detail a');
    }

    public function getUangMasuk($tanggal = null) {
        if ($tanggal) {
            $this->db->where('LEFT(register.dtTanggalKunjungan, 10) = "'. $tanggal.'"');
        }
        return $this->db->select_sum('total_tagihan')
        ->join('register', 'register.txtIdKunjungan = '.$this->table.'.id_kunjungan')
        ->get($this->table);
    }

    public function getAntrian($tanggal = null) {
        if ($tanggal) {
            $this->db->where('LEFT(register.dtTanggalKunjungan, 10) = "'. $tanggal.'"');
        }
        return $this->db->select('count(*) as total_antrian')
        ->join('register', 'register.txtIdKunjungan = '.$this->table.'.id_kunjungan')
        ->where('bitIsPayment', 1)
        ->get($this->table);   
    }

 	public function createKeuangan($id, $layanan) { 		
 		$layanan['id_kunjungan'] = $id;
 		$this->insert($layanan);
        $id_keuangan = $this->db->insert_id();
 		foreach($layanan['layanan'] as $row => $val) {
 			$this->db->insert('keuangan_rawat_inap_detail', array(
                'tgl_tindakan' => $val['tgl_tindakan'],
                'id_keuangan_rawat_inap' => $id_keuangan,
 				'id_kunjungan' => $id,                
 				'id_item_keuangan' => $val['id'],
 				'jasa_sarana' => $val['jasa_sarana'],
 				'jasa_pelayanan' => $val['jasa_pelayanan'],
 				'jumlah' => $val['jml']
 			));
 		}
 	}  

    public function editKeuangan($id, $layanan) {
        $find = $this->find($id);
        $this->deleteKeuangan($id);        
        $layanan['id_kunjungan'] = $find->id_kunjungan;
        $this->insert($layanan);
        $id_keuangan = $this->db->insert_id();
        foreach($layanan['layanan'] as $row => $val) {
            $this->db->insert('keuangan_rawat_inap_detail', array(
                'tgl_tindakan' => $val['tgl_tindakan'],
                'id_keuangan_rawat_inap' => $id_keuangan,
                'id_kunjungan' => $find->id_kunjungan,                
                'id_item_keuangan' => $val['id'],
                'jasa_sarana' => $val['jasa_sarana'],
                'jasa_pelayanan' => $val['jasa_pelayanan'],
                'jumlah' => $val['jml']
            ));
        }
    }

 	public function getKeuangan($id_kunjungan, $id = null) {
        if ($id) {
            $this->db->where('id_keuangan_rawat_inap', $id);
        }
 		return $this->db->join('item_keuangan b', 'b.id = a.id_item_keuangan')
 		->where('id_kunjungan', $id_kunjungan)
 		->get('keuangan_rawat_inap_detail a');
 	}

 	public function deleteKeuangan($id) {
 		$this->db->where('id', $id)->delete('keuangan_rawat_inap');
 		$this->db->where('id_keuangan_rawat_inap', $id)->delete('keuangan_rawat_inap_detail');
 	}

    public function getTindakan($id) {
        return $this->db->join('rekam_medis_tindakan_inap', 'rekam_medis_detail.txtIdRekmedDetail = rekam_medis_tindakan_inap.txtIdRekmedDetail')            
        ->join('tindakan', 'tindakan.intIdTindakan = rekam_medis_tindakan_inap.intIdTindakan')
        ->where('rekam_medis_detail.txtIdKunjungan', $id)
        ->get('rekam_medis_detail');
    }

}