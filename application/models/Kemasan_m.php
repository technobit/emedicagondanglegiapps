<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Kemasan_m extends Base_m {
    protected $table = 'kemasan';
    protected $fillable = array('kemasan');
}