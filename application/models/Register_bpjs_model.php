<?php 
class Register_bpjs_model extends CI_Model{

    var $table = "register_bpjs";
    var $primary_key = "intIdRegisterBPJS";
    var $secondery_key = "txtIdKunjungan";
    function __construct(){
        parent::__construct();
    }

    function insert($arrPost , $txtIdKunjungan = ""){
        if(!empty($txtIdKunjungan)){
            $countData = $this->checkPendaftaran($txtIdKunjungan);
            if($countData > 0){
                $retHapus = $this->hapusPendaftaran($txtIdKunjungan);
            }
        }
        $this->db->set($arrPost);
        $res = $this->db->insert($this->table);
        $retVal['status'] = $res;
        $retVal['message'] = !empty($res) ? "Data Berhasil Di Simpan" : "Data Gagal Di Simpan";
        return $retVal;
    }

    function checkPendaftaran($txtIdKunjungan){
        $this->db->where($this->secondery_key , $txtIdKunjungan);
        $this->db->from($this->table);
        $res = $this->db->count_all_results();
        return $res;
    }

    function hapusPendaftaran($txtIdKunjungan){
        $this->db->where($this->secondery_key , $txtIdKunjungan);
        $ret = $this->db->delete($this->table);
        return $ret;
    }

    function updatePendaftaran($arrPost,$txtIdKunjungan){

        $this->db->where($this->secondery_key , $txtIdKunjungan);
        $res = $this->db->update($this->table , $arrPost);
        $retVal['status'] = $res;
        $retVal['message'] = !empty($res) ? "Data Berhasil Di Simpan" : "Data Gagal Di Simpan";
        return $retVal;
        
    }

    function detail($txtIdKunjungan){
        $this->db->where("register_bpjs.".$this->secondery_key , $txtIdKunjungan);
        $this->db->from($this->table);
        $this->db->join("register" , "register_bpjs.txtIdKunjungan = register.txtIdKunjungan");
        $res = $this->db->get();
        return $res->row_array();
    }


    //// Select Data
    function getPesertaBPJS($txtTanggal, $idJenisPelayanan , $jenisKunjungan , $bitStatus){
        $dtStart = $txtTanggal. " 00:00:00";
        $dtEnd = $txtTanggal. " 23:59:00";
        $this->db->select("
            R.dtTanggalKunjungan,
            R.txtIdKunjungan,
            RB.bitStatus,
            P.txtNoAnggota, 
            JK.txtNamaJaminan,
            R.txtNoJaminanKesehatan,
            P.txtNamaPasien,
            PE.txtNama as NamaPelayanan,
            RB.bitStatus,
            RB.txtPoliKegiatan,
        ");

        $this->db->from("register_bpjs RB");
        $this->db->join("register R" , "R.txtIdKunjungan = RB.txtIdKunjungan");
        $this->db->join("pasien P" , "P.txtIdPasien = R.txtIdPasien");
        $this->db->join("jaminan_kesehatan JK" , "JK.intIdJaminanKesehatan = R.intIdJaminanKesehatan");
        $this->db->join("pelayanan PE" , "R.intIdPelayanan = PE.intIdPelayanan");
        
        if(!empty($jenisKunjungan)){
            $this->db->where("RB.kunjSakit" , $jenisKunjungan);
        }

        if(!empty($bitStatus)){
            $this->db->where("RB.bitStatus" , $bitStatus);
        }

        if((!empty($idJenisPelayanan)) && ($jenisKunjungan!=2)){
            $this->db->where("R.intIdPelayanan" , $idJenisPelayanan);
        }

        $this->db->where("R.dtTanggalKunjungan BETWEEN '".$dtStart."' AND '".$dtEnd."'" );
        $this->db->order_by("R.dtTanggalKunjungan DESC, RB.bitStatus ASC");
        $resQuery = $this->db->get();
        return $resQuery->result_array();
    }

}