<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Grafik_Model extends CI_Model {

	var $table = "register";
	var $primary_key = "txtIdKunjungan";
    
    public function __construct(){
        parent::__construct();    
    }
	public function getJumlahKunjungan($start_Date , $end_Date , $idPelayanan=""){
		$startDate = $start_Date." 00:00:00";
		$endDate = $end_Date." 23:59:00";
		$pelayanan = "";
		if (!empty($idPelayanan)) {
			$pelayanan = "AND pelayanan.intIdJenisPelayanan  = ".$idPelayanan;
		}
		$strQuery = "SELECT pelayanan.intIdJenisPelayanan as jenis, pelayanan.intIdPelayanan as id, pelayanan.txtNama as nama, DATE_FORMAT(register.dtTanggalKunjungan, '%Y-%m-%d') as tanggal,register.dtTanggalKunjungan as tanggal_time
		,(select count(txtIdKunjungan) from register where register.intIdPelayanan = id  AND DATE_FORMAT(register.dtTanggalKunjungan, '%Y-%m-%d') = tanggal) as jumlah
		FROM pelayanan
		JOIN register ON register.intIdPelayanan = pelayanan.intIdPelayanan
		WHERE register.dtTanggalKunjungan BETWEEN '".$startDate."' AND '".$endDate."'
		AND pelayanan.intIdJenisPelayanan  != '8'
		AND pelayanan.intIdJenisPelayanan  != '14'
		".$pelayanan."
		group by tanggal, id
		ORDER BY register.dtTanggalKunjungan DESC
		";
		
		 $data = $this->db->query($strQuery);
	   	 return $data->result_array();	
	}
		
	public function labelKunjungan($start_Date = "" , $end_Date = "" , $idPelayanan=""){
		$startDate = $start_Date." 00:00:00";
		$endDate = $end_Date." 23:59:00";
		$this->db->select("pelayanan.txtNama as nama,DATE_FORMAT(register.dtTanggalKunjungan, '%Y-%m-%d') as tanggal");
		$this->db->from("pelayanan");
		$this->db->join("register" , "register.intIdPelayanan = pelayanan.intIdPelayanan");
		if (!empty($start_Date)) {
			$this->db->where("register.dtTanggalKunjungan BETWEEN '".$startDate."' AND '".$endDate."'");
		
		}
		$this->db->where("pelayanan.intIdJenisPelayanan != '8'");
		if (!empty($idPelayanan)) {
			$this->db->where("pelayanan.intIdJenisPelayanan",$idPelayanan);
		}
		$this->db->order_by('register.dtTanggalKunjungan DESC');
		$this->db->group_by('pelayanan.txtNama');
		$data = $this->db->get();
	   	return $data->result_array();	
	}
	public function getJumlahKunjunganTable($start_Date , $end_Date , $idPelayanan=""){
		$startDate = $start_Date." 00:00:00";
		$endDate = $end_Date." 23:59:00";
		$pelayanan = "";
		if (!empty($idPelayanan)) {
			$pelayanan = "AND pelayanan.intIdJenisPelayanan  = ".$idPelayanan;
		}
		$strQuery = "SELECT DATE_FORMAT(register.dtTanggalKunjungan, '%Y-%m-%d') as tanggal
		,(select count(txtIdKunjungan) from register where register.intIdPelayanan = 1  AND DATE_FORMAT(register.dtTanggalKunjungan, '%Y-%m-%d') = tanggal) as POLI_UMUM
		,(select count(txtIdKunjungan) from register where register.intIdPelayanan = 4  AND DATE_FORMAT(register.dtTanggalKunjungan, '%Y-%m-%d') = tanggal) as POLI_ANAK
		,(select count(txtIdKunjungan) from register where register.intIdPelayanan = 3  AND DATE_FORMAT(register.dtTanggalKunjungan, '%Y-%m-%d') = tanggal) as POLI_GIGI
		,(select count(txtIdKunjungan) from register where register.intIdPelayanan = 7  AND DATE_FORMAT(register.dtTanggalKunjungan, '%Y-%m-%d') = tanggal) as LAB
		,(select count(txtIdKunjungan) from register where register.intIdPelayanan = 9  AND DATE_FORMAT(register.dtTanggalKunjungan, '%Y-%m-%d') = tanggal) as POLI_GIZI
		,(select count(txtIdKunjungan) from register where register.intIdPelayanan = 5  AND DATE_FORMAT(register.dtTanggalKunjungan, '%Y-%m-%d') = tanggal) as POLI_IBU
		,(select count(txtIdKunjungan) from register where register.intIdPelayanan = 6  AND DATE_FORMAT(register.dtTanggalKunjungan, '%Y-%m-%d') = tanggal) as UGD
		,(select count(txtIdKunjungan) from register where register.intIdPelayanan = 8  AND DATE_FORMAT(register.dtTanggalKunjungan, '%Y-%m-%d') = tanggal) as RAWAT_INAP
		,(select count(txtIdKunjungan) from register where register.intIdPelayanan = 42  AND DATE_FORMAT(register.dtTanggalKunjungan, '%Y-%m-%d') = tanggal) as POLI_LANSIA
		FROM pelayanan
		JOIN register ON register.intIdPelayanan = pelayanan.intIdPelayanan
		WHERE register.dtTanggalKunjungan BETWEEN '".$startDate."' AND '".$endDate."'
		AND pelayanan.intIdJenisPelayanan  != '8'
		".$pelayanan."
		group by tanggal
		ORDER BY register.dtTanggalKunjungan DESC
		";
		
		 $data = $this->db->query($strQuery);
	   	 return $data->result_array();	
	}
	public function getJumlahKunjunganDas(){
		
		$strQuery = "SELECT pelayanan.intIdJenisPelayanan as jenis, pelayanan.intIdPelayanan as id, pelayanan.txtNama as nama, DATE_FORMAT(register.dtTanggalKunjungan, '%Y-%m-%d') as tanggal,register.dtTanggalKunjungan as tanggal_time
		,(select count(txtIdKunjungan) from register where register.intIdPelayanan = id  AND DATE_FORMAT(register.dtTanggalKunjungan, '%Y-%m-%d') = tanggal) as jumlah
		FROM pelayanan
		JOIN register ON register.intIdPelayanan = pelayanan.intIdPelayanan
		WHERE register.dtTanggalKunjungan >= curdate() - INTERVAL DAYOFWEEK(curdate())+6 DAY
		AND register.dtTanggalKunjungan < curdate() - INTERVAL DAYOFWEEK(curdate())-1 DAY
		AND pelayanan.intIdJenisPelayanan  != '8'
		AND pelayanan.intIdJenisPelayanan  != '14'
		group by tanggal, id
		ORDER BY register.dtTanggalKunjungan DESC
		";
		
		 $data = $this->db->query($strQuery);
	   	 return $data->result_array();	
	}
		
	public function labelKunjunganDas(){
		
		$this->db->select("pelayanan.txtNama as nama,DATE_FORMAT(register.dtTanggalKunjungan, '%Y-%m-%d') as tanggal");
		$this->db->from("pelayanan");
		$this->db->join("register" , "register.intIdPelayanan = pelayanan.intIdPelayanan");
		$this->db->where("register.dtTanggalKunjungan >= curdate() - INTERVAL DAYOFWEEK(curdate())+6 DAY
		AND register.dtTanggalKunjungan < curdate() - INTERVAL DAYOFWEEK(curdate())-1 DAY");
		$this->db->where("pelayanan.intIdJenisPelayanan != '8'");
		if (!empty($idPelayanan)) {
			$this->db->where("pelayanan.intIdJenisPelayanan",$idPelayanan);
		}
		$this->db->order_by('register.dtTanggalKunjungan DESC');
		$this->db->group_by('pelayanan.txtNama');
		$data = $this->db->get();
	   	return $data->result_array();	
	}
	public function getRekapDataPenyakitGrafik(){
		$this->db->select("DP.txtCategory , DP.txtSubCategory , DP.txtIndonesianName, COUNT(DP.`intIdPenyakit`) AS jumlah");
		$this->db->from("data_penyakit DP");
		$this->db->join("rekam_medis_diagnosa RMDi" , "RMDi.intIdDiagnosaPenyakit = DP.intIdPenyakit");
		$this->db->join("rekam_medis_detail RMD" , "RMD.txtIdRekmedDetail = RMDi.txtIdRekmedDetail");
		$this->db->join("register R" , "R.txtIdKunjungan = RMD.txtIdKunjungan");
		$this->db->where("R.dtTanggalKunjungan >= curdate() - INTERVAL DAYOFWEEK(curdate())+6 DAY
		AND R.dtTanggalKunjungan < curdate() - INTERVAL DAYOFWEEK(curdate())-1 DAY");

		$this->db->limit('10');
		$this->db->group_by("DP.intIdPenyakit");
		$this->db->order_by("jumlah DESC");
		$data = $this->db->get();
		
	   	return $data->result_array();	
	}
	
		public function get($date){
		
		$strQuery = "SELECT 
		(select count(txtIdKunjungan) from register where register.intIdPelayanan = 1  AND DATE_FORMAT(register.dtTanggalKunjungan, '%Y-%m-%d') = '".$date."') as POLI_UMUM
		,(select count(txtIdKunjungan) from register where register.intIdPelayanan = 4  AND DATE_FORMAT(register.dtTanggalKunjungan, '%Y-%m-%d') = '".$date."') as POLI_ANAK
		,(select count(txtIdKunjungan) from register where register.intIdPelayanan = 3  AND DATE_FORMAT(register.dtTanggalKunjungan, '%Y-%m-%d') = '".$date."') as POLI_GIGI
		,(select count(txtIdKunjungan) from register where register.intIdPelayanan = 7  AND DATE_FORMAT(register.dtTanggalKunjungan, '%Y-%m-%d') = '".$date."') as LAB
		,(select count(txtIdKunjungan) from register where register.intIdPelayanan = 9  AND DATE_FORMAT(register.dtTanggalKunjungan, '%Y-%m-%d') = '".$date."') as POLI_GIZI
		,(select count(txtIdKunjungan) from register where register.intIdPelayanan = 5  AND DATE_FORMAT(register.dtTanggalKunjungan, '%Y-%m-%d') = '".$date."') as POLI_IBU
		,(select count(txtIdKunjungan) from register where register.intIdPelayanan = 6  AND DATE_FORMAT(register.dtTanggalKunjungan, '%Y-%m-%d') = '".$date."') as UGD
		,(select count(txtIdKunjungan) from register where register.intIdPelayanan = 8  AND DATE_FORMAT(register.dtTanggalKunjungan, '%Y-%m-%d') = '".$date."') as RAWAT_INAP
		,(select count(txtIdKunjungan) from register where register.intIdPelayanan = 42  AND DATE_FORMAT(register.dtTanggalKunjungan, '%Y-%m-%d') = '".$date."') as POLI_LANSIA
		FROM pelayanan
		group by POLI_UMUM";
		
		 $data = $this->db->query($strQuery);
	   	 return $data->row_array();	
	}
		
		
		
}

