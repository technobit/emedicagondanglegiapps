<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Desa_model extends CI_Model {

	var $table = "desa"; 
	var $primary_key = "id";
	var $foreign_key = "intIdPelayanan";
    
    public function __construct(){
        parent::__construct();
        
    }
    
	/// For List Data / Option Select 2
    public function getListDesa($where = "" , $offset , $limit){
        if($where!=""){
        	$this->db->like("kode_desa" , $where);
            $this->db->or_like("nama_desa" , $where);
        }
        
        $this->db->select('id as id, kode_desa,nama_desa');
		$this->db->join('');
		$this->db->from($this->table);
        $this->db->limit($limit);
        $this->db->offset($offset);
        $data = $this->db->get();
        return $data->result_array();
    }
    
    function getCountListDesa($where = ""){
        if($where==""){
        	$this->db->like("kode_desa" , $where);
            $this->db->or_like("nama_desa" , $where);
        }
        
        $this->db->select($this->table.'.* ');
		$this->db->from($this->table);
        $data = $this->db->count_all_results();
        return $data;
    }
    
	/// For Index Data
    public function getData(){
        $this->db->select(
			'
			desa.id as id_desa,
			pelayanan.intIdPelayanan,
			desa.intIdPelayanan,
			desa.nama_desa,
			kelurahan.Nama as nama_kelurahan,
			desa.keterangan,
			(SELECT COUNT(intIdUser) from pelayanan_akses where intIdPelayanan = pelayanan.intIdPelayanan) as jumlah_petugas
			'
		);
		$this->db->join('pelayanan' , 'pelayanan.intIdPelayanan = desa.intIdPelayanan');
		$this->db->join('kelurahan' , 'kelurahan.IDKelurahan = desa.intIdKelurahan');
		$this->db->from($this->table);
        $data = $this->db->get();
        return $data->result_array();
    }
	
	public function getCountData($search=""){
		
		if(!empty($search)){
			$this->db->like("kode_desa" , $search);
			$this->db->or_like("nama_desa" , $search);
		}
		
        $this->db->select($this->table.'.*');
		$this->db->from($this->table);
        $data = $this->db->count_all_results();
        return $data;
    }
	

    public function getCountDataIndex($search=""){
    	if(!empty($search)){
			$this->db->where($search);
		}
		$this->db->select($this->table.'.* ');	
		$this->db->from($this->table);
		$data = $this->db->count_all_results();	
        return $data;
    }
	
	
	public function getDetail($id){
		
		$this->db->where("pelayanan.".$this->foreign_key , $id);
		$this->db->join('pelayanan' , 'pelayanan.intIdPelayanan = desa.intIdPelayanan');
		$this->db->join('kelurahan' , 'kelurahan.IDKelurahan = desa.intIdKelurahan');
		$query = $this->db->get($this->table);
		$resVal = !empty($query) ? $query->row_array() : false;	
		return $resVal;
	}
	
	public function saveData($arrData = array(),$debug=false){
		
		$this->db->set($arrData);
		if($debug){
			$retVal = $this->db->get_compiled_insert($this->table);
		}else{
			$res = $this->db->insert($this->table);
			if(!$res){
				$retVal['error_stat'] = "Data Gagal Di Simpan";
				$retVal['status'] = false;
			}else{
				$retVal['error_stat'] = "Data Berhasil Di Simpan";
				$retVal['status'] = true;
				$retVal['id'] = $this->db->insert_id();
			}
			
		}
		return $retVal;
	}
	
	public function update($array , $id){
		
		$this->db->where($this->primary_key, $id);
		$query = $this->db->update($this->table, $array);
		if(!$query){
			
			$retVal['error_stat'] = "Data Gagal Di perbarui";
			$retVal['status'] = false;
		}else{
			$retVal['error_stat'] = "Data Berhasil Di Perbarui";
			$retVal['status'] = true;
			$retVal['id'] = $id;
		}
		
		return $retVal;
	}
	
	public function delete($id){
		$this->db->where($this->foreign_key , $id);
		$q = $this->db->delete($this->table);
		
		if(!$q){
			$retVal['error_stat'] = "Data Gagal Di Hapus";
			$retVal['status'] = false;
		}else{
			$retVal['error_stat'] = "Data Berhasil Di Hapus";
			$retVal['status'] = true;
		}
		
		return $retVal;
	}
	
}

