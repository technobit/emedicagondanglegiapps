<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rekam_medis_model extends CI_Model {

	var $table = "rekam_medis";
	var $table_detail = "rekam_medis_detail";
    var $table_detail_gigi = "rekam_medis_gigi";
	var $table_diagnosa = "rekam_medis_diagnosa";
    var $table_tindakan = "rekam_medis_tindakan";
	var $primary_key = "txtIdRekamMedis";
	var $secondar_key = "txtNoRekamMedis";
    
    public function __construct(){
        parent::__construct();
    }
	
	public function getListPasienRekamMedis($offset=0 , $limit=10 , $search = ""){
		
		if(!empty($search)){
			$this->db->like("txtNoRekamMedis",$search);
			$this->db->or_like("txtNoAnggota",$search);
			$this->db->or_like("txtNamaPasien",$search);
		}
		
		$this->db->select("rekam_medis.txtNoRekamMedis,pasien.txtNoAnggota,pasien.txtNamaPasien,pasien.dtTanggalLahir,pasien.charJkPasien,rekam_medis.txtIdRekamMedis");
		$this->db->from($this->table);
		$this->db->join("pasien" , "rekam_medis.txtIdPasien = pasien.txtIdPasien","inner");
		$this->db->limit($limit);
		$this->db->offset($offset);
		$data = $this->db->get();
		return $data->result_array();
	
	}
	
	public function countListPasienRekamMedis($search){
		if(!empty($search)){
			$this->db->like("txtNoRekamMedis",$search);
			$this->db->or_like("txtNoAnggota",$search);
			$this->db->or_like("txtNamaPasien",$search);
		}
		
		$this->db->select("rekam_medis.txtNoRekamMedis,pasien.txtNoAnggota,pasien.txtNamaPasien,pasien.dtTanggalLahir,pasien.charJkPasien,rekam_medis.txtIdRekamMedis");
		$this->db->from($this->table);
		$this->db->join("pasien" , "rekam_medis.txtIdPasien = pasien.txtIdPasien","inner");
		$data = $this->db->count_all_results();
		return $data;
	}
	
	public function getMasterByNoRekmed($noRekmed){
		$this->db->select($this->table.".*");
		$this->db->from($this->table);
		$this->db->where($this->secondar_key , $noRekmed);
		$data = $this->db->get();
		return $data->row_array();
	}
	
	public function getMasterByIdPasien($idPasien){
		$this->db->select($this->table.".*");
		$this->db->from($this->table);
		$this->db->where("txtIdPasien" , $idPasien);
		$data = $this->db->get();
		return $data->row_array();
	}
	
	public function getCountNoRekamMedis($noRekmed){
		$this->db->select($this->table.".*");
		$this->db->from($this->table);
		$this->db->where($this->secondar_key , $noRekmed);
		$data = $this->db->count_all_results();
		return $data;
	}
    
    public function getDetailByIdPasien($idPasien){
		$this->db->select($this->table.'.* ');
		$this->db->from($this->table);
		$this->db->where("txtIdPasien" , $idPasien);
		$data = $this->db->get();
        return $data->row_array();
	}
	
    public function detail($idRekmed){
		$this->db->select($this->table.'.* ');
		$this->db->from($this->table);
		$this->db->where($this->primary_key , $idRekmed);
		$data = $this->db->get();
        return $data->row_array();
	}
	
	public function insertRekamMedis($arrData , $debug = false){
		$this->db->set($arrData);
		if($debug){
			$retVal = $this->db->get_compiled_insert($this->table);
		}else{
			$res = $this->db->insert($this->table);
			if(!$res){
				$retVal['error_stat'] = "Failed To Insert";
				$retVal['status'] = false;
			}else{
				$retVal['error_stat'] = "Success To Insert";
				$retVal['status'] = true;
			}
			
		}
		return $retVal;
	}
	
	public function updateRekamMedis($array , $id){
		
		$this->db->where($this->primary_key , $id);
		$query = $this->db->update($this->table, $array);
		if(!$query){
			$retVal['error_stat'] = "Failed To Insert";
			$retVal['status'] = false;
		}else{
			$retVal['error_stat'] = "Success To Update";
			$retVal['status'] = true;
			
		}
		
		return $retVal;
	}
	
    ///Detail Rekam Medis Untuk poli Umum Dan Anak-anak
	public function insertDetailRekamMedis($arrData , $debug = false , $table = ""){
        $table_insert = $this->table_detail;
        if($table=="gigi"){
            $table_insert = $this->table_detail_gigi;
        }
		$this->db->set($arrData);
		if($debug){
			$retVal = $this->db->get_compiled_insert($table_insert);
		}else{
			$res = $this->db->insert($table);
			if(!$res){
				$retVal['error_stat'] = "Failed To Insert";
				$retVal['status'] = false;
			}else{
				$retVal['error_stat'] = "Success To Insert";
				$retVal['status'] = true;
				$retVal['id'] = $this->db->insert_id();
			}
			
		}
		return $retVal;
	}
	
	public function updateDetailRekamMedis($arrData ,$id , $table = ""){
        $table_insert = $this->table_detail;
        if($table=="gigi"){
            $table_insert = $this->table_detail_gigi;
        }
		$this->db->where("txtIdRekmedDetail", $id);
		$query = $this->db->update($table_insert, $arrData);
		if(!$query){
				$retVal['error_stat'] = "Failed To Insert";
				$retVal['status'] = false;
				$retVal['id'] = $id;
			}else{
				$retVal['error_stat'] = "Success To Insert";
				$retVal['status'] = true;
				$retVal['id'] = $id;
		}
		return $retVal;
	}
	
    public function deleteDetailRekamMedis($idRekamMedis , $idDetailRekmed , $table = ""){
        $table_insert = $this->table_detail;
        if($table=="gigi"){
            $table_insert = $this->table_detail_gigi;
        }
		$this->db->where("txtIdRekmedDetail" , $idDetailRekmed);
		$this->db->where("txtIdRekamMedis" , $idRekamMedis);
		$q = $this->db->delete($table_insert);
		if(!$q){
			$retVal['error_stat'] = "Failed To Delete";
			$retVal['status'] = false;
		}else{
			$retVal['error_stat'] = "Success To Delete";
			$retVal['status'] = true;
		}
		return $retVal;
	}
    
    /// Detail Diagnosa
	public function checkDetailDiagnosa($idDetailRekmed){
		$this->db->select($this->table_diagnosa.".*");
		$this->db->from($this->table_diagnosa);
		$this->db->where("txtIdRekmedDetail" , $idDetailRekmed);
		$data = $this->db->count_all_results();
		return $data;
	}
	
	public function deleteDetailDiagnosa($idDetailRekmed){
		$this->db->where("txtIdRekmedDetail" , $idDetailRekmed);
		$q = $this->db->delete($this->table_diagnosa);
		if(!$q){
			$retVal['error_stat'] = "Failed To Delete";
			$retVal['status'] = false;
		}else{
			$retVal['error_stat'] = "Success To Delete";
			$retVal['status'] = true;
		}
		
		return $retVal;
	}
	
	public function insertDetailDiagnosa($arrData , $debug = false){
		$this->db->set($arrData);
		if($debug){
			$retVal = $this->db->get_compiled_insert($this->table_diagnosa);
		}else{
			$res = $this->db->insert($this->table_diagnosa);
			if(!$res){
				$retVal['error_stat'] = "Failed To Insert";
				$retVal['status'] = false;
			}else{
				$retVal['error_stat'] = "Success To Insert";
				$retVal['status'] = true;
			}
			
		}
		return $retVal;
	}
    
    public function getDetailDiagnosa($idRekmedDetail){
		$this->db->select("
						  data_penyakit.intIdPenyakit as id,
						  data_penyakit.txtCategory,
						  data_penyakit.txtSubCategory,
						  data_penyakit.txtIndonesianName,
						  rekam_medis_diagnosa.txtDetailDiagnosa
						  ");
		$this->db->from($this->table_diagnosa);
		$this->db->where("txtIdRekmedDetail" , $idRekmedDetail);
		$this->db->join("data_penyakit" , "rekam_medis_diagnosa.intIdDiagnosaPenyakit = data_penyakit.intIdPenyakit","left");
		$data = $this->db->get();
		return $data->result_array();
	}
	
	/// For Table And View Rekam Medis Poli Umum
	public function getDetailByIdKunjungan($idKunjungan){
		$this->db->select("
						  rekam_medis_detail.txtIdRekmedDetail,
						  rekam_medis_detail.txtPemeriksaan,
						  rekam_medis_detail.txtTindakan,
						  rekam_medis_detail.txtKeterangan,
						  rekam_medis_detail.txtPengobatan,
						  rekam_medis_diagnosa.txtDetailDiagnosa,
						  data_penyakit.intIdPenyakit,
						  data_penyakit.txtCategory,
						  data_penyakit.txtSubCategory,
						  data_penyakit.txtIndonesianName,
						  ");
		$this->db->from($this->table_detail);
		$this->db->where("txtIdKunjungan" , $idKunjungan);
		$this->db->join("rekam_medis_diagnosa" , "rekam_medis_detail.txtIdRekmedDetail = rekam_medis_diagnosa.txtIdRekmedDetail","left");
		$this->db->join("data_penyakit" , "rekam_medis_diagnosa.intIdDiagnosaPenyakit = data_penyakit.intIdPenyakit","left");
		$data = $this->db->get();
        return $data->row_array();
	}
	
	public function getDetailByIdRekamMedis($idRekamMedis , $offset = 0 , $limit = 5){
		$this->db->select("
						  pelayanan.txtNama as namaPoli,
						  pegawai.txtNamaPegawai as namaPegawai,
						  rekam_medis_detail.txtIdRekmedDetail,
						  rekam_medis_detail.intIdPelayanan,
						  rekam_medis_detail.txtPemeriksaan,
						  rekam_medis_detail.txtTindakan,
						  rekam_medis_detail.txtKeterangan,
						  rekam_medis_detail.txtPengobatan,
						  register.txtUsiaPasienKunjungan,
						  register.dtTanggalKunjungan,
						  ");
		$this->db->from($this->table_detail);
		$this->db->where($this->primary_key , $idRekamMedis);
		$this->db->join("register" , "rekam_medis_detail.txtIdKunjungan = register.txtIdKunjungan","left");
		$this->db->join("pegawai" , "rekam_medis_detail.intIdPegawai = pegawai.intIdPegawai","left");
		$this->db->join("pelayanan" , "rekam_medis_detail.intIdPelayanan = pelayanan.intIdPelayanan","left");
		
		$this->db->order_by("rekam_medis_detail.dtCreatedDate" , "DESC");
		$this->db->limit($limit);
		$this->db->offset($offset);
		$data = $this->db->get();
		return $data->result_array();
	}
    
    public function countDetailByIdRekamMedis($idRekamMedis){
		$this->db->select("rekam_medis_detail.txtIdRekmedDetail");
		$this->db->from($this->table_detail);
		$this->db->where($this->primary_key , $idRekamMedis);
		$this->db->join("pegawai" , "rekam_medis_detail.intIdPegawai = pegawai.intIdPegawai","left");
		$this->db->join("pelayanan" , "rekam_medis_detail.intIdPelayanan = pelayanan.intIdPelayanan","left");
		$this->db->join("register" , "rekam_medis_detail.txtIdKunjungan = register.txtIdKunjungan","left");
		$data = $this->db->count_all_results();
		return $data;
	}
	
	public function getDetailRekamMedisByIdDetail($idRekamMedis , $idDetailRekamMedis){
		$this->db->select("
						  pelayanan.txtNama as namaPoli,
						  pegawai.txtNamaPegawai as namaPegawai,
						  rekam_medis_detail.txtIdRekmedDetail,
						  rekam_medis_detail.intIdPelayanan,
						  rekam_medis_detail.txtPemeriksaan,
						  rekam_medis_detail.txtTindakan,
						  rekam_medis_detail.txtKeterangan,
						  rekam_medis_detail.txtPengobatan,
						  register.txtUsiaPasienKunjungan,
						  register.dtTanggalKunjungan,
						  ");
		$this->db->from($this->table_detail);
		$this->db->where($this->primary_key , $idRekamMedis);
		$this->db->where("txtIdRekmedDetail" , $idDetailRekamMedis);
		$this->db->join("register" , "rekam_medis_detail.txtIdKunjungan = register.txtIdKunjungan","left");
		$this->db->join("pegawai" , "rekam_medis_detail.intIdPegawai = pegawai.intIdPegawai","left");
		$this->db->join("pelayanan" , "rekam_medis_detail.intIdPelayanan = pelayanan.intIdPelayanan","left");
		$this->db->order_by("rekam_medis_detail.dtCreatedDate" , "DESC");
		$data = $this->db->get();
		return $data->row_array();
	}

}

