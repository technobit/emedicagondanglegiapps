<?php 

class Register_loket_model extends MY_Model{

    protected $table = 'register_loket';
    protected $primary_key= 'intIdKunjunganLoket';

    function checkKunjungan($id_pelayanan = ""){
        $date_start = date("Y-m-d")." 00:00:00";	
        $date_end = date("Y-m-d H:i:s");	
        $this->db->select('intNoAntri');
        $this->db->from($this->table);
        if(!empty($id_pelayanan)){
            $this->db->where("intIdPelayanan" , $id_pelayanan);
        }
        $this->db->where($this->table.".dtTanggalKunjungan BETWEEN '".$date_start."' AND '".$date_end."'");
        
        $data = $this->db->count_all_results();
        return $data;
    }

    function dataLoket($txtDate , $intIdStatus , $idLoket){
        $date_start = $txtDate." 00:00:00";	
        $date_end = $txtDate." 23:59:00";	
        $this->db->select('*');
        $this->db->join('pelayanan' , $this->table.'.intIdPelayanan = pelayanan.intIdPelayanan');
        $this->db->from($this->table);
        $this->db->where($this->table.".dtTanggalKunjungan BETWEEN '".$date_start."' AND '".$date_end."'");
        if(!empty($idLoket)){
            $this->db->where($this->table.".intIdPelayanan" , $idLoket);
        }
        
        if(!empty($intIdStatus)){
            $this->db->where('bitIsPoli' , $intIdStatus);
        }
        $this->db->order_by('bitIsPoli','ASC');
        $this->db->order_by('dtTanggalKunjungan','ASC');
        $data = $this->db->get();
        return $data->result_array();
    }

    function insert($arrData){
        $resVal = array();
        $this->db->set($arrData);
        $res = $this->db->insert($this->table);
        $resVal['status'] = $res;
        $resVal['message'] = $res==true ? 'Registrasi Loket Berhasil' : 'Registrasi loket Gagal';
        $resVal['id'] = $this->db->insert_id();
        return $resVal;
    }

    function update($arrData , $id){
        $this->db->where($this->primary_key , $id);
        $ret = $this->db->update($this->table,$arrData);
        $resVal['status'] = $ret;
        $resVal['message'] = $ret==true ? 'Registrasi Loket Berhasil' : 'Registrasi loket Gagal';
        $resVal['id'] = $id;
        return $resVal;
    }

    function detail($id){
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->join('pelayanan' , $this->table.'.intIdPelayanan = pelayanan.intIdPelayanan');
        $this->db->where($this->primary_key , $id);
        $data = $this->db->get();
        return $data->row_array();
    }

    function delete($id){
        $this->db->where($this->primary_key , $id);
        $data = $this->db->delete($this->table);
        $retVal = array();
		if(!$data){
			$retVal['message'] = "Data Gagal Di Hapus";
			$retVal['status'] = false;
		}else{
			$retVal['message'] = "Data Berhasil Di Hapus";
			$retVal['status'] = true;
		}
		
		return $retVal;

    }


    function countRegisterByPelayananSum($date_start="" , $date_end = ""){
        
		$this->db->select("pelayanan.intIdPelayanan AS id, pelayanan.txtNama AS nama, COUNT(register_loket.intIdKunjunganLoket) AS jumlah");
		$this->db->from($this->table);
		$this->db->join("pelayanan" , "register_loket.intIdPelayanan = pelayanan.intIdPelayanan" , "left");
		$this->db->where("register_loket.dtTanggalKunjungan BETWEEN '".$date_start."' AND '".$date_end."'");
		$this->db->where("pelayanan.intIdJenisPelayanan!=8");	
		$this->db->group_by("register_loket.intIdPelayanan");
		$data = $this->db->get();
        return $data->result_array();
    }
    
    public function countRegisterByDate($date_start="" , $date_end = ""){
		$this->db->select("register_loket.dtTanggalKunjungan, COUNT(register_loket.dtTanggalKunjungan) AS Jumlah");
		$this->db->from($this->table);
		$this->db->where("register_loket.dtTanggalKunjungan BETWEEN '".$date_start."' AND '".$date_end."' GROUP BY DAY(register_loket.dtTanggalKunjungan)");
		$data = $this->db->get();
        return $data->result_array();
    }
    
    public function getJumlahAntrianPerPoli($date , $poli = ""){
		$date_start = $date." 00:00:00";
		$date_end = $date." ".date("H:i:s");
		
		 $this->db->from($this->table);
		 $this->db->where("register_loket.dtTanggalKunjungan BETWEEN '".$date_start."' AND '".$date_end."'");
		 if(!empty($poli)) {
			 $this->db->where("intIdPelayanan" , $poli);
		 }
		 $this->db->where("bitIsPoli" , 1);
		 $data = $this->db->count_all_results();
		 return $data;
     }
     
     public function getNoAntrianDiLayani($date , $poli = ""){
        $date_start = $date." 00:00:00";
        $date_end = $date." ".date("H:i:s");
        $this->db->select("intNoAntri");
        $this->db->from($this->table);
        $this->db->where("register_loket.dtTanggalKunjungan BETWEEN '".$date_start."' AND '".$date_end."'");
        
        $this->db->where("intIdPelayanan" , $poli);
        $this->db->where("bitIsPoli" , 2);
        $this->db->order_by("register_loket.dtTanggalKunjungan" , "DESC");
        $data = $this->db->get();
        return $data->row_array();
    }

    
}