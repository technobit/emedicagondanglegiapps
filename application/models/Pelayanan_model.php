<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pelayanan_Model extends CI_Model {

	var $table = "pelayanan";
	var $primary_key = "intIdPelayanan";
    
    public function __construct(){
        parent::__construct();
        
    }
	
	
	public function getListDataPelayanan(){
		$this->db->select($this->table.'.* ');
		$this->db->from($this->table);
		$this->db->join("jenis_pelayanan" , "pelayanan.intIdJenisPelayanan = jenis_pelayanan.intIdJenisPelayanan" , "inner");
		$this->db->where($this->table.".intIdJenisPelayanan!=8");
		$this->db->where($this->table.".intIdJenisPelayanan!=13");
		///$this->db->where($this->table.".intIdJenisPelayanan!=15");
		///$this->db->where($this->table.".intIdJenisPelayanan!=9");
		$this->db->where("bitStatus" , 1);
		$this->db->order_by("intIdJenisPelayanan");
		$data = $this->db->get();
        return $data->result_array();
	}

	public function getListDataPelayananPoliKeuangan(){
		$this->db->select($this->table.'.* ');
		$this->db->from($this->table);
		$this->db->join("jenis_pelayanan" , "pelayanan.intIdJenisPelayanan = jenis_pelayanan.intIdJenisPelayanan" , "inner");
		
		$this->db->where($this->table.".intIdJenisPelayanan!=8");
		$this->db->where($this->table.".intIdJenisPelayanan!=9");
		$this->db->where("bitStatus" , 1);
		$this->db->order_by("intIdJenisPelayanan");
		$data = $this->db->get();
        return $data->result_array();
	}

	public function getListDataPoliRawatJalan(){
		$this->db->select($this->table.'.* ');
		$this->db->from($this->table);
		$this->db->join("jenis_pelayanan" , "pelayanan.intIdJenisPelayanan = jenis_pelayanan.intIdJenisPelayanan" , "inner");
		$this->db->where($this->table.".intIdJenisPelayanan!=4");
		$this->db->where($this->table.".intIdJenisPelayanan!=8");
		$this->db->where($this->table.".intIdJenisPelayanan!=9");
		$this->db->where($this->table.".intIdJenisPelayanan!=13");
		$this->db->where($this->table.".intIdJenisPelayanan!=15");
		$this->db->where("bitStatus" , 1);
		$this->db->order_by("intIdJenisPelayanan");
		$data = $this->db->get();
        return $data->result_array();
	}
    
   	/// For Index Data
    public function getData($offset = 0 , $limit = 10 , $search=""){
		
		if(!empty($search)){
			$this->db->like("txtNama" , $search);
			$this->db->or_like("txtSingkat" , $search);
		}
		
        $this->db->select($this->table.'.*,jenis_pelayanan.txtJenisPelayanan');
		$this->db->from($this->table);
		$this->db->join("jenis_pelayanan" , "pelayanan.intIdJenisPelayanan = jenis_pelayanan.intIdJenisPelayanan");
		$this->db->where("jenis_pelayanan.bitStatusJenisPelayanan" , 1);
		$this->db->offset($offset);
		$this->db->limit($limit);
        $data = $this->db->get();
        return $data->result_array();
    }
	
	public function getCountData($search=""){
		
		
		if(!empty($search)){
			$this->db->like("txtNama" , $search);
			$this->db->or_like("txtSingkat" , $search);
		}
		
        $this->db->select($this->table.'.*,jenis_pelayanan.txtJenisPelayanan');
		$this->db->from($this->table);
		$this->db->join("jenis_pelayanan" , "pelayanan.intIdJenisPelayanan = jenis_pelayanan.intIdJenisPelayanan");
		$this->db->where("jenis_pelayanan.bitStatusJenisPelayanan" , 1);
      
        $data = $this->db->count_all_results();
        return $data;
    }
	

    public function getCountDataIndex($search=""){
    	if(!empty($search)){
			$this->db->where($search);
		}
		$this->db->select($this->table.'.* ');	
		$this->db->from($this->table);
		$data = $this->db->count_all_results();	
        return $data;
    }
	
	
	public function getDetail($id){
		$this->db->where($this->primary_key , $id);
		$this->db->join("jenis_pelayanan" , "pelayanan.intIdJenisPelayanan = jenis_pelayanan.intIdJenisPelayanan" ,"left");
		$query = $this->db->get($this->table,1);
		$resVal = "";
		if($query->num_rows() > 0){
			$resVal = $query->row_array();	
		}else{
			$resVal = false;
		}
		return $resVal;
	}
	public function save($arrData){
		$ret = $this->saveData($arrData);
		return $ret;
	}
	public function saveData($arrData = array(),$debug=false){
		
		$this->db->set($arrData);
		if($debug){
			$retVal = $this->db->get_compiled_insert($this->table);
		}else{
			$res = $this->db->insert($this->table);
			if(!$res){
				$retVal['error_stat'] = "Data Gagal Di Simpan";
				$retVal['status'] = false;
			}else{
				$retVal['error_stat'] = "Data Berhasil Di Simpan";
				$retVal['status'] = true;
				$retVal['id'] = $this->db->insert_id();
			}
			
		}
		return $retVal;
	}
	
	public function update($array , $id){
		
		$this->db->where($this->primary_key, $id);
		$query = $this->db->update($this->table, $array);
		if(!$query){
			
			$retVal['error_stat'] = "Data Gagal Di perbarui";
			$retVal['status'] = false;
		}else{
			$retVal['error_stat'] = "Data Berhasil Di Perbarui";
			$retVal['status'] = true;
			$retVal['id'] = $id;
		}
		
		return $retVal;
	}
	
	public function delete($id){
		$this->db->where($this->primary_key , $id);
		$q = $this->db->delete($this->table);
		
		if(!$q){
			$retVal['error_stat'] = "Data Gagal Di Hapus";
			$retVal['status'] = false;
		}else{
			$retVal['error_stat'] = "Data Berhasil Di Hapus";
			$retVal['status'] = true;
		}
		
		return $retVal;
	}
	
	public function getPelayananUGD(){
		$this->db->where("intIdJenisPelayanan" , 7);
		$this->db->where("bitStatus" , 1);
		$this->db->limit(1);
		$data = $this->db->get($this->table);
		$row = $data->row_array();
		return $row;
	}

	public function getPelayananByIdJenis($idJenisPelayanan){
		$this->db->where("intIdJenisPelayanan" , $idJenisPelayanan);
		$this->db->where("bitStatus" , 1);
		$this->db->limit(1);
		$data = $this->db->get($this->table);
		$row = $data->row_array();
		return $row;
	}

	public function getListPelayananByIdJenis($idJenisPelayanan){
		$this->db->where("intIdJenisPelayanan" , $idJenisPelayanan);
		$this->db->where("bitStatus" , 1);
		$data = $this->db->get($this->table);
		$row = $data->result_array();
		return $row;
	}
	
	public function getPelayananByJenisFasilitas($txtJenisFasilitas){
		$this->db->select("pelayanan.intIdPelayanan, pelayanan.txtNama");
		$this->db->from($this->table);
		$this->db->join("jenis_pelayanan" , "pelayanan.intIdJenisPelayanan = jenis_pelayanan.intIdJenisPelayanan" , "inner");
		if ($txtJenisFasilitas =='PO') {
			$this->db->where_in("txtJenisFasilitas" , array('FASILITAS','POLIKLINIK'));
		}else {
			$this->db->where("txtJenisFasilitas" , $txtJenisFasilitas);
		}
		$this->db->where("bitStatus" , 1);
		$data = $this->db->get();
		return $data->result_array();
	}

	public function getListKamar(){
		$this->db->select("kamar.intIdKamar,kamar.txtKamar , kelas_kamar.txtKelasKamar");
		$this->db->from("kamar");
		$this->db->join("kelas_kamar" , "kamar.intIdKelasKamar = kelas_kamar.intIdKelasKamar" , "left");
		$this->db->where("bitStatusKamar" , 1);
		$data = $this->db->get();
		return $data->result_array();
	}

	public function getCheckByPelayanan($txtPelayanan){
		$this->db->where("txtNama" , $txtPelayanan);
		$this->db->from($this->table);
		$data = $this->db->get();
		return $data->row_array();
	}


	///// Akses Pelayanan 

	public function cekAkses($id_pelayanan){
		$this->db->from("pelayanan_akses");
		$this->db->where("intIdPelayanan" , $id_pelayanan);
		$data = $this->db->count_all_results();
		if($data > 0){
			$this->deleteAkses($id_pelayanan);
		}
		return $data;
	}

	public function saveAkses($arrData , $id_pelayanan){
		$this->cekAkses($id_pelayanan);
		$ret = $this->db->insert_batch("pelayanan_akses",$arrData);
		return $ret;
	}

	public function deleteAkses($id_pelayanan){
		$this->db->where("intIdPelayanan" , $id_pelayanan);
		$data = $this->db->delete("pelayanan_akses");
		return $data;
	}

	public function getListAkses($id_pelayanan){
		
		$this->db->from("pelayanan_akses");
		$this->db->where("intIdPelayanan" , $id_pelayanan);
		$data = $this->db->get();
		return $data->result_array();
	}

	
	
}

