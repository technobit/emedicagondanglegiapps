<?php
class User_model extends CI_Model {

<?php
class Users_Model extends CI_Model {
	var $table_profil = "users_profil";
    var $table_menu = "users_menu";
    var $table_level = "users_level";
	var $primary_key = "intIdUser";    
	public function getDataLogin($username , $password , $select = '*'){
		$this->db->select($select);
		$this->db->where("txtEmail" , $username);
		$this->db->or_where("txtUserName" , $username);
		$this->db->from($this->table_profil);
		$data = $this->db->get();
        return $data->result_array();
	}
    
    public function getData(){
        $this->db->select($this->table_profil.'.*,pegawai.txtNamaPegawai,users_level.txtLevel');
		$this->db->from($this->table_profil);
		$this->db->join("users_level" , "users_profil.intIdLevel = users_level.intIdLevel" , "left");
		$this->db->join("pegawai" , "users_profil.intIdPegawai = pegawai.intIdPegawai" , "left");
        $data = $this->db->get();
        return $data->result_array();
    }
	
	public function getCountData(){
        $this->db->select($this->table_profil.'.*,pegawai.txtNamaPegawai,users_level.txtLevel');
		$this->db->from($this->table_profil);
		$this->db->join($this->table_level , $this->table_profil.".intIdLevel = ".$this->table_level.".intIdLevel" , "left");
		$this->db->join("pegawai" , $this->table_profil.".intIdPegawai = pegawai.intIdPegawai" , "left");
        $data = $data = $this->db->count_all_results();;
        return $data;
    }
	
    public function getCountDataIndex($search=""){
    	if(!empty($search)){
			$this->db->where($search);
		}
		$this->db->select($this->table.'.* ');	
		$this->db->from($this->table);
		$data = $this->db->count_all_results();	
        return $data;
    }
	
	
	public function getDetail($id){
		$this->db->where($this->primary_key , $id);
		$query = $this->db->get($this->table_profil);
		$resVal = "";
		if($query->num_rows() > 0){
			$resVal = $query->row_array();	
		}else{
			$resVal = false;
		}
		return $resVal;
	}
	
	public function saveData($arrData = array(),$debug=false){
		
		$this->db->set($arrData);
		if($debug){
			$retVal = $this->db->get_compiled_insert($this->table);
		}else{
			$res = $this->db->insert($this->table_profil);
			if(!$res){
				$retVal['error_stat'] = "Failed To Insert";
				$retVal['status'] = false;
			}else{
				$retVal['error_stat'] = "Success To Insert";
				$retVal['status'] = true;
				$retVal['id'] = $this->db->insert_id();
			}
			
		}
		return $retVal;
	}
	
	public function update($array , $id){
		
		$this->db->where($this->primary_key , $id);
		$query = $this->db->update($this->table_profil, $array);
		if(!$query){
			
			$retVal['error_stat'] = "Failed To Insert";
			$retVal['status'] = false;
		}else{
			$retVal['error_stat'] = "Success To Update";
			$retVal['status'] = true;
			$retVal['id'] = $id;
		}
		
		return $retVal;
	}
	
	public function delete($id){
		$this->db->where($this->primary_key , $id);
		$q = $this->db->delete($this->table_profil);
		
		if(!$q){
			$retVal['error_stat'] = "Failed To Delete";
			$retVal['status'] = false;
		}else{
			$retVal['error_stat'] = "Success To Delete";
			$retVal['status'] = true;
		}
		
		return $retVal;
	}	
}	
}