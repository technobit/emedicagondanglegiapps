<?php
require_once("Bridge.php");
class KunjunganBPJS extends Bridge{

    var $ci = "";
    var $app_devel_status = "";
    function __construct(){
        $this->ci = &get_instance();
        $this->app_devel_status = BPJS_STATUS_PRODUCTION;
    }   

    
    function addKunjungan($arrPost){   
        $appLink = "v1/kunjungan";

        if($this->app_devel_status==1){
            $response = $this->sendPostRequest($appLink , $arrPost);
        }else{
            $response = array();
            $response['status'] = false;
            $response['message'] = "Masih Devel mz";
        }
        return $response;
    }

    function getRiwayatKunjungan($noKartu){   
        $appLink = "v1/kunjungan/peserta/".$noKartu;
        $response = $this->sendGetRequest($appLink );
        return $response;
    }

    function editKunjungan($arrPost){   
        $appLink = "v1/kunjungan";
        if($this->app_devel_status==1){
            $response = $this->sendPostRequest($appLink , $arrPost , "PUT");
        }else{
            $response = array();
            $response['status'] = false;
            $response['message'] = "Masih Devel mz";
        }
        
        return $response;
    }

    function deleteKunjungan($noKunjungan){
        $appLink = "v1/kunjungan/".$noKunjungan;
        
        if($this->app_devel_status==1){
            $response = $this->sendDeleteRequest($appLink);
        }else{
            $response = array();
            $response['status'] = false;
            $response['message'] = "Masih Devel mz";
        }
        return $response;
    }

    function getRujukanV1($noKunjungan){
        $appLink = "v1/kunjungan/rujukan/".$noKunjungan;
        $response = $this->sendGetRequest($appLink );
        return $response;
    }

    function getRujukanV2($noKunjungan){
        $appLink = "v2/kunjungan/rujukan/".$noKunjungan;
        $response = $this->sendGetRequest($appLink );
        return $response;
    }

    function saveDataKunjungan($post , $mode = "insert"){

        $this->ci->load->model(
            array(
                'register_bpjs_model',
                'pelayanan_model',
            )
        );

        $this->ci->load->library(
            array(
                'bpjs/pendaftaranbpjs',
                'bpjs/pesertabpjs',
                "bpjs/masterbpjs",
            )
        );

        $resAPI = array();
        
        $kdDokter = "038";
		///		Default Constant
		$getDokter = $this->ci->masterbpjs->getDokter(0,1);
		if($getDokter['status']==true){
			$dataDokter = $getDokter['data']['list'][0];
			$kdDokter = $dataDokter['kdDokter'];
		}

        $arrPostBPJS = array(
		"noKunjungan" => $post['noKunjungan'],
		"noKartu" => $post['noKartu'],
		"tglDaftar" => $post['tglDaftar'],
		"keluhan" => $post['keluhan'],
		"kdSadar" => $post['kdSadar'],
		"sistole" => $post['sistole'],
		"diastole" => $post['diastole'],
		"beratBadan" => $post['beratBadan'],
		"tinggiBadan" => $post['tinggiBadan'],
		"respRate" => $post['respRate'],
		"heartRate" => $post['heartRate'],
		"terapi" => $post['terapi'],
		"kdProviderRujukLanjut" => $post['kdProviderRujukLanjut'],
		"kdStatusPulang" => $post['kdStatusPulang'],
		"tglPulang" => $post['tglPulang'],
		"kdDokter" => $kdDokter,
		"kdDiag1" => $post['kdDiag1'],
		"kdDiag2" => $post['kdDiag2'],
		"kdDiag3" => $post['kdDiag3'],
		"kdPoliRujukInternal" => $post['kdPoliRujukInternal'],
		"kdPoliRujukLanjut" => $post['kdPoliRujukLanjut'],
		);

        if($mode=="insert"){
            $resAPI = $this->addKunjungan($arrPostBPJS);
        }else{
            $resAPI = $this->editKunjungan($arrPostBPJS);
        }

        return $resAPI;
    }

}