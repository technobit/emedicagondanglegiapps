<?php
require_once("Bridge.php");
class MasterBPJS extends Bridge{

    function __construct(){
        
    }   

    function getDokter( $offset = 0 , $limit = 30){
        
        $appLink = "v1/dokter/".$offset."/".$limit;
        $response = $this->sendGetRequest($appLink);
        return $response;
    }

    function getKesadaran(){
        $appLink = "v1/kesadaran";
        $response = $this->sendGetRequest($appLink);
        return $response;
    }

    function getStatusPulang($rawatInap = false){
        $appLink = "v1/statuspulang/rawatInap/".$rawatInap;
        $response = $this->sendGetRequest($appLink);
        return $response;
    }
    

}