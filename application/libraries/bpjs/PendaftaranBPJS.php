<?php
require_once("Bridge.php");
class PendaftaranBPJS extends Bridge{

    var $ci = "";
    var $app_devel_status = "";
    function __construct(){
        
         $this->ci = &get_instance();
         $this->app_devel_status = BPJS_STATUS_PRODUCTION;
    }   

    function getPesertaV2($noKodeBPJS){
        $appLink = "v2/peserta/".$noKodeBPJS;
        $response = $this->sendGetRequest($appLink);
        return $response;
    }

    function pendaftaranPeserta($arrPost){
        
        $appLink = "v1/pendaftaran";
        if($this->app_devel_status==1){
            $response = $this->sendPostRequest($appLink , $arrPost);
        }else{
            $response = array();
            $response['status'] = false;
            $response['message'] = "Masih Devel mz";
        }
        return $response;
    }

    function getPendaftaranByNoUrut($noUrut,$tanggalDaftar){
        $appLink = "v1/pendaftaran/noUrut/".$noUrut."/tglDaftar/".$tanggalDaftar;
        $response = $this->sendGetRequest($appLink);
        return $response;
    }

    function getPendaftaranByProvider($tanggalDaftar , $offset = 0 , $limit = 20){
        $appLink = "v1/pendaftaran/tglDaftar/".$tanggalDaftar."/".$offset."/".$limit;
        $response = $this->sendGetRequest($appLink);
        return $response;
    }

    function deletePendaftaranPeserta($noKartuBPJS , $tanggalDaftar , $noUrut){
        $appLink = "v1/pendaftaran/peserta/".$noKartuBPJS."/tglDaftar/".$tanggalDaftar."/noUrut/".$noUrut;
        if($this->app_devel_status==1){
            $response = $this->sendDeleteRequest($appLink);
        }else{
            $response = array();
            $response['status'] = false;
            $response['message'] = "Masih Devel mz";
        }
        return $response;
    }

    function syncPendaftaran($txtIdKunjungan){
        $this->ci->load->model(
            array(
                'register_bpjs_model',
                'pelayanan_model',
            )
        );

        $this->ci->load->library(
            array(
                'bpjs/pendaftaranbpjs',
                'bpjs/pesertabpjs',
            )
        );

        $detailPendaftaran = $this->ci->register_bpjs_model->detail($txtIdKunjungan);
        $idPelayanan = $detailPendaftaran['intIdPelayanan'];
		
		$jamKes = $detailPendaftaran["intIdJaminanKesehatan"];
		
		$noJamkes = $detailPendaftaran["txtNoJaminanKesehatan"];
		
		$txtKeteranganKunjungan = $detailPendaftaran["txtKeteranganKunjungan"];
		
		
		$dateTimerRegister = explode(" " , $detailPendaftaran['dtTanggalKunjungan']);
		
		$dateRegister = $dateTimerRegister[0];
		
		$tglPendaftaran = reverseDate($dateRegister);
		
		$detailJenisPelayanan = $this->ci->pelayanan_model->getDetail($idPelayanan);
		
		$idJenisPelayanan = $detailJenisPelayanan['intIdJenisPelayanan'];
		
		$txtKodeBPJS = $detailJenisPelayanan['txtKodePelayananBPJS'];
		
		$kdProvider = 000;
		
		$detailBPJS = $this->ci->pesertabpjs->getPesertaV2($noJamkes);
		
		if($detailBPJS['status']==1){
			
			$kdProvider = $detailBPJS['data']['kdProviderPst']['kdProvider'];
			
		}
		
		$arrPostBPJS = array(
		"kdProviderPeserta" => $kdProvider,
		"tglDaftar" => $tglPendaftaran,
		"noKartu" => $noJamkes,
		"kdPoli" => $txtKodeBPJS,
		"keluhan" => !empty($txtKeteranganKunjungan) ? $txtKeteranganKunjungan : null,
		"kunjSakit" => $detailPendaftaran['kunjSakit'],
		"sistole" => $detailPendaftaran['sistole'],
		"diastole" => $detailPendaftaran['diastole'],
		"beratBadan" => $detailPendaftaran['beratBadan'],
		"tinggiBadan" => $detailPendaftaran['tinggiBadan'],
		"respRate" => $detailPendaftaran['respRate'],
		"heartRate" => $detailPendaftaran['heartRate'],
		"rujukBalik" => $detailPendaftaran['rujukBalik'],
		"rawatInap" => $detailPendaftaran['rawatInap'],
		);
		
		$resAPI = $this->pendaftaranPeserta($arrPostBPJS);
		return $resAPI;
    }

    

}