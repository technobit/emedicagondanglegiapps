<?php
require_once("Bridge.php");
class KegiatanBPJS extends Bridge{

    var $ci = "";
    var $app_devel_status = "";
    function __construct(){
        $this->ci = &get_instance();
        $this->app_devel_status = BPJS_STATUS_PRODUCTION;
    }   

    
    function addKegiatan($arrPost = array()){   
        $appLink = "v1/kelompok/kegiatan";

        //if($this->app_devel_status==1){
            $response = $this->sendPostRequest($appLink , $arrPost);
        /*}else{
            $response = array();
            $response['status'] = false;
            $response['message'] = "Masih Devel mz";
        }*/
        return $response;
    }
     function getKegiatanKelompok($tanggal){
        $appLink = "v1/kelompok/kegiatan/".$tanggal;
        $response = $this->sendGetRequest($appLink );
        return $response;
    }
    function getPesertaKegiatan($eduID){
        $appLink = "v1/kelompok/peserta/".$eduID;
        $response = $this->sendGetRequest($appLink );
        return $response;
    }

   
   

    function editKegiatan($arrPost){   
        $appLink = "v1/kelompok/kegiatan";
        if($this->app_devel_status==1){
            $response = $this->sendPostRequest($appLink , $arrPost , "PUT");
        }else{
            $response = array();
            $response['status'] = false;
            $response['message'] = "Masih Devel mz";
        }
        
        return $response;
    }

    function deleteKegiatan($eduID){
        $appLink = "v1/kelompok/kegiatan/".$eduID;
        
       // if($this->app_devel_status==1){
            $response = $this->sendDeleteRequest($appLink);
       /* }else{
            $response = array();
            $response['status'] = false;
            $response['message'] = "Masih Devel mz";
        }*/
        return $response;
    }

   

    function saveDataKegiatan($post, $mode = "insert"){


        $resAPI = array();
        
        

        $arrPostBPJS = array(
		"eduId"=> null,
        "clubId"=> 36,
        "tglPelayanan"=> "27-03-2016",
        "kdKegiatan"=> "01",
        "kdKelompok"=> "03",
        "materi"=> "materi",
        "pembicara"=> "pembicara",
        "lokasi"=> "lokasi",
        "keterangan"=> "keterangan",
        "biaya"=> 20000

		);

        if($mode=="insert"){
            $resAPI = $this->addKunjungan($arrPostBPJS);
        }else{
            $resAPI = $this->editKunjungan($arrPostBPJS);
        }

        return $resAPI;
    }
    }