<?php
require_once("Bridge.php");
class DiagnosaBPJS extends Bridge{

    function __construct(){
        
    }   

    function getDiagnosa($kodeDiagnosa , $offset = 0 , $limit = 10){
        
        $appLink = "v1/diagnosa/".$kodeDiagnosa."/".$offset."/".$limit;
        $response = $this->sendGetRequest($appLink);
        return $response;
    }

    function getPendaftaranByNoUrut($noUrut,$tanggalDaftar){
        $appLink = "v1/pendaftaran/noUrut/".$noUrut."/tglDaftar/".$tanggalDaftar;
        $response = $this->sendGetRequest($appLink);
        return $response;
    }

    function getPendaftaranByProvider($tanggalDaftar , $offset = 0 , $limit = 20){
        $appLink = "v1/pendaftaran/tglDaftar/".$tanggalDaftar."/".$offset."/".$limit;
        $response = $this->sendGetRequest($appLink);
        return $response;
    }

    function deletePendaftaranPeserta($noKartuBPJS , $tanggalDaftar , $noUrut){
        $appLink = "v1/pendaftaran/peserta/".$noKartuBPJS."/tglDaftar/".$tanggalDaftar."/noUrut/".$noUrut;
        $response = $this->sendDeleteRequest($appLink);
        return $response;
    }

    

}