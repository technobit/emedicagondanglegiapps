<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bridge {
	
	
	var $consumerID = '';
	
	var $consumerKey ='';
	
	var $APILink = BPJS_API_LINK_PRODUCTION;
	
	var $statusApps = BPJS_STATUS_API;
	
	var $isConnected = "";
	
	var $failConnectedResponse = array();
	
	var $failSaveConnectedResponse = array();
	
	var $response = array();
	
	
	
	function __construct(){
		parent::__construct();
	
	
	}
	
	
	
	private function getSignatureHeader(){
		
		$this->checkKoneksiBPJS();
		$CI = &get_instance();
		$dataBpjs = $CI->session->userdata("pengaturan_bpjs");
		$data =  $dataBpjs['cons_id'];
		
		$secretKey = $dataBpjs['cons_key'];
		
		
		date_default_timezone_set('UTC');
		
		$tStamp = strval(time()-strtotime('1970-01-01 00:00:00'));
		
		$signature = hash_hmac('sha256', $data ."&".$tStamp , $secretKey, true);
		
		$encodedSignature = base64_encode($signature);
		
		$string = "cGttX2J1bHVsYXdhbmc6YnVsdWxhd2FuNjowOTU";
		
		$stringData = base64_decode($string);
		
		
		///		$encodedSignature = urlencode($encodedSignature);
	 	
		
		
		$userNamePcare = $dataBpjs['pcare_username'];
		
		$passPcare = $dataBpjs['pcare_password'];
		
		  
		$xAuthString = $userNamePcare.":".$passPcare.":095";
		
		$xAuth = "Basic ".base64_encode($xAuthString);
		
		$result['xConsId'] = $data;
		
		$result['xTimeStamp'] = $tStamp;
		
		$result['xSignature'] = $encodedSignature;
		
		$result['xAuth'] = $xAuth;
		
		return $result;
		
	}
	
	
	private function getResult($result){
		
		
		$result = json_decode($result , true);
		
		$status = true;
		
		$message = "success";
		
		$code = $result['metaData']['code'];
		
		$status = $code=="200" ? true : false;
		
		$data = $result['response'];
		
		switch($code){
			
			case "200" : $status=true;
			
			$message="success";
			
			break;
			
			case "201" : $status=true;
			
			$message="Data Berhasil Di Simpan";
			
			break;
			
			case "204" : $status=false;
			
			$message="Data Tidak Tersedia";
			
			break;
			
			case "500" : $status=false;
			
			$message="Internal Server Error - Server BPJS Mengalami Gangguan";
			
			break;
			
			case "401" : $status=false;
			
			$message="User Password Salah";
			
			break;
			
			case "408" : $status=false;
			
			$message="Request Time Out";
			
			break;
			
			case "424" : $status=false;
			
			$message="Failed Dependency";
			
			break;
			
			case "412" : $status=false;
			
			$message="Field Harus Di isi ".$data[0]['field'] . " (".$data[0]['message'].")";
			
			break;
			
			default :    $status=false;
			
			$message="Error Pada Sistem";
			
			break;
			
		}
		
		
		$retVal = array();
		
		$retVal['status'] = $status;
		
		$retVal['message'] = $message;
		
		$retVal['data'] = $data;
		
		return $retVal;
		
	}
	
	
	public function sendPostRequest($link , $arrData , $modeRequest="POST"){
		
		$headerSend = $this->getSignatureHeader();
		
		if(!$this->isConnected){
			
			$retVal['status'] = false;
			
			$retVal['message'] = "Tidak Terkoneksi";
			
			$retVal['data'] = array();
			
			return $retVal;
			die;
			
		}
		
		$url = $this->APILink . $link;
		
		$data_string = json_encode($arrData);
		
		$ch = curl_init($url);
		
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $modeRequest);
		
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		'Content-Type: application/json',
		'Content-Length: ' . strlen($data_string),
		'Host: api.bpjs-kesehatan.go.id',
		'Content-Type: application/x-www-form-urlencoded; charset=utf-8',
		'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
		'Accept-Encoding: gzip, deflate',
		'Accept-Language: en-US,en;q=0.5',
		'Cache-Control: no-cache',
		"X-cons-id: ".$headerSend['xConsId'],
		"X-timestamp: ".$headerSend['xTimeStamp'],
		"X-signature: ".$headerSend['xSignature'],
		"X-Authorization: ".$headerSend['xAuth'],
		)
		);
		
		$result = curl_exec($ch);
		
		curl_close ($ch);
		
		$retVal = $this->getResult($result);
		
		return $retVal;
		
	}
	
	
	public function sendDeleteRequest($link){
		
		$headerSend = $this->getSignatureHeader();
		
		if(!$this->isConnected){
			
			$retVal['status'] = false;
			
			$retVal['message'] = "Tidak Terkoneksi";
			
			$retVal['data'] = array();
			
			return $retVal;
			die;
			
		}
		
		$url = $this->APILink . $link;
		
		$ch = curl_init($url);
		
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
		
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		'Host: api.bpjs-kesehatan.go.id',
		"X-cons-id: ".$headerSend['xConsId'],
		"X-timestamp: ".$headerSend['xTimeStamp'],
		"X-signature: ".$headerSend['xSignature'],
		"X-Authorization: ".$headerSend['xAuth'],
		)
		);
		
		$result = curl_exec($ch);
		
		curl_close ($ch);
		
		$retVal = $this->getResult($result);
		
		return $retVal;
		
	}
	
	
	public function sendGetRequest($link ){
		
		$retVal = array();
		
		
		$headerSend = $this->getSignatureHeader();
	
		if(!$this->isConnected){
			
			$retVal['status'] = false;
			
			$retVal['message'] = "Tidak Terkoneksi";
			
			$retVal['data'] = array();
			
			return $retVal;
			die;
			
		}
		
		$url = $this->APILink . $link;
		
		////$url = $this->APILink . $link;
		
		$ch = curl_init($url);
		
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
		
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		'Host: api.bpjs-kesehatan.go.id',
		"X-cons-id: ".$headerSend['xConsId'],
		"X-timestamp: ".$headerSend['xTimeStamp'],
		"X-signature: ".$headerSend['xSignature'],
		"X-Authorization: ".$headerSend['xAuth'],
		)
		);
		
		$result = curl_exec($ch);
		
		curl_close ($ch);
		
		$retVal = $this->getResult($result);
		
		return $retVal;
		
		
	}
	
	
	
	private function checkKoneksiBPJS(){
		
		$retVal = array();
		
        $this->statusApps = BPJS_STATUS_API;
        if(!$this->statusApps){
            $this->isConnected = false;
            $this->failConnectedResponse = array(
			"status" => false,
			"message" => "Koneksi API Tidak Di Aktifkan - Gagal Menyambungkan Ke Server BPJS"
			);
            
        }else{
            
        //// Koneksi internet
		$connected = @fsockopen("pcare.bpjs-kesehatan.go.id", 80);
		
		if(!$connected){
			
			$this->isConnected = false;
			
			$this->failConnectedResponse = array(
			"status" => false,
			"message" => "Koneksi Internet Tidak Tersedia - Gagal Menyambungkan Ke Server BPJS"
			);
			
			$this->failSaveConnectedResponse = array(
			"status" => false,
			"message" => "Koneksi Internet Tidak Tersedia - Pendaftaran Akan Di Sinkronisasi kan ke Server Jika Koneksi Telah Tersedia"
			);
			
			
		}
		else{
			
			$this->isConnected = true;
			
			fclose($connected);
			
		}
        }
        
        
		
	}
	
	
	///	/ Peserta
	public function checkPasienByNoAnggota($noAnggota){
		
		$appLink = "v2/peserta/".$noAnggota;
		
		$response = $this->sendGetRequest($appLink);
		
		return $response;
		
	}
	
	
}

