<?php
require_once("Bridge.php");
class PesertaBPJS extends Bridge{

    function __construct(){
        
    }   

    function getPesertaV2($noKodeBPJS){
        
        $appLink = "v2/peserta/".$noKodeBPJS;
        $response = $this->sendGetRequest($appLink);
        return $response;
    }

    function getPesertaByNIK($NIK ,$jenis_kartu="nik"){
        $appLink = "v2/peserta/".$jenis_kartu."/".$NIK;
        $response = $this->sendGetRequest($appLink);
        return $response;
    }
    

}