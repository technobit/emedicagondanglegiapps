<?php
require_once("Bridge.php");
class PoliBPJS extends Bridge{

    function __construct(){
        
    }   

    function getFKTP(){
        $appLink = "v1/poli/fktp/";
        $response = $this->sendGetRequest($appLink);
        return $response;
    }

    function getFKTL($start , $limit){
        $appLink = "v1/poli/fktl/".$start."/".$limit;
        $response = $this->sendGetRequest($appLink);
        return $response;
    }

    function getProvider($start , $limit){
        $appLink = "v1/provider/".$start."/".$limit;
        $response = $this->sendGetRequest($appLink);
        return $response;
    }

    function getListProviderAll( $list = 1000){
        $arrData = array();
        $retApi = $this->getProvider( 0 , $list);
        $status = $retApi['status'];

        if($status){
            $data = $retApi['data'];
            $jumlah = $data['count'];
            $listData = $data['list'];
            foreach($listData as $rowData){
                $arrData[$rowData['kdProvider']] = $rowData['nmProvider'];
            }
        }

        return $arrData;
    }

    function getListFKTL( $list = 1000){
        $arrData = array();
        $retApi = $this->getFKTL( 0 , $list);
        $status = $retApi['status'];

        if($status){
            $data = $retApi['data'];
            $jumlah = $data['count'];
            $listData = $data['list'];
            foreach($listData as $rowData){
                $arrData[$rowData['kdPoli']] = $rowData['nmPoli'];
            }
        }

        return $arrData;
    }
}