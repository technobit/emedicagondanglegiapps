<?php 
if (!defined('BASEPATH')) exit('No direct script access allowed');  

use Mike42\Escpos\PrintConnectors\FilePrintConnector;
use Mike42\Escpos\Printer;

class Printerthermal {
    var $printer = "";     
    function connectPrint($printerResource){
        $connector = new FilePrintConnector($printerResource);
        $printer = new Printer($connector);
        $this->printer = $printer;
    }

    function setJustification($mode){
        $justification = array(
        "left" => Printer::JUSTIFY_LEFT,
        "center" => Printer::JUSTIFY_CENTER,
        "right" => Printer::JUSTIFY_RIGHT,
        );
        $justification = isset($justification[$mode]) ? $justification[$mode] : "";
        $this->printer->setJustification($justification);
    }

    function selectPrintMode($mode = ""){
        $arrVal = array(
        "font_b" => Printer::MODE_FONT_B,
        "font_emphasiz" => Printer::MODE_EMPHASIZED,
        "font_double_height" => Printer::MODE_DOUBLE_HEIGHT,
        "font_double_width" => Printer::MODE_DOUBLE_WIDTH,
        "font_underline" => Printer::MODE_UNDERLINE,
        );
        $value = isset($arrVal[$mode]) ? $arrVal[$mode] : $mode;
        $this->printer->selectPrintMode($value);
    }

    function setFont($mode= ""){
        $arrVal = array(
        "font_a" => Printer::FONT_A,
        "font_b" => Printer::FONT_B,
        "font_c" => Printer::FONT_C,
        );

        $value = isset($arrVal[$mode]) ? $arrVal[$mode] : $mode;
        $this->printer->setFont($value);
    }

    function setUnderline($mode){
        $arrVal = array(
        "1" => Printer::UNDERLINE_NONE,
        "2" => Printer::UNDERLINE_SINGLE,
        "3" => Printer::UNDERLINE_DOUBLE,
        );
        $value = isset($arrVal[$mode]) ? $arrVal[$mode] : 0;
        $this->printer->setUnderline($value);
    }

    function setText($text){
        $this->printer->text($text);
    }

    function setEmphasis($status = false){
        $this->printer->setEmphasis($status);
    }

    function setCut(){
        $this->printer->cut();   
        $this->printer->close();
    }

    function setFeed($number = 1){
        $this->printer->feed($number);  
    }

    
}

