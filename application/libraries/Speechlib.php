<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Speechlib {

    var $pola_suara = array(
            0=>'nomor antrian {urutan} silakan menuju ke {pelayanan_tujuan}',
            1=>'nomor antrian {urutan} {pelayanan_asal} silakan menuju ke {pelayanan_tujuan}',
            2=>'nomor antrian {urutan} silakan menuju ke ruang {ruang_tujuan}',
            3=>'nomor antrian {urutan} {pelayanan_asal} silakan menuju ke ruang {ruang_tujuan}',
            4=>'nomor antrian {urutan} silakan menuju ke {pelayanan_tujuan} ruang {ruang_tujuan}',
            5=>'nomor antrian {urutan} silakan menuju ke {pelayanan_tujuan}',
    );
    var $daftar = array();
    var $voice_dir = "";
    var $dasar_angka = "";
    var $dasar_kata = "";
    ///var $dasar_angka = "voice_lib".DIRECTORY_SEPARATOR."suara".DIRECTORY_SEPARATOR."ttsfemale-googleid". DIRECTORY_SEPARATOR ."angka-%s.mp3";
    ///var $dasar_kata = 'voice_lib'.DIRECTORY_SEPARATOR.'suara'.DIRECTORY_SEPARATOR.'ttsfemale-googleid'.DIRECTORY_SEPARATOR.'kata-%s.mp3';
    ///var $tujuan ='voice_lib'.DIRECTORY_SEPARATOR.'cache'.DIRECTORY_SEPARATOR.'suara-%1$s.mp3';
    var $tujuan = "";

    function __construct(){
        require("voice_lib/angkaSeribu.class.php");
        require("voice_lib/mp3.class.php");
        $this->voice_lib = str_replace("/",DIRECTORY_SEPARATOR,FCPATH)."libraries".DIRECTORY_SEPARATOR."voice_lib";
        $this->voice_dir = "suara/ttsfemale-googleid/";
        $this->dasar_angka = $this->voice_dir."angka-%s.mp3";
        $this->dasar_kata = $this->voice_dir.'kata-%s.mp3';;
        $this->tujuan = 'cache/suara-%1$s.mp3';
        
        $this->daftar['pelayanan'] = array(
            1=>'Poli Gigi dan Mulut',
            2=>'Poli Anak',
            3=>'Poli Ibu',
            4=>'Poli K.I.A.',
            5=>'Poli Gigi',
            6=>'Poli Mata',
            7=>'Poli T.H.T.',
            8=>'Poli Kandungan',
            9=>'Poli Jantung',
            10=>'Poli Paru',
            11=>'Poli Orthopedi',
            12=>'Poli Penyakit Dalam',
            13=>'Poli Kulit dan Kelamin',
            14=>'Poli Onkologi',
            15=>'Poli Urologi',
            16=>'Poli Syaraf',
            17=>'Poli Rehabilitasi Medis',
            18=>'Poli Bedah Umum',
            19=>'Poli Bedah Anak',
            20=>'Poli Bedah Mulut',
            21=>'Poli Bedah Plastik',
            22=>'Poli Bedah Syaraf',
            23=>'Poli Bedah Digestif',
            24=>'Laboratorium',
            25=>'Apotek',
            26=>'Kasir',
            27=>'Customer Service',
            28=>'Poli Umum 1',
            29=>'Poli Umum 2',
            30=>'Poli Gizi',
            31=>'Poli Umum',
            32=>'Poli Lansia Dan Difabel',
            33=>'Ruangan Umum',
            34=>'Ruangan Lansia Dan Difabel',
            35=>'Ruangan Gizi',
            36=>'Ruangan Ibu',
            37=>'Ruangan Anak',
            38=>'Ruangan Gigi dan Mulut',
            39=>'Loket Umum',
            40=>'Loket BPJS',
            41=>'Loket Rawat Inap',
        );
    }

    function initialize($data){
        $permintaan=array(
            'pilihan_format'=>        (int)@$data['pilihan_format'],
            'urutan'=>          array((int)@$data['urutan'], 'angka'),
            'pelayanan_asal'=>  array((int)@$data['pelayanan_asal'], 'daftar', 'pelayanan'),
            'pelayanan_tujuan'=>array((int)@$data['pelayanan_tujuan'], 'daftar', 'pelayanan'),
            'ruang_asal'=>      array((int)@$data['ruang_asal'], 'angka'),
            'ruang_tujuan'=>    array((int)@$data['ruang_tujuan'], 'angka'),
        );

        $nama_berkas ="format-";
        $nama_berkas .="pf{$permintaan['pilihan_format']}";
        $nama_berkas .="u{$permintaan['urutan'][0]}";
        $nama_berkas .="pa{$permintaan['pelayanan_asal'][0]}";
        $nama_berkas .="pt{$permintaan['pelayanan_tujuan'][0]}";
        $nama_berkas .="ra{$permintaan['ruang_asal'][0]}";
        $nama_berkas .="rt{$permintaan['ruang_tujuan'][0]}";

        $hasil_tujuan=sprintf($this->tujuan, $nama_berkas);
        
        $mp3  = new Mp3();
        $angkaSeribu = new angkaSeribu();
        $daftar_berkas=array();

        $pilihan_format=$this->pola_suara[$permintaan['pilihan_format']];
        
        unset($permintaan['pilihan_format']);
        foreach(explode(" ",$pilihan_format) as $format_kata){
			$format_kata_potong=substr($format_kata, 1, -1);
			if(
				'{'.$format_kata_potong.'}' == $format_kata
				&& array_key_exists($format_kata_potong, $permintaan)
			){
				// Bila dalam pola permintaan.
				$permintaan_pilihan=$permintaan[$format_kata_potong];
				$permintaan_nilai=@$permintaan_pilihan[0];
				$permintaan_jenis=@$permintaan_pilihan[1];
				$permintaan_sasaran=@$permintaan_pilihan[2];
				
				// Bila kosong.
				if(!$permintaan_nilai)
					continue;
				
				if($permintaan_jenis == 'angka'){
					// Angka.
					$angkaSeribu->formatAngkaRibu($permintaan_nilai);
					foreach($angkaSeribu->urutan_angka as $angka){
						$angka=strtolower(trim($angka));
						$daftar_berkas[]=sprintf($this->dasar_angka, $angka);
					}
				}elseif(
					$permintaan_pilihan[1] == 'daftar'
					&& array_key_exists($permintaan_sasaran, $this->daftar) 
				){
					// Ambil nilai.
					$daftar_pilihan=$this->daftar
						[$permintaan_sasaran]
						[$permintaan_nilai];
					
					// Kalimat.
					foreach(explode(" ", $daftar_pilihan) as $kata){
						$kata=strtolower(trim($kata));
						$daftar_berkas[]=sprintf($this->dasar_kata, $kata);
					};
				};
			}else{
				// Langsung masukkan sebagai kalimat.
				foreach(explode(' ', $format_kata) as $kata){
					$kata=strtolower(trim($kata));
					$daftar_berkas[]=sprintf($this->dasar_kata, $kata);
				};
			};
		};
        
        $berkas_ada=count($daftar_berkas);
        foreach($daftar_berkas as $berkas){
            //echo FCPATH."libraries";
            ///$berkas = FCPATH."libraries".DIRECTORY_SEPARATOR.$berkas;
            $mp3->addFile($berkas);
        }
        
        if($berkas_ada){
               
            ///$mp3->savefile($hasil_tujuan);
            ///chmod($hasil_tujuan, 0666);
            $mp3->output("suara.mp3");
        };
        exit;
        }

        function getOutput(){
            
        }
}