<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Bridging_api {
    
    var $consumerID = "";
    var $consumerSecret = "";
    var $APILink = "";
    var $statusApps = "";
    var $isConnected = "";
    var $failConnectedResponse = array();
    var $failSaveConnectedResponse = array();
    var $response = array();
    

    function __construct(){
        /// For Start Awal Before Cloud Mode
        $this->statusApps = BPJS_STATUS_API;
        $this->APILink = BPJS_API_LINK_PRODUCTION;
        $this->consumerID = CONS_ID;
        $this->consumerKey = CONS_KEY;
        $this->checkKoneksiBPJS();
    }


    private function getSignatureHeader(){
        $data = $this->consumerID;
	    $secretKey = $this->consumerKey;

        date_default_timezone_set('UTC'); 
        $tStamp = strval(time()-strtotime('1970-01-01 00:00:00'));
        $signature = hash_hmac('sha256', $data ."&".$tStamp , $secretKey, true);
        $encodedSignature = base64_encode($signature);
        $string = "cGttX2J1bHVsYXdhbmc6YnVsdWxhd2FuNjowOTU";
        $stringData = base64_decode($string);
        
       /// $encodedSignature = urlencode($encodedSignature);
        $userNamePcare = PCARE_USER;
        $passPcare = PCARE_PASS;
        $xAuthString = $userNamePcare.":".$passPcare.":095";
        $xAuth = "Basic ".base64_encode($xAuthString);
        $result['xConsId'] = $data;
        $result['xTimeStamp'] = $tStamp;
        $result['xSignature'] = $encodedSignature;
        $result['xAuth'] = $xAuth;
        return $result;
    }

    private function getResult($result){
        
        $result = json_decode($result , true);

        $status = true;
        $message = "success";
        $code = $result['metaData']['code'];
        $status = $code=="200" ? true : false;
        switch($status){
            case "200" : $status=true;
                         $message="success";
                         break;
            case "204" : $status=false;
                         $message="Data Tidak Tersedia (".$result['response']['message'].")";
                         break;
            case "500" : $status=false;
                         $message="Internal Server Error - Server BPJS Mengalami Gangguan (".$result['response']['message'].")";
                         break;
            case "401" : $status=false;
                         $message="User Password Salah (".$result['response']['message'].")";
                         break;
            case "408" : $status=false;
                         $message="Request Time Out (".$result['response']['message'].")";
                         break;
            case "424" : $status=false;
                         $message="Failed Dependency (".$result['response']['message'].")";
                         break;
            case "412" : $status=false;
                         $message="Field Harus Di isi (".$result['response']['message'].")";
                         break;
            default :    $status=false;
                         $message="Error Pada Sistem (".$result['response']['message'].")";
                         break;
        }

        $data = $result['response'];
        $retVal = array();
        $retVal['status'] = $status;
        $retVal['message'] = $message;
        $retVal['data'] = $data;
        
        return $retVal;
    }

        

    public function sendPostRequest($link ,$arrData){
        $retVal = array();
        if(!$this->isConnected){
            $retVal['status'] = false;
            $retVal['message'] = "Tidak Terkoneksi";
            $retVal['data'] = array();
            return $retVal;die;
        }
        $headerSend = $this->getSignatureHeader();
        $url = $this->APILink . $link;
        $opts = array(
            'http'=>array(
            'method'=>"POST",
            'content' => http_build_query($arrData),
            'header'=>"Host: api.bpjs-kesehatan.go.id\r\n".
            "X-cons-id: ".$headerSend['xConsId']."\r\n".
            "X-timestamp: ".$headerSend['xTimeStamp']."\r\n".
            "X-signature: ".$headerSend['xSignature']."\r\n".
            "X-Authorization: ".$headerSend['xAuth']."\r\n"
        ));
        
        $context = stream_context_create($opts);
        $result = @file_get_contents($url, false, $context);
        $retVal = $this->getResult($result);
        return $retVal;
    }

    public function sendGetRequest($link ){
        $retVal = array();
        if(!$this->isConnected){
            $retVal['status'] = false;
            $retVal['message'] = "Tidak Terkoneksi";
            $retVal['data'] = array();
            return $retVal;die;
        }
        $headerSend = $this->getSignatureHeader();
        $url = $this->APILink . $link;
        $opts = array(
            'http'=>array(
           'method'=>"GET",
            'header'=>"Host: api.bpjs-kesehatan.go.id\r\n".
            "X-cons-id: ".$headerSend['xConsId']."\r\n".
            "X-timestamp: ".$headerSend['xTimeStamp']."\r\n".
            "X-signature: ".$headerSend['xSignature']."\r\n".
            "X-Authorization: ".$headerSend['xAuth']."\r\n"
        ));
        
        $context = stream_context_create($opts);
        $result = @file_get_contents($url, false, $context);
        $retVal = $this->getResult($result);
        return $retVal;
    }

    private function checkKoneksiBPJS(){
		$retVal = array();
		
        $connected = @fsockopen("pcare.bpjs-kesehatan.go.id", 80); 
        if(!$connected){
            $this->isConnected = false;
            $this->failConnectedResponse = array(
                "status" => false,
                "message" => "Koneksi Internet Tidak Tersedia - Gagal Menyambungkan Ke Server BPJS"
            );
            $this->failSaveConnectedResponse = array(
                "status" => false,
                "message" => "Koneksi Internet Tidak Tersedia - Pendaftaran Akan Di Sinkronisasi kan ke Server Jika Koneksi Telah Tersedia"
            );

        }else{

            $this->isConnected = true;
            fclose($connected);
        }
    }

    //// Peserta
    public function checkPasienByNoAnggota($noAnggota){
        $appLink = "v2/peserta/".$noAnggota;
        $response = $this->sendGetRequest($appLink);
        return $response;
    }
    
}