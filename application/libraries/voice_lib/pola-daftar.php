<?php

// Pilihan format.
$pola_suara=array(
	0=>'nomor antrian {urutan} silakan menuju ke {pelayanan_tujuan}',
	1=>'nomor antrian {urutan} {pelayanan_asal} silakan menuju ke {pelayanan_tujuan}',
	2=>'nomor antrian {urutan} silakan menuju ke ruang {ruang_tujuan}',
	3=>'nomor antrian {urutan} {pelayanan_asal} silakan menuju ke ruang {ruang_tujuan}',
);

// Pelayanan.
$daftar_daftar=array();
$daftar_daftar['pelayanan']=array(
	1=>'Poli Gigi dan Mulut',
	2=>'Poli Anak',
	3=>'Poli Ibu',
	4=>'Poli K.I.A.',
	5=>'Poli Gigi',
	6=>'Poli Mata',
	7=>'Poli T.H.T.',
	8=>'Poli Kandungan',
	9=>'Poli Jantung',
	10=>'Poli Paru',
	11=>'Poli Orthopedi',
	12=>'Poli Penyakit Dalam',
	13=>'Poli Kulit dan Kelamin',
	14=>'Poli Onkologi',
	15=>'Poli Urologi',
	16=>'Poli Syaraf',
	17=>'Poli Rehabilitasi Medis',
	18=>'Poli Bedah Umum',
	19=>'Poli Bedah Anak',
	20=>'Poli Bedah Mulut',
	21=>'Poli Bedah Plastik',
	22=>'Poli Bedah Syaraf',
	23=>'Poli Bedah Digestif',
	24=>'Laboratorium',
	25=>'Apotek',
	26=>'Kasir',
	27=>'Customer Service',
	28=>'Poli Umum',
);

?>