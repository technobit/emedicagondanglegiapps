<?php
/**
  $angka=new angkaSeribu();
  ceho $angka->formatAngkaRibu($anka,$panjang,$minimal);
 */
class angkaSeribu{
	public $urutan_angka=array();
	protected $satuan = array(
		"","ribu "/*10^3*/,"juta "/*10^6*/,
		"milyar "/*10^9*/,"triliun "/*10^12*/,
		"dwiyar "/*10^15*/,"trita "/*10^18*/,
		"triyar "/*10^21*/,"caturta "/*10^24*/,
		"caturyar "/*10^27*/,"pancata "/*10^30*/,
		"pancayar "/*10^33*/,"sadta "/*10^36*/,
		"sadyar "/*10^39*/,"saptata "/*10^42*/,
		"saptayar "/*10^45*/,"hastata "/*10^48*/,
		"hastayar "/*10^51*/,"nawata "/*10^54*/,
		"nawayar "/*10^57*/,"dasata "/*10^60*/,
		"dasayar "/*10^63*/
		);
	public function formatAngkaRibu($angka,$panjang=true,$minimal=0) {
		$satuan=$this->satuan;
		$this->urutan_angka=array();
		/*Inisiasi*/
		$x=$angka;
		$tanda=false;
		$teks="";
		
		/*Mulai program*/
		if($x==0)return "nol";
		if($angka<$minimal)return $angka;
		if(!$panjang)return $this->angkaTunggal($angka);
		
		if($x<2000)$tanda=true;
		
		if($x>=pow(10,3*count($satuan)))return "Galat: Terlalu besar. ";
		
		for($i=count($satuan);$i>=1;$i--){
			$tampung=floor($x/pow(10,3*$i));
			if($tampung>0){
				$bagian=$this->ratusan($tampung,$tanda);
				$teks.=$bagian.$satuan[$i];
				trim($satuan[$i]) && $this->urutan_angka[]=$satuan[$i];
			};
			$x-=$tampung*pow(10,3*$i);
		};
		
		$teks.=$this->ratusan($x,false);
		return $teks;
	}
	private function angkaTunggal($ank,$p=null){
		$satuan=$this->satuan;
		if(!$p&&$p!==0){
			$c=0;
			foreach($satuan as $i => $v) {
				if(($ank/ pow(1000,$i)) >= 1) {
					$r["angka"] = $ank / pow(1000,$i);
					$r["satuan"] = $v;
					$c++;
				};
			};
			$depan=number_format($r["angka"],0,",", ".");
			$akhir=$r["satuan"];
		}else {
			$depan=number_format($ank / pow(1000,$p),",", ".");
			$akhir=$satuan[$p];
		};
		trim($depan) && $this->urutan_angka[]=$depan;
		trim($akhir) && $this->urutan_angka[]=$akhir;
		return $depan . " ". $akhir;
	}
	private function ratusan($y,$bendera=false){
		$bilang="";
		$angka=array('','se','dua ','tiga ','empat ','lima ',
			'enam ','tujuh ','delapan ','sembilan ');
		$satuan=array('','puluh ','ratus ');
		
		for($j=2;$j>=1;$j--){
			$tmp=floor($y/pow(10,$j));
			if($tmp>0){
				$bag=$angka[$tmp];
				if($j==1&&$tmp==1){
					$y-=$tmp*pow(10,$j);
					if($y>=1){
						$satuan[$j]="belas ";
					}else{
						$angka[$y]="se";
					};
					
					trim($angka[$y]) && $this->urutan_angka[]=$angka[$y];
					trim($satuan[$j]) && $this->urutan_angka[]=$satuan[$j];
					$bilang.=$angka[$y].$satuan[$j];
					
					return $bilang;
				}else{
					trim($bag) && $this->urutan_angka[]=$bag;
					trim($satuan[$j]) && $this->urutan_angka[]=$satuan[$j];
					$bilang.=$bag.$satuan[$j];
				}
			};
			$y-=$tmp*pow(10,$j);
		};
		
		if(!$bendera){
			$angka[1]="satu ";
			// $this->urutan_angka[]=$angka[1];
		}
		trim($angka[$y]) && $this->urutan_angka[]=$angka[$y];
		$bilang.=$angka[$y];
		return $bilang;
	}
};

?>
