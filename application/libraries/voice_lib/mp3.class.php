<?php
/*
// Berkas pertama.
$mp3 = new mp3();
$mp3->addFile('1.mp3');
$mp3->addFile('2.mp3');

$mp3->output('word.mp3');
DAN/ATAU
$mp3->savefile('/lajur/direktori/berkas.mp3');
*/
class Mp3{
	public $str;
	public $time;
	public $frames;
	
	// Menambah berkas MP3 baru.
	public function addFile($path){
		if(!$this->str){
			$this->str = file_get_contents($path , FILE_USE_INCLUDE_PATH);
		}else{
			$this->str .= file_get_contents($path , FILE_USE_INCLUDE_PATH);
		}
		$this->striptags();
	}
	
	// Menghitung akhiran berkas suara.
	public function getIdvEnd(){
		$strlen = strlen($this->str);
		$str = substr($this->str,($strlen-128));
		$str1 = substr($str,0,3);
		if(strtolower($str1) == strtolower('TAG')){
			return $str;
		}else{
			return false;
		}
	}
	
	// Menghitung awalan berkas suara.
	public function getStart(){
		$strlen = strlen($this->str);
		for($i=0;$i<$strlen;$i++){
			$v = substr($this->str,$i,1);
			$value = ord($v);
			if($value == 255){
				return $i;
			}
		}
	}
	
	// Membuang etiket ID3.
	public function striptags(){
		// Membuang awalan.
		$newStr = '';
		$s = $start = $this->getStart();
		if($s===false){
			return false;
		}else{
			$this->str = substr($this->str,$start);
		}
		
		// Membuang akiran.
		$end = $this->getIdvEnd();
		if($end!==false){
			$this->str = substr($this->str,0,(strlen($this->str)-129));
		}
	}
	
	// Menampilkan kesalahana
	private function error($msg){
		// Galat.
		die('<strong>Kesalahan berkas suara: </strong>'.$msg);
	}
	
	// Menyimpan berkas MP3.
	public function savefile($path){
		return file_put_contents($path, $this->str , FILE_USE_INCLUDE_PATH);
	}
	
	 // Mengirim ke klien.
	public function output($filename){
		// Keluaran.
		// Mengirim ke stdout.
		if(ob_get_contents())
			$this->error('Terdapat data yang telah ditampilkan, tidak dapat mengirim berkas MP3.');
		if(php_sapi_name()!='cli'){
			
			// Jenis berkas.
			header('Content-Type: audio/mpeg3');
			
			// Bila terjadi kesalahan.
			if(headers_sent())
				$this->error('Terdapat data yang telah dikirim, tidak dapat mengirim berkas MP3.');
			
			// Lainnya.
			header('Content-Length: '.strlen($this->str));
			header('Content-Disposition: attachment; filename="Antrian.mp3"');
		}
		echo $this->str;
		return true;
	}
}
?>
