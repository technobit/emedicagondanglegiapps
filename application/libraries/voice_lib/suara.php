<?php
include("angkaSeribu.class.php");
include("mp3.class.php");

// Pengaturan dasar.
$dasar_angka='suara/ttsfemale-googleid/angka-%s.mp3';
$dasar_kata='suara/ttsfemale-googleid/kata-%s.mp3';
$tujuan='tembolok/suara-%1$s.mp3';

// Pola dan daftar.
require_once('./pola-daftar.php');

// Permintaan.
$kalimat=@$_REQUEST['string'];
$data_req=@$_REQUEST['data'];
if(trim($data_req)){
	if(($data = json_decode($data_req, true))===NULL
		&& json_last_error() !== JSON_ERROR_NONE
	){
		die("Gagal mengenali data.");
	};

	// Permintaan.
	$permintaan=array(
		'pilihan_format'=>        (int)@$data['pf'],
		'urutan'=>          array((int)@$data['ur'], 'angka'),
		'pelayanan_asal'=>  array((int)@$data['pa'], 'daftar', 'pelayanan'),
		'pelayanan_tujuan'=>array((int)@$data['pt'], 'daftar', 'pelayanan'),
		'ruang_asal'=>      array((int)@$data['ra'], 'angka'),
		'ruang_tujuan'=>    array((int)@$data['rt'], 'angka'),
	);
	
	// Nama berkas.
	$nama_berkas ="format-";
	$nama_berkas .="pf{$permintaan['pilihan_format']}";
	$nama_berkas .="u{$permintaan['urutan'][0]}";
	$nama_berkas .="pa{$permintaan['pelayanan_asal'][0]}";
	$nama_berkas .="pt{$permintaan['pelayanan_tujuan'][0]}";
	$nama_berkas .="ra{$permintaan['ruang_asal'][0]}";
	$nama_berkas .="rt{$permintaan['ruang_tujuan'][0]}";
}elseif(trim($kalimat)){
	$cincangan=dechex(crc32($kalimat));
	$nama_berkas="kalimat-$cincangan";
}else{
	exit;
}

// Hasil.
$hasil_tujuan=sprintf($tujuan, $nama_berkas);

$mp3=new Mp3();
if(is_readable($hasil_tujuan)){
	$mp3->addFile($hasil_tujuan);
	$mp3->output("suara.mp3");
	exit;
}else{
	
	// Membangun.
	$daftar_berkas=array();
	if(trim($data_req)){
		
		$angkaSeribu=new angkaSeribu();
		
		// Pola.
		$pilihan_format=$pola_suara[$permintaan['pilihan_format']];
		
		// Sudah tidak dibutuhkan.
		unset($permintaan['pilihan_format']);
		
		// Memecah.
		foreach(explode(" ",$pilihan_format) as $format_kata){
			$format_kata_potong=substr($format_kata, 1, -1);
			if(
				'{'.$format_kata_potong.'}' == $format_kata
				&& array_key_exists($format_kata_potong, $permintaan)
			){
				// Bila dalam pola permintaan.
				$permintaan_pilihan=$permintaan[$format_kata_potong];
				$permintaan_nilai=@$permintaan_pilihan[0];
				$permintaan_jenis=@$permintaan_pilihan[1];
				$permintaan_sasaran=@$permintaan_pilihan[2];
				
				// Bila kosong.
				if(!$permintaan_nilai)
					continue;
				
				if($permintaan_jenis == 'angka'){
					// Angka.
					$angkaSeribu->formatAngkaRibu($permintaan_nilai);
					foreach($angkaSeribu->urutan_angka as $angka){
						$angka=strtolower(trim($angka));
						$daftar_berkas[]=sprintf($dasar_angka, $angka);
					}
				}elseif(
					$permintaan_pilihan[1] == 'daftar'
					&& array_key_exists($permintaan_sasaran, $daftar_daftar) 
				){
					// Ambil nilai.
					$daftar_pilihan=$daftar_daftar
						[$permintaan_sasaran]
						[$permintaan_nilai];
					
					// Kalimat.
					foreach(explode(" ", $daftar_pilihan) as $kata){
						$kata=strtolower(trim($kata));
						$daftar_berkas[]=sprintf($dasar_kata, $kata);
					};
				};
			}else{
				// Langsung masukkan sebagai kalimat.
				foreach(explode(' ', $format_kata) as $kata){
					$kata=strtolower(trim($kata));
					$daftar_berkas[]=sprintf($dasar_kata, $kata);
				};
			};
		};
		
	}else{
		// Kalimat.
		$kalimat=strtolower($kalimat);
		foreach(explode(' ', $kalimat) as $kata){
			$daftar_berkas[]=sprintf($dasar_kata, $kata);
		};
	};
	
	// Memasukkan berkas.
	$berkas_ada=count($daftar_berkas);
	foreach($daftar_berkas as $berkas){
		if(!is_readable($berkas)){
			$berkas_ada--;
			continue;
		};
		$mp3->addFile($berkas);
	}
	
	if($berkas_ada){
		$mp3->savefile($hasil_tujuan);
		chmod($hasil_tujuan, 0666);
		$mp3->output("suara.mp3");
	};
	exit;
};
?>