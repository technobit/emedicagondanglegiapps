<?php
class MY_Model extends CI_Model {
    
    var $service = "CallSpExcecution";
    public function __construct(){
        parent::__construct();
              
    }
    
    function retrieveData($parameter , $service = ""){
        $service_exec = !empty($service) ? $service : $this->service;
        $retVal = $this->soap_library->soap_request($service_exec , $parameter);
        return $retVal;
    }

    function getMessage($type = "insert",$status , $additional_string = ""){
        $message = "";
        if($status){
            switch($type){
                case "insert" : $message = "Data Berhasil Di Masukan";break;
                case "update" : $message = "Data Berhasil Di Update";break;
                case "delete" : $message = "Data Berhasil Di Hapus";break;
            }
            if(!empty($additional_string)){
                $message .= $message;
            }
        }else{
            $message = "Data Gagal Di Update";
            if(!empty($additional_string)){
                $message .= $message;
            }
        }
        return $message;
    }

    function getMessageInsert($status , $additional_string = ""){
        if($status){
            $message = "Data Berhasil Di Masukan";
            if(!empty($additional_string)){
                $message .= $message;
            }
        }else{
            $message = "Data Gagal Di Masukan";
            if(!empty($additional_string)){
                $message .= $message;
            }
        }
        return $message;
    }

    function getMessageDelete($status , $additional_string = ""){
        
    }
}