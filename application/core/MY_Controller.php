<?php
class MY_Controller extends CI_Controller {
	var $base_url_site = "";
	var $user_logged_level = "";
	var $cms_title = "";
	var $cms_active_config = array();
	var $datetime_system = "";
	var $dataProfil = array();
	var $logo = "";
	function __construct(){
		parent::__construct();
		$this->load->library("breadcrumbs");
		$this->base_url_site = base_url();
		$this->_check_language();
		$result = $this->db->get('pengaturan_profil');
		$this->dataProfil = $result->row_array();
		$this->datetime_system = date("Y-m-d H:is");
		$this->logo = !empty($this->dataProfil['logo_faskes'])?$this->dataProfil['logo_faskes']:ASSETS_URL."img/emedica_white.png";
	}
	
	public function _render($template,$cnf = array()){
		$this->_checkValidateUser();
		$template = (isset($cnf['template'])) ? $cnf['template'] : 'template/default';
        //loop for css custom file
        $css_script = '';
        if (isset($cnf['custom_css']) && is_array($cnf['custom_css']))
        {
            foreach ($cnf['custom_css'] as $val)
            {
                $css_script .= "<link rel=\"stylesheet\" href=\"" . $val . "\" />\n\t";
            }
        }
        else
        {
            $css_script = isset($cnf['custom_css']) ? isset($cnf['custom_css']) : '';
        }
        //end loop
        //loop for js custom file
        $js_script = '';
        if (isset($cnf['custom_js']) && is_array($cnf['custom_js']))
        {
            foreach ($cnf['custom_js'] as $val)
            {
                $js_script .= "<script type=\"text/javascript\" src=\"" . $val . "\"></script>\n\t\t";
            }
        }
        else
        {
            $js_script = isset($cnf['custom_js']) ? isset($cnf['custom_js']) : '';
        }
        //end loop
        $main_content = (isset($cnf['container'])) ? $cnf['container'] : '';
		$menu_key_app = (isset($cnf['menu_key'])) ? $cnf['menu_key'] : '';
		$menu_key = APP_STATUS_AKSES==1 ? 'dashboard' : $menu_key_app;
		///$menu_key = "dashboard";
		$akses_key = (isset($cnf['akses_key'])) ? $cnf['akses_key'] : 'is_view';

		if(empty($menu_key)){
			show_404();
		}

		/// Cek Apakah Menu Dapat Diakses Atau TIdak
		if($menu_key!="dashboard"){
			$isViewMenu = $this->checkMenu($menu_key , $akses_key);
			
			if(!$isViewMenu){
				$meta_title = isset($cnf['title']) ? $cnf['title'] : "";
				$dt['meta_title'] = $meta_title;
				$main_content = $this->load->view("template/ilegal" , $dt , true);	
			}
		}
		
        $meta_refresh = '1800';
        if(isset($cnf['meta_refresh'])){
            $meta_refresh = '<meta http-equiv="Refresh" content="'.$cnf['meta_refresh'].'" />';
        }
		$site_name = "eMedika";

        $subCat = (isset($cnf['sub_categori'])) ? $cnf['sub_categori'] : '';
        // set expire
        $my_time = time();
        $expired_header = gmdate('D, d M Y H:i:s', $my_time + 120) . " GMT";
		$company = 'Technobit.id';
        $dt = array(
            'meta_title' => isset($cnf['title']) ? $cnf['title'] : $site_name,
            'description' => isset($cnf['description']) ? $cnf['description'] : '',
            'keywords' => isset($cnf['keywords']) ? $cnf['keywords'] : '',
            'url' => isset($cnf['url']) ? $cnf['url'] : '',
            'og_image' => isset($cnf['og_image']) ? $cnf['og_image'] : '',
            'meta_refresh' => $meta_refresh,
            'expires' => '',
			'header_title' => (isset($cnf['header'])) ? $cnf['header'] : '', 
            'custom_css' => $css_script,
			'assets_css_url' => ASSETS_CSS_URL,
			'assets_js_url' => ASSETS_JS_URL,
			'assets_image_url' => ASSETS_IMAGE_URL,
            'custom_js' => $js_script,
			'base_url_admin' => $this->base_url_site,
            'main_content' => $main_content,
			"top_navbar" => $this->_get_header_nav(),
			"side_navbar" => $this->_get_sidebar_nav(),
			"main_footer" => $this->_get_footer(),
        );
		$this->load->view($template,$dt);
	}

	public function _render_antrian($template,$cnf = array()){
		$template = (isset($cnf['template'])) ? $cnf['template'] : 'template/default_antrian';
        //loop for css custom file
        $css_script = '';
        if (isset($cnf['custom_css']) && is_array($cnf['custom_css']))
        {
            foreach ($cnf['custom_css'] as $val)
            {
                $css_script .= "<link rel=\"stylesheet\" href=\"" . $val . "\" />\n\t";
            }
        }
        else
        {
            $css_script = isset($cnf['custom_css']) ? isset($cnf['custom_css']) : '';
        }
        //end loop
        //loop for js custom file
        $js_script = '';
        if (isset($cnf['custom_js']) && is_array($cnf['custom_js']))
        {
            foreach ($cnf['custom_js'] as $val)
            {
                $js_script .= "<script type=\"text/javascript\" src=\"" . $val . "\"></script>\n\t\t";
            }
        }
        else
        {
            $js_script = isset($cnf['custom_js']) ? isset($cnf['custom_js']) : '';
        }
        //end loop
		/// Cek Apakah Menu Dapat Diakses Atau TIdak
		$main_content = (isset($cnf['container'])) ? $cnf['container'] : '';
        $meta_refresh = '1800';
        if(isset($cnf['meta_refresh'])){
            $meta_refresh = '<meta http-equiv="Refresh" content="'.$cnf['meta_refresh'].'" />';
        }
		$site_name = "eMedica";

        $subCat = (isset($cnf['sub_categori'])) ? $cnf['sub_categori'] : '';
        // set expire
        $my_time = time();
        $expired_header = gmdate('D, d M Y H:i:s', $my_time + 120) . " GMT";
		$company = 'Technobit.id';
        $dt = array(
            'meta_title' => isset($cnf['title']) ? $cnf['title'] : $site_name,
			'header_title' => (isset($cnf['header'])) ? $cnf['header'] : '', 
            'custom_css' => $css_script,
			'assets_css_url' => ASSETS_CSS_URL,
			'assets_js_url' => ASSETS_JS_URL,
			'assets_image_url' => ASSETS_IMAGE_URL,
            'custom_js' => $js_script,
			'base_url_admin' => $this->base_url_site,
            'main_content' => $main_content,
			"top_navbar" => $this->_get_header_antrian(),
			"side_navbar" => $this->_get_sidebar_nav(),
			"main_footer" => $this->_get_footer(),
			"logo" => $this->logo
        );
		$this->load->view($template,$dt);
	}

	private function renderingTemplate(){

	}
		
	private function _get_header_nav(){
		$menu = array();
		$dataProfil = $this->session->userdata("pengaturan_profil");
		
		$menu['logo'] = !empty($dataProfil['logo_faskes']) ? $dataProfil['logo_faskes'] : ASSETS_URL."img/emedica_white.png";
		
		$menu['username'] = $this->session->userdata("sipt_username");
		$menu["useremail"] = $this->session->userdata("sipt_email");
		$menu["link_edit_profil"] = $this->base_url_site."user/edit/".$this->session->userdata("sipt_user_id");
		$menu["link_logout"] = base_url()."logout/";
		$ret = $this->load->view("template/navbar/topbar" , $menu , true);
		return $ret;
	}

	private function _get_header_antrian(){
		$menu = array(
			"logo" => $this->logo
		);
		$ret = $this->load->view("template/navbar/topbar_antrian" , $menu , true);
		return $ret;
	}

	public function generateIdKunjungan(){
		$rand_number = rand(1000 , 9999);
		$id = "KJN" . date("YmdHis").$rand_number;
		///$len = strlen($id);
		return $id;
	}
	
	private function _get_sidebar_nav(){
		$menu = array();
		$menu["sidebar_nav"] = $this->config->item("menu_nav");
		$menu["submenu_side"] = $this->config->item("submenu_nav");
		$menu["subsubmenu_side"] =$this->config->item("subsubmenu_nav");
		$ret = $this->load->view("template/navbar/sidebar" , $menu , true);
		return $ret;
	}
	
	private function _get_footer(){
		$dt = array();
		$ret = $this->load->view("template/footer" , $dt , true);
		return $ret;
	}
	
	public function setBreadcrumbs($arrBreadcrumbs = array()){
		$res = "";
		foreach($arrBreadcrumbs as $label=>$url){
			$this->breadcrumbs->add($label , $url);
		}
		$res = $this->breadcrumbs->output();
		return $res;
	}
	
	public function _checkValidateUser(){
		$base_url = base_url();
		$login = base_url()."login/";
		if(!isset($_SESSION["sipt_user_validated"])){
			redirect($login);
		}
	}
	
	private function _check_language(){
		$lang = $this->session->userdata("sipt_user_language");
		if($lang=="ID"){
			$this->lang->load('id_site', 'id');
		}else{
			$this->lang->load('en_site', 'en');
		}
	}

	public function convertPassword($txtPassword){
		$resPassword = "";
		if(!empty($txtPassword)){
			$resPassword = "3M3dic4".$txtPassword."st3v3j0b5";
			$resPassword = md5($resPassword);
		}

		return $resPassword;
	}

	public function checkMenu($menu , $action="is_view"){
		
		$session_menu = $this->session->userdata("sipt_session_menu");
		
		$retVal = isset($session_menu[$menu]);
		if(!isset($session_menu[$menu])){
			$retVal = false;
		}

		/*
		if($action!="is_view"){
			$retVal = $session_menu[$menu][$action]==1 ? true : false;
		}
		*/
		return $retVal;
	}

	public function checkMenuAjax($menu , $action){

		$retVal = array();
		$status = true;

		$message = "Anda Tidak Dapat Mengakses Fitur Ini";

		///$checkMenu = isset($session_menu[$menu]);
		if(!isset($session_menu[$menu])){
			$status = false;
		}

		if($action!="is_view"){
			$status = $session_menu[$menu][$action]==1 ? true : false;
		}
		$retVal['status'] = $status;
		$retVal['message'] = $message;
		return $retVal;
	}
	
	public function setJsonOutput($response){
		$this->output->set_content_type('application/json')->set_output(json_encode($response));
	}

	public function isAjaxRequest(){
		if(!$this->input->is_ajax_request()){
		
			echo "Ilegal";
			die;	
		}
	}
	public function setHeaderExcel($titleRecap , $start_date , $end_date){
        $this->excel->setActiveSheetIndex(0);
        $this->excel->getActiveSheet()->setTitle('Sheet 1');
        $this->excel->getActiveSheet()->setCellValue('A1', 'RSI Gondanglegi');
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(20);
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->mergeCells('A1:N1');
        $this->excel->getActiveSheet()->setCellValue('A2', $titleRecap);
        $this->excel->getActiveSheet()->setCellValue('A3', 'Tanggal Awal');
        $this->excel->getActiveSheet()->setCellValue('A4', 'Tanggal Akhir');
        $this->excel->getActiveSheet()->setCellValue('B3', $start_date);
        $this->excel->getActiveSheet()->setCellValue('B4', $end_date);
    }
	 public function getOutput( $filename){
        ///
        $filename = date('YmdHis')."-".$filename;
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'.xls"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $objWriter->save('php://output');
    }

	
}