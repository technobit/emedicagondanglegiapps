<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Keuangan_diluar extends MY_Controller {

    var $meta_title = "Keuangan Diluar";
    var $meta_desc = "Keuangan Diluar";
    var $main_title = "Keuangan Diluar";
    var $menu_key = "keuangan_luar_pelayanan";
    var $dtBreadcrumbs = array();  

    public function __construct() {
        parent::__construct();
        $this->dtBreadcrumbs = array(
            "Home" =>base_url(),
            "Keuangan" => base_url('keuangan'),
            "Diluar" => base_url('keuangan/diluar'),
        );        
        $this->load->model('item_keuangan_m');   
        $this->load->model('keuangan_diluar_m');
    }
    
    public function index() {        
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "menu_key" => $this->menu_key,
			"akses_key" => "is_view",
            "container" => $this->_index(),
            "custom_js" => array(
                ASSETS_JS_URL."keuangan/diluar/index.js"
            ),
            "custom_css" => array(),
        );  
        $this->_render("default",$dt);             
    }

    private function _index() {        
        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['title'] = $this->meta_title;
        return $this->load->view('keuangan_diluar/index', $dt, true);
    }

    public function get($status = 1) {        
        $this->load->library('datatables');
        $tanggal = $this->input->get('tanggal');
        $this->db->select('keuangan_diluar.*, (select sum((jasa_sarana + jasa_pelayanan) * jumlah) from keuangan_diluar_detail where id_keuangan_diluar = keuangan_diluar.id) as total, pegawai.txtNamaPegawai')
        ->join('users_profil', 'users_profil.intIdUser = keuangan_diluar.created_by')
        ->join('pegawai', 'pegawai.intIdPegawai = users_profil.intIdPegawai')
        ->from('keuangan_diluar');
        $query = $this->db->get_compiled_select();                            
        $response = $this->datatables->collection($query)
        ->orderableColumns('no_bukti, tanggal, txtNamaPegawai, total')
        ->searchableColumns('no_bukti, tanggal')  
        ->editColumn('total', function($row) {
            return toNumber($row->total);
        })      
        ->editColumn('tanggal', function($row) {
            return indonesian_date($row->tanggal);
        })
        ->addColumn('action', function($row) {

                return '<button onclick="cetakBuktiBayar('.$row->id.')" class="btn btn-default btn-flat btn-sm" type="button"><i class="fa fa-print"></i> Print</button> '.
                        '<a href="'.base_url('keuangan/diluar/edit/' . $row->id).'" class="btn btn-flat btn-warning btn-sm"><i class="fa fa-pencil"></i> Edit</a> '.
                       '<a href="'.base_url('keuangan/diluar/delete/' . $row->id).'" class="btn btn-flat btn-danger btn-sm" onclick="return confirm(\'Apakah anda yakin akan menghapus transaksi ini?\')"><i class="fa fa-trash"></i> Hapus</a>';                           
        })
        ->render();
        $this->output->set_content_type('application/json')->set_output(json_encode($response));
    }


    public function get_uang_masuk() {
        if ($this->input->get('tanggal')) {
            $tanggal = $this->input->get('tanggal');
        } else {
            $tanggal = date('Y-m-d');        
        }
        $response = array(            
            'uang_masuk_hari_ini' => $this->keuangan_diluar_m->getUangMasuk($tanggal)->row()->total_tagihan
        );
        $this->output->set_content_type('application/json')->set_output(json_encode($response));
    }

    public function create() { 
        if ($post = $this->input->post()) {
            $post['created_by'] = $this->session->userdata('sipt_user_id');
            $this->keuangan_diluar_m->createKeuangan($post);            
            $this->redirect->with('successMessage', 'Pembayaran keuangan berhasil disimpan.')->to('keuangan/diluar');
        }       
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "container" => $this->_form(),
            "menu_key" => $this->menu_key,
			"akses_key" => "is_insert",
            "custom_js" => array(
                ASSETS_JS_URL."keuangan/diluar/form.js"
            ),
            "custom_css" => array(),
        );          
        $this->_render("default",$dt);             
    }

    public function edit($id) { 
        $result = $this->find($id);
        $result->layanan  = $this->keuangan_diluar_m->getLayanan($id)->result_array();
        if ($post = $this->input->post()) {
            $post['created_by'] = $this->session->userdata('sipt_user_id');
            $this->keuangan_diluar_m->updateKeuangan($id, $post);            
            $this->redirect->with('successMessage', 'Pembayaran keuangan berhasil diperbarui.')->to('keuangan/diluar');
        }               
        $this->form->setData($result);
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "menu_key" => $this->menu_key,
			"akses_key" => "is_update",
            "container" => $this->_form(),
            "custom_js" => array(
                ASSETS_JS_URL."keuangan/diluar/form.js"
            ),
            "custom_css" => array(),
        );          
        $this->_render("default",$dt);             
    }

    private function _form() {        
        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['title'] = $this->meta_title;        
        return $this->load->view('keuangan_diluar/form', $dt, true);
    }

    public function delete($id) {
        $this->keuangan_diluar_m->deleteKeuangan($id);
        $this->redirect->with('successMessage', 'Pembayaran keuangan berhasil dihapus.')->back();
    }

    public function tambah_layanan() {
        $this->load->model('item_keuangan_m');
        $layanan = $this->input->post('layanan');
        $result = $this->item_keuangan_m->findByKey($layanan)->result();
        $this->output->set_content_type('application/json')->set_output(json_encode($result));        
    }

    public function cari_layanan() {
        $this->load->view('keuangan_rawat_jalan/cari_layanan');
    }

    public function get_cari_layanan() {
        $this->load->library('datatables');
        if ($key = $this->input->get('key')) {
            $this->db->like('nama_item', $key);            
        }
        $query = $this->db->get_compiled_select('item_keuangan');
        $response = $this->datatables->collection($query)
        ->orderableColumns('nama_item')      
        ->editColumn('jasa_sarana', function($row) {
            return toNumber($row->jasa_sarana);
        })
        ->editColumn('jasa_pelayanan', function($row) {
            return toNumber($row->jasa_pelayanan);
        })
        ->addColumn('tarif', function($row) {
            return toNumber(($row->jasa_sarana + $row->jasa_pelayanan));
        })
        ->render();
        $this->output->set_content_type('application/json')->set_output(json_encode($response));
    }

    private function find($id) {
        $result = $this->keuangan_diluar_m->find($id);
        if (!$result) {
            $this->redirect->with('errorMessage', 'Data keuangan_diluar tidak ditemukan.')->back();
        } else {
            return $result;
        }
    }

}