<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_pegawai extends MY_Controller {
    
    var $meta_title = "Data Pegawai";
    var $meta_desc = "Data Pegawai";
	var $main_title = "Data Pegawai";
    var $base_url = "";
	var $limit = "20";
	var $arr_level_user = "";
    var $menu_key = "master_pegawai";
    var $mode = "";
    var $main_model = "";
    var $js_folder = "";
    var $jenis_pelayanan = "";
	function __construct(){
        parent::__construct();
		$this->load->model("pegawai_model");
        $this->base_url = $this->base_url_site."data-pegawai/";
        $this->main_model = $this->pegawai_model;
        $this->js_folder = ASSETS_JS_URL."master_data/pegawai/";
        
    }
    
    /// Data Tindakan
	public function index()
	{
        $menu = "K01";
		$this->mode = "view";
		$dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
			"container" => $this->_content(),
			"menu_key" => $this->menu_key,
			"akses_key" => "is_view",
			"custom_js" => array(
				
				ASSETS_URL."plugins/moment/moment.min.js",
				ASSETS_URL."plugins/flexcode/js/ajaxmask.js",
				ASSETS_URL."plugins/flexcode/js/jquery.timer.js",
				$this->js_folder."content.js",
			),
            "custom_css" => array(
				
			),
		);	
		$this->_render("default",$dt);	
	}
    
    public function form($id=""){
		
		$menu = "K02";
		$this->mode = "insert";
		$akses_key = !empty($id) ? "is_update" : "is_insert";
		$dt = array(
            "title" => $this->meta_title,
			"menu" => "M01.1",
			"mode_menu" => "insert",
            "description" => $this->meta_desc,
			"container" => $this->_form($id),
			"menu_key" => $this->menu_key,
			"akses_key" => $akses_key,
			"custom_js" => array(
				ASSETS_URL."plugins/select2/select2.js",
				ASSETS_URL."plugins/validate/jquery.validate_1.11.1.min.js",
				$this->js_folder."form.js",
				ASSETS_URL."plugins/moment/moment.min.js",
				ASSETS_URL."plugins/flexcode/js/ajaxmask.js",
				ASSETS_URL."plugins/flexcode/js/jquery.timer.js",
			),
            "custom_css" => array(
				ASSETS_URL."plugins/select2/select2.css",
			),
		);	
		$this->_render("default",$dt);
	}
	
	private function _content(){
		$dt = array();
		$breadcrumbs = array(
				"Home" =>$this->base_url_site,
				"Master Data" => $this->base_url,
				"Data Pegawai" => "#",
        );
		
		$dt['breadcrumbs'] = $this->setBreadcrumbs($breadcrumbs);
		$dt['title'] = $this->meta_title;
        $arrJabatan = $this->config->item("jabatan_list");
        $arrJabatan["-"] = "-Semua-";
		$dt['frm_search'] = $this->form_builder->inputText("Nama Pegawai / Kode Pegawai" , "searchText" , "" , "col-sm-3" , array("class"=> "form-control"));
        $dt['frm_jenis_pegawai'] = $this->form_builder->inputDropdown("Jabatan Pegawai" , "selectJenisPelayanan" , "-" , $arrJabatan );
		$dt['link_add'] = $this->base_url."tambah-data/";
		$ret = $this->load->view("master/pegawai/index" , $dt, true);
		return $ret;
	}
	
	private function _form($id){
		$dt = array();
		$mode = "insert";
		$dataDetail = array();
		if(!empty($id)){
			$mode = "edit";
			$dataDetail = $this->main_model->getDetail($id);
		}
		
		$arrForm = array("intIdPegawai" ,
                         "txtNoIndukPegawai" ,
                         "txtNamaPegawai",
                         "txtAlamatPegawai",
                         "intJabatanPegawai",
						 "txtNoHandphone",
                         "dtTanggalTerdaftar",
                         "bitStatusPegawai");
		foreach($arrForm as $frmData){
			$$frmData = !empty($dataDetail[$frmData]) ? $dataDetail[$frmData] : "";
		}
        
		$dt['frmintIdPegawai'] = $this->form_builder->inputHidden("intIdPegawai" , $intIdPegawai);
		$dt['frmtxtNoIndukPegawai'] = $this->form_builder->inputText("NIP Pegawai" , "txtNoIndukPegawai" , $txtNoIndukPegawai , "col-sm-3");
        $dt['frmtxtNamaPegawai'] = $this->form_builder->inputText("Nama Pegawai" , "txtNamaPegawai" , $txtNamaPegawai , "col-sm-3");;
		$dt['frmtxtAlamatPegawai'] = $this->form_builder->inputTextArea("Alamat" , "txtAlamatPegawai" , $txtAlamatPegawai , "col-sm-3");
		$dt['frmintJabatanPegawai'] = $this->form_builder->inputDropdown("Jabatan Pegawai" , "txtJabatanPegawai" ,$intJabatanPegawai, $this->config->item("jabatan_list"));
		$dt['txtNoHandphone'] = form_input("txtNoHandphone" , $txtNoHandphone , 'class="form-control" id="txtNoHandphone" placeholder="Cth : 81335240213"');
		$dt['frmdtTanggalTerdaftar'] = $this->form_builder->inputText("Tanggal Terdaftar" , "dtTanggalTerdaftar" , $dtTanggalTerdaftar , "col-sm-3");;
        $dt['frmbitStatusPegawai'] = $this->form_builder->inputDropdown("Status Pegawai" , "bitStatusPegawai" ,$bitStatusPegawai, $this->config->item("status_list"));
		
		$breadcrumbs = array(
				"Home" =>$this->base_url_site,
				"Master Data" => $this->base_url,
				"Data Pegawai" => $this->base_url,
                "Form Input Pegawai" => $this->base_url,
        );
		
		$dt['breadcrumbs'] = $this->setBreadcrumbs($breadcrumbs);
		$dt['title'] = $this->meta_title;
		$dt['link_index'] = $this->base_url;
		$ret = $this->load->view("master/pegawai/form" , $dt, true);
		return $ret;
	}
	
	/// Dependency Public Function
	public function hapusData(){
		if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";
        }
		$status = false;
		$html = "";
		$id = $this->input->post("id");
		if(!empty($id)){
			$resVal = $this->main_model->delete($id);
		}
		$retVal['status'] = $resVal['status'];
		$retVal['message'] = $resVal['error_stat'];
		$this->session->set_flashdata("alert_message",$retVal);
		$this->setJsonOutput($retVal);
	}
	
	public function simpanData(){
		if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";
        }
		
		$status = false;
		$html = "";
		$id = $this->input->post("intIdPegawai");
		$arrInsert = array(
                         "txtNoIndukPegawai"=>$this->input->post("txtNoIndukPegawai") ,
                         "txtNamaPegawai"=>$this->input->post("txtNamaPegawai") ,
                         "txtAlamatPegawai"=>$this->input->post("txtAlamatPegawai"),
                         "intJabatanPegawai"=>$this->input->post("txtJabatanPegawai"),
                         "dtTanggalTerdaftar"=>$this->input->post("dtTanggalTerdaftar"),
						 "txtNoHandphone"=>$this->input->post("txtNoHandphone"),
                         "bitStatusPegawai"=>$this->input->post("bitStatusPegawai"),
						 );
		
		if(!empty($id)){
			$arrInsert['intIdPegawai'] = $id;
			$resVal = $this->main_model->update($arrInsert , $id);
		}else{
			$resVal = $this->main_model->saveData($arrInsert);
		}
		
		$retVal['status'] = $resVal['status'];
		$retVal['message'] = $resVal['error_stat'];
		$this->session->set_flashdata("alert_message",$retVal);
		$this->setJsonOutput($retVal);
	}
	
	public function getData($search="" , $level=""){
		if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";
        }
		
		$status = false;
		$html = "";
		$post = $_POST;
		$length =  $this->input->post("length");
		$start = $this->input->post("start");
		
		$search = $this->input->post("txtName");
		$intIdJabatan = $this->input->post("intIdJabatan");
		
		$offset = $start;
		$data_results = $this->main_model->getData($start , $this->limit , $search);
		$count_data = $this->main_model->getCountData($search);
		
        $jenis_pelayanan = $this->jenis_pelayanan;
		$jabatan_pegawai[0] = 'Tidak Ada';
		$jabatan_pegawai = $this->config->item("jabatan_list");
		
		$status_pegawai = $this->config->item("status_list");
		if($count_data > 0) {
			$retVal['draw'] = $this->input->post('draw');
			$retVal['recordsTotal'] = $count_data;
			$retVal['recordsFiltered'] = $count_data;
			$retVal['data'] = array();
			foreach($data_results as $row){
				
				$intIdPegawai = $row['intIdPegawai'];
				
				
				$btnEdit = "<a href='".$this->base_url."edit/".$row['intIdPegawai']."' class='btn btn-sm btn-info btn-flat'><i class=\"fa fa-pencil\"></i> Edit</a>";
				$btnDelete = "<button onclick='hapusData(".$row['intIdPegawai'].")' type='button' class='btn btn-sm btn-danger btn-flat'><i class=\"fa fa-trash\"></i> Hapus</button>";
				
				$btnAksi = $btnEdit.$btnDelete;
				$retVal['data'][] = array($row['txtNoIndukPegawai'] . "-" . $intIdPegawai,
										  $row['txtNamaPegawai'] ,
										  $row['txtAlamatPegawai'] ,
										  $jabatan_pegawai[$row['intJabatanPegawai']] ,
										  $status_pegawai[$row['bitStatusPegawai']],
										  $btnAksi
									);
			}
		}else{
			$retVal['draw'] = $this->input->post('draw');
			$retVal['recordsTotal'] = 0;
			$retVal['recordsFiltered'] = 0;
			$retVal['data'] = array();
		}
		$this->setJsonOutput($retVal);
	}
	
	public function getListData(){
        
        if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";
        }
        $where = $this->input->get("q");
        $page = !empty($_GET['page']) ? $this->input->get("page") : 1;
        $start = ($page - 1) * $this->limit;
        $data_penyakit = $this->main_model->getListData($where , $start , $this->limit);
        $jumlah = $this->main_model->getCountList($where);
        $retVal = array();
		
        $retVal['total_count'] = $jumlah;
        $retVal['incomplete_results'] = false;
        $retVal['items'] = $data_penyakit;
        echo json_encode($retVal);
    }
    
}
