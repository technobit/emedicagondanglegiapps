<?php 
class Loket extends MY_Controller {

    var $meta_title = "Loket";
    var $meta_desc = "Loket";
	var $main_title = "Loket";
    var $base_url = "";
	var $base_url_pasien = "";
	var $limit = "20";

    public function __construct(){
        parent::__construct();
		$this->base_url = $this->base_url_site."loket/";
		$this->base_url_pasien = $this->base_url_site."pasien/";
		$this->load->model(
			array(
				"pasien_model",
				"register_loket_model",
				"pelayanan_model",
				"register_model",
				"jaminan_kesehatan_model" => "jaminan_kesehatan",
				"register_bpjs_model",
				"rekam_medis_bpjs_model",
			)
		);
    }

    public function register_loket($jenis_loket = '78'){
		$detail_pelayanan = $this->pelayanan_model->getDetail($jenis_loket);
		$dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
			"container" => $this->_build_register_loket($jenis_loket),
			"menu_key" => $detail_pelayanan['txtSingkat'],
			"akses_key" => "is_view",
			"custom_js" => array(
				ASSETS_JS_URL."loket/register_loket.js"
			),
            "custom_css" => array(

			),
		);
		$this->_render("default",$dt);
	}


	public function detail($id_antrian){
		$detail_loket = $this->register_loket_model->detail($id_antrian);
		$jenis_loket=$detail_loket['intIdPelayanan'];
		$detail_pelayanan = $this->pelayanan_model->getDetail($jenis_loket);
		
		
		
		$dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
			"container" => $this->_build_detail_loket($id_antrian),
			"menu_key" => $detail_pelayanan['txtSingkat'],
			"akses_key" => "is_view",
			"custom_js" => array(
				ASSETS_JS_URL."loket/detail.js"
			),
            "custom_css" => array(

			),
		);
		$this->_render("default",$dt);
	}

	private function _build_detail_loket($id_antrian){
		$dt = array();
		
		$detail_loket = $this->register_loket_model->detail($id_antrian);
		$status_antrian = $detail_loket['bitIsPoli'];
		//// Update Sedang Dilayani
		if($status_antrian==1){
			$arrData = array(
				'bitIsPoli' => 2,
				'dtLastUpdateKunjungan' =>date('Y-m-d H:i:s')
			);

			$resUpdate = $this->register_loket_model->update($arrData , $id_antrian);
		}
		
		$jenis_loket = $detail_loket['intIdPelayanan'];
		$detail_pelayanan = $this->pelayanan_model->getDetail($jenis_loket);
		$nama_pelayanan = $detail_pelayanan['txtNama'];
		$breadcrumbs = array(
						"Home" =>$this->base_url_site,
						"Loket" => $this->base_url,
						"Register Loket ".$nama_pelayanan => "#",
						);
		
		$dt['breadcrumbs'] = $this->setBreadcrumbs($breadcrumbs);
		$dt['title'] = "Data Antrian Loket " . $nama_pelayanan . ' - No Antrian ' .$detail_loket['intNoAntri'];
		$dt['id_jenis_loket'] = $jenis_loket;
		$dt['detail_antrian'] = $detail_loket;
		$dt['base_url'] = $this->base_url .'antrian/'. $detail_loket['intIdPelayanan'];
		$ret = $this->load->view("loket/detail" , $dt , true);
		return $ret;
   }

	private function _build_register_loket($jenis_loket){
		$dt = array();
		$detail_pelayanan = $this->pelayanan_model->getDetail($jenis_loket);
		$nama_pelayanan = $detail_pelayanan['txtNama'];
		$breadcrumbs = array(
						"Home" =>$this->base_url_site,
						"Loket" => $this->base_url,
						"Register Loket ".$nama_pelayanan => "#",
						);
		
		$dt['list_status'] = array(
			"0" => "-Semua-",
			"1" => "Antri",
			"2" => "Sedang Di Layani",
			"3" => "Sudah Di Layani",
		);
		$dt['breadcrumbs'] = $this->setBreadcrumbs($breadcrumbs);
		$dt['title'] = "Data Antrian Loket " . $nama_pelayanan;
		$dt['id_jenis_loket'] = $jenis_loket;
		
		$ret = $this->load->view("loket/register_loket" , $dt , true);
		return $ret;
   }

   public function getDataRegisterLoket(){
    $this->isAjaxRequest();
    $retVal['data'] = array();
    $txtDate = $this->input->post('txtDate');
	$intIdStatus = $this->input->post('intIdStatus');
	$intIdJenisLoket = $this->input->post('intIdJenisLoket');
    $status_layanan_list = $this->config->item("status_layanan_list");
    $dataLoket = $this->register_loket_model->dataLoket($txtDate , $intIdStatus , $intIdJenisLoket);
    foreach($dataLoket as $rowLoket){
		$btnAksi = "";
		
		if($rowLoket['bitIsPoli']=='3'){
            $btnAksi = anchor($this->base_url . 'detail/'.$rowLoket['intIdKunjunganLoket'], '<i class="fa fa-send"></i> Detail', 'class="btn btn-xs btn-primary btn-flat" target="_blank"');
        }

        if($rowLoket['bitIsPoli']=='2'){
            $btnAksi = anchor($this->base_url . 'detail/'.$rowLoket['intIdKunjunganLoket'], '<i class="fa fa-send"></i> Detail', 'class="btn btn-xs btn-primary btn-flat" target="_blank"');
            $btnAksi .= form_button('btnPanggil' , '<i class="fa fa-bullhorn"></i> Panggil' , 'class="btn btn-xs btn-default btn-flat" onclick="panggil_antrian('.$rowLoket['intIdKunjunganLoket'].')"');
        }

        if($rowLoket['bitIsPoli']=='1'){
            $btnAksi = anchor($this->base_url . 'detail/'.$rowLoket['intIdKunjunganLoket'], '<i class="fa fa-send"></i> Detail', 'class="btn btn-xs btn-primary btn-flat" target="_blank"');
            $btnAksi .= form_button('btnHapus' , '<i class="fa fa-trash"></i> Hapus' , 'class="btn btn-xs btn-danger btn-flat" onclick="hapus_antrian('.$rowLoket['intIdKunjunganLoket'].')"');
            $btnAksi .= form_button('btnPanggil' , '<i class="fa fa-bullhorn"></i> Panggil' , 'class="btn btn-xs btn-default btn-flat" onclick="panggil_antrian('.$rowLoket['intIdKunjunganLoket'].')"');
        }
        
        $arrData = array(
            $rowLoket['intNoAntri'],
            $rowLoket['txtNama'],
            $status_layanan_list[$rowLoket['bitIsPoli']],
            $btnAksi
        );
        $retVal['data'][] = $arrData;
    }
    echo json_encode($retVal);
}

public function registerLoket(){

	$this->isAjaxRequest();

	$status = false;
	$message = "Data Kunjungan Gagal";
	$date_now = date("Y-m-d H:i:s");
	$id_loket = $this->input->post('intIDPoli');
	if($id_loket==78){ //Loket Umum
		$startAntrian = 100;
	}else if ($id_loket==79) { //Loket BPJS
		$startAntrian = 200;
	}else if ($id_loket==80) { //Rawat Inap
		$startAntrian = 300;
	}

	$jumlah_kunjungan = $this->register_loket_model->checkKunjungan($id_loket);
	if($jumlah_kunjungan > 99) {
		$startAntrian = $startAntrian * 10;
	}
	$intNoAntrian = $startAntrian + ($jumlah_kunjungan + 1);		
	$arrayInput = array(
		"intIdPelayanan" => $id_loket,
		"dtTanggalKunjungan" => $date_now,
		"dtCreatedKunjungan" => $date_now,
		"dtLastUpdateKunjungan" => $date_now,
		"bitIsPoli" => 1,
		"bitStatusKunjungan" => 1,
		"intNoAntri" => $intNoAntrian,
	);

	$retVal = $this->register_loket_model->insert($arrayInput);
	echo json_encode($retVal);
}

public function hapusDataAntrianLoket(){
	/// I just wanna delete AAAAAAAAnnnnnnddd Its Gone
	$this->isAjaxRequest();

	$idKunjungan = $this->input->post('idKunjungan');
	////$this->hapusPendaftaranBPJS($idKunjungan);
	$retVal = $this->register_loket_model->delete($idKunjungan);
	echo json_encode($retVal);
}

public function updateDataLoket(){
	$this->isAjaxRequest();
	
	$data_post = $this->input->post();
	$id_antrian = $data_post['id_kunjungan_loket'];
	$detail_loket = $this->register_loket_model->detail($id_antrian);
	$status_antrian = $detail_loket['bitIsPoli'];
	$arr_update['txtKeterangan'] = $data_post['txtKeterangan'];
	if($status_antrian=='2'){
		$arr_update['bitIsPoli'] = '3';
		$arr_update['dtSelesaiKunjungan'] = date('Y-m-d H:i:s');
	}

	$resUpdate = $this->register_loket_model->update($arr_update , $id_antrian);
	///$resUpdate['intPoli'] = $data_post['id_jenis_loket'];
	$resUpdate['message'] = $resUpdate['status']==1 ? 'Data Berhasil Di Simpan' : 'Data Gagal Di SImpan';
	$resUpdate['base_url'] = $this->base_url .'antrian/'. $detail_loket['intIdPelayanan'];
	echo json_encode($resUpdate);
	
	
}





}
