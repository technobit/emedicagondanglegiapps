<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dasar_target extends MY_Controller {
    
 	var $meta_title = "Imunisasi - Data Dasar Target";
	var $meta_desc = "Imunisasi - Data Dasar Target";
	var $menu_key = "dasar_target";
	var $arrBreadcrumbs = array();
	var $base_url = "";
	function __construct(){
		parent::__construct();
		$this->base_url = $this->base_url_site . "kegiatan_luar/dasar_target/";
		$this->load->model(
		array(
		"dasar_target_model",
		"register_bpjs_model",
		"rekam_medis_bpjs_model",
		"register_model",
		)
		);
	}
	function index(){
		$dt = array(
		"title" => $this->meta_title,
		"description" => $this->meta_desc,
		"container" => $this->_content(),
		"menu_key" => $this->menu_key,
		"akses_key" => "is_view",	
		"custom_js" => array(
		ASSETS_JS_URL."imunisasi/dasar-target.js",
		),
		);
		$this->_render("default",$dt);
	}
	
	private function _content(){
		$this->load->model(array(
			"kelurahan_model" , 
			
		));
		$breadcrumbs = array(
		"Home" => $this->base_url_site,
		"Data kegiatan Luar Gedung" => "#",
		"Data Dasar Target" => $this->base_url,
		);
		$dt = array();
	
		$thun = array();
		$tahun = range("2015",date("Y"));
		foreach ($tahun as $key) {
			# code...
			$thun[$key] = $key;
		}
		$dataKelurahan = $this->kelurahan_model->getDataKelurahan(ID_KECAMATAN);
		$listKelurahan = arrayDropdown($dataKelurahan , "IDKelurahan"  , "Nama");
		$dt['breadcrumbs'] = $this->setBreadcrumbs($breadcrumbs);
		$dt['title'] = $this->meta_title;
		$dt['tahun'] = $this->form_builder->inputDropdown("Tahun","tahun" , date("Y") , $thun,array("id"=>"tahun"));
		$dt['kelurahan'] = $this->form_builder->inputDropdown("Kelurahan" , "cmbKelurahan" ,"", $listKelurahan);
		$dt['link_tambah'] = $this->base_url.'form';
		
		$ret = $this->load->view("imunisasi/data-target/content" , $dt, true);
		return $ret;
	}
    
    public function form($id=""){
		
		$this->mode = "insert";
		$akses_key = !empty($id) ? "is_update" : "is_insert";
		$dt = array(
            "title" => $this->meta_title,
			"menu_key" => $this->menu_key,
			"akses_key" => "is_view",
            "description" => $this->meta_desc,
			"container" => $this->_form($id),
			"custom_js" => array(
				ASSETS_URL."plugins/select2/select2.js",
				ASSETS_URL."plugins/validate/jquery.validate_1.11.1.min.js",
				ASSETS_JS_URL."imunisasi/dasar-target.js",
			),
            "custom_css" => array(
				ASSETS_URL."plugins/select2/select2.css",
			),
		);	
		$this->_render("default",$dt);
	}
	
	private function _form($id){

		$this->load->model(array(
			"kelurahan_model" , 
			"dasar_target_model"
		));
		$dt = array();
		$mode = "insert";
		$thun = array();
		$tahun = range("2015",date("Y"));
		foreach ($tahun as $key) {
			# code...
			$thun[$key] = $key;
		}
		$TargetData = array();
		$data_petugas = array();
		if(!empty($id)){
			$mode = "edit";
			$TargetData = $this->dasar_target_model->getDetail($id);
			
		}
		
		$arrDefault = array("readonly"=>"readonly");
		
		$arrForm = array(
		"id_target_imunisasi" , 
		"id_kelurahan",
		"tahun",
		"jumlah_bayi", 
		"jumlah_balita",
		"jumlah_anak",
		"jumlah_caten", 
 		"jumlah_wus_hamil", 
		"jumlah_wus_tidak_hamil", 
		"jumlah_sd", 
		"jumlah_posyandu", 
		"jumlah_ups_bds", 
		"jumlah_pembina_wil", 
		"jumlah_waktu_tempuh", 
);
		foreach($arrForm as $frmData){
			$$frmData = !empty($TargetData[$frmData]) ? $TargetData[$frmData] : "";
		}

		$dataKelurahan = $this->kelurahan_model->getDataKelurahan(ID_KECAMATAN);
		$listKelurahan = arrayDropdown($dataKelurahan , "IDKelurahan"  , "Nama");
			
		$dt['id_target_imunisasi'] = $this->form_builder->inputHidden("id_target_imunisasi", $id_target_imunisasi);
		$dt['jumlah_bayi'] = $this->form_builder->inputText("Jumlah Bayi" , "jumlah_bayi" , $jumlah_bayi, "col-sm-3");
		$dt['jumlah_balita'] = $this->form_builder->inputText("Jumlah Balita" , "jumlah_balita" , $jumlah_balita, "col-sm-3");
		$dt['jumlah_anak'] = $this->form_builder->inputText("Jumlah Anak" , "jumlah_anak" , $jumlah_anak, "col-sm-3");
		$dt['jumlah_caten'] = $this->form_builder->inputText("Jumlah Ceten" , "jumlah_caten" , $jumlah_caten, "col-sm-3");
		$dt['jumlah_wus_hamil'] = $this->form_builder->inputText("Jumlah WUS Hamil" , "jumlah_wus_hamil" , $jumlah_wus_hamil, "col-sm-3");
		$dt['jumlah_wus_tidak_hamil'] = $this->form_builder->inputText("Jumlah WUS Tidak Hamil" , "jumlah_wus_tidak_hamil" , $jumlah_wus_tidak_hamil, "col-sm-3");
		$dt['jumlah_sd'] = $this->form_builder->inputText("Jumlah SD" , "jumlah_sd" , $jumlah_sd, "col-sm-3");
		$dt['jumlah_posyandu'] = $this->form_builder->inputText("Jumlah Posyandu" , "jumlah_posyandu" , $jumlah_posyandu, "col-sm-3");
		$dt['jumlah_ups_bds'] = $this->form_builder->inputText("Jumlah UPS BOS" , "jumlah_ups_bds" , $jumlah_ups_bds, "col-sm-3");
		$dt['jumlah_pembina_wil'] = $this->form_builder->inputText("Jumlah Pembina WIL" , "jumlah_pembina_wil" , $jumlah_pembina_wil, "col-sm-3");
		$dt['jumlah_waktu_tempuh'] = $this->form_builder->inputText("Jumlah Waktu" , "jumlah_waktu_tempuh" , $jumlah_waktu_tempuh, "col-sm-3");
		$dt['frmListKelurahan'] = $this->form_builder->inputDropdown("Kelurahan" , "cmbKelurahan" ,$id_kelurahan, $listKelurahan);
		$dt['tahun'] = $this->form_builder->inputDropdown("Tahun","tahun" ,!empty($tahun)? $tahun : date("Y")  , $thun,array("id"=>"tahun"));
		
		$breadcrumbs = array(
				"Home" =>$this->base_url_site,
				"Imunisasi" => $this->base_url,
				"Form Data Daftar Targer" => "#",
        );

		$dt['breadcrumbs'] = $this->setBreadcrumbs($breadcrumbs);
		$dt['title'] = $this->meta_title;
		$dt['mode'] = $mode;
		$dt['title_form'] = $this->meta_title; 
		$dt['link_index'] = $this->base_url;
		$dt['link_save'] = $this->base_url.'simpanData';
		
		$ret = $this->load->view("imunisasi/data-target/form" , $dt, true);
		return $ret;
	}
	
	/// Dependency Public Function
	function getDasarTarget(){
		$this->isAjaxRequest();
		$post = $this->input->post();
        $kelurahan = $post['kelurahan'];
        $tahun = $post['tahun'];
        
       //echopre($tanggal);
        $data = $this->dasar_target_model->getData($kelurahan, $tahun);
		$retVal['data'] = array();
        if(!empty($data)){
            foreach($data as $rowTarget){
				$retVal['data'] = array();
					$url = $this->base_url.'form/'.$rowTarget['id_target_imunisasi'];
					
					$btnDel = '<button class="btn btn-danger btn-flat" type="button" onclick="hapusData('.$rowTarget['id_target_imunisasi'].')"><i class="fa fa-trash"></i> Hapus</button>';
                  	$btnEdit = '<a href="'.$url.'" class="btn btn-info btn-flat"><i class="fa fa-edit"></i> Edit</a>'; 
                   	$btnAksi = $btnEdit.$btnDel;
                  	$retVal['data'][] = array(
                        $rowTarget['nama_kelurahan'],
                        $rowTarget['tahun'],
                        $rowTarget['jumlah_bayi'],
                        $rowTarget['jumlah_balita'],
						$rowTarget['jumlah_anak'],
                       	$btnAksi
                   );
            }
       
        }
       $this->setJsonOutput($retVal);
	}
	public function delete($id){
	
		$retVal = array();
		if(!empty($id)){
			$resVal = $this->dasar_target_model->delete($id);
		
		}
		$retVal['status'] = $resVal['status'];
		$retVal['message'] = $resVal['error_stat'];
		$this->session->set_flashdata("alert_message",$retVal);
		redirect($this->base_url);
	}


public function simpanData(){
		
		$html = "";
		$id_target_imunisasi = $this->input->post("id_target_imunisasi");
		$arrInsert = array(
		//"id_target_imunisasi"=>$this->input->post("id_target_imunisasi "),
		"id_kelurahan"=>$this->input->post("cmbKelurahan"),
		"tahun"=>$this->input->post("tahun"),
		"jumlah_bayi" =>$this->input->post("jumlah_bayi"), 
		"jumlah_balita"=>$this->input->post("jumlah_balita"),
		"jumlah_anak"=>$this->input->post("jumlah_anak"),
		"jumlah_caten" =>$this->input->post("jumlah_caten"), 
 		"jumlah_wus_hamil" =>$this->input->post("jumlah_wus_hamil"), 
		"jumlah_wus_tidak_hamil" =>$this->input->post("jumlah_wus_tidak_hamil"),
		"jumlah_sd" =>$this->input->post("jumlah_sd"), 
		"jumlah_posyandu" =>$this->input->post("jumlah_posyandu"),
		"jumlah_ups_bds" =>$this->input->post("jumlah_ups_bds"),
		"jumlah_pembina_wil" =>$this->input->post("jumlah_pembina_wil"), 
		"jumlah_waktu_tempuh" =>$this->input->post("jumlah_waktu_tempuh"),
						 );
	
		if(!empty($id_target_imunisasi)){
			$resVal = $this->dasar_target_model->update($arrInsert , $id_target_imunisasi);
		}else{
			$resVal = $this->dasar_target_model->saveData($arrInsert);
		}
		
		$retVal['status'] = $resVal['status'];
		$retVal['message'] = $resVal['error_stat'];
		$this->session->set_flashdata("alert_message",$retVal);
		redirect($this->base_url);
	}
	
}
