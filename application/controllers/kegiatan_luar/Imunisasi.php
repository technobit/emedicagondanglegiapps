<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Imunisasi extends MY_Controller {
    
 	var $meta_title = "Imunisasi - Daftar Imunisasi";
	var $meta_desc = "Imunisasi - Daftar Imunisasi";
	var $menu_key = "imunisasi";
	var $arrBreadcrumbs = array();
	var $base_url = "";
	function __construct(){
		parent::__construct();
		$this->base_url = $this->base_url_site . "kegiatan_luar/imunisasi/";
		$this->load->model(
		array(
		"imunisasi_model",
		"pasien_model",
		"pegawai_model",
		"kelurahan_model",
		)
		);
	}
	
	function index(){
		$dt = array(
		"title" => $this->meta_title,
		"description" => $this->meta_desc,
		"container" => $this->_content(),
		"menu_key" => $this->menu_key,
		"akses_key" => "is_view",	
		"custom_js" => array(
		ASSETS_JS_URL."imunisasi/imunisasi.js",
		 ASSETS_URL . "plugins/select2/select2.full.min.js",
            ),
         "custom_css" => array(
                 ASSETS_URL . "plugins/select2/select2.min.css",
		),
		);
		$this->_render("default",$dt);
	}
	
	private function _content(){
		$this->load->model(array(
			"kelurahan_model" , 
			
		));
		$breadcrumbs = array(
		"Home" => $this->base_url_site,
		"Imunisasi" => "#",
		"Data Daftar Imunisasi" => $this->base_url,
		);
		$dt = array();
		$jenis = $this->config->item("jenis_lokasi");
		$dataKelurahan = $this->kelurahan_model->getDataKelurahan(ID_KECAMATAN);
		$empty  = array('' => 'Semua', );
		$listKelurahan = arrayDropdown($dataKelurahan , "IDKelurahan"  , "Nama", $empty);
		$dt['breadcrumbs'] = $this->setBreadcrumbs($breadcrumbs);
		$dt['title'] = $this->meta_title;
		$dt['jenis_kegiatan'] = $this->form_builder->inputDropdown("Jenis Kegiatan imunisasi","jenis_kegiatan" , "" , $jenis,array("id"=>"jenis_kegiatan"));
		$dt['kelurahan'] = $this->form_builder->inputDropdown("Kelurahan" , "cmbKelurahan" ,"", $listKelurahan);
		$dt['link_tambah'] = $this->base_url.'form';
		
		$ret = $this->load->view("imunisasi/imunisasi/content" , $dt, true);
		return $ret;
	}
	function formPasien($id){
		$dt = array(
		"title" => $this->meta_title,
		"description" => $this->meta_desc,
		"container" => $this->_pasien($id),
		"menu_key" => $this->menu_key,
		"akses_key" => "is_view",	
		"custom_js" => array(
		ASSETS_JS_URL."imunisasi/imunisasi.js",
			ASSETS_URL."plugins/select2/select2.js",
				ASSETS_URL."plugins/validate/jquery.validate_1.11.1.min.js",
				ASSETS_URL."plugins/moment/moment.min.js",
				ASSETS_URL."plugins/flexcode/js/ajaxmask.js",
				ASSETS_URL."plugins/flexcode/js/jquery.timer.js",
				ASSETS_JS_URL."bpjs/checkNoAnggotaBPJS.js",
				ASSETS_JS_URL."pasien/finger.js",
				ASSETS_JS_URL."pasien/handsign.js",
				ASSETS_PLUGIN_URL . "jSignature/libs/jSignature.min.js",
				ASSETS_PLUGIN_URL . "jSignature/plugins/jSignature.CompressorBase30.js",
				ASSETS_PLUGIN_URL . "jSignature/plugins/jSignature.CompressorSVG.js",
            ),
        "custom_css" => array(
                 ASSETS_URL . "plugins/select2/select2.min.css",
		),
		);
		$this->_render("default",$dt);
	}
	
	private function _pasien($id){
		$this->load->model(array(
			"kelurahan_model" , 
			
		));
		$breadcrumbs = array(
		"Home" => $this->base_url_site,
		"Imunisasi" => "#",
		"Tambah Peserta Imunisasi" => $this->base_url,
		);

		$TargetData = array();
			$mode = "edit";
			$TargetData = $this->imunisasi_model->getDetail($id);		
		$arrDefault = array("readonly"=>"readonly");
		
		$arrForm = array(
		"id_kegiatan_imunisasi" , 
		"id_kelurahan",
		"tanggal_kegiatan",
		"jenis_lokasi", 
		"nama_lokasi",
		"alamat"
		);
		foreach($arrForm as $frmData){
			$$frmData = !empty($TargetData[$frmData]) ? $TargetData[$frmData] : "";
		}

		$dataKelurahan = $this->kelurahan_model->getDataKelurahan(ID_KECAMATAN);
		$listKelurahan = arrayDropdown($dataKelurahan , "IDKelurahan"  , "Nama");
		$listLokasi = $this->config->item("jenis_lokasi");
		$dt['id_kegiatan_imunisasi'] = $this->form_builder->inputHidden("id_kegiatan_imunisasi", $id);
		$dt['jenisLokasi'] = $this->form_builder->inputDropdown("Jenis Lokasi" , "jenis_lokasi" ,$jenis_lokasi, $listLokasi,array_merge(array("disabled"=>"disabled")));
		$dt['namaLokasi'] = $this->form_builder->inputText("Nama Lokasi" , "nama_lokasi" , $nama_lokasi, "col-sm-3",array_merge($arrDefault));
		$dt['alamat'] = $this->form_builder->inputTextArea("Alamat" , "alamat" , $alamat, "col-sm-3",array_merge($arrDefault));
		$dt['namaLokasi'] = $this->form_builder->inputText("Nama Lokasi" , "nama_lokasi" , $nama_lokasi, "col-sm-3",array_merge($arrDefault));
		$dt['tanggal_kegiatan'] = $this->form_builder->inputText("Tanggal Kegiatan" , "tanggal_kegiatan" , empty($tanggal_kegiatan) ? date("Y-m-d") : indonesian_date($tanggal_kegiatan), "col-sm-3",array_merge($arrDefault));
		$dt['id_kegiatan'] = $id;
		$dt['redirect_edit'] = $this->base_url.'form/'.$id;
		//kecamatan
		$dataIdKecamatan = $this->kelurahan_model->getDetailKelurahan($id_kelurahan);
		$idKecamatan = $dataIdKecamatan['IDKecamatan'];
		$list_kecamatan = $this->session->userdata("sipt_kecamatan");
		$default_kecamatan = $mode!="detail" ? $this->session->userdata("sipt_id_kecamatan") : $intIdKecamatan;
		$arrKecamatan[""] = "-Pilih Kecamatan-";
		foreach($list_kecamatan as $kecamatan){
			$arrKecamatan[$kecamatan['IDKecamatan']] = $kecamatan['Nama'];
		}
		$dt['kecamatan'] = $this->form_builder->inputDropdown("Kecamatan" , "selectKecamatanView" ,$idKecamatan, $arrKecamatan , array_merge(array("disabled"=>"disabled")));
			$arrKelurahan[""] = "-Pilih Kelurahan-";
		if(!empty($id_kelurahan)){
			$dataKelurahan = $this->kelurahan_model->getDetailKelurahan($id_kelurahan);
			$arrKelurahan[$dataKelurahan['IDKelurahan']] = $dataKelurahan['Nama'];
		}
		$dt['kelurahan'] = $this->form_builder->inputDropdown("Kelurahan" , "selectKelurahanView" ,$id_kelurahan, $arrKelurahan,array_merge(array("disabled"=>"disabled")));
	
			$dt['breadcrumbs'] = $this->setBreadcrumbs($breadcrumbs);
		$dt['title'] = "Imunisasi - Input Data Peserta";
		$ret = $this->load->view("imunisasi/imunisasi/form_daftar" , $dt, true);
		return $ret;
	}
    function getDaftarImunisasi(){
		$this->isAjaxRequest();
		$post = $this->input->post();
        $start_date = $post['start_date'];
        $end_date = $post['end_date'];        
		$kelurahan = $post['kelurahan'];
        $jenis_kegiatan = $post['jenis_kegiatan'];
        $jenis = $this->config->item("jenis_lokasi");
       //echopre($tanggal);
	    $data = $this->imunisasi_model->getData($kelurahan, $jenis_kegiatan, $start_date, $end_date);
		$retVal['data'] = array();
        if(!empty($data)){
            $retVal['data'] = array();
			foreach($data as $rowTarget){
				
					$url = $this->base_url.'form/'.$rowTarget['id_kegiatan_imunisasi'];
					$urlP = $this->base_url.'formPasien/'.$rowTarget['id_kegiatan_imunisasi'];
					$btnDel = '<button class="btn btn-danger btn-flat" type="button" onclick="hapusData('.$rowTarget['id_kegiatan_imunisasi'].')"><i class="fa fa-trash"></i> Hapus</button>';
                  	$btnEdit = '<a href="'.$url.'" class="btn btn-info btn-flat"><i class="fa fa-edit"></i> Edit</a>'; 
                   	$btnAdd = '<a href="'.$urlP.'" class="btn btn-success btn-flat"><i class="fa fa-users"></i> Tambah Peserta</a>'; 
                   	
					$btnAksi = $btnAdd.$btnEdit.$btnDel;
                  	$retVal['data'][] = array(
                        $rowTarget['nama_lokasi'],
						$rowTarget['Nama'],
                        indonesian_date($rowTarget['tanggal_kegiatan']),
                        $jenis[$rowTarget['jenis_lokasi']],
                        $rowTarget['jumlah_petugas'],
						$rowTarget['jumlah_pasien'],
						
                       	$btnAksi
                   );
            }
       
        }
		//echopre($data);
       $this->setJsonOutput($retVal);
	}
	function getDaftarPesertaImun(){
		$this->isAjaxRequest();
		$post = $this->input->post();
        $id_kegiatan = $post['id_kegiatan'];
		//echopre($post);
        $dataPasien = $this->imunisasi_model->getDataPeserta($id_kegiatan);
		$jenis = $this->config->item("jenis_pasien_imunisasi");
		$retVal['data'] = array();
        if(!empty($dataPasien)){
            $retVal['data'] = array();
			foreach($dataPasien as $rowTarget){
				
					$url = $this->base_url.'form/'.$rowTarget['id_imunisasi_pasien'];
					$btnDel = '<button class="btn btn-danger btn-flat" type="button"  onclick="hapusDataPasien('.$rowTarget['id_imunisasi_pasien'].')"><i class="fa fa-trash"></i> Hapus</button>';
                  	$btnEdit = '<button class="btn btn-info btn-flat" type="button" onclick="getFormDetail('.$rowTarget['id_imunisasi_pasien'].')"><i class="fa fa-edit"></i> Edit</a>'; 
                   	
					$btnAksi = $btnEdit.$btnDel;
                  	$retVal['data'][] = array(
                        $rowTarget['txtNoAnggota'],
                        $rowTarget['txtNamaPasien'],
                        $jenis[$rowTarget['id_jenis_pasien_imunisasi']],
                        $rowTarget['jumlah_imunisasi'],
						$btnAksi
                   );
            }
       
        }
		//echopre($dataPasien);
       $this->setJsonOutput($retVal);
	}
    public function form($id=""){
		
		$this->mode = "insert";
		$akses_key = !empty($id) ? "is_update" : "is_insert";
		$dt = array(
            "title" => $this->meta_title,
			"menu_key" => $this->menu_key,
			"akses_key" => "is_view",
            "description" => $this->meta_desc,
			"container" => $this->_form($id),
			"custom_js" => array(
				ASSETS_JS_URL."imunisasi/imunisasi.js",
				ASSETS_URL."plugins/select2/select2.js",
				ASSETS_URL."plugins/validate/jquery.validate_1.11.1.min.js",
				ASSETS_URL."plugins/moment/moment.min.js",
				ASSETS_URL."plugins/flexcode/js/ajaxmask.js",
				ASSETS_URL."plugins/flexcode/js/jquery.timer.js",
				ASSETS_JS_URL."bpjs/checkNoAnggotaBPJS.js",
				ASSETS_JS_URL."pasien/finger.js",
				ASSETS_PLUGIN_URL . "jSignature/libs/jSignature.min.js",
				ASSETS_PLUGIN_URL . "jSignature/plugins/jSignature.CompressorBase30.js",
				ASSETS_PLUGIN_URL . "jSignature/plugins/jSignature.CompressorSVG.js",
				//$arrJs
			),
			
            "custom_css" => array(
				ASSETS_URL."plugins/select2/select2.min.css",
				ASSETS_URL."plugins/flexcode/css/ajaxmask.css",
			),
		
		);	
		$this->_render("default",$dt);
	}
	
	private function _form($id){

		$this->load->model(array(
			"kelurahan_model" , 
			"imunisasi_model"
		));
		$dt = array();
		$mode = "insert";
		$TargetData = array();
		$data_pegawai = array();
		if(!empty($id)){
			$mode = "edit";
			$TargetData = $this->imunisasi_model->getDetail($id);
			$data_pegawai = $this->imunisasi_model->getDataPegawai($id);
		}
		
		$arrDefault = array("readonly"=>"readonly");
		
		$arrForm = array(
		"id_kegiatan_imunisasi" , 
		"id_kelurahan",
		"tanggal_kegiatan",
		"jenis_lokasi", 
		"nama_lokasi",
		"alamat"
		);
		foreach($arrForm as $frmData){
			$$frmData = !empty($TargetData[$frmData]) ? $TargetData[$frmData] : "";
		}

		$dataKelurahan = $this->kelurahan_model->getDataKelurahan(ID_KECAMATAN);
		$listKelurahan = arrayDropdown($dataKelurahan , "IDKelurahan"  , "Nama");
		$listLokasi = $this->config->item("jenis_lokasi");
		$dt['id_kegiatan_imunisasi'] = $this->form_builder->inputHidden("id_kegiatan_imunisasi", $id_kegiatan_imunisasi);
		$dt['jenisLokasi'] = $this->form_builder->inputDropdown("Jenis Lokasi" , "jenis_lokasi" ,$jenis_lokasi, $listLokasi);
		$dt['namaLokasi'] = $this->form_builder->inputText("Nama Lokasi" , "nama_lokasi" , $nama_lokasi, "col-sm-3");
		$dt['alamat'] = $this->form_builder->inputTextArea("Alamat" , "alamat" , $alamat, "col-sm-3");
		$dt['namaLokasi'] = $this->form_builder->inputText("Nama Lokasi" , "nama_lokasi" , $nama_lokasi, "col-sm-3");
		$dt['tanggal_kegiatan'] = $this->form_builder->inputText("Tanggal Kegiatan" , "tanggal_kegiatan" , empty($tanggal_kegiatan) ? date("Y-m-d") : $tanggal_kegiatan, "col-sm-3");
		$empty = array('' => 'Pilih Petugas', );
		$list_pegawai = arrayDropdown($this->pegawai_model->getListPegawai() , 'intIdPegawai' , 'txtNamaPegawai',$empty);
		$dt['data_petugas'] = $data_pegawai;
		$dt['listPegawai'] = $list_pegawai; 
		$dataIdKecamatan = $this->kelurahan_model->getDetailKelurahan($id_kelurahan);
		$idKecamatan = $dataIdKecamatan['IDKecamatan'];
		$list_kecamatan = $this->session->userdata("sipt_kecamatan");
		$default_kecamatan = $this->session->userdata("sipt_id_kecamatan");
		$arrKecamatan[""] = "-Pilih Kecamatan-";
		foreach($list_kecamatan as $kecamatan){
			$arrKecamatan[$kecamatan['IDKecamatan']] = $kecamatan['Nama'];
		}
		//echopre($arrKecamatan);
		$dt['kecamatan'] = $this->form_builder->inputDropdown("Kecamatan" , "selectKecamatan" ,$idKecamatan, $arrKecamatan , array_merge(array("onchange"=>"getKelurahan()","class"=>"form-control select2")));
			$arrKelurahan[""] = "-Pilih Kelurahan-";
		if(!empty($id_kelurahan)){
			$dataKelurahan = $this->kelurahan_model->getDetailKelurahan($id_kelurahan);
			$arrKelurahan[$dataKelurahan['IDKelurahan']] = $dataKelurahan['Nama'];
		}
		$dt['kelurahan'] = $this->form_builder->inputDropdown("Kelurahan" , "selectKelurahan" ,$id_kelurahan, $arrKelurahan,array_merge(array("class"=>"form-control select2")));
	
		
		
		$breadcrumbs = array(
				"Home" =>$this->base_url_site,
				"Imunisasi" => $this->base_url,
				"Input Imunisasi" => "#",
        );

		$dt['breadcrumbs'] = $this->setBreadcrumbs($breadcrumbs);
		$dt['title'] = "Imunisasi - Input Imunisasi";
		$dt['mode'] = $mode;
		$dt['title_form'] = "Imunisasi - Input Imunisasi"; 
		$dt['link_save'] = $this->base_url.'save';
		$dt['link_home'] = $this->base_url;
		$ret = $this->load->view("imunisasi/imunisasi/form" , $dt, true);
		return $ret;
	}
	
	
	public function form_detail_pasien($idp, $mode = ""){
			$this->load->model(array(
			"pasien_model" , 
			"imunisasi_model"
		));
		$dt = array();
		$mode = "insert";
		$TargetData = array();
		$TargetDt = array();
		$data_imun = array();
		if(!empty($mode)){
			$mode = "edit";
			$TargetDt = $this->imunisasi_model->getDetailPasien($idp);
			$data_imun = $this->imunisasi_model->getDataImun($idp);
		}
		
		$arrDefault = array("readonly"=>"readonly");
		
		$arrForm = array(
		"txtIdPasien" , 
		"txtNoAnggota",
		"txtNamaPasien",
		"dtTanggalLahir", 
		"charJkPasien",
		);
		$arrFormImu = array(
		"id_imunisasi_pasien",
		"txtIdPasien" , 
		"id_jenis_pasien_imunisasi",
		"id_kegiatan_imunisasi", 
		"nama_ibu",
		"status_nikah",
		"nama_suami" , 
		"hpht",
		"kehamilan_ke",
		"jarak_kehamilan", 
		"status_imunisasi_pasien",
		);
		foreach($arrFormImu as $frmDt){
			$$frmDt = !empty($TargetDt[$frmDt]) ? $TargetDt[$frmDt] : "";
		}
		$idP = empty($txtIdPasien) ? $idp : $txtIdPasien;
		$TargetData = $this->pasien_model->getDetail($idP);
		foreach($arrForm as $frmData){
			$$frmData = !empty($TargetData[$frmData]) ? $TargetData[$frmData] : "";
		}
		
		$jkel = $this->config->item("jk_list");
		$statusNikah = $this->config->item("status_nikah");
		$jenisPasien = $this->config->item("jenis_pasien_imunisasi");
		$dt['txtIdPasien'] = $this->form_builder->inputHidden("txtIdPasien", $txtIdPasien);
		$dt['charJkPasien'] = $this->form_builder->inputDropdown("Jenis Kelamin" , "charJkPasien" ,$charJkPasien, $jkel,array('disabled' => 'disabled', ));
		$dt['txtNoAnggota'] = $this->form_builder->inputText("No Anggota" , "txtNoAnggota" , $txtNoAnggota, "col-sm-3",$arrDefault);
		$dt['txtNamaPasien'] = $this->form_builder->inputText("Nama Pengguna Layanan" , "txtNamaPasien" , $txtNamaPasien, "col-sm-3",	$arrDefault);
		$dt['dtTanggalLahir'] = $this->form_builder->inputText("Tanggal Lahir" , "dtTanggalLahir" , $dtTanggalLahir, "col-sm-3", $arrDefault);
		$dataImun = $this->imunisasi_model->getJenisImunisasi();
		$listJenisImun = arrayDropdown($dataImun , "id"  , "jenis_imunisasi");
		$dt['listJenisImun'] =$listJenisImun;
		$dt['data_imun'] = !empty($data_imun) ? $data_imun : "";
		$dt['id_imunisasi_pasien'] = $this->form_builder->inputHidden("id_imunisasi_pasien", $id_imunisasi_pasien);
		$dt['id_jenis_pasien_imunisasi'] = $this->form_builder->inputDropdown("Jenis Pengguna Layanan" , "id_jenis_pasien_imunisasi" ,$id_jenis_pasien_imunisasi, $jenisPasien,array('onChange' =>"hide_form()" , ));
		$dt['nama_suami'] = $this->form_builder->inputText("Nama Suami" , "nama_suami" , $nama_suami, "col-sm-3");
		$dt['status_nikah'] = $this->form_builder->inputDropdown("Status Nikah" , "status_nikah" ,$status_nikah, $statusNikah);
		$dt['hpht'] = $this->form_builder->inputText("HPHT" , "hpht" , $hpht, "col-sm-3");
		$dt['kehamilan_ke'] = $this->form_builder->inputText("Kehamilah ke" , "kehamilan_ke" , $kehamilan_ke, "col-sm-3");
		$dt['nama_ibu'] = $this->form_builder->inputText("Nama Ibu" , "nama_ibu" , $nama_ibu, "col-sm-3");
		$dt['jarak_kehamilan'] = $this->form_builder->inputText("Jarak Kehamilan" , "jarak_kehamilan" , $jarak_kehamilan, "col-sm-3");
		$this->load->view("imunisasi/imunisasi/form_pasien", $dt);
	}
	public function form_tambah_pasien($idp, $mode = ""){
			$this->load->model(array(
			"pasien_model" , 
			"imunisasi_model"
		));
		$dt = array();
		$mode = "insert";
		$TargetData = array();
		$TargetDt = array();
		$data_imun = array();
		if(!empty($mode)){
			$mode = "edit";
			$TargetDt = $this->imunisasi_model->getDetailPasien($idp);
			$data_imun = $this->imunisasi_model->getDataImun($idp);
		}
		
		$arrDefault = array("readonly"=>"readonly");
		
		$arrForm = array(
		"txtIdPasien" , 
		"txtNoAnggota",
		"txtNamaPasien",
		"dtTanggalLahir", 
		"charJkPasien",
		);
		$arrFormImu = array(
		"id_imunisasi_pasien",
		"txtIdPasien" , 
		"id_jenis_pasien_imunisasi",
		"id_kegiatan_imunisasi", 
		"nama_ibu",
		"status_nikah",
		"nama_suami" , 
		"hpht",
		"kehamilan_ke",
		"jarak_kehamilan", 
		"status_imunisasi_pasien",
		);
		foreach($arrFormImu as $frmDt){
			$$frmDt = !empty($TargetDt[$frmDt]) ? $TargetDt[$frmDt] : "";
		}
		$idP = empty($txtIdPasien) ? $idp : $txtIdPasien;
		$TargetData = $this->pasien_model->getDetail($idP);
		foreach($arrForm as $frmData){
			$$frmData = !empty($TargetData[$frmData]) ? $TargetData[$frmData] : "";
		}
		
		$jkel = $this->config->item("jk_list");
		$statusNikah = $this->config->item("status_nikah");
		$jenisPasien = $this->config->item("jenis_pasien_imunisasi");
		$dt['txtIdPasien'] = $this->form_builder->inputHidden("txtIdPasien", $txtIdPasien);
		$dt['charJkPasien'] = $this->form_builder->inputDropdown("Jenis Kelamin" , "charJkPasien" ,$charJkPasien, $jkel,array('disabled' => 'disabled', ));
		$dt['txtNoAnggota'] = $this->form_builder->inputText("No Anggota" , "txtNoAnggota" , $txtNoAnggota, "col-sm-3",$arrDefault);
		$dt['txtNamaPasien'] = $this->form_builder->inputText("Nama Pengguna Layanan" , "txtNamaPasien" , $txtNamaPasien, "col-sm-3",	$arrDefault);
		$dt['dtTanggalLahir'] = $this->form_builder->inputText("Tanggal Lahir" , "dtTanggalLahir" , $dtTanggalLahir, "col-sm-3", $arrDefault);
		$dataImun = $this->imunisasi_model->getJenisImunisasi();
		$listJenisImun = arrayDropdown($dataImun , "id"  , "jenis_imunisasi");
		$dt['listJenisImun'] =$listJenisImun;
		$dt['data_imun'] = !empty($data_imun) ? $data_imun : "";
		$dt['id_imunisasi_pasien'] = $this->form_builder->inputHidden("id_imunisasi_pasien", $id_imunisasi_pasien);
		$dt['id_jenis_pasien_imunisasi'] = $this->form_builder->inputDropdown("Jenis Pengguna Layanan" , "id_jenis_pasien_imunisasi" ,$id_jenis_pasien_imunisasi, $jenisPasien,array('onChange' =>"hide_form()" , ));
		$dt['nama_suami'] = $this->form_builder->inputText("Nama Suami" , "nama_suami" , $nama_suami, "col-sm-3");
		$dt['status_nikah'] = $this->form_builder->inputDropdown("Status Nikah" , "status_nikah" ,$status_nikah, $statusNikah);
		$dt['hpht'] = $this->form_builder->inputText("HPHT" , "hpht" , $hpht, "col-sm-3");
		$dt['kehamilan_ke'] = $this->form_builder->inputText("Kehamilah ke" , "kehamilan_ke" , $kehamilan_ke, "col-sm-3");
		$dt['nama_ibu'] = $this->form_builder->inputText("Nama Ibu" , "nama_ibu" , $nama_ibu, "col-sm-3");
		$dt['jarak_kehamilan'] = $this->form_builder->inputText("Jarak Kehamilan" , "jarak_kehamilan" , $jarak_kehamilan, "col-sm-3");
		$this->load->view("imunisasi/imunisasi/form_pasien", $dt);
	}
	public function form_peserta(){
		$this->load->view("imunisasi/imunisasi/form_peserta");
	}
	
	
	public function delete($id){
	
		$retVal = array();
		$delPasien = $this->imunisasi_model->getDataPeserta($id);
		if (!empty($delPasien)) {
			foreach ($delPasien as $value) {
			$d = $this->imunisasi_model->deleteJenisImun($value['id_imunisasi_pasien']);
			}
		}
		
		if(!empty($id)){
			$resVal = $this->imunisasi_model->delete($id);
		
		}
		$retVal['status'] = $resVal['status'];
		$retVal['message'] = $resVal['error_stat'];
		$this->session->set_flashdata("alert_message",$retVal);
		redirect($this->base_url);
	}
	public function deletePasien($id,$id_kegiatan= ""){
	
		$retVal = array();
		
		
		if(!empty($id)){
			$resVal = $this->imunisasi_model->deletePasien($id, "id_imunisasi_pasien");
		
		}
		$retVal['status'] = $resVal['status'];
		$retVal['message'] = $resVal['error_stat'];
		$this->session->set_flashdata("alert_message",$retVal);
		redirect($this->base_url.'formPasien/'.$id_kegiatan);
	}


public function simpan($id, $idPasien = ""){
	
   
	$post = $this->input->post();
	$txtIdPasien = $post["txtIdPasien"];
	$id_imunisasi_pasien = $post["id_imunisasi_pasien"];
    $id_jenis_pasien_imunisasi = $post["id_jenis_pasien_imunisasi"];
    $status_nikah = $post["status_nikah"];
    $nama_suami = $post["nama_suami"];
    $nama_ibu = $post["nama_ibu"];
    $hpht = $post["hpht"];
    $kehamilan_ke = $post["kehamilan_ke"];
    $jarak_kehamilan = $post["jarak_kehamilan"];
	$listImunisasi = $post["listImunisasi"];
	$id_peserta = $id_imunisasi_pasien;
	$date = !empty($id_peserta)?"last_update":"created_date";
	//echopre($post);
		$arrInsert = array(
				"txtIdPasien" =>$txtIdPasien,
				"id_jenis_pasien_imunisasi" =>  $id_jenis_pasien_imunisasi,
				"id_kegiatan_imunisasi" =>$id,
				"nama_ibu" => $nama_ibu,
				"status_nikah" => $status_nikah,
				"nama_suami" =>$nama_suami,
				"hpht" => $hpht ,
				"kehamilan_ke" =>$kehamilan_ke,
				"jarak_kehamilan" => $jarak_kehamilan,
				$date =>date("d-m-Y H:i:s"),
				"status_imunisasi_pasien"=> 1,
						 );
	
		if(!empty($id_peserta)){
			$resVal = $this->imunisasi_model->updatePeserta($arrInsert , $id_peserta);
		}else{
			$resVal = $this->imunisasi_model->saveDataPeserta($arrInsert);
			$id_peserta = $resVal["id"];
		}
		$listJenisImun = $listImunisasi;
		$arrDataJenisImun = array();
		foreach($listJenisImun as $rowJenisImun){
			$arrDataJenisImun[] = array(
				"id_imunisasi_pasien" => $id_peserta,
				"id_jenis_imunisasi" => $rowJenisImun,
			);
		}
		$resJenisImun = $this->imunisasi_model->saveJenisImun($arrDataJenisImun , $id_peserta);
		
			
		$retVal['status'] = $resVal['status'];
		$retVal['message'] = $resVal['error_stat'];
		$this->session->set_flashdata("alert_message",$retVal);
		$this->setJsonOutput($retVal);
	}
	public function save(){
	$post = $this->input->post();

	$id = $post["id_kegiatan_imunisasi"];
	$jenis_lokasi = $post["jenis_lokasi"];
	$nama_lokasi = $post["nama_lokasi"];
	$alamat = $post["alamat"];
	$kecamatan = $post["selectKecamatan"];
	$kelurahan = $post["selectKelurahan"];
	$tanggal_kegiatan = $post["tanggal_kegiatan"];
	$id_kegiatan = $id;
  	$pegawai = $post["listPegawai"];
  	$petugas = $this->session->userdata('sipt_user_id');
	//echopre($post);
	  $arrInsert = array(
	
  "id_kelurahan" => $kelurahan,
  "tanggal_kegiatan" => $tanggal_kegiatan,
  "jenis_lokasi" => $jenis_lokasi,
  "nama_lokasi" => $nama_lokasi,
  "alamat" => $alamat,
  
		 );
	
		if(!empty($id)){
			$resVal = $this->imunisasi_model->update($arrInsert , $id);
		}else{
			$resVal = $this->imunisasi_model->saveData($arrInsert);
			$id_kegiatan = $resVal['id'];
		}
		$listPetugas = 	$pegawai;
		$arrDataPetugas = array();
		foreach($listPetugas as $rowPetugas){
			$arrDataPetugas[] = array(
				"id_kegiatan_imunisasi" => $id_kegiatan,
				"intIdPegawai" => $rowPetugas,
			);
		}
		$resPetugas = $this->imunisasi_model->savePetugas($arrDataPetugas , $id);
		
		
		$retVal['status'] = $resVal['status'];
		$retVal['message'] = $resVal['error_stat'];
		$id_kegiatan = $resVal['id'];
		$this->session->set_flashdata("alert_message",$retVal);
		redirect($this->base_url);
	}
	
	 public function get_cari_Pasien() {
        $this->load->library('datatables');
        if ($key = $this->input->get('key')) {
           $this->db->where("txtNoAnggota" , $key);
			$this->db->or_where("txtNamaPasien" , $key);;            
        }
        $query = $this->db->get_compiled_select('pasien');
        $response = $this->datatables->collection($query)
        ->editColumn('charJkPasien', function($row) {
            return $row->charJkPasien== "L" ? "Laki - Laki" : "Perempuan";
        })
        ->render();
        $this->output->set_content_type('application/json')->set_output(json_encode($response));
    }
	
	
}















