<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Ugd extends MY_Controller {   
    var $meta_title = "UGD";
    var $meta_desc = "UGD";
	var $main_title = "Pelayanan UGD";
    var $base_url = "";
	var $limit = "10";
    var $menu_key = "ugd_pelayanan";
	var $jenis_poli = "";
	
    public function __construct(){
        parent::__construct();
        $this->base_url = $this->base_url_site."ugd/";
		$this->load->model("pelayanan_model");
        $this->load->model("register_model");
        $this->load->model("pasien_model");
		$this->load->model("tindakan_model");
		$this->load->model("rekam_medis_model" , "rekam_medis");
		$this->load->model("jaminan_kesehatan_model" , "jaminan_kesehatan");
    } 
    
    public function index($id_pelayanan){
		$menu = "P05";
		$asset_js = ASSETS_JS_URL."ugd/content.js";
		$dt = array(
			"container" => $this->_build_ugd( $id_pelayanan),
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
			"menu_key" => $this->menu_key,
			"akses_key" => "is_view",
			"custom_js" => array(
				$asset_js,
			),
            "custom_css" => array(
				
			),
		);	
		$this->_render("default",$dt);	
    }
    
    private function _build_ugd($id_pelayanan){
        $dt = array();
        $detail_pelayanan = $this->pelayanan_model->getDetail($id_pelayanan);
        $nama_poli = $detail_pelayanan['txtNama'];
        $this->meta_title = $nama_poli;
        $this->meta_desc = "Fasilitas ".$nama_poli;
		$breadcrumbs = array(
                        "Home" =>$this->base_url_site,
                        "UGD" => $this->base_url,
                        "Data Pelayanan ".$nama_poli => "#",
                        );
        
		$dt['breadcrumbs'] = $this->setBreadcrumbs($breadcrumbs);
		$dt['title'] = $nama_poli;
        $dt['frm_tanggal'] = $this->form_builder->inputText("Tanggal" , "txtTanggal" , date("Y-m-d") , "col-sm-2" , array("class"=> "form-control datepicker"));
        $dt['frm_id_poliumum'] = $this->form_builder->inputHidden("inputIdPoli" , $id_pelayanan);
        $dt['link_add_pasien'] = $this->base_url_site."pasien/pendaftaran/ugd/";
		$ret = $this->load->view("ugd/content" , $dt , true);
        return $ret;  
    }
	
    public function detail($id_kunjungan){
        
		$menu = "PD01";
		$dt = array(
			"container" => $this->_build_detail_ugd( $id_kunjungan),
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
			"menu_key" => $this->menu_key,
			"akses_key" => "is_insert",
			"custom_js" => array(
				ASSETS_URL."plugins/select2/select2.js",
				ASSETS_URL."plugins/validate/jquery.validate_1.11.1.min.js",
				ASSETS_JS_URL."ugd/form.js"
			),
            "custom_css" => array(
				ASSETS_URL."plugins/select2/select2.css",
			),
		);	
		$this->_render("default",$dt);	
    }
	
    private function _build_detail_ugd($id_kunjungan){
        $detail_register = $this->register_model->getDetailRegister($id_kunjungan);
		
        if(count($detail_register) < 1){
            show_404();
        }
		$idPasien = $detail_register['txtIdPasien'];
		$idPelayanan = $detail_register['intIdPelayanan'];
        $detail_pelayanan = $this->pelayanan_model->getDetail($idPelayanan);
		$detail_pasien = $this->pasien_model->getDetail($idPasien);
		
		$jamkes_list = $this->jaminan_kesehatan->getListDataJaminan();
		$jamkes_list["0"] = "Umum / Tidak Ada";
		$jamkes_list[""] = "Umum / Tidak Ada";
		
		$rekam_medis = $this->rekam_medis->getDetailByIdPasien($idPasien);
		$idRekamMedis = "";
		$noRekamMedis = $detail_pasien['txtNoAnggota'];
		
		if(count($rekam_medis) > 0){
			$idRekamMedis = $rekam_medis['txtIdRekamMedis'];
			$noRekamMedis = $rekam_medis['txtNoRekamMedis'];
		}
		
		$nama_poli = $detail_pelayanan['txtNama'];
        $this->meta_title = $nama_poli;
        $this->meta_desc = "Fasilitas ".$nama_poli;
		$dt['title'] = "Pemeriksaan UGD";
		$dt['form_title'] = "No Pengguna Layanan ".$detail_pasien['txtNoAnggota'];
		/// Build Header Form -> Hidden
		$dt['frm_id_pasien'] = $this->form_builder->inputHidden("txtIdPasien" , $detail_pasien['txtIdPasien']);
		$dt['frm_id_rekam_medis'] = $this->form_builder->inputHidden("txtIdRekamMedis" , $idRekamMedis);
		$dt['frm_id_pelayanan'] = $this->form_builder->inputHidden("txtIdPelayanan" , $detail_register['intIdPelayanan']);
		$dt['frm_id_kunjungan'] = $this->form_builder->inputHidden("txtIdKunjungan" , $detail_register['txtIdKunjungan']);
		
		///// Builder To View
		$dt['frm_tanggal'] = $this->form_builder->inputText("Tanggal Pemeriksaan" , "txtTanggalPemeriksaan", indonesian_date($detail_register['dtTanggalKunjungan']) , "col-sm-3" , array("readonly"=>"readonly"));
		$dt['frm_no_anggota_pasien'] = $this->form_builder->inputText("No Anggota" , "txtNoAnggotaPasien", $detail_pasien['txtNoAnggota'] , "col-sm-3" , array("readonly"=>"readonly"));
		$dt['frm_nama_pasien'] = $this->form_builder->inputText("Nama Pengguna Layanan" , "txtNamaPasien", $detail_pasien['txtNamaPasien'] , "col-sm-3" , array("readonly"=>"readonly"));
		$dt['frm_usia_pasien'] = $this->form_builder->inputText("Usia Pengguna Layanan" , "txtUsiaPasien", $detail_register['txtUsiaPasienKunjungan'] , "col-sm-3" , array("readonly"=>"readonly"));
		$dt['frm_jaminan_kesehatan'] = $this->form_builder->inputText("Jaminan Kesehatan" , "txtJamkes", $jamkes_list[$detail_register['intIdJaminanKesehatan']] , "col-sm-3" , array("readonly"=>"readonly"));
		$dt['frm_no_jaminan_kesehatan'] = $this->form_builder->inputText("No Jaminan Kesehatan" , "txtNoJamkes", $detail_register['txtNoJaminanKesehatan'] , "col-sm-3" , array("readonly"=>"readonly"));
		$dt['frm_no_rekam_medis'] = $this->form_builder->inputText("No Rekam Medis" , "txtNoRekamMedis", $noRekamMedis, "col-sm-3" );
		$form_rekmed = $this->_build_form_rekmed_ugd($detail_pasien,$detail_register);
		$breadcrumbs = array(
                        "Home" =>$this->base_url_site,
                        "UGD" => $this->base_url,
                        "Detail Pelayanan"=> "#",
        );
		
        $dt['breadcrumbs'] = $this->setBreadcrumbs($breadcrumbs);
		$dt['frm_rekmed_poli'] = $form_rekmed;
		$ret = $this->load->view("ugd/form" , $dt,true);
        return $ret;  
    }
	
	/// Form Poli Umum + Anak + Gigi
	private function _build_form_rekmed_ugd($detail_pasien , $detail_register){
		// Cek By Id Kunjungan
		$valArray = array("txtIdRekmedDetail" , "txtPemeriksaan" , "txtDetailDiagnosa", "txtTindakan" , "txtKeterangan" , "txtPengobatan","bitJenisApotik");
		$arrSelected = array();
		$dataRekmedByKunjungan = $this->rekam_medis->getDetailByIdKunjungan($detail_register['txtIdKunjungan']);
		
		
		foreach($valArray as $arrFrmMedis) {
				$$arrFrmMedis = isset($dataRekmedByKunjungan[$arrFrmMedis]) ? $dataRekmedByKunjungan[$arrFrmMedis] : "";
		}
		$arrSelected = array($dataRekmedByKunjungan['intIdPenyakit'] => $dataRekmedByKunjungan['txtIndonesianName']);
		$txtKeterangan = !empty($txtKeterangan) ? $txtKeterangan : $detail_register['txtKeteranganKunjungan'];
		
		if(!empty($txtIdRekmedDetail)){
			$styleInput = array("rows"=>3 , "disabled"=>"disabled");
			$styleSelect = array("disabled"=>"disabled");
			$styleSelect2 = array("class"=>"form-control select-diagnosa","disabled"=>"disabled");
			$styleSelect3 = array("class"=>"form-control" , "id"=>"select-tindakan","disabled"=>"disabled");
			$mode = "edit";
		}else{
			$styleInput = array("rows"=>3 );
			$styleSelect = array();
			$styleSelect2 = array("class"=>"form-control select-diagnosa");
			$styleSelect3 = array("class"=>"form-control" , "id"=>"select-tindakan");
			$mode = "insert";	
		}
		
		$periksaLanjut = $detail_register['bitPeriksaLanjut'];
		$dt['frm_status_periksa'] = "";
		if($periksaLanjut!=0){
			$statusPeriksa = $this->config->item('periksa_list'); 
			$txtPeriksa = $statusPeriksa[$periksaLanjut];
			$dt['frm_status_periksa'] = $this->form_builder->inputText("Status Pemeriksaan" , "txtPeriksa", $txtPeriksa , "col-sm-3",array("readonly" => "readonly"));	
		}
		
		$dt['frm_rekmed_id_detail'] = $this->form_builder->inputHidden("txtIdRekmedDetail" , $txtIdRekmedDetail);
		$dt['frm_rekmed_pemeriksaan'] = $this->form_builder->inputTextArea("Pemeriksaan" , "txtPemeriksaan", $txtPemeriksaan , "col-sm-3",$styleInput );
		$dt['frm_rekmed_diagnosa'] = $this->form_builder->inputDropdown("Diagnosa Penyakit" , "selectDiagnosa[]" ,$arrSelected , $arrSelected , $styleSelect2);
		$dt['frm_rekmed_detail_diagnosa'] = $this->form_builder->inputTextArea("Detail Diagnosa" , "txtDetailDiagnosa[]", $txtDetailDiagnosa, "col-sm-3" ,$styleInput);
		$dt['frm_rekmed_tindakan'] = $this->form_builder->inputDropdown("Tindakan" , "selectTindakan[]" ,$arrSelected , $arrSelected , $styleSelect3);;
		$dt['frm_rekmed_detail_tindakan'] = $this->form_builder->inputTextArea("Detail Tindakan" , "txtDetailTindakan[]", "", "col-sm-3" ,$styleInput);
		$dt['frm_rekmed_pengobatan'] = $this->form_builder->inputTextArea("Pengobatan" , "txtPengobatan", $txtPengobatan, "col-sm-3" ,$styleInput);
		$dt['frm_rekmed_keterangan'] = $this->form_builder->inputTextArea("Keterangan Kunjungan" , "txtKeterangan", $txtKeterangan , "col-sm-3" ,$styleInput);
		$dt['link_form_id_pelayanan'] = $this->base_url.$detail_register['intIdPelayanan'];
		$dt['frm_rekmed_mode'] = $mode;
		
		$dt['frm_rekmed_anak'] = "";
        $arrApotik = array("1"=>"Apotik Rawat Jalan" , "2"=>"Apotik Rawat Inap/UGD");
        $dt['frm_apotik_list'] = $this->form_builder->inputDropdown("Apotik" , "selectApotik" , $bitJenisApotik, $arrApotik , $styleSelect);
		//// Input Pemeriksaan Lanjut
		$data_pelayanan = $this->pelayanan_model->getListDataPelayanan();
		$arrPelayanan = array();
		foreach ($data_pelayanan as $indexPelayanan => $valuePelayanan) {
			$arrPelayanan[$valuePelayanan['intIdPelayanan']] = $valuePelayanan['txtNama'];
		}
		$dt['selectLayananLanjut'] = $this->form_builder->inputDropdown("Jenis Pelayanan" , "selectLayananLanjut" ,0, $arrPelayanan);
		$dt['txtKeteranganLanjut'] = $this->form_builder->inputTextArea("Kebutuhan Pemeriksaan" , "txtKeteranganPemeriksaan" , "");
		
        $ret = $this->load->view("ugd/form_rekam_medis" , $dt , true);
		return $ret;
	}
}