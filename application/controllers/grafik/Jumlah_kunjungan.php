<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Jumlah_kunjungan extends MY_Controller {

    var $meta_title = "Grafik";
    var $meta_desc = "";
    var $main_title = "";
    var $menu_key = "";
    var $dtBreadcrumbs = array();  

    public function __construct() {
        parent::__construct();
        $this->load->model('pelayanan_model');
        $this->load->model('register_model');
        $this->load->model('grafik_model');
        $this->load->library("Excel");
    }
    public function index() {        
    
        $this->dtBreadcrumbs = array(
            "Home" =>base_url(),
            "Grafik" => base_url(),
            "Jumlah Kunjungan" => "#",
        );

        $this->meta_title = "Grafik Jumlah Kunjungan";
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "menu_key" => "grafik_pengunjung_per_poli",
			"akses_key" => "is_view",
            "container" => $this->_build_rekap_pengunjung(),
            "custom_js" => array(
               ASSETS_URL . "plugins/morris/morris.min.js",
                ASSETS_JS_URL."grafik/rekap_jumlah_pengunjung.js"
			),
            "custom_css" => array(
                 ASSETS_URL . "plugins/morris/morris.css",
            ),
        );  
        
        $this->_render("default",$dt);             
    }


    private function _build_rekap_pengunjung(){

        $listPelayanan = $this->pelayanan_model->getListDataPelayanan();
        $arrPelayanan = array(""=>"Semua");
        foreach ($listPelayanan as $valuePelayanan) {
            # code...
            $arrPelayanan[$valuePelayanan['intIdPelayanan']] = $valuePelayanan['txtNama'];
        }
        $label = $this->grafik_model->labelKunjungan("" , "", "");
        $dt['header'] = $label;
        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['frmPelayanan'] = $this->form_builder->inputDropdown("Pelayanan" , "cmbPoli" , "" , $arrPelayanan);
        $dt['title'] = $this->meta_title;
        $ret = $this->load->view("grafik/jumlah_kunjungan" , $dt , true);
        return $ret;
    }

    public function getRekapData(){
        if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";die;
        }

        $idPelayanan =  $this->input->post("id_pelayanan");
		$start_date = $this->input->post("start_date");
        $end_date = $this->input->post("end_date");
        $retVal['data'] = array();
        $no = 0;
       $begin = new DateTime($start_date);
              $end = new DateTime($end_date);
              $end->modify('+1 day');
              $daterange = new DatePeriod($begin, new DateInterval('P1D'), $end);
              foreach($daterange as $date){
              $no ++;
			  $row = $this->grafik_model->get($date->format("Y-m-d"));
			    $dt = array();
                            $dt[] = $no;
                            $dt[] = $date->format("Y-m-d");
                            if (!empty($idPelayanan)) {
                            
                            $dt[] = $idPelayanan == 1 ?  $row['POLI_UMUM'] : "-";
                            $dt[] = $idPelayanan == 4 ? $row['POLI_ANAK'] : "-";
                            $dt[] = $idPelayanan == 3 ? $row['POLI_GIGI'] : "-";
                            $dt[] = $idPelayanan == 7 ? $row['LAB'] : "-";
                            $dt[] = $idPelayanan == 9 ? $row['POLI_GIZI'] : "-";
                            $dt[] = $idPelayanan == 5 ? $row['POLI_IBU'] : "-";
                            $dt[] = $idPelayanan == 6 ? $row['UGD'] : "-";
                            $dt[] = $idPelayanan == 8 ? $row['RAWAT_INAP'] : "-";
                            $dt[] = $idPelayanan == 42?$row['POLI_LANSIA'] : "-";
                            }else {
                            $dt[] =  $row['POLI_UMUM'];
                            $dt[] = $row['POLI_ANAK'];
                            $dt[] = $row['POLI_GIGI'];
                            $dt[] = $row['LAB'];
                            $dt[] = $row['POLI_GIZI'];
                            $dt[] = $row['POLI_IBU'];
                            $dt[] = $row['UGD'];
                            $dt[] = $row['RAWAT_INAP'];
                            $dt[] = $row['POLI_LANSIA'];
                            }
                            
                           $retVal['data'][] = $dt; 
              }
        $this->setJsonOutput($retVal);
    }
    public function getDataRekapJumlahPengunjung(){
       
 if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";die;
        }
		
        $idPelayanan =  $this->input->post("cmbPoli");
		$start_date = $this->input->post("start_date");
        $end_date = $this->input->post("end_date");
        
       
        $begin = new DateTime($start_date);
              $end = new DateTime($end_date);
              $end->modify('+1 day');
              $daterange = new DatePeriod($begin, new DateInterval('P1D'), $end);
              foreach($daterange as $date){
              
			  $row = $this->grafik_model->get($date->format("Y-m-d"));
			    $dt = array();
                $dt['x'] = $date->format("Y-m-d");
                            if (!empty($idPelayanan)) {
                            
                            $idPelayanan == 1 ? $dt[ 'RUANGAN UMUM'] =  $row['POLI_UMUM'] : "";
                            $idPelayanan == 4 ?$dt['RUANGAN ANAK'] = $row['POLI_ANAK'] : "";
                            $idPelayanan == 3 ?$dt['RUANGAN GIGI'] = $row['POLI_GIGI'] : "";
                            $idPelayanan == 7 ?$dt['LAB'] = $row['LAB'] : "";
                            $idPelayanan == 9 ?$dt['RUANGAN GIZI'] = $row['POLI_GIZI'] : "";
                            $idPelayanan == 5 ?$dt['RUANGAN IBU'] = $row['POLI_IBU'] : "";
                            $idPelayanan == 6 ?$dt['UGD'] = $row['UGD'] : "";
                            $idPelayanan == 8 ?$dt['RAWAT INAP'] = $row['RAWAT_INAP'] : "";
                            $idPelayanan == 42 ?$dt['RUANGAN LANSIA'] =$row['POLI_LANSIA'] : "";
                            }else {
                                
                            $dt[ 'RUANGAN UMUM'] =       $row['POLI_UMUM'];
                            $dt['RUANGAN ANAK'] =       $row['POLI_ANAK'];
                            $dt['RUANGAN GIGI'] =      $row['POLI_GIGI'];
                            $dt['LAB'] =     $row['LAB'];
                            $dt['RUANGAN GIZI'] =    $row['POLI_GIZI'];
                            $dt['RUANGAN IBU'] =   $row['POLI_IBU'];
                            $dt['UGD'] =  $row['UGD'];
                            $dt['RAWAT INAP'] = $row['RAWAT_INAP'];
                            $dt['RUANGAN LANSIA'] =$row['POLI_LANSIA'];
                            }
                            
                            $data[] = $dt; 
              }
			 
     	$dat = array('RUANGAN UMUM','RUANGAN ANAK','RUANGAN GIGI','LAB','RUANGAN GIZI', 'RUANGAN IBU', 'UGD','RAWAT INAP', 'RUANGAN LANSIA');
       $retVal = array();
        $retVal['label'] = $dat;
        $retVal['data'] = $data;
        echo json_encode($retVal);
    
        
       
    }
public function getDataRekapJumlahPengunjungBackup(){
        if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";die;
        }

        $idPelayanan =  $this->input->post("cmbPoli");
		$start_date = $this->input->post("start_date");
        $end_date = $this->input->post("end_date");
      
        $data = array();
        $dat = array();
        $dataJumlahKunjungan = $this->grafik_model->getJumlahKunjungan($start_date , $end_date, $idPelayanan);
       
         foreach ($dataJumlahKunjungan as $dt) {
            $row = array();
            $row['x'] = $dt['tanggal'];     
            $row[$dt['nama']] = $dt['jumlah'];
            $data[] = $row;
            
        }
        $label = $this->grafik_model->labelKunjungan($start_date , $end_date, $idPelayanan);
        foreach ($label as $key) {
            $dat[] = $key['nama'];
        }
        $retVal = array();
        
        $retVal['label'] = $dat;
        $retVal['data'] = $data;
        echo json_encode($retVal);
    }
   

    
}