<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Data_penyakit extends MY_Controller {

    var $meta_title = "Grafik";
    var $meta_desc = "";
    var $main_title = "";
    var $menu_key = "";
    var $dtBreadcrumbs = array();  

    public function __construct() {
        parent::__construct();
        $this->load->model('pelayanan_model');
        $this->load->model('register_model');
        $this->load->model('grafik_model');
        $this->load->library("Excel");
    }
    public function index() {        
    
        $this->dtBreadcrumbs = array(
            "Home" =>base_url(),
            "Grafik" => base_url(),
            "Data Penyakit" => "#",
        );

        $this->meta_title = "Grafik - Data Penyakit";
        $dt = array(
            "title" => "Grafik - Data Penyakit",
            "description" => "Grafik - Data Penyakit",
            "menu_key" => "data_penyakit",
			"akses_key" => "is_view",
            "container" => $this->_build_data_penyakit(),
            "custom_js" => array(
               ASSETS_URL . "plugins/morris/morris.min.js",
                ASSETS_JS_URL."grafik/data_penyakit.js"
			),
            "custom_css" => array(
                 ASSETS_URL . "plugins/morris/morris.css",
            ),
        );
        
        $this->_render("default",$dt);
    }

    private function _build_data_penyakit(){

        $listPelayanan = $this->pelayanan_model->getListDataPelayanan();
        $arrPelayanan = array("0"=>"Semua");
        foreach ($listPelayanan as $valuePelayanan) {
            # code...
            $arrPelayanan[$valuePelayanan['intIdPelayanan']] = $valuePelayanan['txtNama'];
        }

        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['frmPelayanan'] = $this->form_builder->inputDropdown("Ruangan" , "cmbPoli" , "" , $arrPelayanan);
        $dt['title'] = $this->meta_title;
        $ret = $this->load->view("grafik/data_penyakit" , $dt , true);
        return $ret;
    }

    public function getDataPenyakit(){
         if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";die;
        }

        $idPelayanan =  $this->input->post("id_pelayanan");
		$start_date = $this->input->post("start_date");
        $end_date = $this->input->post("end_date");
        $dataPengunjung = $this->register_model->getRekapDataPenyakitGrafik($start_date , $end_date , $idPelayanan);
        $data = array(); 
        foreach ($dataPengunjung as $dt) {
           $row = array();
           $row['label'] = $dt['txtIndonesianName'];
           $row['value'] = $dt['jumlah'];       
            $data[] = $row;
        }
       echo json_encode($data);
    }
    public function getRekapDataPenyakit(){
        if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";die;
        }

        $idPelayanan =  $this->input->post("cmbPoli");
		$start_date = $this->input->post("start_date");
        $end_date = $this->input->post("end_date");
        $date = array();
              $date['start'] = $start_date;
              $date['end'] = $end_date;
        $dataPengunjung = $this->register_model->getDataPenyakitV2($date, $idPelayanan);
        $retVal['data'] = array();
        $no = 0;
        foreach($dataPengunjung as $row){
            $no ++;
            $kodeICDX = $row['txtCategory'];
            $retVal['data'][] = array(
                                    $no,
                                    $kodeICDX.' - '.$row['txtIndonesianName'],
                                    $row['jumlah_lama'],
                                    $row['jumlah_baru'],
                                    $row['jumlah_baru'] + $row['jumlah_lama']
                                );
        }
        $this->setJsonOutput($retVal);
    }
    function getDataPenyakitV2(){
        if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";die;
        }

        $idPelayanan =  $this->input->post("cmbPoli");
		$start_date = $this->input->post("start_date");
        $end_date = $this->input->post("end_date");
              
              $date = array();
              $date['start'] = $start_date;
              $date['end'] = $end_date;
              
              $dataPenyakit = $this->register_model->getDataPenyakitV2($date, $idPelayanan);
              $data= array();
              if (!empty($dataPenyakit)) {
                  foreach($dataPenyakit as $dt){
                $row = array();
                $row['y'] = $dt['txtCategory'];
                $row['a'] = $dt['jumlah_lama']; 
                $row['b'] = $dt['jumlah_baru']; 
                $row['c'] = $dt['jumlah_lama'] + $dt['jumlah_baru'];       
                $data[] = $row;
                
              }
              }else{
                $row['y'] = 'data Penyakit';
                $row['a'] = 0; 
                $row['b'] = 0; 
                $row['c'] = 0;       
                $data[] = $row;
              }
              
               
                echo json_encode($data);
              
    }

    
}