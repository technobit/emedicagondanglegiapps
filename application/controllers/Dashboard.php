<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {
    var $meta_title = "Dashboard";
    var $meta_desc = "Dashboard";
	var $main_title = "Dashboard";
    var $base_url = "";
	var $upload_dir = "";
	var $upload_url = "";
	var $limit = "10";
	var $arr_level_user = "";
    var $menu = "dashboard";
    public function __construct(){
        parent::__construct();
		$this->load->model("register_model");
		$this->load->model("register_loket_model");
		$this->load->model("grafik_model");
        $this->base_url = $this->base_url_site."dashboard/";
        
    } 
    
	public function index()
	{	
		$dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
			"menu_key" => "dashboard",
			"akses_key" => "is_view",
			"container" => $this->_home(),
			"custom_js" => array(
				ASSETS_URL."plugins/raphael/raphael.min.js",
				ASSETS_URL."plugins/morris/morris.min.js",
				ASSETS_JS_URL."dashboard/dashboard.js",
			),
            "custom_css" => array(
				ASSETS_URL."plugins/morris/morris.css",
			),
		);	
		$this->_render("default",$dt);	
	}
	
    private function _home(){
     	$menu = $this->session->userdata("sipt_session_menu");  
        $dt = array();
        $ret = $this->load->view("dashboard/content" , $dt , true);
        return $ret;
    }
	
	public function getDataKunjungan(){
		if(!$this->input->is_ajax_request()){
			echo  "Die, Its Ilegal";
		}
		$start = 0;
		//// Date
		$date_start = date("Y-m-d"). " 00:00:00";
		$date_end = date("Y-m-d"). " 23:59:59";
		$date = new DateTime(date("Y-m-d"));

		$start_week = date("Y-m-d", strtotime("last monday midnight", strtotime("this week 1 day"))). " 00:00:00";
		$end_week = date("Y-m-d", strtotime("next sunday", strtotime("last sunday midnight", strtotime("this week 1 day")))). " 23:59:59";
		
		$start_month = date('Y-m-d',strtotime('first day of this month')). " 00:00:00";
		$end_month = date('Y-m-d',strtotime('last day of this month')). " 23:59:59";
		
		// $pengunjung_harian = $this->register_model->countRegisterByPelayananSum($date_start , $date_end);
		// $pengunjung_mingguan = $this->register_model->countRegisterByPelayananSum($start_week , $end_week);
		// $pengunjung_bulanan = $this->register_model->countRegisterByPelayananSum($start_month , $end_month);
		// $pengunjung_harian_sebulan = $this->register_model->countRegisterByDate($start_month , $end_month);
		
		$pengunjung_harian = $this->register_loket_model->countRegisterByPelayananSum($date_start , $date_end);
		$pengunjung_mingguan = $this->register_loket_model->countRegisterByPelayananSum($start_week , $end_week);
		$pengunjung_bulanan = $this->register_loket_model->countRegisterByPelayananSum($start_month , $end_month);
		$pengunjung_harian_sebulan = $this->register_loket_model->countRegisterByDate($start_month , $end_month);
		
		//Jml Harian
		if(empty($pengunjung_harian)){$jumlah_pengunjung_harian = "0";}
		$jml_array_harian = count($pengunjung_harian);
		$array_harian = array();
		for($i = 0; $i < $jml_array_harian; $i++){
			$array_harian_push = array_push($array_harian, $pengunjung_harian[$i]['jumlah']);
			$jumlah_pengunjung_harian = array_sum($array_harian);
			}
		//Jml Mingguan
		if(empty($pengunjung_mingguan)){$jumlah_pengunjung_mingguan = "0";}
		$jml_array_mingguan = count($pengunjung_mingguan);
		$array_mingguan = array();
		for($i = 0; $i < $jml_array_mingguan; $i++){
			$array_mingguan_push = array_push($array_mingguan, $pengunjung_mingguan[$i]['jumlah']);
			$jumlah_pengunjung_mingguan = array_sum($array_mingguan);
			}
		//Jml bulanan
		if(empty($pengunjung_bulanan)){$jumlah_pengunjung_bulanan = "0";}
		$jml_array_bulanan = count($pengunjung_bulanan);
		$array_bulanan = array();
		for($i = 0; $i < $jml_array_bulanan; $i++){
			$array_bulanan_push = array_push($array_bulanan, $pengunjung_bulanan[$i]['jumlah']);
			$jumlah_pengunjung_bulanan = array_sum($array_bulanan);
			}
		
		$retVal = array();
		$retVal['pengunjung_harian'] = $pengunjung_harian;
		$retVal['jumlah_pengunjung_harian'] = $jumlah_pengunjung_harian;
		$retVal['pengunjung_mingguan'] = $pengunjung_mingguan;
		$retVal['jumlah_pengunjung_mingguan'] = $jumlah_pengunjung_mingguan;
//		$retVal['jumlah_pengunjung_mingguan'] = $start_week." - ".$end_week;
		$retVal['pengunjung_bulanan'] = $pengunjung_bulanan;
		$retVal['jumlah_pengunjung_bulanan'] = $jumlah_pengunjung_bulanan;
		$retVal['pengunjung_sebulan_per_hari'] = $pengunjung_harian_sebulan;
		//$retVal['data_register'] = $data_register;
		$this->setJsonOutput($retVal);
	}
	public function getAntrianLoket(){
		if(!$this->input->is_ajax_request()){
			echo  "Die, Its Ilegal";
		}
		$status = false;
		$start = 0;
		//// Date
		$date_start = date("Y-m-d"). " 00:00:00";
		$date_end = date("Y-m-d"). " 23:59:59";
		$data_register = $this->register_model->getRegisterAntrianRawatJalan($date_start , $date_end);
		$jumlah_pengunjung = $this->register_model->countRegisterByPelayanan("",$date_start , $date_end , "");
		$jumlah_sudah = $this->register_model->countRegisterByPelayanan("",$date_start , $date_end , "3");
		$jumlah_sedang = $this->register_model->countRegisterByPelayanan("",$date_start , $date_end , "2");
		$jumlah_antri = $this->register_model->countRegisterByPelayanan("",$date_start , $date_end , "1");
		$status_layanan_list = $this->config->item("status_layanan_list");
		$resData = array();
		if(!empty($data_register)){
			$status = true;
			foreach($data_register as $indexRegister => $rowRegister){
				$personAge = $rowRegister['txtUsiaPasienKunjungan'];
				$data["idKunjungan"] = $rowRegister['txtIdKunjungan'];
				$data["noAntri"] = $rowRegister['intNoAntri'];
				$data["namaPasien"] = $rowRegister['txtNamaPasien'];
				$data["noAnggota"] = $rowRegister['txtNoAnggota'];
				$data["usiaPasien"] = $personAge;
				$data["namaPoli"] = $rowRegister['txtNamaPelayanan'];
				$resData[] = $data;
			}
		}
		
		$retVal = array();
		$retVal['status'] = $status;
		$retVal['data'] = $resData;
		$retVal['jumlah_pengunjung'] = $jumlah_pengunjung;
		$retVal['jumlah_sudah'] = $jumlah_sudah;
		$retVal['jumlah_sedang'] = $jumlah_sedang;
		$this->setJsonOutput($retVal);
	}
	public function getDataRekapJumlahPengunjung(){
        if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";die;
        }
		$end = strtotime("-1 week");  
        $start_date = date("Y-m-d",$end);
        $stat = strtotime("+1 days");  
        $end_date = date("Y-m-d",$stat);
      
        $begin = new DateTime($start_date);
              $end = new DateTime($end_date );
              $daterange = new DatePeriod($begin, new DateInterval('P1D'), $end);
              foreach($daterange as $date){
              
			  $row = $this->grafik_model->get($date->format("Y-m-d"));
			    $dt = array();
                            $dt['x'] = $date->format("Y-m-d");;
                            $dt[ 'RUANGAN UMUM'] =       $row['POLI_UMUM'];
                            $dt['RUANGAN ANAK'] =       $row['POLI_ANAK'];
                            $dt['RUANGAN GIGI'] =      $row['POLI_GIGI'];
                            $dt['LAB'] =     $row['LAB'];
                            $dt['RUANGAN GIZI'] =    $row['POLI_GIZI'];
                            $dt['RUANGAN IBU'] =   $row['POLI_IBU'];
                            $dt['UGD'] =  $row['UGD'];
                            $dt['RAWAT INAP'] = $row['RAWAT_INAP'];
                            $dt['RUANGAN LANSIA'] =$row['POLI_LANSIA'];
                            $data[] = $dt; 
              }
			 
     	$dat = array('RUANGAN UMUM','RUANGAN ANAK','RUANGAN GIGI','LAB','RUANGAN GIZI', 'RUANGAN IBU', 'UGD','RAWAT INAP', 'RUANGAN LANSIA');
       $retVal = array();
        $retVal['label'] = $dat;
        $retVal['data'] = $data;
        echo json_encode($retVal);
    
    }
	public function getDataRekapJumlahPengunjungBackup(){
        if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";die;
        }
		
       $end = strtotime("-1 week");  
       $start_date = date("Y-m-d",$end);
       $end_date = date("Y-m-d");
        $dat = array();
        $dataPengunjung = $this->grafik_model->getJumlahKunjunganTable($start_date , $end_date , "");
     	$data = array();
       
		foreach($dataPengunjung as $row){
          $dt = array();
                            $dt['x'] = $row['tanggal'];
                            $dt[ 'RUANGAN UMUM'] =       $row['POLI_UMUM'];
                            $dt['RUANGAN ANAK'] =       $row['POLI_ANAK'];
                            $dt['RUANGAN GIGI'] =      $row['POLI_GIGI'];
                            $dt['LAB'] =     $row['LAB'];
                            $dt['RUANGAN GIZI'] =    $row['POLI_GIZI'];
                            $dt['RUANGAN IBU'] =   $row['POLI_IBU'];
                            $dt['UGD'] =  $row['UGD'];
                            $dt['RAWAT INAP'] = $row['RAWAT_INAP'];
                            $dt['RUANGAN LANSIA'] =$row['POLI_LANSIA'];
                            $data[] = $dt;  
                               
        
		}
		$dat = array('RUANGAN UMUM','RUANGAN ANAK','RUANGAN GIGI','LAB','RUANGAN GIZI', 'RUANGAN IBU', 'UGD','RAWAT INAP', 'RUANGAN LANSIA');
       $retVal = array();
        $retVal['label'] = $dat;
        $retVal['data'] = $data;
        echo json_encode($retVal);
    }
	 public function getDataPenyakit(){
         if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";die;
        }

        $dataPengunjung = $this->grafik_model->getRekapDataPenyakitGrafik();
        $data = array(); 
        foreach ($dataPengunjung as $dt) {
           $row = array();
           $row['label'] = $dt['txtIndonesianName'];
           $row['value'] = $dt['jumlah'];       
            $data[] = $row;
        }
       echo json_encode($data);
    }
	
	
}
