<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Poliklinik extends MY_Controller {   
    var $meta_title = "Poliklinik";
    var $meta_desc = "Poliklinik";
	var $main_title = "Pelayanan Poliklinik";
    var $base_url = "";
	var $base_url_pasien = "";
	var $upload_dir = "";
	var $upload_url = "";
	var $limit = "10";
	var $arr_level_user = "";
    var $menu = "K01";
	var $jenis_poli = "";
	var $menu_key = "";
    public function __construct(){
        parent::__construct();
        $this->base_url = $this->base_url_site."pelayanan/poliklinik/";

		$this->load->model(
			array(
				"pelayanan_model",
				"register_model",
				"pasien_model",
				"tindakan_model",
				"rekam_medis_model" => "rekam_medis",
				"jaminan_kesehatan_model" => "jaminan_kesehatan",
			)
		);
    } 
    
    public function index( $id_pelayanan ){
		$menu = "P05";
		$asset_js = ASSETS_JS_URL."pelayanan/poliklinik/poli.js";
		$dt = array(
			"container" => $this->_pelayanan( $id_pelayanan),
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
			"menu_key" => $this->menu_key,
			"akses_key" => "is_view",	
			"custom_js" => array(
				$asset_js,
			),
            "custom_css" => array(
				
			),
		);	
		$this->_render("default",$dt);	
    }
	
	public function poli_ibu($id_pelayanan){
		$menu = "P05";
		$asset_js = ASSETS_JS_URL."pelayanan/poli_ibu/poli.js";
		$dt = array(
			"container" => $this->_pelayanan( $id_pelayanan),
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
			"menu_key" => "poli_ibu",
			"akses_key" => "is_view",	
			"custom_js" => array(
				$asset_js,
			),
            "custom_css" => array(
				
			),
		);	
		$this->_render("default",$dt);
	}
	
    public function detail($id_kunjungan){
		$menu = "PD01";
		$asset_js = ASSETS_JS_URL."pelayanan/poliklinik/form.js";
		
		$dt = array(
			"container" => $this->_detail_pelayanan( $id_kunjungan),
			"menu_key" => $this->menu_key,
			"akses_key" => "is_insert",	
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
			"custom_js" => array(
				ASSETS_URL."plugins/select2/select2.js",
				ASSETS_PLUGIN_URL . "jSignature/libs/jSignature.min.js",
				ASSETS_PLUGIN_URL . "jSignature/plugins/jSignature.CompressorBase30.js",
				ASSETS_PLUGIN_URL . "jSignature/plugins/jSignature.CompressorSVG.js",
				ASSETS_URL."plugins/validate/jquery.validate_1.11.1.min.js",
				$asset_js
			),
            "custom_css" => array(
				ASSETS_URL."plugins/select2/select2.css",
			),
		);	
		$this->_render("default",$dt);	
    }
	
	public function detail_ibu($id_kunjungan){
		$menu = "PD01";
		$asset_js = ASSETS_JS_URL."pelayanan/poli_ibu/form.js";
		$dt = array(
			"container" => $this->_build_form_pelayanan_ibu( $id_kunjungan),
			"menu_key" => "poli_ibu",
			"akses_key" => "is_insert",	
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
			"custom_js" => array(
				ASSETS_URL."plugins/select2/select2.js",
				ASSETS_URL."plugins/validate/jquery.validate_1.11.1.min.js",
				$asset_js
			),
            "custom_css" => array(
				ASSETS_URL."plugins/select2/select2.css",
			),
		);	
		$this->_render("default",$dt);
	}

	private function updateStatusRegisterPoli($idKunjungan , $statusPelayanan = 2){

		$arrUpdate = array(
			"bitIsPoli" => $statusPelayanan,
			"dtLastUpdateKunjungan"=> date("Y-m-d H:i:s")
		);
		$resUpdate = $this->register_model->update($arrUpdate , $idKunjungan);
		return $resUpdate;
	}
	
    private function _detail_pelayanan($id_kunjungan){
		
        $detail_register = $this->register_model->getDetailRegisterPelayanan($id_kunjungan);
		
		$noAntrian = $detail_register['intNoAntri'];
		if(count($detail_register) < 1){
            show_404();
        }
		$statusPelayananPoli = $detail_register['bitIsPoli'];

		if($statusPelayananPoli==1){
			/// Update To Sedang Di Layani
			$this->updateStatusRegisterPoli($id_kunjungan , 2);	
		}
		
		$idPasien = $detail_register['txtIdPasien'];
		$idPelayanan = $detail_register['intIdPelayanan'];
		$menuKey = $detail_register['txtSlug'];
		if($menuKey=='ugd'){
			$menuKey = 'ugd_pelayanan';
		}
		$this->menu_key = $menuKey;
		$this->jenis_poli = $detail_register['intIdJenisPelayanan'];		
		$nama_pasien = $detail_register['txtNamaPasien'];
		$idRekamMedis = "";
		$noRekamMedis = $detail_register['txtNoAnggota'];
		
		if(!empty($detail_register['txtIdRekamMedis'])){
			$idRekamMedis = $detail_register['txtIdRekamMedis'];
			$noRekamMedis = $detail_register['txtNoRekamMedis'];
		}
		
		$nama_poli = $detail_register['txtNama'];
		$nama_kelurahan = $detail_register['Nama'];
        ///$this->meta_title = $nama_poli;
        ///
		$this->meta_title = $nama_pasien . "-" . $noAntrian . "[".$nama_poli."]";
		$this->meta_desc = "Fasilitas ".$nama_poli;
		//// Update Status Menjadi Sedang Di Layani
		$dt['title'] = "Pemeriksaan Rawat Jalan ".$nama_poli;
		$dt['form_title'] = "No Pengguna Layanan ".$detail_register['txtNoAnggota'];
		
		/// Build Header Form -> Hidden
		$dt['frm_id_pasien'] = $this->form_builder->inputHidden("txtIdPasien" , $detail_register['txtIdPasien']);
		$dt['frm_id_rekam_medis'] = $this->form_builder->inputHidden("txtIdRekamMedis" , $idRekamMedis);
		$dt['frm_id_pelayanan'] = $this->form_builder->inputHidden("txtIdPelayanan" , $detail_register['intIdPelayanan']);
		$dt['frm_id_kunjungan'] = $this->form_builder->inputHidden("txtIdKunjungan" , $detail_register['txtIdKunjungan']);
		$dt['frm_id_rujukan'] = $this->form_builder->inputHidden("txtRefIdKunjungan" , $detail_register['txtRefIdKunjungan']);
		
		///// Builder To View
		$dt['frm_tanggal'] = $this->form_builder->inputText("Tanggal Pemeriksaan" , "txtTanggalPemeriksaan", indonesian_date($detail_register['dtTanggalKunjungan']) , "col-sm-3" , array("readonly"=>"readonly"));
		$dt['frm_no_anggota_pasien'] = $this->form_builder->inputText("No Anggota / Rekam Medis" , "txtNoAnggotaPasien", $detail_register['txtNoAnggota'] , "col-sm-3" , array("readonly"=>"readonly"));
		$dt['frm_nama_pasien'] = $this->form_builder->inputText("Nama Pengguna Layanan" , "txtNamaPasien", $detail_register['txtNamaPasien'] , "col-sm-3" , array("readonly"=>"readonly"));
		$dt['frm_alamat_pasien'] = $this->form_builder->inputText("Alamat" , "txtAlamat", $detail_register['Nama'] , "col-sm-3" , array("readonly"=>"readonly"));
		$dt['frm_usia_pasien'] = $this->form_builder->inputText("Usia Pengguna Layanan" , "txtUsiaPasien", $detail_register['txtUsiaPasienKunjungan'] , "col-sm-3" , array("readonly"=>"readonly"));
		$dt['frm_jaminan_kesehatan'] = $this->form_builder->inputText("Jaminan Kesehatan" , "txtJamkes", $detail_register['txtNamaJaminan'] , "col-sm-3" , array("readonly"=>"readonly"));
		$dt['frm_no_jaminan_kesehatan'] = $this->form_builder->inputText("No Jaminan Kesehatan" , "txtNoJamkes", $detail_register['txtNoJaminanKesehatan'] , "col-sm-3" , array("readonly"=>"readonly"));
		
		$dt['frm_txt_kunjungan'] = $this->form_builder->inputTextArea("Keterangan Kunjungan" , "txtKetKunjungan", $detail_register['txtKeteranganKunjungan'], "col-sm-3", array("readonly"=>"readonly"));
		
		$form_rekmed = $this->_build_form_poliklinik($detail_register);
		$breadcrumbs = array(
                        "Home" =>$this->base_url_site,
                        $nama_poli => $this->base_url,
                        "Data Pelayanan ".$nama_poli => "#",
        );
        $dt['breadcrumbs'] = $this->setBreadcrumbs($breadcrumbs);
		$dt['frm_rekmed_poli'] = $form_rekmed;
		$ret = $this->load->view("pelayanan/form" , $dt,true);
        return $ret;  
    }
	
	/// Form Poli Umum + Anak + Gigi
	private function _build_form_poliklinik( $detail_register){
		// Cek By Id Kunjungan
		$valArray = array("txtIdRekmedDetail" , "txtPemeriksaan" , "txtDetailDiagnosa", "txtTindakan" , "txtKeterangan" , "txtPengobatan");
		$arrSelected = array();
		$dataRekmedByKunjungan = $this->rekam_medis->getDetailByIdKunjungan($detail_register['txtIdKunjungan']);
		
		foreach($valArray as $arrFrmMedis) {
				$$arrFrmMedis = isset($dataRekmedByKunjungan[$arrFrmMedis]) ? $dataRekmedByKunjungan[$arrFrmMedis] : "";
		}
		
		$arrSelected = array($dataRekmedByKunjungan['intIdPenyakit'] => $dataRekmedByKunjungan['txtIndonesianName']);
		$txtKeterangan = !empty($txtKeterangan) ? $txtKeterangan : $detail_register['txtKeteranganKunjungan'];
		
		if(!empty($txtIdRekmedDetail)){
			$styleInput = array("rows"=>3 , "disabled"=>"disabled");
			$styleSelect = array("disabled"=>"disabled");
			$styleSelect2 = array("class"=>"form-control select-diagnosa","disabled"=>"disabled");
			$styleSelect3 = array("class"=>"form-control" , "id"=>"select-tindakan","disabled"=>"disabled");
			$mode = "edit";
		}else{
			$styleInput = array("rows"=>3 );
			$styleSelect = array();
			$styleSelect2 = array("class"=>"form-control select-diagnosa");
			$styleSelect3 = array("class"=>"form-control" , "id"=>"select-tindakan");
			$mode = "insert";	
		}
		
		// Input Rekam Medis
		$dt['frm_rekmed_id_detail'] = $this->form_builder->inputHidden("txtIdRekmedDetail" , $txtIdRekmedDetail);
		$dt['frm_rekmed_pemeriksaan'] = $this->form_builder->inputTextArea("Pemeriksaan" , "txtPemeriksaan", $txtPemeriksaan , "col-sm-3",$styleInput );
		$dt['frm_rekmed_diagnosa'] = $this->form_builder->inputDropdown("Diagnosa Penyakit" , "selectDiagnosa[]" ,$arrSelected , $arrSelected , $styleSelect2);
		$dt['frm_rekmed_detail_diagnosa'] = $this->form_builder->inputTextArea("Detail Diagnosa" , "txtDetailDiagnosa[]", $txtDetailDiagnosa, "col-sm-3" ,$styleInput);
		$dt['frm_rekmed_tindakan'] = $this->form_builder->inputDropdown("Tindakan" , "selectTindakan[]" ,$arrSelected , $arrSelected , $styleSelect3);;
		$dt['frm_rekmed_detail_tindakan'] = $this->form_builder->inputTextArea("Detail Tindakan" , "txtDetailTindakan[]", "", "col-sm-3" ,$styleInput);
		$dt['frm_rekmed_pengobatan'] = $this->form_builder->inputTextArea("Pengobatan" , "txtPengobatan", $txtPengobatan, "col-sm-3" ,$styleInput);
		$dt['frm_rekmed_keterangan'] = $this->form_builder->inputTextArea("Keterangan Kunjungan" , "txtKeterangan", $txtKeterangan , "col-sm-3" ,$styleInput);
		$dt['link_form_id_pelayanan'] = $this->base_url.$detail_register['intIdPelayanan'];
		$dt['frm_rekmed_mode'] = $mode;
		
		$dt['frm_rekmed_anak'] = "";
		//// Input Pemeriksaan Lanjut
		$data_pelayanan = $this->pelayanan_model->getListDataPelayanan();
		$arrPelayanan = array();
		foreach ($data_pelayanan as $indexPelayanan => $valuePelayanan) {
			$arrPelayanan[$valuePelayanan['intIdPelayanan']] = $valuePelayanan['txtNama'];
		}
		
		$arrSelectMode = array("1"=> "Pemeriksaan Lanjut" , "2"=>"Rujukan");
		$dt['selectMode'] = $this->form_builder->inputRadioGroup("Mode Rujukan" , "selectModeRujukan","",$arrSelectMode);
		$dt['selectLayananLanjut'] = $this->form_builder->inputDropdown("Jenis Pelayanan" , "selectLayananLanjut" ,0, $arrPelayanan);
		$dt['txtKeteranganLanjut'] = $this->form_builder->inputTextArea("Keterangan" , "txtKeteranganPemeriksaan" , "");

		/// Tambah Form Poli Ibu
		$dt['jenisPelayanan'] = $this->jenis_poli; 
        $ret = $this->load->view("pelayanan/poliklinik/form" , $dt , true);
		return $ret;
	}
	
	private function _build_form_pelayanan_ibu($id_kunjungan){
		$detail_register = $this->register_model->getDetailRegister($id_kunjungan);

        if(count($detail_register) < 1){
            show_404();
        }
		$statusPelayananPoli = $detail_register['bitIsPoli'];
		if($statusPelayananPoli==1){
			/// Update To Sedang Di Layani
			$this->updateStatusRegisterPoli($id_kunjungan , 2);	
		}

		$idPasien = $detail_register['txtIdPasien'];
		$idPelayanan = $detail_register['intIdPelayanan'];
        $detail_pelayanan = $this->pelayanan_model->getDetail($idPelayanan);
		$detail_pasien = $this->pasien_model->getDetail($idPasien);
		
		$jamkes_list = $this->jaminan_kesehatan->getListDataJaminan();
		$jamkes_list["0"] = "Umum / Tidak Ada";
		$jamkes_list[""] = "Umum / Tidak Ada";
		
		$rekam_medis = $this->rekam_medis->getDetailByIdPasien($idPasien);
		$idRekamMedis = "";
		$noRekamMedis = $detail_pasien['txtNoAnggota'];
		
		if(count($rekam_medis) > 0){
			$idRekamMedis = $rekam_medis['txtIdRekamMedis'];
			$noRekamMedis = $rekam_medis['txtNoRekamMedis'];
		}
		
		$nama_poli = $detail_pelayanan['txtNama'];
		
        $this->meta_title = $nama_poli;
        $this->meta_desc = "Fasilitas ".$nama_poli;
		//// Update Status Menjadi Sedang Di Layani
        
		$dt['title'] = "Pemeriksaan Rawat Jalan ".$nama_poli;
		$dt['form_title'] = "No Pasien ".$detail_pasien['txtNoAnggota'];
		
		/// Build Header Form -> Hidden
		$dt['frm_id_pasien'] = $this->form_builder->inputHidden("txtIdPasien" , $detail_pasien['txtIdPasien']);
		$dt['frm_id_rekam_medis'] = $this->form_builder->inputHidden("txtIdRekamMedis" , $idRekamMedis);
		$dt['frm_id_pelayanan'] = $this->form_builder->inputHidden("txtIdPelayanan" , $detail_register['intIdPelayanan']);
		$dt['frm_id_kunjungan'] = $this->form_builder->inputHidden("txtIdKunjungan" , $detail_register['txtIdKunjungan']);
		$dt['frm_txt_kunjungan'] = $this->form_builder->inputTextArea("Keterangan Kunjungan" , "txtKetKunjungan", $detail_register['txtKeteranganKunjungan'], "col-sm-3", array("readonly"=>"readonly"));
		///// Builder To View
		
		$dt['frm_tanggal'] = $this->form_builder->inputText("Tanggal Pemeriksaan" , "txtTanggalKunjungan", indonesian_date($detail_register['dtTanggalKunjungan']) , "col-sm-3" , array("readonly"=>"readonly"));
		$dt['frm_no_anggota_pasien'] = $this->form_builder->inputText("No Anggota" , "txtNoAnggotaPasien", $detail_pasien['txtNoAnggota'] , "col-sm-3" , array("readonly"=>"readonly"));
		$dt['frm_nama_pasien'] = $this->form_builder->inputText("Nama Pengguna Layanan" , "txtNamaPasien", $detail_pasien['txtNamaPasien'] , "col-sm-3" , array("readonly"=>"readonly"));
		$dt['frm_usia_pasien'] = $this->form_builder->inputText("Usia Pengguna Layanan" , "txtUsiaPasien", $detail_register['txtUsiaPasienKunjungan'] , "col-sm-3" , array("readonly"=>"readonly"));
		$dt['frm_jaminan_kesehatan'] = $this->form_builder->inputText("Jaminan Kesehatan" , "txtJamkes", $jamkes_list[$detail_register['intIdJaminanKesehatan']] , "col-sm-3" , array("readonly"=>"readonly"));
		$dt['frm_no_jaminan_kesehatan'] = $this->form_builder->inputText("No Jaminan Kesehatan" , "txtNoJamkes", $detail_register['txtNoJaminanKesehatan'] , "col-sm-3" , array("readonly"=>"readonly"));
		$dt['frm_no_rekam_medis'] = $this->form_builder->inputText("No Rekam Medis" , "txtNoRekamMedis", $noRekamMedis, "col-sm-3" );
		
		$form_rekmed = $this->_build_poli_ibu($detail_pasien,$detail_register);
		$breadcrumbs = array(
                        "Home" =>$this->base_url_site,
                        $nama_poli => $this->base_url,
                        "Data Pelayanan ".$nama_poli => "#",
        );
		
		
        $dt['breadcrumbs'] = $this->setBreadcrumbs($breadcrumbs);
		$dt['frm_rekmed_poli'] = $form_rekmed;
		
		$ret = $this->load->view("pelayanan/form" , $dt,true);
        return $ret;  
	}

	//// Pelayanan Form Kehamilan
	private function _build_poli_ibu($detail_pasien , $detail_register){
		$dt = array();
		/// Indeks Pemeriksaan
		$this->form_builder->form_type = "inline";
		$dt['intIdRekamMedisIbu'] = $this->form_builder->inputHidden("intIdRekamMedisIbu" , "");
		$dt['txtTanggalPemeriksaan'] = $this->form_builder->inputText("Tanggal Pemeriksaan" , "txtTanggalPemeriksaan" , date("Y-m-d") , "col-sm-4" );
		$dt['txtKehamilan'] = $this->form_builder->inputText("Kehamilan Ke" , "txtKehamilanKe" , "" , "col-sm-4" );
		$dt['txtNoIndeks'] = $this->form_builder->inputText("No Indeks / Kode" , "txtNoIndeks" , "" , "col-sm-4" );
		$dt['txtStatusPemeriksaan'] = $this->form_builder->inputDropdown("Status Pemeriksaan" , "bitStatusPeriksa" , "" , $this->config->item("status_list") , array() , "col-sm-4");
		$ret = $this->load->view("pelayanan/poli_ibu/form" , $dt , true);
		return $ret;
	}
	
    private function _pelayanan($id_pelayanan){
        $dt = array();	
        $detail_pelayanan = $this->pelayanan_model->getDetail($id_pelayanan);
		$intIdJenisPelayanan = $detail_pelayanan['intIdJenisPelayanan'];
		if($intIdJenisPelayanan==7){
			redirect($this->base_url_site."ugd/".$id_pelayanan);
		}
		$menuKey = $detail_pelayanan['txtSlug'];
		$this->menu_key = $menuKey;

        $nama_poli = $detail_pelayanan['txtNama'];
        $this->meta_title = $nama_poli;
        $this->meta_desc = "Fasilitas ".$nama_poli;
		$breadcrumbs = array(
                        "Home" =>$this->base_url_site,
                        "Poliklinik" => $this->base_url,
                        "Data Pelayanan ".$nama_poli => "#",
                        );
        
		$dt['breadcrumbs'] = $this->setBreadcrumbs($breadcrumbs);
		$dt['title'] = $nama_poli;
        $dt['frm_tanggal'] = $this->form_builder->inputText("Tanggal" , "txtTanggal" , date("Y-m-d") , "col-sm-2" , array("class"=> "form-control datepicker"));
        $dt['frm_id_poliumum'] = $this->form_builder->inputHidden("inputIdPoli" , $id_pelayanan);
		$ret = $this->load->view("pelayanan/content" , $dt , true);
        return $ret;  
    }
	
}