<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Rekapitulasi extends MY_Controller {

    var $meta_title = "Rekapitulasi";
    var $meta_desc = "Item Keuangan";
    var $main_title = "Item Keuangan";
    var $menu_key = "dashboard";
    var $dtBreadcrumbs = array();  

    public function __construct() {
        parent::__construct();
        $this->load->library("Excel");
        $this->load->model(
            array(
                'pelayanan_model',
                'rekapitulasi_model',
                'register_model',
                'rekam_medis_obat_m' => 'rekam_medis_obat'
            )
        );
    }

    public function index() {        
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "menu_key" => $this->menu_key,
			"akses_key" => "is_view",
            "container" => $this->_index(),
            "custom_js" => array(
                
            ),
            "custom_css" => array(),
        );  
        $this->_render("default",$dt);             
    }

    public function rekapitulasi_pengunjung(){
        $this->dtBreadcrumbs = array(
            "Home" =>base_url(),
            "Rekapitulasi" => base_url(),
            "Data Pengunjung" => "#",
        );

        $this->meta_title = "Rekapitulasi Data Pengunjung pada Pelayanan";
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "menu_key" => "rekap_pengunjung_per_poli",
			"akses_key" => "is_view",
            "container" => $this->_build_rekap_pengunjung(),
            "custom_js" => array(
                ASSETS_JS_URL."rekapitulasi/rekap_pengunjung.js"
            ),
            "custom_css" => array(),
        );  
        
        $this->_render("default",$dt);             
    }

    public function rekapitulasi_loket(){
        $this->dtBreadcrumbs = array(
            "Home" =>base_url(),
            "Rekapitulasi" => base_url(),
            "Rekap Loket" => "#",
        );

        $this->meta_title = "Data Register Loket";
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "menu_key" => "rekap_loket",
			"akses_key" => "is_view",
            "container" => $this->_build_rekap_loket(),
            "custom_js" => array(
                ASSETS_JS_URL."rekapitulasi/rekap_loket.js"
            ),
            "custom_css" => array(),
        );  
        
        $this->_render("default",$dt);             
    }

    public function rekapitulasi_jumlah_loket(){
		
        $this->dtBreadcrumbs = array(
            "Home" =>base_url(),
            "Rekapitulasi" => base_url(),
            "Rekap Loket" => "#",
        );

        $this->meta_title = "Rekap Register Loket";
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "menu_key" => "rekap_jumlah_loket",
			"akses_key" => "is_view",
            "container" => $this->_build_rekap_jumlah_loket(),
            "custom_js" => array(
                ASSETS_JS_URL."rekapitulasi/rekap_jumlah_loket.js"
            ),
            "custom_css" => array(),
        );  
        
        $this->_render("default",$dt);
    }

    private function _build_rekap_jumlah_loket(){
		
        $listPelayanan = $this->pelayanan_model->getListDataPelayanan();
        $arrPelayanan = arrayDropdown($listPelayanan , "intIdPelayanan" , "txtNama" , array("0"=>"Semua"));
        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['frmPelayanan'] = $this->form_builder->inputDropdown("Poli" , "cmbPoli" , "" , $arrPelayanan);
        $dt['title'] = $this->meta_title;
        $ret = $this->load->view("rekapitulasi/rekap_jumlah_loket" , $dt , true);
        return $ret;

    }

    private function _build_rekap_loket(){
        $listPelayanan = $this->pelayanan_model->getListDataPelayanan();
        $arrPelayanan = array("0"=>"Semua");
        foreach ($listPelayanan as $valuePelayanan) {
            # code...
            $arrPelayanan[$valuePelayanan['intIdPelayanan']] = $valuePelayanan['txtNama'];
        }

        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['frmPelayanan'] = $this->form_builder->inputDropdown("Poli" , "cmbPoli" , "" , $arrPelayanan);
        $dt['title'] = $this->meta_title;
        $ret = $this->load->view("rekapitulasi/rekap_loket" , $dt , true);
        return $ret;
    }

    private function _build_rekap_pengunjung(){

        $listPelayanan = $this->pelayanan_model->getListDataPelayanan();
        $arrPelayanan = array("0"=>"Semua");
        foreach ($listPelayanan as $valuePelayanan) {
            # code...
            $arrPelayanan[$valuePelayanan['intIdPelayanan']] = $valuePelayanan['txtNama'];
        }

        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['frmPelayanan'] = $this->form_builder->inputDropdown("Poli" , "cmbPoli" , "" , $arrPelayanan);
        $dt['title'] = $this->meta_title;
        $ret = $this->load->view("rekapitulasi/rekap_pengunjung" , $dt , true);
        return $ret;
    }


    public function rekap_jumlah_pengunjung(){
        $this->dtBreadcrumbs = array(
            "Home" =>base_url(),
            "Rekapitulasi" => base_url(),
            "Jumlah Data Pengunjung Per Poli" => "#",
        );

        $this->meta_title = "Rekapitulasi Jumlah Kunjungan";
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "menu_key" => "rekap_pengunjung_per_poli",
			"akses_key" => "is_view",
            "container" => $this->_build_rekap_pengunjung(),
            "custom_js" => array(
                ASSETS_JS_URL."rekapitulasi/rekap_pengunjung.js"
            ),
            "custom_css" => array(),
        );  
        
        $this->_render("default",$dt);             
    }

    public function rekapitulasi_jumlah_pengunjung(){
        $this->dtBreadcrumbs = array(
            "Home" =>base_url(),
            "Rekapitulasi" => base_url(),
            "Data Jumlah Pengunjung" => "#",
        );
        
        $this->meta_title = "Rekapitulasi Jumlah Kunjungan";
        $dt = array(
            "title" => "Rekapitulasi Jumlah Kunjungan",
            "description" => "Rekapitulasi Jumlah Kunjungan",
            "menu_key" => "rekap_jumlah_pengunjung",
			"akses_key" => "is_view",
            "container" => $this->_build_jumlah_pengunjung(),
            "custom_js" => array(
                ASSETS_JS_URL."rekapitulasi/rekap_jumlah_pengunjung.js"
            ),
            "custom_css" => array(),
        );  
        
        $this->_render("default",$dt);
    }

     private function _build_jumlah_pengunjung(){

        $listPelayanan = $this->pelayanan_model->getListDataPelayanan();
        $arrPelayanan = array("0"=>"Semua");
        foreach ($listPelayanan as $valuePelayanan) {
            # code...
            $arrPelayanan[$valuePelayanan['intIdPelayanan']] = $valuePelayanan['txtNama'];
        }

        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['frmPelayanan'] = $this->form_builder->inputDropdown("Poli" , "cmbPoli" , "" , $arrPelayanan);
        $dt['title'] = $this->meta_title;
        $ret = $this->load->view("rekapitulasi/rekap_jumlah_pengunjung" , $dt , true);
        return $ret;
    }

    public function rekapitulasi_pengunjung_jamkes(){
        $this->dtBreadcrumbs = array(
            "Home" =>base_url(),
            "Rekapitulasi" => base_url(),
            "Data Penggunaan Jaminan Kesehatan" => "#",
        );

        $this->meta_title = "Rekapitulasi Jaminan Kesehatan";
        $dt = array(
            "title" => "Rekapitulasi Jaminan Kesehatan",
            "description" => "Rekapitulasi Jaminan Kesehatan",
            "menu_key" => "rekap_jaminan_kesehatan",
			"akses_key" => "is_view",
            "container" => $this->_build_rekap_pengunjung_jamkes(),
            "custom_js" => array(
                ASSETS_JS_URL."rekapitulasi/rekap_jaminan_kesehatan.js"
            ),
            "custom_css" => array(),
        );  
        
        $this->_render("default",$dt);
    }

    private function _build_rekap_pengunjung_jamkes(){

        $listPelayanan = $this->pelayanan_model->getListDataPelayanan();
        $arrPelayanan = array("0"=>"Semua");
        foreach ($listPelayanan as $valuePelayanan) {
            # code...
            $arrPelayanan[$valuePelayanan['intIdPelayanan']] = $valuePelayanan['txtNama'];
        }

        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['frmPelayanan'] = $this->form_builder->inputDropdown("Poli" , "cmbPoli" , "" , $arrPelayanan);
        $dt['title'] = $this->meta_title;
        $ret = $this->load->view("rekapitulasi/rekap_jaminan_kesehatan" , $dt , true);
        return $ret;
    }

    public function rekapitulasi_data_penyakit(){
        $this->dtBreadcrumbs = array(
            "Home" =>base_url(),
            "Rekapitulasi" => base_url(),
            "Data Penyakit" => "#",
        );

        $this->meta_title = "Rekapitulasi Jumlah Pengunjung";
        $dt = array(
            "title" => "Rekapitulasi Jumlah Pengunjung",
            "description" => "Rekapitulasi Jumlah Pengunjung",
            "menu_key" => "rekap_jumlah_pengunjung",
			"akses_key" => "is_view",
            "container" => $this->_build_data_penyakit(),
            "custom_js" => array(
                ASSETS_JS_URL."rekapitulasi/rekap_data_penyakit.js"
            ),
            "custom_css" => array(),
        );  
        
        $this->_render("default",$dt);
    }

    private function _build_data_penyakit(){

        $listPelayanan = $this->pelayanan_model->getListDataPelayanan();
        $arrPelayanan = array("0"=>"Semua");
        foreach ($listPelayanan as $valuePelayanan) {
            # code...
            $arrPelayanan[$valuePelayanan['intIdPelayanan']] = $valuePelayanan['txtNama'];
        }

        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['frmPelayanan'] = $this->form_builder->inputDropdown("Poli" , "cmbPoli" , "" , $arrPelayanan);
        $dt['title'] = "Rekapitulasi Data Penyakit";
        $ret = $this->load->view("rekapitulasi/rekap_data_penyakit" , $dt , true);
        return $ret;
    }

     public function rekapitulasi_data_tindakan(){
        $this->dtBreadcrumbs = array(
            "Home" =>base_url(),
            "Rekapitulasi" => base_url(),
            "Data Tindakan" => "#",
        );

        $this->meta_title = "Rekapitulasi Data Tindakan";
        $dt = array(
            "title" => $this->meta_title,
            "description" => "Rekapitulasi Data Tindakan",
            "menu_key" => "rekap_data_tindakan",
			"akses_key" => "is_view",
            "container" => $this->_build_data_tindakan(),
            "custom_js" => array(
                ASSETS_JS_URL."rekapitulasi/rekap_data_tindakan.js"
            ),
            "custom_css" => array(),
        );  
        
        $this->_render("default",$dt);
    }

    private function _build_data_tindakan(){
        $listPelayanan = $this->pelayanan_model->getListDataPelayanan();
        $arrPelayanan = array("0"=>"Semua");
        foreach ($listPelayanan as $valuePelayanan) {
            # code...
            $arrPelayanan[$valuePelayanan['intIdPelayanan']] = $valuePelayanan['txtNama'];
        }

        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['frmPelayanan'] = $this->form_builder->inputDropdown("Poli" , "cmbPoli" , "" , $arrPelayanan);
        $dt['title'] = "Rekapitulasi Data Tindakan";
        $ret = $this->load->view("rekapitulasi/rekap_data_tindakan" , $dt , true);
        return $ret;
    }

    public function rekapitulasi_data_rujukan(){
        $this->dtBreadcrumbs = array(
            "Home" =>base_url(),
            "Rekapitulasi" => base_url(),
            "Data Rujukan" => "#",
        );

        $this->meta_title = "Rekapitulasi Jumlah Rujukan";
        $dt = array(
            "title" => $this->meta_title,
            "description" => "Rekapitulasi Jumlah Rujukan",
            "menu_key" => "rekap_data_rujukan",
			"akses_key" => "is_view",
            "container" => $this->_build_data_rujukan(),
            "custom_js" => array(
                ASSETS_JS_URL."rekapitulasi/rekap_data_rujukan.js"
            ),
            "custom_css" => array(),
        );  
        
        $this->_render("default",$dt);
    }

    private function _build_data_rujukan(){
        $listPelayanan = $this->pelayanan_model->getListDataPelayanan();
        $arrPelayanan = array("0"=>"Semua");
        foreach ($listPelayanan as $valuePelayanan) {
            # code...
            $arrPelayanan[$valuePelayanan['intIdPelayanan']] = $valuePelayanan['txtNama'];
        }

        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['frmPelayanan'] = $this->form_builder->inputDropdown("Poli" , "cmbPoli" , "" , $arrPelayanan);
        $dt['title'] = $this->meta_title;
        $ret = $this->load->view("rekapitulasi/rekap_data_rujukan" , $dt , true);
        return $ret;
    }

    public function rekapitulasi_pasien_rujukan(){
        $this->dtBreadcrumbs = array(
            "Home" =>base_url(),
            "Rekapitulasi" => base_url(),
            "Data Rujukan" => "#",
        );

        $this->meta_title = "Rekapitulasi Data Pasien Rujukan";
        $dt = array(
            "title" => $this->meta_title,
            "description" => "Rekapitulasi Data Pasien Rujukan",
            "menu_key" => "rekap_pasien_rujukan",
			"akses_key" => "is_view",
            "container" => $this->_build_pasien_rujukan(),
            "custom_js" => array(
                ASSETS_JS_URL."rekapitulasi/rekap_pasien_rujukan.js"
            ),
            "custom_css" => array(),
        );  
        
        $this->_render("default",$dt);
    }

    private function _build_pasien_rujukan(){
        
        $arrPelayanan = array("0"=>"Semua");
        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['title'] = $this->meta_title;
        $ret = $this->load->view("rekapitulasi/rekap_pasien_rujukan" , $dt , true);
        return $ret;
    }

     public function rekapitulasi_ewars(){
        $this->dtBreadcrumbs = array(
            "Home" =>base_url(),
            "Rekapitulasi" => base_url(),
            "Data Penyakit Menular Tertentu" => "#",
        );
        
        $this->meta_title = "Rekapitulasi Data Penyakit Tertentu";
        $dt = array(
            "title" => "Rekapitulasi Data Penyakit Tertentu",
            "description" => "Rekapitulasi Data Penyakit Tertentu",
            "menu_key" => "rekap_data_penyakit_tertentu",
			"akses_key" => "is_view",
            "container" => $this->_build_rekap_ewars(),
            "custom_js" => array(
                ASSETS_URL."plugins/select2/select2.js",
                ASSETS_JS_URL."rekapitulasi/rekap_ewars.js"
            ),
            "custom_css" => array(
                ASSETS_URL."plugins/select2/select2.css",
            ),
        );  
        
        $this->_render("default",$dt);
    }

    private function _build_rekap_ewars(){
        $listPelayanan = $this->pelayanan_model->getListDataPelayanan();
        $arrPelayanan = array("0"=>"Semua");
        foreach ($listPelayanan as $valuePelayanan) {
            # code...
            $arrPelayanan[$valuePelayanan['intIdPelayanan']] = $valuePelayanan['txtNama'];
        }

        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['frmPelayanan'] = $this->form_builder->inputDropdown("Poli" , "cmbPoli" , "" , $arrPelayanan);
        $dt['title'] = $this->meta_title;
        $arrPenyakit = array();
        $dt['frmPenyakit'] = $this->form_builder->inputDropdown("Penyakit" , "cmbPenyakit" , "" , $arrPenyakit , array("multiple"=>""));
        $ret = $this->load->view("rekapitulasi/rekap_data_penyakit_menular" , $dt , true);
        return $ret;
    }

    public function rekapitulasi_obat(){
        $this->dtBreadcrumbs = array(
            "Home" =>base_url(),
            "Rekapitulasi" => base_url(),
            "Data Penyakit Menular Tertentu" => "#",
        );
        
        $this->meta_title = "Rekapitulasi Obat";
        $dt = array(
            "title" => "Rekapitulasi Data Obat Apotik",
            "description" => "Rekapitulasi Data Obat Apotik",
            "menu_key" => "rekap_data_obat_apotik",
			"akses_key" => "is_view",
            "container" => $this->_build_rekap_apotik(),
            "custom_js" => array(
                ASSETS_URL."plugins/select2/select2.js",
                ASSETS_JS_URL."rekapitulasi/rekap_obat.js"
            ),
            "custom_css" => array(
                ASSETS_URL."plugins/select2/select2.css",
            ),
        );  
        
        $this->_render("default",$dt);
    }

    private function _build_rekap_apotik(){
        $dt = array();
        $arrApotik = array(
            1 => "Rawat Jalan",
            2 => "Unit Gawat Darurat / Rawat Inap",
        );
        ///$dt['listApotik'] = form_dropdown("intIdApotik" , $arrApotik , "" , 'id="intIdApotik" class="form-control"');
        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['title'] = $this->meta_title;
        $dt['listApotik'] = $this->form_builder->inputDropdown("Apotik" , "intIdApotik" , "" , $arrApotik);
        $ret = $this->load->view("rekapitulasi/rekap_obat_apotik" , $dt , true);
        return $ret;
    }

    public function getRekapitulasiObat(){
        if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";die;
        }

        $start_date =  $this->input->post("start_date");
		$end_date = $this->input->post("end_date");
        $listApotik = $this->input->post("intIdApotik");
        $length =  $this->input->post("length");
		$start = $this->input->post("start");
		$dataPemakaian = $this->rekam_medis_obat->getRekapitulasiPemakaianObat($listApotik , $start_date , $end_date , $length , $start);
        $countDataPemakaian = $this->rekam_medis_obat->getCountRekapPemakaianObat($listApotik , $start_date , $end_date);
        if($countDataPemakaian > 0) {
            $retVal['draw'] = $this->input->post('draw');
			$retVal['recordsTotal'] = $countDataPemakaian;
			$retVal['recordsFiltered'] = $countDataPemakaian;
			$retVal['data'] = array();
			foreach($dataPemakaian as $row){ 
				$retVal['data'][] = array($row['kode_obat'],
										  $row['nama_obat'],
                                          $row['jumlah'] ,
									);
			}
        }else{
            $retVal['draw'] = $this->input->post('draw');
			$retVal['recordsTotal'] = 0;
			$retVal['recordsFiltered'] = 0;
			$retVal['data'] = array();
        }
        $this->setJsonOutput($retVal);
    }

    public function getDataRujukan(){
        if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";die;
        }

		$start_date = $this->input->post("start_date");
        $end_date = $this->input->post("end_date");
        $dataRujukan = $this->register_model->getRekapRujukan($start_date , $end_date);
        $retVal['data'] = array();
        foreach($dataRujukan as $rowRujukan){
        $retVal['data'][] = array(
                                $rowRujukan['txtNama'],
                                $rowRujukan['jumlah']
                            );
        }
        $this->setJsonOutput($retVal);
    }

    public function getDataPasienRujukan(){
        if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";die;
        }

		$start_date = $this->input->post("start_date");
        $end_date = $this->input->post("end_date");
        $dataRujukan = $this->register_model->getDataPasienRujukan($start_date , $end_date);
        $retVal['data'] = array();
        foreach($dataRujukan as $rowRujukan){
        $tanggal = indonesian_date($rowRujukan['dtTanggalKunjungan']);
        $jKelamin = $rowRujukan['charJkPasien']=="L" ? "Laki-Laki" : "Perempuan";
        $retVal['data'][] = array(
                                $tanggal,
                                $rowRujukan['txtNamaPasien'],
                                $rowRujukan['txtUsiaPasienKunjungan'],
                                $jKelamin,
                                $rowRujukan['txtNamaJaminan'],
                                $rowRujukan['txtNoJaminanKesehatan'],
                                $rowRujukan['Kecamatan'],
                                $rowRujukan['NamaPelayanan'],
                                $rowRujukan['txtPoliLuar'],
                                $rowRujukan['txtnamaIndo'],
                            );
        }
        $this->setJsonOutput($retVal);
    }

    public function getDataRekapJumlahPengunjung(){
        if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";die;
        }

        $idPelayanan =  $this->input->post("cmbPoli");
		$start_date = $this->input->post("start_date");
        $end_date = $this->input->post("end_date");
        $status = false;
        $data = array();
        $dataJumlahKunjungan = $this->register_model->getJumlahKunjungan($start_date , $end_date, $idPelayanan);
        if(!empty($dataJumlahKunjungan)){
            $status = true;
            $data = $dataJumlahKunjungan;
        }
        $retVal = array();
        $retVal['status'] = $status;
        $retVal['data'] = $data;
        echo json_encode($retVal);
    }

    public function getDataRekapJaminanKesehatan(){
        if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";die;
        }

        $idPelayanan =  $this->input->post("cmbPoli");
		$start_date = $this->input->post("date1");
        $end_date = $this->input->post("date2");
        $status = false;
        $data = "";
        $dataResult = $this->register_model->getJumlahKunjunganByJaminanKesehatan($start_date , $end_date, $idPelayanan);
        if(!empty($dataResult)){
            $status = true;
            
            foreach ($dataResult as $key => $value) {
                # code...
                $data .= "<tr>";
                $data .= "<td>".$value['NamaJaminan']."</td>";
                $data .= "<td>".$value['jumlah']."</td>";
                $data .= "</tr>";
            }
        }else{
                $data .= "<tr>";
                $data .= "<td colspan='2' class='text-center'>Data Tidak Ada</td>";
                $data .= "</tr>";
        }
        $retVal = array();
        $retVal['status'] = $status;
        $retVal['data'] = $data;
        echo json_encode($retVal);
    }

    public function getDataRekapLoket(){
        if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";die;
        }

        

        $idPelayanan =  $this->input->post("id_pelayanan");
		$start_date = $this->input->post("start_date");
        $end_date = $this->input->post("end_date");
        $length =  $this->input->post("length");
		$start = $this->input->post("start");

        $dataPengunjung = $this->rekapitulasi_model->getDataRekapLoket($idPelayanan , $start_date , $end_date , $start , $length);
        $countDataPengunjung = $this->rekapitulasi_model->getCountDataRekapLoket($idPelayanan , $start_date , $end_date );

        $retVal['draw'] = $this->input->post('draw');
        $retVal['recordsTotal'] = 0;
        $retVal['recordsFiltered'] = 0;
        $retVal['data'] = array();

        if(!empty($countDataPengunjung)){
            $retVal['draw'] = $this->input->post('draw');
			$retVal['recordsTotal'] = $countDataPengunjung;
			$retVal['recordsFiltered'] = $countDataPengunjung;
			$retVal['data'] = array();
			foreach($dataPengunjung as $row){
                $jkBaru = $row['bitIsBaru']=="1" ? 'V' : "";
                $jkLama = $row['bitIsBaru']=="0" ? 'V' : "";
                $lkMuda = (($row['charJkPasien']=="L") && ($row['intUsiaPasienHari'] <= 20075)) ? $row['txtUsiaPasienKunjungan'] : "";
                $lkTua = (($row['charJkPasien']=="L") && ($row['intUsiaPasienHari'] >= 20075)) ? $row['txtUsiaPasienKunjungan'] : "";;
                $prMuda = (($row['charJkPasien']=="P") && ($row['intUsiaPasienHari'] <= 20075)) ? $row['txtUsiaPasienKunjungan'] : "";
                $prTua = (($row['charJkPasien']=="P") && ($row['intUsiaPasienHari'] >= 20075)) ? $row['txtUsiaPasienKunjungan'] : "";;
				$retVal['data'][] = array($row['txtNoAnggota'],
										  $row['txtNamaPasien'],
                                          $jkBaru,
                                          $jkLama,
                                          $lkMuda,
                                          $lkTua,
                                          $prMuda,
                                          $prTua,
                                          $row['NamaKelurahan'],
                                          $row['txtJaminanKesehatan'],
                                          $row['txtNamaPelayanan'],
                                          $row['txtKeteranganKunjungan'],
									);
			}
        }

        $this->setJsonOutput($retVal);
    }

    public function getDataJumlahLoket(){
        $this->isAjaxRequest();
        $idPelayanan =  $this->input->post("id_pelayanan");
		$start_date = $this->input->post("start_date");
        $end_date = $this->input->post("end_date");

        $dataJumlahLoket = $this->rekapitulasi_model->getJumlahRekapLoket($idPelayanan , $start_date , $end_date);
        $retVal['data'] = array();
        if(!empty($dataJumlahLoket)){   
			foreach($dataJumlahLoket as $row){
				$retVal['data'][] = array($row['txtNama'],
                                          $row['jumlah_kunjungan_baru'],
                                          $row['jumlah_kunjungan_lama'],
                                          $row['jumlah_kunjungan_laki_muda'],
                                          $row['jumlah_kunjungan_laki_tua'],
                                          $row['jumlah_kunjungan_perempuan_muda'],
                                          $row['jumlah_kunjungan_perempuan_tua']
									);
			}
        }

        $this->setJsonOutput($retVal);
    }
    
    public function getDataRekapPengunjung(){
        if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";die;
        }

        $idPelayanan =  $this->input->post("id_pelayanan");
		$start_date = $this->input->post("start_date");
        $end_date = $this->input->post("end_date");
        $length =  $this->input->post("length");
		$start = $this->input->post("start");

        $dataPengunjung = $this->register_model->getRekapPengunjungRawatJalan($idPelayanan , $start_date , $end_date , $start , $length);
        $countDataPengunjung = $this->register_model->getCountRekapPengunjungRawatJalan($idPelayanan , $start_date , $end_date );
        if($countDataPengunjung > 0) {
            $retVal['draw'] = $this->input->post('draw');
			$retVal['recordsTotal'] = $countDataPengunjung;
			$retVal['recordsFiltered'] = $countDataPengunjung;
			$retVal['data'] = array();
			foreach($dataPengunjung as $row){
                $jenisKelamin = $row['charJkPasien']=="L" ? "Laki-Laki" : "Perempuan";
                $jenisKunjungan = $row['bitIsBaru']=="1" ? "Baru" : "Lama";
                $jenisKasus = $row['bitIsKasusBaru']=="1" ? "Baru" : ($row['bitIsKasusBaru']=="0" ? "Lama" : "");
                $jenisWilayah = $row['txtWilayahPasien']=="dalam" ? "Dalam" : "Luar";
                $kodeICDX = $row['txtSubCategory']!="0" ? $row['txtCategory'].".".$row['txtSubCategory'] : $row['txtCategory']; 
				$retVal['data'][] = array($row['txtNoAnggota'],
										  $row['txtNamaPasien'],
                                          $row['txtUsiaPasienKunjungan'] ,
                                          $jenisKelamin,
                                          $row['NamaKelurahan'] ,
                                          $row['txtJaminanKesehatan'] ,
                                          $jenisKunjungan ,
										  $jenisWilayah ,
                                          $jenisKasus ,
                                          $kodeICDX ,
                                          $row['txtPengobatan'] ,
                                          $row['txtKeteranganKunjungan'] ,
                                          $row['txtNamaPelayanan'] ,
									);
			}
        }else{
            $retVal['draw'] = $this->input->post('draw');
			$retVal['recordsTotal'] = 0;
			$retVal['recordsFiltered'] = 0;
			$retVal['data'] = array();
        }
        $this->setJsonOutput($retVal);
    }

    public function getRekapDataPenyakit(){
        if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";die;
        }

        $idPelayanan =  $this->input->post("id_pelayanan");
		$start_date = $this->input->post("start_date");
        $end_date = $this->input->post("end_date");
        $dataPengunjung = $this->register_model->getRekapDataPenyakit($start_date , $end_date , $idPelayanan);
        $retVal['data'] = array();
        foreach($dataPengunjung as $row){
            $kodeICDX = $row['txtSubCategory']!="0" ? $row['txtCategory'].".".$row['txtSubCategory'] : $row['txtCategory'];
            $retVal['data'][] = array(
                                    $kodeICDX,
                                    $row['txtIndonesianName'],
                                    $row['jumlah']
                                );
        }
        $this->setJsonOutput($retVal);
    }

    public function getRekapDataPenyakitEwars(){

        if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";die;
        }

        $idPelayanan =  $this->input->post("id_pelayanan");
		$start_date = $this->input->post("start_date");
        $end_date = $this->input->post("end_date");
        $listPenyakit = $this->input->post("data_penyakit");
        if(empty($listPenyakit)){
        $retVal['data'] = array();
        echo json_encode($retVal);
        die;
        }
        
        $dataPenyakit = $listPenyakit;
        $dataResult = $this->register_model->getRekapDataPenyakit($start_date , $end_date , $idPelayanan,$dataPenyakit);
        $retVal['data'] = array();
        foreach($dataResult as $row){
            $kodeICDX = $row['txtSubCategory']!="0" ? $row['txtCategory'].".".$row['txtSubCategory'] : $row['txtCategory'];
            $retVal['data'][] = array(
                                    $kodeICDX,
                                    $row['txtIndonesianName'],
                                    $row['jumlah']
                                );
        }
        $this->setJsonOutput($retVal);
    }

    public function getRekapDataTindakan(){
        if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";die;
        }

        $idPelayanan =  $this->input->post("id_pelayanan");
		$start_date = $this->input->post("start_date");
        $end_date = $this->input->post("end_date");
        $dataRows = $this->register_model->getRekapTindakan($start_date , $end_date , $idPelayanan);
        $retVal['data'] = array();
        foreach($dataRows as $row){
            ///$kodeICDX = $row['txtSubCategory']!="0" ? $row['txtCategory'].".".$row['txtSubCategory'] : $row['txtCategory'];
            $retVal['data'][] = array(
                                    $row['txtTindakan'],
                                    $row['txtDeskripsi'],
                                    $row['jumlah']
                                );
        }
        $this->setJsonOutput($retVal);
    }

    public function downloadExcel($jenis_rekapitulasi , $idPelayanan , $start_date , $end_date , $start = 0 , $length = 10,$pernyataan =""){
        $namaPoli = "Semua Poli";
        if($idPelayanan!=0){
            $dataPoli =  $this->pelayanan_model->getDetail($idPelayanan);
            $namaPoli = $dataPoli['txtNama'];
        }
        if($jenis_rekapitulasi=='rekap-pengunjung'){
            $titleRecap = "Rekapitulasi Pengunjung Rawat Jalan ".$namaPoli;
            $this->setHeaderExcel($titleRecap , $start_date , $end_date);
            $this->DownloadRekapPengunjung($idPelayanan , $start_date , $end_date , $start , $length);
            $filename = "Rekap_Pengunjung_Poli_".str_replace(" ","_",$namaPoli);
        }else if($jenis_rekapitulasi=='jumlah-pengunjung'){
            $titleRecap = "Rekapitulasi Pengunjung Rawat Jalan ".$namaPoli;
            $this->setHeaderExcel($titleRecap , $start_date , $end_date);
            $this->DownloadJumlahPengunjung($idPelayanan , $start_date , $end_date);
            $filename = "Rekap_Jumlah_Pengunjung_Poli_".str_replace(" ","_",$namaPoli);
        }else if($jenis_rekapitulasi=="rekap-jaminan-kesehatan"){
            $titleRecap = "Rekapitulasi Pengunjung Rawat Jalan ".$namaPoli;
            $this->setHeaderExcel($titleRecap , $start_date , $end_date);
            $this->DownloadRekapJamkes($idPelayanan , $start_date , $end_date);
            $filename = "Rekap_Jaminan_Kesehatan_Poli_".str_replace(" ","_",$namaPoli);
        }else if($jenis_rekapitulasi=="rekap-data-penyakit"){
            $titleRecap = "Rekapitulasi Data Penyakit ".$namaPoli;
            $this->setHeaderExcel($titleRecap , $start_date , $end_date);
            $this->DownloadRekapDataPenyakit($idPelayanan , $start_date , $end_date);
            $filename = "Rekap_Data_Penyakit_Poli_".str_replace(" ","_",$namaPoli);
        }else if($jenis_rekapitulasi=="rekap-data-tindakan"){
            $titleRecap = "Rekapitulasi Data Tindakan ".$namaPoli;
            $this->setHeaderExcel($titleRecap , $start_date , $end_date);
            $this->DownloadRekapDataTindakan($idPelayanan , $start_date , $end_date);
            $filename = "Rekap_Data_Tindakan_Poli_".str_replace(" ","_",$namaPoli);
        }else if($jenis_rekapitulasi=="rekap-data-rujukan"){
            $titleRecap = "Rekapitulasi Data Rujukan";
            $this->setHeaderExcel($titleRecap , $start_date , $end_date);
            $this->DownloadRekapDataRujukan($start_date , $end_date);
            $filename = "Rekap_Data_Rujukan";
        }else if($jenis_rekapitulasi=="rekap-data-penyakit-tertentu"){
            $titleRecap = "Rekapitulasi Data Penyakit Tertentu";
            $this->setHeaderExcel($titleRecap , $start_date , $end_date);
            $this->DownloadRekapDataPenyakitTertentu($idPelayanan,$start_date , $end_date , $start);
            $filename = "Rekap_Data_Penyakit_Tertentu";
        }else if($jenis_rekapitulasi=="rekap-obat-apotik"){
            $arrApotik = array(
                1 => "Rawat Jalan",
                2 => "Unit Gawat Darurat / Rawat Inap",
            );
            $nameApotik = $arrApotik[$idPelayanan];
            $nameApotikFile = str_replace(" " , "_",$nameApotik);
            $titleRecap = "Rekapitulasi Pemakaian Obat ".$nameApotik;
            $this->setHeaderExcel($titleRecap , $start_date , $end_date);
            $this->DownloadRekapPemakaianObat($idPelayanan,$start_date , $end_date , $start , $length);
            $filename = "Rekap_Pemakaian_Obat_".$nameApotikFile;
        }else if($jenis_rekapitulasi=="rekap-pasien-rujukan"){
            $titleRecap = "Rekapitulasi Data Pasien Rujukan";
            $this->setHeaderExcel($titleRecap , $start_date , $end_date);
            $this->DownloadRekapDataPasienRujukan($start_date , $end_date);
            $filename = "Rekap_Data_Pasien_Rujukan";
        }else if($jenis_rekapitulasi=="rekap-loket"){
            $titleRecap = "Rekapitulasi Data Loket";
            $this->setHeaderExcel($titleRecap , $start_date , $end_date);
            $this->DownloadRekapLoket($idPelayanan,$start_date , $end_date , $start , $length);
            $filename = "Rekap_Data_Loket";
        }else if($jenis_rekapitulasi=="rekap-jumlah-loket"){
            $titleRecap = "Rekapitulasi Jumlah Registrasi Loket";
            $this->setHeaderExcel($titleRecap , $start_date , $end_date);
            $this->DownloadJumlahRekapLoket($idPelayanan,$start_date , $end_date);
            $filename = "Rekap_Jumlah_Loket";
        }else if($jenis_rekapitulasi=="data-rekap-tindakan"){
            $titleRecap = "Rekapitulasi Jumlah Registrasi Loket";
            $this->setHeaderExcel($titleRecap , $start_date , $end_date);
            $this->Download($idPelayanan,$start_date , $end_date,$pernyataan);
            $filename = "Rekap_Jumlah_Loket";
        }
        $this->getOutput($filename);
    }

    private function Download($idPelayanan , $start_date , $end_date,$pernyataan){
        $this->excel->getActiveSheet()->setCellValue('A6', "Tanggal");
        $this->excel->getActiveSheet()->setCellValue('B6', "No Anggota");
        $this->excel->getActiveSheet()->setCellValue('C6', "Nama");
        $this->excel->getActiveSheet()->setCellValue('D6', "Usia");
        $this->excel->getActiveSheet()->setCellValue('E6', "Jenis Kelamin");
        $this->excel->getActiveSheet()->setCellValue('F6', "Pelayanan");
        $this->excel->getActiveSheet()->setCellValue('G6', "Tindakan");
        $this->excel->getActiveSheet()->setCellValue('H6', "Persetujuan");
        
          $indexNo = 7;
          $dataRows = $this->rekapitulasi_model->getRekapDataTindakan($start_date , $end_date , $idPelayanan,$pernyataan);
        $retVal['data'] = array();
        foreach ($dataRows as $key => $value) {
			
			$resPeriksa = $value['txtTindakan'].', ';
			$arrResult[$value['txtIdRekmedDetail']]['dtTanggalKunjungan'] = $value['dtTanggalKunjungan'];
			$arrResult[$value['txtIdRekmedDetail']]['txtNoAnggota'] = $value['txtNoAnggota'];
			$arrResult[$value['txtIdRekmedDetail']]['txtNamaPasien'] = $value['txtNamaPasien'];
		    $arrResult[$value['txtIdRekmedDetail']]['txtUsiaPasienKunjungan'] = $value['txtUsiaPasienKunjungan'];
			$arrResult[$value['txtIdRekmedDetail']]['charJkPasien'] = $value['charJkPasien'];
			$arrResult[$value['txtIdRekmedDetail']]['txtNama'] = $value['txtNama'];
			$arrResult[$value['txtIdRekmedDetail']]['tindakan'][$value['txtTindakan']] = $resPeriksa;
		    $arrResult[$value['txtIdRekmedDetail']]['bitSetuju'] = $value['bitSetuju'];
			  
        }
        
		foreach($arrResult as $row){
            $indexNo ++;
            $personAge = $row['txtUsiaPasienKunjungan'];
            $jenisKelamin = $row['charJkPasien']=="L" ? "Laki-Laki" : "Perempuan";
         
			$hasilPeriksa = $row['tindakan'];
			$htmlResult = "";
			foreach ($hasilPeriksa as $rowDetailPeriksa) {
				# code...
				$htmlResult .= $rowDetailPeriksa;
			}
			
			 $this->excel->getActiveSheet()->setCellValue('A'.$indexNo,indonesian_date($row['dtTanggalKunjungan']));
            $this->excel->getActiveSheet()->setCellValue('B'.$indexNo,$row['txtNoAnggota']);
            $this->excel->getActiveSheet()->setCellValue('C'.$indexNo,$row['txtNamaPasien']);
            $this->excel->getActiveSheet()->setCellValue('D'.$indexNo,$personAge);
            $this->excel->getActiveSheet()->setCellValue('E'.$indexNo,$jenisKelamin);
            $this->excel->getActiveSheet()->setCellValue('F'.$indexNo,$row['txtNama']);
            $this->excel->getActiveSheet()->setCellValue('G'.$indexNo,$htmlResult);
            $this->excel->getActiveSheet()->setCellValue('H'.$indexNo,$row['bitSetuju']==1?'Setuju':'Tidak Setuju');
   
			
			 
		}
    }

    private function DownloadRekapDataPenyakitTertentu($idPelayanan , $start_date , $end_date , $dataPenyakit ){
        $arrPenyakit = explode("%" , $dataPenyakit);
        
        $this->excel->getActiveSheet()->setCellValue('A6', "Kode Penyakit");
        $this->excel->getActiveSheet()->setCellValue('B6', "Penyakit");
        $this->excel->getActiveSheet()->setCellValue('C6', "Jumlah");
        $dataResult = $this->register_model->getRekapDataPenyakit($start_date , $end_date , $idPelayanan,$arrPenyakit);
        $indexNo = 7;
        foreach ($dataResult as $rows) {
            # code...
            $kodeICDX = $rows['txtSubCategory']!="0" ? $rows['txtCategory'].".".$rows['txtSubCategory'] : $rows['txtCategory'];
            $this->excel->getActiveSheet()->setCellValue('A'.($indexNo), $kodeICDX);
            $this->excel->getActiveSheet()->setCellValue('B'.($indexNo), $rows['txtIndonesianName']);
            $this->excel->getActiveSheet()->setCellValue('C'.($indexNo), $rows['jumlah']);
            $indexNo++;
        }

    }

    private function DownloadRekapPengunjung($idPelayanan , $start_date , $end_date , $start , $length){
        
        $no = 1;
        $this->excel->getActiveSheet()->setCellValue('A6', "No");
        $this->excel->getActiveSheet()->setCellValue('B6', "No Anggota");
        $this->excel->getActiveSheet()->setCellValue('C6', "Nama Pasien");
        $this->excel->getActiveSheet()->setCellValue('D6', "Usia Pasien");
        $this->excel->getActiveSheet()->setCellValue('E6', "Jenis Kelamin");
        $this->excel->getActiveSheet()->setCellValue('F6', "Alamat");
        $this->excel->getActiveSheet()->setCellValue('G6', "Jenis Pembayaran");
        $this->excel->getActiveSheet()->setCellValue('H6', "Jenis Kunjungan");
        $this->excel->getActiveSheet()->setCellValue('I6', "Jenis Wilayah");
        $this->excel->getActiveSheet()->setCellValue('J6', "Jenis Kasus");
        $this->excel->getActiveSheet()->setCellValue('K6', "Kode ICD-X");
        $this->excel->getActiveSheet()->setCellValue('L6', "Pengobatan");
        $this->excel->getActiveSheet()->setCellValue('M6', "Keterangan");
        $this->excel->getActiveSheet()->setCellValue('N6', "Nama Pelayanan");
        
        $indexNo = 6;
        $dataPengunjung = $this->register_model->getRekapPengunjungRawatJalan($idPelayanan , $start_date , $end_date , $start , $length);
        foreach ($dataPengunjung as $row) {
            # code...
            $jenisKelamin = $row['charJkPasien']=="L" ? "Laki-Laki" : "Perempuan";
            $jenisKunjungan = $row['bitIsBaru']=="1" ? "Baru" : "Lama";
            ///$jenisKasus = $row['bitIsKasusBaru']=="1" ? "Baru" : "Lama";
            $jenisKasus = $row['bitIsKasusBaru']=="1" ? "Baru" : ($row['bitIsKasusBaru']=="0" ? "Lama" : "");
            $jenisWilayah = $row['txtWilayahPasien']=="dalam" ? "Dalam" : "Luar";
            $kodeICDX = $row['txtSubCategory']!="0" ? $row['txtCategory'].".".$row['txtSubCategory'] : $row['txtCategory'];

            $this->excel->getActiveSheet()->setCellValue('A'.($indexNo + $no), $no);
            $this->excel->getActiveSheet()->setCellValue('B'.($indexNo + $no), $row['txtNoAnggota']);
            $this->excel->getActiveSheet()->setCellValue('C'.($indexNo + $no), $row['txtNamaPasien']);
            $this->excel->getActiveSheet()->setCellValue('D'.($indexNo + $no), $row['txtUsiaPasienKunjungan']);
            $this->excel->getActiveSheet()->setCellValue('E'.($indexNo + $no), $jenisKelamin);
            $this->excel->getActiveSheet()->setCellValue('F'.($indexNo + $no), $row['NamaKelurahan']);
            $this->excel->getActiveSheet()->setCellValue('G'.($indexNo + $no), $row['txtJaminanKesehatan']);
            $this->excel->getActiveSheet()->setCellValue('H'.($indexNo + $no), $jenisKunjungan);
            $this->excel->getActiveSheet()->setCellValue('I'.($indexNo + $no), $jenisWilayah);
            $this->excel->getActiveSheet()->setCellValue('J'.($indexNo + $no), $jenisKasus);
            $this->excel->getActiveSheet()->setCellValue('K'.($indexNo + $no), $kodeICDX);
            $this->excel->getActiveSheet()->setCellValue('L'.($indexNo + $no), $row['txtPengobatan']);
            $this->excel->getActiveSheet()->setCellValue('M'.($indexNo + $no), $row['txtKeteranganKunjungan']);
            $this->excel->getActiveSheet()->setCellValue('N'.($indexNo + $no), $row['txtNamaPelayanan']);
            $no++;
        }

    }

    private function DownloadRekapLoket($idPelayanan , $start_date , $end_date , $start , $length){

        $no = 1;
        $this->excel->getActiveSheet()->setCellValue('A6', "No");
        $this->excel->getActiveSheet()->mergeCells('A6:A8');
        $this->excel->getActiveSheet()->setCellValue('B6', "No Anggota");
        $this->excel->getActiveSheet()->mergeCells('B6:B8');
        $this->excel->getActiveSheet()->setCellValue('C6', "Nama Pasien");
        $this->excel->getActiveSheet()->mergeCells('C6:C8');
        $this->excel->getActiveSheet()->setCellValue('D6', "Jenis Kunjungan");
        $this->excel->getActiveSheet()->mergeCells('D6:E7');
        $this->excel->getActiveSheet()->setCellValue('D8', "Baru");
        $this->excel->getActiveSheet()->setCellValue('E8', "Lama");
        $this->excel->getActiveSheet()->setCellValue('F6', "Umur");
        $this->excel->getActiveSheet()->mergeCells('F6:I6');
        $this->excel->getActiveSheet()->setCellValue('F7', "Laki - Laki");
        $this->excel->getActiveSheet()->mergeCells('F7:G7');
        $this->excel->getActiveSheet()->setCellValue('H7', "Perempuan");
        $this->excel->getActiveSheet()->mergeCells('H7:I7');
        $this->excel->getActiveSheet()->setCellValue('F8', "0-50");
        $this->excel->getActiveSheet()->setCellValue('G8', "50 >");
        $this->excel->getActiveSheet()->setCellValue('H8', "0-50");
        $this->excel->getActiveSheet()->setCellValue('I8', "50 >");
        $this->excel->getActiveSheet()->setCellValue('J6', "Alamat");
        $this->excel->getActiveSheet()->mergeCells('J6:J8');
        $this->excel->getActiveSheet()->setCellValue('K6', "Jenis Pembayaran");
        $this->excel->getActiveSheet()->mergeCells('K6:K8');
        $this->excel->getActiveSheet()->setCellValue('L6', "Pelayanan");
        $this->excel->getActiveSheet()->mergeCells('L6:L8');
        $this->excel->getActiveSheet()->setCellValue('M6', "Keterangan");
        $this->excel->getActiveSheet()->mergeCells('M6:M8');


        $dataPengunjung = $this->rekapitulasi_model->getDataRekapLoket($idPelayanan , $start_date , $end_date , $start , $length);
        $indexNo = 8;
        foreach($dataPengunjung as $row){
            $jkBaru = $row['bitIsBaru']=="1" ? 'V' : "";
            $jkLama = $row['bitIsBaru']=="0" ? 'V' : "";
            $lkMuda = (($row['charJkPasien']=="L") && ($row['intUsiaPasienHari'] <= 20075)) ? $row['txtUsiaPasienKunjungan'] : "";
            $lkTua = (($row['charJkPasien']=="L") && ($row['intUsiaPasienHari'] >= 20075)) ? $row['txtUsiaPasienKunjungan'] : "";;
            $prMuda = (($row['charJkPasien']=="P") && ($row['intUsiaPasienHari'] <= 20075)) ? $row['txtUsiaPasienKunjungan'] : "";
            $prTua = (($row['charJkPasien']=="P") && ($row['intUsiaPasienHari'] >= 20075)) ? $row['txtUsiaPasienKunjungan'] : "";;

            $this->excel->getActiveSheet()->setCellValue('A'.($indexNo + $no), $no);
            $this->excel->getActiveSheet()->setCellValue('B'.($indexNo + $no), $row['txtNoAnggota']);
            $this->excel->getActiveSheet()->setCellValue('C'.($indexNo + $no), $row['txtNamaPasien']);
            $this->excel->getActiveSheet()->setCellValue('D'.($indexNo + $no), $jkBaru);
            $this->excel->getActiveSheet()->setCellValue('E'.($indexNo + $no), $jkLama);
            $this->excel->getActiveSheet()->setCellValue('F'.($indexNo + $no), $lkMuda);
            $this->excel->getActiveSheet()->setCellValue('G'.($indexNo + $no), $lkTua);
            $this->excel->getActiveSheet()->setCellValue('H'.($indexNo + $no), $prMuda);
            $this->excel->getActiveSheet()->setCellValue('I'.($indexNo + $no), $prTua);
            $this->excel->getActiveSheet()->setCellValue('J'.($indexNo + $no), $row['NamaKelurahan']);
            $this->excel->getActiveSheet()->setCellValue('K'.($indexNo + $no), $row['txtJaminanKesehatan']);
            $this->excel->getActiveSheet()->setCellValue('L'.($indexNo + $no), $row['txtNamaPelayanan']);
            $this->excel->getActiveSheet()->setCellValue('M'.($indexNo + $no), $row['txtKeteranganKunjungan']);
            $no++;
        }
    }

     private function DownloadJumlahRekapLoket($idPelayanan , $start_date , $end_date){

        $no = 1;
        $this->excel->getActiveSheet()->setCellValue('A6', "No");
        $this->excel->getActiveSheet()->mergeCells('A6:A8');
        $this->excel->getActiveSheet()->setCellValue('B6', "Pelayanan");
        $this->excel->getActiveSheet()->mergeCells('B6:B8');
        $this->excel->getActiveSheet()->setCellValue('C6', "Jenis Kunjungan");
        $this->excel->getActiveSheet()->mergeCells('C6:D7');
        $this->excel->getActiveSheet()->setCellValue('C8', "Baru");
        $this->excel->getActiveSheet()->setCellValue('D8', "Lama");
        $this->excel->getActiveSheet()->setCellValue('E6', "Umur");
        $this->excel->getActiveSheet()->mergeCells('E6:H6');
        $this->excel->getActiveSheet()->setCellValue('E7', "Laki - Laki");
        $this->excel->getActiveSheet()->mergeCells('E7:F7');
        $this->excel->getActiveSheet()->setCellValue('G7', "Perempuan");
        $this->excel->getActiveSheet()->mergeCells('G7:H7');
        $this->excel->getActiveSheet()->setCellValue('E8', "0-50");
        $this->excel->getActiveSheet()->setCellValue('F8', "50 >");
        $this->excel->getActiveSheet()->setCellValue('G8', "0-50");
        $this->excel->getActiveSheet()->setCellValue('H8', "50 >");


        $dataJumlahLoket = $this->rekapitulasi_model->getJumlahRekapLoket($idPelayanan , $start_date , $end_date);
        $indexNo = 8;
        foreach($dataJumlahLoket as $row){
            $this->excel->getActiveSheet()->setCellValue('A'.($indexNo + $no), $no);
            $this->excel->getActiveSheet()->setCellValue('B'.($indexNo + $no), $row['txtNama']);
            $this->excel->getActiveSheet()->setCellValue('C'.($indexNo + $no), $row['jumlah_kunjungan_baru']);
            $this->excel->getActiveSheet()->setCellValue('D'.($indexNo + $no), $row['jumlah_kunjungan_lama']);
            $this->excel->getActiveSheet()->setCellValue('E'.($indexNo + $no), $row['jumlah_kunjungan_laki_muda']);
            $this->excel->getActiveSheet()->setCellValue('F'.($indexNo + $no), $row['jumlah_kunjungan_laki_tua']);
            $this->excel->getActiveSheet()->setCellValue('G'.($indexNo + $no), $row['jumlah_kunjungan_perempuan_muda']);
            $this->excel->getActiveSheet()->setCellValue('H'.($indexNo + $no), $row['jumlah_kunjungan_perempuan_tua']);
            $no++;
        }
    }
    

    private function DownloadJumlahPengunjung($idPelayanan , $start_date , $end_date){
        $no = 1;
        
        $this->excel->getActiveSheet()->setCellValue('A6', "Nama Poli");
        $this->excel->getActiveSheet()->setCellValue('B6', "Jumlah Kedatangan");
        $this->excel->getActiveSheet()->setCellValue('C6', "Status");
        $this->excel->getActiveSheet()->setCellValue('C7', "Antrian");
        $this->excel->getActiveSheet()->setCellValue('D7', "Sedang Di Layani");
        $this->excel->getActiveSheet()->setCellValue('E7', "Selesai Di Layani");
        $this->excel->getActiveSheet()->setCellValue('F7', "Rujukan");

        $dataJumlahKunjungan = $this->register_model->getJumlahKunjungan($start_date , $end_date, $idPelayanan);
        $indexNo = 7;
        foreach ($dataJumlahKunjungan as $rows) {
            # code...
            $this->excel->getActiveSheet()->setCellValue('A'.($indexNo + $no), $rows['NAMA_PELAYANAN']);
            $this->excel->getActiveSheet()->setCellValue('B'.($indexNo + $no), $rows['jumlah']);
            $this->excel->getActiveSheet()->setCellValue('C'.($indexNo + $no), $rows['jumlah_antri']);
            $this->excel->getActiveSheet()->setCellValue('D'.($indexNo + $no), $rows['jumlah_sedang_dilayani']);
            $this->excel->getActiveSheet()->setCellValue('E'.($indexNo + $no), $rows['jumlah_selesai']);
            $this->excel->getActiveSheet()->setCellValue('F'.($indexNo + $no), $rows['jumlah_rujukan']);
            $no++;
        }
    }

    private function DownloadRekapJamkes($idPelayanan , $start_date , $end_date){
        $this->excel->getActiveSheet()->setCellValue('A6', "Nama Poli");
        $this->excel->getActiveSheet()->setCellValue('B6', "Jaminan Kesehatan");
        $dataJumlahKunjungan = $this->register_model->getJumlahKunjungan($start_date , $end_date, $idPelayanan);
        $no = 1;
        $indexNo = 7;
        $dataResult = $this->register_model->getJumlahKunjunganByJaminanKesehatan($start_date , $end_date, $idPelayanan);
        foreach ($dataResult as $rows) {
            # code...
            $this->excel->getActiveSheet()->setCellValue('A'.($indexNo + $no), $rows['NamaJaminan']);
            $this->excel->getActiveSheet()->setCellValue('B'.($indexNo + $no), $rows['jumlah']);
            $no++;
        }
    }

    private function DownloadRekapDataPenyakit($idPelayanan , $start_date , $end_date){
        $this->excel->getActiveSheet()->setCellValue('A6', "Kode Penyakit");
        $this->excel->getActiveSheet()->setCellValue('B6', "Penyakit");
        $this->excel->getActiveSheet()->setCellValue('C6', "Jumlah");
        $dataResult = $this->register_model->getRekapDataPenyakit($start_date , $end_date , $idPelayanan);
        $indexNo = 7;
        foreach ($dataResult as $rows) {
            # code...
            $kodeICDX = $rows['txtSubCategory']!="0" ? $rows['txtCategory'].".".$rows['txtSubCategory'] : $rows['txtCategory'];
            $this->excel->getActiveSheet()->setCellValue('A'.($indexNo), $kodeICDX);
            $this->excel->getActiveSheet()->setCellValue('B'.($indexNo), $rows['txtIndonesianName']);
            $this->excel->getActiveSheet()->setCellValue('C'.($indexNo), $rows['jumlah']);
            $indexNo++;
        }
    }

    private function DownloadRekapDataTindakan($idPelayanan , $start_date , $end_date){
        $this->excel->getActiveSheet()->setCellValue('A6', "Tindakan");
        $this->excel->getActiveSheet()->setCellValue('B6', "Deskripsi");
        $this->excel->getActiveSheet()->setCellValue('C6', "Jumlah");
        $dataResult = $this->register_model->getRekapTindakan($start_date , $end_date , $idPelayanan);
        $indexNo = 7;
        foreach ($dataResult as $rows) {
            # code...
            $this->excel->getActiveSheet()->setCellValue('A'.($indexNo), $rows['txtTindakan']);
            $this->excel->getActiveSheet()->setCellValue('B'.($indexNo), $rows['txtDeskripsi']);
            $this->excel->getActiveSheet()->setCellValue('C'.($indexNo), $rows['jumlah']);
            $indexNo++;
        }
    }

    private function DownloadRekapDataRujukan( $start_date , $end_date){
        $this->excel->getActiveSheet()->setCellValue('A6', "Tempat Rujukan");
        $this->excel->getActiveSheet()->setCellValue('B6', "Jumlah");
        $dataResult = $this->register_model->getRekapRujukan($start_date , $end_date);
        $indexNo = 7;
        foreach ($dataResult as $rows) {
            # code...
            $this->excel->getActiveSheet()->setCellValue('A'.($indexNo), $rows['txtNama']);
            $this->excel->getActiveSheet()->setCellValue('B'.($indexNo), $rows['jumlah']);
            $indexNo++;
        }
    }


    private function DownloadRekapDataPasienRujukan( $start_date , $end_date){
        $this->excel->getActiveSheet()->setCellValue('A6', "Tanggal");
        $this->excel->getActiveSheet()->setCellValue('B6', "Nama");
        $this->excel->getActiveSheet()->setCellValue('C6', "Usia");
        $this->excel->getActiveSheet()->setCellValue('D6', "Jenis Kelamin");
        $this->excel->getActiveSheet()->setCellValue('E6', "Jaminan Kesehatan");
        $this->excel->getActiveSheet()->setCellValue('F6', "No Jamkes");
        $this->excel->getActiveSheet()->setCellValue('G6', "Kecamatan");
        $this->excel->getActiveSheet()->setCellValue('H6', "Instansi");
        $this->excel->getActiveSheet()->setCellValue('I6', "Poli");
        $this->excel->getActiveSheet()->setCellValue('J6', "Nama Penyakit");
        $dataResult = $this->register_model->getDataPasienRujukan($start_date , $end_date);
        $indexNo = 7;
        foreach ($dataResult as $rowRujukan) {
            # code...
            $tanggal = indonesian_date($rowRujukan['dtTanggalKunjungan']);
            $jKelamin = $rowRujukan['charJkPasien']=="L" ? "Laki-Laki" : "Perempuan";
            $this->excel->getActiveSheet()->setCellValue('A'.($indexNo), $tanggal);
            $this->excel->getActiveSheet()->setCellValue('B'.($indexNo), $rowRujukan['txtNamaPasien']);
            $this->excel->getActiveSheet()->setCellValue('C'.($indexNo), $rowRujukan['txtUsiaPasienKunjungan']);
            $this->excel->getActiveSheet()->setCellValue('D'.($indexNo), $jKelamin);
            $this->excel->getActiveSheet()->setCellValue('E'.($indexNo), $rowRujukan['txtNamaJaminan']);
            $this->excel->getActiveSheet()->setCellValue('F'.($indexNo), $rowRujukan['txtNoJaminanKesehatan']);
            $this->excel->getActiveSheet()->setCellValue('G'.($indexNo), $rowRujukan['Kecamatan']);
            $this->excel->getActiveSheet()->setCellValue('H'.($indexNo), $rowRujukan['NamaPelayanan']);
            $this->excel->getActiveSheet()->setCellValue('I'.($indexNo), $rowRujukan['txtPoliLuar']);
            $this->excel->getActiveSheet()->setCellValue('J'.($indexNo), $rowRujukan['txtnamaIndo']);
            $indexNo++;
        }
    }

    private function DownloadRekapPemakaianObat( $idApotik,$start_date , $end_date , $offset , $limit){
        $this->excel->getActiveSheet()->setCellValue('A6', "Kode Obat");
        $this->excel->getActiveSheet()->setCellValue('B6', "Nama Obat");
        $this->excel->getActiveSheet()->setCellValue('C6', "Jumlah");
        $dataResult = $this->rekam_medis_obat->getRekapitulasiPemakaianObat($idApotik , $start_date , $end_date , $limit , $offset);
        $indexNo = 7;
        foreach ($dataResult as $rows) {
            # code...
            $this->excel->getActiveSheet()->setCellValue('A'.($indexNo), $rows['kode_obat']);
            $this->excel->getActiveSheet()->setCellValue('B'.($indexNo), $rows['nama_obat']);
            $this->excel->getActiveSheet()->setCellValue('C'.($indexNo), $rows['jumlah']);
            $indexNo++;
        }
    }

}