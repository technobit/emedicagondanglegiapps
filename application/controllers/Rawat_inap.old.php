<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Rawat_inap extends MY_Controller {
    var $meta_title = "Rawat Inap";
    var $meta_desc = "Rawat Inap";
    var $main_title = "Rawat Inap";
    var $base_url = "";
    var $limit = "10";
    var $menu = "K01";
    var $jenis_poli = "";
    var $menu_key = "rawat_inap_pelayanan";

    public function __construct(){
        parent::__construct();
        $this->base_url = $this->base_url_site."rawat-inap/";
		$this->load->model("pelayanan_model");
        $this->load->model("register_model");
        $this->load->model("pasien_model");
        $this->load->model("jaminan_kesehatan_model" , "jaminan_kesehatan");
        $this->load->model("register_kamar_model" , "register_kamar");
        $this->load->model("rekam_medis_model" , "rekam_medis");
        $this->load->model("rekam_medis_keluarga_m" , "rekmed_keluarga");
        $this->load->model("rekam_medis_rawat_inap_m" , "rekmed_rawat_inap");
        
    }

	/// Digunakan untuk Data Pasien Yang Telah Teregistrasi
    public function index($id_pelayanan){
		$menu = "P05";
		$asset_js = ASSETS_JS_URL."rawat_inap/content.js";
		$dt = array(
			"container" => $this->_build_content( $id_pelayanan),
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "menu_key" => $this->menu_key,
			"akses_key" => "is_view",	
			"custom_js" => array(
				$asset_js,
			),
            "custom_css" => array(

			),
		);
		$this->_render("default",$dt);
    }

    private function _build_content($id_pelayanan){
        $dt = array();
        $detail_pelayanan = $this->pelayanan_model->getDetail($id_pelayanan);
		$dataKamar = $this->register_kamar->getListDataKamar();

		$arrDataKamar = array("" => "Semua");
		foreach ($dataKamar as $rowKamar) {
			# code...
			$arrDataKamar[$rowKamar['intIdKamar']] = $rowKamar['txtKamar']." - ".$rowKamar['txtKelasKamar'];
		}

        $nama_poli = $detail_pelayanan['txtNama'];
        $this->meta_title = $nama_poli;
        $this->meta_desc = "Fasilitas ".$nama_poli;
		$breadcrumbs = array(
                        "Home" =>$this->base_url_site,
                        "Rawat Inap" => $this->base_url,
                        "Data Pelayanan ".$nama_poli => "#",
                        );

		$dt['breadcrumbs'] = $this->setBreadcrumbs($breadcrumbs);
		$dt['title'] = $nama_poli;
        $dt['frm_tanggal'] = $this->form_builder->inputText("Tanggal" , "txtTanggal" , date("Y-m-d") , "col-sm-2" , array("class"=> "form-control datepicker"));
        $dt['frm_id_poliumum'] = $this->form_builder->inputHidden("inputIdPoli" , $id_pelayanan);
		$dt['frm_kelas_kamar'] = $this->form_builder->inputDropdown("Kamar" , "intIdKamar" , "" , $arrDataKamar , array() , "col-sm-2");
        $dt['frm_kelas_kamar_riwayat'] = $this->form_builder->inputDropdown("Kamar" , "intIdKamarRiwayat" , "" , $arrDataKamar , array() , "col-sm-2");
        $dt['link_add_pasien'] = $this->base_url_site."pasien/pendaftaran/rawat-inap/";
		$ret = $this->load->view("rawat_inap/content" , $dt , true);
        return $ret;
    }

    public function detail($id_kunjungan){
		$menu = "PD01";
		$asset_js = ASSETS_JS_URL."rawat_inap/form.js";
		$dt = array(
			"container" => $this->_build_detail_registration( $id_kunjungan),
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "menu_key" => $this->menu_key,
			"akses_key" => "is_insert",	
			"custom_js" => array(
				ASSETS_URL."plugins/select2/select2.js",
				ASSETS_URL."plugins/validate/jquery.validate_1.11.1.min.js",
				$asset_js
			),
            "custom_css" => array(
				ASSETS_URL."plugins/select2/select2.css",
			),
		);
		$this->_render("default",$dt);
    }

    private function _build_detail_registration($id_kunjungan){

        $detail_register = $this->register_model->getDetailRegister($id_kunjungan);
        $breadcrumbs = array(
                        "Home" =>$this->base_url_site,
                        "Rawat Inap" => $this->base_url,
                        "Detail Registrasi" => "#",
                        );
		$dt['txtIdKunjungan'] = $this->form_builder->inputHidden("txtIdKunjungan" , $id_kunjungan);
		$dt['title'] = "Detail Registrasi";
		$dt['breadcrumbs'] = $this->setBreadcrumbs($breadcrumbs);

        $mode = "insert";
        ////echopre($detail_register);die;
        $idPasien = $detail_register['txtIdPasien'];
        $detail_rekam_medis = $this->rekam_medis->getMasterByIdPasien($idPasien);
        $idRekamMedis = $detail_rekam_medis['txtIdRekamMedis'];
        $detail_pasien = $this->pasien_model->getDetail($idPasien);

        $this->form_builder->form_type = "inline";
        $personAge = personAge($detail_pasien['dtTanggalLahir']);
        $tanggalRegistrasi = explode(" " , $detail_register['dtTanggalKunjungan']);
        $tanggalMasuk = $tanggalRegistrasi[0];
        
        $dt['frm_id_pasien'] = $this->form_builder->inputHidden("txtIdPasien" , $idPasien);
        $dt['frm_id_rekam_medis'] = $this->form_builder->inputHidden("txtIdRekamMedis" , $idRekamMedis);
        $dt['frm_id_pelayanan'] = $this->form_builder->inputHidden("intIdPelayanan" , $detail_register['intIdPelayanan']);
        $dt['frm_status_pasien'] = $this->form_builder->inputHidden("txtStatusPasien" , $detail_register['bitIsPoli']);
        $dt['intIdRekamMedisRawatInap'] = $this->form_builder->inputHidden("txtIdRekmedDetail" , ""); 
        $dt['intIdKeluarga'] = $this->form_builder->inputHidden("intIdKeluarga" , "");

        ////  Fillable Form
        $dt['frm_nama_pasien'] = $this->form_builder->inputText("Nama Pasien" , "txtNamaPasien",$detail_pasien['txtNamaPasien'] , "col-sm-3" , array("readonly" => "readonly"));
        $dt['frm_no_rekam_medis'] = $this->form_builder->inputText("Nama Pasien" , "txtNamaPasien",$detail_pasien['txtNamaPasien'] , "col-sm-3" , array("readonly" => "readonly"));
        $dt['frm_umur_pasien'] = $this->form_builder->inputText("Umur Pasien" , "txtUmurPasien",$personAge , "col-sm-3" , array("readonly" => "readonly"));
        $dt['frm_jk_pasien'] = $this->form_builder->inputDropdown("Jenis Kelamin" , "txtJenisKelamin" , $detail_pasien['charJkPasien'] , $this->config->item("jk_list"), array("readonly" => "readonly"));
        $dt['frm_telpon_pasien'] = $this->form_builder->inputText("No Telpon" , "txtNoTelpon",$detail_pasien['txtNoHandphone'] ,"col-sm-3" , array("readonly" => "readonly"));
        $dt['frm_alamat'] = $this->form_builder->inputText("Alamat" , "txtAlamat","","col-sm-3" , array("readonly" => "readonly"));
        $dt['frm_agama'] = $this->form_builder->inputDropdown("Agama" , "txtAgama" , $detail_pasien['txtAgama'] , $this->config->item("agama_list") , array("readonly" => "readonly"));
        $dt['frm_pendidikan'] = $this->form_builder->inputText("Pendidikan" , "txtPendidikan","" );
        $dt['frm_tanggal_masuk'] = $this->form_builder->inputText("Tanggal Masuk" , "txtTanggalMasuk",$tanggalMasuk);

        /// Form Keluarga Pasien
        $dt['frm_nama_keluarga'] = $this->form_builder->inputText("Nama Keluarga" , "txtNamaKeluarga","" );
        $dt['frm_usia_keluarga'] = $this->form_builder->inputText("Umur Kerabat" , "txtUmurKeluarga","" );
        $dt['frm_jk_keluarga'] = $this->form_builder->inputDropdown("Jenis Kelamin" , "txtJenisKelaminKeluarga" , "" , $this->config->item("jk_list"));
        $dt['frm_alamat_keluarga'] = $this->form_builder->inputText("Alamat" , "txtAlamatKeluarga","" );
        $dt['frm_telpon_keluarga'] = $this->form_builder->inputText("No Telpon Kerabat" , "txtNoTelponKeluarga","" );
        $dt['frm_agama_keluarga'] = $this->form_builder->inputDropdown("Agama" , "txtAgamaKeluarga" , "" , $this->config->item("agama_list"));
        $dt['frm_pendidikan_keluarga'] = $this->form_builder->inputText("Pendidikan" , "txtPendidikanKeluarga","" );
        $dt['frm_hubungan_keluarga'] = $this->form_builder->inputText("Hubungan Status Keluarga" , "txtHubungan","" );

        /// Form Ruangan
        $this->form_builder->form_type = "horizontal";
        $disable = !empty($dataPasien['txtNoIdJaminanKesehatan']) ? "readonly='readonly'" : "disabled='disabled'";
		$jamkes_list = $this->jaminan_kesehatan->getListDataJaminan();
		$jamkes_list["0"] = "Umum / Tidak Ada";
		$dt['selectJamkes'] =  form_dropdown('selectJamkes' , $jamkes_list, $detail_pasien['intIdJaminanKesehatan'] , 'class="form-control" id="selectJamkes" onchange="setNoJaminanKesehatan()"');
		$dt['inputNoJamkes'] = form_input("inputNoJamkes" , $detail_pasien['txtNoIdJaminanKesehatan'] , "class='form-control' id='inputNoJamkes' placeholder='No Jaminan Kesehatan' ".$disable."");
		$dt['tglMasuk'] = $this->form_builder->inputText("Tanggal Masuk" , "txtTanggalMasuk",$tanggalMasuk );
		$dt['txtKeterangan'] = $this->form_builder->inputTextArea("Keterangan" , "txtKeterangan" , "");
		$arrKamar = array();
		$dataListKamar = $this->pelayanan_model->getListKamar();
		foreach ($dataListKamar as $key => $value) {
			# code...
			$arrKamar[$value['intIdKamar']] = $value['txtKamar']." - ".$value['txtKelasKamar'];
		}

		$dt['selectKamar'] = $this->form_builder->inputDropdown("Kamar" , "txtKelasKamar" , "" , $arrKamar);
		/// Others
        /// Form Pasien
        $dt['buttonLabel'] = $mode == "insert" ? "Simpan" : "Update";
		$ret = $this->load->view("rawat_inap/form_registrasi" , $dt , true);
		return $ret;
    }

    public function saveRegisterRawatInap(){
        if(!$this->input->is_ajax_request()){
            echo "Ilegal";die;
        }

        $statusSave = false;
        $message = "Data Gagal Di Simpan";
        $txtIdKunjungan = $this->input->post("txtIdKunjungan");
        $intIdKeluarga = $this->input->post("intIdKeluarga");
        $txtStatusPasien = $this->input->post("txtStatusPasien");
        $txtIdRekamMedis = $this->input->post("txtIdRekamMedis");
        $idRekmedDetail = $this->input->post("txtIdRekmedDetail");
        $intIdPelayanan = $this->input->post("intIdPelayanan");
                
        $arrInputKerabatPasien = array(
            "txtIdKunjungan" => $txtIdKunjungan,
            "txtNamaKeluarga" => $this->input->post("txtNamaKeluarga"),
            "txtUsiaKerabat" => $this->input->post("txtUmurKeluarga"),
            "charJenisKelamin" => $this->input->post("txtJenisKelaminKeluarga"),
            "txtAlamat" => $this->input->post("txtAlamatKeluarga"),
            "txtNoTelpon" => $this->input->post("txtNoTelponKeluarga"),
            "txtAgama" => $this->input->post("txtAgamaKeluarga"),
            "txtPendidikan" => $this->input->post("txtPendidikanKeluarga"),
            "txtHubunganKeluarga" => $this->input->post("txtHubungan"),
        );

        $statusKeluarga  = false;
        $retValKel = $this->rekmed_keluarga->saveData($arrInputKerabatPasien , $intIdKeluarga);
        $intIdKeluarga = $retValKel['id'];
        $statusKeluarga = $retValKel['status'];
        
        if($statusKeluarga){
            $date_input = $this->input->post("txtTanggalMasuk");        
            $arrUpdateStatusKunjungan = array(
                "intIdJaminanKesehatan" => $this->input->post("selectJamkes"),
                "txtNoJaminanKesehatan" => $this->input->post("inputNoJamkes"),
                "dtTanggalKunjungan" => $date_input." ".date("H:i:s"),
            );
            
            /// Ubah Sedang Di Rawat
            if($txtStatusPasien==1){
                $arrUpdateStatusKunjungan['bitIsPoli'] = 2;
            }

            /// Update Kunjungan Dan BPJS
            $resKunjungan = $this->register_model->update($arrUpdateStatusKunjungan , $txtIdKunjungan);
            $statusKunjungan = $resKunjungan['status'];
        }

        /// Simpan Kamar
        if($statusKunjungan){
            $arrUpdateRawatInap = array(
                "txtIdKunjungan" => $txtIdKunjungan,
                "intIdKamar" => $this->input->post("txtKelasKamar"),
            );

            $resRegisterKamar = $this->register_kamar->saveData($arrUpdateRawatInap , $txtIdKunjungan);
            $statusKamar = $resRegisterKamar['status'];
        }

        /// Simpan Rekam Medis detail
        if($statusKamar){
            $arrInsertDetailRekmed = array(
                "txtIdRekamMedis" => $txtIdRekamMedis,
                "txtIdKunjungan" => $txtIdKunjungan,
                "intIdPelayanan" => $intIdPelayanan,
                "bitStatusRekmedDetail" => 2 //// Rekam Medis Rawat Inap
            );
            if(empty($idRekmedDetail)){
				$arrInsertDetailRekmed['dtCreatedDate'] = $date_input." ".date("H:i:s");
				$arrInsertDetailRekmed['dtLastUpdate'] = $date_input." ".date("H:i:s");
				$retValDetail = $this->rekam_medis->insertDetailRekamMedis($arrInsertDetailRekmed);	
			}else{
				$arrInsertDetailRekmed['dtLastUpdate'] = $date_input." ".date("H:i:s");
				$retValDetail = $this->rekam_medis->updateDetailRekamMedis($arrInsertDetailRekmed, $idRekmedDetail);	
			}
            $idRekmedDetail = $retValDetail['id'];
            $statusSave = $retValDetail['status'];
            $messageSave = $retValDetail['error_stat'];
        }
        
        $retVal['status'] = $statusSave;
        $retVal['message'] = $messageSave;
        $retVal['id'] = $idRekmedDetail;
        $retVal['id_rekmed'] = $txtIdRekamMedis;
        echo json_encode($retVal);
    }

    public function detail_rawat_inap($intIdRekamMedis, $intIdRekamMedisDetail){
        $menu = "PD01";
		$dt = array(
			"container" => $this->_build_detail_rawat_inap($intIdRekamMedis, $intIdRekamMedisDetail),
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "menu_key" => $this->menu_key,
			"akses_key" => "is_insert",	
			"custom_js" => array(
				ASSETS_URL."plugins/select2/select2.js",
                ASSETS_URL."plugins/datetimepicker/bootstrap-datetimepicker.min.js",
				ASSETS_URL."plugins/validate/jquery.validate_1.11.1.min.js",
				ASSETS_JS_URL."rawat_inap/form_rawat_inap.js",
			),
            "custom_css" => array(
				ASSETS_URL."plugins/select2/select2.css",
                ASSETS_URL."plugins/datetimepicker/bootstrap-datetimepicker.min.css",
			),
		);
		$this->_render("default",$dt);
    }

    private function _build_detail_rawat_inap($intIdRekamMedis , $intIdRekamMedisDetail){
        $breadcrumbs = array(
                        "Home" =>$this->base_url_site,
                        "Rawat Inap" => $this->base_url,
                        "Detail Registrasi" => "#",
                        );
		$dt = array();
		$dt['title'] = "Detail Rekam Medis";
        $resDetailInfo = $this->rekam_medis->getDetailRawatInap($intIdRekamMedis , $intIdRekamMedisDetail);
        
        $dtTanggalKunjungan = explode(" ",$resDetailInfo['dtTanggalKunjungan']);
        $tanggalKunjungan = indonesian_date($dtTanggalKunjungan[0]) . " ".$dtTanggalKunjungan[1];
        $jenisKelamin = $resDetailInfo['charJkPasien']=="L" ? "Laki-Laki" : "Perempuan";
        /// Create Form Informasi
        $dt['intIdRekamMedisDetail'] = $this->form_builder->inputHidden("txtIdRekmedDetail" , $intIdRekamMedisDetail);
        $dt['txtIdRekamMedis'] = $this->form_builder->inputHidden("txtIdRekmed" , $resDetailInfo['txtIdRekamMedis']);
        $dt['txtIdKunjungan'] = $this->form_builder->inputHidden("txtIdKunjungan" , $resDetailInfo['txtIdKunjungan']);
        $dt['txtIdPasien'] = $this->form_builder->inputHidden("txtIdPasien" , $resDetailInfo['txtIdPasien']);
        
        $dt['txtNamaPasien'] = $this->form_builder->inputText("Nama Pasien","txtNamaPasien" , $resDetailInfo['txtNamaPasien'] , "col-sm-3" , array("readonly"=>"readonly"));
        $dt['txtNoRekamMedis'] = $this->form_builder->inputText("No Rekam Medis","txtNoRekamMedis" , $resDetailInfo['txtNoRekamMedis'] , "col-sm-3" , array("readonly"=>"readonly"));
        $dt['txtKamarPasien'] = $this->form_builder->inputText("Kamar Pasien","txtKamar" , $resDetailInfo['txtKamar'] , "col-sm-3" , array("readonly"=>"readonly"));
        $dt['txtTanggalMasuk'] = $this->form_builder->inputText("Tanggal Masuk","dtTanggalKunjungan" , $tanggalKunjungan , "col-sm-3" , array("readonly"=>"readonly"));

        $dt['txtUsiaPasien'] = $this->form_builder->inputText("Usia Pasien","txtUsiaPasien" , $resDetailInfo['txtUsiaPasienKunjungan'] , "col-sm-3" , array("readonly"=>"readonly"));
        $dt['txtJenisKelamin'] = $this->form_builder->inputText("Jenis Kelamin","txtJenisKelamin" , $jenisKelamin , "col-sm-3" , array("readonly"=>"readonly"));
        $dt['txtJaminanKesehatan'] =  $this->form_builder->inputText("Jaminan Kesehatan" , "txtJamkes", $resDetailInfo['txtNamaJaminan'] , "col-sm-3" , array("readonly"=>"readonly"));
        $dt['txtNoJaminanKesehatan'] = $this->form_builder->inputText("No Jaminan Kesehatan" , "txtNoJamkes", $resDetailInfo['txtNoJaminanKesehatan'] , "col-sm-3" , array("readonly"=>"readonly"));
        
        $dt['link_detail_register'] = $this->base_url."detail-register-rawat-inap/".$resDetailInfo['txtIdKunjungan']; 
        $dt['breadcrumbs'] = $this->setBreadcrumbs($breadcrumbs);
        $ret = $this->load->view("rawat_inap/form_detail" , $dt , true);
        return $ret;
    }

    public function hapusRawatInap(){
        if(!$this->input->is_ajax_request()){
			echo "Error";
			die;
		}

        $id_kunjungan = $this->input->post("idKunjungan");
        $id_rekmed_detail = $this->input->post("txtIdRekmedDetail");
        $hapusRekamMedis = $this->rekam_medis->deleteRekamMedisRawatInap($id_rekmed_detail);
        $hapusRegister = $this->register_model->deleteRegisterRawatInap($id_kunjungan);
        echo json_encode($hapusRegister);
    }

}