<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kegiatan extends MY_Controller {
    
 var $meta_title = "Data BPJS - Kegiatan";
	
	var $meta_desc = "Data BPJS - Kegiatan";
	
	var $menu_key = "bpjs_kegiatan";
	
	var $arrBreadcrumbs = array();
	
	var $base_url = "";
	
	function __construct(){
		
		
		parent::__construct();
		
		$this->base_url = $this->base_url_site . "bpjs/kegiatan/";
		
		$this->arrBreadcrumbs = array(
		"Home" => $this->base_url_site,
		"Data kegiatan" => $this->base_url,
		
		);
		
		$this->load->library(array(
		"bpjs/masterbpjs",
		"bpjs/kunjunganbpjs",
		"bpjs/kegiatanbpjs",
		));
		
		$this->load->model(
		array(
		"pelayanan_model",
		"register_bpjs_model",
		"rekam_medis_bpjs_model",
		"register_model",
		)
		);
		
		
	}
	
	
    
  
   
	function index(){
		
		$dt = array(
		"title" => $this->meta_title,
		"description" => $this->meta_desc,
		"container" => $this->_content(),
		"menu_key" => $this->menu_key,
		"akses_key" => "is_view",	
		"custom_js" => array(
		ASSETS_JS_URL."bpjs/kegiatan/index.js",
		ASSETS_JS_URL."bpjs/checkNoAnggotaBPJS.js",
		),
		);
		
		
		$this->_render("default",$dt);
		
		
	}
    
    public function form($eduID = ""){
	
	$dt = array(
		"title" =>  "Data BPJS - Peserta",
		"description" => "Data BPJS - Peserta",
		"container" => $this->_form($eduID),
		"menu_key" => "",
		"akses_key" => "is_view",	
		"custom_js" => array(
		ASSETS_JS_URL."bpjs/kegiatan/index.js",
		ASSETS_JS_URL."bpjs/checkNoAnggotaBPJS.js",
		),
		);
		
		
		$this->_render("default",$dt);
		
	}
	
	private function _content(){
		$dt = array();
		$bulan = array();
		$thun = array();
		$tahun = range("2015",date("Y"));
		foreach ($tahun as $key) {
			# code...
			$thun[$key] = $key;
		}
		$mount = $this->config->item('month');
		foreach ($mount as $key => $value) {
			# code...
			$bulan[$key] = $value;
		}
		
		$dt['breadcrumbs'] = $this->setBreadcrumbs();
		$dt['title'] = $this->meta_title;
		$dt['tahun'] = $this->form_builder->inputDropdown("Tahun","tahun" , date("Y") , $thun,array("id"=>"tahun"));
		$dt['bulan'] = $this->form_builder->inputDropdown("Bulan","bulan" , date("m") , $bulan,array("id"=>"bulan"));
		$dt['link_peserta'] = $this->base_url."form/";
		
		$ret = $this->load->view("bpjs/kegiatan/content" , $dt, true);
		return $ret;
	}
	
	private function _form($eduID = ""){
		$dt = array();
		$edu = "";
		if (!empty($eduID)) {
			# code...
			$edu = $eduID;
		}else {
			# code...
			$edu = "";
		}
		$kelompok = array();
		$jnsKelompok = $this->config->item('bpjs_jenis_kelompok');
		foreach ($jnsKelompok as $key => $value) {
			# code...
			$kelompok[$key] = $value;
		}
		$kegiatan = array();
		$jnsKegiatan = $this->config->item('bpjs_jenis_kegiatan');
		foreach ($jnsKegiatan as $key => $value) {
			# code...
			$kegiatan[$key] = $value;
		}
		 $dt['frm_tanggal'] = $this->form_builder->inputText("Tanggal Pelaksanaan" , "txtTanggal" , date("d-m-Y") , "col-sm-3" , array("class"=> "form-control datepicker"));
		 $dt['eduID'] =  $edu;
		 $dt['frm_materi'] = $this->form_builder->inputText("Materi" , "txtMateri" , "" , "col-sm-3" , array("class"=> "form-control"));
		 $dt['frm_pembicara'] = $this->form_builder->inputText("Pembicara" , "txtPembicara" , "" , "col-sm-3" , array("class"=> "form-control"));
		 $dt['frm_lokasi'] = $this->form_builder->inputText("Lokasi" , "txtLokasi" , "" , "col-sm-3" , array("class"=> "form-control"));
		 $dt['frm_biaya'] = $this->form_builder->inputText("Biaya" , "txtBiaya" , "" , "col-sm-3" , array("class"=> "form-control"));
		 $dt['frm_ket'] = $this->form_builder->inputTextArea("Keterangan" , "txtKet" , "" , "col-sm-3" , array("class"=> "form-control"));


		$dt['jenis_kelompok'] = $this->form_builder->inputDropdown("Jenis Kelompok","jenis_kelompok" , date("Y") , $kelompok,array("id"=>"jenis_kelompok"));
		$dt['club'] = $this->form_builder->inputDropdown("Club Prolanis","club" , date("Y") , "",array("id"=>"club", "style"=>"display: none;"));
		$dt['jenis_kegiatan'] = $this->form_builder->inputDropdown("Jenis Kegiatan","jenis_kegiatan" , date("m") , $kegiatan,array("id"=>"jenis_kegiatan"));
		
		$breadcrumbs =array(
		"Home" => $this->base_url_site,
		"Data Peserta Kegiatan" => $this->base_url.'form',
		
		);
		
		
		$dt['breadcrumbs'] = $this->setBreadcrumbs($breadcrumbs);
		$dt['title'] = "Data BPJS - Peserta";
		$dt['link_index'] = $this->base_url;
		$dt['link_save'] = $this->base_url.'saveData';
		$ret = $this->load->view("bpjs/kegiatan/peserta" , $dt, true);
		return $ret;
	}
	
	/// Dependency Public Function
	
	
	function getDataKegiatan(){
		$this->isAjaxRequest();
		$post = $this->input->post();
        $bulan = $post['bulan'];
        $tahun = $post['tahun'];
        $tanggal = "";
      	$bln = "";
      	$thn = "";
        if(!empty($bulan)){
        	$bln = $bulan;
        }else{
        	$bln = date("m");
        }

        if (!empty($tahun)) {
        	# code...
        	$thn = $tahun;
        }else{
        	$thn = date("Y");
        }
		
        $tanggal = $bln.'-'.$bln.'-'.$thn;
       //echopre($tanggal);
        $dataKegiatan = $this->kegiatanbpjs->getKegiatanKelompok($tanggal);
		$retVal['data'] = array();
      	$resVal = $dataKegiatan['data']['list'];
        if($dataKegiatan['status'] != ""){
            foreach($resVal as $rowKegiatan){
					$url = $this->base_url.'form/'.$rowKegiatan['eduId'];
					$btnAksi = array();
					$btnAksi[] = '<button class="btn btn-xs btn-flat btn-warning" type="button" onclick="hapusData('.$rowKegiatan['eduId'].')"><i class="fa fa-trash"  ></i> Hapus</button>';
                  	$btnAksi[] = '<a href="'.$url.'" class="btn btn-xs btn-flat btn-primary" type="button"><i class="fa fa-edit"></i> Edit</button>'; 
                   
                  	$retVal['data'][] = array(
                        $rowKegiatan['kegiatan']['nama'],
                        $rowKegiatan['tglPelayanan'],
                        $rowKegiatan['clubProl']['nama'],
                        $rowKegiatan['materi'],
                       	$btnAksi
                   );
            }
       
        }
       echo json_encode($retVal);
	}
	public function delete($id){
		
			$retVal = array();
			$resVal = $this->kegiatanbpjs->deleteKegiatan($id);
			$retVal['message'] = $resval;
		 echo json_encode($retVal);
	}
public function getPesertaKelompok(){
	$this->isAjaxRequest();
		$post = $this->input->post();
		//echopre($post);
        $ID = $post['eID'];
     	$retVal['data'] = array();
        if(!empty($ID)){
        	
			  $data = $this->kegiatanbpjs->getPesertaKegiatan($ID);
		
      	$resVal = $data['data']['list'];
        if($data['status'] != ""){
			$no = 0;
            foreach($resVal as $row){
                  	$no ++;
					$btnAksi =  array();
					
					$btnAksi[] = '<button class="btn btn-xs btn-flat btn-primary" type="button" ><i class="fa fa-edit"></i> Edit</button>';
					$btnAksi[] = '<button class="btn btn-xs btn-flat btn-warning" type="button" ><i class="fa fa-delete"></i> Hapus</button>'; 
                   	$kelamin =  $row['peserta']['sex'] == "L" ? "Laki - Laki" : "Perempuan";
                  	$birthday = $row['peserta']['tglLahir'];
					$biday = new DateTime($birthday);
					$today = new DateTime();
					$diff = $today->diff($biday);
				  	$retVal['data'][] = array(
                        $no,
						$row['peserta']['noKartu'],
                        $row['peserta']['nama'],
                        $kelamin,
                        $row['peserta']['hubunganKeluarga'],
						$row['peserta']['tglLahir'],
                       	$diff->y,
						$btnAksi
                   );
            }
       
        }
        }

        
      
       echo json_encode($retVal);
}
public function form_input(){
		
        $dt = array();
      
        $this->load->view("bpjs/kegiatan/form" , $dt);
    }
function saveData(){

        $post = $this->input->post();
        $arrPostBPJS = array(
        "eduId"=> null,
        "clubId"=> $post['club'],
        "tglPelayanan"=> $post['txtTanggal'],
        "kdKegiatan"=> $post['jenis_kegiatan'],
        "kdKelompok"=> $post['jenis_kelompok'],
        "materi"=> $post['txtMateri'],
        "pembicara"=> $post['txtPembicara'],
        "lokasi"=> $post['txtLokasi'],
        "keterangan"=> $post['txtKet'],
        "biaya"=> $post['txtBiaya']

		);

        $resAPI = $this->kegiatanbpjs->addKegiatan($arrPostBPJS);
        $resVal = array();
		$retVal['status'] = $resAPI['status'];
		$retVal['message'] = $resAPI['message'];
		$retVal['id'] = $resAPI['data']['message'];
		echo json_encode($retVal);
	   
    }


	
}
