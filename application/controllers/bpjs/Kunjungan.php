<?php 

class Kunjungan extends MY_Controller{
	
	var $meta_title = "Data BPJS - Kunjungan";
	
	var $meta_desc = "Data BPJS - Kunjungan";
	
	var $menu_key = "bpjs_kunjungan";
	
	var $arrBreadcrumbs = array();
	
	var $base_url = "";
	
	function __construct(){
		
		
		parent::__construct();
		
		$this->base_url = $this->base_url_site . "bpjs/kunjungan";
		
		$this->arrBreadcrumbs = array(
		"Home" => $this->base_url_site,
		"Data Kunjungan" => $this->base_url,
		
		);
		
		$this->load->library(array(
		"bpjs/masterbpjs",
		"bpjs/kunjunganbpjs",
		));
		
		$this->load->model(
		array(
		"pelayanan_model",
		"register_bpjs_model",
		"rekam_medis_bpjs_model",
		"register_model",
		)
		);
		
		
	}
	
	
	
	function index(){
		
		$dt = array(
		"title" => $this->meta_title,
		"description" => $this->meta_desc,
		"container" => $this->__build_index(),
		"menu_key" => $this->menu_key,
		"akses_key" => "is_view",	
		"custom_js" => array(
		ASSETS_JS_URL."bpjs/kunjungan/index.js",
		),
		);
		
		
		$this->_render("default",$dt);
		
		
	}
	
	
	function __build_index(){
		
		$dt['breadcrumbs'] = $this->setBreadcrumbs($this->arrBreadcrumbs);
		
		$dt['title'] = "Data BPJS - Peserta";
		
		$dt['link_form_register'] = $this->base_url."pendaftaran/loket/";
		
		$dt['arrPelayanan'] = arrayDropdown($this->pelayanan_model->getListDataPelayanan(),"intIdPelayanan" , "txtNama" , array("" => "-Semua-"));
		
		$dt['arrStatus'] = $this->config->item("bpjs_upload_status");

        $dt['arrStatusPulang'] = array(
            "" => "-Semua-",
        );
		
		$ret = $this->load->view("bpjs/kunjungan/content_kunjungan" , $dt , true);
		
		return $ret;
		
	}

    function getStatusPulang(){
        $this->isAjaxRequest();

        $post = $this->input->post();
        $intIdPelayanan = $post['intIdPelayanan']; 

        $isRawatInap = $intIdPelayanan==8 ? true : false;
        $listData = $this->config->item("status_pulang_rawat_jalan");
        if($isRawatInap){
            $listData = $this->config->item("status_pulang_rawat_inap");
        }
        $html = '<option value="">-Semua-</option>';
        if(!empty($intIdPelayanan)){
            foreach($listData as $statusPulang => $txtPulang){
            $html .= '<option value="'.$statusPulang.'">'.$txtPulang.'</option>';
            }
        }
        echo $html;
    }
	
	///	// 	Get Ajax Data
	function getDataKunjungan(){
		
		$this->isAjaxRequest();
		
		$post = $this->input->post();
		
        $tgl_awal = $post['tgl_awal']." 00:00:00";

        $tgl_akhir = $post['tgl_akhir']." 23:59:00";

        $intIdJenisPelayanan = $post['intIdJenisPelayanan'];

        $kdStatusPulang = $post['kdStatusPulang'];

        $bitStatusPelayanan = $post['bitStatusPelayanan'];

        $dataKunjungan = $this->rekam_medis_bpjs_model->getDataRekamMedisBPJS($tgl_awal , $tgl_akhir , $intIdJenisPelayanan , $kdStatusPulang , $bitStatusPelayanan);
		
		$retVal['data'] = array();
        
        if(!empty($dataKunjungan)){
            
            foreach($dataKunjungan as $rowKunjungan){
                   $poli = $rowKunjungan['NamaPelayanan'];
                   $txtIdKunjungan = $rowKunjungan['txtIdKunjungan'];
                   $txtIdRekmedDetail = $rowKunjungan['txtIdRekmedDetail'];
                   $jenisStatusPulang = $rowKunjungan['rawatInap']=='true' ? $this->config->item("status_pulang_rawat_inap") : $this->config->item("status_pulang_rawat_jalan");
                   $bitStatus = $rowKunjungan['statusKunjungan'];
                   $btnUpload = $bitStatus==0 ? '<button id="btn-edit-'.$txtIdKunjungan.'" class="btn btn-xs btn-flat btn-warning" type="button" onclick="uploadData(\''.$txtIdKunjungan.'\' , \''.$txtIdRekmedDetail.'\')"><i class="fa fa-upload"></i> Upload</button>' : "";
                   $btnEdit = '<button class="btn btn-xs btn-flat btn-primary" type="button" onclick="updateBPJS(\''.$txtIdKunjungan.'\')"><i class="fa fa-edit"></i> Edit</button>'; 
                   $btnAksi = $btnUpload . $btnEdit;
                   $retVal['data'][] = array(
                        indonesian_date($rowKunjungan['dtTanggalKunjungan']),
                        $rowKunjungan['txtNoAnggota'],
                        $rowKunjungan['txtNamaPasien'],
                        $rowKunjungan['txtNamaJaminan']." - ".$rowKunjungan['txtNoJaminanKesehatan'],
                        $poli,
                        $rowKunjungan['kdDiag1']." - ".$rowKunjungan['txtDiag1'],
                        $jenisStatusPulang[$rowKunjungan['kdStatusPulang']],
                        $rowKunjungan['statusKunjungan']==1 ? '<span id="span-'.$txtIdKunjungan.'" class="label label-success">Terupload</span>' : '<span id="span-'.$txtIdKunjungan.'" class="label label-danger">Belum Terupload</span>',
                        $btnAksi
                   );
            }
        }
		echo json_encode($retVal);	
	}
	
	function uploadKunjungan(){

        $this->load->library(array(
        "bpjs/pendaftaranbpjs",
		));

        $this->isAjaxRequest();
        $post = $this->input->post();

        $status = false;
		$message = "Data Gagal Di Upload - Sinkronisasi Gagal";
        
        $txtIdKunjungan = $post['txtIdKunjungan'];
        $txtIdRekamMedisDetail = $post['txtIdRekamMedisDetail'];

        /// Check Data Kunjungan
        $txtIdKunjungan = $post['txtIdKunjungan'];
		$detailPendaftaran = $this->register_bpjs_model->detail($txtIdKunjungan);
        
        $bitStatusKunjunganBPJS = $detailPendaftaran['bitStatus'];
        if($bitStatusKunjunganBPJS==0){
            $retPendaftaran = $this->pendaftaranbpjs->syncPendaftaran($txtIdKunjungan);
        }
        /// Check Data Rekam Medis Detail
        $txtIdRekamMedisDetail = $post['txtIdRekamMedisDetail'];
        $detailRekmedDetailBPJS = $this->rekam_medis_bpjs_model->detail($txtIdRekamMedisDetail);  

        $arrPostBPJS = array(
            "noKunjungan" => null,
            "noKartu" => $detailRekmedDetailBPJS['noKartu'],
            "tglDaftar" => $detailRekmedDetailBPJS['tglDaftar'],
            "keluhan" => $detailRekmedDetailBPJS['keluhan'],
            "kdSadar" => $detailRekmedDetailBPJS['kdSadar'],
            "sistole" => $detailRekmedDetailBPJS['sistole'],
            "diastole" => $detailRekmedDetailBPJS['diastole'],
            "beratBadan" => $detailRekmedDetailBPJS['beratBadan'],
            "tinggiBadan" => $detailRekmedDetailBPJS['tinggiBadan'],
            "respRate" => $detailRekmedDetailBPJS['respRate'],
            "heartRate" => $detailRekmedDetailBPJS['heartRate'],
            "terapi" => $detailRekmedDetailBPJS['terapi'],
            "kdProviderRujukLanjut" => $detailRekmedDetailBPJS['kdProviderRujukLanjut'],
            "kdStatusPulang" => $detailRekmedDetailBPJS['kdStatusPulang'],
            "tglPulang" => $detailRekmedDetailBPJS['tglPulang'],
            "kdDokter" => $detailRekmedDetailBPJS['kdDokter'],
            "kdDiag1" => $detailRekmedDetailBPJS['kdDiag1'],
            "kdDiag2" => $detailRekmedDetailBPJS['kdDiag2'],
            "kdDiag3" => $detailRekmedDetailBPJS['kdDiag3'],
            "kdPoliRujukInternal" => $detailRekmedDetailBPJS['kdPoliRujukInternal'],
            "kdPoliRujukLanjut" => $detailRekmedDetailBPJS['kdPoliRujukLanjut'],
		);

        $resAPI = $this->kunjunganbpjs->addKunjungan($arrPostBPJS);
        $statusAPI = $resAPI['status'];

        if($statusAPI){
            $noKunjungan = $statusAPI==true ? $resAPI['data']['message'] : 000;
            $arrPostBPJS['dtSendServer'] = $statusAPI==true ? date("Y-m-d H:i:s") : "";
            $arrPostBPJS['bitStatus'] = $statusAPI==true ? 1 : 0;
            $arrPostBPJS['txtMessage'] = $resAPI['message'];
            $resInsert = $this->rekam_medis_bpjs_model->insert($arrPostBPJS , $txtIdRekamMedisDetail);
            $status = $statusAPI;
            $message = "Data Berhasil Di Sinkronkan - No Urut Peserta : ".$noUrut;
        }

        
        $retVal['status'] = $status;
		$retVal['message'] = $message;
		echo json_encode($retVal);
    }
}