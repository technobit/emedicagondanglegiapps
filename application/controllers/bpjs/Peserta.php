<?php

class Peserta extends MY_Controller{
	
	
	var $base_url = "";
	
	var $meta_title = "Data BPJS - Data Peserta";
	
	var $meta_desc = "Data BPJS - Data Peserta";
	
	var $menu = "bpjs_peserta";
	
	var $rrBreadcrumbs = array();
	
	function __construct(){
		
		parent::__construct();
		
		$this->base_url = $this->base_url_site . "bpjs/peserta";
		
		$this->arrBreadcrumbs = array(
		"Home" => $this->base_url_site,
		"Data" => $this->base_url
		);
		
		$this->load->library(
        array(
            "bpjs/pesertabpjs",
            "bpjs/pendaftaranbpjs",
        )
        );
		
		$this->load->model(
		array(
		"pelayanan_model",
		"register_bpjs_model",
		"register_model",
		)
		);
		
	}
	
	
	function index(){
		
		$dt = array(
		"title" => $this->meta_title,
		"description" => $this->meta_desc,
		"menu_key" => $this->menu,
		"akses_key" => "is_view",
		"container" => $this->__build_index(),
		"custom_js" => array(
		ASSETS_JS_URL."bpjs/peserta/content_peserta.js",
		),
		);
		
		
		$this->_render("default",$dt);
		
	}
	
	
	function __build_index(){
		
		$dt = array();
		
		$dt['breadcrumbs'] = $this->setBreadcrumbs($this->arrBreadcrumbs);
		
		$dt['title'] = "Data BPJS - Peserta";
		
		$dt['link_form_register'] = $this->base_url."pendaftaran/loket/";
		
		$arrJenisKunjungan = array(
		"" => "-Semua-",
		"1" => "Kunjungan Sakit",
		"2" => "Kunjungan Sehat",
		);
		
		
		$arrServer = array(
		"" => "-Semua-",
		"1" => "Terupload",
		"0" => "Belum Terupload"
		);
		
		
		$dt['arrPelayanan'] = arrayDropdown($this->pelayanan_model->getListDataPelayanan(),"intIdPelayanan" , "txtNama" , array("" => "-Semua-"));
		
		$dt['arrJenisKunjungan'] = $arrJenisKunjungan;
		
		$dt['arrServer'] = $this->config->item("bpjs_upload_status");
		
		$ret = $this->load->view("bpjs/peserta/content_bpjs_peserta" , $dt , true);
		
		return $ret;
		
	}
	
	
	///	/ Ajax Request
	function getDataPeserta(){
		
		if(!$this->input->is_ajax_request()){
			
			echo "ilegal";
			die;
			
		}
		
		$post = $this->input->post();
		
		$txtTanggal = $post['txtTanggal'];
		
		$intIdJenisPelayanan = $post['intIdJenisPelayanan'];
		
		$kunjSakit = $post['kunjSakit'];
		
		$bitStatus = $post['bitStatus'];
		
		$dataPeserta = $this->register_bpjs_model->getPesertaBPJS($txtTanggal , $intIdJenisPelayanan , $kunjSakit , $bitStatus);
		
		$retVal['data'] = array();
		
		if(!empty($dataPeserta)){
			
			foreach($dataPeserta as $rowPeserta){
				
				$poli = $kunjSakit==2 ? $rowPeserta['txtPoliKegiatan'] : $rowPeserta['NamaPelayanan'];
				
				$statusUpload = $rowPeserta['bitStatus'];
				
				$bitStatus = $rowPeserta['bitStatus'];

                $txtIdKunjungan = $rowPeserta['txtIdKunjungan'];
				
				$btnUpload = $bitStatus==0 ? '<button id="btn-edit-'.$txtIdKunjungan.'" class="btn btn-xs btn-flat btn-warning" type="button" onclick="uploadDataPendaftaran(\''.$rowPeserta['txtIdKunjungan'].'\')"><i class="fa fa-upload"></i> Upload</button>' : "";
				
				$btnEdit = '<button class="btn btn-xs btn-flat btn-primary" type="button" onclick="updateBPJS(\''.$rowPeserta['txtIdKunjungan'].'\')"><i class="fa fa-edit"></i> Edit</button>';
				
				$btnAksi = $btnUpload . $btnEdit;
				
				$retVal['data'][] = array(
				indonesian_date($rowPeserta['dtTanggalKunjungan']),
				$rowPeserta['txtNoAnggota'],
				$rowPeserta['txtNamaPasien'],
				$rowPeserta['txtNamaJaminan']." - ".$rowPeserta['txtNoJaminanKesehatan'],
				$poli,
				$rowPeserta['bitStatus']==1 ? '<span id="span-'.$txtIdKunjungan.'" class="label label-success">Terupload</span>' : '<span id="span-'.$txtIdKunjungan.'" class="label label-danger">Belum Terupload</span>',
				$btnAksi
				);		
			}
		}
		echo json_encode($retVal);
	}
	
	
	function getPesertaByNoBPJS(){
		
		if(!$this->input->is_ajax_request()){
			
			echo "ilegal";
			die;
			
		}
		
		$post = $this->input->post();
		
		$txtNoBPJS = $this->input->post("txtNoAnggota");
		
		$response = $this->pesertabpjs->getPesertaV2($txtNoBPJS);
		
		$htmlResult = $response['status']==true ? $this->load->view("bpjsapi/check" , $response['data'] , true) : "";
		
		$response['htmlResult'] = $htmlResult;
		
		echo json_encode($response);
		
	}

	function getPesertaByNIK(){
		if(!$this->input->is_ajax_request()){
			
			echo "ilegal";
			die;
			
		}
		
		$post = $this->input->post();
		
		$txtNoBPJS = $this->input->post("txtNIK");
		
		$response = $this->pesertabpjs->getPesertaByNIK($txtNoBPJS);
		
		$htmlResult = $response['status']==true ? $this->load->view("bpjsapi/check" , $response['data'] , true) : "";
		
		$response['htmlResult'] = $htmlResult;
		
		echo json_encode($response);
	}
	
	
	function uploadPendaftaran(){
		
		if(!$this->input->is_ajax_request()){
			
			echo "ilegal";
			die;
			
		}
		

		$status = false;
		
		$message = "Data Gagal Di Upload - Sinkronisasi Gagal";
		
		$post = $this->input->post();
		
		$txtIdKunjungan = $post['txtIdKunjungan'];
		
		$detailPendaftaran = $this->register_bpjs_model->detail($txtIdKunjungan);
	
		$idPelayanan = $detailPendaftaran['intIdPelayanan'];
		
		$jamKes = $detailPendaftaran["intIdJaminanKesehatan"];
		
		$noJamkes = $detailPendaftaran["txtNoJaminanKesehatan"];
		
		$txtKeteranganKunjungan = $detailPendaftaran["txtKeteranganKunjungan"];
		
		
		$dateTimerRegister = explode(" " , $detailPendaftaran['dtTanggalKunjungan']);
		
		$dateRegister = $dateTimerRegister[0];
		
		$tglPendaftaran = reverseDate($dateRegister);
		
		$detailJenisPelayanan = $this->pelayanan_model->getDetail($idPelayanan);
		
		$idJenisPelayanan = $detailJenisPelayanan['intIdJenisPelayanan'];
		
		$txtKodeBPJS = $detailJenisPelayanan['txtKodePelayananBPJS'];
		
		$kdProvider = 000;
		
		$detailBPJS = $this->pesertabpjs->getPesertaV2($noJamkes);
		
		if($detailBPJS['status']==1){
			
			$kdProvider = $detailBPJS['data']['kdProviderPst']['kdProvider'];
			
		}
		
		$arrPostBPJS = array(
		"kdProviderPeserta" => $kdProvider,
		"tglDaftar" => $tglPendaftaran,
		"noKartu" => $noJamkes,
		"kdPoli" => $txtKodeBPJS,
		"keluhan" => !empty($txtKeteranganKunjungan) ? $txtKeteranganKunjungan : null,
		"kunjSakit" => $detailPendaftaran['kunjSakit'],
		"sistole" => $detailPendaftaran['sistole'],
		"diastole" => $detailPendaftaran['diastole'],
		"beratBadan" => $detailPendaftaran['beratBadan'],
		"tinggiBadan" => $detailPendaftaran['tinggiBadan'],
		"respRate" => $detailPendaftaran['respRate'],
		"heartRate" => $detailPendaftaran['heartRate'],
		"rujukBalik" => $detailPendaftaran['rujukBalik'],
		"rawatInap" => $detailPendaftaran['rawatInap'],
		);
		
		$resAPI = $this->pendaftaranbpjs->pendaftaranPeserta($arrPostBPJS);
		$statusAPI = $resAPI['status'];
		
		if($statusAPI){
			
            $noUrut = $resAPI['data']['message'];

			$arrPostBPJS['bitStatus'] = true;
			
			$arrPostBPJS['dtSendServer'] = date("Y-m-d H:i:s");
			
			$arrPostBPJS['noUrut'] = $resAPI['data']['message'];
			
			$resUpdate = $this->register_bpjs_model->updatePendaftaran($arrPostBPJS , $txtIdKunjungan);
            
            $status = $statusAPI;

            $message = "Data Berhasil Di Sinkronkan - No Urut Peserta : ".$noUrut;
		}
		
		$retVal = array();
		
		$retVal['status'] = $status;
		
		$retVal['message'] = $message;
		
		echo json_encode($retVal);
		
	}
	
	
	private function hapusPendaftaranBPJS($idKunjungan){
		
		$retDelete = false;
		
		$checkData = $this->register_bpjs_model->detail($idKunjungan);
		
		if(!empty($checkData)){
			
			$bitStatus = $checkData['bitStatus'];
			
			if($bitStatus==1){
				
				$noUrut = $checkData['noUrut'];
				
				$tglDaftar = $checkData['tglDaftar'];
				
				$noKartu = $checkData['noKartu'];
				
				$resDelete = $this->pendaftaranbpjs->deletePendaftaranPeserta($noKartu , $tglDaftar , $noUrut);
				
				$retDelete = $resDelete['status'];
				
			}
			
		}
		
		return $retDelete;
		
	}
	function getPesertaNoBPJS($txtNoBPJS){
		$response = $this->pesertabpjs->getPesertaV2($txtNoBPJS);
		$resVal = array();
		$retVal['status'] = $response['status'];
		$retVal['data'] = $response['data'];
		
		echo json_encode($retVal);
		
	}	
}