<?php 

class Poli extends MY_Controller{

    var $base_url = "";
    var $limit = "10";
    
    function __construct(){
        parent::__construct();
        $this->load->library("bridging_api");
    }

    function getMasterPoli($start = 0 , $limit = 10){
        $response = array();
        if(!$this->input->is_ajax_request()){
            echo "ilegal";die;
        }
        $post = $this->input->post();
        $appLink = "v1/poli/fktp/".$start."/".$limit;
        $response = $this->bridging_api->sendGetRequest($appLink);
        $htmlResult ="";
        if($response['status']==true){
            $data = $response['data'];
            if($data['count']>0){
                foreach($data['list'] as $rowPoli){
                    $htmlResult .= '<option value="'.$rowPoli['kdPoli'].'">'.$rowPoli['kdPoli'].' - '.$rowPoli['nmPoli'].'</option>';
                }
            }
            
        }
        $response['html'] = $htmlResult;
        echo json_encode($response);
    }

    function getMasterFKTL(){
        $this->isAjaxRequest();
        $where = $this->input->get("q");

        $page = !empty($_GET['page']) ? $this->input->get("page") : 1;
        $start = ($page - 1) * $this->limit;

        $jumlah = 0;
        $arrData = array();
        $retVal = array();

        $this->load->library(
            array(
                "bpjs/polibpjs"
            )
        );

        $retApi = $this->polibpjs->getFKTL($start , "100");
        $status = $retApi['status'];

        if($status){
            $data = $retApi['data'];
            $jumlah = $data['count'];
            $listData = $data['list'];
            foreach($listData as $rowData){
                $arrData[] = array(
                    "id"=> $rowData['kdPoli'],
                    "text"=> $rowData['nmPoli'],
                );
            }
        }

        $retVal['start'] = $start;
        $retVal['total_count'] = $jumlah;
        $retVal['incomplete_results'] = false;
        $retVal['items'] = $arrData;
        echo json_encode($retVal);
    }

    function getMasterProvider(){
        $this->isAjaxRequest();
        $where = $this->input->get("q");
        $page = !empty($_GET['page']) ? $_GET['page'] : 1;
        $start = ($page - 1) * $this->limit;
        
        $jumlah = 0;
        $arrData = array();
        $retVal = array();

        $this->load->library(
            array(
                "bpjs/polibpjs"
            )
        );

        $retApi = $this->polibpjs->getProvider($start , 100);
        ///echopre($retApi);
        $status = $retApi['status'];

        if($status){
            $data = $retApi['data'];
            $jumlah = $data['count'];
            $listData = $data['list'];
            foreach($listData as $rowData){
                $arrData[] = array(
                    "id"=> $rowData['kdProvider'],
                    "text"=> $rowData['nmProvider'],
                );
            }


        }
        $retVal['start'] = $start;
        $retVal['total_count'] = $jumlah;
        $retVal['incomplete_results'] = false;
        $retVal['items'] = $arrData;
        echo json_encode($retVal);
    }

        

}