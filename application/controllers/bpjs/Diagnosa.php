<?php

class Diagnosa extends MY_Controller{

    var $base_url = "";

    function __construct(){
        parent::__construct();
        $this->load->library(array(
            "bpjs/diagnosabpjs"
        ));

        $this->load->model(array(
            "data_penyakit_model",
            "diagnosabpjs_model",
        ));
    }

    function getDiagnosaTest(){

        $post = $this->input->post();
        $appLink = "v1/diagnosa/A01/0/14000";
        $response = $this->bridging_api->sendGetRequest($appLink);
    }

    function getDiagnosaUmum(){
        if(!$this->input->is_ajax_request()){
            echo "Ilegal";die;
        }

        $status = false;
        $message = "Data Diagnosa Tidak Ditemukan";
        $kodeDiagnosaBPJS = "";
        $namaDiagnosaBPJS = "";
        $htmlResVal = "";
        $jumlahDiagnosa = 0;

        $post = $this->input->post();
        
        $idDiagnosa = $post['idDiagnosa'];
        $number = $post['number'];
        $detailDiagnosa = $this->data_penyakit_model->getDetail($idDiagnosa);
        $kodeDiagnosa = $detailDiagnosa['txtCategory'];
        
        $offset = !empty($post['offset']) ? $post['offset'] : 0;
        $limit = !empty($post['limit']) ? $post['limit'] : 100;

        $diagnosaBPJS = $this->diagnosabpjs->getDiagnosa($kodeDiagnosa , $offset , $limit);
        
        $status = $diagnosaBPJS['status'];
        if($status){
            
            $dataDiagnosa = $diagnosaBPJS['data'];
            //// Cek Jumlah Dan Nama, Jika Kode Sama maka output langsung keluar sesuai dengan kode
            $jumlahDiagnosa = $dataDiagnosa['count'];
            $diagnosaList = $dataDiagnosa['list'];
            ///$kodeDiagnosa = "";
            if($jumlahDiagnosa==1){
                ////$diagnosaList = $dataDiagnosa['list'][0];
                $kodeDiagnosaBPJS = $diagnosaList[0]['kdDiag'];
                $namaDiagnosaBPJS = $diagnosaList[0]['nmDiag'];
                $message = "Diagnosa Ditemukan";
            }else if($jumlahDiagnosa > 1){
                
                $jumlahKodeDiagnosa = strlen($kodeDiagnosa);
                $kodeDiagnosa = $jumlahKodeDiagnosa > 3 ? substr($kodeDiagnosa , 0 , 3) : $kodeDiagnosa;
                $diagnosaBPJS = $this->diagnosabpjs->getDiagnosa($kodeDiagnosa , $offset , $limit);
                
                $dataDiagnosa = $diagnosaBPJS['data'];
                $jumlahDiagnosa = $dataDiagnosa['count'];
                $diagnosaList = $dataDiagnosa['list'];

                $dt['dataDiagnosa'] = $diagnosaList;
                $dt['jumlahDiagnosa'] = $jumlahDiagnosa;
                $dt['number'] = $number;
                
                $message = "Cari Diagnosa Terlebih Dahulu";
                $htmlResVal = $this->load->view("bpjsapi/get_diagnosa" , $dt , true);
            }

        }else{
            /// If Not Connected To API
            
            $dataDiagnosa = $this->diagnosabpjs_model->getDiagnosaBPJS($kodeDiagnosa);
            $jumlahDiagnosa = $this->diagnosabpjs_model->getCountDiagnosaBPJS($kodeDiagnosa);
            
            if($jumlahDiagnosa==1){
                $status = true;
                $kodeDiagnosaBPJS = $dataDiagnosa[0]['kdDiag'];
                $namaDiagnosaBPJS = $dataDiagnosa[0]['nmDiag'];
                $message = "Get Exact Name From Local Repository";
            }else{
                $kodeDiagnosa = substr($kodeDiagnosa , 0 , 3);
                $dataDiagnosa = $this->diagnosabpjs_model->getDiagnosaBPJS($kodeDiagnosa);
                $jumlahDiagnosa = $this->diagnosabpjs_model->getCountDiagnosaBPJS($kodeDiagnosa);
                $dt['dataDiagnosa'] = $dataDiagnosa;
                $dt['jumlahDiagnosa'] = $jumlahDiagnosa;
                $dt['number'] = $number;
                $status = true;/// Get From local
                $htmlResVal = $this->load->view("bpjsapi/get_diagnosa" , $dt , true);
                $message = "Get Data From Local Repository";
            }
            
        }

        $arrDiagnosaBPJS = array(
            "kdDiagnosaBPJS" => $kodeDiagnosaBPJS,
            "nmDiagnosaBPJS" => $namaDiagnosaBPJS,
        );

        $retVal = array(
            "status" => $status , 
            "message" => $message , 
            "diagnosa" => $arrDiagnosaBPJS , 
            "jumlahDiagnosa" => $jumlahDiagnosa,
            "htmlVal" => $htmlResVal
        );
        
        echo json_encode($retVal);
    }

    
}
