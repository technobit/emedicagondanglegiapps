<?php 


class Test extends MY_Controller{
    var $base_url = "";

    function __construct(){
        parent::__construct();
        $this->load->library(array(
            'bpjs/kunjunganbpjs'
        ));
    }

    function getDataRiwayat(){
        $noBPJS = "0000110466281";

        $dataRiwayat = $this->kunjunganbpjs->getRiwayatKunjungan($noBPJS);
        echopre($dataRiwayat);
    }

    function deleteKunjungan($noKunjungan){
        $deleteKunjungan = $this->kunjunganbpjs->deleteKunjungan($noKunjungan);
        echopre($deleteKunjungan);
    }
}