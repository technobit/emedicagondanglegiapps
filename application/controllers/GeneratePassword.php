<?php 
    class GeneratePassword extends MY_Controller {

        function __construct(){
            parent::__construct();
            $this->load->library('excel');
            $this->load->model("pegawai_model");
            $this->load->model("users_model");
        }

        function index(){
            $this->load->library("upload");
            $arrResult = array();
            if($this->input->post()){
            $file_path = "";
            $config['upload_path'] = ASSETS_UPLOAD_DIR;
            $config['allowed_types'] = 'xls|xlsx';
            ///echopre($config);  
            $this->upload->initialize($config);
            $resUpload = $this->upload->do_upload("fileExcel");

            $data_upload = $this->upload->data(); 
            $file_path = $data_upload['full_path'];
            $objPHPExcel = PHPExcel_IOFactory::load($file_path);
            $allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
            $rowHeader = $allDataInSheet[1];
            unset($allDataInSheet[1]);
            
            foreach($allDataInSheet as $rowSheet){
                $arrDataInsert = array(
                    "txtNoIndukPegawai"=>$rowSheet['E'],
                    "txtNamaPegawai"=>$rowSheet['B'],
                    "txtPangkatPegawai"=>$rowSheet['F'],
                    "bitStatusPegawai"=>1,
                );

                $ret = $this->pegawai_model->saveData($arrDataInsert);
                if($ret['status']){
                    $arrResult[$ret['id']] =array("Nama" => $rowSheet['B'],"Status" =>$ret['error_stat']);
                }
            }
            }
            $dt['arrResult'] = $arrResult;
            $this->load->view("other/upload_excel" , $dt);
        }

        function user(){
            $this->load->library("upload");
            $arrResult = array();
            if($this->input->post()){
            $file_path = "";
            $config['upload_path'] = ASSETS_UPLOAD_DIR;
            $config['allowed_types'] = 'xls|xlsx';
            ///echopre($config);  
            $this->upload->initialize($config);
            $resUpload = $this->upload->do_upload("fileExcel");
            $data_upload = $this->upload->data(); 
            $file_path = $data_upload['full_path'];
            $objPHPExcel = PHPExcel_IOFactory::load($file_path);
            $allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
            $rowHeader = $allDataInSheet[1];
            unset($allDataInSheet[1]);
            foreach($allDataInSheet as $rowSheet){
                
                $password_user = $rowSheet['E'];
                $password_user = "3M3dic4".$password_user."st3v3j0b5";
                $password_user = md5($password_user);
                $arrDataInsert = array(
                    "intIdPegawai"=>$rowSheet['A'],
                    "txtUserName"=>$rowSheet['D'],
                    "txtPassword"=>$password_user,
                    "intIdLevel"=>$rowSheet['F'],
                    "dtCreatedDate" => "NOW()",
                    "bitStatusUser" => 1,
                    "intIdKabupaten" => 43993,
                );
                
                if(!empty($rowSheet['A'])){
                    $ret = $this->users_model->saveData($arrDataInsert);
                    if($ret['status']){
                        $arrResult[$ret['id']] =array("Nama" => $rowSheet['B'],"Status" =>$ret['error_stat']);
                    }
                }else{
                    break;
                }
            }
            }
            $dt['arrResult'] = $arrResult;
            $this->load->view("other/upload_excel" , $dt);
        } 
    }
