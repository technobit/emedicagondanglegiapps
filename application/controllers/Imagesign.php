<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Imagesign extends MY_Controller {

    var $meta_title = "Grafik";
    var $meta_desc = "";
    var $main_title = "";
    var $menu_key = "";
    var $dtBreadcrumbs = array();  

    public function __construct() {
        parent::__construct();
        $this->load->model("pasien_model");
        
    }
    public function index() {        
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "menu_key" => $this->menu_key,
			"akses_key" => "is_view",
            "container" => $this->_index(),
            "custom_js" => array(),
            "custom_css" => array(),
        );  
        $this->_render("default",$dt);             
    }

    public function imageSignPost(){
        $postData = $this->input->post();
        $status = false;
        $message = "Data Gagal Di Simpan";
        $retVal['post_send'] = $postData;
        if(!empty($postData)){
            $status = true;
            $message = "Data Berhasil Di Simpan";
        }

        $txtIdUser = $postData['txtIDUser'];
        $txtIdGroup = $postData['txtIDGroup'];
        $strBase64Image = $postData['strBase64Image'];

        $arrData = array(
            "txtIdPasien" => $txtIdUser,
            "txtImageBase64" => $strBase64Image,
        );

        $cekDataPasienLama = $this->pasien_model->checkTTDPasien($txtIdUser);

        if($cekDataPasienLama){
            $retDelete = $this->pasien_model->deleteTTDPasien($txtIdUser);
        }

        $resData = $this->pasien_model->saveTTDPasien($arrData);

        $retVal['status']  = $resData['status'];
        $retVal['message'] = $resData['message'];
        echo json_encode($retVal);
    }

    public function checkHandsign(){
        $this->isAjaxRequest();
        $txtIdPasien = $this->input->post("txtIdPasien");
        $retHandSign = $this->pasien_model->getTTDPasienV2($txtIdPasien);
        $retVal = array();
        $status = false;

        if(!empty($retHandSign)){
            $status = true;
        }
        
        $retVal['status'] = $status;
        echo json_encode($retVal);
    }

    public function getImageSign($txtIdPasien){

        $dataPasien = $this->pasien_model->getTTDPasienV2($txtIdPasien);
        ///echopre($dataPasien);die;
        $imageBase64 = $dataPasien['txtSignatureImage'];
        ///echo $imageBase64;die;
        header('Content-Type: image/jpeg');
        $fileBase64 = base64_decode($imageBase64);
        echo $fileBase64;die;
        
    }
}