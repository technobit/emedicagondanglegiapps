<?php 

class Fingerscan extends MY_Controller{

    var $time_limit_reg = "30";
    var $time_limit_ver = "10";
    var $base_url = "";

    function __construct(){
        parent::__construct();
        $this->load->model("finger_model");
        $this->base_url = $this->base_url_site."fingerscan/";
    }

    function registerPasien($txtIdPasien){
        if(!empty($txtIdPasien)){
            echo "$txtIdPasien;SecurityKey;".$this->time_limit_reg.";".$this->base_url."process_register;".$this->base_url."get_activation_ac/";
        }
    }

    function registerPegawai($intIdPegawai){
        if(!empty($intIdPegawai)){
            echo "$intIdPegawai;SecurityKey;".$this->time_limit_reg.";".$this->base_url."insertFingerPegawai;".$this->base_url."get_activation_ac/";
        }
    }

    function registerUlangPegawai($intIdPegawai){
        if(!empty($intIdPegawai)){
            echo "$intIdPegawai;SecurityKey;".$this->time_limit_reg.";".$this->base_url."reInsertFingerPegawai;".$this->base_url."get_activation_ac/";
        }
    }

    function process_register(){
        $regTemp = $this->input->post("RegTemp");
        $data 	 = explode(";",$regTemp);

        $vStamp 	= $data[0];
		$sn 		= $data[1];
		$user_id	= $data[2];
		$regTemp 	= $data[3];

        $device = $this->finger_model->getDeviceBySN($sn);
        $salt = md5($device['ac'].$device['vkey'].$regTemp.$sn.$user_id);

        if (strtoupper($vStamp) == strtoupper($salt)) {

            $idFinger = $this->finger_model->checkNumber($user_id);
            $arrPost = array(
                "txtIdPasien"=>$user_id,
                "finger_id"=>($idFinger + 1),
                "finger_data"=>$regTemp,
            );
            $data = $this->finger_model->insertFinger($arrPost);
        }else{
            $msg = "Parameter";

        }

    }

    function insertFingerPegawai(){
        $regTemp = $this->input->post("RegTemp");
        $data 	 = explode(";",$regTemp);

        $vStamp 	= $data[0];
		$sn 		= $data[1];
		$user_id	= $data[2];
		$regTemp 	= $data[3];

        $device = $this->finger_model->getDeviceBySN($sn);
        $salt = md5($device['ac'].$device['vkey'].$regTemp.$sn.$user_id);

        if (strtoupper($vStamp) == strtoupper($salt)) {

            $arrPost = array(
                "intIDPegawai"=>$user_id,
                "txtFingerTemplate"=>$regTemp,
            );
            $data = $this->finger_model->insertFingerPegawai($arrPost);
        }else{
            $msg = "Parameter";
            echo $this->base_url;

        }
    }

    function reInsertFingerPegawai(){
        $regTemp = $this->input->post("RegTemp");
        $data 	 = explode(";",$regTemp);

        $vStamp 	= $data[0];
		$sn 		= $data[1];
		$user_id	= $data[2];
		$regTemp 	= $data[3];

        $device = $this->finger_model->getDeviceBySN($sn);
        $salt = md5($device['ac'].$device['vkey'].$regTemp.$sn.$user_id);
        if (strtoupper($vStamp) == strtoupper($salt)) {

            $arrPost = array(
                "intIDPegawai"=>$user_id,
                "txtFingerTemplate"=>$regTemp,
            );

            $data = $this->finger_model->insertFingerPegawai($arrPost , $user_id);
        }else{
            $msg = "Parameter";
            echo $this->base_url;

        }
    } 

    function get_activation_ac(){
        $ac = $this->input->get("vc");
        if(!empty($ac)){
            $device = $this->finger_model->getDeviceByVC($ac);
            echo $device['ac'].$device['sn'];
        }
    }

    function checkFingerRegistration(){
        if(!$this->input->is_ajax_request()){
            echo "Ilegal";die;   
        }
        $status = false;
        $message = "";
        $txtIdPasien = $this->input->post("txtIdPasien");
        $checkData = $this->finger_model->checkNumber($txtIdPasien);
        if($checkData > 0){
            $status = true;
            $message = "Data Berhasil Teregistrasi";
        }else{
            $status = false;
            $message = "Data Gagal Teregistrasi";
        }
        $retVal = array();
        $retVal['status'] = $status;
        $retVal['message'] = $message;
        echo json_encode($retVal);
    }

    function checkFingerRegistrationPegawai(){
        if(!$this->input->is_ajax_request()){
            echo "Ilegal";die;   
        }
        $status = false;
        $message = "";
        $intIdPegawai = $this->input->post("intIdPegawai");
        $checkData = $this->finger_model->checkNumberPegawai($intIdPegawai);
        if($checkData > 0){
            $status = true;
            $message = "Data Berhasil Teregistrasi";
        }else{
            $status = false;
            $message = "Data Gagal Teregistrasi";
        }
        $retVal = array();
        $retVal['status'] = $status;
        $retVal['message'] = $message;
        echo json_encode($retVal);
    }



    function deleteFinger(){
        if(!$this->input->is_ajax_request()){
            echo "Ilegal";die;   
        }
        $txtIdPasien = $this->input->post("txtIdPasien");
        $hapusData = $this->finger_model->hapusFinger($txtIdPasien);
        $retVal = $hapusData;
        echo json_encode($retVal);
    }

    function findPasienByUniqueID(){
        if(!$this->input->is_ajax_request()){
            echo "Ilegal";die;   
        }
        $uniqueID = $this->input->post("uniqueID");
        $data = $this->finger_model->getTxtIdPasienByID($uniqueID);

        $status = false;
        $txtIdPasien = "";
        $message = "Data Tidak Di Temukan";
        if(!empty($data)){
            $status = true;
            $txtIdPasien = $data['txtIdPasien'];
            $message = "Data Berhasil Di Temukan";
        }
        $retVal = array();
        $retVal['status'] = $status==true ? 1 : 0;
        $retVal['txtIdPasien'] = $txtIdPasien;  
        $retVal['message'] = $message;
        echo json_encode($retVal);
    }

}