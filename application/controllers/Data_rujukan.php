<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data_rujukan extends MY_Controller {
    
    var $meta_title = "Data Rujukan Luar";
    var $meta_desc = "Data Rujukan Luar";
	var $main_title = "Data Rujukan Luar";
    var $base_url = "";
	var $upload_dir = "";
	var $upload_url = "";
	var $limit = "20";
	var $arr_level_user = "";
    var $menu = "pasien_rujukan";
    var $mode = "";
	function __construct(){
        parent::__construct();
		
		$this->load->model("register_model");
        $this->load->model("PoliLuar_model" , "poli_luar");
        $this->load->model("pelayanan_model");
        $this->base_url = $this->base_url_site."data-rujukan/";
    }
    
    /// Data ICD 10
	public function index()
	{
		$dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
			"menu_key" => $this->menu,
			"akses_key" => "is_view",
			"container" => $this->_content(),
			"custom_js" => array(
				ASSETS_JS_URL."data_rujukan/index.js",
				
			),
            "custom_css" => array(
				
			),
		);	
		$this->_render("default",$dt);	
	}
    
    public function form($id=""){
		
		$dt = array(
            "title" => $this->meta_title,
			"menu_key" => $this->menu,
			"akses_key" => "is_update",
            "description" => $this->meta_desc,
			"container" => $this->_form($id),
			"custom_js" => array(
				ASSETS_URL."plugins/select2/select2.js",
				ASSETS_URL."plugins/validate/jquery.validate_1.11.1.min.js",
				ASSETS_JS_URL."data_rujukan/form.js",
			),
            "custom_css" => array(
				ASSETS_URL."plugins/select2/select2.css",
			),
		);	
		$this->_render("default",$dt);
	}
	
	private function _content(){
		$dt = array();
		$breadcrumbs = array(
				"Home" =>$this->base_url_site,
				"Pengguna Layanan" => $this->base_url,
				"Data Rujukan Luar" => "#",
        );
		$listPelayanan = $this->pelayanan_model->getListDataPelayanan();
        $arrPelayanan = array("0" => "Semua");
        foreach($listPelayanan as $rowPelayanan) {
            $arrPelayanan[$rowPelayanan['intIdPelayanan']] = $rowPelayanan['txtNama'];
        }
		$dt['breadcrumbs'] = $this->setBreadcrumbs($breadcrumbs);
		$dt['title'] = $this->meta_title;
		$dt['link_add'] = $this->base_url."tambah-data/";
        $dt['intIdPelayanan'] = $this->form_builder->inputDropdown("Pelayanan Asal" , "intIdPelayanan" , "" , $arrPelayanan , array() , "col-sm-2"); 
		$ret = $this->load->view("data_rujukan/content" , $dt, true);
		return $ret;
	}
	
	private function _form($id){
		$dt = array();
		
        $detailRujukan = $this->register_model->getDetailPasienRujukan($id);
		
        $jenisFasilitas = "RUJUKAN_LUAR";
        /// Data Pasien
		$dt['txtIdKunjungan'] = $this->form_builder->inputHidden("txtIdKunjungan" , $detailRujukan['txtIdKunjungan']);
		$dt['txtNoAnggota'] = $this->form_builder->inputText("No Anggota" , "txtNoAnggota" , $detailRujukan['txtNoAnggota'] , "col-sm-3" , array("readonly" => "readonly"));
        $dt['txtNamaPasien'] = $this->form_builder->inputText("Nama" , "txtNamaPasien" , $detailRujukan['txtNamaPasien'] , "col-sm-3" , array("readonly" => "readonly"));
        $dt['txtUsiaPasienKunjungan'] = $this->form_builder->inputText("Usia Pengguna Layanan" , "txtUsiaPasienKunjungan" , $detailRujukan['txtUsiaPasienKunjungan'] , "col-sm-3" , array("readonly" => "readonly"));
        $rujukanLuar = $this->pelayanan_model->getPelayananByJenisFasilitas($jenisFasilitas);
        $poliLuar = $this->poli_luar->getListData();
        $arrRujukanLuar = arrayDropdown($rujukanLuar , "intIdPelayanan" , "txtNama");
		$arrPoliLuar = arrayDropdown($poliLuar , "intIDPoliLuar" , "txtPoliLuar");
		$arrBitPeriksaLanjut = array(
			"3" => "Rujukan Luar",
			"4" => "Rujukan BPJS",
		);
		$dt['selectMode'] = $this->form_builder->inputRadioGroup("Mode Rujukan" , "selectModeRujukan",$detailRujukan['bitPeriksaLanjut'],$arrBitPeriksaLanjut );
        $dt['selectLayananLanjut'] = $this->form_builder->inputDropdown("Jenis Pelayanan" , "selectLayananLanjut" ,$detailRujukan['intIdPelayanan'], $arrRujukanLuar);
		$dt['poliLanjut'] = $this->form_builder->inputDropdown("Ruangan / Pelayanan" , "poliLanjut" , $detailRujukan['intIDPoliLuar'] , $arrPoliLuar );
        $dt['txtKeteranganLanjut'] = $this->form_builder->inputTextArea("Keterangan" , "txtKeteranganPemeriksaan" , $detailRujukan['txtKeteranganKunjungan'] , "col-sm-3");
		

        /// Data Diagnosa
        $dt['txtPemeriksaan'] = $this->form_builder->inputTextArea("Pemeriksaan Fisik" , "txtPemeriksaan" , $detailRujukan['txtPemeriksaan'] , "col-sm-3" , array("readonly" => "readonly"));
        $dt['txtDiagnosa'] = $this->form_builder->inputTextArea("Diagnosa Sementara" , "txtDiagnosa" , $detailRujukan['txtnamaIndo'] , "col-sm-3" , array("readonly" => "readonly"));

		$breadcrumbs = array(
			"Home" =>$this->base_url_site,
			"Master Data" => $this->base_url,
			"Detail Rujukan" => "#",
        );
		
		$dt['breadcrumbs'] = $this->setBreadcrumbs($breadcrumbs);
		$dt['title'] = "Detail Rujukan Luar";
		$dt['link_index'] = $this->base_url;
        $dt['link_edit_diagnosa'] = $this->base_url_site."pelayanan/poliklinik/detail/".$id;
		$ret = $this->load->view("data_rujukan/form" , $dt, true);
		return $ret;
	}

	/// Dependency Public Function
	public function saveData(){
		
		$txtIdKunjungan = $this->input->post("txtIdKunjungan");
		$arrInsert = array(
						 "intIdPelayanan" =>$this->input->post("selectLayananLanjut"),
						 "intIdPoliLuar"=>$this->input->post("poliLanjut"),
						 "txtKeteranganKunjungan"=>$this->input->post("txtKeteranganPemeriksaan"),
						 );
		$resVal = $this->register_model->update($arrInsert , $txtIdKunjungan);
		$retVal['status'] = $resVal['status'];
		$retVal['message'] = $resVal['error_stat'];
		$this->session->set_flashdata("alert_message",$retVal);
		redirect($this->base_url);
	}
	
	public function getDataRujukan(){
		if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";
        }
		
        $post = $this->input->post();
        $dtStart = $post['start_date'];
        $dtEnd = $post['end_date'];
        $intIdPelayanan = $post['id_pelayanan'];
        $dataRujukan = $this->register_model->getDataPasienRujukan($dtStart , $dtEnd , $intIdPelayanan);
        ///echopre($dataRujukan);
        $retVal['data'] = array();
        foreach($dataRujukan as $rowRujukan){
            $txtIdKunjungan = $rowRujukan['txtIdKunjungan'];
            $btnEdit = '<a class="btn btn-primary btn-xs btn-flat" href="'.$this->base_url.'detail/'.$txtIdKunjungan.'/"><i class="fa fa-edit"></i> Edit</a>';
            $btnBatal = '<button class="btn btn-danger btn-xs btn-flat" type="button" onclick="hapusDataRujukan(\''.$txtIdKunjungan.'\')"><i class="fa fa-trash"></i> Hapus</button>';
            $btnPrint = '<a class="btn btn-success btn-xs btn-flat" href="'.$this->base_url.'cetak-surat/'.$txtIdKunjungan.'/"><i class="fa fa-print"></i> Cetak Surat</a>';
            $btnAksi = $btnEdit  . $btnPrint . $btnBatal;
            $retVal['data'][] = array(
                $rowRujukan['txtNoAnggota'],
                $rowRujukan['txtNamaPasien'],
                $rowRujukan['txtUsiaPasienKunjungan'],
                $rowRujukan['NamaPelayananAsal'],
                $rowRujukan['NamaPelayanan'],
                $rowRujukan['txtPoliLuar'],
                $rowRujukan['txtnamaIndo'],
                $btnAksi
            );
        }
        echo json_encode($retVal);
	}

}
