<?php

class UploadDiagnosa extends MY_Controller{
    var $base_url = "";

    function __construct(){
        parent::__construct();
        $this->load->library('excel');
        $this->load->model("diagnosabpjs_model" , "diagnosa");
        $this->load->model("data_penyakit_model" , "data_penyakit");
        $this->base_url = $this->base_url_site. "other/uploaddiagnosa/";

    }

    function index(){
        $this->load->library("upload");

        $post = $this->input->post();
        if(!empty($post)){
            $file_path = "";
            $config['upload_path'] = ASSETS_UPLOAD_DIR;
            $config['allowed_types'] = 'xls|xlsx';
            ///echopre($config);
            $this->upload->initialize($config);
            $resUpload = $this->upload->do_upload("fileExcel");

            $data_upload = $this->upload->data();
            $file_path = $data_upload['full_path'];
            $objPHPExcel = PHPExcel_IOFactory::load($file_path);
            $allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
            $htmlRes = array();
            foreach($allDataInSheet as $rowSheet){
                $kodeICD = $rowSheet['A'];
                $namaDiagnosa = $rowSheet['B'];
                $strLenKode = strlen($kodeICD);
                if($strLenKode >= 3){
                    $checkDiagnosa = $this->diagnosa->checkDiagnosa($kodeICD);
                    if($checkDiagnosa < 1){
                        $arrData = array(
                            "txtKodeICD" => $kodeICD,
                            "txtEnglishName" => $namaDiagnosa,
                        );
                    $insertData = $this->diagnosa->insertDiagnosa($arrData);
                    $status = $insertData['status'];
                    $htmlRes[]= $kodeICD." - ".$insertData['message']."\n";
                    }
                }else{
                    $htmlRes[]= $kodeICD." - Data Header\n";
                }

            }
        }
        $dt['arrResult'] = $htmlRes;

        $this->load->view('other/upload_excel' , $dt);
    }

    function updateKodeICD($page = 1){
        $limit = 1000;
        $offset = ($page - 1) * $limit;
        $data = $this->data_penyakit->listDataDiagnosa( $limit , $offset);

        foreach($data as $rowPenyakit){
            $kodeICD = $rowPenyakit['txtCategory'];
            $strPosKode = strpos($kodeICD , '.');
            ///echo $strPosKode;die;
            if(empty($strPosKode)){
                $strLenKode = strlen($kodeICD);
                if($strLenKode > 3){
                    $mainKode = substr($kodeICD , 0 , 3);
                    $resKode = substr($kodeICD , 3, $strLenKode);

                    $kodeICDNew = $mainKode.".".$resKode;
                    $id = $rowPenyakit['id'];
                    $arrayUpdate = array(
                        "txtCategory" => $kodeICDNew,
                    );
                    $resUpdate = $this->data_penyakit->update($arrayUpdate , $id);
                }
            }
        }

        $limitData = $this->data_penyakit->countAllDiagnosis();
        if($offset > $limitData){
          echo "Selesai";die;
        }

        $pagePlus = $page + 1;
        $dt = array();
        $dt['akhirData'] = $offset;
        $dt['jumlahData'] = $this->data_penyakit->countAllDiagnosis();
        $dt['urlPage'] = $this->base_url."updateKodeICD/".$pagePlus;
        $this->load->view("other/rename_diagnosa",$dt);
    }

    function migrasiDiagnosa($page = 1){

        $limit = 1000;
        $offset = ($page - 1) * $limit;

        $limitData = $this->data_penyakit->countDiagnosaBPJSBaru();
        $limitDataDiagnosaBaru = $this->data_penyakit->countDiagnosaBaru();

        if($limitDataDiagnosaBaru >= $limitData){
            echo "Stahp NO Migrate More";die;
        }
        
        $dataDiagnosaBaru = $this->data_penyakit->getDiagnosaBaru($limit , $offset);
        $arrDataToInsert = array();
        foreach($dataDiagnosaBaru as $rowDiagnosaBaru){
            $arrInsert = array(
                "intIdPenyakit" => $rowDiagnosaBaru['txtKodeICD'],
                "txtCategory" => $rowDiagnosaBaru['txtKodeICD'],
                "txtEnglishName" => $rowDiagnosaBaru['txtEnglishName'],
                "txtIndonesianName" => $rowDiagnosaBaru['txtEnglishName'],
                "bitStatusPenyakit" => 1
            );
            $arrDataToInsert[] = $arrInsert;
        }

        $resInsertBatch = $this->data_penyakit->saveDiagnosaBaru($arrDataToInsert , true);
        if($offset > $limitData){
          echo "Selesai";die;
        }
        
        $pagePlus = $page + 1;
        $dt = array();
        $dt['akhirData'] = $offset;
        $dt['jumlahData'] = $limitData;
        $dt['urlPage'] = $this->base_url."migrasiDiagnosa/".$pagePlus;
        $this->load->view("other/rename_diagnosa",$dt);
    }

    function migrasiSisaDiagnosa($page = 1){
        
        $limit = 20;
        $offset = ($page - 1) * $limit;

        $data = $this->data_penyakit->getAllUsedDiagnosa($limit , $offset);
        $limitData = $this->data_penyakit->getCountAllUsedDiagnosa();

        if(empty($limitData)){
            echo "Done";die;
        }

        $arrDataToInsert = array();
        foreach($data as $rowDiagnosaBaru){
            $arrInsert = array(
                "intIdPenyakit" => $rowDiagnosaBaru['txtCategory'],
                "txtCategory" => $rowDiagnosaBaru['txtCategory'],
                "txtEnglishName" => $rowDiagnosaBaru['txtEnglishName'],
                "txtIndonesianName" => $rowDiagnosaBaru['txtIndonesianName'],
                "bitStatusPenyakit" => 1
            );
            $arrDataToInsert[] = $arrInsert;
        }

        $resInsertBatch = $this->data_penyakit->saveDiagnosaBaru($arrDataToInsert , true);
        
        $pagePlus = $page + 1;
        $dt = array();
        $dt['akhirData'] = $offset;
        $dt['jumlahData'] = $limitData;
        $dt['urlPage'] = $this->base_url."migrasiSisaDiagnosa/".$pagePlus;
        $this->load->view("other/rename_diagnosa",$dt);
    }

    public function renameIDDiagnosa($page = 1){
        
        $limit = 20;
        $offset = ($page - 1) * $limit;

        $data = $this->data_penyakit->getAllUsedDiagnosaBaru($limit);        
        $limitData = $this->data_penyakit->getCountAllUsedDiagnosaBaru();
        if(empty($limitData)){
            echo "Done";die;
        }

        $arrDataToInsert = array();
        foreach($data as $rowDiagnosaBaru){
            $arrInsert = array(
                "intIdDiagnosaPenyakit" => $rowDiagnosaBaru['txtCategory'],
            );

            $resUpdate = $this->data_penyakit->updateBatchRekmedDiagnosaBaru($arrInsert , $rowDiagnosaBaru['intIdDiagnosaPenyakit']);
            $arrDataToInsert[$rowDiagnosaBaru['txtCategory']] = $resUpdate;
        }

        $pagePlus = $page + 1;
        $dt = array();
        $dt['akhirData'] = $offset;
        $dt['jumlahData'] = $limitData;
        $dt['urlPage'] = $this->base_url."renameIDDiagnosa/".$pagePlus;
        $this->load->view("other/rename_diagnosa",$dt);
    }

    public function renameDiagnosa($page = 1){
        $limit = 1000;
        $offset = ($page - 1) * $limit;

        $data = $this->data_penyakit->listDiagnosaRename( $limit , $offset);
        $limitData = $this->data_penyakit->countAllDiagnosis();

        $arrData = array();
        foreach($data as $rowPenyakit){
              $id = $rowPenyakit['id'];
              $arrayUpdate = array(
                "intIdPenyakit" => $id,
                "txtIndonesianName" => $rowPenyakit['txtEnglishName'],
              );
              $arrData[] = $arrayUpdate;
              
        }
        $resUpdate = $this->data_penyakit->updateBatchDiagnosa($arrData);

        if($offset > $limitData){
          echo "Selesai";die;
        }

        $pagePlus = $page + 1;
        $dt = array();
        $dt['akhirData'] = $offset;
        $dt['jumlahData'] = $limitData;
        $dt['urlPage'] = $this->base_url."renameDiagnosa/".$pagePlus;
        $this->load->view("other/rename_diagnosa",$dt);
    }

}
