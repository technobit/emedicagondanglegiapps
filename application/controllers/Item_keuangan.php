<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Item_keuangan extends MY_Controller {

    var $meta_title = "Item Keuangan";
    var $meta_desc = "Item Keuangan";
    var $main_title = "Item Keuangan";
    var $menu_key = "keuangan_item";
    var $dtBreadcrumbs = array();  

    public function __construct() {
        parent::__construct();
        $this->dtBreadcrumbs = array(
            "Home" =>base_url(),
            "Keuangan" => base_url('keuangan'),
            "Item Keuangan" => base_url('keuangan/item_keuangan'),
        );
        $this->load->model('item_keuangan_m');
    }
    public function index() {        
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "menu_key" => $this->menu_key,
			"akses_key" => "is_view",
            "container" => $this->_index(),
            "custom_js" => array(
                ASSETS_JS_URL."keuangan/item_keuangan/index.js"
            ),
            "custom_css" => array(),
        );  
        $this->_render("default",$dt);             
    }

    private function _index() {        
        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['title'] = $this->meta_title;
        return $this->load->view('item_keuangan/index', $dt, true);
    }

    public function get($id = null) {
        $this->load->library('datatables');
        $query = $this->db->select('item_keuangan.*, pegawai.txtNamaPegawai')
        ->join('users_profil', 'users_profil.intIdUser = item_keuangan.created_by')
        ->join('pegawai', 'pegawai.intIdPegawai = users_profil.intIdPegawai')
        ->get_compiled_select('item_keuangan');
        $response = $this->datatables->collection($query)
        ->orderableColumns('nama_item, jasa_sarana, jasa_pelayanan, tarif, txtNamaPegawai, updated_at')
        ->searchableColumns('nama_item, txtNamaPegawai')
        ->editColumn('jasa_sarana', function($row) {
            return toNumber($row->jasa_sarana);
        })
        ->editColumn('jasa_pelayanan', function($row) {
            return toNumber($row->jasa_pelayanan);
        })
        ->editColumn('updated_at', function($row) {
            return indonesian_date($row->updated_at);
        }) 
        ->editColumn('aktif', function($row) {
            return ($row->aktif == 1) ? '<span class="text-success glyphicon glyphicon-check"></span>' : '<span class="glyphicon glyphicon-unchecked"></span>';
        })
        ->addColumn('tarif', function($row) {
            return toNumber(($row->jasa_sarana + $row->jasa_pelayanan));
        })
        ->addColumn('action', function($row) {
                return '<a href="'.base_url('keuangan/item_keuangan/edit/' . $row->id).'" class="btn btn-warning btn-sm"><i class="fa fa-pencil"></i> Ubah</a> '.
                       '<a onclick="return confirm(\'Apakah anda yakin akan menghapus data ini?\')" href="'.base_url('keuangan/item_keuangan/delete/' . $row->id).'" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> Hapus</a>';
        })
        ->render();
        $this->output->set_content_type('application/json')->set_output(json_encode($response));
    }

    public function create() {
        if ($post = $this->input->post()) {
            $this->validation();
            $post['created_by'] = $this->session->userdata('sipt_user_id');
            $this->item_keuangan_m->insert($post);
            $this->redirect->with('successMessage', 'Data item keuangan berhasil ditambahkan.')->to('keuangan/item_keuangan');
        }
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "menu_key" => $this->menu_key,
			"akses_key" => "is_insert",
            "container" => $this->_form(),
            "custom_js" => array(
                ASSETS_JS_URL."keuangan/item_keuangan/form.js"
            ),
            "custom_css" => array(),
        );  
        $this->_render("default",$dt);            
    }

    public function edit($id) {
        $result = $this->find($id);
        if ($post = $this->input->post()) {
            $this->validation();
            $this->item_keuangan_m->update($id, $post);
            $this->redirect->with('successMessage', 'Data item keuangan berhasil diperbarui.')->to('keuangan/item_keuangan');
        }
        $this->form->setData($result);
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "menu_key" => $this->menu_key,
			"akses_key" => "is_update",
            "container" => $this->_form(),
            "custom_js" => array(
                ASSETS_JS_URL."keuangan/item_keuangan/form.js"
            ),
            "custom_css" => array(),
        );  
        $this->_render("default",$dt);      
    }

    private function _form() {
        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['title'] = $this->meta_title;
        return $this->load->view('item_keuangan/form', $dt, true);
    }

    public function delete($id) {
        $result = $this->find($id);
        $this->item_keuangan_m->delete($id);
        $this->redirect->with('successMessage', 'Data item keuangan berhasil dihapus.')->to('keuangan/item_keuangan');
    }

    private function find($id) {
        $result = $this->item_keuangan_m->find($id);
        if (!$result) {
            $this->redirect->with('errorMessage', 'Data obat tidak ditemukan.')->back();
        } else {
            return $result;
        }
    }

    private function validation() {
        $this->load->library('form_validation');        
        $this->form_validation->set_rules('nama_item', 'Jenis Pelayanan', 'required');        
        $this->form_validation->set_rules('jasa_sarana', 'jasa Sarana', 'required|numeric');        
        $this->form_validation->set_rules('jasa_pelayanan', 'Jasa Pelayanan', 'required|numeric');  
        if (!$this->form_validation->run()) {
            $this->redirect->withInput()->withValidation()->back();
        }        
    }

}