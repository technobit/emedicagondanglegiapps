<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends MY_Controller {
    
    var $meta_title = "Laporan";
    var $meta_desc = "Pengguna Layanan";
	var $main_title = "Pengguna Layanan";
    var $base_url = "";
	var $limit = "20";
	var $arr_level_user = "";
    var $menu = "K01";
    var $mode = "";
    var $main_model = "";
    var $js_folder = "";
    var $jenis_pelayanan = "";
	function __construct(){
        parent::__construct();
		
		$this->load->model("register_model" );
        $this->base_url = $this->base_url_site."laporan/";
        $this->js_folder = ASSETS_JS_URL."master_data/tindakan/";
        $this->jenis_pelayanan = $this->config->item("jenis_pelayanan");
    }
    
  
    /// Data Laporan
	public function index()
	{
        $menu = "K01";
		$this->mode = "view";
		$dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
			"container" => $this->_build_index(),
			"custom_js" => array(
				ASSETS_JS_URL."laporn/content.js",
			),
            "custom_css" => array(
				
			),
		);	
		$this->_render("default",$dt);	
	}

        

    private function _build_index(){
        $dt = array();
		$breadcrumbs = array(
				"Home" =>$this->base_url_site,
				"Laporan" => $this->base_url,
        );
		
		$dt['breadcrumbs'] = $this->setBreadcrumbs($breadcrumbs);
		$dt['title'] = "Laporan";

        $arrLaporanHarian = array(
             "kunjungan"=> array(
                "icon" => "fa fa-edit",
                "title" => "Laporan Data Kunjungan Harian",
                "link" => $this->base_url."harian/data-kunjungan-registrasi/",
             ),
             "pelayanan"=> array(
                "icon" => "fa fa-edit",
                "title" => "Laporan Data Kunjungan Per Pelayanan",
                "link" => $this->base_url."harian/data-kunjungan-pelayanan/",
             ),
        );

        $arrLaporanBulanan = array(
            "data_kesakitan"=> array(
                "icon" => "fa fa-edit",
                "title" => "LB-1 - Laporan Data Kesakitan",
                "link" => $this->base_url."harian/data-pelayanan/",
             ),
             "stock_apotik"=> array(
                "icon" => "fa fa-edit",
                "title" => "LB-2 - Laporan Stock Apotik",
                "link" => $this->base_url."harian/data-pelayanan/",
             ),
             "stock_gudang"=> array(
                "icon" => "fa fa-edit",
                "title" => "LB-2 - Laporan Stock Gudang",
                "link" => $this->base_url."harian/data-jamkes/",
             ),
             "stock_gudang"=> array(
                "icon" => "fa fa-edit",
                "title" => "LB-2 - Laporan Stock Gudang",
                "link" => $this->base_url."harian/data-jamkes/",
             ),
        );

        $dt['laporan_harian'] = $arrLaporanHarian;
        $dt['laporan_bulanan'] = $arrLaporanHarian;
		$ret = $this->load->view("laporan/content" , $dt, true);
		return $ret;
    }
    
}
