<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Mike42\Escpos\PrintConnectors\FilePrintConnector;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\CapabilityProfile\SimpleCapabilityProfile;
use Mike42\Escpos\Printer;

class Printpage extends MY_Controller {
    
    var $meta_title = "Data Tindakan";
    var $meta_desc = "Pengguna Layanan";
	var $main_title = "Pengguna Layanan";
    var $base_url = "";
	var $limit = "20";
	var $arr_level_user = "";
    var $menu = "K01";
    var $mode = "";
    var $main_model = "";
    var $js_folder = "";
    var $jenis_pelayanan = "";
	var $serverPrint = "";
	var $serverPrintKasir = "";
	var $serverPrinterSelfService = "";
	function __construct(){
        parent::__construct();
		$this->load->model("pelayanan_model" );
        $this->load->model("pasien_model" );
		$this->load->model("pegawai_model" );
        $this->load->model("register_model");
		$this->load->model('item_keuangan_m');
        $this->load->model('keuangan_rawat_jalan_m');
		$this->load->model('keuangan_rawat_inap_m');
		$this->load->model('keuangan_diluar_m');
        $this->base_url = $this->base_url_site."print-antrian/";
		$this->serverPrint = $this->session->userdata("printer_loket");
		$this->serverPrintKasir =  $this->session->userdata("printer_kasir");
		$this->serverPrintSelfService = $this->session->userdata("printer_self_service");
    }

    /// Print Antrian
	public function index($idKunjungan=""){

		$detail_register = $this->register_model->getDetailRegister($idKunjungan);	
        $noAntri = $detail_register['intNoAntri'];		
		$idPelayanan = $detail_register['intIdPelayanan'];
		$detail_pelayanan = $this->pelayanan_model->getDetail($idPelayanan);
		$namaPelayanan = $detail_pelayanan['txtNama'];

		$tanggalKunjungan = explode(' ',$detail_register['dtTanggalKunjungan']);
		$tanggal = indonesian_date($tanggalKunjungan[0]);
		$jam = $tanggalKunjungan[1];
		///$connector = new FilePrintConnector($this->serverPrint);
		$connector = new WindowsPrintConnector("smb://Guest@CYBERGROUND38/epson");
		$printer = new Printer($connector);

		$printer->setJustification(Printer::JUSTIFY_CENTER); //
		$printer->selectPrintMode(Printer::MODE_FONT_B | Printer::MODE_EMPHASIZED | Printer::MODE_UNDERLINE | Printer::MODE_DOUBLE_HEIGHT);
		$printer->text("Puskesmas Bululawang \n Kab. Malang");
		$printer->selectPrintMode();
		$printer->feed(2);
		$printer->selectPrintMode(Printer::MODE_DOUBLE_HEIGHT);
		$printer->text("Tanggal : ".$tanggal."\n");
		$printer->selectPrintMode();
		$printer->feed();
		$printer->selectPrintMode(Printer::MODE_DOUBLE_HEIGHT);
		$printer->text("No. Antrian \n");
		$printer->selectPrintMode();
		$printer->feed();
		$printer->selectPrintMode(Printer::MODE_EMPHASIZED | Printer::MODE_DOUBLE_HEIGHT | Printer::MODE_DOUBLE_WIDTH);
		//$printer->setTextSize(8,8);
		$printer->text("".$noAntri."\n");
		///$printer->setTextSize(1,1);
		$printer->selectPrintMode();
		$printer->feed(2);
		$printer->selectPrintMode(Printer::MODE_DOUBLE_HEIGHT);
		$printer->text("Pelayanan : \n");
		$printer->selectPrintMode();
		$printer->feed();
		$printer->selectPrintMode(Printer::MODE_FONT_B | Printer::MODE_EMPHASIZED | Printer::MODE_DOUBLE_HEIGHT);
		$printer->text("".$namaPelayanan."\n");
		$printer->selectPrintMode();
		$printer->cut();
		$printer->close();

	}

	function samplePrint(){
		
		///$connector = new FilePrintConnector($this->serverPrint);
		///$connector = new WindowsPrintConnector("smb://DESKTOP-CCQSBK0/epson_thermal");
		$connector = new WindowsPrintConnector($this->serverPrint);

		///$connector = new WindowsPrintConnector($this->serverPrintKasir);

		//$connector = new FilePrintConnector("\\\\192.168.0.18\\epson");
		///$profile = CapabilityProfile::load("simple");
		$printer = new Printer($connector );
		$printer->setJustification(Printer::JUSTIFY_CENTER); // Reset
		$printer->selectPrintMode(Printer::MODE_FONT_B | Printer::MODE_EMPHASIZED | Printer::MODE_UNDERLINE | Printer::MODE_DOUBLE_HEIGHT | Printer::MODE_DOUBLE_HEIGHT);
		$printer->text("PUSKESMAS BULULAWANG\n");
		$printer->selectPrintMode();
		$printer->feed(3);
		$printer->text("BERIKUT TES KONEKSI PRINTER \n");
		$printer->cut();
		$printer->close();
	}
	
	function samplePrintKasir(){
		
		///$connector = new FilePrintConnector($this->serverPrint);
		///$connector = new WindowsPrintConnector("smb://DESKTOP-CCQSBK0/epson_thermal");
		///$connector = new WindowsPrintConnector($this->serverPrint);
		$connector = new WindowsPrintConnector($this->serverPrintKasir);
		//$connector = new FilePrintConnector("\\\\192.168.0.18\\epson");
		///$profile = CapabilityProfile::load("simple");
		$printer = new Printer($connector );
		$printer->setJustification(Printer::JUSTIFY_CENTER); // Reset
		$printer->selectPrintMode(Printer::MODE_FONT_B | Printer::MODE_EMPHASIZED | Printer::MODE_UNDERLINE | Printer::MODE_DOUBLE_HEIGHT | Printer::MODE_DOUBLE_HEIGHT);
		$printer->text("PUSKESMAS BULULAWANG\n");
		$printer->selectPrintMode();
		$printer->feed(3);
		$printer->text("BERIKUT TES KONEKSI PRINTER \n");
		$printer->cut();
		$printer->close();
	}

	function samplePrintSelfService(){
		
		///$connector = new FilePrintConnector($this->serverPrint);
		///$connector = new WindowsPrintConnector("smb://DESKTOP-CCQSBK0/epson_thermal");
		///$connector = new WindowsPrintConnector($this->serverPrint);
		$connector = new WindowsPrintConnector($this->serverPrintSelfService);
		//$connector = new FilePrintConnector("\\\\192.168.0.18\\epson");
		///$profile = CapabilityProfile::load("simple");
		$printer = new Printer($connector );
		$printer->setJustification(Printer::JUSTIFY_CENTER); // Reset
		$printer->selectPrintMode(Printer::MODE_FONT_B | Printer::MODE_EMPHASIZED | Printer::MODE_UNDERLINE | Printer::MODE_DOUBLE_HEIGHT | Printer::MODE_DOUBLE_HEIGHT);
		$printer->text("PUSKESMAS BULULAWANG\n");
		$printer->selectPrintMode();
		$printer->feed(3);
		$printer->text("BERIKUT TES KONEKSI PRINTER \n");
		$printer->cut();
		$printer->close();
	}

	function cetakNoAntrianLoket(){

		$this->load->model('register_loket_model');
		$idKunjungan = $this->input->post("idKunjungan");

		$detail_register = $this->register_loket_model->detail($idKunjungan);	
        $noAntri = $detail_register['intNoAntri'];
		
		$idPelayanan = $detail_register['intIdPelayanan'];
		$detail_pelayanan = $this->pelayanan_model->getDetail($idPelayanan);
		$namaPelayanan = $detail_pelayanan['txtNama'];
		
		$tanggalKunjungan = explode(' ',$detail_register['dtTanggalKunjungan']);
		$tanggal = indonesian_date($tanggalKunjungan[0]);
		$jam = $tanggalKunjungan[1];

		$connector = new WindowsPrintConnector($this->serverPrintSelfService);
		$printer = new Printer($connector);
		$printer->setJustification(Printer::JUSTIFY_CENTER); //
		$printer->selectPrintMode(Printer::MODE_FONT_B | Printer::MODE_EMPHASIZED | Printer::MODE_UNDERLINE | Printer::MODE_DOUBLE_HEIGHT);
		$printer->text("                                   \nRumah Sakit Emedica");
		$printer->selectPrintMode();
		$printer->feed(2);
		$printer->selectPrintMode(Printer::MODE_FONT_B);
		$printer->text("Tanggal : ".$tanggal);
		$printer->selectPrintMode();
		$printer->feed();
		$printer->selectPrintMode(Printer::MODE_FONT_B);
		$printer->text("Nomor Antrian :\n");
		$printer->selectPrintMode();
		$printer->feed();
		$printer->selectPrintMode(Printer::MODE_EMPHASIZED | Printer::MODE_DOUBLE_HEIGHT | Printer::MODE_DOUBLE_HEIGHT | Printer::MODE_DOUBLE_WIDTH);
		//$printer->setTextSize(8,8);
		$printer->text("".$noAntri."\n");
		///$printer->setTextSize(1,1);
		$printer->selectPrintMode();
		$printer->feed();
		$printer->selectPrintMode(Printer::MODE_FONT_B);
		$printer->text("Loket :");
		$printer->selectPrintMode();
		$printer->feed();
		$printer->selectPrintMode(Printer::MODE_FONT_A | Printer::MODE_EMPHASIZED | Printer::MODE_DOUBLE_HEIGHT);
		$printer->text("".$namaPelayanan."\n");
		$printer->selectPrintMode();
		$printer->feed();
		$printer->selectPrintMode(Printer::MODE_FONT_B| Printer::MODE_EMPHASIZED);
		$printer->text("...................................\n");
		$printer->selectPrintMode();
		$printer->feed(2);
		$printer->cut();
		$printer->close();
	}

	function cetakNoAntrian($mode = "loket"){
		if(!$this->input->is_ajax_request()){
			echo "Ilegal";die;
		}

		$idKunjungan = $this->input->post("idKunjungan");
		$detail_register = $this->register_model->getDetailRegister($idKunjungan);	
        $noAntri = $detail_register['intNoAntri'];
		
		$idPelayanan = $detail_register['intIdPelayanan'];
		$detail_pelayanan = $this->pelayanan_model->getDetail($idPelayanan);
		$namaPelayanan = $detail_pelayanan['txtNama'];
		$ruangPelayanan = $detail_pelayanan['intRuang'];
		
		$tanggalKunjungan = explode(' ',$detail_register['dtTanggalKunjungan']);
		$tanggal = indonesian_date($tanggalKunjungan[0]);
		$jam = $tanggalKunjungan[1];
		///$connector = new FilePrintConnector($this->serverPrint);
		switch($mode) {
			case "loket" : $connector = new WindowsPrintConnector($this->serverPrint);break;
			case "self-service" : $connector = new WindowsPrintConnector($this->serverPrintSelfService);break;
		}
		
		$printer = new Printer($connector);
		$printer->setJustification(Printer::JUSTIFY_CENTER); //
		$printer->selectPrintMode(Printer::MODE_FONT_B | Printer::MODE_EMPHASIZED | Printer::MODE_UNDERLINE | Printer::MODE_DOUBLE_HEIGHT);
		$printer->text("                                   \nRumah Sakit Emedica");
		$printer->selectPrintMode();
		$printer->feed(2);
		$printer->selectPrintMode(Printer::MODE_FONT_B);
		$printer->text("Tanggal : ".$tanggal);
		$printer->selectPrintMode();
		$printer->feed();
		$printer->selectPrintMode(Printer::MODE_FONT_B);
		$printer->text("Nomor Antrian :\n");
		$printer->selectPrintMode();
		$printer->feed();
		$printer->selectPrintMode(Printer::MODE_EMPHASIZED | Printer::MODE_DOUBLE_HEIGHT | Printer::MODE_DOUBLE_HEIGHT | Printer::MODE_DOUBLE_WIDTH);
		//$printer->setTextSize(8,8);
		$printer->text("".$noAntri."\n");
		///$printer->setTextSize(1,1);
		$printer->selectPrintMode();
		$printer->feed();
		$printer->selectPrintMode(Printer::MODE_FONT_B);
		$printer->text("Pelayanan :");
		$printer->selectPrintMode();
		$printer->feed();
		$printer->selectPrintMode(Printer::MODE_FONT_A | Printer::MODE_EMPHASIZED | Printer::MODE_DOUBLE_HEIGHT);
		$printer->text("".$namaPelayanan." - Ruang : ".$ruangPelayanan."\n");
		$printer->selectPrintMode();
		$printer->feed();
		$printer->selectPrintMode(Printer::MODE_FONT_B| Printer::MODE_EMPHASIZED);
		$printer->text("...................................\n");
		$printer->selectPrintMode();
		$printer->feed(2);
		$printer->cut();
		$printer->close();
	}

	

	/// For Partial Functions
	function printInvoiceRawatInap(){
		$idKunjungan = $this->input->post("idKunjungan");
		$idItem = $this->input->post("idItem");

		$dataPasien = $this->register_model->getDetail($idKunjungan);
		$dataKeuangan = $this->keuangan_rawat_inap_m->get_keuangan_inap($idItem)->row_array();
		$dataKeuanganDetail = $this->keuangan_rawat_inap_m->get_detail_keuangan($idItem)->result();
		$bitBebasBiaya = $dataKeuangan['bitBebasBiaya'];
		$date = indonesian_date($dataPasien['dtTanggalKunjungan']);
		$kasirId = $dataKeuangan['created_by'];
		$kasirDetail = $this->pegawai_model->getDetail($kasirId);
		$namaKasir = $kasirDetail['txtNamaPegawai'];


		$connector = new WindowsPrintConnector($this->serverPrintKasir);		
		$printer = new Printer($connector);

		$printer->setJustification(Printer::JUSTIFY_CENTER); //
		$printer->selectPrintMode(Printer::MODE_FONT_B | Printer::MODE_EMPHASIZED | Printer::MODE_UNDERLINE | Printer::MODE_DOUBLE_HEIGHT);
		$printer->text("                                   \nRumah Sakit Emedica");
		$printer->selectPrintMode();
		$printer->feed(2);
		/* Title of receipt */
		$printer -> setEmphasis(true);
		$printer -> text("BUKTI PEMBAYARAN\n");
		$printer -> setEmphasis(false);
		$printer -> feed();

		/* Title of receipt */
		$printer -> setJustification(Printer::JUSTIFY_LEFT);
		
		$printer -> text("No Nota : ".str_replace("KJN" , "" , $idKunjungan)." - ".$idItem."\n");
		$printer -> text("Tanggal     : ".$date."\n");
		$printer -> text("Kasir       : ".$namaKasir."\n");
		$printer -> text("No. Anggota : ".$dataPasien['txtNoAnggota']."\n");
		$printer -> text("Nama Pengguna Layanan : ".$dataPasien['txtNamaPasien']."\n");
		$printer -> feed();
		///$printer -> text("Jamkes      : ".$dataPasien['txtNamaJaminan']."\n");
		///$printer -> text("No. Jamkes  : ".$dataPasien['txtNoJaminanKesehatan']."\n");
		/// Write Lines
		$strStrip = str_pad("=" , 33 , "=");
		$printer -> text($strStrip);		
		$printer -> setJustification(Printer::JUSTIFY_LEFT);
		/* Items */
		$totalTagihan = 0;
		foreach($dataKeuanganDetail as $rowKeuangan){
			$namaItem = str_pad($rowKeuangan->nama_item,32," ") . "\n";
			$jumlahItem = str_pad($rowKeuangan->jumlah."X",10," ");
			$totalItemPrice = ($rowKeuangan->jasa_sarana + $rowKeuangan->jasa_pelayanan) * $rowKeuangan->jumlah;
			$totalItem = str_pad(number_format($totalItemPrice,0,".","."),22," ",STR_PAD_LEFT)."\n"; 
			$printer->text($namaItem);
			$printer->text($jumlahItem);
			$printer->text($totalItem);
			$totalTagihan += $totalItemPrice;
		}
		$strStrip = str_pad("=" , 33 , "=");
		$printer -> text($strStrip);		

		///$totalTagihan = $dataPasien['total_tagihan'];
		$totalBayar = $dataKeuangan['total_bayar'];
		$kembalian = $totalBayar - $totalTagihan;
		
		$printer -> setEmphasis(true);
		$printer -> text(str_pad("Subtotal" , 12 , " "));
		$printer -> text(str_pad(number_format($totalTagihan , 0 , "." , ".") , 21 , " " , STR_PAD_LEFT));
		if($bitBebasBiaya==1){
			$totalBayar = 0;
			$kembalian  = 0;
		$printer -> text(str_pad("Potongan" , 12 , " "));
		$printer -> text(str_pad("(".number_format($totalTagihan , 0 , "." , ".").")" , 21 , " " , STR_PAD_LEFT));
		}
		$printer -> text(str_pad("Bayar" , 12 , " "));
		$printer -> text(str_pad(number_format($totalBayar , 0 , "." , ".") , 21 , " " , STR_PAD_LEFT));
		
		$printer -> text(str_pad("Kembalian" , 12 , " "));
		$printer -> text(str_pad(number_format($kembalian , 0 , "." , ".") , 21 , " ", STR_PAD_LEFT));
		$printer -> setEmphasis(false);
		$printer -> feed();
		/* Footer */
		$printer -> feed(2);
		$printer -> setJustification(Printer::JUSTIFY_CENTER);
		$printer -> text("Terima Kasih \n");
		$printer -> text("Semoga Lekas Sembuh \n");
		$printer -> feed(2);
		/* Cut the receipt and please open your heart :')' */
		$printer -> cut();
		$printer -> close();
	}	

	function printInvoiceRawatJalan($id){
		$dataPasien = $this->register_model->getDetailKeuangan($id);

		$keuangan = $this->keuangan_rawat_jalan_m->getKeuangan($id)->result();
		///$dataPasien = $this->register_model->getDetailKeuangan($id);
		///$keuangan = $this->keuangan_rawat_jalan_m->getKeuangan($id)->result();
		$dataPasien = $this->register_model->getDetailKeuangan($id);
		$bitBebasBiaya = $dataPasien['bitBebasBiaya'];
		$date = indonesian_date($dataPasien['dtTanggalKunjungan']);
		$kasirId = $dataPasien['created_by'];
		$kasirDetail = $this->pegawai_model->getDetail($kasirId);
		$namaKasir = $kasirDetail['txtNamaPegawai'];
		/* Start the printer */
		///$connector = new WindowsPrintConnector($this->serverPrint);
		$connector = new WindowsPrintConnector($this->serverPrintKasir);
		$printer = new Printer($connector);

		$printer->setJustification(Printer::JUSTIFY_CENTER); //
		$printer->selectPrintMode(Printer::MODE_FONT_B | Printer::MODE_EMPHASIZED | Printer::MODE_UNDERLINE | Printer::MODE_DOUBLE_HEIGHT);
		$printer->text("                                   \nRumah Sakit Emedica");
		$printer->selectPrintMode();
		$printer->feed(2);
		/* Title of receipt */
		$printer -> setEmphasis(true);
		$printer -> text("BUKTI PEMBAYARAN\n");
		$printer -> setEmphasis(false);
		$printer -> feed();

		/* Title of receipt */
		$printer -> setJustification(Printer::JUSTIFY_LEFT);
		
		$printer -> text("No Nota     : ".str_replace("KJN" , "" , $id)."\n");
		$printer -> text("Tanggal     : ".$date."\n");
		$printer -> text("Kasir       : ".$namaKasir."\n");
		$printer -> text("No. Anggota : ".$dataPasien['txtNoAnggota']."\n");
		$printer -> text("Nama Pengguna Layanan : ".$dataPasien['txtNamaPasien']."\n");
		$printer -> feed();
		///$printer -> text("Jamkes      : ".$dataPasien['txtNamaJaminan']."\n");
		///$printer -> text("No. Jamkes  : ".$dataPasien['txtNoJaminanKesehatan']."\n");
		
		/// Write Lines
		$strStrip = str_pad("=" , 33 , "=");
		$printer -> text($strStrip);		
		$printer -> setJustification(Printer::JUSTIFY_LEFT);
		/* Items */
		$totalTagihan = 0;
		foreach($keuangan as $rowKeuangan){
			$namaItem = str_pad($rowKeuangan->nama_item,33," ");
			$jumlahItem = str_pad($rowKeuangan->jumlah."X",10," ");
			$totalItemPrice = ($rowKeuangan->jasa_sarana + $rowKeuangan->jasa_pelayanan) * $rowKeuangan->jumlah;
			$totalItem = str_pad(number_format($totalItemPrice,0,".","."),23," ",STR_PAD_LEFT)."\n"; 
			$printer->text($namaItem);
			$printer->text($jumlahItem);
			$printer->text($totalItem);
			$totalTagihan += $totalItemPrice;

		}
		$strStrip = str_pad("=" , 33 , "=");
		$printer -> text($strStrip);		

		///$totalTagihan = $dataPasien['total_tagihan'];
		$totalBayar = $dataPasien['total_bayar'];
		$kembalian = $totalBayar - $totalTagihan;
		
		$printer -> setEmphasis(true);
		$printer -> text(str_pad("Subtotal" , 12 , " "));
		$printer -> text(str_pad(number_format($totalTagihan , 0 , "." , ".") , 21 , " " , STR_PAD_LEFT));
		if($bitBebasBiaya==1){
			$totalBayar = 0;
			$kembalian  = 0;
		$printer -> text(str_pad("Potongan" , 12 , " "));
		$printer -> text(str_pad("(".number_format($totalTagihan , 0 , "." , ".").")" , 21 , " " , STR_PAD_LEFT));
		}
		$strStrip = str_pad("=" , 33 , "=");
		$printer -> text($strStrip);		

		///$totalTagihan = $dataPasien['total_tagihan'];
		$totalBayar = $dataPasien['total_bayar'];
		$kembalian = $totalBayar - $totalTagihan;
		
		$printer -> setEmphasis(true);
		$printer -> text(str_pad("Subtotal" , 12 , " "));
		$printer -> text(str_pad(number_format($totalTagihan , 0 , "." , ".") , 21 , " " , STR_PAD_LEFT));
		if($bitBebasBiaya==1){
			$totalBayar = 0;
			$kembalian  = 0;
		$printer -> text(str_pad("Potongan" , 12 , " "));
		$printer -> text(str_pad("(".number_format($totalTagihan , 0 , "." , ".").")" , 21 , " " , STR_PAD_LEFT));
		}

		$printer -> text(str_pad("Bayar" , 12 , " "));
		$printer -> text(str_pad(number_format($totalBayar , 0 , "." , ".") , 21 , " " , STR_PAD_LEFT));
		
		$printer -> text(str_pad("Kembalian" , 12 , " "));
		$printer -> text(str_pad(number_format($kembalian , 0 , "." , ".") , 21 , " ", STR_PAD_LEFT));
		$printer -> setEmphasis(false);
		$printer -> feed();
		/* Footer */
		$printer -> feed(2);
		$printer -> setJustification(Printer::JUSTIFY_CENTER);
		$printer -> text("Terima Kasih \n");
		$printer -> text("Semoga Lekas Sembuh \n");
		$printer -> feed(2);

		/* Cut the receipt and please open your heart :')' */
		$printer -> cut();
		$printer -> close();
	}

	public function printInvoiceDiluar(){
		$idBuktiBayar = $this->input->post("idBuktiBayar");
		$dataKeuangan = $this->keuangan_diluar_m->detailKeuangan($idBuktiBayar)->row_array();
		$dataKeuanganDetail = $this->keuangan_diluar_m->getLayanan($idBuktiBayar)->result();
		$date = indonesian_date($dataKeuangan['tanggal']);
		$namaKasir = $dataKeuangan['txtNamaPegawai'];
		$connector = new WindowsPrintConnector($this->serverPrintKasir);
		$printer = new Printer($connector);

		$printer->setJustification(Printer::JUSTIFY_CENTER); //
		$printer->selectPrintMode(Printer::MODE_FONT_B | Printer::MODE_EMPHASIZED | Printer::MODE_UNDERLINE | Printer::MODE_DOUBLE_HEIGHT);
		$printer->text("                                   \nRumah Sakit Emedica");
		$printer->selectPrintMode();
		$printer->feed(2);
		/* Title of receipt */
		$printer -> setEmphasis(true);
		$printer -> text("BUKTI PEMBAYARAN\n");
		$printer -> setEmphasis(false);
		$printer -> feed();

		/* Title of receipt */
		$printer -> setJustification(Printer::JUSTIFY_LEFT);
		
		$printer -> text("No Nota     : ".$dataKeuangan['no_bukti']."\n");
		$printer -> text("Tanggal     : ".$date."\n");
		$printer -> text("Kasir       : ".$namaKasir."\n");
		$printer -> feed();
		
		/// Write Lines
		$strStrip = str_pad("=" , 33 , "=");
		$printer -> text($strStrip);		
		$printer -> setJustification(Printer::JUSTIFY_LEFT);
		/* Items */
		$totalTagihan = 0;
		foreach($dataKeuanganDetail as $rowKeuangan){
			$namaItem = str_pad($rowKeuangan->nama_item,32," ") . "\n";
			$jumlahItem = str_pad($rowKeuangan->jumlah."X",10," ");
			$totalItemPrice = ($rowKeuangan->jasa_sarana + $rowKeuangan->jasa_pelayanan) * $rowKeuangan->jumlah;
			$totalItem = str_pad(number_format($totalItemPrice,0,".","."),22," ",STR_PAD_LEFT)."\n"; 
			$printer->text($namaItem);
			$printer->text($jumlahItem);
			$printer->text($totalItem);
			$totalTagihan += $totalItemPrice;
		}
		$strStrip = str_pad("=" , 33 , "=");
		$printer -> text($strStrip);		

		///$totalTagihan = $dataPasien['total_tagihan'];
		$totalBayar = $dataKeuangan['total_bayar'];
		$kembalian = $totalBayar - $totalTagihan;
		
		$printer -> setEmphasis(true);
		$printer -> text(str_pad("Subtotal" , 12 , " "));
		$printer -> text(str_pad(number_format($totalTagihan , 0 , "." , ".") , 21 , " " , STR_PAD_LEFT));

		$printer -> text(str_pad("Bayar" , 12 , " "));
		$printer -> text(str_pad(number_format($totalBayar , 0 , "." , ".") , 21 , " " , STR_PAD_LEFT));
		
		$printer -> text(str_pad("Kembalian" , 12 , " "));
		$printer -> text(str_pad(number_format($kembalian , 0 , "." , ".") , 21 , " ", STR_PAD_LEFT));
		$printer -> setEmphasis(false);
		$printer -> feed(2);
		/* Footer */
		/* Cut the receipt and please open your heart :')' */
		$printer -> cut();
		$printer -> close();
	}

	public function _formatText($name , $price , $AddRupiah = false)
    {
        $rightCols = 10;
        $leftCols = 30;
        if ($AddRupiah) {
            $leftCols = $leftCols / 2 - $rightCols / 2;
        }
        $left = str_pad($name, $leftCols) ;
        $sign = ($AddRupiah ? 'Rp.' : '');
        $right = str_pad($sign . $price, $rightCols, ' ', STR_PAD_LEFT);
        return "$left$right\n";
    }

	
}