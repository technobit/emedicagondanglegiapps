<?php

class Data_penyakit extends MY_Controller {

	var $meta_title = "Laporan Data Kesakitan - (LB-1)";
    var $meta_desc = "Laporan Keuangan";
    var $main_title = "Laporan Keuangan";
    var $menu_key = "laporan_kesakitan";
    var $dtBreadcrumbs = array();  

	public function __construct() {
		parent::__construct();
        $this->load->model("pelayanan_model");
        $this->load->model("data_penyakit_model" , "data_penyakit");
        
		$this->load->library('excel');
	}

	public function index() {        
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "menu_key" => $this->menu_key,
			"akses_key" => "is_view",
            "container" => $this->_index(),
            "custom_js" => array(   
            	ASSETS_JS_URL."laporan/data_penyakit/index.js"             
            ),
            "custom_css" => array(),
        );  
        $this->_render("default",$dt);             
    }

    private function _index() {        
        $listPelayanan = $this->pelayanan_model->getListDataPelayanan();
        $arrPelayanan = array("0"=>"Semua");
        foreach ($listPelayanan as $valuePelayanan) {
            # code...
            $arrPelayanan[$valuePelayanan['intIdPelayanan']] = $valuePelayanan['txtNama'];
        }

        $arrBulan = $this->config->item("month_list");
        $arrBulan[0] = "-Pilih Bulan-";
        $rangeYear = range(2010 , 2017);
        $arrYear = array();
        foreach($rangeYear as $year){
            $arrYear[$year] = $year;
        }


        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['frmPelayanan'] = $this->form_builder->inputDropdown("Ruangan" , "cmbPoli" , "" , $arrPelayanan);
        $dt['frmTahun'] = $this->form_builder->inputDropdown("Tahun" , "cmbTahun" , date('Y'), $arrYear);
        $dt['frmBulanan'] = $this->form_builder->inputDropdown("Bulan" , "cmbBulan" , "0" , $arrBulan);
        $dt['title'] = $this->meta_title;
        return $this->load->view('laporan/data_penyakit/content', $dt, true);
    }

    public function getDataLaporan(){
        if(!$this->input->is_ajax_request()){
            echo "Ilegall";die;
        }

        $poli = $this->input->post("id_pelayanan");
        $bulan = $this->input->post("bulan");
        $tahun = $this->input->post("tahun");
        $retVal = array();
        if($bulan==0){
            $retVal['draw'] = $this->input->post('draw');
            $retVal['recordsTotal'] = 0;
            $retVal['recordsFiltered'] = 0;
            $retVal['data'] = array();
            echo json_encode($retVal);
            die;    
        }else{
            $dataRows = $this->data_penyakit->getDataLB1V2($poli , $bulan , $tahun);
            if(!empty($dataRows)){
                
                $data = array();
                $no = 1;
                foreach ($dataRows as $rows) {
                    # code...
                     
                    $ICDX = $rows['txtCategory']; 
                    $data[] = array(
                        $rows['txtIndonesianName'],
                        $ICDX,
                        $rows['Register_0_7_baru'],
                        $rows['Register_0_7_lama'],
                        $rows['Register_8_14_baru'],
                        $rows['Register_8_14_lama'],
                        $rows['Register_28_60_baru'],
                        $rows['Register_28_60_lama'],
                        $rows['Register_60_365_baru'],
                        $rows['Register_60_365_lama'],
                        $rows['Register_1_4_baru'],
                        $rows['Register_1_4_lama'],
                        $rows['Register_5_9_baru'],
                        $rows['Register_5_9_lama'],
                        $rows['Register_10_14_baru'],
                        $rows['Register_10_14_lama'],
                        $rows['Register_15_19_baru'],
                        $rows['Register_15_19_lama'],
                        $rows['Register_20_44_baru'],
                        $rows['Register_20_44_lama'],
                        $rows['Register_45_54_baru'],
                        $rows['Register_45_54_lama'],
                        $rows['Register_55_59_baru'],
                        $rows['Register_55_59_lama'],
                        $rows['Register_60_69_baru'],
                        $rows['Register_60_69_lama'],
                        $rows['Register_70_baru'],
                        $rows['Register_70_lama'],
                    );
                    $no++;
                }
                $retVal['draw'] = $this->input->post('draw');
                $retVal['data'] = $data;
                $retVal['recordsTotal'] = $no;
                $retVal['recordsFiltered'] = $no;
            }else{
                $retVal['draw'] = $this->input->post('draw');
                $retVal['recordsTotal'] = 0;
                $retVal['recordsFiltered'] = 0;
                $retVal['data'] = array();

            }
            echo json_encode($retVal);
        }
        
           
    }

    public function DownloadDataExcel($idPelayanan , $idBulan , $idTahun = ""){
			$namaPoli = "";
			$arrBulan = $this->config->item("month_list");
            $bulan = $arrBulan[$idBulan];
			if($idPelayanan!=0){
				$dataPoli =  $this->pelayanan_model->getDetail($idPelayanan);
				$namaPoli = $dataPoli['txtNama'];
			}else{
                $namaPoli = "Semua";
            }

			$titleRecap = "Laporan Bulanan Data Kesakitan";
            $this->setHeaderExcel($titleRecap , $bulan);
            $this->getDataKesakitan($idPelayanan , $idBulan , $idTahun);
            $filename = "Laporan_LB-1_".$bulan."_".str_replace(" ","_",$namaPoli);
		    $this->getOutput($filename);
	}

    private function getDataKesakitan($idPelayanan , $idBulan , $idTahun){
        $no = 1;
        $this->excel->getActiveSheet()->setCellValue('A6', "Nama Penyakit");
        $this->excel->getActiveSheet()->setCellValue('B6', "Kode ICD-X");
        $this->excel->getActiveSheet()->mergeCells('A6:A8');
        $this->excel->getActiveSheet()->mergeCells('B6:B8');
        $this->excel->getActiveSheet()->setCellValue('C6', "Jumlah Penderita Menurut Golongan Umur");
        $this->excel->getActiveSheet()->mergeCells('C6:AO6');
        $this->excel->getActiveSheet()->setCellValue('C7', "0-7 Hari");
        $this->excel->getActiveSheet()->mergeCells('C7:E7');
        $this->excel->getActiveSheet()->setCellValue('F7', "8-28 Hari");
        $this->excel->getActiveSheet()->mergeCells('F7:H7');
        $this->excel->getActiveSheet()->setCellValue('I7', "28-60 Hari");
        $this->excel->getActiveSheet()->mergeCells('I7:K7');
        $this->excel->getActiveSheet()->setCellValue('L7', "60-1 Tahun");
        $this->excel->getActiveSheet()->mergeCells('L7:N7');
        $this->excel->getActiveSheet()->setCellValue('O7', "1-4 Tahun");
        $this->excel->getActiveSheet()->mergeCells('O7:Q7');
        $this->excel->getActiveSheet()->setCellValue('R7', "5-9 Tahun");
        $this->excel->getActiveSheet()->mergeCells('R7:T7');
        $this->excel->getActiveSheet()->setCellValue('U7', "10-14 Tahun");
        $this->excel->getActiveSheet()->mergeCells('U7:W7');
        $this->excel->getActiveSheet()->setCellValue('X7', "15-19 Tahun");
        $this->excel->getActiveSheet()->mergeCells('X7:Z7');
        $this->excel->getActiveSheet()->setCellValue('AA7', "20-44 Tahun");
        $this->excel->getActiveSheet()->mergeCells('AA7:AC7');
        $this->excel->getActiveSheet()->setCellValue('AD7', "45-54 Tahun");
        $this->excel->getActiveSheet()->mergeCells('AD7:AF7');
        $this->excel->getActiveSheet()->setCellValue('AG7', "55-59 Tahun");
        $this->excel->getActiveSheet()->mergeCells('AG7:AI7');
        $this->excel->getActiveSheet()->setCellValue('AJ7', "60-69 Tahun");
        $this->excel->getActiveSheet()->mergeCells('AJ7:AL7');
        $this->excel->getActiveSheet()->setCellValue('AM7', "> 70 Tahun");
        $this->excel->getActiveSheet()->mergeCells('AM7:AO7');
        
        /// Sub Header
        $this->excel->getActiveSheet()->setCellValue('C8', "B");
        $this->excel->getActiveSheet()->setCellValue('D8', "L");
        $this->excel->getActiveSheet()->setCellValue('E8', "KKL");

        $this->excel->getActiveSheet()->setCellValue('F8', "B");
        $this->excel->getActiveSheet()->setCellValue('G8', "L");
        $this->excel->getActiveSheet()->setCellValue('H8', "KKL");

        $this->excel->getActiveSheet()->setCellValue('I8', "B");
        $this->excel->getActiveSheet()->setCellValue('J8', "L");
        $this->excel->getActiveSheet()->setCellValue('K8', "KKL");

        $this->excel->getActiveSheet()->setCellValue('L8', "B");
        $this->excel->getActiveSheet()->setCellValue('M8', "L");
        $this->excel->getActiveSheet()->setCellValue('N8', "KKL");
        
        $this->excel->getActiveSheet()->setCellValue('O8', "B");
        $this->excel->getActiveSheet()->setCellValue('P8', "L");
        $this->excel->getActiveSheet()->setCellValue('Q8', "KKL");

        $this->excel->getActiveSheet()->setCellValue('R8', "B");
        $this->excel->getActiveSheet()->setCellValue('S8', "L");
        $this->excel->getActiveSheet()->setCellValue('T8', "KKL");

        $this->excel->getActiveSheet()->setCellValue('U8', "B");
        $this->excel->getActiveSheet()->setCellValue('V8', "L");
        $this->excel->getActiveSheet()->setCellValue('W8', "KKL");

        $this->excel->getActiveSheet()->setCellValue('X8', "B");
        $this->excel->getActiveSheet()->setCellValue('Y8', "L");
        $this->excel->getActiveSheet()->setCellValue('Z8', "KKL");

        $this->excel->getActiveSheet()->setCellValue('AA8', "B");
        $this->excel->getActiveSheet()->setCellValue('AB8', "L");
        $this->excel->getActiveSheet()->setCellValue('AC8', "KKL");

        $this->excel->getActiveSheet()->setCellValue('AD8', "B");
        $this->excel->getActiveSheet()->setCellValue('AE8', "L");
        $this->excel->getActiveSheet()->setCellValue('AF8', "KKL");

        $this->excel->getActiveSheet()->setCellValue('AG8', "B");
        $this->excel->getActiveSheet()->setCellValue('AH8', "L");
        $this->excel->getActiveSheet()->setCellValue('AI8', "KKL");

        $this->excel->getActiveSheet()->setCellValue('AJ8', "B");
        $this->excel->getActiveSheet()->setCellValue('AK8', "L");
        $this->excel->getActiveSheet()->setCellValue('AL8', "KKL");

        $this->excel->getActiveSheet()->setCellValue('AM8', "B");
        $this->excel->getActiveSheet()->setCellValue('AN8', "L");
        $this->excel->getActiveSheet()->setCellValue('AO8', "KKL");

        $dataRows = $this->data_penyakit->getDataLB1V2($idPelayanan , $idBulan , $idTahun);
        ////$dataRows = $this->data_penyakit->getDataLB1V2($poli , $bulan , $tahun);

        $indexNo = 8;
        foreach ($dataRows as $rows) {
        $ICDX = $rows['txtCategory'];
            # code...
        $this->excel->getActiveSheet()->setCellValue('A'.($indexNo + $no), $rows['txtIndonesianName']);
        $this->excel->getActiveSheet()->setCellValue('B'.($indexNo + $no), $ICDX);
        $this->excel->getActiveSheet()->setCellValue('C'.($indexNo + $no), $rows['Register_0_7_baru']);
        $this->excel->getActiveSheet()->setCellValue('D'.($indexNo + $no), $rows['Register_0_7_lama']);
        $this->excel->getActiveSheet()->setCellValue('E'.($indexNo + $no), "0");

        $this->excel->getActiveSheet()->setCellValue('F'.($indexNo + $no), $rows['Register_8_14_baru']);
        $this->excel->getActiveSheet()->setCellValue('G'.($indexNo + $no), $rows['Register_8_14_lama']);
        $this->excel->getActiveSheet()->setCellValue('H'.($indexNo + $no), "0");

        $this->excel->getActiveSheet()->setCellValue('I'.($indexNo + $no), $rows['Register_28_60_baru']);
        $this->excel->getActiveSheet()->setCellValue('J'.($indexNo + $no), $rows['Register_28_60_lama']);
        $this->excel->getActiveSheet()->setCellValue('K'.($indexNo + $no), "0");

        $this->excel->getActiveSheet()->setCellValue('L'.($indexNo + $no), $rows['Register_60_365_baru']);
        $this->excel->getActiveSheet()->setCellValue('M'.($indexNo + $no), $rows['Register_60_365_lama']);
        $this->excel->getActiveSheet()->setCellValue('N'.($indexNo + $no), "0");
        
        $this->excel->getActiveSheet()->setCellValue('O'.($indexNo + $no), $rows['Register_1_4_baru']);
        $this->excel->getActiveSheet()->setCellValue('P'.($indexNo + $no), $rows['Register_1_4_lama']);
        $this->excel->getActiveSheet()->setCellValue('Q'.($indexNo + $no), "0");

        $this->excel->getActiveSheet()->setCellValue('R'.($indexNo + $no), $rows['Register_5_9_baru']);
        $this->excel->getActiveSheet()->setCellValue('S'.($indexNo + $no), $rows['Register_5_9_lama']);
        $this->excel->getActiveSheet()->setCellValue('T'.($indexNo + $no), "0");

        $this->excel->getActiveSheet()->setCellValue('U'.($indexNo + $no), $rows['Register_10_14_baru']);
        $this->excel->getActiveSheet()->setCellValue('V'.($indexNo + $no), $rows['Register_10_14_lama']);
        $this->excel->getActiveSheet()->setCellValue('W'.($indexNo + $no), "0");

        $this->excel->getActiveSheet()->setCellValue('X'.($indexNo + $no), $rows['Register_15_19_baru']);
        $this->excel->getActiveSheet()->setCellValue('Y'.($indexNo + $no), $rows['Register_15_19_lama']);
        $this->excel->getActiveSheet()->setCellValue('Z'.($indexNo + $no), "0");

        $this->excel->getActiveSheet()->setCellValue('AA'.($indexNo + $no), $rows['Register_20_44_baru']);
        $this->excel->getActiveSheet()->setCellValue('AB'.($indexNo + $no), $rows['Register_20_44_lama']);
        $this->excel->getActiveSheet()->setCellValue('AC'.($indexNo + $no), "0");

        $this->excel->getActiveSheet()->setCellValue('AD'.($indexNo + $no), $rows['Register_45_54_baru']);
        $this->excel->getActiveSheet()->setCellValue('AE'.($indexNo + $no), $rows['Register_45_54_lama']);
        $this->excel->getActiveSheet()->setCellValue('AF'.($indexNo + $no), "0");

        $this->excel->getActiveSheet()->setCellValue('AG'.($indexNo + $no), $rows['Register_55_59_baru']);
        $this->excel->getActiveSheet()->setCellValue('AH'.($indexNo + $no), $rows['Register_55_59_lama']);
        $this->excel->getActiveSheet()->setCellValue('AI'.($indexNo + $no), "0");

        $this->excel->getActiveSheet()->setCellValue('AJ'.($indexNo + $no), $rows['Register_60_69_baru']);
        $this->excel->getActiveSheet()->setCellValue('AK'.($indexNo + $no), $rows['Register_60_69_lama']);
        $this->excel->getActiveSheet()->setCellValue('AL'.($indexNo + $no), "0");

        $this->excel->getActiveSheet()->setCellValue('AM'.($indexNo + $no), $rows['Register_70_baru']);
        $this->excel->getActiveSheet()->setCellValue('AN'.($indexNo + $no), $rows['Register_70_lama']);
        $this->excel->getActiveSheet()->setCellValue('AO'.($indexNo + $no), "0");

        $no++;
        }
    }

	private function setHeaderExcelLaporan($titleRecap , $bulan){
		
        $this->excel->setActiveSheetIndex(0);
        $this->excel->getActiveSheet()->setTitle('Sheet 1');
        $this->excel->getActiveSheet()->setCellValue('A1', 'Puskesmas '.APP_KELURAHAN);
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(20);
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->mergeCells('A1:N1');
        $this->excel->getActiveSheet()->setCellValue('A2', $titleRecap);
        $this->excel->getActiveSheet()->setCellValue('A3', 'Bulan');
        $this->excel->getActiveSheet()->setCellValue('B3', $bulan);
        
    }

    private function getOutputLaporan( $filename){
        ///
        $filename = date('YmdHis')."-".$filename;
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache

        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');

        $objWriter->save('php://output');
    }
}