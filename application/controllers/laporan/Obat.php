<?php

class Obat extends MY_Controller {

	var $meta_title = "Laporan Obat";
    var $meta_desc = "Laporan Obat";
    var $main_title = "Laporan Obat";
    var $menu_key = "laporan_stok_farmasi";
    var $dtBreadcrumbs = array();  

	public function __construct() {
		parent::__construct();
		$this->load->library('excel');
	}

	public function permintaan_pemakaian() {        
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "menu_key" => $this->menu_key,
			"akses_key" => "is_view",
            "container" => $this->_permintaan_pemakaian(),
            "custom_js" => array(   
            	ASSETS_JS_URL."laporan/obat/permintaan_pemakaian.js"             
            ),
            "custom_css" => array(),
        );  
        $this->_render("default",$dt);             
    }

    private function _permintaan_pemakaian() {        
        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['title'] = $this->meta_title;
        return $this->load->view('laporan/obat/permintaan_pemakaian', $dt, true);
    }

    public function get_permintaan_pemakaian() {
    	$this->load->library('datatables');
   		$get = $this->input->get();

		$periode = explode("-",$get['periode']);
		$periodeYear = $periode[0];
		$periodeMonth = $periode[1];
		$periodeYearBefore = $periodeYear;
		$periodeMonthBefore = $periodeMonth - 1;
		if($periodeMonth==1){
			$periodeYearBefore = $periodeYear - 1;
			$periodeMonthBefore = 12;
		}
		$strPeriodeBeforer = $periodeYearBefore."-".$periodeMonthBefore;
		
   		if (isset($get['draw'])) {
   			if ($get['jenis_obat']) {
   				$this->db->where('o.id_jenis_obat', $get['jenis_obat']);
   			}
   			if ($get['apotik']) {
   				$query = $this->db->select('
					o.id,
					o.nama_obat,
					k.kemasan,
					coalesce((select stok from obat_apotik_periode op where id_apotik="'.$get['apotik'].'" and left(op.periode, 7) = "'.$strPeriodeBeforer.'" and op.id_obat = o.id order by op.periode,op.id desc limit 1), 0) as stok_awal,
					coalesce((select sum(jml) from permintaan_obat_detail pod inner join permintaan_obat po on pod.id_permintaan_obat = po.id where po.id_apotik="'.$get['apotik'].'" and pod.id_obat = o.id and left(po.tgl_permintaan, 7) = "'.$get['periode'].'"), 0) as penerimaan,
					coalesce((select sum(jml) from rekam_medis_obat rmo inner join rekam_medis_detail rmd on rmd.txtIdRekmedDetail = rmo.txtIdRekmedDetail where rmo.id_apotik="'.$get['apotik'].'" and rmo.id_obat=o.id and left(rmd.dtTanggalPemeriksaan, 7) = "'.$get['periode'].'"), 0) as pemakaian,
					0 as permintaan,
					o.stok_optimal
				', false)	
				->join('kemasan k', 'k.id = o.id_kemasan')				
				->get_compiled_select('obat o');
   			} else {
   				$query = $this->db->select('
					o.id,
					o.nama_obat,
					k.kemasan,
					coalesce((select stok from obat_periode op where left(op.periode, 7) = "'.$strPeriodeBeforer.'" and op.id_obat = o.id order by op.periode,op.id desc limit 1), 0) as stok_awal,
					coalesce((select sum(jml) from penerimaan_obat_detail pod inner join penerimaan_obat po on pod.id_penerimaan_obat = po.id where pod.id_obat = o.id and left(po.tgl_penerimaan, 7) = "'.$get['periode'].'"), 0) as penerimaan,
					coalesce((select sum(jml) from pengambilan_obat_detail pod inner join pengambilan_obat po on pod.id_pengambilan_obat = po.id where pod.id_obat = o.id and left(po.tgl_pengambilan, 7) = "'.$get['periode'].'"), 0) as pemakaian,
					coalesce((select sum(jml) from permintaan_obat_detail pod inner join permintaan_obat po on pod.id_permintaan_obat = po.id where pod.id_obat = o.id and left(po.tgl_permintaan, 7) = "'.$get['periode'].'" and po.status = 2), 0) as permintaan,
					o.stok_optimal
				', false)	
				->join('kemasan k', 'k.id = o.id_kemasan')				
				->get_compiled_select('obat o');
   			}
			$response = $this->datatables->collection($query)	        
			->addColumn('persediaan', function($row) {
				return $row->stok_awal + $row->penerimaan;
			})
			->addColumn('sisa_stok', function($row) {
				return ($row->stok_awal + $row->penerimaan) - $row->pemakaian;
			})
			->orderableColumns('nama_obat')
	        ->searchableColumns('nama_obat, kemasan')        
	        ->render();
		} else {
	     	$response = array(
	     		'draw' => $this->input->post('draw'),
	     		'recordsTotal' => 0,
	     		'recordsFiltered' => 0,
	     		'data' => array()
	     	);
	    }
        $this->output->set_content_type('application/json')->set_output(json_encode($response));
    }

    public function excel_permintaan_pemakaian() {
		
		$post = $this->input->post();
		$monthList = $this->config->item("month_list");
		$periode = $post['tahun'] . '-' . $post['bulan'];

		$periodeYear = $post['tahun'];
		$periodeMonth = $post['bulan'];
		$periodeYearBefore = $periodeYear;
		$periodeMonthBefore = $periodeMonth - 1;
		if($periodeMonth==1){
			$periodeYearBefore = $periodeYear - 1;
			$periodeMonthBefore = 12;
		}
		$strPeriodeBeforer = $periodeYearBefore."-".$periodeMonthBefore;
		$periodeMonthInt = (int) $periodeMonth;
		$periodeMonthStr = $monthList[$periodeMonthInt];
		$this->excel->setActiveSheetIndex(0);
		$this->excel->getActiveSheet()->setTitle('Laporan LB-2 '.$periodeMonthStr);
		
		/*if ($post['tgl_awal']) {
			$this->db->where("left(b.dtTanggalKunjungan, 10) >= '".$post['tgl_awal']."'");
		}
		if ($post['tgl_akhir']) {
			$this->db->where("left(b.dtTanggalKunjungan, 10) <= '".$post['tgl_akhir']."'");
		}*/
		$excel = $this->excel->getActiveSheet();
		$row = 2;		
		$excel->setCellValue('A1', 'Nama Obat');
		$excel->setCellValue('B1', 'Kemasan');
		$excel->setCellValue('C1', 'Stok Awal');
		$excel->setCellValue('D1', 'Penerimaan');
		$excel->setCellValue('E1', 'Persediaan');
		$excel->setCellValue('F1', 'Pemakaian');
		$excel->setCellValue('G1', 'Sisa Stok');		

		if ($post['jenis_obat']) {
   				$this->db->where('o.id_jenis_obat', $post['jenis_obat']);
   			}
   			if ($post['apotik']) {
   				$data = $this->db->select('
					o.id,
					o.nama_obat,
					k.kemasan,
					coalesce((select stok from obat_apotik_periode op where id_apotik="'.$get['apotik'].'" and left(op.periode, 7) = "'.$strPeriodeBeforer.'" and op.id_obat = o.id order by op.periode,op.id desc limit 1), 0) as stok_awal,
					coalesce((select sum(jml) from permintaan_obat_detail pod inner join permintaan_obat po on pod.id_permintaan_obat = po.id where po.id_apotik="'.$get['apotik'].'" and pod.id_obat = o.id and left(po.tgl_permintaan, 7) = "'.$periode.'"), 0) as penerimaan,
					coalesce((select sum(jml) from rekam_medis_obat rmo inner join rekam_medis_detail rmd on rmd.txtIdRekmedDetail = rmo.txtIdRekmedDetail where rmo.id_apotik="'.$get['apotik'].'" and rmo.id_obat=o.id and left(rmd.dtTanggalPemeriksaan, 7) = "'.$periode.'"), 0) as pemakaian,
					0 as permintaan,
					o.stok_optimal
				', false)	
				->join('kemasan k', 'k.id = o.id_kemasan')		
				->order_by('nama_obat', 'asc')		
				->get('obat o');
   			} else {
   				$data = $this->db->select('
					o.id,
					o.nama_obat,
					k.kemasan,
					coalesce((select stok from obat_periode op where left(op.periode, 7) = "'.$strPeriodeBeforer.'" and op.id_obat = o.id order by op.periode,op.id desc limit 1), 0) as stok_awal,
					coalesce((select sum(jml) from penerimaan_obat_detail pod inner join penerimaan_obat po on pod.id_penerimaan_obat = po.id where pod.id_obat = o.id and left(po.tgl_penerimaan, 7) = "'.$periode.'"), 0) as penerimaan,
					coalesce((select sum(jml) from pengambilan_obat_detail pod inner join pengambilan_obat po on pod.id_pengambilan_obat = po.id where pod.id_obat = o.id and left(po.tgl_pengambilan, 7) = "'.$periode.'"), 0) as pemakaian,
					coalesce((select sum(jml) from permintaan_obat_detail pod inner join permintaan_obat po on pod.id_permintaan_obat = po.id where pod.id_obat = o.id and left(po.tgl_permintaan, 7) = "'.$periode.'" and po.status = 2), 0) as permintaan,
					o.stok_optimal
				', false)	
				->join('kemasan k', 'k.id = o.id_kemasan')		
				->order_by('nama_obat', 'asc')				
				->get('obat o');
   			}		

		foreach ($data->result() as $obat) {
			$excel->setCellValue('A'.$row, $obat->nama_obat);
			$excel->setCellValue('B'.$row, $obat->kemasan);
			$excel->setCellValue('C'.$row, $obat->stok_awal);
			$excel->setCellValue('D'.$row, $obat->penerimaan);
			$excel->setCellValue('E'.$row, $obat->stok_awal + $obat->penerimaan);
			$excel->setCellValue('F'.$row, $obat->pemakaian);
			$excel->setCellValue('G'.$row, ($obat->stok_awal + $obat->penerimaan) - $obat->pemakaian);
			$row++;
		}
 	
        $filename=date('YmdHis').'-Data-Obat';
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$filename.'_'.$periodeYear.'_'.$periodeMonthStr.'.xls"'); 
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5'); 
        $objWriter->save('php://output');
	}

}
