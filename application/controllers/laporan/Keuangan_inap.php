<?php

class Keuangan_inap extends MY_Controller {

	var $meta_title = "Laporan Keuangan";
    var $meta_desc = "Laporan Keuangan";
    var $main_title = "Laporan Keuangan";
    var $menu_key = "laporan_keuangan";
    var $dtBreadcrumbs = array();  

	public function __construct() {
		parent::__construct();
		$this->load->library('excel');
	}

	public function index() {        
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "menu_key" => $this->menu_key,
			"akses_key" => "is_view",
            "container" => $this->_index(),
            "custom_js" => array(   
            	ASSETS_JS_URL."laporan/keuangan/index.js",
            	ASSETS_JS_URL."laporan/keuangan/index.js"             
            ),
            "custom_css" => array(),
        );  
        $this->_render("default",$dt);             
    }

    private function _index() {        
        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['title'] = $this->meta_title;
        return $this->load->view('laporan/keuangan/index', $dt, true);
    }

    public function transaksi() {        
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "menu_key" => $this->menu_key,
			"akses_key" => "is_view",
            "container" => $this->_transaksi(),
            "custom_js" => array(   
            	ASSETS_JS_URL."laporan/keuangan_inap/transaksi.js"   	          
            ),
            "custom_css" => array(),
        );  
        $this->_render("default",$dt);             
    }

    private function _transaksi() {        
        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['title'] = $this->meta_title;
        return $this->load->view('laporan/keuangan_inap/data_transaksi', $dt, true);
    }

   	public function get_transaksi() {   	
   		$this->load->library('datatables');
   		$get = $this->input->get();
   		if (isset($get['draw'])) {
	        if (isset($get['tgl_awal']) && $get['tgl_awal'] <> '') {
				$this->db->where("e.tgl_bayar >= '".$get['tgl_awal']."'");
			}
			if (isset($get['tgl_akhir']) && $get['tgl_akhir'] <> '') {
				$this->db->where("e.tgl_bayar <= '".$get['tgl_akhir']."'");
			}

			if (isset($get['jamkes']) && $get['jamkes'] <> '') {
				$this->db->where('d.intIdJaminanKesehatan', $get['jamkes']);
			}
	        $this->db->select('e.tgl_bayar as tanggal, c.nama_item, d.txtNamaJaminan, txtNamaPegawai, (a.jasa_sarana+a.jasa_pelayanan) as total')
			->join('register b', 'b.txtIdKunjungan = a.id_kunjungan')		
			->join('item_keuangan c', 'c.id = a.id_item_keuangan')
			->join('jaminan_kesehatan d', 'd.intIdJaminanKesehatan = b.intIdJaminanKesehatan')
			->join('keuangan_rawat_inap e', 'e.id = a.id_keuangan_rawat_inap')
			->join('users_profil f', 'f.intIdUser = e.created_by')
	        ->join('pegawai g', 'g.intIdPegawai = f.intIdPegawai')
			->from('keuangan_rawat_inap_detail a');
	        $query = $this->db->get_compiled_select();       	                           
	        $response = $this->datatables->collection($query)
	        ->orderableColumns('tanggal, nama_item, txtNamaJaminan, txtNamaPegawai, total')
	        ->searchableColumns('tanggal, nama_item, txtNamaJaminan, txtNamaPegawai')        
	        ->render();
	     } else {
	     	$response = array(
	     		'draw' => $this->input->post('draw'),
	     		'recordsTotal' => 0,
	     		'recordsFiltered' => 0,
	     		'data' => array()
	     	);
	     }
        $this->output->set_content_type('application/json')->set_output(json_encode($response));
   	}

	public function excel_transaksi() {	
		$post = $this->input->post();

		$this->excel->setActiveSheetIndex(0);
		$this->excel->getActiveSheet()->setTitle('Data Transaksi');
		
		if ($post['tgl_awal']) {
			$this->db->where("e.tgl_bayar >= '".$post['tgl_awal']."'");
		}
		if ($post['tgl_akhir']) {
			$this->db->where("e.tgl_bayar <= '".$post['tgl_akhir']."'");
		}

		if ($post['jamkes']) {
			$this->db->where('d.intIdJaminanKesehatan', $post['jamkes']);
		}

		$data =  $this->db->select('e.tgl_bayar as tanggal, c.nama_item, d.txtNamaJaminan, txtNamaPegawai, (a.jasa_sarana+a.jasa_pelayanan) as total')
		->join('register b', 'b.txtIdKunjungan = a.id_kunjungan')		
		->join('item_keuangan c', 'c.id = a.id_item_keuangan')
		->join('jaminan_kesehatan d', 'd.intIdJaminanKesehatan = b.intIdJaminanKesehatan')
		->join('keuangan_rawat_inap e', 'e.id = a.id_keuangan_rawat_inap')
		->join('users_profil f', 'f.intIdUser = e.created_by')
        ->join('pegawai g', 'g.intIdPegawai = f.intIdPegawai')
		->get('keuangan_rawat_inap_detail a')->result_array();
		
		$this->excel->getActiveSheet()->fromArray($data);
 	
        $filename=date('YmdHis').'-laporan-keuangan.xls';
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$filename.'"'); 
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5'); 
        $objWriter->save('php://output');
	}

	public function rekap_keuangan_transaksi() {        
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "menu_key" => $this->menu_key,
			"akses_key" => "is_view",
            "container" => $this->_rekap_keuangan_transaksi(),
            "custom_js" => array(   
            	ASSETS_JS_URL."laporan/keuangan_inap/rekap_keuangan_transaksi.js"             
            ),
            "custom_css" => array(),
        );  
        $this->_render("default",$dt);             
    }

    private function _rekap_keuangan_transaksi() {        
        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['title'] = $this->meta_title;
        return $this->load->view('laporan/keuangan_inap/rekap_keuangan_transaksi', $dt, true);
    }

    public function get_rekap_keuangan_transaksi() {   	
   		$this->load->library('datatables');
   		$get = $this->input->get();
   		if (isset($get['draw'])) {
	        if (isset($get['tgl_awal']) && $get['tgl_awal'] <> '') {
				$this->db->where("e.tgl_bayar >= '".$get['tgl_awal']."'");
			}
			if (isset($get['tgl_akhir']) && $get['tgl_akhir'] <> '') {
				$this->db->where("e.tgl_bayar <= '".$get['tgl_akhir']."'");
			}

	        $this->db->select('c.nama_item, sum(a.jasa_sarana + a.jasa_pelayanan) as total')
			->join('register b', 'b.txtIdKunjungan = a.id_kunjungan')		
			->join('item_keuangan c', 'c.id = a.id_item_keuangan')
			->join('jaminan_kesehatan d', 'd.intIdJaminanKesehatan = b.intIdJaminanKesehatan')
			->join('keuangan_rawat_inap e', 'e.id = a.id_keuangan_rawat_inap')
			->group_by('a.id_item_keuangan')
			->from('keuangan_rawat_inap_detail a');
	        $query = $this->db->get_compiled_select();                            
	        $response = $this->datatables->collection($query)
	        ->orderableColumns('nama_item, total')
	        ->searchableColumns('nama_item')        
	        ->render();
	     } else {
	     	$response = array(
	     		'draw' => $this->input->post('draw'),
	     		'recordsTotal' => 0,
	     		'recordsFiltered' => 0,
	     		'data' => array()
	     	);
	     }
        $this->output->set_content_type('application/json')->set_output(json_encode($response));
   	}

	public function excel_rekap_keuangan_transaksi() {
		$post = $this->input->post();

		$this->excel->setActiveSheetIndex(0);
		$this->excel->getActiveSheet()->setTitle('Rekap Keuangan Transaksi');
		
		if (isset($get['tgl_awal']) && $get['tgl_awal'] <> '') {
			$this->db->where("e.tgl_bayar >= '".$get['tgl_awal']."'");
		}
		if (isset($get['tgl_akhir']) && $get['tgl_akhir'] <> '') {
			$this->db->where("e.tgl_bayar <= '".$get['tgl_akhir']."'");
		}

		$data = $this->db->select('c.nama_item, sum(a.jasa_sarana + a.jasa_pelayanan) as total')
		->join('register b', 'b.txtIdKunjungan = a.id_kunjungan')		
		->join('item_keuangan c', 'c.id = a.id_item_keuangan')
		->join('jaminan_kesehatan d', 'd.intIdJaminanKesehatan = b.intIdJaminanKesehatan')
		->join('keuangan_rawat_inap e', 'e.id = a.id_keuangan_rawat_inap')
		->group_by('a.id_item_keuangan')
		->get('keuangan_rawat_inap_detail a')->result_array();
		
		$this->excel->getActiveSheet()->fromArray($data);
 	
        $filename=date('YmdHis').'-rekap-keuangan-transaksi.xls';
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$filename.'"'); 
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5'); 
        $objWriter->save('php://output');
	}

	public function rekap_keuangan_jamkes() {        
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "menu_key" => $this->menu_key,
			"akses_key" => "is_view",
            "container" => $this->_rekap_keuangan_jamkes(),
            "custom_js" => array(   
            	ASSETS_JS_URL."laporan/keuangan_inap/rekap_keuangan_jamkes.js"             
            ),
            "custom_css" => array(),
        );  
        $this->_render("default",$dt);             
    }

    private function _rekap_keuangan_jamkes() {        
        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['title'] = $this->meta_title;
        return $this->load->view('laporan/keuangan_inap/rekap_keuangan_jamkes', $dt, true);
    }

    public function get_rekap_keuangan_jamkes() {   	
   		$this->load->library('datatables');
   		$get = $this->input->get();
   		if (isset($get['draw'])) {
	        if (isset($get['tgl_awal']) && $get['tgl_awal'] <> '') {
				$this->db->where("e.tgl_bayar >= '".$get['tgl_awal']."'");
			}
			if (isset($get['tgl_akhir']) && $get['tgl_akhir'] <> '') {
				$this->db->where("e.tgl_bayar <= '".$get['tgl_akhir']."'");
			}

	        $this->db->select('d.txtNamaJaminan, sum(a.jasa_sarana + a.jasa_pelayanan) as total')
			->join('register b', 'b.txtIdKunjungan = a.id_kunjungan')		
			->join('item_keuangan c', 'c.id = a.id_item_keuangan')
			->join('jaminan_kesehatan d', 'd.intIdJaminanKesehatan = b.intIdJaminanKesehatan')
			->join('keuangan_rawat_inap e', 'e.id = a.id_keuangan_rawat_inap')
			->group_by('b.intIdJaminanKesehatan')
			->from('keuangan_rawat_inap_detail a');
	        $query = $this->db->get_compiled_select();                            
	        $response = $this->datatables->collection($query)
	        ->orderableColumns('txtNamaJaminan, total')
	        ->searchableColumns('txtNamaJaminan')        
	        ->render();
	     } else {
	     	$response = array(
	     		'draw' => $this->input->post('draw'),
	     		'recordsTotal' => 0,
	     		'recordsFiltered' => 0,
	     		'data' => array()
	     	);
	     }
        $this->output->set_content_type('application/json')->set_output(json_encode($response));
   	}

	public function excel_rekap_keuangan_jamkes() {
		$post = $this->input->post();

		$this->excel->setActiveSheetIndex(0);
		$this->excel->getActiveSheet()->setTitle('Rekap Keuangan Jamkes');
		
		if (isset($get['tgl_awal']) && $get['tgl_awal'] <> '') {
			$this->db->where("e.tgl_bayar >= '".$get['tgl_awal']."'");
		}
		if (isset($get['tgl_akhir']) && $get['tgl_akhir'] <> '') {
			$this->db->where("e.tgl_bayar <= '".$get['tgl_akhir']."'");
		}

		$data = $this->db->select('d.txtNamaJaminan, sum(a.jasa_sarana + a.jasa_pelayanan) as total')
		->join('register b', 'b.txtIdKunjungan = a.id_kunjungan')		
		->join('item_keuangan c', 'c.id = a.id_item_keuangan')
		->join('jaminan_kesehatan d', 'd.intIdJaminanKesehatan = b.intIdJaminanKesehatan')
		->join('keuangan_rawat_inap e', 'e.id = a.id_keuangan_rawat_inap')
		->group_by('b.intIdJaminanKesehatan')
		->from('keuangan_rawat_inap_detail a')
		->get()->result_array();
		
		$this->excel->getActiveSheet()->fromArray($data);
 	
        $filename=date('YmdHis').'-rekap-keuangan-jamkes.xls';
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$filename.'"'); 
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5'); 
        $objWriter->save('php://output');
	}

}