<?php

class Keuangan_luar extends MY_Controller {

	var $meta_title = "Laporan Keuangan Lain";
    var $meta_desc = "Laporan Keuangan";
    var $main_title = "Laporan Keuangan Luar";
    var $menu_key = "laporan_keuangan";
    var $dtBreadcrumbs = array();  

	public function __construct() {
		parent::__construct();
		$this->load->library('excel');
	}

	public function index() {        
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "menu_key" => $this->menu_key,
			"akses_key" => "is_view",
            "container" => $this->_index(),
            "custom_js" => array(   
            	ASSETS_JS_URL."laporan/keuangan/index.js",             
            ),
            "custom_css" => array(),
        );  
        $this->_render("default",$dt);             
    }

    private function _index() {        
        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['title'] = $this->meta_title;
        return $this->load->view('laporan/keuangan/index', $dt, true);
    }

    public function transaksi() {        
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "menu_key" => $this->menu_key,
			"akses_key" => "is_view",
            "container" => $this->_transaksi(),
            "custom_js" => array(   
            	ASSETS_JS_URL."laporan/keuangan_luar/transaksi.js"   	          
            ),
            "custom_css" => array(),
        );  
        $this->_render("default",$dt);             
    }

    private function _transaksi() {        
        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['title'] = $this->meta_title;
        return $this->load->view('laporan/keuangan_luar/data_transaksi', $dt, true);
    }

   	public function get_transaksi() {   	
   		$this->load->library('datatables');
   		$get = $this->input->get();
   		if (isset($get['draw'])) {
	        if (isset($get['tgl_awal']) && $get['tgl_awal'] <> '') {
				$this->db->where("left(KD.tanggal, 10) >= '".$get['tgl_awal']."'");
			}
			if (isset($get['tgl_akhir']) && $get['tgl_akhir'] <> '') {
				$this->db->where("left(KD.tanggal, 10) <= '".$get['tgl_akhir']."'");
			}

	        $this->db->select('
            KD.no_bukti,
            left(KD.tanggal, 10) as tanggal, 
            IK.nama_item, 
            P.txtNamaPegawai, 
            (KDD.jasa_sarana+KDD.jasa_pelayanan) as total')		
			->join('item_keuangan IK', 'IK.id = KDD.id_item_keuangan')
			->join('keuangan_diluar KD', 'KD.id = KDD.id_keuangan_diluar')
	        ->join('pegawai P', 'P.intIdPegawai = KD.created_by')
			->from('keuangan_diluar_detail KDD');
	        $query = $this->db->get_compiled_select();                            
	        $response = $this->datatables->collection($query)
	        ->orderableColumns('tanggal, nama_item, txtNamaJaminan, txtNamaPegawai, total')        
	        ->render();
	     } else {
	     	$response = array(
	     		'draw' => $this->input->post('draw'),
	     		'recordsTotal' => 0,
	     		'recordsFiltered' => 0,
	     		'data' => array()
	     	);
	     }
        $this->output->set_content_type('application/json')->set_output(json_encode($response));
   	}

	public function excel_transaksi() {	
		$post = $this->input->post();

		$this->excel->setActiveSheetIndex(0);
		$this->excel->getActiveSheet()->setTitle('Data Transaksi');
		
		if ($post['tgl_awal']) {
			$this->db->where("left(KD.tanggal, 10) >= '".$post['tgl_awal']."'");
		}
		if ($post['tgl_akhir']) {
			$this->db->where("left(KD.tanggal, 10) <= '".$post['tgl_akhir']."'");
		}

		$data = $this->db->select('
            KD.no_bukti,
            left(KD.tanggal, 10) as tanggal, 
            IK.nama_item, 
            P.txtNamaPegawai, 
            (KDD.jasa_sarana+KDD.jasa_pelayanan) as total')		
			->join('item_keuangan IK', 'IK.id = KDD.id_item_keuangan')
			->join('keuangan_diluar KD', 'KD.id = KDD.id_keuangan_diluar')
	        ->join('pegawai P', 'P.intIdPegawai = KD.created_by')
			->from('keuangan_diluar_detail KDD')->get();
		$data = $data->result_array();
		$this->excel->getActiveSheet()->fromArray($data);
 	
        $filename=date('YmdHis').'-laporan-keuangan-luar.xls';
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$filename.'"'); 
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5'); 
        $objWriter->save('php://output');
	}

	public function rekap_keuangan_transaksi() {        
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "menu_key" => $this->menu_key,
			"akses_key" => "is_view",
            "container" => $this->_rekap_keuangan_transaksi(),
            "custom_js" => array(   
            	ASSETS_JS_URL."laporan/keuangan/rekap_keuangan_transaksi.js"             
            ),
            "custom_css" => array(),
        );  
        $this->_render("default",$dt);             
    }

    private function _rekap_keuangan_transaksi() {        
        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['title'] = $this->meta_title;
        return $this->load->view('laporan/keuangan/rekap_keuangan_transaksi', $dt, true);
    }

    public function get_rekap_keuangan_transaksi() {   	
   		$this->load->library('datatables');
   		$get = $this->input->get();
   		if (isset($get['draw'])) {
	        if (isset($get['tgl_awal']) && $get['tgl_awal'] <> '') {
				$this->db->where("left(b.dtTanggalKunjungan, 10) >= '".$get['tgl_awal']."'");
			}
			if (isset($get['tgl_akhir']) && $get['tgl_akhir'] <> '') {
				$this->db->where("left(b.dtTanggalKunjungan, 10) <= '".$get['tgl_akhir']."'");
			}

	        $this->db->select('c.nama_item, sum(a.jasa_sarana + a.jasa_pelayanan) as total')
			->join('register b', 'b.txtIdKunjungan = a.id_kunjungan')		
			->join('item_keuangan c', 'c.id = a.id_item_keuangan')
			->join('jaminan_kesehatan d', 'd.intIdJaminanKesehatan = b.intIdJaminanKesehatan')
			->join('keuangan_rawat_jalan e', 'e.id_kunjungan = a.id_kunjungan')
			->group_by('a.id_item_keuangan')
			->from('keuangan_rawat_jalan_detail a');
	        $query = $this->db->get_compiled_select();                            
	        $response = $this->datatables->collection($query)
	        ->orderableColumns('nama_item, total')
	        ->searchableColumns('nama_item')        
	        ->render();
	     } else {
	     	$response = array(
	     		'draw' => $this->input->post('draw'),
	     		'recordsTotal' => 0,
	     		'recordsFiltered' => 0,
	     		'data' => array()
	     	);
	     }
        $this->output->set_content_type('application/json')->set_output(json_encode($response));
   	}

	public function excel_rekap_keuangan_transaksi() {
		$post = $this->input->post();

		$this->excel->setActiveSheetIndex(0);
		$this->excel->getActiveSheet()->setTitle('Rekap Keuangan Transaksi');
		
		if ($post['tgl_awal']) {
			$this->db->where("left(b.dtTanggalKunjungan, 10) >= '".$post['tgl_awal']."'");
		}
		if ($post['tgl_akhir']) {
			$this->db->where("left(b.dtTanggalKunjungan, 10) <= '".$post['tgl_akhir']."'");
		}

		$data = $this->db->select('c.nama_item, sum(a.jasa_sarana + a.jasa_pelayanan) as total')
		->join('register b', 'b.txtIdKunjungan = a.id_kunjungan')		
		->join('item_keuangan c', 'c.id = a.id_item_keuangan')
		->join('jaminan_kesehatan d', 'd.intIdJaminanKesehatan = b.intIdJaminanKesehatan')
		->join('keuangan_rawat_jalan e', 'e.id_kunjungan = a.id_kunjungan')
		->group_by('a.id_item_keuangan')
		->get('keuangan_rawat_jalan_detail a')->result_array();
		
		$this->excel->getActiveSheet()->fromArray($data);
 	
        $filename=date('YmdHis').'-rekap-keuangan-transaksi.xls';
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$filename.'"'); 
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5'); 
        $objWriter->save('php://output');
	}

	public function rekap_keuangan_jamkes() {        
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "menu_key" => $this->menu_key,
			"akses_key" => "is_view",
            "container" => $this->_rekap_keuangan_jamkes(),
            "custom_js" => array(   
            	ASSETS_JS_URL."laporan/keuangan/rekap_keuangan_jamkes.js"             
            ),
            "custom_css" => array(),
        );  
        $this->_render("default",$dt);             
    }

    private function _rekap_keuangan_jamkes() {        
        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['title'] = $this->meta_title;
        return $this->load->view('laporan/keuangan/rekap_keuangan_jamkes', $dt, true);
    }

    public function get_rekap_keuangan_jamkes() {   	
   		$this->load->library('datatables');
   		$get = $this->input->get();
   		if (isset($get['draw'])) {
	        if (isset($get['tgl_awal']) && $get['tgl_awal'] <> '') {
				$this->db->where("left(b.dtTanggalKunjungan, 10) >= '".$get['tgl_awal']."'");
			}
			if (isset($get['tgl_akhir']) && $get['tgl_akhir'] <> '') {
				$this->db->where("left(b.dtTanggalKunjungan, 10) <= '".$get['tgl_akhir']."'");
			}

	        $this->db->select('d.txtNamaJaminan, sum(a.jasa_sarana + a.jasa_pelayanan) as total')
			->join('register b', 'b.txtIdKunjungan = a.id_kunjungan')		
			->join('item_keuangan c', 'c.id = a.id_item_keuangan')
			->join('jaminan_kesehatan d', 'd.intIdJaminanKesehatan = b.intIdJaminanKesehatan')
			->join('keuangan_rawat_jalan e', 'e.id_kunjungan = a.id_kunjungan')
			->group_by('b.intIdJaminanKesehatan')
			->from('keuangan_rawat_jalan_detail a');
	        $query = $this->db->get_compiled_select();                            
	        $response = $this->datatables->collection($query)
	        ->orderableColumns('txtNamaJaminan, total')
	        ->searchableColumns('txtNamaJaminan')        
	        ->render();
	     } else {
	     	$response = array(
	     		'draw' => $this->input->post('draw'),
	     		'recordsTotal' => 0,
	     		'recordsFiltered' => 0,
	     		'data' => array()
	     	);
	     }
        $this->output->set_content_type('application/json')->set_output(json_encode($response));
   	}

	public function excel_rekap_keuangan_jamkes() {
		$post = $this->input->post();

		$this->excel->setActiveSheetIndex(0);
		$this->excel->getActiveSheet()->setTitle('Rekap Keuangan Jamkes');
		
		if ($post['tgl_awal']) {
			$this->db->where("left(b.dtTanggalKunjungan, 10) >= '".$post['tgl_awal']."'");
		}
		if ($post['tgl_akhir']) {
			$this->db->where("left(b.dtTanggalKunjungan, 10) <= '".$post['tgl_akhir']."'");
		}

		$data = $this->db->select('d.txtNamaJaminan, sum(a.jasa_sarana + a.jasa_pelayanan) as total')
		->join('register b', 'b.txtIdKunjungan = a.id_kunjungan')		
		->join('item_keuangan c', 'c.id = a.id_item_keuangan')
		->join('jaminan_kesehatan d', 'd.intIdJaminanKesehatan = b.intIdJaminanKesehatan')
		->join('keuangan_rawat_jalan e', 'e.id_kunjungan = a.id_kunjungan')
		->group_by('b.intIdJaminanKesehatan')
		->from('keuangan_rawat_jalan_detail a')
		->get()->result_array();
		
		$this->excel->getActiveSheet()->fromArray($data);
 	
        $filename=date('YmdHis').'-rekap-keuangan-jamkes.xls';
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$filename.'"'); 
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5'); 
        $objWriter->save('php://output');
	}

}