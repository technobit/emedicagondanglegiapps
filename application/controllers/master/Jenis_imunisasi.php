<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class jenis_imunisasi extends MY_Controller {

    var $meta_title = "Jenis Imunisasi";
    var $meta_desc = "Jenis Imunisasi";
    var $main_title = "Jenis Imunisasi";
    var $dtBreadcrumbs = array();  

    public function __construct() {
        parent::__construct();
        $this->dtBreadcrumbs = array(
            "Home" =>base_url(),
            "Data Induk" => base_url('jenis_imunisasi'),
            "Data Jenis Imunisasi" => base_url('jenis_imunisasi'),
        );
        $this->load->model('jenis_imunisasi_m');
    }
    public function index() {            
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "container" => $this->_index(),
            "menu_key" => "master_jenis_imunisasi",
			"akses_key" => "is_view",
            "custom_js" => array(),
            "custom_css" => array()            
        );  
        $this->_render("default",$dt);  
    }

    private function _index() {        
        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['title'] = $this->meta_title;
        $dt['rsjenis_imunisasi'] = $this->jenis_imunisasi_m->get();   
        return $this->load->view('master/jenis_imunisasi/index', $dt, true);
    }

    public function create() {
        if ($post = $this->input->post()) {
            $this->validation();
            $this->jenis_imunisasi_m->insert($post);
            $this->redirect->with('successMessage', 'Data jenis_imunisasi berhasil ditambahkan.')->to('master/jenis_imunisasi');
        }
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "menu_key" => "data_jenis_imunisasi",
			"akses_key" => "is_insert",
            "container" => $this->_form(),
            "custom_js" => array(),
            "custom_css" => array()            
        );  
        $this->_render("default",$dt);  
    }

    public function edit($id) {
        $result = $this->find($id);
        if ($post = $this->input->post()) {
            $this->validation();
            $this->jenis_imunisasi_m->update($id, $post);
            $this->redirect->with('successMessage', 'Data jenis_imunisasi berhasil diperbarui.')->to('master/jenis_imunisasi');
        }
        $this->form->setData($result);
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "menu_key" => "data_jenis_imunisasi",
			"akses_key" => "is_update",
            "container" => $this->_form(),
            "custom_js" => array(),
            "custom_css" => array()            
        );  
        $this->_render("default",$dt);  
    }

    private function _form() {
        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['title'] = $this->meta_title;        
        return $this->load->view('master/jenis_imunisasi/form', $dt, true);
    }

    public function delete($id) {
        $result = $this->find($id);
        $this->jenis_imunisasi_m->delete($id);
        $this->redirect->with('successMessage', 'Data jenis_imunisasi berhasil dihapus.')->to('master/jenis_imunisasi');
    }

    private function find($id) {
        $result = $this->jenis_imunisasi_m->find($id);
        if (!$result) {
            $this->redirect->with('errorMessage', 'Data keamsan tidak ditemukan.')->back();
        } else {
            return $result;
        }
    }

    private function validation() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('jenis_imunisasi', 'jenis_imunisasi', 'required');
        if (!$this->form_validation->run()) {
            $this->redirect->withInput()->withValidation()->back();
        }        
    }

}