<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kamar extends MY_Controller {
    
    var $meta_title = "Data Kamar";
    var $meta_desc = "Kelas Kamar";
	var $main_title = "Kamar";
    var $base_url = "";
    var $main_model = "";
    var $js_folder = "";
	function __construct(){
        parent::__construct();
		
		$this->load->model("kamar_model" );
        $this->base_url = $this->base_url_site."master/kamar/";
        $this->js_folder = ASSETS_JS_URL."master_data/kamar/";
    }
    
    /// Data Tindakan
	public function index()
	{
        $menu = "K01";
		$this->mode = "view";
		$dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "menu_key" => "master_kelas",
			"akses_key" => "is_view",	
			"container" => $this->_content(),
			"custom_js" => array(
				ASSETS_JS_URL."master_data/kamar/content_kamar.js",
			),
            "custom_css" => array(
				
			),
		);	
		$this->_render("default",$dt);	
	}
    
    public function form($id=""){
		$menu = "K02";
		$this->mode = "insert";
		$dt = array(
            "title" => $this->meta_title,
			"menu_key" => "master_kelas",
			"akses_key" => "is_insert",	
            "description" => $this->meta_desc,
			"container" => $this->_form($id),
			"custom_js" => array(
				ASSETS_URL."plugins/select2/select2.js",
				ASSETS_URL."plugins/validate/jquery.validate_1.11.1.min.js",
				ASSETS_JS_URL."master_data/kamar/form_kamar.js",
			),
            "custom_css" => array(
				ASSETS_URL."plugins/select2/select2.css",
			),
		);	
		$this->_render("default",$dt);
	}
	
	private function _content(){
		$dt = array();
		$breadcrumbs = array(
				"Home" =>$this->base_url_site,
				"Master Data" => $this->base_url,
				"Data Kamar" => "#",
        );
		$dataKelas = $this->kamar_model->getDataListKelas();
		$arrKelas = array();
		foreach($dataKelas as $rowKelas){
			$arrKelas[$rowKelas['intIdKelasKamar']] = $rowKelas['txtKelasKamar'];
		}

		$dt['breadcrumbs'] = $this->setBreadcrumbs($breadcrumbs);
		$dt['title'] = $this->meta_title;
		$dt['intIdKelasKamar'] = $this->form_builder->inputDropdown("Kelas" , "intIdKelasKamar" ,"", $arrKelas);
		$dt['link_add'] = $this->base_url."form/";
		$ret = $this->load->view("master/kamar/content" , $dt, true);
		return $ret;
	}
	
	private function _form($id){
		$dt = array();
		$mode = "insert";
		$dataDetail = array();
		if(!empty($id)){
			$mode = "edit";
			$dataDetail = $this->kamar_model->getDetailKamar($id);
		}
		
		$arrForm = array("intIdKamar" ,
                         "intIdKelasKamar" ,
                         "txtKamar",
						 "txtKeterangan",
						 "intKapasitasKamar",
						 "bitStatusKamar",
						 );
		foreach($arrForm as $frmData){
			$$frmData = !empty($dataDetail[$frmData]) ? $dataDetail[$frmData] : "";
		}
        $dataKelas = $this->kamar_model->getDataListKelas();
		$arrKelas = array();
		foreach($dataKelas as $rowKelas){
			$arrKelas[$rowKelas['intIdKelasKamar']] = $rowKelas['txtKelasKamar'];
		}

		$dt['intIdKamar'] = $this->form_builder->inputHidden("intIdKamar" , $intIdKamar);
		$dt['intIdKelasKamar'] = $this->form_builder->inputDropdown("Kelas" , "intIdKelasKamar" ,$intIdKelasKamar, $arrKelas);
		$dt['txtKamar'] = $this->form_builder->inputText("Kamar" , "txtKamar" , $txtKamar , "col-sm-3");
		$dt['txtKeterangan'] = $this->form_builder->inputTextArea("Keterangan" , "txtKeterangan" , $txtKeterangan , "col-sm-3");
		$dt['intKapasitasKamar'] = $this->form_builder->inputText("Kapasitas" , "intKapasitasKamar" , $intKapasitasKamar , "col-sm-3");
		$dt['bitStatusKamar'] = $this->form_builder->inputDropdown("Status" , "bitStatusKamar" ,$bitStatusKamar, $this->config->item("status_list"));
		
		$breadcrumbs = array(
				"Home" =>$this->base_url_site,
				"Master Data" => $this->base_url,
				"Data Kamar" => $this->base_url,
                "Form Input Kamar" => $this->base_url,
        );
		
		$dt['breadcrumbs'] = $this->setBreadcrumbs($breadcrumbs);
		$dt['title'] = $this->meta_title;
		$dt['link_index'] = $this->base_url;
		$ret = $this->load->view("master/kamar/form" , $dt, true);
		return $ret;
	}
	
	/// Dependency Public Function
	public function delete(){
		if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";
        }
		$status = false;
		$html = "";
		$id = $this->input->post("id");
		if(!empty($id)){
			$resVal = $this->kamar_model->deleteKamar($id);
		}
		$retVal['status'] = $resVal['status'];
		$retVal['message'] = $resVal['error_stat'];
		////$this->session->set_flashdata("alert_message",$retVal);
		$this->setJsonOutput($retVal);
	}
	
	public function simpanData(){
		if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";die;
        }
		
		$status = false;
		$html = "";
		$id = $this->input->post("intIdKamar");
		$arrInsert = array(
						 "intIdKelasKamar" =>$this->input->post("intIdKelasKamar"),
						 "txtKamar"=>$this->input->post("txtKamar"),
						 "txtKeterangan"=>$this->input->post("txtKeterangan"),
						 "intKapasitasKamar"=>$this->input->post("intKapasitasKamar"),
						 "bitStatusKamar"=>$this->input->post("bitStatusKamar"),
						 );
		if(!empty($id)){
			$dt['dtLastUpdate'] = date("Y-m-d H:i:s");
		}else{
			$dt['dtCreatedDate'] = date("Y-m-d H:i:s");
			$dt['dtLastUpdate'] = date("Y-m-d H:i:s");
			
		}
		$resVal = $this->kamar_model->saveDataKamar($arrInsert , $id);
		
		$retVal['status'] = $resVal['status'];
		$retVal['message'] = $resVal['error_stat'];
		$this->setJsonOutput($retVal);
	}   

	public function getDataKamar(){
		if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";die;
        }

		$intIdKelasKamar = $this->input->post("intIdKelasKamar");
		$dataKamar = $this->kamar_model->getDataKamar($intIdKelasKamar);
		
		$retVal = array();
		$retVal['data'] = array();

		foreach($dataKamar as $rowKamar){
			$id = $rowKamar['intIdKamar'];
			$link_edit = $this->base_url."form/".$rowKamar['intIdKamar'];
			$btnDetail = '<a class="btn btn-primary btn-flat" href="'.$link_edit.'"><i class="fa fa-edit"></i> Edit</a>';
			$btnDelete = '<button class="btn btn-warning btn-flat" onclick="hapusData('.$id.')"><i class="fa fa-trash"></i> Hapus</button>';
			$btnAksi = $btnDetail.$btnDelete;
			$statusKamar = $rowKamar['bitStatusKamar']==1 ? "Aktif" : "Non Aktif";
			$arrData = array(
				$rowKamar['txtKamar'],
				$rowKamar['txtKelasKamar'],
				$rowKamar['txtKeterangan'],
				$rowKamar['intKapasitasKamar'],
				$statusKamar,
				$btnAksi
			);
			$retVal['data'][] = $arrData;
		}
		
		$this->setJsonOutput($retVal);
	}
}