<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pelayanan extends MY_Controller {
    
    var $meta_title = "Data Pelayanan";
    var $meta_desc = "Data Pelayanan";
	var $main_title = "Data Pelayanan";
    var $base_url = "";
	var $upload_dir = "";
	var $upload_url = "";
	var $menu_key = "master_pelayanan";
	var $limit = "20";
	var $arr_level_user = "";
    var $menu = "K01";
    var $mode = "";
	function __construct(){
        parent::__construct();
		$this->load->model("Pelayanan_model" , "pelayanan");
		$this->load->model("jenis_pelayanan_model" 	);
        $this->base_url = $this->base_url_site."data-pelayanan/";
    }
    
  
    /// Data ICD 10
	public function index()
	{
		
        $menu = "K01";
		$this->mode = "view";
		$dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
			"container" => $this->_content(),
			"menu_key" => $this->menu_key,
			"akses_key" => "is_view",
			"custom_js" => array(
				ASSETS_JS_URL."master_data/pelayanan/content.js",
			),
            "custom_css" => array(
				
			),
		);	
		$this->_render("default",$dt);	
	}
    
    public function form($id=""){
		
		$menu = "K02";
		$this->mode = "insert";
		$akses_key = !empty($id) ? "is_update" : "is_insert";
		$dt = array(
            "title" => $this->meta_title,			
            "description" => $this->meta_desc,
			"container" => $this->_form($id),
			"menu_key" => $this->menu_key,
			"akses_key" => $akses_key,
			"custom_js" => array(
				ASSETS_URL."plugins/select2/select2.js",
				ASSETS_URL."plugins/validate/jquery.validate_1.11.1.min.js",
				ASSETS_JS_URL."master_data/pelayanan/form.js",
			),
            "custom_css" => array(
				ASSETS_URL."plugins/select2/select2.css",
			),
		);	
		$this->_render("default",$dt);
	}
	
	private function _content(){
		$dt = array();
		$breadcrumbs = array(
				"Home" =>$this->base_url_site,
				"Master Data" => $this->base_url,
				"Data Pelayanan" => "#",
        );
		
		$dt['breadcrumbs'] = $this->setBreadcrumbs($breadcrumbs);
		$dt['title'] = $this->meta_title;
		$dt['frm_search'] = $this->form_builder->inputText("Nama / Singkatan" , "searchText","" ,"col-sm-3" , array("class"=> "form-control"));
		$dt['link_add'] = $this->base_url."tambah-data/";
		$dt['link_add_polindes'] = $this->base_url."tambah-polindes/";
		
		$ret = $this->load->view("master/pelayanan/content" , $dt, true);
		return $ret;
	}
	
	private function _form($id){
		$dt = array();
		$mode = "insert";
		$pemeriksaan = array();
		if(!empty($id)){
			$mode = "edit";
			$pelayanan = $this->pelayanan->getDetail($id);
		}
		
		$arrForm = array("intIdPelayanan" , "intIdJenisPelayanan" , "txtNama", "txtSingkat","intIdParentPelayanan", "dtCreatedDate", "dtLastUpdate", "intRuang", "intTypeForm", "bitStatus" , "txtKodePelayananBPJS");
		foreach($arrForm as $frmData){
			$$frmData = !empty($pelayanan[$frmData]) ? $pelayanan[$frmData] : "";
		}
		
		$jenisPelayanan = $this->jenis_pelayanan_model->getListData();
		$arrJenis = array();
		foreach($jenisPelayanan as $rowJenis){
			$arrJenis[$rowJenis['intIdJenisPelayanan']] = $rowJenis['txtJenisPelayanan'];
		}
		
		$dt['frmIdPelayanan'] = $this->form_builder->inputHidden("intIdPelayanan", $intIdPelayanan);
		///$dt['frmIdJenisPelayanan'] = $this->form_builder->inputText("Jenis Pelayanan" , "intIdJenisPelayanan" , $intIdJenisPelayanan, "col-sm-3");
		$dt['frmIdJenisPelayanan'] = $this->form_builder->inputDropdown("Jenis Pelayanan","intIdJenisPelayanan" , $intIdJenisPelayanan , $arrJenis);
		$dt['frmtxtNama'] = $this->form_builder->inputText("Nama" , "txtNama" , $txtNama,"col-sm-3");
		$dt['txtSingkat'] = $this->form_builder->inputText("Singkatan" , "txtSingkatan" , $txtSingkat , "col-sm-3");
		$dt['intRuang'] = $this->form_builder->inputText("Ruangan" , "intRuang" , $intRuang , "col-sm-3");
		$dt['txtIdParentPelayanan'] = $this->form_builder->inputText("Id Parent Pelayanan" , "intIdParentPelayanan" , $intIdParentPelayanan , "col-sm-3");
		$dt['frm_intTypeForm'] = $this->form_builder->inputHidden("intTypeForm" , $intTypeForm);
		$dt['frm_created'] = $this->form_builder->inputHidden("dtCreatedDate" , $dtCreatedDate);
		$dt['frm_tgl'] = $this->form_builder->inputHidden("date" , date("Y-m-d h:i:s"));
		$dt['frmStatusPemeriksaan'] = $this->form_builder->inputDropdown("Status Pelayanan" , "cmbStatus" ,$bitStatus, $this->config->item("status_list"));


		$arrPoliBPJS = $this->getPoliBPJS();
		$dt['selectKodeBPJS'] = $this->form_builder->inputDropdown("Kode Ruangan BPJS" , "selectKodeBPJS" ,$txtKodePelayananBPJS, $arrPoliBPJS);
		
		$breadcrumbs = array(
				"Home" =>$this->base_url_site,
				"Pelayanan" => "#",
        );
		
		$dt['breadcrumbs'] = $this->setBreadcrumbs($breadcrumbs);
		$dt['title'] = $this->meta_title;
		$dt['link_index'] = $this->base_url;
		$ret = $this->load->view("master/pelayanan/form" , $dt, true);
		return $ret;
	}

	private function getPoliBPJS(){
		$this->load->library("bridging_api");
		$arrPoliBPJS = array("0"=>"-Pilih Ruangan BPJS-");
		$appLink = "v1/poli/fktp/0/50";
		$response = $this->bridging_api->sendGetRequest($appLink);
		if($response['status']==true){
            $data = $response['data'];
            if($data['count']>0){
                foreach($data['list'] as $rowPoli){
                    $arrPoliBPJS[$rowPoli['kdPoli']] = $rowPoli['kdPoli'].' - '.$rowPoli['nmPoli'];
                }
            }
            
        }
		return $arrPoliBPJS;
	}
	
	/// Dependency Public Function
	public function hapusDataPelayanan(){
		if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";
        }
		$status = false;
		$html = "";
		$id = $this->input->post("id");
		if(!empty($id)){
			$resVal = $this->pelayanan->delete($id);
		}
		$retVal['status'] = $resVal['status'];
		$retVal['message'] = $resVal['error_stat'];
		$this->session->set_flashdata("alert_message",$retVal);
		$this->setJsonOutput($retVal);
	}
	
	public function simpanDataPelayanan(){
		if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";
        }
		
		$status = false;
		$html = "";
		$IdPelayanan = $this->input->post("intIdPelayanan");
		$arrInsert = array(
						 "intIdJenisPelayanan" =>$this->input->post("intIdJenisPelayanan"),
						 "txtNama"=>$this->input->post("txtNama"),
						 "txtSingkat"=>$this->input->post("txtSingkatan"),
						 "intIdParentPelayanan"=>$this->input->post("intIdParentPelayanan"),
						 "dtCreatedDate"=>date("Y-m-d H:i:s"),
						 "dtLastUpdate"=>date("Y-m-d H:i:s"),
						 "intRuang" => $this->input->post("intRuang"),
						 "intTypeForm"=>$this->input->post("intTypeForm"),
						 "bitStatus"=>$this->input->post("cmbStatus"),
						 "txtKodePelayananBPJS"=>$this->input->post("selectKodeBPJS"),
						 );
		$arrUpdate = array(
						 "intIdJenisPelayanan" =>$this->input->post("intIdJenisPelayanan"),
						 "txtNama"=>$this->input->post("txtNama"),
						 "txtSingkat"=>$this->input->post("txtSingkatan"),
						 "intIdParentPelayanan"=>$this->input->post("intIdParentPelayanan"),
						 "dtLastUpdate"=>date("Y-m-d H:i:s"),
						 "intRuang" => $this->input->post("intRuang"),
						 "intTypeForm"=>$this->input->post("intTypeForm"),
						 "bitStatus"=>$this->input->post("cmbStatus"),
						 "txtKodePelayananBPJS"=>$this->input->post("selectKodeBPJS"),
						 );
		
		if(!empty($IdPelayanan)){
			$resVal = $this->pelayanan->update($arrUpdate , $IdPelayanan);
		}else{
			$resVal = $this->pelayanan->saveData($arrInsert);
		}
		
		$retVal['status'] = $resVal['status'];
		$retVal['message'] = $resVal['error_stat'];
		$this->session->set_flashdata("alert_message",$retVal);
		$this->setJsonOutput($retVal);
	}
	
	public function getData($search="" , $level=""){
		if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";
        }
		
		$status = false;
		$html = "";
		$post = $_POST;
		$length =  $this->input->post("length");
		$start = $this->input->post("start");
		
		$offset = ($start - 0) * $this->limit;
		$data_results = $this->pelayanan->getData($start , $this->limit , $search);
		$count_data = $this->pelayanan->getCountData($search);
		
		if($count_data > 0) {
			$retVal['draw'] = $this->input->post('draw');
			$retVal['recordsTotal'] = $count_data;
			$retVal['recordsFiltered'] = $count_data;
			$retVal['data'] = array();
			foreach($data_results as $row){
				$btnEdit = "<a href='".$this->base_url."edit/".$row['intIdPelayanan']."' class='btn btn-info btn-flat'><i class=\"fa fa-pencil\"></i> Edit</a>";
				$btnDelete = "<button onclick='hapusData(".$row['intIdPelayanan'].")' type='button' class='btn btn-danger btn-flat'><i class=\"fa fa-trash\"></i> Hapus</button>";
				$btnAksi = $btnEdit.$btnDelete;
				$status = $row['bitStatus']==1 ? "Aktif" : "Non Aktif";
				$retVal['data'][] = array(
										  $row['txtNama'],
										  $row['txtJenisPelayanan'],
										  $row['txtSingkat'],
										  $status,
										  $btnAksi
									);
			}
		}else{
			$retVal['draw'] = $this->input->post('draw');
			$retVal['recordsTotal'] = 0;
			$retVal['recordsFiltered'] = 0;
			$retVal['data'] = array();
		}
		$this->setJsonOutput($retVal);
	}

	public function getDataPolindes(){
		$this->isAjaxRequest();


		$retVal['data'] = array();
		$this->setJsonOutput($retVal);
	}
	
	public function getListPelayanan(){
        if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";
        }
		
		$status = false;
		$data = array();
        $jenisFasilitas = $this->input->post('jenisFasilitas');
		if ($jenisFasilitas =='POLI') {
			$rowArray = $this->pelayanan->getPelayananByJenisFasilitas('PO');
			
			
		}else {
		$rowArray = $this->pelayanan->getPelayananByJenisFasilitas($jenisFasilitas);
			
		}
		if(!empty($rowArray)){
			$status = true;
			$data = $rowArray;
		}
		
		$retVal = array();
		$retVal["status"] = $status;
		$retVal['data'] = $data;
		$this->setJsonOutput($retVal); 
    }
		
}
