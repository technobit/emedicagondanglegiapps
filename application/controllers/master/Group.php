<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Group extends MY_Controller {
    
    var $meta_title = "Group";
    var $meta_desc = "Handphone";
	var $main_title = "Handphone";
    var $base_url = "";
	var $upload_dir = "";
	var $upload_url = "";
	var $menu_key = "master_grup";
	var $limit = "20";
	var $arr_level_user = "";
    var $menu = "K01";
    var $mode = "";
	
	function __construct(){
        parent::__construct();
		$this->load->model("group_model");
		$this->load->model("pegawai_model");
        $this->base_url = $this->base_url_site."data-group/";
    }
      
	public function index()
	{
		$this->mode = "view";
		$dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
			"container" => $this->_content(),
			"menu_key" => $this->menu_key,
			"akses_key" => "is_view",
			"custom_js" => array(
				ASSETS_JS_URL."master_data/group/content.js",
			),
            "custom_css" => array(
				
			),
		);	
		$this->_render("default",$dt);	
	}
    
    public function form($id=""){
		
		$this->mode = "insert";
		$akses_key = !empty($id) ? "is_update" : "is_insert";
		$dt = array(
            "title" => $this->meta_title,
			"menu_key" => $this->menu_key,
			"akses_key" => "is_view",
            "description" => $this->meta_desc,
			"container" => $this->_form($id),
			"custom_js" => array(
				ASSETS_URL."plugins/select2/select2.js",
				ASSETS_URL."plugins/validate/jquery.validate_1.11.1.min.js",
				ASSETS_JS_URL."master_data/group/form.js",
			),
            "custom_css" => array(
				ASSETS_URL."plugins/select2/select2.css",
			),
		);	
		$this->_render("default",$dt);
	}
	
	private function _content(){
		$dt = array();
		$breadcrumbs = array(
				"Home" =>$this->base_url_site,
				"Data Group" => "#",
        );

		$group_type = $this->config->item("group_type");
		$status_list = $this->config->item("status_list");
		
		$dt['breadcrumbs'] = $this->setBreadcrumbs($breadcrumbs);
		$dt['title'] = $this->meta_title;
		$dt['link_add'] = $this->base_url."tambah-data/";
		$dt['intIdGroup'] = form_hidden("intIdGroup");
		$dt['txtGroupName'] = form_input("txtGroupName" , "" , 'class="form-control" id="txtGroupName"');
		$dt['bitGroupStatus'] = form_dropdown("bitGroupStatus" , $status_list,""  , 'class="form-control" id="bitGroupStatus"');
		$ret = $this->load->view("master/group/content" , $dt, true);
		return $ret;
	}
	
	private function _form($id){
		$dt = array();
		$mode = "insert";
		$detailData = $this->group_model->getDetail($id);
		if(empty($detailData)){
			show_404();
		}

		$status_list = $this->config->item("status_list");
		$dt['intIdGroup'] = form_hidden("intIdGroup" , $id);
		$dt['txtGroupName'] = form_input("txtGroupName" , $detailData['txtGroupName'] , 'class="form-control" id="txtGroupName" readonly="readonly"');
		$dt['bitGroupStatus'] = form_input("bitGroupStatus" , $status_list[$detailData['bitGroupStatus']] , 'class="form-control" id="bitGroupStatus" readonly="readonly"');
		$breadcrumbs = array(
				"Home" =>$this->base_url_site,
				"Data Group" => "#",
        );
		
		$dt['breadcrumbs'] = $this->setBreadcrumbs($breadcrumbs);
		$dt['title'] = $this->meta_title;
		$dt['nameGroup'] = $detailData['txtGroupName'];
		$dt['link_index'] = $this->base_url;
		$ret = $this->load->view("master/group/form" , $dt, true);
		return $ret;
	}

	public function formInputNoHandphone(){
		if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";
        }

		$intIdGroup = $this->input->post("intIdGroup");
		$listPegawai = $this->pegawai_model->getListPegawai();
		$arrPegawai = array();

		foreach ($listPegawai as $rowPegawai) {
			# code...
			$arrPegawai[$rowPegawai['intIdPegawai']] = $rowPegawai['txtNamaPegawai'];
		}

		$dt['intIdGroup'] = $this->form_builder->inputHidden("intIdGroup" , $intIdGroup);
		$dt['frmPegawai'] = $this->form_builder->inputDropdown("Nama Pegawai" , "intIdPegawai" , "" , $arrPegawai);
		$retVal = array();
		$retVal['resHtml'] = $this->load->view("master/group/form_nama" , $dt, true);
		echo json_encode($retVal);
	}

	/// Dependency Public Function
	public function getDetail(){
		if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";
        }
		$id = $this->input->post("id");
		$data = $this->group_model->getDetail($id);
		echo json_encode($data);
	}

	public function hapusData(){
		if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";
        }
		$status = false;
		$html = "";
		$id = $this->input->post("id");
		if(!empty($id)){
			$resVal = $this->group_model->delete($id);
		}
		$retVal['status'] = $resVal['status'];
		$retVal['message'] = $resVal['error_stat'];
		$this->session->set_flashdata("alert_message",$retVal);
		$this->setJsonOutput($retVal);
	}

	public function hapusPegawai(){
		if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";
        }
		$status = false;
		$html = "";
		$id = $this->input->post("id");
		$intIdGroup = $this->input->post("intIdGroup");
		if(!empty($id)){
			$resVal = $this->group_model->deletePegawai($id , $intIdGroup);
		}
		$retVal['status'] = $resVal['status'];
		$retVal['message'] = $resVal['error_stat'];
		///$this->session->set_flashdata("alert_message",$retVal);
		///$this->setJsonOutput($retVal);
		echo json_encode($retVal);
	}
	
	public function simpanData(){
		if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";
        }

		$status = false;
		$html = "";
		$id = $this->input->post("intIdGroup");
		$arrInsert = array(
						 "txtGroupName" =>$this->input->post("txtGroupName"),
						 "bitGroupStatus"=>$this->input->post("bitGroupStatus"),
						 );
		$resVal = $this->group_model->saveData($arrInsert , $id);
		$retVal['status'] = $resVal['status'];
		$retVal['message'] = $resVal['error_stat'];		
		echo json_encode($retVal);
	}

	public function simpanPegawai(){
		if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";
        }

		$status = false;
		$html = "";		
		$intIdPegawai = $this->input->post("intIdPegawai");
		$intIdGroup = $this->input->post("intIdGroup");
		$statusPegawai = $this->group_model->checkDataPasien($intIdPegawai , $intIdGroup);
		if($statusPegawai==true){
			$arrInsert = array(
							"intIdPegawai" =>$this->input->post("intIdPegawai"),
							"intIdGroup"=>$this->input->post("intIdGroup"),
							);
			$resVal = $this->group_model->saveDataPegawai($arrInsert);
			$retVal['status'] = $resVal['status'];
			$retVal['message'] = $resVal['error_stat'];
		}else{
			$retVal['status'] = false;
			$retVal['message'] = "Data Sudah Ada";
		}
		echo json_encode($retVal);
	}

	public function getDataPegawai(){
		if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";
        }

		$intIdGroup = $this->input->post("intIdGroup");
		$dataPasien = $this->group_model->getDataPasien($intIdGroup);
		///echopre($dataPasien);
		if(!empty($dataPasien)){
			$retVal['data'] = array();
			foreach($dataPasien as $row){
				$id = $row['intIdPegawai'];				
				$btnDelete = "<button onclick='hapusData(".$id." , ".$intIdGroup.")' type='button' class='btn btn-danger btn-flat'><i class=\"fa fa-trash\"></i> Hapus</button>";
				$btnAksi = $btnDelete;
				$retVal['data'][] = array($row['txtNamaPegawai'],
										  $row['txtNoHandphone'],
										  $btnAksi
									);
			}
		}else{
			$retVal['data'] = array();
		}
		$this->setJsonOutput($retVal);
	}
	
	public function getData(){
		if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";
        }
		$group_status = $this->config->item("group_type");
		$status_list = $this->config->item("status_list");
		$status = false;
		$html = "";
		$post = $_POST;
		$data_results = $this->group_model->getData();
		///echopre($data_results);
		$count_data = count($data_results);
		if($count_data > 0) {			
			$retVal['data'] = array();
			foreach($data_results as $row){
				$id = $row['intIdGroup'];
				$groupStatus = $status_list[$row['bitGroupStatus']];
				$btnEdit = "<button onclick='detailData(".$id.")' type='button' class='btn btn-info btn-flat'><i class=\"fa fa-pencil\"></i> Detail</button>";
				$btnDetail = "<a href='".$this->base_url."edit/".$id."' class='btn btn-success btn-flat'><i class=\"fa fa-pencil\"></i> Edit</a>";
				$btnDelete = "<button onclick='hapusData(".$id.")' type='button' class='btn btn-danger btn-flat'><i class=\"fa fa-trash\"></i> Hapus</button>";
				$btnAksi = $btnEdit.$btnDetail.$btnDelete;
				$retVal['data'][] = array($id,
										  $row['txtGroupName'],
										  $row['jumlah_pegawai'],
										  $groupStatus,
										  $btnAksi
									);
			}
		}else{
			$retVal['data'] = array();
		}
		$this->setJsonOutput($retVal);
	}

	
}
