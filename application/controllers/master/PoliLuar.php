<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PoliLuar extends MY_Controller {

    var $meta_title = "Master Data - Ruangan Luar";
    var $meta_desc = "Master Data - Ruangan Luar";
	var $main_title = "Master Data - Ruangan Luar";
    var $base_url = "";
    var $menu = "D01";
    var $arrBreadcrumbs = array();
    public function __construct(){
        parent::__construct();
        $this->load->model("PoliLuar_model" , "poli_model");
        $this->base_url = $this->base_url_site."data-poli-luar";
        $this->arrBreadcrumbs = array(
            "Master Data" => $this->base_url,
            "Ruangan Luar" => $this->base_url,
        );
    } 
    
	public function index()
	{
        if(!empty($this->input->post())){
            $post = $this->input->post();
            $primaryID = $post['intIDPoliLuar'];
            
            $arrDataInput = array(
                "txtPoliLuar" => $post['txtPoliLuar'],
                "txtKeterangan" => $post['txtKeterangan'],
                "bitStatus" => $post['bitStatus'],
            );
            $resVal = $this->poli_model->saveUpdate($arrDataInput , $primaryID);

            $status = $resVal['status'];
            $message = $resVal['message'];
            $arrMessage = array(
                "status" => $status ,
                "message" => $message ,  
            );
            $this->session->set_flashdata('insert_alert' , $arrMessage);
            redirect($this->base_url);
        }

		$dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
			"container" => $this->_home(),
            "menu_key" => "master_poli_luar",
			"akses_key" => "is_view",	
            "main_title" => $this->main_title,
            "breadcrumbs" => $this->arrBreadcrumbs,
			"custom_js" => array(
				ASSETS_JS_URL."master_data/poli_luar/index.js",
			),
            "custom_css" => array(
				
			),
		);	
		$this->_render("default",$dt);	
	}
	
    private function _home(){
        $dt = array();
        ///$data = $this->base_model->executeSP("imb_mPoliLuar_Retrieve");
        $data = $this->poli_model->getData();
        $this->form_builder->form_type = "inline";
        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->arrBreadcrumbs);
        $dt['linkForm'] = $this->base_url;
        $dt['intIDPoliLuar'] = $this->form_builder->inputHidden("intIDPoliLuar" , "");
        $dt['txtPoliLuar'] = $this->form_builder->inputText("Poli Luar" , "txtPoliLuar" , "" , "col-sm-3" , array("required" => "required"));
        $dt['txtKeterangan'] = $this->form_builder->inputTextArea("Keterangan" , "txtKeterangan" , "" , "col-sm-3");
        $dt['bitStatus'] = $this->form_builder->inputDropdown("Status" , "bitStatus" , array() , $this->config->item("status_list"));
        $dt['data'] = $data;
        $ret = $this->load->view("master/poli_luar/content" , $dt , true);
        return $ret;
    }

    public function detail(){
        if(!$this->input->is_ajax_request()){
            echo "Ilegal";die;
        }
        $post = $this->input->post();
        $id = $post['intIDPoliLuar'];
        ///$resVal = $this->base_model->executeSP("imb_mPoli Luar_Detail" , $post, true);
        $resVal = $this->poli_model->getDetail($id);
        echo json_encode($resVal);
    }

    public function hapus($id){
        $resVal = $this->poli_model->delete($id);
        $status = $resVal['status'];
        $message = $resVal['message'];
        $arrMessage = array(
            "status" => $status ,
            "message" => $message ,  
        );
        $this->session->set_flashdata('insert_alert' , $arrMessage);
        redirect($this->base_url);   
    }
	
}