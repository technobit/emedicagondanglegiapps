<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kelas_kamar extends MY_Controller {
    
    var $meta_title = "Data Kelas Kamar";
    var $meta_desc = "Kelas Kamar";
	var $main_title = "Kamar";
    var $base_url = "";
    var $main_model = "";
    var $js_folder = "";
	function __construct(){
        parent::__construct();
		
		$this->load->model("kamar_model" );
        $this->base_url = $this->base_url_site."master/kelas_kamar/";
        $this->js_folder = ASSETS_JS_URL."master_data/kamar/";
    }
    
    /// Data Tindakan
	public function index()
	{
        $menu = "K01";
		$this->mode = "view";
		$dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "menu_key" => "master_kelas",
			"akses_key" => "is_view",	
			"container" => $this->_content(),
			"custom_js" => array(
				ASSETS_JS_URL."master_data/kamar/content.js",
			),
            "custom_css" => array(
				
			),
		);	
		$this->_render("default",$dt);	
	}
    
    public function form($id=""){
		$menu = "K02";
		$this->mode = "insert";
		$dt = array(
            "title" => $this->meta_title,
			"menu_key" => "master_kelas",
			"akses_key" => "is_insert",	
            "description" => $this->meta_desc,
			"container" => $this->_form($id),
			"custom_js" => array(
				ASSETS_URL."plugins/select2/select2.js",
				ASSETS_URL."plugins/validate/jquery.validate_1.11.1.min.js",
				ASSETS_JS_URL."master_data/kamar/form_kelas.js",
			),
            "custom_css" => array(
				ASSETS_URL."plugins/select2/select2.css",
			),
		);	
		$this->_render("default",$dt);
	}
	
	private function _content(){
		$dt = array();
		$breadcrumbs = array(
				"Home" =>$this->base_url_site,
				"Master Data" => $this->base_url,
				"Data Kelas Kamar" => "#",
        );
		
		$dataKelas = $this->kamar_model->getDataKelas();
		
		$dt['breadcrumbs'] = $this->setBreadcrumbs($breadcrumbs);
		$dt['title'] = $this->meta_title;
		$dt['data_kelas'] = $dataKelas;
		$dt['link_add'] = $this->base_url."form/";
		$ret = $this->load->view("master/kelas_kamar/content" , $dt, true);
		return $ret;
	}
	
	private function _form($id){
		$dt = array();
		$mode = "insert";
		$dataDetail = array();
		if(!empty($id)){
			$mode = "edit";
			$dataDetail = $this->kamar_model->getDetailKelas($id);
		}
		
		$arrForm = array("intIdKelasKamar" ,
                         "txtKelasKamar" ,
                         "bitStatusKelasKamar",
						 );
		foreach($arrForm as $frmData){
			$$frmData = !empty($dataDetail[$frmData]) ? $dataDetail[$frmData] : "";
		}
        
		$dt['intIdKelasKamar'] = $this->form_builder->inputHidden("intIdKelasKamar" , $intIdKelasKamar);
		$dt['txtKelasKamar'] = $this->form_builder->inputText("Kelas" , "txtKelasKamar" , $txtKelasKamar , "col-sm-3");
		$dt['bitStatusKelasKamar'] = $this->form_builder->inputDropdown("Status" , "bitStatusKelasKamar" ,$bitStatusKelasKamar, $this->config->item("status_list"));
		
		$breadcrumbs = array(
				"Home" =>$this->base_url_site,
				"Master Data" => $this->base_url,
				"Data Kelas Kamar" => $this->base_url,
                "Form Input Kelas Kamar" => $this->base_url,
        );
		
		$dt['breadcrumbs'] = $this->setBreadcrumbs($breadcrumbs);
		$dt['title'] = $this->meta_title;
		$dt['link_index'] = $this->base_url;
		$ret = $this->load->view("master/kelas_kamar/form" , $dt, true);
		return $ret;
	}
	
	/// Dependency Public Function
	public function delete(){
		if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";
        }
		$status = false;
		$html = "";
		$id = $this->input->post("id");
		if(!empty($id)){
			$resVal = $this->kamar_model->deleteKelas($id);
		}
		$retVal['status'] = $resVal['status'];
		$retVal['message'] = $resVal['error_stat'];
		////$this->session->set_flashdata("alert_message",$retVal);
		$this->setJsonOutput($retVal);
	}
	
	public function simpanData(){
		if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";
        }
		
		$status = false;
		$html = "";
		$id = $this->input->post("intIdKelasKamar");
		$arrInsert = array(
						 "txtKelasKamar" =>$this->input->post("txtKelasKamar"),
						 "bitStatusKelasKamar"=>$this->input->post("bitStatusKelasKamar"),
						 );
		if(!empty($id)){
			$dt['dtLastUpdate'] = date("Y-m-d H:i:s");
		}else{
			$dt['dtCreatedDate'] = date("Y-m-d H:i:s");
			$dt['dtLastUpdate'] = date("Y-m-d H:i:s");
			
		}
		$resVal = $this->kamar_model->saveDataKelas($arrInsert , $id);
		
		$retVal['status'] = $resVal['status'];
		$retVal['message'] = $resVal['error_stat'];
		$this->setJsonOutput($retVal);
	}   
}