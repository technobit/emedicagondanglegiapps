<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pekerjaan extends MY_Controller {

    var $meta_title = "Master Data - Pekerjaan";
    var $meta_desc = "Master Data - Pekerjaan";
	var $main_title = "Master Data - Pekerjaan";
    var $base_url = "";
    var $menu = "D01";
    var $arrBreadcrumbs = array();
    public function __construct(){
        parent::__construct();
        $this->load->model("pekerjaan_model");
        $this->base_url = $this->base_url_site."data-pekerjaan";
        $this->arrBreadcrumbs = array(
            "Master Data" => $this->base_url,
            "Pekerjaan" => $this->base_url,
        );
    } 
    
	public function index()
	{
        if(!empty($this->input->post())){
            $post = $this->input->post();
            $primaryID = $post['id_pekerjaan'];
            
            $arrDataInput = array(
                "jenis_pekerjaan" => $post['jenis_pekerjaan'],
                "pekerjaan" => $post['pekerjaan'],
               
            );
            $resVal = $this->pekerjaan_model->saveUpdate($arrDataInput , $primaryID);

            $status = $resVal['status'];
            $message = $resVal['message'];
            $arrMessage = array(
                "status" => $status ,
                "message" => $message ,  
            );
            $this->session->set_flashdata('insert_alert' , $arrMessage);
            redirect($this->base_url);
        }

		$dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
			"container" => $this->_home(),
            "menu_key" => "master_pekerjaan",
			"akses_key" => "is_view",	
            "main_title" => $this->main_title,
            "breadcrumbs" => $this->arrBreadcrumbs,
			"custom_js" => array(
				ASSETS_JS_URL."master_data/pekerjaan/index.js",
			),
            "custom_css" => array(
				
			),
		);	
		$this->_render("default",$dt);	
	}
	
    private function _home(){
        $dt = array();
        ///$data = $this->base_model->executeSP("imb_mpekerjaan_Retrieve");
        $data = $this->pekerjaan_model->getData();
        $arrJenis = $this->config->item("pekerjaan");
        $this->form_builder->form_type = "inline";
        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->arrBreadcrumbs);
        $dt['linkForm'] = $this->base_url;
        $dt['id_pekerjaan'] = $this->form_builder->inputHidden("id_pekerjaan" , "");
        $dt['jenis_pekerjaan'] = $this->form_builder->inputDropdown("Jenis Pekerjaan" , "jenis_pekerjaan" , array() , $arrJenis);
        $dt['pekerjaan'] = $this->form_builder->inputText("Pekerjaan" , "pekerjaan" , "" , "col-sm-3" , array("required" => "required"));
        $dt['data'] = $data;
        $dt['jenis'] = $arrJenis;
        $ret = $this->load->view("master/pekerjaan/content" , $dt , true);
        return $ret;
    }

    public function detail(){
        if(!$this->input->is_ajax_request()){
            echo "Ilegal";die;
        }
        $post = $this->input->post();
        $id = $post['id_pekerjaan'];
        ///$resVal = $this->base_model->executeSP("imb_mPekerjaan_Detail" , $post, true);
        $resVal = $this->pekerjaan_model->getDetail($id);
        echo json_encode($resVal);
    }

    public function hapus($id){
        $resVal = $this->pekerjaan_model->delete($id);
        $status = $resVal['status'];
        $message = $resVal['message'];
        $arrMessage = array(
            "status" => $status ,
            "message" => $message ,  
        );
        $this->session->set_flashdata('insert_alert' , $arrMessage);
        redirect($this->base_url);   
    }
	
}