<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Desa extends MY_Controller {
    
    var $meta_title = "Master Desa";
    var $meta_desc = "Master Desa";
	var $main_title = "Master Desa";
    var $base_url = "";
	var $upload_dir = "";
	var $upload_url = "";
	var $menu_key = "master_desa";
	var $limit = "20";
	var $arr_level_user = "";
    var $menu = "K01";
    var $mode = "";
	function __construct(){
        parent::__construct();
		
		$this->load->model("Desa_model" , "desa");
		$this->load->model("pelayanan_model");
        $this->base_url = $this->base_url_site."data-desa/";
    }
    
  
    /// Data ICD 10
	public function index()
	{
		$this->mode = "view";
		$dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
			"container" => $this->_content(),
			"menu_key" => $this->menu_key,
			"akses_key" => "is_view",
			"custom_js" => array(
				ASSETS_JS_URL."master_data/desa/content.js",
			),
            "custom_css" => array(
				
			),
		);	
		$this->_render("default",$dt);	
	}
    
    public function form($id=""){
		
		$this->mode = "insert";
		$akses_key = !empty($id) ? "is_update" : "is_insert";
		$dt = array(
            "title" => $this->meta_title,
			"menu_key" => $this->menu_key,
			"akses_key" => "is_view",
            "description" => $this->meta_desc,
			"container" => $this->_form($id),
			"custom_js" => array(
				ASSETS_URL."plugins/select2/select2.js",
				ASSETS_URL."plugins/validate/jquery.validate_1.11.1.min.js",
				ASSETS_JS_URL."master_data/desa/form.js",
			),
            "custom_css" => array(
				ASSETS_URL."plugins/select2/select2.css",
			),
		);	
		$this->_render("default",$dt);
	}
	
	private function _content(){
		$dt = array();
		$breadcrumbs = array(
				"Home" =>$this->base_url_site,
				"Master Data" => $this->base_url,
				"Desa" => "#",
        );
		
		$dt['breadcrumbs'] = $this->setBreadcrumbs($breadcrumbs);
		$dt['title'] = $this->meta_title;
		$dt['frm_search'] = $this->form_builder->inputText("Kode / Nama Desa" , "searchText","" ,"col-sm-3" , array("class"=> "form-control"));
		$dt['link_add'] = $this->base_url."tambah-data/";
		
		$ret = $this->load->view("master/desa/content" , $dt, true);
		return $ret;
	}
	
	private function _form($id){

		$this->load->model(array(
			"kelurahan_model" , 
			"pegawai_model"
		));
		$dt = array();
		$mode = "insert";
		$desa = array();
		$data_petugas = array();
		if(!empty($id)){
			$mode = "edit";
			$desa = $this->desa->getDetail($id);
			$data_petugas = $this->pelayanan_model->getListAkses($id);
		}
		
		$arrDefault = array("readonly"=>"readonly");
		
		$arrForm = array("id" , 
		"intIdPelayanan",
		"intIdKelurahan",
		"nama_desa", "keterangan", "status_desa");
		foreach($arrForm as $frmData){
			$$frmData = !empty($desa[$frmData]) ? $desa[$frmData] : "";
		}

		$dataKelurahan = $this->kelurahan_model->getDataKelurahan(ID_KECAMATAN);
		$listKelurahan = arrayDropdown($dataKelurahan , "IDKelurahan"  , "Nama");
		$list_pegawai = arrayDropdown($this->pegawai_model->getListPegawai() , 'intIdPegawai' , 'txtNamaPegawai');

		$dt['frmid'] = $this->form_builder->inputHidden("id", $id);
		$dt['frmid_pelayanan'] = $this->form_builder->inputHidden("id_pelayanan", $intIdPelayanan);
		$dt['frmnama_desa'] = $this->form_builder->inputText("Nama Unit" , "nama_desa" , $nama_desa, "col-sm-3");
		$dt['frmketerangan'] = $this->form_builder->inputTextArea("Keterangan" , "keterangan" , $keterangan , "col-sm-3");
		$dt['frmStatusdesa'] = $this->form_builder->inputDropdown("Status Desa" , "cmbStatus" ,$status_desa, $this->config->item("status_list"));
		$dt['frmListKelurahan'] = $this->form_builder->inputDropdown("Kelurahan" , "cmbKelurahan" ,$intIdKelurahan, $listKelurahan);
		$dt['listPegawai'] = $list_pegawai;
		$dt['data_petugas'] = $data_petugas;

		$breadcrumbs = array(
				"Home" =>$this->base_url_site,
				"Master Data" => $this->base_url,
				"Poli Desa" => "#",
        );

		$dt['breadcrumbs'] = $this->setBreadcrumbs($breadcrumbs);
		$dt['title'] = $this->meta_title;
		$dt['mode'] = $mode;
		$dt['link_index'] = $this->base_url;
		$ret = $this->load->view("master/desa/form" , $dt, true);
		return $ret;
	}
	
	/// Dependency Public Function
	public function hapusDataDesa(){
		if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";
        }
		$status = false;
		$html = "";
		$id = $this->input->post("id");
		if(!empty($id)){
			$resValDesa = $this->desa->delete($id);
			$resValD = $this->pelayanan_model->delete($id);
			$resVal = $this->pelayanan_model->deleteAkses($id);
		}
		$retVal['status'] = $resValDesa['status'];
		$retVal['message'] = $resValDesa['error_stat'];
		$this->session->set_flashdata("alert_message",$retVal);
		$this->setJsonOutput($retVal);
	}
	
	public function simpanDataDesa(){
		$this->isAjaxRequest();

		$status = false;
		$html = "";
		$id = $this->input->post("id");
		$id_pelayanan = $this->input->post("id_pelayanan");

		$arrDataPelayanan = array(
			"intIdJenisPelayanan" => 14,
			"txtNama" => $this->input->post("nama_desa"),
			"bitStatus" => $this->input->post("cmbStatus"),
		);

		if(!empty($id_pelayanan)){
			$resPelayanan = $this->pelayanan_model->update($arrDataPelayanan , $id_pelayanan);
		}else{
			$resPelayanan = $this->pelayanan_model->save($arrDataPelayanan);
		}

		$id_pelayanan = $resPelayanan['id'];
		$listPetugas = $this->input->post("listPegawai");
		$arrDataAksesPelayanan = array();
		foreach($listPetugas as $rowPetugas){
			$arrDataAksesPelayanan[] = array(
				"intIdPelayanan" => $id_pelayanan,
				"intIdUser" => $rowPetugas,
			);
		}
		$resAkses = $this->pelayanan_model->saveAkses($arrDataAksesPelayanan , $id_pelayanan);
		$arrInsert = array(
						 "intIdPelayanan" => $id_pelayanan,
						 "intIdKelurahan" => $this->input->post("cmbKelurahan"),
						 "nama_desa"=>$this->input->post("nama_desa"),
						 "keterangan"=>$this->input->post("keterangan"),
						 "status_desa"=>$this->input->post("cmbStatus")
						 );
		if(!empty($id)){
			$arrInsert['last_update'] = date('Y-m-d H:i:s');
			$resVal = $this->desa->update($arrInsert , $id);
		}else{
			$arrInsert['created_date'] = date('Y-m-d H:i:s');
			$arrInsert['last_update'] = date('Y-m-d H:i:s');
			$resVal = $this->desa->saveData($arrInsert);
		}

		$retVal['status'] = $resVal['status'];
		$retVal['message'] = $resVal['error_stat'];
		$this->session->set_flashdata("alert_message",$retVal);
		$this->setJsonOutput($retVal);
	}
	
	public function getData($search="" , $level=""){
		if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";
        }
		
		$status = false;
		$html = "";
		$post = $this->input->post();
		$data_results = $this->desa->getData();
		$retVal['data'] = array();
		if(!empty($data_results)) {
			$retVal['data'] = array();
			foreach($data_results as $row){
				$btnEdit = "<a href='".$this->base_url."edit/".$row['intIdPelayanan']."' class='btn btn-info btn-flat'><i class=\"fa fa-pencil\"></i> Edit</a>";
				$btnDelete = "<button onclick='hapusData(".$row['intIdPelayanan'].")' type='button' class='btn btn-danger btn-flat'><i class=\"fa fa-trash\"></i> Hapus</button>";
				$btnAksi = $btnEdit.$btnDelete;
				$retVal['data'][] = array($row['nama_desa'],
										  $row['nama_kelurahan'],
										  $row['jumlah_petugas'],
										  $row['keterangan'],
										  $btnAksi
									);
			}
		}
		$this->setJsonOutput($retVal);
	}
	
	public function getListDesa (){
        
        if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";
        }
        $where = $this->input->get("q");
        $page = !empty($_GET['page']) ? $this->input->get("page") : 1;
        $start = ($page - 1) * $this->limit;
        $desa = $this->desa->getListDesa($where , $start , $this->limit);
        $jumlah = $this->desa->getCountListDesa($where);
        $retVal = array();
		
        $retVal['total_count'] = $jumlah;
        $retVal['incomplete_results'] = false;
        $retVal['items'] = $desa;
        echo json_encode($retVal);
    }
    
	
}
