<?php

class Laboratorium extends MY_Controller {

	var $meta_title = "Rekapitulasi Kunjungan Laboratorium";
    var $meta_desc = "Rekapitulasi Kunjungan Laboratorium";
    var $main_title = "Rekapitulasi Kunjungan Laboratorium";
    var $menu_key = "rekap_kunjungan_lab";
    var $dtBreadcrumbs = array();  
    var $jenis_pemeriksaan = "";
    var $jenisKelamin = "";

	public function __construct() {
		parent::__construct();
        $this->base_url = $this->base_url_site."rekapitulasi/kunjungan-laboratorium/";
        $this->load->model("pelayanan_model");
        $this->load->model("register_model");
        $this->load->model("pasien_model");
        $this->load->model("laboratorium_model");
		$this->load->library('excel');
        $this->jenis_pemeriksaan = $this->config->item("jenis_pemeriksaan_lab");
        $this->jenisKelamin = $this->config->item("jk_list");
	}

	public function index() {        
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "menu_key" => $this->menu_key,
			"akses_key" => "is_view",
            "container" => $this->_index(),
            "custom_js" => array(   
            	ASSETS_JS_URL."rekapitulasi/rekap_kunjungan_lab.js"             
            ),
            "custom_css" => array(),
        );  
        $this->_render("default",$dt);             
    }

    private function _index() {        
        
        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $arrPemeriksaan = array("0" => " - Semua - ");
        $jenisPemeriksaan = array_merge($arrPemeriksaan , $this->config->item("jenis_pemeriksaan_lab")); 
        $dt['jenisPemeriksaan'] = $this->form_builder->inputDropdown("Jenis Pemeriksaan" , "intIdJenisPemeriksaan" , "0" , $jenisPemeriksaan);
        $dt['title'] = $this->meta_title;
        return $this->load->view('rekapitulasi/rekap_kunjungan_lab', $dt, true);
    }

    public function getData(){
        if(!$this->input->is_ajax_request()){
            echo "Ilegall";die;
        }
        
        $startDate = $this->input->post("start_date")." 00:00:00";
        $endDate = $this->input->post("end_date")." 23:59:59";
        $jenisPemeriksaan = $this->input->post("intIdJenisPemeriksaan");
        $jenisKelamin = $this->config->item("jk_list");

        $dataPeriksa = $this->laboratorium_model->getDataRekap($startDate , $endDate , $jenisPemeriksaan);
        $arrResult = array();

        foreach ($dataPeriksa as $key => $value) {
			# code...
			$resPeriksa = '	<dt>'.$value['txtLabelPemeriksaan'].'</dt>
							<dd>'.$value['txtValuePemeriksaan'].' '.$value['txtSatuanPemeriksaan'].' <br>
							'.$value['txtParameterPemeriksaan'].'
							</dd>';
            $arrResult[$value['txtIdKunjungan']]['tanggal_pemeriksaan'] = indonesian_date($value['dtTanggalKunjungan']);
            $arrResult[$value['txtIdKunjungan']]['txtNamaPasien'] = $value['txtNamaPasien'];
            $arrResult[$value['txtIdKunjungan']]['txtNoAnggota'] = $value['txtNoAnggota'];
            $arrResult[$value['txtIdKunjungan']]['charJkPasien'] = $this->jenisKelamin[$value['charJkPasien']];
            $arrResult[$value['txtIdKunjungan']]['AlamatKecamatan'] = $value['AlamatKecamatan'];
			$arrResult[$value['txtIdKunjungan']]['jenis_pemeriksaan'] = $this->jenis_pemeriksaan[$value['intIdJenisPemeriksaan']];
			$arrResult[$value['txtIdKunjungan']]['hasil_periksa'][$value['txtFieldPemeriksaan']] = $resPeriksa;
		}
        
        $retVal = array();
        $retVal['data'] = array();
        foreach($arrResult as $rowVal) {

            $hasilPeriksa = $rowVal['hasil_periksa'];
			$htmlResult = "<dl>";
			foreach ($hasilPeriksa as $rowDetailPeriksa) {
				# code...
				$htmlResult .= $rowDetailPeriksa;
			}
			$htmlResult .= "</dl>";

            $arrData = array(
                $rowVal['tanggal_pemeriksaan'],
                $rowVal['txtNoAnggota'],
                $rowVal['txtNamaPasien'],
                $rowVal['charJkPasien'],
                $rowVal['AlamatKecamatan'],
                $rowVal['jenis_pemeriksaan'],
                $htmlResult
            );

            $retVal['data'][] = $arrData;
        }
        echo json_encode($retVal);   
    }

    public function DownloadDataExcel($intIdJenisPemeriksaan , $startDate , $endDate){
			$namaPoli = "";
			$titleRecap = "Laporan Kunjungan Laboratorium";
            $startDate = $startDate . " 00:00:00";
            $endDate = $endDate . " 23:59:00";
            $dataPeriksa = $this->laboratorium_model->getDataRekap($startDate , $endDate , $intIdJenisPemeriksaan);
            $arrResult = array();

            foreach ($dataPeriksa as $key => $value) {
                # code...
                $resPeriksa =   $value['txtLabelPemeriksaan'].'
                                '.$value['txtValuePemeriksaan'].' '.$value['txtSatuanPemeriksaan'].' 
                                '.$value['txtParameterPemeriksaan'];
                $arrResult[$value['txtIdKunjungan']]['tanggal_pemeriksaan'] = indonesian_date($value['dtTanggalKunjungan']);
                $arrResult[$value['txtIdKunjungan']]['txtNamaPasien'] = $value['txtNamaPasien'];
                $arrResult[$value['txtIdKunjungan']]['txtNoAnggota'] = $value['txtNoAnggota'];
                $arrResult[$value['txtIdKunjungan']]['charJkPasien'] = $this->jenisKelamin[$value['charJkPasien']];
                $arrResult[$value['txtIdKunjungan']]['AlamatKecamatan'] = $value['AlamatKecamatan'];
                $arrResult[$value['txtIdKunjungan']]['jenis_pemeriksaan'] = $this->jenis_pemeriksaan[$value['intIdJenisPemeriksaan']];
                $arrResult[$value['txtIdKunjungan']]['hasil_periksa'][$value['txtFieldPemeriksaan']] = $resPeriksa;
            }


            $this->setHeaderExcel($titleRecap , $startDate , $endDate);
            $this->getDataLaboratorium($arrResult);
            $filename = "Rekap_Kunjungan_Laboratorium_".date("YmdHi");
		    $this->getOutput($filename);
	}

    private function getDataLaboratorium($dataRows){
        
        $indexNo = 7;
        $this->excel->getActiveSheet()->setCellValue('A6', "Tanggal");
        $this->excel->getActiveSheet()->setCellValue('B6', "No Anggota");
        $this->excel->getActiveSheet()->setCellValue('C6', "Nama");
        $this->excel->getActiveSheet()->setCellValue('D6', "Jenis Kelamin");
        $this->excel->getActiveSheet()->setCellValue('E6', "Alamat");
        $this->excel->getActiveSheet()->setCellValue('F6', "Jenis Pemeriksaan");
        $this->excel->getActiveSheet()->setCellValue('G6', "Hasil Pemeriksaan");
        foreach ($dataRows as $rows) {
            $hasilPeriksa = $rows['hasil_periksa'];
			$htmlResult = "";
			foreach ($hasilPeriksa as $rowDetailPeriksa) {
				# code...
				$htmlResult .= $rowDetailPeriksa;
			}

            $this->excel->getActiveSheet()->setCellValue('A'.($indexNo), $rows['tanggal_pemeriksaan']);
            $this->excel->getActiveSheet()->setCellValue('B'.($indexNo), $rows['txtNoAnggota']);
            $this->excel->getActiveSheet()->setCellValue('C'.($indexNo), $rows['txtNamaPasien']);
            $this->excel->getActiveSheet()->setCellValue('D'.($indexNo), $rows['charJkPasien']);
            $this->excel->getActiveSheet()->setCellValue('E'.($indexNo), $rows['AlamatKecamatan']);
            $this->excel->getActiveSheet()->setCellValue('F'.($indexNo), $rows['jenis_pemeriksaan']);
            $this->excel->getActiveSheet()->setCellValue('G'.($indexNo), $htmlResult);
            $this->excel->getActiveSheet()->getStyle('G'.$indexNo)->getAlignment()->setWrapText(true);
            $indexNo++;
        }

    }

	private function setHeaderExcel($titleRecap , $startDate , $endDate){
		
        $this->excel->setActiveSheetIndex(0);
        $this->excel->getActiveSheet()->setTitle('Sheet 1');
        $this->excel->getActiveSheet()->setCellValue('A1', 'Puskesmas '.APP_KELURAHAN);
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(20);
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->mergeCells('A1:N1');
        $this->excel->getActiveSheet()->setCellValue('A2', $titleRecap);
        $this->excel->getActiveSheet()->setCellValue('A3', 'Tanggal Awal');
        $this->excel->getActiveSheet()->setCellValue('A4', 'Tanggal Akhir');
        $this->excel->getActiveSheet()->setCellValue('B3', $startDate);
        $this->excel->getActiveSheet()->setCellValue('B4', $endDate);
        
    }

    private function getOutput( $filename){
        ///
        $filename = date('YmdHis')."-".$filename;
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'.xls"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $objWriter->save('php://output');
    }
}