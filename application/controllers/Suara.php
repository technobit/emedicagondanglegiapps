<?php 

class Suara extends MY_Controller{

    var $format_panggilan = 0;
    function __construct(){
        parent::__construct();
        $this->load->model(array(
            "pelayanan_model" ,
            "panggilan_model" ,
            "pasien_model",
            "register_model",
            "register_loket_model"
        ));
        $this->load->library("speechlib");
    }

    /// For Testing Purposes Only
    function index(){
        $arrData = array(
                            "pilihan_format" => 0,
                            "urutan" => 2,
                            "pelayanan_asal" => 1,
                            "pelayanan_tujuan" => 28,
                            "ruang_asal" => "",
                            "ruang_tujuan" => "",
                        );
        $this->speechlib->initialize($arrData);
    }

    function antrian($idKunjungan){
        $format_panggilan = 4;
        $pelayanan_asal = 0;
        $pelayanan_tujuan = 0;
        $detail_register = $this->register_model->getDetailRegister($idKunjungan);
        	
        $noAntri = $detail_register['intNoAntri'];
		$idPelayanan = $detail_register['intIdPelayanan'];
        $bitPeriksaLanjut = $detail_register['bitPeriksaLanjut'];
        if($bitPeriksaLanjut!=0){
            ///$format_panggilan = 1;
            $intRefIdPelayanan = $detail_register['intRefIdPelayanan'];
            $detailPelayananAsal = $this->pelayanan_model->getDetail($intRefIdPelayanan);
            $pelayanan_asal = $detailPelayananAsal['intTypeSuara'];    
        }
        $detailPelayanan = $this->pelayanan_model->getDetail($idPelayanan);
        $pelayanan_tujuan = $detailPelayanan['intTypeSuara'];
        $ruang_tujuan = $detailPelayanan['intRuang'];

        $arrData = array(
            "pilihan_format" => $format_panggilan,
            "urutan" => $noAntri,
            "pelayanan_asal" => $pelayanan_asal,
            "pelayanan_tujuan" => $pelayanan_tujuan,
            "ruang_asal" => "",
            "ruang_tujuan" => $ruang_tujuan,
        );
		$this->speechlib->initialize($arrData);
    }

    //// Services For Insert By Ajax
    function antrian_loket($id){
        $format_panggilan = 5;
        $pelayanan_asal = 0;
        $pelayanan_tujuan = 0;
        $detail_register = $this->register_loket_model->detail($id);
        	
        $noAntri = $detail_register['intNoAntri'];
		$idPelayanan = $detail_register['intIdPelayanan'];
        
        $detailPelayanan = $this->pelayanan_model->getDetail($idPelayanan);
        $pelayanan_tujuan = $detailPelayanan['intTypeSuara'];
        $ruang_tujuan = $detailPelayanan['intRuang'];

        //// Insert To Panggilan Terpusat
        $arrInsertPanggilan = array(
            'txtIDKunjungan' => $id,
            'intStatus' => 0,
            'dtKunjungan' => date('Y-m-d H:i:s')
        );

        $resPanggilanModel = $this->panggilan_model->insertData($arrInsertPanggilan);
        if($resPanggilanModel){
            echo "Success To Insert";
        }
		//$this->speechlib->initialize($arrData);
    }


    //// Services For Desktop Apps
    function call_antrian_loket($id){
        $format_panggilan = 5;
        $pelayanan_asal = 0;
        $pelayanan_tujuan = 0;
        $detail_register = $this->register_loket_model->detail($id);
        	
        $noAntri = $detail_register['intNoAntri'];
		$idPelayanan = $detail_register['intIdPelayanan'];
        
        $detailPelayanan = $this->pelayanan_model->getDetail($idPelayanan);
        $pelayanan_tujuan = $detailPelayanan['intTypeSuara'];
        $ruang_tujuan = $detailPelayanan['intRuang'];

        $arrData = array(
            "pilihan_format" => $format_panggilan,
            "urutan" => $noAntri,
            "pelayanan_asal" => $pelayanan_asal,
            "pelayanan_tujuan" => $pelayanan_tujuan,
            "ruang_asal" => "",
            "ruang_tujuan" => $ruang_tujuan,
        );


		$this->speechlib->initialize($arrData);
    }


}