<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Level extends MY_Controller {
    var $meta_title = "Level Menu";
    var $meta_desc = "Level Menu";
	var $main_title = "Level Menu";
    var $base_url = "";
	var $limit = "20";
    var $menu = "K01";
    var $main_model = "";
	var $arr_form = "";
	function __construct(){
        parent::__construct();	
		$this->load->model("level_menu_model");
		$this->load->model("level_model");
        $this->base_url = $this->base_url_site."level-menu/";
		
    }
    
    /// Data Users
	public function index()
	{
        $menu = "K01";
		$this->mode = "view";
		$dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
			"container" => $this->_content(),
			"custom_js" => array(
				ASSETS_JS_URL."users/level-menu/content.js",
			),
            "custom_css" => array(
				
			),
		);	
		$this->_render("default",$dt);	
	}
    
    public function form($id=""){
		$menu = "K02";
		$this->mode = "insert";
		$dt = array(
            "title" => $this->meta_title,
			"menu" => "M01.1",
			"mode_menu" => "insert",
            "description" => $this->meta_desc,
			"container" => $this->_form($id),
			"custom_js" => array(
				ASSETS_URL."plugins/select2/select2.js",
				ASSETS_URL."plugins/validate/jquery.validate_1.11.1.min.js",
				ASSETS_JS_URL."users/level/form.js",
			),
            "custom_css" => array(
				ASSETS_URL."plugins/select2/select2.css",
			),
		);	
		$this->_render("default",$dt);
	}
	
	
	
	private function _content(){
		$dt = array();
		$breadcrumbs = array(
				"Home" =>$this->base_url_site,
				"Level Menu" => $this->base_url,
				"Data Level" => "#",
        );
		
		$dt['breadcrumbs'] = $this->setBreadcrumbs($breadcrumbs);
		$dt['title'] = $this->meta_title;
		$dt['link_add'] = $this->base_url."tambah-data/";
		$ret = $this->load->view("users/menu_level/content" , $dt, true);
		return $ret;
	}
	
	private function _form($id){
		$dt = array();
		$mode = "insert";
		$dataDetailLevel = $this->level_model->getDetail($id);
		echopre($dataDetailLevel);die;
		$arrForm = $this->arr_form;
		foreach($arrForm as $frmData){
			$$frmData = !empty($dataDetail[$frmData]) ? $dataDetail[$frmData] : "";
		}
		
		$dt['intIdLevel'] = $this->form_builder->inputHidden("intIdLevel" , $intIdLevel);
		$dt['txtLevel'] = $this->form_builder->inputText("txtLevel" , "txtLevel" , $txtLevel , "col-sm-3");
		$dt['bitStatusLevel'] = $this->form_builder->inputDropdown("Status Level" , "bitStatusLevel" ,$bitStatusLevel, $this->config->item("status_list"));
		$breadcrumbs = array(
				"Home" =>$this->base_url_site,
				"Master Data" => $this->base_url,
				"Data Level" => $this->base_url,
                "Form Input Level" => $this->base_url,
        );
		
		$dt['breadcrumbs'] = $this->setBreadcrumbs($breadcrumbs);
		$dt['title'] = $this->meta_title;
		$dt['form_mode'] = $mode;
		$dt['link_index'] = $this->base_url;
		$ret = $this->load->view("users/level/form" , $dt, true);
		return $ret;
	}
	
	/// Dependency Public Function
	public function hapusData(){
		if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";
        }
		$status = false;
		$html = "";
		$id = $this->input->post("id");
		if(!empty($id)){
			$resVal = $this->main_model->delete($id);
		}
		$retVal['status'] = $resVal['status'];
		$retVal['message'] = $resVal['error_stat'];
		$this->session->set_flashdata("alert_message",$retVal);
		$this->setJsonOutput($retVal);
	}
	
	public function simpanData(){
		if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";
        }
		
		$status = false;
		$html = "";
		$id = $this->input->post("intIdLevel");
		
		$arrInsert = array();
		foreach ($this->arr_form as $value) {
			# code...
			$arrInsert[$value] = $this->input->post($value);
		}
		
		if(!empty($id)){
			$arrInsert['intIdLevel'] = $id;
			$resVal = $this->main_model->update($arrInsert , $id);
		}else{
			$resVal = $this->main_model->saveData($arrInsert);
		}
		
		$retVal['status'] = $resVal['status'];
		$retVal['message'] = $resVal['error_stat'];
		$this->session->set_flashdata("alert_message",$retVal);
		$this->setJsonOutput($retVal);
	}
	
	public function getData($search="" , $level=""){
		if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";
        }
		
		$status = false;
		$html = "";
		$post = $_POST;
		$length =  $this->input->post("length");
		$start = $this->input->post("start");
		$offset = ($start - 0) * $this->limit;
		$data_results = $this->level_model->getData();
		$count_data = $this->level_model->getCountData();
        $status = $this->config->item("status_list");
		if($count_data > 0) {
			$retVal['draw'] = $this->input->post('draw');
			$retVal['recordsTotal'] = $count_data;
			$retVal['recordsFiltered'] = $count_data;
			$retVal['data'] = array();
			foreach($data_results as $row){
				$btnEdit = "<a href='".$this->base_url."edit/".$row['intIdLevel']."' class='btn btn-info btn-flat'><i class=\"fa fa-pencil\"></i> Edit</a>";
				$btnAksi = $btnEdit;
				$retVal['data'][] = array(
                                          $row['intIdLevel'] ,
                                          $row['txtLevel'] ,
										  $status[$row['bitStatusLevel']],
										  $btnAksi
									);
			}
		}else{
			$retVal['draw'] = $this->input->post('draw');
			$retVal['recordsTotal'] = 0;
			$retVal['recordsFiltered'] = 0;
			$retVal['data'] = array();
		}
		$this->setJsonOutput($retVal);
	}	
}