<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Level extends MY_Controller {
    var $meta_title = "Level";
    var $meta_desc = "Level";
	var $main_title = "Level";
    var $base_url = "";
	var $limit = "20";
    var $menu = "K01";
    var $main_model = "";
	var $arr_form = "";
	var $menu_key = "user_level";
	function __construct(){
        parent::__construct();	
		$this->load->model("level_model");
		$this->load->model("level_menu_model");
		$this->main_model= $this->level_model;
        $this->base_url = $this->base_url_site."level/";
		$this->arr_form = array(
			"intIdLevel" ,
			"txtLevel" ,
			"bitStatusLevel"
		);
    }
    
    /// Data Users
	public function index()
	{
		$this->mode = "view";
		$dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
			"menu_key" => $this->menu_key,
			"akses_key" => "is_view",
			"container" => $this->_content(),
			"custom_js" => array(
				ASSETS_JS_URL."users/level/content.js",
			),
            "custom_css" => array(
				
			),
		);	
		$this->_render("default",$dt);	
	}
    
    public function form($id=""){
		$menu = "K02";
		$akses_key = !empty($id) ? "is_insert" : "is_update";
		$dt = array(
            "title" => $this->meta_title,
			"menu_key" => $this->menu_key,
			"akses_key" => $akses_key,
            "description" => $this->meta_desc,
			"container" => $this->_form($id),
			"custom_js" => array(
				ASSETS_URL."plugins/select2/select2.js",
				ASSETS_URL."plugins/validate/jquery.validate_1.11.1.min.js",
				ASSETS_JS_URL."users/level/form.js",
			),
            "custom_css" => array(
				ASSETS_URL."plugins/select2/select2.css",
			),
		);	
		$this->_render("default",$dt);
	}
	
	public function detail_menu($id=""){
		$menu = "K02";
		$this->mode = "insert";
		$dt = array(
            "title" => $this->meta_title,
			"menu_key" => $this->menu_key,
			"akses_key" => "is_update",
            "description" => $this->meta_desc,
			"container" => $this->_build_detail_level($id),
			"custom_js" => array(
				ASSETS_URL."plugins/select2/select2.js",
				ASSETS_URL."plugins/validate/jquery.validate_1.11.1.min.js",
				ASSETS_URL."plugins/bootbox/bootbox.js",
				ASSETS_JS_URL."users/level/form_level_menu.js",
			),
            "custom_css" => array(
				ASSETS_URL."plugins/select2/select2.css",
			),
		);	
		$this->_render("default",$dt);
	}
	
	private function _build_detail_level($id){
		$dataDetail = $this->main_model->getDetail($id);
		if(empty($dataDetail)){
			show_404();
		}
		
		$arrForm = $this->arr_form;
		foreach($arrForm as $frmData){
			$$frmData = !empty($dataDetail[$frmData]) ? $dataDetail[$frmData] : "";
		}
		
		$dt['intIdLevel'] = $this->form_builder->inputHidden("intIdLevel" , $intIdLevel);
		$dt['txtLevel'] = $this->form_builder->inputText("txtLevel" , "txtLevel" , $txtLevel , "col-sm-3");
		$dt['bitStatusLevel'] = $this->form_builder->inputDropdown("Status Level" , "bitStatusLevel" ,$bitStatusLevel, $this->config->item("status_list"));
		
		$breadcrumbs = array(
				"Home" =>$this->base_url_site,
				"Level" => $this->base_url,
				"Detail Level ".$txtLevel => "#",
				
        );
		
		$dt['breadcrumbs'] = $this->setBreadcrumbs($breadcrumbs);
		$dt['title'] = "Detail Level ".$txtLevel;
		$arrMenu = array();
		$dataLevelMenu = $this->level_menu_model->getMenuByIdLevel($id);
		foreach ($dataLevelMenu as $key => $value) {
			# code...
			$arrMenu[$value['txtMenu']] = array(
										"is_view" => $value['txtMenu'],
										"is_insert" => $value['bitIsInsert'],
										"is_update" => $value['bitIsUpdate'],
										"is_delete" => $value['bitIsDelete'],
										);
			
		}
		
		$dt['data_menu'] = $arrMenu;
		$dt['config_menu'] = $this->config->item("menu_nav");
		$dt['config_submenu'] = $this->config->item("submenu_nav");
		$dt['config_subsubmenu'] = $this->config->item("subsubmenu_nav");
		$dt['link_index'] = $this->base_url;
		$ret = $this->load->view("users/level/form_level_menu" , $dt, true);
		return $ret;
	}
	
	private function _content(){
		$dt = array();
		$breadcrumbs = array(
				"Home" =>$this->base_url_site,
				"Level" => $this->base_url,
				"Data Level" => "#",
        );
		
		$dt['breadcrumbs'] = $this->setBreadcrumbs($breadcrumbs);
		$dt['title'] = $this->meta_title;
		$dt['link_add'] = $this->base_url."tambah-data/";
		$ret = $this->load->view("users/level/content" , $dt, true);
		return $ret;
	}
	
	private function _form($id){
		$dt = array();
		$mode = "insert";
		$dataDetail = array();
		if(!empty($id)){
			$mode = "edit";
			$dataDetail = $this->main_model->getDetail($id);
		}
		
		$arrForm = $this->arr_form;
		foreach($arrForm as $frmData){
			$$frmData = !empty($dataDetail[$frmData]) ? $dataDetail[$frmData] : "";
		}
		
		$dt['intIdLevel'] = $this->form_builder->inputHidden("intIdLevel" , $intIdLevel);
		$dt['txtLevel'] = $this->form_builder->inputText("txtLevel" , "txtLevel" , $txtLevel , "col-sm-3" , array());
		$dt['bitStatusLevel'] = $this->form_builder->inputDropdown("Status Level" , "bitStatusLevel" ,$bitStatusLevel, $this->config->item("status_list") , array());
		$breadcrumbs = array(
				"Home" =>$this->base_url_site,
				"Master Data" => $this->base_url,
				"Data Level" => $this->base_url,
                "Form Input Level" => $this->base_url,
        );
		
		$dt['breadcrumbs'] = $this->setBreadcrumbs($breadcrumbs);
		$dt['title'] = $this->meta_title;
		$dt['form_mode'] = $mode;
		$dt['link_index'] = $this->base_url;
		$ret = $this->load->view("users/level/form" , $dt, true);
		return $ret;
	}
	
	/// Dependency Public Function
	public function hapusData(){
		if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";
        }
		$status = false;
		$html = "";
		$id = $this->input->post("id");
		if(!empty($id)){
			$resVal = $this->main_model->delete($id);
		}
		$retVal['status'] = $resVal['status'];
		$retVal['message'] = $resVal['error_stat'];
		$this->session->set_flashdata("alert_message",$retVal);
		$this->setJsonOutput($retVal);
	}
	
	public function simpanData(){
		if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";
        }
		
		$status = false;
		$html = "";
		$id = $this->input->post("intIdLevel");
		
		$arrInsert = array();
		foreach ($this->arr_form as $value) {
			# code...
			$arrInsert[$value] = $this->input->post($value);
		}
		
		if(!empty($id)){
			$arrInsert['intIdLevel'] = $id;
			$resVal = $this->main_model->update($arrInsert , $id);
		}else{
			$resVal = $this->main_model->saveData($arrInsert);
		}
		
		$retVal['status'] = $resVal['status'];
		$retVal['message'] = $resVal['error_stat'];
		$this->session->set_flashdata("alert_message",$retVal);
		$this->setJsonOutput($retVal);
	}
	
	public function simpanDataMenu(){
		if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";
        }
		
		$arrInsertBatch = array();
		$intIdLevel = $this->input->post("intIdLevel");
		$countLevelMenu = $this->level_menu_model->checkMenuByIdLevel($intIdLevel);
		if($countLevelMenu > 0){
			$deleteMenuLevel = $this->level_menu_model->delete($intIdLevel);
		}
		$menuPost = $this->input->post("menu");
		foreach ($menuPost as $key => $value) {
			# code...
			$arrInsert = array(
				"intIdLevel" => $intIdLevel,
				"txtMenu" => $value['is_view'],
				"bitIsInsert" => isset($value['is_insert']) ? 1 : 0,
				"bitIsUpdate" => isset($value['is_update']) ? 1 : 0,
				"bitIsDelete" => isset($value['is_delete']) ? 1 : 0,
			);
			$arrInsertBatch[] = $arrInsert;
		}
		
		$retVal = $this->level_menu_model->insertBatch($arrInsertBatch);
		$this->setJsonOutput($retVal);
		
	}
	
	public function getData($search="" , $level=""){
		if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";
        }
		
		$status = false;
		$html = "";
		$post = $_POST;
		$length =  $this->input->post("length");
		$start = $this->input->post("start");
		$offset = ($start - 0) * $this->limit;
		$data_results = $this->main_model->getData();
		$count_data = $this->main_model->getCountData();
        $status = $this->config->item("status_list");
		if($count_data > 0) {
			$retVal['draw'] = $this->input->post('draw');
			$retVal['recordsTotal'] = $count_data;
			$retVal['recordsFiltered'] = $count_data;
			$retVal['data'] = array();
			foreach($data_results as $row){
				$btnEdit = "<a href='".$this->base_url."edit/".$row['intIdLevel']."' class='btn btn-info btn-flat'><i class=\"fa fa-pencil\"></i> Edit</a>";
				$btnDetail = "<a href='".$this->base_url."detail-menu/".$row['intIdLevel']."' class='btn btn-warning btn-flat'><i class=\"fa fa-pencil\"></i> Detail Menu</a>";
				$btnAksi = $btnEdit . $btnDetail;
				$retVal['data'][] = array(
                                          $row['intIdLevel'] ,
                                          $row['txtLevel'] ,
										  $status[$row['bitStatusLevel']],
										  $btnAksi
									);
			}
		}else{
			$retVal['draw'] = $this->input->post('draw');
			$retVal['recordsTotal'] = 0;
			$retVal['recordsFiltered'] = 0;
			$retVal['data'] = array();
		}
		$this->setJsonOutput($retVal);
	}	
}