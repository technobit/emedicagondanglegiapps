<?php
class Pengguna extends MY_Controller {
    var $meta_title = "Pengguna";
    var $meta_desc = "Pengguna";
	var $main_title = "Pengguna";
    var $base_url = "";
	var $limit = "20";
    var $menu = "K01";
    var $main_model = "";
	var $arr_form = "";
	var $menu_key = "user_pengguna";
	function __construct(){
        parent::__construct();
		
		$this->load->model("users_model");
		$this->load->model("pegawai_model");
		$this->load->model("level_model");
		
		$this->main_model= $this->users_model;
        $this->base_url = $this->base_url_site."pengguna/";
		$this->arr_form = array("intIdUser" ,"intIdPegawai" ,"txtUserName" ,"txtPassword","txtEmail","intIdLevel","txtProfilePict","bitStatusUser");
		
    }
    
    /// Data Users
	public function index()
	{
        
		
		$dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
			"container" => $this->_content(),
			"menu_key" => $this->menu_key,
			"akses_key" => "is_view",
			"custom_js" => array(
				ASSETS_JS_URL."users/pengguna/content.js",
			),
            "custom_css" => array(
				
			),
		);	
		$this->_render("default",$dt);	
	}
    
    public function form($id=""){
		$menu = "K02";
		$akses_key = !empty($id) ? "is_update" : "is_insert"; 
		$dt = array(
            "title" => $this->meta_title,
			"menu_key" => $this->menu_key,
			"akses_key" => $akses_key,
            "description" => $this->meta_desc,
			"container" => $this->_form($id),
			"custom_js" => array(
				ASSETS_URL."plugins/select2/select2.js",
				ASSETS_URL."plugins/validate/jquery.validate_1.11.1.min.js",
				ASSETS_JS_URL."users/pengguna/form.js",
			),
            "custom_css" => array(
				ASSETS_URL."plugins/select2/select2.css",
			),
		);	
		$this->_render("default",$dt);
	}

	public function form_password($id=""){
		$menu = "K02";
		$akses_key = !empty($id) ? "is_update" : "is_insert"; 
		$dt = array(
            "title" => $this->meta_title,
			"menu_key" => $this->menu_key,
			"akses_key" => $akses_key,
            "description" => $this->meta_desc,
			"container" => $this->_form_password($id),
			"custom_js" => array(
				ASSETS_URL."plugins/validate/jquery.validate_1.11.1.min.js",
				ASSETS_JS_URL."users/pengguna/form_password.js",
			),
            "custom_css" => array(
			),
		);	
		$this->_render("default",$dt);
	}
	
	private function _content(){
		$dt = array();
		$breadcrumbs = array(
				"Home" =>$this->base_url_site,
				"Pengguna" => $this->base_url,
				"Data Pengguna" => "#",
        );
		
		$dt['breadcrumbs'] = $this->setBreadcrumbs($breadcrumbs);
		$dt['title'] = $this->meta_title;
		$dt['link_add'] = $this->base_url."tambah-data/";
		$ret = $this->load->view("users/pengguna/content" , $dt, true);
		
		return $ret;
	}
	
	private function _form($id){
		$dt = array();
		$mode = "insert";
		$dataDetail = array();
		if(!empty($id)){
			$mode = "edit";
			$dataDetail = $this->main_model->getDetail($id);
		}
		
		$arrForm = $this->arr_form;
		foreach($arrForm as $frmData){
			$$frmData = !empty($dataDetail[$frmData]) ? $dataDetail[$frmData] : "";
		}
		
		$arrPegawai = array();
		$dataPegawai = $this->pegawai_model->getListPegawai();
		foreach ($dataPegawai as $key => $value) {
			# code...
			$arrPegawai[$value['intIdPegawai']] = $value['txtNamaPegawai'] . "-" . $value['txtNoIndukPegawai'];
		}
		
		$arrLevel = array();
		$dataLevel = $this->level_model->getListLevel();
		//echopre($dataLevel);die;
		foreach ($dataLevel as $valLevel) {
			# code...
			$arrLevel[$valLevel['intIdLevel']] = $valLevel['txtLevel'];
		}
		
		$dt['intIdUser'] = $this->form_builder->inputHidden("intIdUser" , $intIdUser);
		$dt['intIdPegawai'] = $this->form_builder->inputDropdown("Data Pegawai" , "intIdPegawai" ,$intIdPegawai, $arrPegawai);
        $dt['txtUserName'] = $this->form_builder->inputText("Username" , "txtUserName" , $txtUserName , "col-sm-3");
		$dt['txtEmail'] = $this->form_builder->inputText("Email" , "txtEmail" , $txtEmail , "col-sm-3");
		$link_password = "";
		if($mode!="edit"){
			$dt['txtPassword'] = $this->form_builder->inputPassword("Password" , "txtPassword" , $txtPassword , "col-sm-3");
			$dt['txtRePassword'] = $this->form_builder->inputPassword("Ulangi Password" , "txtRePassword" , "" , "col-sm-3");
			
		}else{
			$dt['txtPassword'] = "";
			$dt['txtRePassword'] = "";
			$link_password = $this->base_url."edit-password/".$id;
		}
		$dt['intIdLevel'] = $this->form_builder->inputDropdown("Level" , "intIdLevel" ,$intIdLevel, $arrLevel);
		$dt['bitStatusUser'] = $this->form_builder->inputDropdown("Status User" , "bitStatusUser" ,$bitStatusUser, $this->config->item("status_list"));
		$breadcrumbs = array(
				"Home" =>$this->base_url_site,
				"Master Data" => $this->base_url,
				"Data Pengguna" => $this->base_url,
                "Form Input Pengguna" => $this->base_url,
        );
		
		$dt['breadcrumbs'] = $this->setBreadcrumbs($breadcrumbs);
		$dt['title'] = $this->meta_title;
		$dt['form_mode'] = $mode;
		$dt['link_index'] = $this->base_url;
		$dt['link_ganti_password'] = $link_password;
		$ret = $this->load->view("users/pengguna/form" , $dt, true);
		return $ret;
	}

	private function _form_password($id){
		$dt = array();
		$mode = "edit";
		$dataDetail = $this->main_model->getDetail($id);
		if(empty($dataDetail)){
			show_404();
		}
				
		$dt['intIdUser'] = $this->form_builder->inputHidden("intIdUser" , $id);
		$dt['txtOldPassword'] = $this->form_builder->inputPassword("Password Lama" , "txtOldPassword" , "" , "col-sm-3");
		$dt['txtPassword'] = $this->form_builder->inputPassword("Password" , "txtPassword" , "" , "col-sm-3");
		$dt['txtRePassword'] = $this->form_builder->inputPassword("Ulangi Password" , "txtRePassword" , "" , "col-sm-3");
		$breadcrumbs = array(
				"Home" =>$this->base_url_site,
				"Master Data" => $this->base_url,
				"Data Pengguna" => $this->base_url,
                "Form Password Pengguna" => $this->base_url,
        );
		
		$dt['breadcrumbs'] = $this->setBreadcrumbs($breadcrumbs);
		$dt['title'] = $this->meta_title;
		$dt['form_mode'] = $mode;
		$dt['link_index'] = $this->base_url."edit/".$id;
		$ret = $this->load->view("users/pengguna/form_password" , $dt, true);
		return $ret;
	}
	
	/// Dependency Public Function
	public function hapusData(){
		if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";
        }
		$status = false;
		$html = "";
		$id = $this->input->post("id");
		if(!empty($id)){
			$resVal = $this->main_model->delete($id);
		}
		$retVal['status'] = $resVal['status'];
		$retVal['message'] = $resVal['error_stat'];
		$this->session->set_flashdata("alert_message",$retVal);
		$this->setJsonOutput($retVal);
	}
	
	public function simpanData(){
		if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";die;
        }
		
		$status = false;
		$html = "";
		$id = $this->input->post("intIdUser");
		
		$arrInsert = array();
		foreach ($this->arr_form as $value) {
			# code...
			$arrInsert[$value] = $this->input->post($value);
		}
		
		$arrInsert['intIdKabupaten'] = USER_LUMAJANG;
		$arrInsert['intIdKecamatan'] = "";
		if(!empty($id)){
			$arrInsert['intIdUser'] = $id;
			unset($arrInsert['txtPassword']);
			$resVal = $this->main_model->update($arrInsert , $id);
			
		}else{
			$txtPassword = $this->input->post("txtPassword");
			$txtPassword = "3M3dic4".$txtPassword."st3v3j0b5";
			$txtPassword = md5($txtPassword);
			$arrInsert['txtPassword'] = $txtPassword;	
			$resVal = $this->main_model->saveData($arrInsert);
		}
		
		$retVal['status'] = $resVal['status'];
		$retVal['message'] = $resVal['error_stat'];
		$this->session->set_flashdata("alert_message",$retVal);
		$this->setJsonOutput($retVal);
	}

	public function changePassword(){
		if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";die;
        }

		$txtOldPassword = $this->convertPassword($this->input->post("txtOldPassword"));		
		$id = $this->input->post("intIdUser");
		$dataDetail = $this->main_model->getDetail($id);
		$oldPassword = $dataDetail['txtPassword'];
		if($oldPassword!=$txtOldPassword){
			$retVal['status'] = false;
			$retVal['message'] = "Data Gagal Di Update, Password Lama Tidak Sesuai";
			$retVal['link_index'] = "";
			$this->setJsonOutput($retVal);
		}else{
			$txtPassword = $this->input->post("txtPassword");	
			$arrInsert['txtPassword'] = $this->convertPassword($txtPassword);
			$resVal = $this->main_model->update($arrInsert , $id);
			$retVal['status'] = $resVal['status'];
			$retVal['message'] = $resVal['error_stat'];
			$retVal['link_index'] = $this->base_url."edit/".$id;
			$this->setJsonOutput($retVal);
		}
	}

	
	
	public function getData($search="" , $level=""){
		if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";
        }
		
		$status = false;
		$html = "";
		$post = $_POST;
		$length =  $this->input->post("length");
		$start = $this->input->post("start");
		$offset = ($start - 0) * $this->limit;
		$data_results = $this->main_model->getData();
		$count_data = $this->main_model->getCountData();
        $status = $this->config->item("status_list");
		if($count_data > 0) {
			$retVal['draw'] = $this->input->post('draw');
			$retVal['recordsTotal'] = $count_data;
			$retVal['recordsFiltered'] = $count_data;
			$retVal['data'] = array();
			foreach($data_results as $row){
				$btnEdit = "<a href='".$this->base_url."edit/".$row['intIdUser']."' class='btn btn-info btn-flat'><i class=\"fa fa-pencil\"></i> Edit</a>";
				$btnAksi = $btnEdit;
				$retVal['data'][] = array($row['txtUserName'],
										  $row['txtNamaPegawai'] ,
										  $row['txtEmail'] ,
										  $row['txtLevel'] ,
										  $status[$row['bitStatusUser']],
										  $btnAksi
									);
			}
		}else{
			$retVal['draw'] = $this->input->post('draw');
			$retVal['recordsTotal'] = 0;
			$retVal['recordsFiltered'] = 0;
			$retVal['data'] = array();
		}
		$this->setJsonOutput($retVal);
	}	
}