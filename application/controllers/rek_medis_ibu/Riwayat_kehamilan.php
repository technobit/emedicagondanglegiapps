<?php
class Riwayat_kehamilan extends MY_Controller {
    var $base_url = "";
	var $limit = "20";
    var $menu = "K01";
    var $date_now = "";
    
	function __construct(){
        parent::__construct();
		$this->load->model("riwayat_kehamilan_model" , "riwayat_kehamilan");
        $this->base_url = $this->base_url_site."rekam-medis-ibu/";
        $this->date_now = date("Y-m-d H:i:s");
    }
    
    
   public function getFrmRiwayatKehamilan($idRekmedIbu = "" , $intIdRiwayatKehamilan = ""){
		
		if(!$this->input->is_ajax_request()){
			echo "Die, Ilegall Access";
			die;
		}
		
		$labelSave = "Simpan";
		$arrayVal = array(
						  "intKehamilanKe",
						  "txtKomplikasi",
						  "txtKondisiPersalinan",
						  "txtTempatPersalinan",
						  "txtTempatPersalinanLain",
						  "txtKomplikasiPersalinan",
						  "txtPenolong",
						  "txtPenolongLain",
						  "txtKeadaanBBL",
						  "txtJenisKelaminBBL",
						  "txtBeratBBL",
						  "txtKeadaanSekarang",
						  "bitIsKB",
						  "bitStatusRiwayatKehamilan",
						  );
		
		$detailRiwayatKehamilan = array();
		if(!empty($intIdRiwayatKehamilan)){
			$detailRiwayatKehamilan = $this->riwayat_kehamilan->getDetailRiwayatKehamilan($intIdRiwayatKehamilan);
			$labelSave = "Update";
		}
		
		foreach($arrayVal as $rowVal){
			$$rowVal = isset($detailRiwayatKehamilan[$rowVal]) ? $detailRiwayatKehamilan[$rowVal] : "";
		}
		
		$dt['intIdRiwayatKehamilan'] = $this->form_builder->inputHidden("intIdRiwayatKehamilan" , $intIdRiwayatKehamilan);
		$dt['intIdRekamMedisIbu'] = $this->form_builder->inputHidden("intIdRekamMedisIbu" , $idRekmedIbu);
		$arrKomplikasi = array(
								"apb" => "APB",
								"ht" => "HT",
								);
		$txtKomplikasi = !empty($txtKomplikasi) ? explode("|" , $txtKomplikasi) : array();
		$dt['txtKomplikasi'] = $this->form_builder->inputCheckboxGroup("Komplikasi" , "txtKomplikasi",$txtKomplikasi , $arrKomplikasi);
		$dt['intKehamilanKe'] = $this->form_builder->inputText("Kehamilan Ke" , "intKehamilanKe",$intKehamilanKe);
		$arrKondisiPersalinan = array(
									  "normal" => "Normal",
									  "aborsi" => "Aborsi",
									  "ips" => "IPS",
									  "iufd" => "IUFD",
									  "su" => "SU",
									  "alat" => "Alat",
									  "sc" => "SC",
									  );
		
		$dt['txtKondisiPersalinan'] = $this->form_builder->inputDropdown("Persalinan" , "txtPersalinan",$txtKondisiPersalinan , $arrKondisiPersalinan);
		$arrTempatPersalinan = array(
									  "rs" => "Rumah Sakit",
									  "puskesmas" => "Puskesmas",
									  "bps" => "BPS",
									  "rumah" => "Rumah",
									  "lain" => "Lain-lain",
									);
		$dt['txtTempatPersalinan'] = form_dropdown("txtTempatPersalinan" , $arrTempatPersalinan,$txtTempatPersalinan , "class='form-control' onchange='tempatPersalinan()'");
		$dt['txtTempatPersalinanLain'] = form_input("txtTempatPersalinanLain" , $txtTempatPersalinanLain , "class='form-control' style='display:none'");
		$arrKomplikasiPersalinan = array(
									  "plama" => "Persalinan Lama",
									  "infe" => "Infeksi",
									  "hpp" => "HPP",
									  );
		$txtKomplikasiPersalinan = !empty($txtKomplikasiPersalinan) ? explode("|" , $txtKomplikasiPersalinan) : array();
		
		$dt['txtKomplikasiPersalinan'] = $this->form_builder->inputCheckboxGroup("Komplikasi Persalinan" , "txtKomplikasiPersalinan",$txtKomplikasiPersalinan , $arrKomplikasiPersalinan);
		$arrPenolongPersalinan = array(
									  "dokter" => "Dokter",
									  "bidan" => "Bidan",
									  "lain" => "Lain-lain",
									  );
		
		$dt['txtPenolong'] = form_dropdown("txtPenolong" , $arrPenolongPersalinan,$txtPenolong, "class='form-control' onchange='cmbPenolong()'");
		$dt['txtPenolongLain'] = form_input("txtPenolongLain" , $txtPenolongLain , "class='form-control' style='display:none'");
		
		$arrJenisKelamin = array(
						"L" => "Laki-Laki",
						"P" => "Perempuan",
						);
		
		$dt['txtJenisKelamin'] = $this->form_builder->inputDropdown("Jenis Kelamin" , "txtJenisKelamin",$txtJenisKelaminBBL , $arrJenisKelamin);
		$dt['txtBeratBadan'] = $this->form_builder->inputText("Barat Badan BBL" , "txtBeratBadanBBL",$txtBeratBBL);
		$arrKeadaanBBL = array(
						"sehat" => "Sehat",
						"sakit" => "Sakit",
						"mati" => "Mati",
						);
		$dt['txtKeadaanBBL'] = $this->form_builder->inputDropdown("Keadaan BBL" , "txtKeadaanBBL",$txtKeadaanBBL , $arrKeadaanBBL);
		$arrKeadaanSekarang = array(
						"hidup" => "Hidup",
						"mati" => "Mati",
						);
		$dt['txtKeadaanSekarang'] = $this->form_builder->inputDropdown("Keadaan Sekarang" , "txtKeadaanSekarang",$txtKeadaanSekarang , $arrKeadaanSekarang);
		$arrKeadaanKB = array(
						"1" => "Ya",
						"0" => "Tidak",
						);
		
		$dt['txtISKb'] = $this->form_builder->inputDropdown("KB" , "txtISKb",$bitIsKB , $arrKeadaanKB);
		$dt['buttonLabel'] = $labelSave;
		$retVal = $this->load->view("pelayanan/poli_ibu/form_riwayat_kehamilan" , $dt , true);
		echo $retVal;
	}
	
    public function getDataRiwayatKehamilan(){
		if(!$this->input->is_ajax_request()){
			echo "Die, Ilegall Access";
			die;
		}
		$status = false;
		$html = "";
		$length =  $this->input->post("length");
		$start = $this->input->post("start");
		$id = $this->input->post("txtIdRekamMedis");
		$data_rekmed = $this->riwayat_kehamilan->getDataRiwayatKehamilan($start , $this->limit , $id);
		$count_data = $this->riwayat_kehamilan->getCountDataRiwayatKehamilan($id);
		if($count_data > 0) {
			$retVal['draw'] = $this->input->post('draw');
			$retVal['recordsTotal'] = $count_data;
			$retVal['recordsFiltered'] = $count_data;
			$retVal['data'] = array();
			foreach($data_rekmed as $pasien){
				$btnDetail = '<button class="btn btn-flat btn-success" type="button" onclick="getDetailFrmRiwayatKehamilan(\''.$pasien['intIdRiwayatKehamilan'].'\')"><i class="fa fa-list"></i> Detail</button>';
				$btnDelete = '<button class="btn btn-flat btn-danger" type="button" onclick="hapusData(\''.$pasien['intIdRiwayatKehamilan'].'\')"><i class="fa fa-trash"></i> Hapus</button>';
				$btnAksi = $btnDetail . $btnDelete;
				$retVal['data'][] = array(
										  $pasien['intKehamilanKe'],
										  $pasien['txtKomplikasi'],
										  $pasien['txtKondisiPersalinan'],
										  $pasien['txtTempatPersalinan'],
										  $pasien['txtKomplikasiPersalinan'],
										  $btnAksi);
			}
		}else{
			$retVal['draw'] = $this->input->post('draw');
			$retVal['recordsTotal'] = 0;
			$retVal['recordsFiltered'] = 0;
			$retVal['data'] = array();
		}
		$this->setJsonOutput($retVal);
	}
	
    
	
	public function saveDataRiwayatKehamilan(){
		if(!$this->input->is_ajax_request()){
			echo "Die, Ilegall Access";
			die;
		}
		$intIdRiwayatKehamilan = $this->input->post('intIdRiwayatKehamilan');
		$txtKomplikasi = $this->input->post('txtKomplikasi');	
		$txtKomplikasiPersalinan = $this->input->post('txtKomplikasiPersalinan');
		$txtKomplikasiPersalinan = !empty($txtKomplikasiPersalinan) ? implode($txtKomplikasiPersalinan , "|") : "";
		$txtKomplikasi = !empty($txtKomplikasi) ? implode($txtKomplikasi , "|") : "";
		$arrPost = array(
						"intIdRekamMedisIbu" => $this->input->post('intIdRekamMedisIbu'),
						"intKehamilanKe" => $this->input->post('intKehamilanKe'),
						"txtKomplikasi" => $txtKomplikasi,
						"txtKondisiPersalinan" => $this->input->post('txtPersalinan'),
						"txtTempatPersalinan" => $this->input->post('txtTempatPersalinan'),
						"txtTempatPersalinanLain" => $this->input->post('txtTempatPersalinanLain'),
						"txtKomplikasiPersalinan" => $txtKomplikasiPersalinan,
						"txtPenolong" => $this->input->post('txtPenolong'),
						"txtPenolongLain" => $this->input->post('txtPenolongLain'),
						"txtKeadaanBBL" => $this->input->post('txtKeadaanBBL'),
						"txtJenisKelaminBBL" => $this->input->post('txtJenisKelamin'),
						"txtBeratBBL" => $this->input->post('txtBeratBadanBBL'),
						"txtKeadaanSekarang" => $this->input->post('txtKeadaanSekarang'),
						"bitIsKB" => $this->input->post('txtISKb'),
						"bitStatusRiwayatKehamilan" => 1,
		);
		
		if(empty($intIdRiwayatKehamilan)){
			$arrPost['dtCreatedDate'] = $this->date_now;
			$arrPost['dtLastUpdate'] = $this->date_now;
		}else{
			$arrPost['dtLastUpdate'] = $this->date_now;
		}
		$retVal = $this->riwayat_kehamilan->saveUpdateRiwayat($arrPost , $intIdRiwayatKehamilan);
		$this->setJsonOutput($retVal);
	}
	
	public function deleteDataRiwayatKehamilan(){
		if(!$this->input->is_ajax_request()){
			echo "Die, Ilegall Access";
			die;
		}
		
		$intIdRiwayatKehamilan = $this->input->post('intIdRiwayatKehamilan');
		$retVal = $this->riwayat_kehamilan->deleteRiwayat($intIdRiwayatKehamilan);
		$this->setJsonOutput($retVal);
	}
	
	
	
}
?>