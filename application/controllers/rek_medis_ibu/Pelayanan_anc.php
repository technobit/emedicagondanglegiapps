<?php
class Pelayanan_anc extends MY_Controller {
    var $base_url = "";
	var $limit = "20";
    var $menu = "K01";
    var $date_now = "";
    
	function __construct(){
        parent::__construct();
		$this->load->model("pelayanan_anc_model" , "pelayanan_anc");
        $this->base_url = $this->base_url_site."rekam-medis-ibu/";
        $this->date_now = date("Y-m-d H:i:s");
    }
    
    
    public function saveDataPelayananAnc(){
		if(!$this->input->is_ajax_request()){
			echo "Die, Ilegall Access";
			die;
		}
		
		$id = $this->input->post("intIdRekmedDetailAnc");
		$arrPost = array(
						"txtKeluhanPasien" => $this->input->post("txtKeluhanPasien"),
						"bitIsBukuKia"=> $this->input->post("bitIsBukuKia"),
						"txtBeratBadan"=> $this->input->post("txtBeratBadan"),
						"txtTD"=> $this->input->post("txtTD"),
						"txtNadi"=> $this->input->post("txtNadi"),
						"txtRR"=> $this->input->post("txtRR"),
						"txtAbdomen"=> $this->input->post("txtAbdomen"),
						"txtOedemTungkai"=> $this->input->post("txtOedemTungkai"),
						"txtTFU"=> $this->input->post("txtTFU"),
						"txtLetakJanin"=> $this->input->post("txtLetakJanin"),
						"txtDJJ"=> $this->input->post("txtDJJ"),
						"txtGerakJanin"=> $this->input->post("txtGerakJanin"),
						"txtUmurKehamilan"=> $this->input->post("txtUmurKehamilan"),
						"txtPenyuluhan"=> $this->input->post("txtPenyuluhan"),
						"txtPenunjang"=> $this->input->post("txtPenunjang"),
						"txtSkor"=> $this->input->post("txtSkor"),
						"txtKesimpulan"=> $this->input->post("txtKesimpulan"),
						"txtTerapi"=> $this->input->post("txtTerapi"),
						"txtRujukan"=> $this->input->post("txtRujukan")
					);
		if(empty($id)){
			$arrPost['dtCreatedDate'] = $this->date_now;
			$arrPost['dtLastUpdate'] = $this->date_now;
			$arrPost['intIdPegawai'] = $this->session->userdata("sipt_user_id");
			$arrPost['intIdRekamMedisIbu'] = $this->input->post("intIdRekamMedisIbu");
		}else{
			$arrPost['intIdPegawai'] = $this->session->userdata("sipt_user_id");
			$arrPost['dtLastUpdate'] = $this->date_now;
		}
		$retVal = $this->pelayanan_anc->saveUpdate($arrPost , $id);
		$this->setJsonOutput($retVal);
		
	}
    
    public function deleteDataPelayananANC(){
		if(!$this->input->is_ajax_request()){
			echo "Die, Ilegall Access";
			die;
		}
		$idPost = $this->input->post('idPelayananANC');
		$retVal = $this->pelayanan_anc->delete($idPost);
		$this->setJsonOutput($retVal);
	}
    
    public function getFrmPelayananANC($idRekmedIbu = "" , $intIdRekmedDetailAnc=""){
		if(!$this->input->is_ajax_request()){
			echo "Die, Ilegall Access";
			die;
		}
		$labelSave = "Simpan";
		$arrayVal = array(
						"txtKeluhanPasien",
						"bitIsBukuKia",
						"txtBeratBadan",
						"txtTD",
						"txtNadi",
						"txtRR",
						"txtAbdomen",
						"txtOedemTungkai",
						"txtTFU",
						"txtLetakJanin",
						"txtDJJ",
						"txtGerakJanin",
						"txtUmurKehamilan",
						"txtPenyuluhan",
						"txtPenunjang",
						"txtSkor",
						"txtKesimpulan",
						"txtTerapi",
						"txtRujukan",
					);
		$detailPelayananANC = array();
		if(!empty($intIdRekmedDetailAnc)){
			$detailPelayananANC = $this->pelayanan_anc->getDetail($intIdRekmedDetailAnc);
			$labelSave = "Update";
		}
		
		foreach($arrayVal as $rowVal){
			$$rowVal = isset($detailPelayananANC[$rowVal]) ? $detailPelayananANC[$rowVal] : "";
		}
		
		$this->form_builder->form_type = "inline";
		$dt['intIdRekmedDetailAnc'] = $this->form_builder->inputHidden("intIdRekmedDetailAnc" , $intIdRekmedDetailAnc);
		$dt['intIdRekamMedisIbu'] = $this->form_builder->inputHidden("intIdRekamMedisIbu" , $idRekmedIbu);
		
		//// Umum
		$dt['txtKeluhanPasien'] = $this->form_builder->inputTextArea("Keluhan Pasien" , "txtKeluhanPasien",$txtKeluhanPasien , "col-md-3" , array("rows" => "3"));
		$arrBukuKia = array("1"=>"Ya","0"=>"Tidak");
		$dt['bitIsBukuKia'] = $this->form_builder->inputRadioGroup("Bawa Buku KIA" , "bitIsBukuKia",$bitIsBukuKia,$arrBukuKia);
		$dt['txtBeratBadan'] = $this->form_builder->inputText("Berat Badan (kg)" , "txtBeratBadan",$txtBeratBadan);
		$dt['txtTD'] = $this->form_builder->inputText("TD (mmHg)" , "txtTD",$txtTD);
		$dt['txtNadi'] = $this->form_builder->inputText("Nadi" , "txtNadi",$txtNadi);
		$dt['txtRR'] = $this->form_builder->inputText("RR" , "txtRR",$txtRR);
		$dt['txtAbdomen'] = $this->form_builder->inputText("Abdomen" , "txtAbdomen",$txtAbdomen);
		$dt['txtOedemTungkai'] = $this->form_builder->inputText("Oedem Tungkai" , "txtOedemTungkai",$txtOedemTungkai);
		
		//// Kebidanan
		$dt['txtTFU'] = $this->form_builder->inputText("TFU" , "txtTFU",$txtTFU);
		$dt['txtLetakJanin'] = $this->form_builder->inputText("Letak Janin" , "txtLetakJanin",$txtLetakJanin);
		$dt['txtDJJ'] = $this->form_builder->inputText("DFF" , "txtDJJ",$txtDJJ);
		$arrGerakJanin = array("aktif"=>"Aktif","jarang"=>"Jarang");
		$dt['txtGerakJanin'] = $this->form_builder->inputDropdown("Gerak Janin" , "txtGerakJanin",$txtGerakJanin , $arrGerakJanin);
		$dt['txtUmurKehamilan'] = $this->form_builder->inputText("Umur Kehamilan" , "txtUmurKehamilan",$txtUmurKehamilan);
		
		//// Atribut Lain
		$dt['txtPenyuluhan'] = $this->form_builder->inputText("Penyuluhan" , "txtPenyuluhan",$txtPenyuluhan);
		$dt['txtPenunjang'] = $this->form_builder->inputText("Lab/Penunjang" , "txtPenunjang",$txtPenunjang);
		$dt['txtSkor'] = $this->form_builder->inputText("SKOR (KSPR)" , "txtSkor",$txtSkor);
		$dt['txtKesimpulan'] = $this->form_builder->inputText("Kesimpulan" , "txtKesimpulan",$txtKesimpulan);
		$dt['txtTerapi'] = $this->form_builder->inputText("Terapi / TT" , "txtTerapi",$txtTerapi);
		$dt['txtRujukan'] = $this->form_builder->inputText("Rujuk Ke" , "txtRujukan",$txtRujukan);
		
		$dt['buttonLabel'] = $labelSave;
		$retVal = $this->load->view("pelayanan/poli_ibu/form_pelayanan_anc" , $dt , true);
		echo $retVal;
	}
    
    	public function getDataPelayananANC(){
		if(!$this->input->is_ajax_request()){
			echo "Die, Ilegall Access";
			die;
		}
		
		$status = false;
		$html = "";
		$length =  $this->input->post("length");
		$start = $this->input->post("start");
		$id = $this->input->post("txtIdRekamMedis");
		$data_rekmed = $this->pelayanan_anc->getDataHistory($start , $this->limit , $id);
		$count_data = $this->pelayanan_anc->getCountDataHistory($id);
		
		if($count_data > 0){
			$retVal['draw'] = $this->input->post('draw');
			$retVal['recordsTotal'] = $count_data;
			$retVal['recordsFiltered'] = $count_data;
			$retVal['data'] = array();
			foreach($data_rekmed as $pasien){
				$btnDetail = '<button class="btn btn-flat btn-success" type="button" onclick="getDetailPelayananANC(\''.$pasien['intIdRekmedDetailAnc'].'\')"><i class="fa fa-list"></i> Detail</button>';
				$btnDelete = '<button class="btn btn-flat btn-danger" type="button" onclick="hapusDataPelayananANC(\''.$pasien['intIdRekmedDetailAnc'].'\')"><i class="fa fa-trash"></i> Hapus</button>';
				$btnAksi = $btnDetail . $btnDelete;
				$retVal['data'][] = array(
										  $pasien['dtCreatedDate'],
										  $pasien['txtKeluhanPasien'],
										  $pasien['txtSkor'],
										  $pasien['intIdPegawai'],
										  $btnAksi);
			}
		}else{
			$retVal['draw'] = $this->input->post('draw');
			$retVal['recordsTotal'] = 0;
			$retVal['recordsFiltered'] = 0;
			$retVal['data'] = array();
		}
		$this->setJsonOutput($retVal);
	}	
}