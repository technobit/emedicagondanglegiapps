<?php
class Riwayat_kehamilan_sekarang extends MY_Controller {
    var $base_url = "";
	var $limit = "20";
    var $menu = "K01";
    var $date_now = "";
    
	function __construct(){
        parent::__construct();
		$this->load->model("riwayat_sekarang_model" , "riwayat_sekarang");
        $this->base_url = $this->base_url_site."rekam-medis-ibu/";
        $this->date_now = date("Y-m-d H:i:s");
    }
    
    
  public function getFrmRiwayatSekarang($idRekmedIbu = "" ){
		if(!$this->input->is_ajax_request()){
			echo "Die, Ilegall Access";
			die;
		}
		
		$labelSave = "Simpan";
		$detailRiwayatKehamilanSekarang = array();
		$arrayVal = array(
						  "txtHaid",
						  "txtBBHamil",
						  "txtMual",
						  "txtPusing",
						  "txtNyeriPerut",
						  "txtGerakJanin",
						  "txtOedema",
						  "txtNafsuMakan",
						  "txtPendarahan",
						  "txtWaktuPendarahan",
						  "txtPenyakitDiDerita",
						  "txtRiwayatPenyakitKeluarga",
						  "txtKebiasaanIbu",
						  "txtStatusTT",
						  "txtFaktorResikoAids",
						  "txtHPL",
						  "txtSkorKSPR",
						  "txtKeluhanPasien",
						  "txtRujukanKe",
						  "bitStatus",
						);
		$dt['txtTanggalUpdate'] = "";
		if(!empty($idRekmedIbu)){
			
			$detailRiwayatKehamilanSekarang = $this->riwayat_sekarang->getDetail($idRekmedIbu);
			$labelSave = "Update";
			$tanggalUpdate = explode(" " ,$detailRiwayatKehamilanSekarang['dtLastUpdate']);
			$tanggalUpdate = indonesian_date($tanggalUpdate[0]);
			$dt['txtTanggalUpdate'] = $this->form_builder->inputText("Tanggal Update" , "txtTanggalUpdate",$tanggalUpdate,"col-sm-3" , array("readonly"=>"readonly"));
		}
		
		foreach($arrayVal as $rowVal){
			$$rowVal = isset($detailRiwayatKehamilanSekarang[$rowVal]) ? $detailRiwayatKehamilanSekarang[$rowVal] : "";
		}
		
		
		$dt['intIdRekamMedisIbu'] = $this->form_builder->inputHidden("intIdRekamMedisIbu" , $idRekmedIbu);
		$dt['txtFormMode'] = $this->form_builder->inputHidden("txtFormMode" , $labelSave);
		
		$arrKondisiHaid = array("teratur"=>"Teratur" , "tidak"=>"Tidak");
		$dt['txtHaid'] = $this->form_builder->inputDropdown("Haid" , "txtHaid",$txtHaid , $arrKondisiHaid);
		
		$dt['txtBBHamil'] = $this->form_builder->inputText("BB Sebelum Hamil" , "txtBBHamil",$txtBBHamil);
		
		$arrKondisiMual = array("tidak" => "Tidak" , "kadang"=>"Kadang-Kadang" , "terus"=>"Terus Menerus");
		$dt['txtMual'] = $this->form_builder->inputDropdown("Mual/Muntah" , "txtMual",$txtMual , $arrKondisiMual);
		
		$arrPusing = array("tidak" => "Tidak" , "kadang"=>"Kadang-Kadang" , "terus"=>"Terus Menerus");
		$dt['txtPusing'] = $this->form_builder->inputDropdown("Pusing" , "txtPusing",$txtPusing , $arrPusing);
		
		$arrNyeriPerut = array("tidak" => "Tidak Ada" , "ada"=>"Ada" );
		$dt['txtNyeriPerut'] = $this->form_builder->inputDropdown("Nyeri Perut" , "txtNyeriPerut",$txtNyeriPerut,$arrNyeriPerut);
		
		$arrGerakJanin = array("aktif" => "Aktif" , "ada"=>"Ada" );
		$dt['txtGerakJanin'] = $this->form_builder->inputDropdown("Gerak Janin" , "txtGerakJanin",$txtGerakJanin,$arrGerakJanin);
		
		$arrOedema = array("tidak_ada" => "Tidak Ada" , "ada_tibia"=>"Ada - Tibia" , "ada_umum" => "Ada - Umum" );
		$dt['txtOedema'] = $this->form_builder->inputDropdown("Oedema" , "txtOedema",$txtOedema,$arrOedema);
		
		$arrNafsuMakan = array("baik" => "Baik" , "menurun"=>"Menurun");
		$dt['txtNafsuMakan'] = $this->form_builder->inputDropdown("Nafsu Makan" , "txtNafsuMakan",$txtNafsuMakan,$arrNafsuMakan);
		
		$arrPendarahan = array("tidak_ada" => "Tidak Ada" , "ada"=>"Ada");
		$dt['txtPendarahan'] = $this->form_builder->inputDropdown("Pendarahan" , "txtPendarahan",$txtPendarahan,$arrPendarahan);
		
		$arrPenyakit = array("paru" => "Paru" ,
							 "dm"=>"DM",
							 "epilepsi"=>"Epilepsi",
							 "hati"=>"Hati",
							 "psikosis"=>"Psikosis",
							 "ginjal"=>"Ginjal",
							 "malaria"=>"Malaria",
							 "jantung"=>"Jantung",
							 "hipertensi"=>"Hipertensi",
							 "asthma"=>"Asthma",
							 "diare_lama"=>"Diare Lama",
							 "pms"=>"PMS",
							 );
		$txtPenyakitDiDerita = !empty($txtPenyakitDiDerita) ? explode("|",$txtPenyakitDiDerita) : array();
		$dt['txtPenyakitDiDerita'] = $this->form_builder->inputCheckboxGroup("Penyakit Yang Di Derita Bumil" , "txtPenyakitDiDerita",$txtPenyakitDiDerita,$arrPenyakit);
		
		$arrPenyakitKeluarga = array("hipertensi"=>"Hipertensi",
									"dm"=>"DM",
									"paru"=>"Paru-Paru",
									"jantung"=>"Jantung",
									"psikosis"=>"Psikosis",
									"gemeli"=>"Gemeli",
									);
		$txtRiwayatPenyakitKeluarga = !empty($txtRiwayatPenyakitKeluarga) ? explode("|",$txtRiwayatPenyakitKeluarga) : array();
		$dt['txtRiwayatPenyakitKeluarga'] = $this->form_builder->inputCheckboxGroup("Riwayat Penyakit Keluarga" , "txtRiwayatPenyakitKeluarga",$txtRiwayatPenyakitKeluarga,$arrPenyakitKeluarga);
		
		$arrKebiasaanIbu = array("merokok"=>"Merokok",
									"minuman_keras"=>"Minuman Keras",
									"narkotika"=>"Narkotika",
									"minum_jamu"=>"Minum Jamu",
									"pijat_perut"=>"Pijat Perut",
									);
		$txtKebiasaanIbu = !empty($txtKebiasaanIbu) ? explode("|",$txtKebiasaanIbu) : array();
		$dt['txtKebiasaanIbu'] = $this->form_builder->inputCheckboxGroup("Kebiasaan Ibu" , "txtKebiasaanIbu",$txtKebiasaanIbu,$arrKebiasaanIbu);
		
		$arrStatusTT = array(
							"-"=>"Pilih Status",
							"t0"=>"T0",
							"t1"=>"T1",
							"t2"=>"T2",
							"t3"=>"T3",
							"t4"=>"T4",
							"t5"=>"T5",
						   );
		
		$dt['txtStatusTT'] = $this->form_builder->inputDropdown("Status TT" , "txtStatusTT",$txtStatusTT,$arrStatusTT);
		
		$arrFaktorResikoAids = array(
							"tidak_ada"=>"Tidak Ada",
							"ada"=>"Ada",
						   );
		$dt['txtFaktorResikoAids'] = $this->form_builder->inputDropdown("Faktor Resiko Aids" , "txtFaktorResikoAids",$txtFaktorResikoAids,$arrFaktorResikoAids);
		
		/// Kanan
		$dt['txtHPL'] = $this->form_builder->inputText("HPL" , "txtHPL",$txtHPL);
		$dt['txtKeluhanPasien'] = $this->form_builder->inputTextArea("Keluhan Pengguna Layanan" , "txtKeluhanPasien",$txtKeluhanPasien);
		
		$arrSkorKSPR = array(
							"-" => "-Pilih Skor-",
							"rst"=>"RST",
							"rt"=>"RT",
							"rp"=>"RP",
						   );
		$dt['txtSkorKSPR'] = $this->form_builder->inputDropdown("Hasil Skor KSPR" , "txtSkorKSPR",$txtSkorKSPR,$arrSkorKSPR);
		$dt['txtRujukanKe'] = $this->form_builder->inputText("Rujukan" , "txtRujukanKe",$txtRujukanKe);
		$dt['buttonLabel'] = $labelSave;
		$retVal = $this->load->view("pelayanan/poli_ibu/form_riwayat_sekarang" , $dt , true);
		echo $retVal;
	}
    
    public function saveDataRiwayatSekarang(){
		if(!$this->input->is_ajax_request()){
			echo "Die, Ilegall Access";
			die;
		}
		$id = $this->input->post("intIdRekamMedisIbu");
		$txtPenyakitDiDerita = $this->input->post("txtPenyakitDiDerita");
		$txtPenyakitDiDerita = (!empty($txtPenyakitDiDerita) && is_array($txtPenyakitDiDerita)) ? implode("|" , $txtPenyakitDiDerita) : "";
		
		$txtRiwayatPenyakitKeluarga = $this->input->post("txtRiwayatPenyakitKeluarga");
		$txtRiwayatPenyakitKeluarga = !empty($txtRiwayatPenyakitKeluarga) ? implode("|" , $txtRiwayatPenyakitKeluarga) : "";
		
		$txtKebiasaanIbu = $this->input->post("txtKebiasaanIbu");
		$txtKebiasaanIbu = !empty($txtKebiasaanIbu) ? implode("|" , $txtKebiasaanIbu) : "";
		
		$arrPost = array(
					"txtHaid" => $this->input->post("txtHaid"),
					"txtBBHamil" => $this->input->post("txtBBHamil"),
					"txtMual" => $this->input->post("txtMual"),
					"txtPusing" => $this->input->post("txtPusing"),
					"txtNyeriPerut" => $this->input->post("txtNyeriPerut"),
					"txtGerakJanin" => $this->input->post("txtGerakJanin"),
					"txtOedema" => $this->input->post("txtOedema"),
					"txtNafsuMakan" => $this->input->post("txtNafsuMakan"),
					"txtPendarahan" => $this->input->post("txtPendarahan"),
					"txtWaktuPendarahan" => "",
					"txtPenyakitDiDerita" => $txtPenyakitDiDerita,
					"txtRiwayatPenyakitKeluarga" => $txtRiwayatPenyakitKeluarga,
					"txtKebiasaanIbu" => $txtKebiasaanIbu,
					"txtStatusTT" => $this->input->post("txtStatusTT"),
					"txtFaktorResikoAids" => $this->input->post("txtFaktorResikoAids"),
					"txtSkorKSPR" => $this->input->post("txtSkorKSPR"),
					"txtKeluhanPasien" => $this->input->post("txtKeluhanPasien"),
					"txtRujukanKe" => $this->input->post("txtRujukanKe"),
					"txtHPL" => $this->input->post("txtHPL"),
					"bitStatus" => 1,
				   );
		$formMode = $this->input->post("txtFormMode");
		$arrPost['intIdRekamMedisIbu'] = $id;
		if($formMode=="Simpan"){
			$arrPost['dtCreatedDate'] = $this->date_now;
			$arrPost['dtLastUpdate'] = $this->date_now;
		}else{
			$arrPost['dtLastUpdate'] = $this->date_now;
		}
		$retVal = $this->riwayat_sekarang->saveUpdate($arrPost , $id);
		$this->setJsonOutput($retVal);
	}
	
}
?>