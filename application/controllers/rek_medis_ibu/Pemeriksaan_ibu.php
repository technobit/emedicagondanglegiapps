<?php
class Pemeriksaan_ibu extends MY_Controller {
    var $base_url = "";
	var $limit = "20";
    var $menu = "K01";
    var $date_now = "";
    
	function __construct(){
        parent::__construct();
		$this->load->model("pemeriksaan_ibu_model" , "pemeriksaan_ibu");
        $this->base_url = $this->base_url_site."rekam-medis-ibu/";
        $this->date_now = date("Y-m-d H:i:s");
    }
    
    
    public function getFrmPemeriksaan($idRekmedIbu = ""){
		if(!$this->input->is_ajax_request()){
			echo "Die, Ilegall Access";
			die;
		}
		
		$labelSave = "Simpan";
		$detailPemeriksaan = array();
		$arrayVal = array(
						  "txtTB",
						  "txtLila",
						  "txtIMT",
						  "txtBentukTubuh",
						  "txtKesadaran",
						  "txtMuka",
						  "txtKulit",
						  "txtMata",
						  "txtMulut",
						  "txtGigi",
						  "txtPembesaranKel",
						  "txtDada",
						  "txtParuJantung",
						  "txtJantung",
						  "txtRefleks",
						  "txtPayudara",
						  "txtTungkai",
						  "bitStatus",
						);
		
		$dt['txtTanggalUpdate'] = "";
		if(!empty($idRekmedIbu)){
			
			$detailPemeriksaan = $this->pemeriksaan_ibu->getDetail($idRekmedIbu);
			if(!empty($detailPemeriksaan)){
				$labelSave = "Update";	
			}
			$tanggalUpdate = explode(" " ,$detailPemeriksaan['dtLastUpdate']);
			$tanggalUpdate = indonesian_date($tanggalUpdate[0]);
			$dt['txtTanggalUpdate'] = $this->form_builder->inputText("Tanggal Update" , "txtTanggalUpdate",$tanggalUpdate,"col-sm-3" , array("readonly"=>"readonly"));
		}
		
		foreach($arrayVal as $rowVal){
			$$rowVal = isset($detailPemeriksaan[$rowVal]) ? $detailPemeriksaan[$rowVal] : "";
		}
		
		$dt['intIdRekamMedisIbu'] = $this->form_builder->inputHidden("intIdRekamMedisIbu" , $idRekmedIbu);
		$dt['txtFormMode'] = $this->form_builder->inputHidden("txtFormMode" , $labelSave);
		
		$dt['txtTB'] = $this->form_builder->inputText("TB" , "txtTB",$txtTB);
		$dt['txtLila'] = $this->form_builder->inputText("LILA" , "txtLila",$txtLila);
		$dt['txtIMT'] = $this->form_builder->inputText("IMT" , "txtIMT",$txtIMT);
		
		$arrBentukTubuh = array(
							"normal"=>"Normal",
							"tl_belakang"=>"Kelainan Tulang Belakang",
							"tungkai"=>"Kelainan Tungkai",
							"panggul"=>"Kelainan Bentuk Panggul",
						  );
		$dt['txtBentukTubuh'] = $this->form_builder->inputDropdown("Bentuk Tubuh" , "txtBentukTubuh",$txtBentukTubuh , $arrBentukTubuh);
		
		$arrKesadaran = array(
							"baik"=>"Baik",
							"ada_gangguan"=>"Ada Gangguan",
						  );
		$dt['txtKesadaran'] = $this->form_builder->inputDropdown("Kesadaran" , "txtKesadaran",$txtKesadaran , $arrKesadaran);
		
		$arrMuka = array(
						"-"=>"Pilih Kondisi Wajah",
						"pucat"=>"Pucat",
						"kuning"=>"Kuning",
						);
		$dt['txtMuka'] = $this->form_builder->inputDropdown("Muka" , "txtMuka",$txtMuka , $arrMuka);
		
		$arrKulit = array(
						"ruam_kulit"=>"Ruam Kulit",
						"herpes"=>"Herpes",
						"sarcoma"=>"Sarcoma",
						"tatto"=>"Tatto",
						"bekas_sayatan"=>"Bekas Luka Sayatan",
						"bekas_jarum"=>"Bekas Tusukan Jarum",
						"dermailitis"=>"Dermalitis",
						);
		
		$txtKulit = !empty($txtKulit) ? explode("|" , $txtKulit) : array();
		$dt['txtKulit'] = $this->form_builder->inputCheckBoxGroup("Kulit" , "txtKulit",$txtKulit , $arrKulit);
		
		$arrMata = array(
						"normal"=>"Normal",
						"oedema_palpebra"=>"Oedema Palpebra",
						"conjunctiva_pucat"=>"Conjunctiva Pucat",
						"icterus"=>"Icterus",
						);
		$txtKulit = !empty($txtMata) ? explode("|" , $txtMata) : array();
		$dt['txtMata'] = $this->form_builder->inputDropdown("Mata" , "txtMata",$txtMata , $arrMata);
		
		$arrMulut = array(
						"normal"=>"Normal",
						"cyanosis"=>"Cyanosis",
						"slomatitis"=>"Slomatitis",
						"tonsilitis"=>"Tonsilitis",
						"faringitis"=>"Faringitis",
						);
		$dt['txtMulut'] = $this->form_builder->inputDropdown("Mulut" , "txtMulut",$txtMulut , $arrMulut);
		
		$arrGigi = array(
						"normal"=>"Normal",
						"gigi"=>"Gigi",
						);
		
		$dt['txtGigi'] = $this->form_builder->inputDropdown("Gigi" , "txtGigi",$txtGigi , $arrGigi);
		
		$arrPembesaranKel = array(
						"leher"=>"Leher",
						"ketiak"=>"Ketiak",
						"lipatan_paha"=>"Lipatan Paha",
						"tiroid"=>"Tiroid",
						);
		
		$txtPembesaranKel = !empty($txtPembesaranKel) ? explode("|" , $txtPembesaranKel) : array();
		$dt['txtPembesaranKel'] = $this->form_builder->inputCheckBoxGroup("Pembesaran Kelenjar" , "txtPembesaranKel",$txtPembesaranKel , $arrPembesaranKel);
		
		$arrDada = array(
						"normal"=>"Normal",
						"abnormal"=>"Abnormal",
						);
		
		$dt['txtDada'] = $this->form_builder->inputDropdown("Dada" , "txtDada",$txtDada , $arrDada);
		
		$arrParu = array(
						"normal"=>"Normal",
						"sesak"=>"Sesak",
						);
		
		$dt['txtParuJantung'] = $this->form_builder->inputDropdown("Paru/jantung" , "txtParuJantung",$txtParuJantung , $arrParu);
		
		$arrJantung = array(
						"tidak_ada"=>"Tidak Ada",
						"berdebar"=>"Berdebar-debar",
						"sesak_napas"=>"Mudah Sesak Napas",
						);
		
		$dt['txtJantung'] = $this->form_builder->inputDropdown("Jantung" , "txtJantung",$txtJantung , $arrJantung);
		
		$arrPayudara = array(
						"normal"=>"Normal",
						"kemerahan"=>"Kemerahan",
						"benjolan"=>"Benjolan",
						"puting_masuk"=>"Puting Susu Masuk",
						"kulit_jeruk"=>"Kulit Jeruk",
						"keluar_cairan"=>"Keluar Cairan",
						);
		
		$dt['txtPayudara'] = $this->form_builder->inputDropdown("Payudara" , "txtPayudara",$txtPayudara , $arrPayudara);
		
		$arrTungkai = array(
						"normal"=>"Normal",
						"oedema"=>"Oedema",
						);
		
		$dt['txtTungkai'] = $this->form_builder->inputDropdown("Tungkai" , "txtTungkai",$txtTungkai , $arrTungkai);
		
		$arrRefleks = array(
						"tidak_ada"=>"Tidak Ada",
						"ada"=>"Ada",
						);
		
		$dt['txtRefleks'] = $this->form_builder->inputDropdown("Refleks" , "txtRefleks",$txtRefleks , $arrRefleks);
		$dt['buttonLabel'] = $labelSave;
		$retVal = $this->load->view("pelayanan/poli_ibu/form_pemeriksaan" , $dt , true);
		echo $retVal;
	}
    
    public function saveDataPemeriksaan(){
		if(!$this->input->is_ajax_request()){
			echo "Die, Ilegall Access";
			die;
		}
				
		$id = $this->input->post("intIdRekamMedisIbu");
		$txtKulit = $this->input->post("txtKulit");
		$txtKulit = !empty($txtKulit) ? implode("|" , $txtKulit) : "";
		
		$txtPembesaranKel = $this->input->post("txtPembesaranKel");
		$txtPembesaranKel = !empty($txtPembesaranKel) ? implode("|" , $txtPembesaranKel) : "";
		
		$arrPost = array(
					"txtTB" => $this->input->post("txtTB"),
					"txtLila" => $this->input->post("txtLila"),
					"txtIMT" => $this->input->post("txtIMT"),
					"txtBentukTubuh" => $this->input->post("txtBentukTubuh"),
					"txtKesadaran" => $this->input->post("txtKesadaran"),
					"txtMuka" => $this->input->post("txtMuka"),
					"txtKulit" => $txtKulit,
					"txtMata" => $this->input->post("txtMata"),
					"txtMulut" => $this->input->post("txtMulut"),
					"txtGigi" => $this->input->post("txtGigi"),
					"txtPembesaranKel" => $txtPembesaranKel,
					"txtDada" => $this->input->post("txtDada"),
					"txtParuJantung" => $this->input->post("txtParuJantung"),
					"txtJantung" => $this->input->post("txtJantung"),
					"txtRefleks" => $this->input->post("txtRefleks"),
					"txtPayudara" => $this->input->post("txtPayudara"),
					"txtTungkai" => $this->input->post("txtTungkai"),
					"bitStatusPemeriksaan" => 1,
				   );
		$formMode = $this->input->post("txtFormMode");
		if($formMode=="Simpan"){
			$arrPost['dtCreatedDate'] = $this->date_now;
			$arrPost['dtLastUpdate'] = $this->date_now;
			$arrPost['intIdRekamMedisIbu'] = $id;
		}else{
			$arrPost['dtLastUpdate'] = $this->date_now;
		}
		$retVal = $this->pemeriksaan_ibu->saveUpdate($arrPost , $id);
		$this->setJsonOutput($retVal);
	}
	
	
}
?>