<?php
class Pelayanan_nifas extends MY_Controller {
    var $meta_title = "Rekam Medis Ibu";
    var $meta_desc = "Rekam Medis Ibu";
	var $main_title = "Rekam Medis Ibu";
    var $base_url = "";
	var $limit = "20";
    var $menu = "K01";
    var $date_now = "";
    
	function __construct(){
        parent::__construct();
		$this->load->model("pelayanan_nifas_model" , "pelayanan_nifas");
        $this->base_url = $this->base_url_site."rekam-medis-ibu/";
        $this->date_now = date("Y-m-d H:i:s");
    }

    public function deleteDataPelayananNifas(){
		if(!$this->input->is_ajax_request()){
			echo "Die, Ilegall Access";
			die;
		}
		
		$id = $this->input->post('idPelayananNifas');
		$retVal = $this->pelayanan_nifas->delete($id);
		$this->setJsonOutput($retVal);
	}
    
    public function getDataPelayananNifas(){
		if(!$this->input->is_ajax_request()){
			echo "Die, Ilegall Access";
			die;
		}
		$length =  $this->input->post("length");
		$start = $this->input->post("start");
		$id = $this->input->post("txtIdRekamMedis");
		$data_rekmed = $this->pelayanan_nifas->getDataHistory($start , $this->limit , $id);
		$count_data = $this->pelayanan_nifas->getCountDataHistory($id);
		
		if($count_data > 0){
			$retVal['draw'] = $this->input->post('draw');
			$retVal['recordsTotal'] = $count_data;
			$retVal['recordsFiltered'] = $count_data;
			$retVal['data'] = array();
			foreach($data_rekmed as $pasien){
				$btnDetail = '<button class="btn btn-flat btn-success" type="button" onclick="getDetailPelayananNifas(\''.$pasien['intIdNifasIbu'].'\')"><i class="fa fa-list"></i> Detail</button>';
				$btnDelete = '<button class="btn btn-flat btn-danger" type="button" onclick="hapusDataPelayananNifas(\''.$pasien['intIdNifasIbu'].'\')"><i class="fa fa-trash"></i> Hapus</button>';
				$btnAksi = $btnDetail . $btnDelete;
				$retVal['data'][] = array(
										  $pasien['dtCreatedDate'],
										  $pasien['txtKeluhanPasien'],
										  $pasien['intIdPegawai'],
										  $pasien['txtTindakan'],
										  $btnAksi);
			}
		}else{
			$retVal['draw'] = $this->input->post('draw');
			$retVal['recordsTotal'] = 0;
			$retVal['recordsFiltered'] = 0;
			$retVal['data'] = array();
		}
		$this->setJsonOutput($retVal);
	}
    
    public function saveDataPelayananNifas(){
		if(!$this->input->is_ajax_request()){
			echo "Die, Ilegall Access";
			die;
		}
		
		$id = $this->input->post("intIdNifasIbu");
		$arrPost = array(
					"txtKeluhanPasien"=>$this->input->post("txtKeluhanPasien"),
					"txtTD"=>$this->input->post("txtTD"),
					"txtNadi"=>$this->input->post("txtNadi"),
					"txtRR"=>$this->input->post("txtRR"),
					"txtSuhu"=>$this->input->post("txtSuhu"),
					"txtKontraksiRahim"=>$this->input->post("txtKontraksiRahim"),
					"txtPendarahan"=>$this->input->post("txtPendarahan"),
					"txtLochia"=>$this->input->post("txtLochia"),
					"txtBAB"=>$this->input->post("txtBAB"),
					"txtBAK"=>$this->input->post("txtBAK"),
					"txtASI"=>$this->input->post("txtASI"),
					"txtTindakan"=>$this->input->post("txtTindakan"),
				);
		
		if(empty($id)){
			$arrPost['dtCreatedDate'] = $this->date_now;
			$arrPost['dtLastUpdate'] = $this->date_now;
			$arrPost['intIdRekamMedisIbu'] = $this->input->post("intIdRekamMedisIbu");
			$arrPost['intIdPegawai'] = $this->session->userdata("sipt_user_id");
		}else{
			$arrPost['dtLastUpdate'] = $this->date_now;
			$arrPost['intIdPegawai'] = $this->session->userdata("sipt_user_id");
		}
		$retVal = $this->pelayanan_nifas->saveUpdate($arrPost , $id);
		$this->setJsonOutput($retVal);
	}
    
    public function getFrmPelayananNifas($idRekmedIbu = "" , $intIdNifasIbu = ""){
		if(!$this->input->is_ajax_request()){
			echo "Die, Ilegall Access";
			die;
		}
		$labelSave = "Simpan";
		$arrayVal = array(
						"txtKeluhanPasien",
						"txtTD",
						"txtNadi",
						"txtRR",
						"txtSuhu",
						"txtKontraksiRahim",
						"txtPendarahan",
						"txtLochia",
						"txtBAB",
						"txtBAK",
						"txtASI",
						"txtTindakan",
					);
		$detailPelayananNifas = array();
		if(!empty($intIdNifasIbu)){
			$detailPelayananNifas = $this->pelayanan_nifas->getDetail($intIdNifasIbu);
			$labelSave = "Update";
		}
		
		foreach($arrayVal as $rowVal){
			$$rowVal = isset($detailPelayananNifas[$rowVal]) ? $detailPelayananNifas[$rowVal] : "";
		}
		
		$this->form_builder->form_type = "inline";
		$dt['intIdNifasIbu'] = $this->form_builder->inputHidden("intIdNifasIbu" , $intIdNifasIbu);
		$dt['intIdRekamMedisIbu'] = $this->form_builder->inputHidden("intIdRekamMedisIbu" , $idRekmedIbu);
		
		$dt['txtKeluhanPasien'] = $this->form_builder->inputTextArea("Keluhan Pengguna Layanan" , "txtKeluhanPasien",$txtKeluhanPasien , "col-md-3" , array("rows" => "3"));
		$dt['txtTD'] = $this->form_builder->inputText("TD (mmHg)" , "txtTD",$txtTD);
		$dt['txtNadi'] = $this->form_builder->inputText("Nadi" , "txtNadi",$txtNadi);
		$dt['txtRR'] = $this->form_builder->inputText("RR" , "txtRR",$txtRR);
		$dt['txtSuhu'] = $this->form_builder->inputText("Suhu" , "txtSuhu",$txtSuhu);
		$dt['txtKontraksiRahim'] = $this->form_builder->inputText("Kontraksi Rahim" , "txtKontraksiRahim",$txtKontraksiRahim);
		$dt['txtPendarahan'] = $this->form_builder->inputText("Pendarahan" , "txtPendarahan",$txtPendarahan);
		$dt['txtLochia'] = $this->form_builder->inputText("Lochia" , "txtLochia",$txtLochia);
		$dt['txtBAB'] = $this->form_builder->inputText("BAB" , "txtBAB",$txtBAB);
		$dt['txtBAK'] = $this->form_builder->inputText("BAK" , "txtBAK",$txtBAK);
		$dt['txtASI'] = $this->form_builder->inputText("ASI" , "txtASI",$txtASI);
		$dt['txtTindakan'] = $this->form_builder->inputTextArea("Tindakan" , "txtTindakan",$txtTindakan , "col-md-3" , array("rows" => "3"));
		$dt['buttonLabel'] = $labelSave;
		$retVal = $this->load->view("pelayanan/poli_ibu/form_pelayanan_nifas" , $dt , true);
		echo $retVal;
		
	}
    
}
?>