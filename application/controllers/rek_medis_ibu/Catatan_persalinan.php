<?php
class Catatan_persalinan extends MY_Controller {
    var $base_url = "";
	var $limit = "20";
    var $menu = "K01";
    var $date_now = "";
    
	function __construct(){
        parent::__construct();
		$this->load->model("persalinan_ibu_model" , "persalinan_ibu");
        $this->base_url = $this->base_url_site."rekam-medis-ibu/";
        $this->date_now = date("Y-m-d H:i:s");
    }
    
    public function getFrmCatatanPersalinan($idRekmedIbu = ""){
		if(!$this->input->is_ajax_request()){
			echo "Die, Ilegall Access";
			die;
		}
		$labelSave = "Simpan";
		$arrayVal = array(
						  "txtTempatPersalinan",
						  "txtTanggalPersalinan",
						  "txtPenolong",
						  "txtNamaPenolong",
						  "txtProsesPersalinan",
						  "txtKomplikasiPersalinan",
						  "txtBeratBBL",
						  "txtPBBBL",
						  "txtLK",
						  "txtIsHidup",
						  "txtKeadaanIbu",
						  );
		$dt['txtTanggalUpdate'] = "";
		
		$detailPersalinan = $this->persalinan_ibu->getDetail($idRekmedIbu);
		if(!empty($detailPelayananNifas)){
			$labelSave = "Update";
			$tanggalUpdate = explode(" " ,$detailPersalinan['dtLastUpdate']);
			$tanggalUpdate = indonesian_date($tanggalUpdate[0]);
			$dt['txtTanggalUpdate'] = $this->form_builder->inputText("Tanggal Update" , "txtTanggalUpdate",$tanggalUpdate,"col-sm-3" , array("readonly"=>"readonly"));	
		}
		
		foreach($arrayVal as $rowVal){
			$$rowVal = isset($detailPersalinan[$rowVal]) ? $detailPersalinan[$rowVal] : "";
		}
		$this->form_builder->form_type = "inline";
		$dt['intIdRekamMedisIbu'] = $this->form_builder->inputHidden("intIdRekamMedisIbu" , $idRekmedIbu);
		$dt['txtFormMode'] = $this->form_builder->inputHidden("txtFormMode" , $labelSave);
		
		$arrTempatPersalinan = array(
							   "rumah_pasien"=>"Rumah Pengguna Layanan",
							   "polindes"=>"Polindes",
							   "pustu"=>"Pustu",
							   "pkm"=>"PKM",
							   "pkm_poned"=>"PKM Poned Bululawang",
							   );
		$dt['txtTempatPersalinan'] = $this->form_builder->inputDropdown("Tempat Persalinan" , "txtTempatPersalinan",$txtTempatPersalinan , $arrTempatPersalinan);
		$dt['txtTanggalPersalinan'] = $this->form_builder->inputText("Tanggal Persalinan" , "txtTanggalPersalinan",$txtTanggalPersalinan);
		$arrPenolong = array(
							   "bidan"=>"Bidan",
							   "dokter"=>"Dokter",
							   "spog"=>"SPOG",
							   "dukun"=>"Dukun",
							   "lain"=>"Lain-lain",
							   );
		$dt['txtPenolong'] = $this->form_builder->inputDropdown("Penolong" , "txtPenolong",$txtPenolong , $arrPenolong);
		$dt['txtNamaPenolong'] = $this->form_builder->inputText("Nama Penolong" , "txtNamaPenolong",$txtNamaPenolong);
		
		$arrProsesPersalinan = array(
							   "normal"=>"Normal",
							   "drip"=>"DRIP",
							   "vakum"=>"Vakum",
							   "curetage"=>"Curetage",
							   "plas_manual"=>"Plas Manual",
							   "sc"=>"SC",
							   "imd"=>"IMD",
							   );
		$dt['txtProsesPersalinan'] = $this->form_builder->inputDropdown("Proses Persalinan" , "txtProsesPersalinan",$txtProsesPersalinan , $arrProsesPersalinan);
		
		$arrKomplikasiPersalinan = array(
						"I"=>"I",
						"P"=>"P",
						"S"=>"S",
						"PE"=>"PE",
						"eklampsia"=>"Eklampsia",
						"partus_lama"=>"Partus Lama",
						"infeksi"=>"Infeksi",
						"perdarahan"=>"Perdarahan",
						);
		
		$txtKomplikasiPersalinan = !empty($txtKomplikasiPersalinan) ? explode("|" , $txtKomplikasiPersalinan) : array();
		$dt['txtKomplikasiPersalinan'] = $this->form_builder->inputCheckBoxGroup("Komplikasi Persalinan" , "txtKomplikasiPersalinan",$txtKomplikasiPersalinan , $arrKomplikasiPersalinan);
		
		$dt['txtBeratBBL'] = $this->form_builder->inputText("Keadaan BBL - BB" , "txtBeratBBL",$txtBeratBBL);
		$dt['txtPBBBL'] = $this->form_builder->inputText("Keadaan BBL - PB" , "txtPBBBL",$txtPBBBL);
		$dt['txtLK'] = $this->form_builder->inputText("Keadaan BBL - LK" , "txtLK",$txtLK);
		
		$arrKondisiBayi = array("1" => "Hidup" , "0"=>"Mati");
		$dt['txtIsHidup'] = $this->form_builder->inputDropdown("Keadaan BBL - Kondisi" , "txtIsHidup",$txtIsHidup , $arrKondisiBayi);
		
		$arrKondisiIbu = array("1" => "Hidup" , "0"=>"Mati");
		$dt['txtKeadaanIbu'] = $this->form_builder->inputDropdown("Keadaan Ibu" , "txtKeadaanIbu",$txtKeadaanIbu , $arrKondisiIbu);
		
		$dt['buttonLabel'] = $labelSave;
		$retVal = $this->load->view("pelayanan/poli_ibu/form_catatan_persalinan" , $dt , true);
		echo $retVal;
	}
    
    public function saveDataCatatanPersalinan(){
		if(!$this->input->is_ajax_request()){
			echo "Die, Ilegall Access";
			die;
		}
		$id = $this->input->post("intIdRekamMedisIbu");
		///echopre($_POST);
		$txtKomplikasiPersalinan = $this->input->post("txtKomplikasiPersalinan");
		$txtKomplikasiPersalinan = !empty($txtKomplikasiPersalinan) ? implode("|" , $txtKomplikasiPersalinan) : "";
		
		$arrPost = array(
						  "txtTempatPersalinan"=>$this->input->post("txtTempatPersalinan"),
						  "txtTanggalPersalinan"=>$this->input->post("txtTanggalPersalinan"),
						  "txtPenolong"=>$this->input->post("txtPenolong"),
						  "txtNamaPenolong"=>$this->input->post("txtNamaPenolong"),
						  "txtProsesPersalinan"=>$this->input->post("txtProsesPersalinan"),
						  "txtKomplikasiPersalinan"=>$txtKomplikasiPersalinan,
						  "txtBeratBBL"=>$this->input->post("txtBeratBBL"),
						  "txtPBBBL"=>$this->input->post("txtPBBBL"),
						  "txtLK"=>$this->input->post("txtLK"),
						  "txtIsHidup"=>$this->input->post("txtIsHidup"),
						  "txtKeadaanIbu"=>$this->input->post("txtKeadaanIbu"),
						  );
		$formMode = $this->input->post("txtFormMode");
		if($formMode=="Simpan"){
			$arrPost['dtCreatedDate'] = $this->date_now;
			$arrPost['dtLastUpdate'] = $this->date_now;
			$arrPost['intIdRekamMedisIbu'] = $id;
		}else{
			$arrPost['dtLastUpdate'] = $this->date_now;
		}
		$retVal = $this->persalinan_ibu->saveUpdate($arrPost , $id);
		$this->setJsonOutput($retVal);
	}
	
}
?>