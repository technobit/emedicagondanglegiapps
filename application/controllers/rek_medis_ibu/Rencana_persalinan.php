<?php
class Rencana_persalinan extends MY_Controller {
    var $base_url = "";
	var $limit = "20";
    var $menu = "K01";
    var $date_now = "";
    
	function __construct(){
        parent::__construct();
		$this->load->model("rencana_persalinan_model" , "rencana_persalinan");
        $this->base_url = $this->base_url_site."rekam-medis-ibu/";
        $this->date_now = date("Y-m-d H:i:s");
    }
    
    public function getFrmRencanaPersalinan($idRekmedIbu = ""){
		if(!$this->input->is_ajax_request()){
			echo "Die, Ilegall Access";
			die;
		}
		
		$labelSave = "Simpan";
		$detailRencanaPersalinan = array();
		$arrayVal = array(
						  "txtGolDarahIbu",
						  "txtPenolong",
						  "txtTempat",
						  "txtPendamping",
						  "txtCalonDonor",
						  "txtStickerP4K",
						  "dtTanggalPasang",
						  "txtKesimpulan",
						  "bitStatus",
						);
		
		$dt['txtTanggalUpdate'] = "";
		
		$detailRencanaPersalinan = $this->rencana_persalinan->getDetail($idRekmedIbu);
		if(!empty($detailRencanaPersalinan)){
			$labelSave = "Update";
			$tanggalUpdate = explode(" " ,$detailRencanaPersalinan['dtLastUpdate']);
			$tanggalUpdate = indonesian_date($tanggalUpdate[0]);
			$dt['txtTanggalUpdate'] = $this->form_builder->inputText("Tanggal Update" , "txtTanggalUpdate",$tanggalUpdate,"col-sm-3" , array("readonly"=>"readonly"));	
		}
		
		foreach($arrayVal as $rowVal){
			$$rowVal = isset($detailRencanaPersalinan[$rowVal]) ? $detailRencanaPersalinan[$rowVal] : "";
		}
		
		$dt['intIdRekamMedisIbu'] = $this->form_builder->inputHidden("intIdRekamMedisIbu" , $idRekmedIbu);
		$dt['txtFormMode'] = $this->form_builder->inputHidden("txtFormMode" , $labelSave);
		
		$dt['txtGolDarahIbu'] = $this->form_builder->inputText("Golongan Darah Ibu" , "txtGolDarahIbu",$txtGolDarahIbu);
		$dt['txtPenolong'] = $this->form_builder->inputText("Penolong" , "txtPenolong",$txtPenolong);
		$dt['txtTempat'] = $this->form_builder->inputText("Tempat" , "txtTempat",$txtTempat);
		$dt['txtPendamping'] = $this->form_builder->inputText("Pendamping" , "txtPendamping",$txtPendamping);
		$dt['txtCalonDonor'] = $this->form_builder->inputText("Calon Donor" , "txtCalonDonor",$txtCalonDonor);
		$arrSticker = array(
							"+"=>"+",
							"-"=>"-"
							);
		
		$dt['txtStickerP4K'] = $this->form_builder->inputRadioGroup("Sticker P4K" , "txtStickerP4K",$txtStickerP4K,$arrSticker);
		$dt['dtTanggalPasang'] = $this->form_builder->inputText("Tanggal Pasang" , "dtTanggalPasang",$dtTanggalPasang);
		$dt['txtKesimpulan'] = $this->form_builder->inputTextArea("Kesimpulan" , "txtKesimpulan",$txtKesimpulan);
		$dt['buttonLabel'] = $labelSave;
		$retVal = $this->load->view("pelayanan/poli_ibu/form_rencana_persalinan" , $dt , true);
		echo $retVal;
	}
	
    public function saveDataRencanaPersalinan(){
		if(!$this->input->is_ajax_request()){
			echo "Die, Ilegall Access";
			die;
		}
		$id = $this->input->post("intIdRekamMedisIbu");
		$arrPost = array(
					"txtGolDarahIbu" => $this->input->post("txtGolDarahIbu"),
					"txtPenolong" => $this->input->post("txtPenolong"),
					"txtTempat" => $this->input->post("txtTempat"),
					"txtPendamping" => $this->input->post("txtPendamping"),
					"txtCalonDonor" => $this->input->post("txtCalonDonor"),
					"txtStickerP4K" => $this->input->post("txtStickerP4K"),
					"dtTanggalPasang" => $this->input->post("dtTanggalPasang"),
					"txtKesimpulan" => $this->input->post("txtKesimpulan"),
					"bitStatus" => 1,
				);
		$formMode = $this->input->post("txtFormMode");
		if($formMode=="Simpan"){
			$arrPost['dtCreatedDate'] = $this->date_now;
			$arrPost['dtLastUpdate'] = $this->date_now;
			$arrPost['intIdRekamMedisIbu'] = $id;
		}else{
			$arrPost['dtLastUpdate'] = $this->date_now;
		}
		$retVal = $this->rencana_persalinan->saveUpdate($arrPost , $id);
		$this->setJsonOutput($retVal);
	}
	
	
}
?>