<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MY_Controller {
    var $meta_title = "Login";
    var $meta_desc = "Admin SIPT";
	var $main_title = "Login";
    var $base_url = "";
	var $upload_dir = "";
	var $upload_url = "";
	var $limit = "10";
    
    public function __construct(){
        parent::__construct();
        $this->base_url = base_url()."login/";
		$this->load->model("users_model");
		$this->load->model("level_model");
		$this->load->model("level_menu_model");
		$this->load->model("kecamatan_model");
		$this->lang->load('id_site', 'id');
    }
    
	public function index()
	{
        $res = "";
        if(isset($_POST['submit'])){
            $res = $this->checkLogin();
        }

        $dt["alert"] = $res;
		$dt["forget_link"] = $this->base_url."lupa-password/";
		$this->load->view("login/content" , $dt);
	}
	
    public function checkLogin(){
		
        $res = array();
        $email_user = $this->security->xss_clean($this->input->post("email_user"));
        $password_user = $this->security->xss_clean($this->input->post("password_user"));
		
		$resVal = $this->users_model->getDataLogin($email_user , $password_user);
		$password_user = "3M3dic4".$password_user."st3v3j0b5";
		$password_user = md5($password_user);
		
		if(count($resVal) < 1){
			$retVal['status'] = false;
			$retVal['message'] = "Anda Tidak Terdaftar";
			return $retVal;
			exit();
		}
		
		$retuser = $resVal[0];
		if($retuser['bitStatusUser']==0){
			$retVal['status'] = false;
			$retVal['message'] = "Login Gagal, Akun Anda Tidak Aktif";
			return $retVal;
			exit();
		}else{
			if($retuser['bitStatusUser']==1){
				$password = $retuser['txtPassword'];
				if($password!=$password_user){
					$retVal['status'] = false;
					$retVal['message'] = "Login Gagal, Password Anda Tidak Cocok";
					return $retVal;
					exit();
				}

				if($retuser['bitStatusLevel']!=1){
					$retVal['status'] = false;
					$retVal['message'] = "Login Gagal, Level / Grup User Tidak Aktif";
					return $retVal;
					exit();
				}

				$resPengaturanPrinter = $this->users_model->dataPengaturanPrinter();
				
				$resPengaturanProfil = $this->users_model->getDataProfil();
				$resPengaturanBpjs = $this->users_model->getDataBpjs();
					$idLevel = $retuser["intIdLevel"];
					$arrSession = array(
								"sipt_user_id" => $retuser["intIdUser"],
								"sipt_username" => $retuser["txtUserName"],
                                "sipt_email" => $retuser["txtEmail"],
								"sipt_level" => $idLevel,
								"sipt_text_level" =>$retuser['txtLevel'],
                                "sipt_is_active" => $retuser["bitStatusUser"],
								"sipt_id_kabupaten" => $retuser["intIdKabupaten"],
								"sipt_id_kecamatan" => $retuser["intIdKecamatan"],
								"sipt_id_pegawai" => $retuser["intIdPegawai"],
								"sipt_user_validated" => true,
								"sipt_user_language" => "ID", /// Default To Indonesian,  Or Gabon Maybe
								);
					
					$resKecamatan = $this->kecamatan_model->getDataKecamatan($retuser['intIdKabupaten'],"IDKecamatan,Nama");
					$resLevelMenu = $this->level_menu_model->getMenuByIdLevel($idLevel);

					$arrMenu = array();
					foreach ($resLevelMenu as $valMenu) {
						# code... For Akses Menu
						$arrMenu[$valMenu['txtMenu']] = array(
							'is_insert'=>$valMenu['bitIsInsert'],
							'is_update'=>$valMenu['bitIsUpdate'],
							'is_delete'=>$valMenu['bitIsUpdate'],
							);
					}
					$arrSession['pengaturan_profil'] = $resPengaturanProfil;
					$arrSession['pengaturan_bpjs'] = $resPengaturanBpjs;
					
					/*
					$arrSession['printer_loket'] = $resPengaturanPrinter['printerLoket'];
					$arrSession['printer_self_service'] = $resPengaturanPrinter['printerSelfService'];
					$arrSession['printer_kasir'] = $resPengaturanPrinter['printerKasir'];
					*/
					$arrSession['printer_loket'] = "smb://".$resPengaturanPrinter['printerLoketProfile']."@".$resPengaturanPrinter['printerLoketName']."/".$resPengaturanPrinter['printerLoketPrinterName'];
					$arrSession['printer_self_service'] = "smb://".$resPengaturanPrinter['printerSelfServiceProfile']."@".$resPengaturanPrinter['printerSelfServiceName']."/".$resPengaturanPrinter['printerSelfServicePrinterName'];
					$arrSession['printer_kasir'] = "smb://".$resPengaturanPrinter['printerKasirProfile']."@".$resPengaturanPrinter['printerKasirName']."/".$resPengaturanPrinter['printerKasirPrinterName'];
					$arrSession['sipt_session_menu'] = $arrMenu;
					$arrSession['sipt_kecamatan'] = $resKecamatan;		
					$redirect_url = base_url()."dashboard/";
					$this->session->set_userdata($arrSession);
                    redirect($redirect_url);
			}else{
				$retVal['status'] = false;
				$retVal['message'] = "Akun Anda Tidak Aktif";
				return $retVal;
				exit();
			}
		}
    }
	
    public function logout(){
		$this->session->sess_destroy();
		redirect("/");
	}
    	
}
