<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laboratorium extends MY_Controller {
    
    var $meta_title = "Laboratorium";
    var $meta_desc = "Ruangan Umum";
	var $main_title = "Laboratorium";
    var $base_url = "";
	var $base_url_pasien = "";
	var $upload_dir = "";
	var $upload_url = "";
	var $limit = "10";
	var $arr_level_user = "";
    var $menu = "laboratorium";
	var $jenis_poli = "";
	var $jenis_pemeriksaan = "";
    public function __construct(){
        parent::__construct();
        $this->base_url = $this->base_url_site."pelayanan/laboratorium/";
		$this->load->model("pelayanan_model");
        $this->load->model("register_model");
        $this->load->model("pasien_model");
		$this->load->model("tindakan_model");
		$this->load->model("laboratorium_model");
		$this->load->model("rekam_medis_model" , "rekam_medis");
		$this->load->model("jaminan_kesehatan_model" , "jaminan_kesehatan");
		$this->jenis_pemeriksaan = $this->config->item("jenis_pemeriksaan_lab");
    } 
    
    public function index($id_pelayanan){
        
        
		$dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
			"container" => $this->_build_laboratorium( $id_pelayanan),
			"menu_key" => $this->menu,
			"akses_key" => "is_view",
			"custom_js" => array(
				ASSETS_JS_URL."pelayanan/laboratorium/content.js",
			),
            "custom_css" => array(
				
			),
		);	
		$this->_render("default",$dt);	
    }
	
    public function detail($id_kunjungan){
        
		$menu = "LD01";
		$akses_key = !empty($id_kunjungan) ? "is_update" : "is_insert";
		$dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
			"menu_key" => $this->menu,
			"akses_key" => $akses_key,
			"container" => $this->_build_detail_laboratorium( $id_kunjungan),
			"custom_js" => array(
				ASSETS_URL."plugins/select2/select2.js",
				ASSETS_URL."plugins/validate/jquery.validate_1.11.1.min.js",
				ASSETS_JS_URL."pelayanan/laboratorium/form.js",
			),
            "custom_css" => array(
				ASSETS_URL."plugins/select2/select2.css",
			),
		);	
		$this->_render("default",$dt);	
    }
	
	private function _build_laboratorium($id_pelayanan){
		$dt = array();
        $detail_pelayanan = $this->pelayanan_model->getDetail($id_pelayanan);
        $nama_poli = $detail_pelayanan['txtNama'];
        $this->meta_title = $nama_poli;
        $this->meta_desc = "Fasilitas ".$nama_poli;
		$breadcrumbs = array(
                        "Home" =>$this->base_url_site,
                        "Laboratorium" => $this->base_url,
                        "Data Pelayanan ".$nama_poli => "#",
                        );
        
		$dt['breadcrumbs'] = $this->setBreadcrumbs($breadcrumbs);
		$dt['title'] = $nama_poli;
        $dt['frm_tanggal'] = $this->form_builder->inputText("Tanggal" , "txtTanggal" , date("Y-m-d") , "col-sm-2" , array("class"=> "form-control datepicker"));
        $dt['frm_id_poliumum'] = $this->form_builder->inputHidden("inputIdPoli" , $id_pelayanan);
		$ret = $this->load->view("pelayanan/laboraturium/content" , $dt , true);
        return $ret;
	}

	private function updateStatusRegisterPoli($idKunjungan , $statusPelayanan = 2){

		$arrUpdate = array(
			"bitIsPoli" => $statusPelayanan,
			"dtLastUpdateKunjungan"=> date("Y-m-d H:i:s")
		);
		$resUpdate = $this->register_model->update($arrUpdate , $idKunjungan);
		return $resUpdate;
	}
	
	private function _build_detail_laboratorium($id_kunjungan){
		$detail_register = $this->register_model->getDetailRegister($id_kunjungan);
		if(count($detail_register) < 1){
            show_404();
        }
		$statusPelayananPoli = $detail_register['bitIsPoli'];
		if($statusPelayananPoli==1){
			/// Update To Sedang Di Layani
			$this->updateStatusRegisterPoli($id_kunjungan , 2);	
		}

		$idPasien = $detail_register['txtIdPasien'];
		$idPelayanan = $detail_register['intIdPelayanan'];
        $detail_pelayanan = $this->pelayanan_model->getDetail($idPelayanan);
		$detail_pasien = $this->pasien_model->getDetail($idPasien);
		
		$jamkes_list = $this->jaminan_kesehatan->getListDataJaminan();
		$jamkes_list["0"] = "Umum / Tidak Ada";
		$jamkes_list[""] = "Umum / Tidak Ada";
		$nama_poli = $detail_pelayanan['txtNama'];
		$rekam_medis = $this->rekam_medis->getDetailByIdPasien($idPasien);
		$idRekamMedis = "";
		$noRekamMedis = $detail_pasien['txtNoAnggota'];
		
		/// Build Header Form -> Hidden
		$dt['frm_id_pasien'] = $this->form_builder->inputHidden("txtIdPasien" , $detail_pasien['txtIdPasien']);
		$dt['frm_id_rekam_medis'] = $this->form_builder->inputHidden("txtIdRekamMedis" , $idRekamMedis);
		$dt['frm_id_pelayanan'] = $this->form_builder->inputHidden("txtIdPelayanan" , $detail_register['intIdPelayanan']);
		$dt['frm_id_kunjungan'] = $this->form_builder->inputHidden("txtIdKunjungan" , $detail_register['txtIdKunjungan']);
		$dt['frm_id_kunjungan_referral'] = $this->form_builder->inputHidden("txtIdKunjunganReff" , $detail_register['txtRefIdKunjungan']);
		$dt['frm_txt_kunjungan'] = $this->form_builder->inputTextArea("Keterangan Kunjungan" , "txtKetKunjungan", $detail_register['txtKeteranganKunjungan'], "col-sm-3", array("readonly"=>"readonly"));
		
		$dt['frm_tanggal'] = $this->form_builder->inputText("Tanggal Pemeriksaan" , "txtTanggalKunjungan", indonesian_date($detail_register['dtTanggalKunjungan']) , "col-sm-3" , array("readonly"=>"readonly"));
		$dt['frm_no_anggota_pasien'] = $this->form_builder->inputText("No Anggota" , "txtNoAnggotaPasien", $detail_pasien['txtNoAnggota'] , "col-sm-3" , array("readonly"=>"readonly"));
		$dt['frm_nama_pasien'] = $this->form_builder->inputText("Nama Pengguna Layanan" , "txtNamaPasien", $detail_pasien['txtNamaPasien'] , "col-sm-3" , array("readonly"=>"readonly"));
		$dt['frm_usia_pasien'] = $this->form_builder->inputText("Usia Pengguna Layanan" , "txtUsiaPasien", $detail_register['txtUsiaPasienKunjungan'] , "col-sm-3" , array("readonly"=>"readonly"));
		$dt['frm_jaminan_kesehatan'] = $this->form_builder->inputText("Jaminan Kesehatan" , "txtJamkes", $jamkes_list[$detail_register['intIdJaminanKesehatan']] , "col-sm-3" , array("readonly"=>"readonly"));
		$dt['frm_no_jaminan_kesehatan'] = $this->form_builder->inputText("No Jaminan Kesehatan" , "txtNoJamkes", $detail_register['txtNoJaminanKesehatan'] , "col-sm-3" , array("readonly"=>"readonly"));
		$dt['frm_no_rekam_medis'] = $this->form_builder->inputText("No Rekam Medis" , "txtNoRekamMedis", $noRekamMedis, "col-sm-3",array("readonly"=>"readonly"));
		$dt['frm_txt_ket_kunjungan'] = $this->form_builder->inputTextArea("Pemeriksaan" , "txtPeeriksaan", $detail_register['txtKeteranganKunjungan'], "col-sm-3",array("readonly"=>"readonly"));
		
		$breadcrumbs = array(
                        "Home" =>$this->base_url_site,
                        $nama_poli => $this->base_url,
                        "Detail ".$nama_poli => "#",
        );
		$dt['title'] = "Detail Laboratorium";
        $dt['breadcrumbs'] = $this->setBreadcrumbs($breadcrumbs);
		$jenis_item_periksa = $this->config->item('jenis_pemeriksaan_lab');
		$this->form_builder->form_type = "form_inline";
		$dt['frm_jenis_pemeriksaan'] = $this->form_builder->inputDropdown("Jenis Pemeriksaan","txtJenisPemeriksaan" , "" , $jenis_item_periksa);
		$dt['intIdLaboratorium'] = $this->form_builder->inputHidden("intIdLaboratorium" , "");
		$ret = $this->load->view("pelayanan/laboraturium/form" , $dt,true);
        return $ret;  
	}

	public function getDetailPemeriksaanByKunjungan(){

		if(!$this->input->is_ajax_request()){
			echo "Data Gagal";die;
		}

		$status = false;
		$html = "";
		$length =  $this->input->post("length");
		$start = $this->input->post("start");

		$txtIdKunjungan = $this->input->post("txtIdKunjungan");
		$mode = $this->input->post("mode");
		$offset = ($start - 0) * $this->limit;
		$fieldPemeriksaan = "txtIdKunjungan";
		if($mode=='poliklinik'){
			$fieldPemeriksaan = "txtIdReferralKunjungan";
		}

		$data_periksa = $this->laboratorium_model->getDataByIdKunjungan($txtIdKunjungan,$offset , $this->limit , $fieldPemeriksaan);
		$arrResult = array();
		///echopre($data_periksa);	
		foreach ($data_periksa as $key => $value) {
			# code...
			$resPeriksa = '	<dt>'.$value['txtLabelPemeriksaan'].'</dt>
							<dd>'.$value['txtValuePemeriksaan'].' '.$value['txtSatuanPemeriksaan'].' <br>
							'.$value['txtParameterPemeriksaan'].'
							</dd>';
			$btnPeriksa = '<button type="button" class="btn btn-default btn-flat btn-xs" onclick="getDetailPemeriksaan(\''.$value['intIdJenisPemeriksaan'].'\',\''.$value['intIdLaboratorium'].'\')"><i class="fa fa-edit"></i> Detail</button>';
			$btnPeriksa .= '<button type="button" class="btn btn-danger btn-flat btn-xs" onclick="hapusPemeriksaan(\''.$value['intIdLaboratorium'].'\')"><i class="fa fa-trash"></i> Hapus</button>';
			$arrResult[$value['intIdJenisPemeriksaan']]['tanggal_pemeriksaan'] = $value['dtCreatedDate'];
			$arrResult[$value['intIdJenisPemeriksaan']]['jenis_pemeriksaan'] = $this->jenis_pemeriksaan[$value['intIdJenisPemeriksaan']];
			$arrResult[$value['intIdJenisPemeriksaan']]['btn_aksi_pemeriksaan'] = $btnPeriksa;
			$arrResult[$value['intIdJenisPemeriksaan']]['hasil_periksa'][$value['txtFieldPemeriksaan']] = $resPeriksa;
		}

		$retVal['data'] = array();	
		foreach($arrResult as $rowPeriksa){
			$hasilPeriksa = $rowPeriksa['hasil_periksa'];
			$htmlResult = "<dl>";
			foreach ($hasilPeriksa as $rowDetailPeriksa) {
				# code...
				$htmlResult .= $rowDetailPeriksa;
			}
			$htmlResult .= "</dl>";
			///echo $htmlResult;
			if($mode=="poliklinik"){
				$arrData = array(
				$rowPeriksa['tanggal_pemeriksaan'],
				$rowPeriksa['jenis_pemeriksaan'],
				$htmlResult);
			}else{
				$arrData = array(
				$rowPeriksa['tanggal_pemeriksaan'],
				$rowPeriksa['jenis_pemeriksaan'],
				$htmlResult,
				$rowPeriksa['btn_aksi_pemeriksaan']);
			}
			$retVal['data'][] = $arrData;
			 
		}
		$this->setJsonOutput($retVal);
	}

	public function getDetailPemeriksaanByPasien(){
		
		if(!$this->input->is_ajax_request()){
			echo "Data Gagal";die;
		}

		$status = false;
		$html = "";
		$length =  $this->input->post("length");
		$start = $this->input->post("start");
		$txtIdPasien = $this->input->post("txtIdPasien");
		$fieldPemeriksaan = "txtIdPasien";
		$data_periksa = $this->laboratorium_model->getDataByIdKunjungan($txtIdPasien,$start , $length , $fieldPemeriksaan);
		$jumlah = $this->laboratorium_model->getCountDataByIdPasien($txtIdPasien);
		$arrResult = array();
		///echopre($data_periksa);	
		$retVal['data'] = array();
		if($jumlah==0) { 
			$retVal['draw'] = $this->input->post('draw');
			$retVal['recordsTotal'] = 0;
			$retVal['recordsFiltered'] = 0;
			$retVal['data'] = array();
		}else{
			$retVal['draw'] = $this->input->post('draw');
			$retVal['recordsTotal'] = $jumlah;
			$retVal['recordsFiltered'] = $jumlah;
			foreach ($data_periksa as $key => $value) {
			# code...
			$resPeriksa = '	<dt>'.$value['txtLabelPemeriksaan'].'</dt>
							<dd>'.$value['txtValuePemeriksaan'].' '.$value['txtSatuanPemeriksaan'].' <br>
							'.$value['txtParameterPemeriksaan'].'
							</dd>';
			$btnPeriksa = '<button type="button" class="btn btn-default btn-flat btn-xs" onclick="getDetailPemeriksaan(\''.$value['intIdJenisPemeriksaan'].'\',\''.$value['intIdLaboratorium'].'\')"><i class="fa fa-edit"></i> Detail</button>';
			$btnPeriksa .= '<button type="button" class="btn btn-danger btn-flat btn-xs" onclick="hapusPemeriksaan(\''.$value['intIdLaboratorium'].'\')"><i class="fa fa-trash"></i> Hapus</button>';
			$arrResult[$value['txtIdKunjungan']]['tanggal_pemeriksaan'] = indonesian_date($value['dtCreatedDate']);
			$arrResult[$value['txtIdKunjungan']]['jenis_pemeriksaan'] = $this->jenis_pemeriksaan[$value['intIdJenisPemeriksaan']];
			$arrResult[$value['txtIdKunjungan']]['hasil_periksa'][$value['txtFieldPemeriksaan']] = $resPeriksa;
			}
				
			foreach($arrResult as $rowPeriksa){
				$hasilPeriksa = $rowPeriksa['hasil_periksa'];
				$htmlResult = "<dl>";
				foreach ($hasilPeriksa as $rowDetailPeriksa) {
					# code...
					$htmlResult .= $rowDetailPeriksa;
				}
				$htmlResult .= "</dl>";
				///echo $htmlResult;
				$arrData = array(
					$rowPeriksa['tanggal_pemeriksaan'],
					$rowPeriksa['jenis_pemeriksaan'],
					$htmlResult
				);
				$retVal['data'][] = $arrData; 
			}
			
		}
		$this->setJsonOutput($retVal);
		
	}
    
	public function savePemeriksaan(){
		if(!$this->input->is_ajax_request()){
			echo "Ilegal Method!";die;
		}
		$retVal = array();
		/// Save In parent
		$jenisInput = $this->input->post("txtJenisPemeriksaan");
		$id = $this->input->post("intIdLaboratorium");
		$arrInsert = array(
			"txtIdKunjungan" => $this->input->post("txtIdKunjungan"),
			"txtIdReferralKunjungan" => $this->input->post("txtIdKunjunganReff"),
			"txtIdPasien" => $this->input->post("txtIdPasien"),
			"intIdJenisPemeriksaan" => $this->input->post("txtJenisPemeriksaan"),
			"intIdPegawai" => $this->session->userdata("sipt_user_id"),
			"bitStatusPemeriksaan" => 0, //// Data Belum Valid harus Selesai baru DiUpdate
		);

		if(empty($id)){
			$arrInsert['dtCreatedDate'] = date("Y-m-d H:i:s");
		}
		$arrInsert['dtLastUpdate'] = date("Y-m-d H:i:s");
		$retParent = $this->laboratorium_model->saveData($arrInsert , $id);
		$idLab = $retParent['id'];
		/// Check Input Value For Attributes
		$inputAttributes = $this->input->post("inputLab");
		$arrInputFormDetail = array();
		$numberAttributes = 0;
		$emptyAttributes = 0;
		
		foreach ($inputAttributes as $keyAttributes => $valueAttributes) {
			# code...
			$textSatuan = $valueAttributes['txtSatuan'];
			$textValue = $valueAttributes['txtValue'];
			$textParameter = $valueAttributes['txtParamater'];
			$textLabel = $valueAttributes['txtLabel'];	
			if(empty($textValue)){
				$emptyAttributes++;
			}
			
			$inputAttr = array(
							"intIdLaboratorium"=>$idLab,
							"txtFieldPemeriksaan"=>$keyAttributes,
							"txtValuePemeriksaan"=>$textValue,
							"txtLabelPemeriksaan"=>$textLabel,
							"txtParameterPemeriksaan"=>$textParameter,
							"txtSatuanPemeriksaan" => $textSatuan
						);
			$arrInputFormDetail[] = $inputAttr;
			$numberAttributes++;
		}

		if($numberAttributes==$emptyAttributes){
			$retVal['status'] = false;
			$retVal['message'] = "Nilai Pemeriksaan Belum Di Isi";
			$this->setJsonOutput($retVal);
			die;
		}
		
		$retChild = $this->laboratorium_model->insertBatchDetail($arrInputFormDetail , $idLab);
		$status = ($retChild > 0) ? true : false;
		$retVal['status'] = $status;
		$retVal['message'] = $retChild['message'];
		$this->setJsonOutput($retVal);
	}
	
	//// Dependencies Function Form Periksa

	public function deletePemeriksaan(){
		if(!$this->input->is_ajax_request()){
			echo "Ilegal Method!";die;
		}

		$status = false;
		$message = "Data Gagal Di Hapus";
		$idLaboratorium = $this->input->post('idLaboratorium');
		if(!empty($idLaboratorium)){
			$resDeleteDetail = $this->laboratorium_model->delete_detail($idLaboratorium);
			$resDelete = $this->laboratorium_model->delete($idLaboratorium);
		}

		$status = $resDelete['status'];
		$message = $resDelete['message'];
		$retVal = array();
		$retVal['status'] = $status;
		$retVal['message'] = $message;
		echo json_encode($retVal);
		///$this->setJsonOutput($retVal);
	}

	public function getFormPemeriksaan($mode,$id)
	{
		if(!$this->input->is_ajax_request()){
			echo "Ilegal Method!";die;
		}
		# code...
		$arrResultPeriksa = array();
		$idJenisPemeriksaan = "";
		if($mode=="insert"){
			$idJenisPemeriksaan = $id;
		}else{
			$detailPemeriksaan = $this->laboratorium_model->getDetailPeriksaByIdLab($id);
			
			foreach ($detailPemeriksaan as $key => $valuePeriksa) {
				# code...
				$idJenisPemeriksaan = $valuePeriksa['intIdJenisPemeriksaan'];
				$arrResultPeriksa[$valuePeriksa['txtFieldPemeriksaan']] = $valuePeriksa['txtValuePemeriksaan']; 
			}
			///echopre($arrResultPeriksa);
		}
		$dt = array();
		$status_form = true;
		///echo $idJenisPemeriksaan;
		switch ($idJenisPemeriksaan) {
			case '1':
				# code...
				$arrForm = $this->getArrFormHermatologi();
				break;
			case '2':
				# code...
				$arrForm = $this->getArrFormFaalHati();
				break;
			case '3':
				# code...
				$arrForm = $this->getArrFormWidal();
				break;
			case '4':
				# code...
				$arrForm = $this->getArrFormFaalGinjal();
				break;
			case '5':
				# code...
				$arrForm = $this->getArrFormLemak();
				break;
			case '6':
				# code...
				$arrForm = $this->getFormFaeces();
				break;
			case '7':
				# code...
				$arrForm = $this->getFormUrine();
				break;
			case '8':
				# code...
				$arrForm = $this->getFormKehamilan();
				break;
			case '9':
				# code...
				$arrForm = $this->getFormGulaDarah();
				break;
			case '10':
				# code...
				$arrForm = $this->getFormAsamUrat();
				break;
			case '11':
				# code...
				$arrForm = $this->getFormKolesterol();
				break;
			case '12':
				# code...
				$arrForm = $this->getFormHIV();
				break;
			case '13':
				# code...
				$arrForm = $this->getFormMalaria();
				break;
			case '14':
				# code...
				$arrForm = $this->getFormSputun();
				break;
			case '15':
				# code...
				$arrForm = $this->getFormGolonganDarah();
				break;
			case '16':
				# code...
				$arrForm = $this->getFormHepatitis();
				break;
			default:
				# code...
				$status_form = false;
				break;
		}
		
		$dt['arr_form'] = $arrForm;
		$dt['arr_result'] = $arrResultPeriksa;
		$html = $this->load->view("pelayanan/laboraturium/form_pemeriksaan" , $dt , true);
		$retVal = array();
		$retVal['status'] = $status_form;
		$retVal['form'] = $html;  
		echo $html;
	}
	
	private function getFormGulaDarah(){
		$arrInput = array(
			"puasa"=>array(
				"label"=>"Puasa",
				"type"=>"text",
				"list_value"=>"",
				"parameter"=>"(75-120 mg/dl)",
				"satuan"=>"mg/dl",
			),
			"2_jam_pp"=>array(
				"label"=>"2 Jam PP",
				"type"=>"text",
				"list_value"=>"",
				"parameter"=>"(< 140 mg/dl)",
				"satuan"=>"mg/dl",
			),
			"sesaat"=>array(
				"label"=>"Sesaat",
				"type"=>"text",
				"list_value"=>"",
				"parameter"=>"(< 200 mg/dl)",
				"satuan"=>"mg/dl",
			),
		);
		return $arrInput;
	}
	
	private function getFormUrine(){
		$arrInput = array(
			"protein"=>array(
				"label"=>"Protein",
				"type"=>"text",
				"list_value"=>"",
				"parameter"=>"",
				"satuan"=>"(-)",
			),
			"reduksi"=>array(
				"label"=>"Reduksi",
				"type"=>"text",
				"list_value"=>"",
				"parameter"=>"",
				"satuan"=>"(-)",
			),
			"bilirubin"=>array(
				"label"=>"Bilirubin",
				"type"=>"text",
				"list_value"=>"",
				"parameter"=>"",
				"satuan"=>"(-)",
			),
			"sediment"=>array(
				"label"=>"Sediment",
				"type"=>"dropdown",
				"list_value"=>array("lpb"=>"LPB","450"=>"450x"),
				"parameter"=>"",
				"satuan"=>"(-)",
			),
			"sel_epithel"=>array(
				"label"=>"Sel Epithel",
				"type"=>"text",
				"list_value"=>"",
				"parameter"=>"",
				"satuan"=>"(+)",
			),
			"lekosit"=>array(
				"label"=>"Lekosit",
				"type"=>"text",
				"list_value"=>"",
				"parameter"=>"(0-6)",
				"satuan"=>"",
			),
			"eritrosit"=>array(
				"label"=>"Eritrosit",
				"type"=>"text",
				"list_value"=>"",
				"parameter"=>"(0-1)",
				"satuan"=>"",
			),
			"kristal"=>array(
				"label"=>"Kristal",
				"type"=>"text",
				"list_value"=>"",
				"parameter"=>"",
				"satuan"=>"(-)",
			),
			"silinder"=>array(
				"label"=>"Silinder",
				"type"=>"text",
				"list_value"=>"",
				"parameter"=>"",
				"satuan"=>"(-)",
			),
			"bakteri"=>array(
				"label"=>"Bakteri",
				"type"=>"text",
				"list_value"=>"",
				"parameter"=>"",
				"satuan"=>"(-)",
			),
			"lain-lain"=>array(
				"label"=>"Lain - Lain",
				"type"=>"text",
				"list_value"=>"",
				"parameter"=>"",
				"satuan"=>"(-)",
			),
			
		);
		return $arrInput;
	}
	
	private function getFormKehamilan(){
		$arrInput = array(
			"telur_cacing"=>array(
				"label"=>"Plano Test",
				"type"=>"text",
				"list_value"=>"",
				"parameter"=>"",
				"satuan"=>"",
			),
		);
		return $arrInput;
	}
	
	private function getFormFaeces(){
		$arrInput = array(
			"telur_cacing"=>array(
				"label"=>"Telur Cacing",
				"type"=>"text",
				"list_value"=>"",
				"parameter"=>"",
				"satuan"=>"",
			),
		);
		return $arrInput;
	}
	
	private function getArrFormLemak(){
		$arrInput = array(
			"cholesterol"=>array(
				"label"=>"Cholesterol",
				"type"=>"text",
				"list_value"=>"",
				"parameter"=>"( < 200 mg/dl)",
				"satuan"=>"mg/dl",
			),
			"trigliserida"=>array(
				"label"=>"Trigliserida",
				"type"=>"text",
				"list_value"=>"",
				"parameter"=>"( < 150 mg/dl)",
				"satuan"=>"mg/dl",)
			);
			return $arrInput;
	}
	
	private function getArrFormFaalGinjal(){
		$arrInput = array(
			"ureum_darah"=>array(
				"label"=>"Ureum Darah",
				"type"=>"text",
				"list_value"=>"",
				"parameter"=>"(10-50)",
				"satuan"=>"mg/dl",
			),
			"createnin_darah"=>array(
				"label"=>"Ureum Darah",
				"type"=>"text",
				"list_value"=>"",
				"parameter"=>"(L : 0.7-1.1) (P : 0.6 - 1.0)",
				"satuan"=>"mg/dl",
			),
			"asam_urat"=>array(
				"label"=>"Asam Urat",
				"type"=>"text",
				"list_value"=>"",
				"parameter"=>"(L : 3-7) (P : 2.5 - 5)",
				"satuan"=>"mg/dl",
			),
			"albumin"=>array(
				"label"=>"Asam Urat",
				"type"=>"text",
				"list_value"=>"",
				"parameter"=>"(L : 3-7) (P : 2.5 - 5)",
				"satuan"=>"mg/dl",
			),
			
		);
		return $arrInput;
	}
	
	private function getArrFormWidal(){
		$arrInputWidal = array(
			"type_O"=>array(
				"label"=>"Type O",
				"type"=>"text",
				"list_value"=>"",
				"parameter"=>"",
				"satuan"=>"",
			),
			"type_H"=>array(
				"label"=>"Type H",
				"type"=>"text",
				"list_value"=>"",
				"parameter"=>"",
				"satuan"=>"",
			),
			"type_A"=>array(
				"label"=>"Type A",
				"type"=>"text",
				"list_value"=>"",
				"parameter"=>"",
				"satuan"=>"",
			),
			"type_B"=>array(
				"label"=>"Type B",
				"type"=>"text",
				"list_value"=>"",
				"parameter"=>"",
				"satuan"=>"",
			),
		);
		return $arrInputWidal;
	}
	
	private function getArrFormFaalHati(){
		$arrInputFaalHati = array(
			"sgot"=>array(
				"label"=>"SGOT",
				"type"=>"text",
				"list_value"=>"",
				"parameter"=>"(L:6-37),(P:5-31)",
				"satuan"=>"U/L",
			),
			"sgpt"=>array(
				"label"=>"SGPT",
				"type"=>"text",
				"list_value"=>"",
				"parameter"=>"(L:6-40),(P:6-31)",
				"satuan"=>"U/L",
			),
			"billirubin"=>array(
				"label"=>"Billirubin",
				"type"=>"text",
				"list_value"=>"",
				"parameter"=>"(0,1 - 1,2 mg/dl)",
				"satuan"=>"mg/dl",
			),
		);
		return $arrInputFaalHati;
	}
	
	private function getArrFormHermatologi(){
		$arrInputHermatologi = array(
			"Hb"=>array(
				"label"=>"Hb",
				"type"=>"text",
				"list_value"=>"",
				"parameter"=>"(L:14-18),(P:12-16)",
				"satuan"=>"gr%",
			),
			"lekosit"=>array(
				"label"=>"Lekosit",
				"type"=>"text",
				"list_value"=>"",
				"parameter"=>"(4000-10000),(Bayi:9000-12000)",
				"satuan"=>"g/h",
			),
			"led"=>array(
				"label"=>"LED",
				"type"=>"text",
				"list_value"=>"",
				"parameter"=>"(L : 0-10 mm/jam),(P : 0-12 mm/jam)",
				"satuan"=>"%",
			),
			"eos"=>array(
				"label"=>"EOS",
				"type"=>"text",
				"list_value"=>"",
				"parameter"=>"(1-3)",
				"satuan"=>"%",
			),
			"bas"=>array(
				"label"=>"BAS",
				"type"=>"text",
				"list_value"=>"",
				"parameter"=>"(0-1)",
				"satuan"=>"%",
			),
			"stab"=>array(
				"label"=>"STAB",
				"type"=>"text",
				"list_value"=>"",
				"parameter"=>"(2-6)",
				"satuan"=>"%",
			),
			"sag"=>array(
				"label"=>"SAG",
				"type"=>"text",
				"list_value"=>"",
				"parameter"=>"(50-70)",
				"satuan"=>"%",
			),
			"lym"=>array(
				"label"=>"Lym",
				"type"=>"text",
				"list_value"=>"",
				"parameter"=>"(20-40)",
				"satuan"=>"%",
			),
			"mon"=>array(
				"label"=>"Mon",
				"type"=>"text",
				"list_value"=>"",
				"parameter"=>"(2-6)",
				"satuan"=>"%",
			),
			"pcv"=>array(
				"label"=>"Hermatokrit(PCV)",
				"type"=>"text",
				"list_value"=>"",
				"parameter"=>"L(:40-54),P(:38-47)",
				"satuan"=>"%",
			),
			"trombosit"=>array(
				"label"=>"Trombosit",
				"type"=>"text",
				"list_value"=>"",
				"parameter"=>"150-500rb",
				"satuan"=>"/mm",
			),
		);
		
		return $arrInputHermatologi;
	}
	// Ini Nomor 10
	private function getFormAsamUrat(){
		$arrData = array(
			"asam_urat"=>array(
				"label"=>"Kadar Asam Urat",
				"type"=>"text",
				"list_value"=>"",
				"parameter"=>"",
				"satuan"=>"mg/dl",
			),
		);
		return $arrData;
	}

	private function getFormKolesterol(){
		$arrData = array(
			"asam_urat"=>array(
				"label"=>"Kadar Cholersterol",
				"type"=>"text",
				"list_value"=>"",
				"parameter"=>"",
				"satuan"=>"mg/dl",
			),
		);
		return $arrData;
	}

	private function getFormHIV(){
		$arrData = array(
			"asam_urat"=>array(
				"label"=>"HIV",
				"type"=>"text",
				"list_value"=>"",
				"parameter"=>"",
				"satuan"=>"(+) / (-)",
			),
		);
		return $arrData;
	}

	private function getFormMalaria(){
		$arrData = array(
			"asam_urat"=>array(
				"label"=>"Malaria",
				"type"=>"text",
				"list_value"=>"",
				"parameter"=>"",
				"satuan"=>"(+) / (-)",
			),
		);
		return $arrData;
	}

	private function getFormSputun(){
		$arrData = array(
			"bta_p"=>array(
				"label"=>"BTA (+)",
				"type"=>"dropdown",
				"list_value"=>array("0","1" , "2" , "3" , "4"),
				"parameter"=>"",
				"satuan"=>"",
			),
			"bta_n"=>array(
				"label"=>"BTA (-)",
				"type"=>"text",
				"list_value"=>"",
				"parameter"=>"",
				"satuan"=>"",
			),
		);
		return $arrData;
	}

	private function getFormGolonganDarah(){
		$arrData = array(
			"gol_darah"=>array(
				"label"=>"Golongan Darah",
				"type"=>"dropdown",
				"list_value"=>array(
					"A" => "A",
					"B" => "B" , 
					"AB" => "AB" , 
					"O" => "O"),
				"parameter"=>"",
				"satuan"=>"",
			),
		);
		return $arrData;
	}

	private function getFormHepatitis(){
		$arrData = array(
			"asam_urat"=>array(
				"label"=>"Hepatitis",
				"type"=>"text",
				"list_value"=>"",
				"parameter"=>"",
				"satuan"=>"(+) / (-)",
			),
		);
		return $arrData;
	}
	
	
}