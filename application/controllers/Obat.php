<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Obat extends MY_Controller {

    var $meta_title = "Data Obat";
    var $meta_desc = "Data Obat";
    var $main_title = "Data Obat";
    var $dtBreadcrumbs = array();  
    var $menu_key = "master_obat";
    var $limit = "20";

    public function __construct() {
        parent::__construct();
        $this->dtBreadcrumbs = array(
            "Home" =>base_url(),             
            "Data Obat" => base_url('data-obat'),
        );
        $this->load->model('obat_m');
    }
    public function index() {        
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "container" => $this->_index(),
            "menu_key" => $this->menu_key,
            "akses_key" => "is_view",	
            "custom_js" => array(
                ASSETS_JS_URL."gudang_apotik/obat/index.js"
            ),
            "custom_css" => array(),
        );  
        $this->_render("default",$dt);             
    }

    private function _index() {        
        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['title'] = $this->meta_title;
        return $this->load->view('obat/index', $dt, true);
    }

    public function get($id = null) {
        $this->load->library('datatables');
        $this->db->like('nama_obat', $this->input->get('nama'));
        if ($this->input->get('jenis_obat')) {
            $this->db->where('id_jenis_obat', $this->input->get('jenis_obat'));
        }
        $query = $this->db->select('obat.*, kemasan.kemasan, jenis_obat.jenis_obat')
        ->join('kemasan', 'kemasan.id = obat.id_kemasan')
        ->join('jenis_obat', 'jenis_obat.id = obat.id_jenis_obat', 'left')
        ->get_compiled_select('obat');
        $response = $this->datatables->collection($query)
        ->orderableColumns('nama_obat, kode_obat, kemasan, jenis_obat, stok, stok_optimal')
        ->searchableColumns('nama_obat, kode_obat, kemasan, jenis_obat, stok, stok_optimal')
        ->addColumn('action', function($row) {
                return '<a href="'.base_url('data-obat/edit/' . $row->id).'" class="btn btn-warning btn-sm"><i class="fa fa-pencil"></i> Ubah</a> '.
                           '<a onclick="return confirm(\'Apakah anda yakin akan menghapus data ini?\')" href="'.base_url('data-obat/delete/' . $row->id).'" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> Hapus</a>';
        })
        ->render();
        $this->output->set_content_type('application/json')->set_output(json_encode($response));
    }

    public function create() {
        if ($post = $this->input->post()) {
            $rules = array(
                array(
                    'field' => 'stok',
                    'label' => 'Stok Awal',
                    'rules' => 'required|numeric'
                )
            );
            $this->validation($rules);
            $this->obat_m->createObat($post);
            $this->redirect->with('successMessage', 'Data kemasan berhasil ditambahkan.')->to('data-obat');
        }
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "container" => $this->_create(),
            "menu_key" => $this->menu_key,
            "akses_key" => "is_insert",	
            "custom_js" => array(
                ASSETS_URL."plugins/select2/select2.js",
                ASSETS_JS_URL."gudang_apotik/obat/form.js"
            ),
            "custom_css" => array(
                ASSETS_URL."plugins/select2/select2.css",
            ),
        );  
        $this->_render("default",$dt);            
    }

    public function edit($id) {
        $result = $this->find($id);
        if ($post = $this->input->post()) {            
            $this->obat_m->update($id, $post);
            $this->redirect->with('successMessage', 'Data obat berhasil diperbarui.')->to('data-obat');
        }
        $this->form->setData($result);
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "container" => $this->_edit(),
            "menu_key" => $this->menu_key,
            "akses_key" => "is_update",	
            "custom_js" => array(
                ASSETS_URL."plugins/select2/select2.js",
                ASSETS_JS_URL."gudang_apotik/obat/form.js"
            ),
            "custom_css" => array(
                ASSETS_URL."plugins/select2/select2.css",
            ),
        );  
        $this->_render("default",$dt);      
    }

    private function _create() {
        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['title'] = $this->meta_title;
        return $this->load->view('obat/create', $dt, true);
    }

    private function _edit() {
        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['title'] = $this->meta_title;
        return $this->load->view('obat/edit', $dt, true);
    }

    public function delete($id) {
        $result = $this->find($id);
        $this->obat_m->delete($id);
        $this->redirect->with('successMessage', 'Data obat berhasil dihapus.')->to('data-obat');
    }

    /*public function add_stok($id) {
        $result = $this->find($id);
        if ($post = $this->input->post()) {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('tgl', 'Tanggal', 'required');
            $this->form_validation->set_rules('jml', 'Jumlah Penambahan', 'required');
            if (!$this->form_validation->run()) {
                $this->redirect->withInput()->withValidation()->back();
            } else {
                $this->obat_m->add_stok($id, $post);
                $this->redirect->with('successMessage', 'Stok obat berhasil ditambahkan')->to('gudang_apotik/obat');
            }
        }
        $this->form->setData($result);        
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "menu_key" => $this->menu_key,
            "akses_key" => "is_insert",	
            "container" => $this->_add_stok(),
            "custom_js" => array(
                ASSETS_JS_URL."apotik_gudang/obat/add_stok.js"
            ),
            "custom_css" => array(),
        );  
        $this->_render("default",$dt);   
    }

    private function _add_stok() {
        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['title'] = $this->meta_title;
        return $this->load->view('obat/add_stok', $dt, true);
    }*/

    private function find($id) {
        $result = $this->obat_m->find($id);
        if (!$result) {
            $this->redirect->with('errorMessage', 'Data obat tidak ditemukan.')->back();
        } else {
            return $result;
        }
    }

    private function validation($rules = null) {
        $this->load->library('form_validation');        
        $this->form_validation->set_rules('nama_obat', 'Nama Obat', 'required');                    
        if ($rules) {
            $this->form_validation->set_rules($rules);
        }
        if (!$this->form_validation->run()) {
            $this->redirect->withInput()->withValidation()->back();
        }        
    }

    public function getListDataObat(){
        
        if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";
        }
        $limit = 20;
        $where = $this->input->get("q");
        $page = !empty($_GET['page']) ? $this->input->get("page") : 1;
        $start = ($page - 1) * $limit;
        $data_obat = $this->obat_m->getListObat($where , $start , $limit);
        
        $jumlah = $this->obat_m->getCountListObat($where);
        $retVal = array();
		
        $retVal['total_count'] = $jumlah;
        $retVal['incomplete_results'] = false;
        $retVal['items'] = $data_obat;
        
        echo json_encode($retVal);
    }
        public function getObatData(){
        
        if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";
        }
        $where = $this->input->get("q");
        $page = !empty($_GET['page']) ? $this->input->get("page") : 1;
	    $start = ($page - 1) * $this->limit;
        $data_penyakit = $this->obat_m->getListDataObat($where, $start , $this->limit);
        $jumlah = $this->obat_m->getCountListDataObat($where);
        $retVal = array();
		
        $retVal['total_count'] = $jumlah;
        $retVal['incomplete_results'] = false;
        $retVal['items'] = $data_penyakit;
        echo json_encode($retVal);
    }
}