<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pemeriksaan extends MY_Controller {
    var $meta_title = "Data Pemeriksaan";
    var $meta_desc = "Pasien";
	var $main_title = "Pasien";
    var $base_url = "";
	var $limit = "20";
	var $arr_level_user = "";
    var $menu = "K01";
    var $mode = "";
    var $main_model = "";
    var $js_folder = "";
    var $jenis_pelayanan = "";
	
	function __construct(){
        parent::__construct();
		$this->load->model("tindakan_model" );
        $this->base_url = $this->base_url_site."data-pemeriksaan/";
        $this->main_model = $this->tindakan_model;
        $this->js_folder = ASSETS_JS_URL."master_data/tindakan/";
        $this->jenis_pelayanan = $this->config->item("jenis_pelayanan");
    }
  
    /// Data Tindakan
	public function index()
	{
        $menu = "K01";
		$this->mode = "view";
		$dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
			"container" => $this->_content(),
			"custom_js" => array(
				$this->js_folder."content.js",
			),
            "custom_css" => array(
				
			),
		);	
		$this->_render("default",$dt);	
	}
    
    public function form($id=""){
		
		$menu = "K02";
		$this->mode = "insert";
		$dt = array(
            "title" => $this->meta_title,
			"menu" => "M01.1",
			"mode_menu" => "insert",
            "description" => $this->meta_desc,
			"container" => $this->_form($id),
			"custom_js" => array(
				ASSETS_URL."plugins/select2/select2.js",
				ASSETS_URL."plugins/validate/jquery.validate_1.11.1.min.js",
				$this->js_folder."form.js",
			),
            "custom_css" => array(
				ASSETS_URL."plugins/select2/select2.css",
			),
		);	
		$this->_render("default",$dt);
	}
	
	private function _content(){
		$dt = array();
		$breadcrumbs = array(
				"Home" =>$this->base_url_site,
				"Master Data" => $this->base_url,
				"Data Pemeriksaan" => "#",
        );
		
		$dt['breadcrumbs'] = $this->setBreadcrumbs($breadcrumbs);
		$dt['title'] = $this->meta_title;
		$dt['frm_search'] = $this->form_builder->inputText("Nama Tindakan / Kode Tindakan" , "searchText" , "" , "col-sm-3" , array("class"=> "form-control"));
        $dt['frm_pelayanan'] = $this->form_builder->inputDropdown("Jenis Pelayanan" , "selectJenisPelayanan" , "" , $this->config->item("jenis_pelayanan"));
		$dt['link_add'] = $this->base_url."tambah-data/";
		$ret = $this->load->view("master/tindakan/content" , $dt, true);
		return $ret;
	}
	
	private function _form($id){
		$dt = array();
		$mode = "insert";
		$dataDetail = array();
		if(!empty($id)){
			$mode = "edit";
			$dataDetail = $this->main_model->getDetail($id);
		}
		
		$arrForm = array("intIdPemeriksaan" ,
                         "txtKodePemeriksaan" ,
                         "txtPemeriksaan",
                         "intIdJenisPelayanan",
                         "txtDeskripsiPemeriksaan",
                         "bitStatusPemeriksaan");
                         
		foreach($arrForm as $frmData){
			$$frmData = !empty($dataDetail[$frmData]) ? $dataDetail[$frmData] : "";
		}
        
		$dt['frmIdPemeriksaan'] = $this->form_builder->inputHidden("intIdTindakan" , $intIdTindakan);
		$dt['frmtxtKodeTindakan'] = $this->form_builder->inputText("Kode Tindakan" , "txtKodeTindakan" , $txtKodeTindakan , "col-sm-3");
        $dt['frmJenisPelayanan'] = $this->form_builder->inputDropdown("Jenis Pelayanan" , "intIdJenisPelayanan" ,$intIdJenisPelayanan, $this->jenis_pelayanan);
		$dt['frmtxtTindakan'] = $this->form_builder->inputText("Tindakan" , "txtTindakan" , $txtTindakan , "col-sm-3");
		$dt['frmtxtDeskripsi'] = $this->form_builder->inputTextArea("Deskripsi Tindakan" , "txtDeskripsi" , $txtDeskripsi , "col-sm-3");
		$dt['frmStatusTindakan'] = $this->form_builder->inputDropdown("Status Pasien" , "bitStatusTindakan" ,$bitStatusTindakan, $this->config->item("status_list"));
		
		$breadcrumbs = array(
				"Home" =>$this->base_url_site,
				"Master Data" => $this->base_url,
				"Data Pemeriksaan" => $this->base_url,
                "Form Input Pemeriksaan" => $this->base_url,
        );
		
		$dt['breadcrumbs'] = $this->setBreadcrumbs($breadcrumbs);
		$dt['title'] = $this->meta_title;
		$dt['link_index'] = $this->base_url;
		$ret = $this->load->view("master/tindakan/form" , $dt, true);
		return $ret;
	}
	
	/// Dependency Public Function
	public function hapusData(){
		if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";
        }
		$status = false;
		$html = "";
		$id = $this->input->post("id");
		if(!empty($id)){
			$resVal = $this->main_model->delete($id);
		}
		$retVal['status'] = $resVal['status'];
		$retVal['message'] = $resVal['error_stat'];
		$this->session->set_flashdata("alert_message",$retVal);
		$this->setJsonOutput($retVal);
	}
	
	public function simpanData(){
		if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";
        }
		
		$status = false;
		$html = "";
		$id = $this->input->post("intIdTindakan");
		$arrInsert = array(
						 "txtKodeTindakan" =>$this->input->post("txtKodeTindakan"),
						 "intIdJenisPelayanan"=>$this->input->post("intIdJenisPelayanan"),
						 "txtTindakan"=>$this->input->post("txtTindakan"),
						 "txtDeskripsi"=>$this->input->post("txtDeskripsi"),
                         "bitStatusTindakan"=>$this->input->post("bitStatusTindakan")
						 );
		
		if(!empty($id)){
			$resVal = $this->main_model->update($arrInsert , $id);
		}else{
			$resVal = $this->main_model->saveData($arrInsert);
		}
		
		$retVal['status'] = $resVal['status'];
		$retVal['message'] = $resVal['error_stat'];
		$this->session->set_flashdata("alert_message",$retVal);
		$this->setJsonOutput($retVal);
	}
	
	public function getData($search="" , $level=""){
		if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";
        }
		
		$status = false;
		$html = "";
		$post = $_POST;
		$length =  $this->input->post("length");
		$start = $this->input->post("start");
		
		$offset = ($start - 0) * $this->limit;
		$data_results = $this->main_model->getData($start , $this->limit , $search);
		$count_data = $this->main_model->getCountData($search);
		
        $jenis_pelayanan = $this->jenis_pelayanan;
		if($count_data > 0) {
			$retVal['draw'] = $this->input->post('draw');
			$retVal['recordsTotal'] = $count_data;
			$retVal['recordsFiltered'] = $count_data;
			$retVal['data'] = array();
			foreach($data_results as $row){
				$btnEdit = "<a href='".$this->base_url."edit/".$row['intIdTindakan']."' class='btn btn-info btn-flat'><i class=\"fa fa-pencil\"></i> Edit</a>";
				$btnDelete = "<button onclick='hapusData(".$row['intIdTindakan'].")' type='button' class='btn btn-danger btn-flat'><i class=\"fa fa-trash\"></i> Hapus</button>";
				$btnAksi = $btnEdit.$btnDelete;
				$retVal['data'][] = array($row['txtKodeTindakan'],
										  $row['txtTindakan'] ,
										  $jenis_pelayanan[$row['intIdJenisPelayanan']] ,
										  $row['txtDeskripsi'],
										  $btnAksi
									);
			}
		}else{
			$retVal['draw'] = $this->input->post('draw');
			$retVal['recordsTotal'] = 0;
			$retVal['recordsFiltered'] = 0;
			$retVal['data'] = array();
		}
		$this->setJsonOutput($retVal);
	}
	
	public function getListData(){
        
        if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";
        }
        $where = $this->input->get("q");
        $page = !empty($_GET['page']) ? $this->input->get("page") : 1;
        $start = ($page - 1) * $this->limit;
        $data_penyakit = $this->main_model->getListData($where , $start , $this->limit);
        $jumlah = $this->main_model->getCountList($where);
        $retVal = array();
		
        $retVal['total_count'] = $jumlah;
        $retVal['incomplete_results'] = false;
        $retVal['items'] = $data_penyakit;
        echo json_encode($retVal);
    }
    
 }
?>