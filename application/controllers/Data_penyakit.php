<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data_penyakit extends MY_Controller {
    
    var $meta_title = "Data Penyakit";
    var $meta_desc = "Pengguna Layanan";
	var $main_title = "Pengguna Layanan";
    var $base_url = "";
	var $upload_dir = "";
	var $upload_url = "";
	var $limit = "20";
	var $arr_level_user = "";
    var $menu = "K01";
    var $mode = "";
	function __construct(){
        parent::__construct();
		
		$this->load->model("data_penyakit_model" , "data_penyakit");
        $this->base_url = $this->base_url_site."data-penyakit/";
    }
    
  
    /// Data ICD 10
	public function index()
	{
        $menu = "K01";
		$this->mode = "view";
		$dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
			"menu_key" => "master_penyakit",
			"akses_key" => "is_view",
			"container" => $this->_content(),
			"custom_js" => array(
				ASSETS_JS_URL."master_data/penyakit/content.js",
				
			),
            "custom_css" => array(
				
			),
		);	
		$this->_render("default",$dt);	
	}
    
    public function form($id=""){
		
		$menu = "K02";
		$this->mode = "insert";
		$mode_akses = !empty($id) ? "is_update" : "is_insert";
		$dt = array(
            "title" => $this->meta_title,
			"menu" => "M01.1",
			"mode_menu" => "insert",
			"menu_key" => "master_penyakit",
			"akses_key" => $mode_akses,
            "description" => $this->meta_desc,
			"container" => $this->_form($id),
			"custom_js" => array(
				ASSETS_URL."plugins/select2/select2.js",
				ASSETS_URL."plugins/validate/jquery.validate_1.11.1.min.js",
				ASSETS_JS_URL."master_data/penyakit/form.js",
			),
            "custom_css" => array(
				ASSETS_URL."plugins/select2/select2.css",
			),
		);	
		$this->_render("default",$dt);
	}
	
	private function _content(){
		$dt = array();
		$breadcrumbs = array(
				"Home" =>$this->base_url_site,
				"Master Data" => $this->base_url,
				"Data Penyakit" => "#",
        );
		
		$dt['breadcrumbs'] = $this->setBreadcrumbs($breadcrumbs);
		$dt['title'] = $this->meta_title;
		$dt['frm_search'] = $this->form_builder->inputText("Nama Penyakit / Kode Penyakit" , "searchText" , "" , "col-sm-3" , array("class"=> "form-control"));
		$dt['link_add'] = $this->base_url."tambah-data/";
		
		$ret = $this->load->view("master/penyakit/content" , $dt, true);
		return $ret;
	}
	
	private function _form($id){
		$dt = array();
		$mode = "insert";
		$dataPenyakit = array();
		if(!empty($id)){
			$mode = "edit";
			$dataPenyakit = $this->data_penyakit->getDetail($id);
		}
		
		$arrForm = array("intIdPenyakit" , "txtCategory" , "txtSubCategory", "txtEnglishName","txtIndonesianName" , "bitStatusPenyakit");
		foreach($arrForm as $frmData){
			$$frmData = !empty($dataPenyakit[$frmData]) ? $dataPenyakit[$frmData] : "";
		}
		
		$dt['frmIdPenyakit'] = $this->form_builder->inputHidden("intIdPenyakit" , $intIdPenyakit);
		$dt['frmInputCategory'] = $this->form_builder->inputText("Kode ICD-X" , "txtCategory" , $txtCategory , "col-sm-3");
		$dt['frmInputTextIndonesia'] = $this->form_builder->inputTextArea("Nama Penyakit (Indonesia)" , "txtIndonesianName" , $txtIndonesianName , "col-sm-3");
		$dt['frmInputTextEnglish'] = $this->form_builder->inputTextArea("Nama Penyakit (Inggris)" , "txtEnglishName" , $txtEnglishName , "col-sm-3");
		$dt['frmStatusPenyakit'] = $this->form_builder->inputDropdown("Status Pengguna Layanan" , "cmbStatus" ,$bitStatusPenyakit, $this->config->item("status_list"));
		
		$breadcrumbs = array(
				"Home" =>$this->base_url_site,
				"Master Data" => $this->base_url,
				"Data Penyakit" => "#",
        );
		
		$dt['breadcrumbs'] = $this->setBreadcrumbs($breadcrumbs);
		$dt['title'] = $this->meta_title;
		$dt['link_index'] = $this->base_url;
		$ret = $this->load->view("master/penyakit/form" , $dt, true);
		return $ret;
	}
	
	/// Dependency Public Function
	public function hapusDataPenyakit(){
		if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";
        }
		$status = false;
		$html = "";
		$id = $this->input->post("id");
		if(!empty($id)){
			$resVal = $this->data_penyakit->delete($id);
		}
		$retVal['status'] = $resVal['status'];
		$retVal['message'] = $resVal['error_stat'];
		$this->session->set_flashdata("alert_message",$retVal);
		$this->setJsonOutput($retVal);
	}
	
	public function simpanDataPenyakit(){
		if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";
        }
		
		$status = false;
		$html = "";
		$idPenyakit = $this->input->post("intIdPenyakit");
		$arrInsert = array(
						 "txtCategory" =>$this->input->post("txtCategory"),
						 "txtIndonesianName"=>$this->input->post("txtIndonesianName"),
						 "txtEnglishName"=>$this->input->post("txtEnglishName"),
						 "bitStatusPenyakit"=>$this->input->post("cmbStatus")
						 );
		
		if(!empty($idPenyakit)){
			$arrInsert['intIdPenyakit'] = $this->input->post('intIdPenyakit');
			$resVal = $this->data_penyakit->update($arrInsert , $idPenyakit);
		}else{
			$resVal = $this->data_penyakit->saveData($arrInsert);
		}
		$retVal['status'] = $resVal['status'];
		$retVal['message'] = $resVal['error_stat'];
		$this->session->set_flashdata("alert_message",$retVal);
		$this->setJsonOutput($retVal);
	}
	
	public function getData($search="" , $level=""){
		if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";
        }
		
		$status = false;
		$html = "";
		$post = $_POST;
		$length =  $this->input->post("length");
		$start = $this->input->post("start");
		
		$offset = ($start - 0) * $this->limit;
		$data_results = $this->data_penyakit->getData($start , $this->limit , $search);
		$count_data = $this->data_penyakit->getCountData($search);

		if($count_data > 0) {
			$retVal['draw'] = $this->input->post('draw');
			$retVal['recordsTotal'] = $count_data;
			$retVal['recordsFiltered'] = $count_data;
			$retVal['data'] = array();
			foreach($data_results as $row){
				$btnEdit = "<a href='".$this->base_url."edit/".$row['intIdPenyakit']."' class='btn btn-info btn-flat'><i class=\"fa fa-pencil\"></i> Edit</a>";
				$btnDelete = "<button onclick='hapusData(".$row['intIdPenyakit'].")' type='button' class='btn btn-danger btn-flat'><i class=\"fa fa-trash\"></i> Hapus</button>";
				$btnAksi = $btnEdit.$btnDelete;
				$retVal['data'][] = array($row['txtCategory'],
										  $row['txtIndonesianName'],
										  $row['txtEnglishName'],
										  $btnAksi
									);
			}
		}else{
			$retVal['draw'] = $this->input->post('draw');
			$retVal['recordsTotal'] = 0;
			$retVal['recordsFiltered'] = 0;
			$retVal['data'] = array();
		}
		$this->setJsonOutput($retVal);
	}
	
	public function getListDataPenyakit(){
        
        if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";
        }
        $where = $this->input->get("q");
        $page = !empty($_GET['page']) ? $this->input->get("page") : 1;
        $start = ($page - 1) * $this->limit;
        $data_penyakit = $this->data_penyakit->getListDataPenyakit($where , $start , $this->limit);
        $jumlah = $this->data_penyakit->getCountListDataPenyakit($where);
        $retVal = array();
		
        $retVal['total_count'] = $jumlah;
        $retVal['incomplete_results'] = false;
        $retVal['items'] = $data_penyakit;
        echo json_encode($retVal);
    }

	
}
