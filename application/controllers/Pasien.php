<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Pasien extends MY_Controller {
    var $meta_title = "Data Pengguna Layanan";
    var $meta_desc = "Pengguna Layanan";
	var $main_title = "Pengguna Layanan";
    var $base_url = "";
	var $upload_dir = "";
	var $upload_url = "";
	var $limit = "20";
	var $arr_level_user = "";
    var $menu_key = "pasien_data";

	function __construct(){
        parent::__construct();
		$this->load->model("pasien_model");
		$this->load->model("pekerjaan_model");
		$this->load->model("pelayanan_model");
		$this->load->model("rekam_medis_model");
		$this->load->model("register_model");
		$this->load->model("kelurahan_model");
		$this->load->model("jaminan_kesehatan_model" , "jaminan_kesehatan");
        $this->base_url = $this->base_url_site."pasien/";
        $this->base_url_loket = $this->base_url_site."loket/";
		$this->base_url_rekmed = $this->base_url_site."rekam-medis/";
    }
    
	public function index()
	{
        
		$dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
			"container" => $this->_data_pasien(),
			"menu_key" => $this->menu_key,
			"akses_key" => "is_view",	
			"custom_js" => array(
				ASSETS_JS_URL."pasien/pasien.js",
			),
            "custom_css" => array(
				
			),
		);	
		$this->_render("default",$dt);	
	}
    
	/// Form Input For Register
    public function addPasien($mode,$id="" , $id_loket = ""){
		$menu = "P02";
		$arrJs = "";
		if($mode=="loket"){
			$arrJs = ASSETS_JS_URL."pasien/form.js";
			
		}else if($mode=="insert" || $mode=="detail"){
			$arrJs = ASSETS_JS_URL."pasien/form_insert.js";
		}else if($mode=="ugd"){
			$arrJs = ASSETS_JS_URL."pasien/form_ugd.js";	
		}
		
		$akses_key = !empty($id) ? "is_insert" : "is_update";
		
		$dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
			"container" => $this->getFormPasien($mode,$id , $id_loket),
			"menu_key" => $this->menu_key,
			"akses_key" => $akses_key,	
			"custom_js" => array(
				ASSETS_URL."plugins/select2/select2.js",
				ASSETS_URL."plugins/validate/jquery.validate_1.11.1.min.js",
				ASSETS_URL."plugins/moment/moment.min.js",
				ASSETS_URL."plugins/flexcode/js/ajaxmask.js",
				ASSETS_URL."plugins/flexcode/js/jquery.timer.js",
				ASSETS_JS_URL."bpjs/checkNoAnggotaBPJS.js",
				ASSETS_JS_URL."pasien/finger.js",
				ASSETS_JS_URL."pasien/handsign.js",
				ASSETS_PLUGIN_URL . "jSignature/libs/jSignature.min.js",
				ASSETS_PLUGIN_URL . "jSignature/plugins/jSignature.CompressorBase30.js",
				ASSETS_PLUGIN_URL . "jSignature/plugins/jSignature.CompressorSVG.js",
				$arrJs
			),
			
            "custom_css" => array(
				ASSETS_URL."plugins/select2/select2.min.css",
				ASSETS_URL."plugins/flexcode/css/ajaxmask.css",
			),
		);	
		$this->_render("default",$dt);
	}
	
	private function _data_pasien(){
		$dt = array();
		$breadcrumbs = array(
				"Home" =>$this->base_url_site,
				"Pengguna Layanan" => $this->base_url,
				"Data Pengguna Layanan" => "#",
        );
		
		$dt['breadcrumbs'] = $this->setBreadcrumbs($breadcrumbs);
		$dt['title'] = "Data Pengguna Layanan";
		$dt['frm_tanggal'] = $this->form_builder->inputText("Nama Pengguna Layanan / No. Identitas / No. KTP / No Jaminan Kesehatan" , "id_pasien" , "" , "col-sm-3" , array("class"=> "form-control"));
		$dt['link_tambah_pasien'] = $this->base_url."pendaftaran/insert/";
		$retVal = $this->load->view("pasien/content" , $dt , true);
        return $retVal;
	}
	
	public function getDataPasien($search = ""){
		
		if(!$this->input->is_ajax_request()){
			echo "Ilegal";die;
		}
		
		$status = false;
		$html = "";
		$post = $_POST;
		$length =  $this->input->post("length");
		$start = $this->input->post("start");
		
		$offset = $start;
		$data_pasien = $this->pasien_model->getDataPasien($offset , $this->limit , $search);
		$count_data = $this->pasien_model->countGetDataPasien($search);
		
		if($count_data > 0) {
			$retVal['draw'] = $this->input->post('draw');
			$retVal['recordsTotal'] = $count_data;
			$retVal['recordsFiltered'] = $count_data;
			$retVal['data'] = array();
			foreach($data_pasien as $pasien){
				$jenisKelamin = $pasien['charJkPasien']=="L" ? "Laki-Laki" : "Perempuan";
				$btnEdit = "<a href='".$this->base_url."pendaftaran/detail/".$pasien['txtIdPasien']."' class='btn btn-info btn-flat'><i class=\"fa fa-pencil\"></i> Detail</a>";
				$btnDelete = "<button onclick='hapusPasien(\'".$pasien['txtIdPasien']."\')' class='btn btn-warning btn-flat'><i class=\"fa fa-trash\"></i> Hapus</button>";
				$btnAksi = $btnEdit;
				$retVal['data'][] = array($pasien['txtNoAnggota'],$pasien['txtNamaPasien'] , $pasien['dtTanggalLahir'] , $jenisKelamin , $btnAksi);
			}
		}else{
			$retVal['draw'] = $this->input->post('draw');
			$retVal['recordsTotal'] = 0;
			$retVal['recordsFiltered'] = 0;
			$retVal['data'] = array();
		}
		$this->setJsonOutput($retVal);
	}
	
	public function deletePasien(){
		if(!$this->input->is_ajax_request()){
			echo "Ilegal";die;
		}
		$status = false;
		$message = "Data Gagal Di hapus";
		$id = $this->input->post("id");
		$resDetail = $this->pasien_model->delete($id);
		if($resDetail['status']){
			$status = true;
			$message = "Data Berhasil Di Hapus";	
		}
		$retVal['status'] = $status;
		$retVal['message'] = $message;
		$this->setJsonOutput($retVal);
	}
	
	private function getFormPasien($mode,$id , $id_kunjungan_loket){
		$this->load->model("finger_model");
		$dt = array();
		$breadcrumbs = array();
		$titleForm = "";
		$arrDetaiPasien = array(
							"txtIdPasien",
							"txtNoAnggota",
							"intNoAnggota",
							"txtNoKtp",
							"txtNoKk",
							"txtNamaKepKk",
							"txtNamaPasien",
							"txtTempatLahir",
							"dtTanggalLahir",
							"txtAlamatPasien",
							"intIdKecamatan",
							"intIdKelurahan",
							"txtAgama",
							"charJkPasien",
							//"txtPekerjaan",
							"intIdJaminanKesehatan",
							"txtNoIdJaminanKesehatan",
							"txtNoHandphone",
							"txtUsiaTemp",
							"txtWilayahPasien",
							"txtStatusKK",
							"txtTtdPasien",
							"bitStatusPasien",
							"bitSementara",
							"intIdPekerjaan",
							
						);
			
		$detailPasien = array();
		$detailPasien['txtIdPasien'] = $this->generateIdPasien();
		$arrDefault = array();
		$arrDefaultText = array();
		$stringDefault = "";
		$styleDate = $stringDefault."";
		$styleUsia = $stringDefault." style='display:none;'";
		$link_rekam_medis = "";
		$link_loket = $this->base_url_site."loket/";
		///$id_kunjungan_loket = 0;

		if($mode=='insert'){
			$breadcrumbs = array(
				"Home" =>$this->base_url_site,
				"Pengguna Layanan" => $this->base_url,
				"Form Registrasi Pengguna Layanan" => "#",
            );
			$titleForm = "Tambah Pengguna Layanan";

		}else if($mode=='loket'){
			$breadcrumbs = array(
				"Home" =>$this->base_url_site,
				"Pengguna Layanan" => $this->base_url,
				"Form Tambah Pengguna Layanan" => "#",
            );
			$titleForm = "Tambah Pengguna Layanan";
			$id_kunjungan_loket = $id;

		}else if($mode=="ugd"){
			$breadcrumbs = array(
				"Home" =>$this->base_url_site,
				"Pengguna Layanan UGD" => $this->base_url,
				"Form Tambah Pengguna Layanan" => "#",
            );
			$titleForm = "Tambah Pengguna Layanan UGD";
			$link_loket = $this->base_url_site."loket-ugd/";
		}else if($mode=="rawat-inap"){
			$breadcrumbs = array(
				"Home" =>$this->base_url_site,
				"Pengguna Layanan Rawat Inap" => $this->base_url,
				"Form Tambah Pengguna Layanan" => "#",
            );
			$titleForm = "Tambah Pengguna Layanan Rawat Inap";
			$link_loket = $this->base_url_site."loket-rawat-inap/";

		}else if($mode=='detail'){
			$detailPasien = $this->pasien_model->getDetail($id);
			
			if(empty($detailPasien)){
				redirect($this->base_url);
			}
			$breadcrumbs = array(
				"Home" =>$this->base_url_site,
				"Pasien" => $this->base_url,
				"Detail Pengguna Layanan" => "#",
            );
			$titleForm = "Detail Pengguna Layanan No Anggota : ".$detailPasien['txtNoAnggota'];
			$arrDefault = array("disabled"=>"disabled");
			$arrDefaultText = array("disabled"=>"disabled" , "rows"=>"3");
			$stringDefault = "disabled='disabled'";
			if($detailPasien['dtTanggalLahir']!=""){
				$styleDate = $stringDefault."";
				$styleUsia = $stringDefault." style='display:none;'";
			}else if($detailPasien['dtUsiaTemp']!=""){
				$styleDate = $stringDefault." style='display:none;'";
				$styleUsia = $stringDefault;
			}
			$link_rekam_medis = $this->base_url_rekmed."detail-rekam-medis/".$detailPasien['txtIdPasien'];
		}
		$dt['link_input_rekam_medis'] = $link_rekam_medis;
		$dt['breadcrumbs'] = $this->setBreadcrumbs($breadcrumbs);
		$dt['title_form'] = $titleForm;
		foreach($arrDetaiPasien as $pasien){
			$$pasien = isset($detailPasien[$pasien]) ? $detailPasien[$pasien] : "";
		}
	
		
		$link_finger_registrasi = $this->base_url_site."finger/register/".$txtIdPasien;
		$link_registrasi = "finspot:FingerspotReg;".base64_encode($link_finger_registrasi);
		$link_tanda_tangan = "EmedicaSignature://open?id=".$txtIdPasien."-1";

		$dt['linkRegisterFinger'] = $link_registrasi;
		$dt['linkTandaTanganPasien'] = $link_tanda_tangan;
		$dt['inputIdPasien'] = $this->form_builder->inputHidden("inputIdPasien" , $txtIdPasien);
		$dt['inputMode'] = $this->form_builder->inputHidden("inputMode" , $mode);
		$dt['input_id_kunjungan'] = $this->form_builder->inputHidden("input_id_kunjungan" , $id_kunjungan_loket);
		///$dt['inputNoAnggota'] = $this->form_builder->inputText("No Anggota" , "inputNoAnggota" , $txtNoAnggota , "col-sm-3",  $arrDefault);
		$dt['inputNoAnggota'] = form_input("inputNoAnggota" , $txtNoAnggota , 'id="inputNoAnggota" class="form-control" readonly=""'.$stringDefault);
		$dt['bitSementara'] = form_checkbox('bitSementara' , '1' , $bitSementara);
		$dt['inputNoAnggotaLama'] = form_input("inputNoAnggotaLama" , $txtNoAnggota , 'id="inputNoAnggotaLama" placeholder="No Rekam Medis Lama" '.$stringDefault);
		$dt['inputNamaPasien'] = $this->form_builder->inputText("Nama Pengguna Layanan" , "inputNamaPasien" , $txtNamaPasien,"col-sm-3", $arrDefault);
		///$dt['inputNoIdentitas'] = $this->form_builder->inputText("No. KTP / NIK" , "inputNoIdentitas" , $txtNoKtp,"col-sm-3", $arrDefault);
		$dt['inputNoIdentitas'] = form_input("inputNoIdentitas" , $txtNoKtp , 'class="form-control" id="inputNoIdentitas" '.$stringDefault);
		$dt['inputNoKK'] = $this->form_builder->inputText("No. Kartu Keluarga" , "inputNoKK" , $txtNoKk , "col-sm-3",$arrDefault);
		$dt['inputNamaKK'] = $this->form_builder->inputText("Nama Kepala Keluarga" , "inputNamaKK" , $txtNamaKepKk , "col-sm-3",$arrDefault);
		$dt['txtStatusKK'] = $this->form_builder->inputText("Status Keluarga" , "txtStatusKK" , $txtStatusKK , "col-sm-3",$arrDefault);
		$empty = array('' => 'Pilih Pekerjaan','0' =>'Lain - Lain' );
		$getPekerjaan = $this->pekerjaan_model->getData();
		$pekerjaan_list = arrayDropdown($getPekerjaan,'id_pekerjaan','pekerjaan',$empty);
		$dt['txtPekerjaan'] = $this->form_builder->inputText("" , "txtPekerjaan" , '' , "col-sm-3",array('disabled' => 'disabled', ));
		$agama_list = $this->config->item("agama_list");
		$dt['inputAgama'] = $this->form_builder->inputDropdown("Agama" , "inputAgama" ,$txtAgama, $agama_list ,$arrDefault);
		//$dt['inputPekerjaan'] = $this->form_builder->inputText("Pekerjaan" , "inputPekerjaan" , $txtPekerjaan,"col-sm-3",$arrDefault);
		$dt['inputPekerjaan'] = $this->form_builder->inputDropdown("Pekerjaan" , "inputPekerjaan" ,$intIdPekerjaan, $pekerjaan_list ,array_merge($arrDefault,array('onChange' => 'pekerjaan()', )) );
		
		///$dt['txtNoHandphone'] = $this->form_builder->inputText("No. Handphone" , "txtNoHandphone" , $txtNoHandphone,"col-sm-3",$arrDefault);
		////$dt['txtNoHandphone'] = "";
		$dt['txtNoHandphone'] = form_input("txtNoHandphone" , $txtNoHandphone , 'class="form-control" id="txtNoHandphone" placeholder="Cth : 81335240213" '.$stringDefault);

		$jk_list = $this->config->item("jk_list");
		$dt['selectJenisKelamin'] = $this->form_builder->inputDropdown("Jenis Kelamin" , "selectJenisKelamin" ,$charJkPasien, $jk_list , $arrDefault);
		$dt['inputTempatLahir'] = $this->form_builder->inputText("Tempat Lahir" , "inputTempatLahir" , $txtTempatLahir , "col-sm-3",$arrDefault);
		$dt['inputTanggalLahir'] = form_input("inputTanggalLahir" , $dtTanggalLahir , "class='form-control pull-right datepicker' placeholder='Tanggal Lahir' id='inputTanggalLahir' ".$styleDate."");
		$dt['inputUsia'] = form_input("inputUsia" , $txtUsiaTemp , "class='form-control pull-right' id='inputUsia' placeholder='Usia Pengguna Layanan (Ex:65 Tahun)' ".$styleUsia."");
		
		$jamkes_list = $this->jaminan_kesehatan->getListDataJaminan();
		$jamkes_list["0"] = "-Umum-";
		$intIdJaminanKesehatan = !empty($intIdJaminanKesehatan) ? $intIdJaminanKesehatan : 0;

		$dt['selectJamkes'] =  form_dropdown('selectJamkes' , $jamkes_list, $intIdJaminanKesehatan , 'class="form-control" id="selectJamkes" onchange="setNoJaminanKesehatan()" '.$stringDefault.'');
		$dt['inputNoJamkes'] = form_input("inputNoJamkes" , $txtNoIdJaminanKesehatan , "class='form-control' id='inputNoJamkes' placeholder='No Jaminan Kesehatan' readonly='readonly'");
		$dt['inputAlamat'] = $this->form_builder->inputTextArea("Alamat" , "inputAlamat" , $txtAlamatPasien , "col-sm-3",$arrDefaultText);
		/// Kecamatan
		$list_kecamatan = $this->session->userdata("sipt_kecamatan");
		$default_kecamatan = $mode!="detail" ? $this->session->userdata("sipt_id_kecamatan") : $intIdKecamatan;
		$arrKecamatan[""] = "-Pilih Kecamatan-";
		foreach($list_kecamatan as $kecamatan){
			$arrKecamatan[$kecamatan['IDKecamatan']] = $kecamatan['Nama'];
		}
		$dt['selectKecamatan'] = $this->form_builder->inputDropdown("Kecamatan" , "selectKecamatan" ,$default_kecamatan, $arrKecamatan , array_merge(array("onchange"=>"getKelurahan()") , $arrDefault));
		
		$arrKelurahan[""] = "-Pilih Kelurahan-";
		if(!empty($intIdKelurahan)){
			$dataKelurahan = $this->kelurahan_model->getDetailKelurahan($intIdKelurahan);
			$arrKelurahan[$dataKelurahan['IDKelurahan']] = $dataKelurahan['Nama'];
		}
		$dt['selectKelurahan'] = $this->form_builder->inputDropdown("Kelurahan" , "selectKelurahan" ,$intIdKelurahan, $arrKelurahan , $arrDefault);
		$dt['selectStatus'] = $this->form_builder->inputDropdown("Status Pengguna Layanan" , "selectStatus" ,$bitStatusPasien, $this->config->item("status_list") , $arrDefault);
		
		$arrWilayah = array("dalam"=>"Dalam Wilayah" , "luar" => "Luar Wilayah");
		$dt['selectWilayah'] = $this->form_builder->inputDropdown("Wilayah Pengguna Layanan" , "selectWilayah" ,$txtWilayahPasien, $arrWilayah, $arrDefault);

		/// Finger Radio
		$checkFinger = $this->finger_model->checkNumber($txtIdPasien);
		$registerFingerStatus = $checkFinger > 0 ? "Sudah" : "Belum";
		$dt['inputFinger'] = $this->form_builder->inputText("Rekam Sidik Jari" , "txtFingerPrintStatus" , $registerFingerStatus,"col-sm-3",array("readonly"=>""));

		//// Tanda Tangan
		$detailTandaTangan = $this->pasien_model->getTTDPasienV2($txtIdPasien);
		$dt['frm_ttd'] = form_hidden("txtTtdPasien" , $txtTtdPasien);
		$dt['inputTTDBase64'] = form_input("inputTTDBase64" , "" , "class='form-control' id='inputTTDBase64' readonly='readonly'");
		$dt['txtTandaTangan'] = !empty($detailTandaTangan) ? $this->base_url_site . "imagesign/getImageSign/". $txtIdPasien : "";		

		$dt['mode_form'] = $mode;
		$dt['link_pasien'] = $this->base_url;
		$dt['link_ke_pendaftaran'] = $this->base_url_loket."daftar/".$txtIdPasien . '/'.$id_kunjungan_loket;
		$dt['link_loket'] = $link_loket;
		$dt['status_sidik'] = $registerFingerStatus;
		$retVal = $this->load->view("pasien/form_loket_v3" , $dt , true);	
        return $retVal;
	}

	private function getNoAnggotaBaru($numberAdd = 0){
		$getNoAnggota = $this->pasien_model->getNoPasienTerbaru();
		$noAnggota = ($getNoAnggota['intNoAnggota'] + 1) + $numberAdd;
		$cekNoAnggota = $this->pasien_model->cekNoAnggotaPasien($noAnggota);

		if($cekNoAnggota > 0){
		$number = $numberAdd + 1;
			//echopre($numberAdd);
			
		$noAnggota = $this->getNoAnggotaBaru($number);
		
		}
		
		
		return $noAnggota;

	}

	private function getNoSementara($numberAdd = 0){
		$getNoAnggota = $this->pasien_model->getJumlahPasienSementara();
		$noAnggota = ($getNoAnggota + 1) + $numberAdd;
		$noAnggota = "SM".$noAnggota;
		$cekNoAnggota = $this->pasien_model->cekNoAnggotaPasien($noAnggota);
		
		///echo $cekNoAnggota;
		
		if($cekNoAnggota > 0){
			$numberAdd += 1;
			$noAnggota = $this->getNoSementara($numberAdd);
		}

		return $noAnggota;
	}

	
	
	public function saveDataPasien(){
		$this->isAjaxRequest();
		
		$status = false;
		$message = "Failed To Save";
		$mode = $this->input->post("inputMode");
		
		if(($mode=="insert") || ($mode=="loket") || ($mode=="ugd") || ($mode=="rawat-inap")){
			$mode_form = "insert";
		}else{
			$mode_form = "update";
		}

		$txtTandaTangan = str_replace(base_url(),"" , $this->input->post("txtTtdPasien"));
		///$bitSementara = $this->input->post("bitSementara");
		$bitSementara = 0;
		$id_pasien = $this->input->post("inputIdPasien");
		$inputNoAnggota = $this->input->post("inputNoAnggota");
		$txtPekerjaan = $this->input->post("inputPekerjaan");
		$txtPekerjaan_text = $this->input->post("txtPekerjaan");
		if ($txtPekerjaan == "0") {
			$input = $this->inputPekerjaan($txtPekerjaan_text);
			$frm_pekerjaan = $input;
		}else{
			$frm_pekerjaan = $txtPekerjaan;
		}
		$arrInsert = array(
			"txtNoKtp" => $this->input->post("inputNoIdentitas"),
			"txtNoKk" => $this->input->post("inputNoKK"),
			"txtNamaKepKk" => $this->input->post("inputNamaKK"),
			"txtStatusKK" => $this->input->post("txtStatusKK"),
			"txtNamaPasien" => $this->input->post("inputNamaPasien"),
			"txtTempatLahir" => $this->input->post("inputTempatLahir"),
			"dtTanggalLahir" => $this->input->post("inputTanggalLahir"),
			"txtAlamatPasien" => $this->input->post("inputAlamat"),
			"intIdKecamatan" => $this->input->post("selectKecamatan"),
			"intIdKelurahan" => $this->input->post("selectKelurahan"),
			"txtAgama" => $this->input->post("inputAgama"),
			"charJkPasien" => $this->input->post("selectJenisKelamin"),
			
			"txtNoHandphone" => $this->input->post("txtNoHandphone"),
			"intIdJaminanKesehatan" => $this->input->post("selectJamkes"),
			"txtNoIdJaminanKesehatan" => $this->input->post("inputNoJamkes"),
			"bitStatusPasien" => $this->input->post("selectStatus"),
			"txtUsiaTemp" => $this->input->post("inputUsia"),
			"txtWilayahPasien" => $this->input->post("selectWilayah"),
			"txtTtdPasien" => $txtTandaTangan,
			"intIdPekerjaan" =>  $frm_pekerjaan,
		);
		//echopre($arrInsert);
		//// Proses Insert
		if($mode_form=="insert"){
			///$noAnggota = ($bitSementara==1) ? $this->getNoSementara() : $this->getNoAnggotaBaru();
			$noAnggota = $this->getNoAnggotaBaru();
			$arrInsert['txtIdPasien'] = $id_pasien;
			$arrInsert['dtCreatedPasien'] = date("Y-m-d H:i:s");
			$arrInsert['dtUpdatePasien'] = date("Y-m-d H:i:s");
			$arrInsert['txtNoAnggota'] = $noAnggota;
			///echopre($arrInsert);die;
			$resVal = $this->pasien_model->saveData($arrInsert);
		}else{
			$noAnggota = $inputNoAnggota;
			/*
			if(($bitSementara!=1) && (!is_numeric($inputNoAnggota))){
				$noAnggota = $this->getNoAnggotaBaru();
			}
			*/
			
			$arrInsert['dtUpdatePasien'] = date("Y-m-d H:i:s");
			$arrInsert['txtNoAnggota'] = $noAnggota;
			///echopre($arrInsert);die;
			$resVal = $this->pasien_model->update($arrInsert , $id_pasien);
		}

		if($bitSementara!=1){
			$resNomor = $this->pasien_model->insertNoPasienTerbaru($noAnggota);
		}
		
		if($resVal['status']){
			$status = true;
			$message = "Success To Save";
		}

		$retVal = array();
		$mode = $this->input->post("inputMode");
		$retVal['status'] = $status;
		$retVal['message'] = $message;
		$retVal['mode'] = $mode;
		$retVal['idPasien'] = $id_pasien;
		$retVal['no_anggota'] = $status==true ? $arrInsert['txtNoAnggota'] : "";
		$retVal['link_rekmed'] = $this->base_url_rekmed."add-rekam-medis/".$id_pasien;
		echo json_encode($retVal);
	}
	
	public function getDataPasienSearch(){
		if(!$this->input->is_ajax_request()){
			echo "Ilegal Input";
			die;	
		}
		
		$idInput = $this->input->post("id_pasien");
		$dataPasien = $this->pasien_model->getPasienById($idInput);
		$jk_list = $this->config->item("jk_list");
		$status = false;
		$resVal = array();
		if(count($dataPasien) > 0){
			$status = true;
			foreach($dataPasien as $rowPasien){
			$jenis_kelamin = $jk_list[$rowPasien['charJkPasien']];
			$tanggal_lahir = indonesian_date($rowPasien['dtTanggalLahir']);
			$resVal[] = array($rowPasien['txtNoKtp'] ,
							  $rowPasien['txtNoAnggota'] ,
							  $rowPasien['txtNoKk'] ,
							  $rowPasien['txtNamaPasien'] ,
							  $tanggal_lahir ,
							  $jenis_kelamin,
							  $rowPasien['txtIdPasien'] 
							  );	
			}
		}
		$retVal['status'] = $status;
		$retVal['data'] = $resVal;
		echo json_encode($retVal);
	}

	public function getDataByNoAnggota(){
		if(!$this->input->is_ajax_request()){
			echo "Ilegal Input";
			die;	
		}

		$retVal = array();
		$status = false;
		$txtNoAnggota = $this->input->post("txtNoAnggota");
		$jk_list = $this->config->item("jk_list");
		$dataPasien = $this->pasien_model->getDataPasienNoAnggota($txtNoAnggota);
		$resVal = array();
		if(count($dataPasien) > 0){
			
			$status = true;
			foreach($dataPasien as $rowPasien){
				$jenis_kelamin = $jk_list[$rowPasien['charJkPasien']];
				$tanggal_lahir = indonesian_date($rowPasien['dtTanggalLahir']);
				$resVal[] = array(
					$rowPasien['txtNoAnggota'],
					$rowPasien['txtNamaPasien'],
					$tanggal_lahir,
					$jenis_kelamin,
					$rowPasien['txtIdPasien'],
					$rowPasien['txtNoIdJaminanKesehatan'],
				);
			}
		}

		$retVal['status'] = $status;
		$retVal['data'] = $resVal;
		echo json_encode($retVal);
	}
	
	private function generateIdPasien(){
		$rand_number = rand(1000 , 9999);
		$idpasien = "PSN" . date("YmdHis").$rand_number;
		$len = strlen($idpasien);
		return $idpasien;
	}
	
	public function getNoAnggotaPasien($idPasien){
		if(!$this->input->is_ajax_request()){
			echo "Die, Ilegall Access";
			die;
		}
		$dataPasien = $this->pasien_model->getDetail($idPasien);
		$this->setJsonOutput($dataPasien);
	}
    
	public function getDataKelurahan(){
		if(!$this->input->is_ajax_request()){
			echo "Die, Ilegall Access";
			die;
		}
		
		$this->load->model("kelurahan_model");
		$status = false;
		$html = "<option value='0'>-Pilih Kecamatan-</option>";
		
		$idKecamatan = $this->input->post("idKecamatan");
		$dataKelurahan = $this->kelurahan_model->getDataKelurahan($idKecamatan , 'IDKelurahan,Nama');
		if(count($dataKelurahan) > 0){
			$status = true;
			foreach($dataKelurahan as $valKelurahan){
				$html .= "<option value='".$valKelurahan['IDKelurahan']."'>".$valKelurahan['Nama']."</option>";
			}
		}
		$retVal['status'] = $status;
		$retVal['html'] = $html;
		echo json_encode($retVal);
	}
	
	public function getListDataPasien(){
        if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";
        }
        $where = $this->input->get("q");
        $page = !empty($_GET['page']) ? $this->input->get("page") : 1;
        $start = ($page - 1) * $this->limit;
        $data_pasien = $this->pasien_model->getListDataPasien($where , $start , $this->limit);
        $jumlah = $this->pasien_model->getCountListDataPasien($where);
        $retVal = array();
        $retVal['total_count'] = $jumlah;
        $retVal['incomplete_results'] = false;
        $retVal['items'] = $data_pasien;
        $this->setJsonOutput($retVal);
    }

	public function checkNoAnggota(){
		if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";die;
        }
		///echopre($_POST);
		$txtNoAnggota = $this->input->post("inputNoAnggota");
		$cekNoAnggota = $this->pasien_model->cekNoAnggota($txtNoAnggota);
		if($cekNoAnggota < 1){
			echo json_encode("true");
		}else{
			echo json_encode("No Anggota Sudah Ada! Gunakan No Anggota Lain");
		}
	}
	public function getFormPasienModal($mode,$id=""){
		$this->load->model("finger_model");
		$dt = array();
		$breadcrumbs = array();
		$titleForm = "";
		$arrDetaiPasien = array(
							"txtIdPasien",
							"txtNoAnggota",
							"intNoAnggota",
							"txtNoKtp",
							"txtNoKk",
							"txtNamaKepKk",
							"txtNamaPasien",
							"txtTempatLahir",
							"dtTanggalLahir",
							"txtAlamatPasien",
							"intIdKecamatan",
							"intIdKelurahan",
							"txtAgama",
							"charJkPasien",
							"txtPekerjaan",
							"intIdJaminanKesehatan",
							"txtNoIdJaminanKesehatan",
							"txtNoHandphone",
							"txtUsiaTemp",
							"txtWilayahPasien",
							"txtStatusKK",
							"txtTtdPasien",
							"bitStatusPasien",
							"bitSementara",
						);
		$detailPasien = array();
		$detailPasien['txtIdPasien'] = $this->generateIdPasien();
		$arrDefault = array();
		$arrDefaultText = array();
		$stringDefault = "";
		$styleDate = $stringDefault."";
		$styleUsia = $stringDefault." style='display:none;'";
		$link_rekam_medis = "";
		$link_loket = $this->base_url_site."loket/";
		
		if($mode=='insert'){
			$breadcrumbs = array(
				"Home" =>$this->base_url_site,
				"Pengguna Layanan" => $this->base_url,
				"Form Registrasi Pengguna Layanan" => "#",
            );
			$titleForm = "Tambah Pengguna Layanan";
		}else if($mode=='loket'){
			$breadcrumbs = array(
				"Home" =>$this->base_url_site,
				"Pengguna Layanan" => $this->base_url,
				"Form Tambah Pengguna Layanan" => "#",
            );
			$titleForm = "Tambah Pengguna Layanan";
		}else if($mode=="ugd"){
			$breadcrumbs = array(
				"Home" =>$this->base_url_site,
				"Pengguna Layanan UGD" => $this->base_url,
				"Form Tambah Pengguna Layanan" => "#",
            );
			$titleForm = "Tambah Pengguna Layanan UGD";
			$link_loket = $this->base_url_site."loket-ugd/";
		}else if($mode=="rawat-inap"){
			$breadcrumbs = array(
				"Home" =>$this->base_url_site,
				"Pengguna Layanan Rawat Inap" => $this->base_url,
				"Form Tambah Pengguna Layanan" => "#",
            );
			$titleForm = "Tambah Pengguna Layanan Rawat Inap";
			$link_loket = $this->base_url_site."loket-rawat-inap/";

		}else if($mode=='detail'){
			$detailPasien = $this->pasien_model->getDetail($id);
			
			if(empty($detailPasien)){
				redirect($this->base_url);
			}
			$breadcrumbs = array(
				"Home" =>$this->base_url_site,
				"Pasien" => $this->base_url,
				"Detail Pengguna Layanan" => "#",
            );
			$titleForm = "Detail Pengguna Layanan No Anggota : ".$detailPasien['txtNoAnggota'];
			$arrDefault = array("disabled"=>"disabled");
			$arrDefaultText = array("disabled"=>"disabled" , "rows"=>"3");
			$stringDefault = "disabled='disabled'";
			if($detailPasien['dtTanggalLahir']!=""){
				$styleDate = $stringDefault."";
				$styleUsia = $stringDefault." style='display:none;'";
			}else if($detailPasien['dtUsiaTemp']!=""){
				$styleDate = $stringDefault." style='display:none;'";
				$styleUsia = $stringDefault;
			}
			$link_rekam_medis = $this->base_url_rekmed."add-rekam-medis/".$id;
		}
		$dt['link_input_rekam_medis'] = $link_rekam_medis;
		$dt['breadcrumbs'] = $this->setBreadcrumbs($breadcrumbs);
		$dt['title_form'] = $titleForm;
		
		foreach($arrDetaiPasien as $pasien){
			$$pasien = isset($detailPasien[$pasien]) ? $detailPasien[$pasien] : "";
		}
		
		$link_finger_registrasi = $this->base_url_site."finger/register/".$txtIdPasien;
		$link_registrasi = "finspot:FingerspotReg;".base64_encode($link_finger_registrasi);
		$link_tanda_tangan = "EmedicaSignature://open?id=".$txtIdPasien."-1";
		$dt['linkRegisterFinger'] = $link_registrasi;
		$dt['linkTandaTanganPasien'] = $link_tanda_tangan;
		$dt['inputIdPasien'] = $this->form_builder->inputHidden("inputIdPasien" , $txtIdPasien);
		$dt['inputMode'] = $this->form_builder->inputHidden("inputMode" , $mode);
		///$dt['inputNoAnggota'] = $this->form_builder->inputText("No Anggota" , "inputNoAnggota" , $txtNoAnggota , "col-sm-3",  $arrDefault);
		$dt['inputNoAnggota'] = form_input("inputNoAnggota" , $txtNoAnggota , 'id="inputNoAnggota" class="form-control" readonly=""'.$stringDefault);
		$dt['bitSementara'] = form_checkbox('bitSementara' , '1' , $bitSementara);
		$dt['inputNoAnggotaLama'] = form_input("inputNoAnggotaLama" , $txtNoAnggota , 'id="inputNoAnggotaLama" placeholder="No Rekam Medis Lama" '.$stringDefault);
		$dt['inputNamaPasien'] = $this->form_builder->inputText("Nama Pengguna Layanan" , "inputNamaPasien" , $txtNamaPasien,"col-sm-3", $arrDefault);
		$dt['inputNoIdentitas'] = $this->form_builder->inputText("No. KTP / NIK" , "inputNoIdentitas" , $txtNoKtp,"col-sm-3", $arrDefault);
		$dt['inputNoKK'] = $this->form_builder->inputText("No. Kartu Keluarga" , "inputNoKK" , $txtNoKk , "col-sm-3",$arrDefault);
		$dt['inputNamaKK'] = $this->form_builder->inputText("Nama Kepala Keluarga" , "inputNamaKK" , $txtNamaKepKk , "col-sm-3",$arrDefault);
		$dt['txtStatusKK'] = $this->form_builder->inputText("Status Keluarga" , "txtStatusKK" , $txtStatusKK , "col-sm-3",$arrDefault);
		$getPekerjaan = $this->pekerjaan_model->getData();
	$empty = array('' => 'Pilih Pekerjaan','0' =>'Lain - Lain' );
		$getPekerjaan = $this->pekerjaan_model->getData();
		$pekerjaan_list = arrayDropdown($getPekerjaan,'id_pekerjaan','pekerjaan',$empty);
		$dt['txtPekerjaan'] = $this->form_builder->inputText("" , "txtPekerjaan" , '' , "col-sm-3",array('disabled' => 'disabled', ));
		$agama_list = $this->config->item("agama_list");
		
		$dt['inputAgama'] = $this->form_builder->inputDropdown("Agama" , "inputAgama" ,$txtAgama, $agama_list ,$arrDefault);
		//$dt['inputPekerjaan'] = $this->form_builder->inputText("Pekerjaan" , "inputPekerjaan" , $txtPekerjaan,"col-sm-3",$arrDefault);
		$dt['inputPekerjaan'] = $this->form_builder->inputDropdown("Pekerjaan" , "inputPekerjaan" ,$txtPekerjaan, $pekerjaan_list ,array_merge($arrDefault,array("style"=>"width:100%","onChange" => "pekerjaan()", )));
		
		///$dt['txtNoHandphone'] = $this->form_builder->inputText("No. Handphone" , "txtNoHandphone" , $txtNoHandphone,"col-sm-3",$arrDefault);
		////$dt['txtNoHandphone'] = "";
		$dt['txtNoHandphone'] = form_input("txtNoHandphone" , $txtNoHandphone , 'class="form-control" id="txtNoHandphone" placeholder="Cth : 81335240213" '.$stringDefault);

		$jk_list = $this->config->item("jk_list");
		$dt['selectJenisKelamin'] = $this->form_builder->inputDropdown("Jenis Kelamin" , "selectJenisKelamin" ,$charJkPasien, $jk_list , $arrDefault);
		$dt['inputTempatLahir'] = $this->form_builder->inputText("Tempat Lahir" , "inputTempatLahir" , $txtTempatLahir , "col-sm-3",$arrDefault);
		$dt['inputTanggalLahir'] = form_input("inputTanggalLahir" , $dtTanggalLahir , "class='form-control pull-right datepicker' placeholder='Tanggal Lahir' id='inputTanggalLahir' ".$styleDate."");
		$dt['inputUsia'] = form_input("inputUsia" , $txtUsiaTemp , "class='form-control pull-right' id='inputUsia' placeholder='Usia Pengguna Layanan (Ex:65 Tahun)' ".$styleUsia."");
		
		$jamkes_list = $this->jaminan_kesehatan->getListDataJaminan();
		$jamkes_list["0"] = "-Umum-";
		$intIdJaminanKesehatan = !empty($intIdJaminanKesehatan) ? $intIdJaminanKesehatan : 0;

		$dt['selectJamkes'] =  form_dropdown('selectJamkes' , $jamkes_list, $intIdJaminanKesehatan , 'class="form-control" id="selectJamkes" onchange="setNoJaminanKesehatan()" '.$stringDefault.'');
		$dt['inputNoJamkes'] = form_input("inputNoJamkes" , $txtNoIdJaminanKesehatan , "class='form-control' id='inputNoJamkes' placeholder='No Jaminan Kesehatan' readonly='readonly'");
		$dt['inputAlamat'] = $this->form_builder->inputTextArea("Alamat" , "inputAlamat" , $txtAlamatPasien , "col-sm-3",$arrDefaultText);
		/// Kecamatan
		$list_kecamatan = $this->session->userdata("sipt_kecamatan");
		$default_kecamatan = $mode!="detail" ? $this->session->userdata("sipt_id_kecamatan") : $intIdKecamatan;
		$arrKecamatan[""] = "-Pilih Kecamatan-";
		foreach($list_kecamatan as $kecamatan){
			$arrKecamatan[$kecamatan['IDKecamatan']] = $kecamatan['Nama'];
		}
		$dt['selectKecamatan'] = $this->form_builder->inputDropdown("Kecamatan" , "selectKecamatan" ,$default_kecamatan, $arrKecamatan , array_merge(array("onchange"=>"getKelurahanModal()","class"=>"select2","style"=>"width:100%") , $arrDefault));
		
		$arrKelurahan[""] = "-Pilih Kelurahan-";
		if(!empty($intIdKelurahan)){
			$dataKelurahan = $this->kelurahan_model->getDetailKelurahan($intIdKelurahan);
			$arrKelurahan[$dataKelurahan['IDKelurahan']] = $dataKelurahan['Nama'];
		}
		$dt['selectKelurahan'] = $this->form_builder->inputDropdown("Kelurahan" , "selectKelurahan" ,$intIdKelurahan, $arrKelurahan , array_merge(array("class"=>"select2","style"=>"width:100%") , $arrDefault));
		$dt['selectStatus'] = $this->form_builder->inputDropdown("Status Pengguna Layanan" , "selectStatus" ,$bitStatusPasien, $this->config->item("status_list") , $arrDefault);
		
		$arrWilayah = array("dalam"=>"Dalam Wilayah" , "luar" => "Luar Wilayah");
		$dt['selectWilayah'] = $this->form_builder->inputDropdown("Wilayah Pengguna Layanan" , "selectWilayah" ,$txtWilayahPasien, $arrWilayah, $arrDefault);

		/// Finger Radio
		$checkFinger = $this->finger_model->checkNumber($txtIdPasien);
		$registerFingerStatus = $checkFinger > 0 ? "Sudah" : "Belum";
		$dt['inputFinger'] = $this->form_builder->inputText("Rekam Sidik Jari" , "txtFingerPrintStatus" , $registerFingerStatus,"col-sm-3",array("readonly"=>""));

		//// Tanda Tangan
		$detailTandaTangan = $this->pasien_model->getTTDPasienV2($txtIdPasien);
		$dt['frm_ttd'] = form_hidden("txtTtdPasien" , $txtTtdPasien);
		$dt['inputTTDBase64'] = form_input("inputTTDBase64" , "" , "class='form-control' id='inputTTDBase64' readonly='readonly'");
		$dt['txtTandaTangan'] = !empty($detailTandaTangan) ? $this->base_url_site . "imagesign/getImageSign/". $txtIdPasien : "";		

		$dt['mode_form'] = $mode;
		$dt['link_pasien'] = $this->base_url;
		$dt['link_ke_pendaftaran'] = $this->base_url_loket."daftar/".$txtIdPasien;
		$dt['link_loket'] = $link_loket;
		$dt['status_sidik'] = $registerFingerStatus;
		$retVal = $this->load->view("pasien/form_loket_modal" , $dt);
		return 	$retVal;
        
	}
	private function inputPekerjaan($data){
		 $arrDataInput = array(
                "jenis_pekerjaan" => 'non_formal',
                "pekerjaan" => $data,
               
            );
			$primaryID = "";
            $resVal = $this->pekerjaan_model->saveUpdate($arrDataInput , $primaryID);
			$retVal =  $resVal['id'];
			return $retVal;
	}
		
}
