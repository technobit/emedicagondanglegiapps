<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Grafik extends MY_Controller {

    var $meta_title = "Grafik";
    var $meta_desc = "";
    var $main_title = "";
    var $menu_key = "";
    var $dtBreadcrumbs = array();  

    public function __construct() {
        parent::__construct();
        $this->load->model('pelayanan_model');
        $this->load->model('register_model');
        $this->load->library("Excel");
    }
    public function index() {        
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "menu_key" => $this->menu_key,
			"akses_key" => "is_view",
            "container" => $this->_index(),
            "custom_js" => array(
                
            ),
            "custom_css" => array(),
        );  
        $this->_render("default",$dt);             
    }


    public function jumlah_kunjungan(){
        $this->dtBreadcrumbs = array(
            "Home" =>base_url(),
            "Grafik" => base_url(),
            "Jumlah Kunjungan" => "#",
        );

        $this->meta_title = "Grafik Jumlah Kunjungan";
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "menu_key" => "grafik_pengunjung_per_poli",
			"akses_key" => "is_view",
            "container" => $this->_build_rekap_pengunjung(),
            "custom_js" => array(
                ASSETS_URL."plugins/chartjs/Chart.min.js",
                ASSETS_JS_URL."grafik/rekap_jumlah_pengunjung.js"
			),
            "custom_css" => array(),
        );  
        
        $this->_render("default",$dt);             
    }


    private function _build_rekap_pengunjung(){

        $listPelayanan = $this->pelayanan_model->getListDataPelayanan();
        $arrPelayanan = array("0"=>"Semua");
        foreach ($listPelayanan as $valuePelayanan) {
            # code...
            $arrPelayanan[$valuePelayanan['intIdPelayanan']] = $valuePelayanan['txtNama'];
        }

        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['frmPelayanan'] = $this->form_builder->inputDropdown("Pelayanan" , "cmbPoli" , "" , $arrPelayanan);
        $dt['title'] = $this->meta_title;
        $ret = $this->load->view("grafik/jumlah_kunjungan" , $dt , true);
        return $ret;
    }

    public function rekapitulasi_jumlah_pengunjung(){
        $this->dtBreadcrumbs = array(
            "Home" =>base_url(),
            "Rekapitulasi" => base_url(),
            "Data Jumlah Pengunjung" => "#",
        );
        
        $this->meta_title = "Rekapitulasi Jumlah Pengunjung";
        $dt = array(
            "title" => "Rekapitulasi Jumlah Pengunjung",
            "description" => "Rekapitulasi Jumlah Pengunjung",
            "menu_key" => "rekap_jumlah_pengunjung",
			"akses_key" => "is_view",
            "container" => $this->_build_jumlah_pengunjung(),
            "custom_js" => array(
                ASSETS_JS_URL."rekapitulasi/rekap_jumlah_pengunjung.js"
            ),
            "custom_css" => array(),
        );  
        
        $this->_render("default",$dt);
    }

     private function _build_jumlah_pengunjung(){

        $listPelayanan = $this->pelayanan_model->getListDataPelayanan();
        $arrPelayanan = array("0"=>"Semua");
        foreach ($listPelayanan as $valuePelayanan) {
            # code...
            $arrPelayanan[$valuePelayanan['intIdPelayanan']] = $valuePelayanan['txtNama'];
        }

        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['frmPelayanan'] = $this->form_builder->inputDropdown("Pelayanan" , "cmbPoli" , "" , $arrPelayanan);
        $dt['title'] = $this->meta_title;
        $ret = $this->load->view("rekapitulasi/rekap_jumlah_pengunjung" , $dt , true);
        return $ret;
    }

    public function jaminan_kesehatan(){
        $this->dtBreadcrumbs = array(
            "Home" =>base_url(),
            "Rekapitulasi" => base_url(),
            "Data Penggunaan Jaminan Kesehatan" => "#",
        );

        $this->meta_title = "Grafik Jaminan Kesehatan";
        $dt = array(
            "title" => "Grafik Jaminan Kesehatan",
            "description" => "Grafik Jaminan Kesehatan",
            "menu_key" => "grafik_jaminan_kesehatan",
			"akses_key" => "is_view",
            "container" => $this->_build_rekap_pengunjung_jamkes(),
            "custom_js" => array(
                ASSETS_URL."plugins/chartjs/Chart.min.js",
                ASSETS_JS_URL."grafik/jaminan_kesehatan.js"
			),
            "custom_css" => array(),
        );  
        
        $this->_render("default",$dt);
    }

    private function _build_rekap_pengunjung_jamkes(){

        $listPelayanan = $this->pelayanan_model->getListDataPelayanan();
        $arrPelayanan = array("0"=>"Semua");
        foreach ($listPelayanan as $valuePelayanan) {
            # code...
            $arrPelayanan[$valuePelayanan['intIdPelayanan']] = $valuePelayanan['txtNama'];
        }

        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['frmPelayanan'] = $this->form_builder->inputDropdown("Pelayanan" , "cmbPoli" , "" , $arrPelayanan);
        $dt['title'] = $this->meta_title;
        $ret = $this->load->view("grafik/jumlah_jamkes" , $dt , true);
        return $ret;
    }

    public function rekapitulasi_data_penyakit(){
        $this->dtBreadcrumbs = array(
            "Home" =>base_url(),
            "Rekapitulasi" => base_url(),
            "Data Penyakit" => "#",
        );

        $this->meta_title = "Rekapitulasi Jumlah Pengunjung";
        $dt = array(
            "title" => "Rekapitulasi Jumlah Pengunjung",
            "description" => "Rekapitulasi Jumlah Pengunjung",
            "menu_key" => "rekap_jumlah_pengunjung",
			"akses_key" => "is_view",
            "container" => $this->_build_data_penyakit(),
            "custom_js" => array(
                ASSETS_JS_URL."rekapitulasi/rekap_data_penyakit.js"
            ),
            "custom_css" => array(),
        );  
        
        $this->_render("default",$dt);
    }

    private function _build_data_penyakit(){

        $listPelayanan = $this->pelayanan_model->getListDataPelayanan();
        $arrPelayanan = array("0"=>"Semua");
        foreach ($listPelayanan as $valuePelayanan) {
            # code...
            $arrPelayanan[$valuePelayanan['intIdPelayanan']] = $valuePelayanan['txtNama'];
        }

        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['frmPelayanan'] = $this->form_builder->inputDropdown("Poli" , "cmbPoli" , "" , $arrPelayanan);
        $dt['title'] = $this->meta_title;
        $ret = $this->load->view("rekapitulasi/rekap_data_penyakit" , $dt , true);
        return $ret;
    }

    public function getDataRekapJumlahPengunjung(){
        if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";die;
        }

        $idPelayanan =  $this->input->post("cmbPoli");
		$start_date = $this->input->post("start_date");
        $end_date = $this->input->post("end_date");
        $status = false;
        $data = array();
        $dataJumlahKunjungan = $this->register_model->getJumlahKunjungan($start_date , $end_date, $idPelayanan);
        if(!empty($dataJumlahKunjungan)){
            $status = true;
            $data = $dataJumlahKunjungan;
        }
        $retVal = array();
        $retVal['status'] = $status;
        $retVal['data'] = $data;
        echo json_encode($retVal);
    }

    public function getDataRekapJaminanKesehatan(){
        if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";die;
        }

        $idPelayanan =  $this->input->post("cmbPoli");
		$start_date = $this->input->post("start_date");
        $end_date = $this->input->post("end_date");
        $status = false;
        $jamkes = array();
        $jumlah = array();
        $dataResult = $this->register_model->getJumlahKunjunganByJaminanKesehatan($start_date , $end_date, $idPelayanan);
        if(!empty($dataResult)){
            $status = true;
            foreach ($dataResult as $key => $value) {
                # code...
                $jamkes[] = $value['NamaJaminan'];
                $jumlah[] = $value['jumlah'];
            }
        }else{
                $jamkes = '';
                $jumlah = '';
        }
        $retVal = array();
        $retVal['status'] = $status;
        $retVal['jamkes'] = $jamkes;
        $retVal['jumlah'] = $jumlah;
        echo json_encode($retVal);
    }
    
    public function getDataRekapPengunjung(){
        if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";die;
        }

        $idPelayanan =  $this->input->post("id_pelayanan");
		$start_date = $this->input->post("start_date");
        $end_date = $this->input->post("end_date");
        $length =  $this->input->post("length");
		$start = $this->input->post("start");
		$offset = $length;
        $dataPengunjung = $this->register_model->getRekapPengunjungRawatJalan($idPelayanan , $start_date , $end_date , $start , $length);
        $countDataPengunjung = $this->register_model->getCountRekapPengunjungRawatJalan($idPelayanan , $start_date , $end_date );
        if($countDataPengunjung > 0) {
            $retVal['draw'] = $this->input->post('draw');
			$retVal['recordsTotal'] = $countDataPengunjung;
			$retVal['recordsFiltered'] = $countDataPengunjung;
			$retVal['data'] = array();
			foreach($dataPengunjung as $row){
                $jenisKelamin = $row['charJkPasien']=="L" ? "Laki-Laki" : "Perempuan";
                $jenisKunjungan = $row['bitIsBaru']=="1" ? "Baru" : "Lama";
                $jenisKasus = $row['bitIsKasusBaru']=="1" ? "Baru" : "Lama";
                $jenisWilayah = $row['txtWilayahPasien']=="dalam" ? "Dalam" : "Luar";
                $kodeICDX = $row['txtSubCategory']!="0" ? $row['txtCategory'].".".$row['txtSubCategory'] : $row['txtCategory']; 
				$retVal['data'][] = array($row['txtNoAnggota'],
										  $row['txtNamaPasien'],
                                          $row['txtUsiaPasienKunjungan'] ,
                                          $jenisKelamin,
                                          $row['NamaKelurahan'] ,
                                          $row['txtJaminanKesehatan'] ,
                                          $jenisKunjungan ,
										  $jenisWilayah ,
                                          $jenisKasus ,
                                          $kodeICDX ,
                                          $row['txtPengobatan'] ,
                                          $row['txtKeteranganKunjungan'] ,
                                          $row['txtNamaPelayanan'] ,
									);
			}
        }else{
            $retVal['draw'] = $this->input->post('draw');
			$retVal['recordsTotal'] = 0;
			$retVal['recordsFiltered'] = 0;
			$retVal['data'] = array();
        }
        $this->setJsonOutput($retVal);
    }

    public function getRekapDataPenyakit(){
        if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";die;
        }

        $idPelayanan =  $this->input->post("id_pelayanan");
		$start_date = $this->input->post("start_date");
        $end_date = $this->input->post("end_date");
        $length =  $this->input->post("length");
		$start = $this->input->post("start");
		$offset = $length;
        $dataPengunjung = $this->register_model->getRekapPengunjungRawatJalan($idPelayanan , $start_date , $end_date , $start , $length);
        $countDataPengunjung = $this->register_model->getCountRekapPengunjungRawatJalan($idPelayanan , $start_date , $end_date );
        if($countDataPengunjung > 0) {
            $retVal['draw'] = $this->input->post('draw');
			$retVal['recordsTotal'] = $countDataPengunjung;
			$retVal['recordsFiltered'] = $countDataPengunjung;
			$retVal['data'] = array();
			foreach($dataPengunjung as $row){

                $jenisKelamin = $row['charJkPasien']=="L" ? "Laki-Laki" : "Perempuan";
                
				$retVal['data'][] = array($row['txtNoAnggota'],
										  $row['txtNamaPasien'],
                                          $jenisKelamin,
                                          $row['txtUsiaPasienKunjungan'] ,
                                          $row['txtJaminanKesehatan'] ,
										  $row['txtNamaPelayanan'] ,
									);
			}
        }else{
            $retVal['draw'] = $this->input->post('draw');
			$retVal['recordsTotal'] = 0;
			$retVal['recordsFiltered'] = 0;
			$retVal['data'] = array();
        }
        $this->setJsonOutput($retVal);
    }

    public function downloadExcel($jenis_rekapitulasi , $idPelayanan , $start_date , $end_date , $start = 0 , $length = 10){
        $namaPoli = "Semua Poli";
        if($idPelayanan!=0){
            $dataPoli =  $this->pelayanan_model->getDetail($idPelayanan);
            $namaPoli = $dataPoli['txtNama'];
        }

        if($jenis_rekapitulasi=='rekap-pengunjung'){
            $titleRecap = "Rekapitulasi Pengunjung Rawat Jalan ".$namaPoli;
            $this->setHeaderExcelGrafik($titleRecap , $start_date , $end_date);
            $this->DownloadRekapPengunjung($idPelayanan , $start_date , $end_date , $start , $length);
            $filename = "Rekap_Pengunjung_Poli_".str_replace(" ","_",$namaPoli);
        }else if($jenis_rekapitulasi=='jumlah-pengunjung'){
            $titleRecap = "Rekapitulasi Pengunjung Rawat Jalan ".$namaPoli;
            $this->setHeaderExcelGrafik($titleRecap , $start_date , $end_date);
            $this->DownloadJumlahPengunjung($idPelayanan , $start_date , $end_date);
            $filename = "Rekap_Jumlah_Pengunjung_Poli_".str_replace(" ","_",$namaPoli);
        }else if($jenis_rekapitulasi=="rekap-jaminan-kesehatan"){
            $titleRecap = "Rekapitulasi Pengunjung Rawat Jalan ".$namaPoli;
            $this->setHeaderExcelGrafik($titleRecap , $start_date , $end_date);
            $this->DownloadRekapJamkes($idPelayanan , $start_date , $end_date);
            $filename = "Rekap_Jaminan_Kesehatan_Poli_".str_replace(" ","_",$namaPoli);
        }

        $this->getOutputGrafik($filename);
    }

    private function setHeaderExcelGrafik($titleRecap , $start_date , $end_date){
        $this->excel->setActiveSheetIndex(0);
        $this->excel->getActiveSheet()->setTitle('Sheet 1');
        $this->excel->getActiveSheet()->setCellValue('A1', 'Puskesmas '.APP_KELURAHAN);
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(20);
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->mergeCells('A1:N1');
        $this->excel->getActiveSheet()->setCellValue('A2', $titleRecap);
        $this->excel->getActiveSheet()->setCellValue('A3', 'Tanggal Awal');
        $this->excel->getActiveSheet()->setCellValue('A4', 'Tanggal Akhir');
        $this->excel->getActiveSheet()->setCellValue('B3', $start_date);
        $this->excel->getActiveSheet()->setCellValue('B4', $end_date);
    }

    private function getOutputGrafik( $filename){
        ///
        $filename = date('YmdHis')."-".$filename;
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache

        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');

        $objWriter->save('php://output');
    }

    private function DownloadRekapPengunjung($idPelayanan , $start_date , $end_date , $start , $length){
        
        $no = 1;
        /// Header
        $this->excel->getActiveSheet()->setCellValue('A6', "No");
        $this->excel->getActiveSheet()->setCellValue('B6', "No Anggota");
        $this->excel->getActiveSheet()->setCellValue('C6', "Nama Pasien");
        $this->excel->getActiveSheet()->setCellValue('D6', "Usia Pasien");
        $this->excel->getActiveSheet()->setCellValue('E6', "Jenis Kelamin");
        $this->excel->getActiveSheet()->setCellValue('F6', "Alamat");
        $this->excel->getActiveSheet()->setCellValue('G6', "Jenis Pembayaran");
        $this->excel->getActiveSheet()->setCellValue('H6', "Jenis Kunjungan");
        $this->excel->getActiveSheet()->setCellValue('I6', "Jenis Wilayah");
        $this->excel->getActiveSheet()->setCellValue('J6', "Jenis Kasus");
        $this->excel->getActiveSheet()->setCellValue('K6', "Kode ICD-X");
        $this->excel->getActiveSheet()->setCellValue('L6', "Pengobatan");
        $this->excel->getActiveSheet()->setCellValue('M6', "Keterangan");
        $this->excel->getActiveSheet()->setCellValue('N6', "Nama Pelayanan");
        
        $indexNo = 6;
        $dataPengunjung = $this->register_model->getRekapPengunjungRawatJalan($idPelayanan , $start_date , $end_date , $start , $length);
        foreach ($dataPengunjung as $row) {
            # code...
            $jenisKelamin = $row['charJkPasien']=="L" ? "Laki-Laki" : "Perempuan";
            $jenisKunjungan = $row['bitIsBaru']=="1" ? "Baru" : "Lama";
            $jenisKasus = $row['bitIsKasusBaru']=="1" ? "Baru" : "Lama";
            $jenisWilayah = $row['txtWilayahPasien']=="dalam" ? "Dalam" : "Luar";
            $kodeICDX = $row['txtSubCategory']!="0" ? $row['txtCategory'].".".$row['txtSubCategory'] : $row['txtCategory'];

            $this->excel->getActiveSheet()->setCellValue('A'.($indexNo + $no), $no);
            $this->excel->getActiveSheet()->setCellValue('B'.($indexNo + $no), $row['txtNoAnggota']);
            $this->excel->getActiveSheet()->setCellValue('C'.($indexNo + $no), $row['txtNamaPasien']);
            $this->excel->getActiveSheet()->setCellValue('D'.($indexNo + $no), $row['txtUsiaPasienKunjungan']);
            $this->excel->getActiveSheet()->setCellValue('E'.($indexNo + $no), $jenisKelamin);
            $this->excel->getActiveSheet()->setCellValue('F'.($indexNo + $no), $row['NamaKelurahan']);
            $this->excel->getActiveSheet()->setCellValue('G'.($indexNo + $no), $row['txtJaminanKesehatan']);
            $this->excel->getActiveSheet()->setCellValue('H'.($indexNo + $no), $jenisKunjungan);
            $this->excel->getActiveSheet()->setCellValue('I'.($indexNo + $no), $jenisWilayah);
            $this->excel->getActiveSheet()->setCellValue('J'.($indexNo + $no), $jenisKasus);
            $this->excel->getActiveSheet()->setCellValue('K'.($indexNo + $no), $kodeICDX);
            $this->excel->getActiveSheet()->setCellValue('L'.($indexNo + $no), $row['txtPengobatan']);
            $this->excel->getActiveSheet()->setCellValue('M'.($indexNo + $no), $row['txtKeteranganKunjungan']);
            $this->excel->getActiveSheet()->setCellValue('N'.($indexNo + $no), $row['txtNamaPelayanan']);
            

            
            $no++;
        }

    }

    private function DownloadJumlahPengunjung($idPelayanan , $start_date , $end_date){
        $no = 1;
        
        $this->excel->getActiveSheet()->setCellValue('A6', "Nama Poli");
        $this->excel->getActiveSheet()->setCellValue('B6', "Jumlah Kedatangan");
        $this->excel->getActiveSheet()->setCellValue('C6', "Status");

        $this->excel->getActiveSheet()->setCellValue('C7', "Antrian");
        $this->excel->getActiveSheet()->setCellValue('D7', "Sedang Di Layani");
        $this->excel->getActiveSheet()->setCellValue('E7', "Selesai Di Layani");
        $this->excel->getActiveSheet()->setCellValue('F7', "Rujukan");

        $dataJumlahKunjungan = $this->register_model->getJumlahKunjungan($start_date , $end_date, $idPelayanan);
        $indexNo = 7;
        foreach ($dataJumlahKunjungan as $rows) {
            # code...
            $this->excel->getActiveSheet()->setCellValue('A'.($indexNo + $no), $rows['NAMA_PELAYANAN']);
            $this->excel->getActiveSheet()->setCellValue('B'.($indexNo + $no), $rows['jumlah']);
            $this->excel->getActiveSheet()->setCellValue('C'.($indexNo + $no), $rows['jumlah_antri']);
            $this->excel->getActiveSheet()->setCellValue('D'.($indexNo + $no), $rows['jumlah_sedang_dilayani']);
            $this->excel->getActiveSheet()->setCellValue('E'.($indexNo + $no), $rows['jumlah_selesai']);
            $this->excel->getActiveSheet()->setCellValue('F'.($indexNo + $no), $rows['jumlah_rujukan']);
            $no++;
        }
    }

    private function DownloadRekapJamkes($idPelayanan , $start_date , $end_date){
        $this->excel->getActiveSheet()->setCellValue('A6', "Nama Poli");
        $this->excel->getActiveSheet()->setCellValue('B6', "Jaminan Kesehatan");
        $dataJumlahKunjungan = $this->register_model->getJumlahKunjungan($start_date , $end_date, $idPelayanan);
        $no = 1;
        $indexNo = 7;
        $dataResult = $this->register_model->getJumlahKunjunganByJaminanKesehatan($start_date , $end_date, $idPelayanan);
        foreach ($dataResult as $rows) {
            # code...
            $this->excel->getActiveSheet()->setCellValue('A'.($indexNo + $no), $rows['NamaJaminan']);
            $this->excel->getActiveSheet()->setCellValue('B'.($indexNo + $no), $rows['jumlah']);
            $no++;
        }

        
    }

    
}