<?php
class Pemeriksaan extends MY_Controller {
    var $base_url = "";
	var $limit = "20";
    var $menu = "K01";
    var $date_now = "";
    
	function __construct(){
        parent::__construct();
		$this->load->model("rekam_medis_model" , "rekam_medis");
		$this->load->model("pemeriksaan_model");
        $this->load->model("assplann_model");
        $this->base_url = $this->base_url_site."rekam-medis/";
        $this->date_now = date("Y-m-d H:i:s");
    }
    
    public function getFrmPemeriksaan($idDetailRekamMedis = ""){
		if(!$this->input->is_ajax_request()){
			echo "Die, Ilegall Access";
			die;
		}
        $link_cetak = $this->base_url_site."rawat-inap/cetak-frm-pemeriksaan/".$idDetailRekamMedis;
        $labelSave = "Save";
        $detailPemeriksaan = $this->pemeriksaan_model->getDetail($idDetailRekamMedis);
        if(!empty($detailPemeriksaan)){
            $labelSave = "Update";
            $link_cetak = $this->base_url_site."rawat-inap/cetak-frm-pemeriksaan/".$idDetailRekamMedis;
        }
        $arrPemeriksaan = array(
            "txtKeadaanUmum",
            "txtTD",
            "txtTemp",
            "txtGizi",
            "txtNadi",
            "txtRR",
            "txtBB",
            "txtTB",
            "txtKepala",
            "txtThoraxCor",
            "txtThorax",
            "txtThoraxPulmo",
            "txtAbdomen",
            "txtExtrimitas",
            "txtLaboratorium",
            "txtECG",
            "txtRadiologi",
            "txtLainLain",
            "txtKesadaran"
        );        

        foreach($arrPemeriksaan as $rowPemeriksaan){
            $$rowPemeriksaan = isset($detailPemeriksaan[$rowPemeriksaan]) ? $detailPemeriksaan[$rowPemeriksaan] : ""; 
        }

        $dt['txtIdRekmedDetail'] = $this->form_builder->inputHidden("txtIdRekmedDetail" , $idDetailRekamMedis);
        $dt['txtKeadaanUmum'] = $this->form_builder->inputTextArea("Keadaan Umum" , "txtKeadaanUmum" , $txtKeadaanUmum);

		$dt['frm_rekmed_td'] = form_input("txtTD" , $txtTD , 'class="form-control" id="txtTD"');
        $dt['frm_rekmed_temp'] = form_input("txtTemp" , $txtTemp , 'class="form-control" id="txtTemp"');
        $dt['frm_rekmed_gizi'] = form_input("txtGizi" , $txtGizi , 'class="form-control" id="txtGizi"');
        $dt['frm_rekmed_nadi'] = form_input("txtNadi" , $txtNadi , 'class="form-control" id="txtNadi"');
        $dt['frm_rekmed_rr'] = form_input("txtRR" , $txtRR , 'class="form-control" id="txtRR"');
		$dt['frm_rekmed_berat_badan'] = form_input("txtBerat" , $txtBB , 'class="form-control" id="txtBerat"');
        $dt['frm_rekmed_tinggi_badan'] = form_input("txtTinggi" , $txtTB , 'class="form-control" id="txtTinggi"');
        $kesadaran_list = $this->config->item("kesadaran_list");
        $dt['frm_rekmed_kesadaran'] = form_dropdown("txtKesadaran" , $kesadaran_list , $txtKesadaran , 'class="form-control" id="txtKesadaran"');


        $dt['txtKepala'] = $this->form_builder->inputTextArea("Kepala / Leher" , "txtKepala" , $txtKepala);
        $dt['txtThoraxCor'] = $this->form_builder->inputText("Thorax Cor" , "txtThoraxCor" , $txtThoraxCor);
        $dt['txtThorax'] = $this->form_builder->inputTextArea("Thorax" , "txtThorax" , $txtThorax);
        $dt['txtThoraxPulmo'] = $this->form_builder->inputText("Thorax Pulmo" , "txtThoraxPulmo" , $txtThoraxPulmo);
        $dt['txtAbdomen'] = $this->form_builder->inputTextArea("Abdomen" , "txtAbdomen" , $txtAbdomen);
        $dt['txtEkstrimitas'] = $this->form_builder->inputTextArea("Ekstrimitas" , "txtEkstrimitas" , $txtExtrimitas);
        $dt['link_cetak'] = $link_cetak;
        $dt['txtLaboratorium'] = $this->form_builder->inputTextArea("Laboratorium" , "txtLaboratorium" , $txtLaboratorium);
        $dt['txtECG'] = $this->form_builder->inputTextArea("EKG" , "txtECG" , $txtECG);
        $dt['txtRadiologi'] = $this->form_builder->inputTextArea("Radiologi" , "txtRadiologi" , $txtRadiologi);
        $dt['txtLainLain'] = $this->form_builder->inputTextArea("Lain-Lain" , "txtLainLain" , $txtLainLain);
		
		$dt['buttonLabel'] = $labelSave;
		$retVal = $this->load->view("rawat_inap/form_pemeriksaan" , $dt , true);
		echo $retVal;
	}
    
    public function saveData(){
		if(!$this->input->is_ajax_request()){
			echo "Die, Ilegall Access";
			die;
		}
        $txtIdRekmedDetail = $this->input->post("txtIdRekmedDetail");
        $arrPost = array(
            "txtIdRekmedDetail" => $this->input->post("txtIdRekmedDetail"),
            "txtKeadaanUmum" => $this->input->post("txtKeadaanUmum"),
            "txtTD" => $this->input->post("txtTD"),
            "txtTemp" => $this->input->post("txtTemp"),
            "txtGizi" => $this->input->post("txtGizi"),
            "txtNadi" => $this->input->post("txtNadi"),
            "txtRR" => $this->input->post("txtRR"),
            "txtBB" => $this->input->post("txtBerat"),
            "txtTB" => $this->input->post("txtTinggi"),
            "txtKepala" => $this->input->post("txtKepala"),
            "txtThorax" => $this->input->post("txtThorax"),
            "txtAbdomen" => $this->input->post("txtAbdomen"),
            "txtExtrimitas" => $this->input->post("txtEkstrimitas"),
            "txtLaboratorium" => $this->input->post("txtLaboratorium"),
            "txtECG" => $this->input->post("txtECG"),
            "txtLainLain" => $this->input->post("txtLainLain"),
            "txtKesadaran" => $this->input->post("txtKesadaran"),
        );
        
        $resVal = $this->pemeriksaan_model->saveUpdate($arrPost , $txtIdRekmedDetail);
        $retVal = array();
        $retVal['status'] = $resVal['status'];
        $retVal['message'] = $resVal['message'];
        $retVal['id'] = $resVal['id'];
        echo json_encode($retVal);
    }
   public function cetak_form($idDetailRekamMedis) {
		
		
		$dt = array(
		"title" => "Rawat Inap - Cetak Anamnesis",
		"description" => "Rawat Inap - Cetak Anamnesis",
		"container" => $this->_build_cetak_form($idDetailRekamMedis),
		"menu_key" => "cetak_assesment_planning",
		"akses_key" => "is_view",
		"custom_js" => array(),
		"custom_css" => array(),
		);
		
		$this->_render("default",$dt);
		
	}
	
	
	private function _build_cetak_form($idDetailRekamMedis){
		
		
		$breadcrumbs = array(
		"Home" =>$this->base_url_site,
		"Rawat Inap" => $this->base_url,
		"Cetak Anamnesis" => "#",
		);
         $idRekamMedis = $this->rekam_medis->getIdRekamMedisByDetail($idDetailRekamMedis);
		$resDetailInfo = $this->rekam_medis->getDetailRawatInap($idRekamMedis['txtIdRekamMedis'] , $idDetailRekamMedis);
        $detailData = $this->pemeriksaan_model->getDetail($idDetailRekamMedis);
        $detailData_ = $this->assplann_model->getDetail($idDetailRekamMedis);
        $dtTanggalKunjungan = explode(" ",$resDetailInfo['dtTanggalKunjungan']);
        $tanggalKunjungan = !empty($resDetailInfo['dtTanggalKunjungan'])?indonesian_date($dtTanggalKunjungan[0]) . " ".$dtTanggalKunjungan[1]:"";
        $jenisKelamin = $resDetailInfo['charJkPasien']=="L" ? "Laki-Laki" : "Perempuan";
        $dt['kesadaran'] = $this->config->item('kesadaran_list');
        $dt['tanggalKunjungan'] = $tanggalKunjungan;
        $dt['jenisKelamin'] = $jenisKelamin;
        $dt['breadcrumbs'] = $this->setBreadcrumbs($breadcrumbs);
		$dt['title'] = "Rawat Inap - Cetak Pemeriksaan Fisik Dan Lanjut";
        $dt['detailPasien'] = $resDetailInfo;		
		$dt['detailData'] =  $detailData;
        $dt['detailData_'] =  $detailData_;
		return $this->load->view('rawat_inap/print_out/form_pemeriksaan', $dt, true);
		
	}
}