<?php
class Pengobatan extends MY_Controller {
    var $base_url = "";
	var $limit = "20";
    var $menu = "K01";
    var $date_now = "";
    var $arrWaktuObat = array("siang" => "Siang" , "pagi"=>"Pagi" , "malam" => "Malam");
    
	function __construct(){
        parent::__construct();
		$this->load->model("rekam_medis_model" , "rekam_medis");
		$this->load->model("pengobatan_model");
        $this->load->model("register_model");
        $this->base_url = $this->base_url_site."rawat-inap/";
        $this->date_now = date("Y-m-d H:i:s");
    }
    
    public function getForm($idDetailRekamMedis = "" , $intIdRekmedObat = ""){
		if(!$this->input->is_ajax_request()){
			echo "Die, Ilegall Access";
			die;
		}
        
        $labelSave = "Simpan";
        
        $detailObat = $this->pengobatan_model->getDetail($intIdRekmedObat);
        
        if(!empty($detailObat)){
            $labelSave = "Update";
        }

        $arrData = array(
            "txtIdRekmedDetail",
            "dtTanggal",
            "intIdObat",
            "nama_obat",
            "txtDosis",
            "txtTime",
        );        

        foreach($arrData as $rowData){
            $$rowData = isset($detailObat[$rowData]) ? $detailObat[$rowData] : ""; 
        }

        $arrWaktuObat = array("siang" => "Siang" , "pagi"=>"Pagi" , "malam" => "Malam");
        $arrObat = !empty($nama_obat) ? array($intIdObat => $nama_obat) : array();
        $dtTanggal = !empty($dtTanggal) ? $dtTanggal : date("Y-m-d H:i:s");
        $dt['intIdRekmedObat'] = $this->form_builder->inputHidden("intIdRekmedObat" , $intIdRekmedObat); 
        $dt['txtIdRekmedDetail'] = $this->form_builder->inputHidden("txtIdRekmedDetail" , $idDetailRekamMedis);
        $dt['dtTanggal'] = $this->form_builder->inputText("Tanggal Obat" , "dtTanggal" , $dtTanggal);
        $dt['intIdObat'] = $this->form_builder->inputDropdown("Nama Obat" , "intIdObat" , $intIdObat , $arrObat , array("class"=>"form-control select-obat" , "style"=>"width:100%;"));
        $dt['txtDosis'] = $this->form_builder->inputText("Dosis" , "txtDosis" , $txtDosis);
        $dt['txtTime'] = $this->form_builder->inputRadioGroup("Waktu Pemberian Obat" , "txtTime" , $txtTime , $this->arrWaktuObat);

		$dt['buttonLabel'] = $labelSave;
		$retVal = $this->load->view("rawat_inap/form_pengobatan" , $dt , true);
		echo $retVal;
	}

    public function getFormAntrianApotik($idRekamMedis,$idDetailRekamMedis = "" , $txtIdKunjungan = ""){
        if(!$this->input->is_ajax_request()){
			echo "Die, Ilegall Access";
			die;
		}

        $labelSave = "Simpan";
        $detailAntrianApotik = array();

        $arrData = array(            
            "txtPengobatan",
            "bitIsApotik",
            "bitJenisApotik",
        );   

        foreach($arrData as $rowData){
            $$rowData = isset($detailAntrianApotik[$rowData]) ? $detailAntrianApotik[$rowData] : ""; 
        }

        $arrApotik = array("1"=>"Apotik Rawat Jalan" , "2"=>"Apotik Rawat Inap/UGD");
        $dt['txtIdKunjungan'] = $this->form_builder->inputHidden("txtIdKunjungan" , $txtIdKunjungan);
        $dt['txtIdKunjunganReff'] = $this->form_builder->inputHidden("txtIdKunjunganReff" , "");
        $dt['txtIdRekmed'] = $this->form_builder->inputHidden("txtIdRekmed" , $idRekamMedis);
        $dt['txtIdRekmedDetail'] = $this->form_builder->inputHidden("txtIdRekmedDetail" , $idDetailRekamMedis);
        $dt['txtPengobatan'] = $this->form_builder->inputTextArea("Resep / Obat" , "txtPengobatan" , $txtPengobatan);
        $dt['bitJenisApotik'] = $this->form_builder->inputDropdown("Apotik" , "selectApotik" , "", $arrApotik );
        
        $dt['buttonLabel'] = $labelSave;
		$retVal = $this->load->view("rawat_inap/form_antrian_apotik" , $dt , true);
		echo $retVal;
    }
    
    public function saveData(){
		if(!$this->input->is_ajax_request()){
			echo "Die, Ilegall Access";
			die;
		}
        $intIdRekmedObat = $this->input->post("intIdRekmedObat");
        $txtIdRekmedDetail = $this->input->post("txtIdRekmedDetail");

        $arrPost = array(
            "txtIdRekmedDetail" => $txtIdRekmedDetail,
            "dtTanggal" => $this->input->post("dtTanggal"),
            "intIdObat" => $this->input->post("intIdObat"),
            "txtDosis" => $this->input->post("txtDosis"),
            "txtTime" => $this->input->post("txtTime"),
        );
        
        $resVal = $this->pengobatan_model->saveUpdate($arrPost , $intIdRekmedObat);
        $retVal = array();
        $retVal['status'] = $resVal['status'];
        $retVal['message'] = $resVal['message'];
        echo json_encode($retVal);
    }

    public function saveDataAntrianApotik(){
        if(!$this->input->is_ajax_request()){
			echo "Die, Ilegall Access";
			die;
		}
        $date_now = date("Y-m-d H:i:s");
        $txtIdRekmedDetail = $this->input->post("txtIdRekmedDetail");
        $txtIdRekmed = $this->input->post("txtIdRekmed");
        $txtIdKunjungan = $this->input->post("txtIdKunjungan");
        $detail_register = $this->register_model->getDetailRegister($txtIdKunjungan);

        //// Update Pengobatan Resep On Rekam medis detail
        $intIdPelayanan = $detail_register['intIdPelayanan'];
        /// Update Status Antrian Dan Lokasi
        $idKunjunganBaru = $this->generateIdKunjungan();
        $arrPostRegister = array(
            "txtIdKunjungan" => $idKunjunganBaru,
            "txtIdPasien" => $detail_register['txtIdPasien'],
            "txtUsiaPasienKunjungan" => $detail_register['txtUsiaPasienKunjungan'],
            "bitIsApotik" => 1,
            "dtTanggalKunjungan" => $date_now,
            'txtRefIdKunjungan' => $txtIdKunjungan,
            "dtCreatedKunjungan" => $date_now,
            "dtLastUpdateKunjungan" => $date_now,
            "intIdPelayanan" => $intIdPelayanan,
            "bitJenisApotik" => $this->input->post("selectApotik"),
            "bitStatusKunjungan" => 2,
        ); 
        $resSave = $this->register_model->saveData($arrPostRegister);

        $arrPostDetail = array(
            "txtIdRekamMedis" => $txtIdRekmed,
            "txtIdKunjungan" => $idKunjunganBaru,
            "intIdPelayanan" => $intIdPelayanan,
            "txtPengobatan" => $this->input->post("txtPengobatan"),
            "bitStatusRekmedDetail" => 2, //// Rekam Medis Rawat Inap
        );
		$retValDetail = $this->rekam_medis->insertDetailRekamMedis($arrPostDetail, $txtIdRekmedDetail);
        
        $retVal = array();
		$retVal['status'] = $resSave['status'];
		$retVal['message'] = $resSave['error_stat'];
		$this->setJsonOutput($retVal);	

    }

    public function getDataObat(){
        if(!$this->input->is_ajax_request()){
			echo "Die, Ilegall Access";
			die;
		}

        $txtIdRekmedDetail = $this->input->post("txtIdRekmedDetail");
        $dataObat = $this->pengobatan_model->getData($txtIdRekmedDetail);
        
        $retVal['data'] = array();
        foreach($dataObat as $rowObat) {
            $arrWaktuObat = $this->arrWaktuObat;
            $id = $rowObat['intIdRekmedObat'];
            $btnEdit = '<button onclick="getFormDataObat('.$txtIdRekmedDetail.','.$id.')" class="btn btn-flat btn-primary">Detail</button>';
            $btnHapus = '<button onclick="hapusDataObat('.$id.')" class="btn btn-flat btn-warning">Hapus</button>';
            $btnAksi = $btnEdit.$btnHapus;
            $arrData = array(
                $rowObat['dtTanggal'],
                $rowObat['nama_obat'],
                $rowObat['txtDosis'],
                $arrWaktuObat[$rowObat['txtTime']],
                $btnAksi
            );
            $retVal['data'][] = $arrData;
        }
        $this->setJsonOutput($retVal);
    }

    public function deleteData(){
        if(!$this->input->is_ajax_request()){
			echo "Die, Ilegall Access";
			die;
		}
        $id = $this->input->post("intIdRekmedObat");
        $resHapus = $this->pengobatan_model->delete($id);
        echo json_encode($resHapus);
    }
}