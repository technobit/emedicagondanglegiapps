<?php
class Asuhan extends MY_Controller {
    var $base_url = "";
	var $limit = "20";
    var $menu = "K01";
    var $date_now = "";
    
	function __construct(){
        parent::__construct();
		$this->load->model("rekam_medis_model" , "rekam_medis");
		$this->load->model("rekam_medis_asuhan");
        $this->base_url = $this->base_url_site."rekam-medis/";
        $this->date_now = date("Y-m-d H:i:s");
    }
    
    public function getForm($idDetailRekamMedis = "" , $intIdKeperawatan = ""){
		if(!$this->input->is_ajax_request()){
			echo "Die, Ilegall Access";
			die;
		}
        $labelSave = "Save";
        $detailAsuhan = $this->rekam_medis_asuhan->getDetail($intIdKeperawatan);
        if(!empty($detailAsuhan)){
            $labelSave = "Update";
        }

        $arrData = array(
            "txtIdRekmedDetail",
            "dtTanggalPemeriksaan",
            "txtDiagnosaKeperawatan",
            "txtIntervensi",
            "txtImplementasi",
        );        

        foreach($arrData as $rowData){
            $$rowData = isset($detailAsuhan[$rowData]) ? $detailAsuhan[$rowData] : ""; 
        }

        $dtTanggalPemeriksaan = !empty($dtTanggalPemeriksaan) ? $dtTanggalPemeriksaan : date("Y-m-d H:i:s");
        $dt['intIdKeperawatan'] = $this->form_builder->inputHidden("intIdKeperawatan" , $intIdKeperawatan); 
        $dt['txtIdRekmedDetail'] = $this->form_builder->inputHidden("txtIdRekmedDetail" , $idDetailRekamMedis);
        $dt['dtTanggalPemeriksaan'] = $this->form_builder->inputText("Tanggal" , "dtTanggalPemeriksaan" , $dtTanggalPemeriksaan);
        $dt['txtDiagnosaKeperawatan'] = $this->form_builder->inputTextArea("Diagnosa Keperawatan" , "txtDiagnosaKeperawatan" , $txtDiagnosaKeperawatan);
        $dt['txtIntervensi'] = $this->form_builder->inputTextArea("Intervensi" , "txtIntervensi" , $txtIntervensi);
        $dt['txtImplementasi'] = $this->form_builder->inputTextArea("Implementasi" , "txtImplementasi" , $txtImplementasi);

		$dt['buttonLabel'] = $labelSave;
		$retVal = $this->load->view("rawat_inap/form_asuhan" , $dt , true);
		echo $retVal;
	}
    
    public function saveData(){
		if(!$this->input->is_ajax_request()){
			echo "Die, Ilegall Access";
			die;
		}
        
        $intIdKeperawatan = $this->input->post("intIdKeperawatan");
        $txtIdRekmedDetail = $this->input->post("txtIdRekmedDetail");

        $arrPost = array(
            "txtIdRekmedDetail" => $txtIdRekmedDetail,
            "dtTanggalPemeriksaan" => $this->input->post("dtTanggalPemeriksaan"),
            "txtDiagnosaKeperawatan" => $this->input->post("txtDiagnosaKeperawatan"),
            "txtIntervensi" => $this->input->post("txtIntervensi"),
            "txtImplementasi" => $this->input->post("txtImplementasi"),
            "intIdPegawai" => $this->session->userdata("sipt_id_pegawai"),
        );
        
        $resVal = $this->rekam_medis_asuhan->saveUpdate($arrPost , $intIdKeperawatan);
        $retVal = array();
        $retVal['status'] = $resVal['status'];
        $retVal['message'] = $resVal['message'];
        echo json_encode($retVal);
    }

    public function getData(){
        if(!$this->input->is_ajax_request()){
			echo "Die, Ilegall Access";
			die;
		}
        $txtIdRekmedDetail = $this->input->post("txtIdRekmedDetail");
        $dataAsuhan = $this->rekam_medis_asuhan->getData($txtIdRekmedDetail);
        
        $retVal['data'] = array();
        foreach($dataAsuhan as $rowAsuhan) {
            $intIdKeperawatan = $rowAsuhan['intIdKeperawatan'];
            $btnDetail = '<button onclick="getFormAsuhan('.$txtIdRekmedDetail.','.$intIdKeperawatan.')" class="btn btn-flat btn-primary">Detail</button>';
            $btnHapus = '<button onclick="hapusAsuhan('.$txtIdRekmedDetail.','.$intIdKeperawatan.')" class="btn btn-flat btn-warning">Hapus</button>';
            $btnAksi = $btnDetail.$btnHapus;
            $arrData = array(
                indonesian_date($rowAsuhan['dtTanggalPemeriksaan']),
                $rowAsuhan['txtDiagnosaKeperawatan'],
                $rowAsuhan['txtIntervensi'],
                $rowAsuhan['txtImplementasi'],
                $btnAksi
            );
            $retVal['data'][] = $arrData;
        }
        $this->setJsonOutput($retVal);
    }

    public function deleteData(){
        if(!$this->input->is_ajax_request()){
			echo "Die, Ilegall Access";
			die;
		}
        $txtIdRekmedDetail = $this->input->post("txtIdRekmedDetail");
        $intIdAsuhan = $this->input->post("intIdAsuhan");
        $resHapus = $this->rekam_medis_asuhan->delete($intIdAsuhan);
        echo json_encode($resHapus);
    }
}