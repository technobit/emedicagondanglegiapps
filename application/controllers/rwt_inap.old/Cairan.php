<?php
class Cairan extends MY_Controller {

    var $base_url = "";
	var $limit = "20";
    var $menu = "K01";
    var $date_now = "";
    var $arrWaktuObat = array("siang" => "Siang" , "pagi"=>"Pagi" , "malam" => "Malam");
    
	function __construct(){
        parent::__construct();
		$this->load->model("rekam_medis_model" , "rekam_medis");
		///$this->load->model("pengobatan_model");
        $this->load->model("rekam_medis_cairan_m" , "cairan_model");
        $this->base_url = $this->base_url_site."rawat-inap/";
        $this->date_now = date("Y-m-d H:i:s");
    }
    
    public function getForm($idDetailRekamMedis = "" , $id = ""){
		if(!$this->input->is_ajax_request()){
			echo "Die, Ilegall Access";
			die;
		}
        
        $labelSave = "Simpan";
        $detailCairan = $this->cairan_model->getDetail($id);
        if(!empty($detailCairan)){
            $labelSave = "Update";
        }

        $arrData = array(
            "dtTanggal",
            "dtJam",
            "txtJenisCairan",
            "txtDosis",
            "txtJumlahUrine",
        );        

        foreach($arrData as $rowData){
            $$rowData = isset($detailCairan[$rowData]) ? $detailCairan[$rowData] : ""; 
        }

        $dtTanggal = !empty($dtTanggal) ? $dtTanggal." ".$dtJam : date("Y-m-d H:i:s");
        $dtJam = !empty($dtJam) ? $dtJam : date("H:i");
        $dt['intIdCairan'] = $this->form_builder->inputHidden("intIdCairan" , $id); 
        $dt['txtIdRekmedDetail'] = $this->form_builder->inputHidden("txtIdRekmedDetail" , $idDetailRekamMedis);
        $dt['dtTanggal'] = $this->form_builder->inputText("Tanggal" , "dtTanggal" , $dtTanggal);
        $dt['txtJenisCairan'] = $this->form_builder->inputText("Jenis Cairan" , "txtJenisCairan" , $txtJenisCairan);
        $dt['txtDosis'] = $this->form_builder->inputText("Dosis Cairan" , "txtDosis" , $txtDosis);
        $dt['txtJumlahUrine'] = $this->form_builder->inputText("Jumlah Urine" , "txtJumlahUrine" , $txtJumlahUrine);
		$dt['buttonLabel'] = $labelSave;
        
		$retVal = $this->load->view("rawat_inap/form_cairan" , $dt , true);
		echo $retVal;
	}
    
    public function saveData(){
		if(!$this->input->is_ajax_request()){
			echo "Die, Ilegall Access";
			die;
		}

        $intIdRekmedCairan = $this->input->post("intIdCairan");
        $txtIdRekmedDetail = $this->input->post("txtIdRekmedDetail");
        
        $dtTanggalJam = explode(" " , $this->input->post("dtTanggal"));
        $dtTanggal = $dtTanggalJam[0];
        $dtJam = $dtTanggalJam[1];

        $arrPost = array(
            "txtIdRekmedDetail" => $txtIdRekmedDetail,
            "dtTanggal" => $dtTanggal,
            "dtJam" => $dtJam,
            "txtJenisCairan" => $this->input->post("txtJenisCairan"), 
            "txtDosis" => $this->input->post("txtDosis"),
            "txtJumlahUrine" => $this->input->post("txtJumlahUrine"),
            "intIdPegawai" => $this->session->userdata("sipt_id_pegawai"),
        );
        
        $resVal = $this->cairan_model->saveUpdate($arrPost , $intIdRekmedCairan);
        $retVal = array();
        $retVal['status'] = $resVal['status'];
        $retVal['message'] = $resVal['message'];
        echo json_encode($retVal);
    }

    public function getDataCairan(){
        if(!$this->input->is_ajax_request()){
			echo "Die, Ilegall Access";
			die;
		}

        $txtIdRekmedDetail = $this->input->post("txtIdRekmedDetail");
        $dataObat = $this->cairan_model->getData($txtIdRekmedDetail);
        
        $retVal['data'] = array();
        foreach($dataObat as $rowObat) {
            $arrWaktuObat = $this->arrWaktuObat;
            $id = $rowObat['intIdCairan'];
            $btnEdit = '<button onclick="getFormDataCairan('.$txtIdRekmedDetail.','.$id.')" class="btn btn-flat btn-primary">Detail</button>';
            $btnHapus = '<button onclick="hapusDataCairan('.$id.')" class="btn btn-flat btn-warning">Hapus</button>';
            $btnAksi = $btnEdit.$btnHapus;
            $arrData = array(
                $rowObat['dtTanggal'],
                $rowObat['dtJam'],
                $rowObat['txtJenisCairan'],
                $rowObat['txtDosis'],
                $rowObat['txtJumlahUrine'],
                $btnAksi
            );
            $retVal['data'][] = $arrData;
        }
        $this->setJsonOutput($retVal);
    }

    public function deleteData(){
        if(!$this->input->is_ajax_request()){
			echo "Die, Ilegall Access";
			die;
		}
        $id = $this->input->post("intId");
        $resHapus = $this->cairan_model->delete($id);
        echo json_encode($resHapus);
    }
}