<?php
class Assplanning extends MY_Controller {
    var $base_url = "";
	var $limit = "20";
    var $menu = "K01";
    var $date_now = "";
    
	function __construct(){
        parent::__construct();
		$this->load->model("rekam_medis_model" , "rekam_medis");
		$this->load->model("assplann_model");
        $this->base_url = $this->base_url_site."rekam-medis/";
        $this->date_now = date("Y-m-d H:i:s");
    }
    
    public function getForm($idDetailRekamMedis = ""){
		if(!$this->input->is_ajax_request()){
			echo "Die, Ilegall Access";
			die;
		}

        $labelSave = "Simpan";
        $detailData = $this->assplann_model->getDetail($idDetailRekamMedis);
        $link_cetak = $this->base_url_site."rawat-inap/cetak-assesment/".$idDetailRekamMedis;
        if(!empty($detailData)){
            $labelSave = "Update";
            $link_cetak = $this->base_url_site."rawat-inap/cetak-assesment/".$idDetailRekamMedis;
        }
        ///echopre($detailPemeriksaan);

        $arrInput = array(
            "txtAssesment",
            "txtDiagnosisLab",
            "txtDiagnosisRadio",
            "txtDiagnosisLainLain",
            "txtDiagnosis",
            "txtTerapi",
            "txtMonitoring",
            "txtEdukasi",
        );        

        foreach($arrInput as $rowData){
            $$rowData = isset($detailData[$rowData]) ? $detailData[$rowData] : ""; 
        }

        $dt['txtIdRekmedDetail'] = $this->form_builder->inputHidden("txtIdRekmedDetail" , $idDetailRekamMedis);
        $dt['txtAssesment'] = $this->form_builder->inputTextArea("Assesment" , "txtAssesment" , $txtAssesment);

		$dt['txtDiagnosisLab'] = form_input("txtDiagnosisLab" , $txtDiagnosisLab , 'class="form-control" id="txtDiagnosisLab"');
        $dt['txtDiagnosisRadio'] = form_input("txtDiagnosisRadio" , $txtDiagnosisRadio , 'class="form-control" id="txtDiagnosisRadio"');
        $dt['txtDiagnosisLainLain'] = form_input("txtDiagnosisLainLain" , $txtDiagnosisLainLain , 'class="form-control" id="txtDiagnosisLainLain"');

        $dt['txtDiagnosis'] = $this->form_builder->inputTextArea("Diagnosis" , "txtDiagnosis" , $txtDiagnosis);

        $dt['txtTerapi'] = $this->form_builder->inputTextArea("Terapi" , "txtTerapi" , $txtTerapi);
        $dt['txtMonitoring'] = $this->form_builder->inputTextArea("Monitoring" , "txtMonitoring" , $txtMonitoring);
        $dt['txtEdukasi'] = $this->form_builder->inputTextArea("Edukasi" , "txtEdukasi" , $txtEdukasi);
		$dt['link_cetak'] = $link_cetak;
		$dt['buttonLabel'] = $labelSave;
		$retVal = $this->load->view("rawat_inap/form_assesment" , $dt , true);
		echo $retVal;
	}
    
    public function saveData(){
		if(!$this->input->is_ajax_request()){
			echo "Die, Ilegall Access";
			die;
		}
        $txtIdRekmedDetail = $this->input->post("txtIdRekmedDetail");
        /*
        $arrPost = array(
            "txtIdRekmedDetail" => $this->input->post("txtIdRekmedDetail"),
            "txtAssesment" => $this->input->post("txtAssesment"),
            "txtDiagnosisLab"=> $this->input->post("txtDiagnosisLab"),
            "txtDiagnosisRadio"=> $this->input->post("txtDiagnosisRadio"),
            "txtDiagnosisLainLain"=> $this->input->post("txtDiagnosisLainLain"),
            "txtDiagnosis"=> $this->input->post("txtDiagnosis"),
            "txtTerapi"=> $this->input->post("txtTerapi"),
            "txtMonitoring"=> $this->input->post("txtMonitoring"),
            "txtEdukasi"=> $this->input->post("txtEdukasi"),
        );
        */

        $arrPost = array(
            "txtIdRekmedDetail" => $this->input->post("txtIdRekmedDetail"),
            "txtDiagnosis"=> $this->input->post("txtDiagnosis"),
            "txtTerapi"=> $this->input->post("txtTerapi"),
        );
        
        $resVal = $this->assplann_model->saveUpdate($arrPost , $txtIdRekmedDetail);
        $retVal = array();
        $retVal['status'] = $resVal['status'];
        $retVal['message'] = $resVal['message'];
        $retVal['id'] = $resVal['id'];
        echo json_encode($retVal);
    }
    public function cetak_form($idDetailRekamMedis) {
		
		
		$dt = array(
		"title" => "Rawat Inap - Cetak Diagnonis Dan Terapi",
		"description" => "Rawat Inap - Cetak Diagnonis Dan Terapi",
		"container" => $this->_build_cetak_form($idDetailRekamMedis),
		"menu_key" => "cetak_assesment_planning",
		"akses_key" => "is_view",
		"custom_js" => array(),
		"custom_css" => array(),
		);
		
		$this->_render("default",$dt);
		
	}
	
	
	private function _build_cetak_form($idDetailRekamMedis){
		
		
		$breadcrumbs = array(
		"Home" =>$this->base_url_site,
		"Rawat Inap" => $this->base_url,
		"Cetak Diagnonis Dan Terapi" => "#",
		);
        $idRekamMedis = $this->rekam_medis->getIdRekamMedisByDetail($idDetailRekamMedis);
        $resDetailInfo = $this->rekam_medis->getDetailRawatInap($idRekamMedis['txtIdRekamMedis'] , $idDetailRekamMedis);
        $detailData = $this->assplann_model->getDetail($idDetailRekamMedis);
       	$dtTanggalKunjungan = explode(" ",$resDetailInfo['dtTanggalKunjungan']);
        $tanggalKunjungan = !empty($resDetailInfo['dtTanggalKunjungan'])?indonesian_date($dtTanggalKunjungan[0]) . " ".$dtTanggalKunjungan[1] : "";
        $jenisKelamin = $resDetailInfo['charJkPasien']=="L" ? "Laki-Laki" : "Perempuan";
        $dt['tanggalKunjungan'] = $tanggalKunjungan;
        $dt['jenisKelamin'] = $jenisKelamin;
        $dt['breadcrumbs'] = $this->setBreadcrumbs($breadcrumbs);
		$dt['title'] = "Rawat Inap - Cetak Diagnonis Dan Terapi";
        $dt['detailPasien'] = $resDetailInfo;		
		$dt['detailData'] =  $detailData;
		return $this->load->view('rawat_inap/print_out/form_assesment', $dt, true);
		
	}
}