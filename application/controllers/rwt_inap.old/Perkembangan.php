<?php
class Perkembangan extends MY_Controller {
    var $base_url = "";
	var $limit = "20";
    var $menu = "K01";
    var $date_now = "";
    
	function __construct(){
        parent::__construct();
		$this->load->model("rekam_medis_model" , "rekam_medis");
		$this->load->model("rekam_medis_perkembangan_m" , "rekmed_perkembangan");
        $this->base_url = $this->base_url_site."rekam-medis/";
        $this->date_now = date("Y-m-d H:i:s");
    }
    
    public function getForm($idDetailRekamMedis = "" , $intIdPerkembangan = ""){
		if(!$this->input->is_ajax_request()){
			echo "Die, Ilegall Access";
			die;
		}
        $labelSave = "Save";
        $detailPerkembangan = $this->rekmed_perkembangan->getDetail($intIdPerkembangan);
        if(!empty($detailPerkembangan)){
            $labelSave = "Update";
        }

        $arrData = array(
            "txtIdRekmedDetail",
            "dtTanggalPemeriksaan",
            "txtInstruksiDokter",
            "txtCatatanPerawat",
        );        

        foreach($arrData as $rowData){
            $$rowData = isset($detailPerkembangan[$rowData]) ? $detailPerkembangan[$rowData] : ""; 
        }

        $dtTanggalPemeriksaan = !empty($dtTanggalPemeriksaan) ? $dtTanggalPemeriksaan : date("Y-m-d H:i:s");
        $dt['intIdPerkembangan'] = $this->form_builder->inputHidden("intIdPerkembangan" , $intIdPerkembangan); 
        $dt['txtIdRekmedDetail'] = $this->form_builder->inputHidden("txtIdRekmedDetail" , $idDetailRekamMedis);
        $dt['dtTanggalPemeriksaan'] = $this->form_builder->inputText("Tanggal" , "dtTanggalPemeriksaan" , $dtTanggalPemeriksaan);
        $dt['txtInstruksiDokter'] = $this->form_builder->inputTextArea("Instruksi Dokter" , "txtInstruksiDokter" , $txtInstruksiDokter);
        $dt['txtCatatanPerawat'] = $this->form_builder->inputTextArea("Catatan Perawat" , "txtCatatanPerawat" , $txtCatatanPerawat);

		$dt['buttonLabel'] = $labelSave;
		$retVal = $this->load->view("rawat_inap/form_perkembangan" , $dt , true);
		echo $retVal;
	}
    
    public function saveData(){
		if(!$this->input->is_ajax_request()){
			echo "Die, Ilegall Access";
			die;
		}
        ////echopre($_POST);die;
        $intIdPerkembangan = $this->input->post("intIdPerkembangan");
        $txtIdRekmedDetail = $this->input->post("txtIdRekmedDetail");

        $arrPost = array(
            "txtIdRekmedDetail" => $txtIdRekmedDetail,
            "dtTanggalPemeriksaan" => $this->input->post("dtTanggalPemeriksaan"),
            "txtInstruksiDokter" => $this->input->post("txtInstruksiDokter"),
            "txtCatatanPerawat" => $this->input->post("txtCatatanPerawat"),
            "intIdPegawai" => $this->session->userdata("sipt_id_pegawai"),
        );
        
        $resVal = $this->rekmed_perkembangan->saveUpdate($arrPost , $intIdPerkembangan);
        $retVal = array();
        $retVal['status'] = $resVal['status'];
        $retVal['message'] = $resVal['message'];
        echo json_encode($retVal);
    }

    public function getDataPerkembangan(){
        if(!$this->input->is_ajax_request()){
			echo "Die, Ilegall Access";
			die;
		}
        $txtIdRekmedDetail = $this->input->post("txtIdRekmedDetail");
        $dataPerkembangan = $this->rekmed_perkembangan->getData($txtIdRekmedDetail);
        
        $retVal['data'] = array();
        foreach($dataPerkembangan as $rowPerkembangan) {
            $intIdPerkembangan = $rowPerkembangan['intIdPerkembangan'];
            $btnPerkembangan = '<button onclick="getFormPerkembangan('.$txtIdRekmedDetail.','.$intIdPerkembangan.')" class="btn btn-flat btn-primary">Detail</button>';
            $btnHapus = '<button onclick="hapusPerkembangan('.$txtIdRekmedDetail.','.$intIdPerkembangan.')" class="btn btn-flat btn-warning">Hapus</button>';
            $btnAksi = $btnPerkembangan.$btnHapus;
            $arrData = array(
                $rowPerkembangan['dtTanggalPemeriksaan'],
                $rowPerkembangan['txtInstruksiDokter'],
                $rowPerkembangan['txtCatatanPerawat'],
                $btnAksi
            );
            $retVal['data'][] = $arrData;
        }
        $this->setJsonOutput($retVal);
    }

    public function deleteData(){
        if(!$this->input->is_ajax_request()){
			echo "Die, Ilegall Access";
			die;
		}
        $txtIdRekmedDetail = $this->input->post("txtIdRekmedDetail");
        $intIdPerkembangan = $this->input->post("intIdPerkembangan");
        $resHapus = $this->rekmed_perkembangan->delete($intIdPerkembangan);
        echo json_encode($resHapus);
    }
}