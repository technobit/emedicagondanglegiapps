<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Obat_apotik extends MY_Controller {

    var $meta_title;
    var $meta_desc;
    var $main_title;
    var $dtBreadcrumbs = array();
    var $menu_key = "rawat_jalan_data_obat";  

    public function __construct() {
        parent::__construct();
        $this->meta_title = "Obat Apotik " . ucwords(str_replace("_", " ", $this->uri->segment(2)));
        $this->meta_desc = "Obat Apotik " . ucwords(str_replace("_", " ", $this->uri->segment(2)));
        $this->main_title = "Obat Apotik " . ucwords(str_replace("_", " ", $this->uri->segment(2)));
        $this->dtBreadcrumbs = array(
            "Home" =>base_url(),
            "Apotik " . ucwords(str_replace("_", " ", $this->uri->segment(2))) => base_url('apotik/' . $this->uri->segment(2)),
            "Data Stok Obat" => base_url('apotik/ '.$this->uri->segment(2).'/obat'),
        );
        $this->load->model('obat_m');
    }
    public function index($id) {     
        $this->menu_key = $id==1 ? "rawat_jalan_data_obat" : "ugd_data_obat";   
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "container" => $this->_index($id),
            "menu_key" => $this->menu_key,
			"akses_key" => "is_view",
            "custom_js" => array(
                ASSETS_JS_URL."apotik/obat/index.js"
            ),
            "custom_css" => array(),
        );  
        $this->_render("default",$dt);             
    }

    
    private function _index($id) {        
        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['title'] = $this->meta_title;        
        return $this->load->view('obat_apotik/index', $dt, true);
    }

    public function get($id) {
        $this->load->library('datatables');
        $this->db->like('nama_obat', $this->input->get('nama'));
        if ($this->input->get('jenis_obat')) {
            $this->db->where('id_jenis_obat', $this->input->get('jenis_obat'));
        }
        $query = $this->db->select('obat.*, kemasan.kemasan, jenis_obat, obat_apotik.stok as  stok_apotik')
        ->join('kemasan', 'kemasan.id = obat.id_kemasan')
        ->join('jenis_obat', 'jenis_obat.id = obat.id_jenis_obat', 'left')
        ->join('(select * from obat_apotik where id_apotik="'.$id.'") obat_apotik', 'obat.id = obat_apotik.id_obat', 'left')        
        ->get_compiled_select('obat');
        $response = $this->datatables->collection($query)
        ->addColumn('action', function($row) {
                return '<a href="'.base_url('apotik/'.$this->uri->segment(2).'/obat/opname/' . $row->id).'" class="btn btn-warning btn-sm"><i class="fa fa-pencil"></i> Penyesuaian</a>';
        })
        ->orderableColumns('nama_obat, kode_obat, kemasan, jenis_obat, stok')
        ->searchableColumns('nama_obat, kode_obat, kemasan, jenis_obat, stok')    
        ->render();
        $this->output->set_content_type('application/json')->set_output(json_encode($response));
    }


    public function opname($id_apotik, $id) {        
        $this->menu_key = $id==1 ? "rawat_jalan_data_obat" : "ugd_data_obat";
        if ($post = $this->input->post()) {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('stok_akhir', 'Stok Akhir', 'required|numeric');
            if (!$this->form_validation->run()) {
                $this->redirect->withValidation()->withInput()->back();
            }
            $this->obat_m->opname($id_apotik, $id, $post);
            $this->redirect->with('successMessage', 'Stok berhasil disesuaikan')->to('apotik/' . $this->uri->segment(2) . '/obat');
        }
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "container" => $this->_opname($id_apotik, $id),
            "menu_key" => $this->menu_key,
			"akses_key" => "is_insert",
            "custom_js" => array(),
            "custom_css" => array(),
        );  
        $this->_render("default",$dt);  
    }

    private function _opname($id_apotik, $id) {        
        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['title'] = $this->meta_title;        
        $obat = $this->find($id_apotik, $id);
        $this->form->setData($obat);
        return $this->load->view('obat_apotik/opname', $dt, true);
    }

    private function find($id_apotik, $id) {
        $result = $this->obat_m->findObatApotik($id_apotik, $id);
        if (!$result) {
            $this->redirect->with('errorMessage', 'Data obat tidak ditemukan.')->back();
        } else {
            return $result;
        }
    }

}