<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Kemasan extends MY_Controller {

    var $meta_title = "Kemasan";
    var $meta_desc = "Kemasan";
    var $main_title = "Kemasan";
    var $dtBreadcrumbs = array();  

    public function __construct() {
        parent::__construct();
        $this->dtBreadcrumbs = array(
            "Home" =>base_url(),
            "Gudang Apotik" => base_url('gudang_apotik'),
            "Data Kemasan" => base_url('gudang_apotik/kemasan'),
        );
        $this->load->model('kemasan_m');
    }
    public function index() {            
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "container" => $this->_index(),
            "menu_key" => "gudang_apotik_data_kemasan",
			"akses_key" => "is_view",
            "custom_js" => array(),
            "custom_css" => array()            
        );  
        $this->_render("default",$dt);  
    }

    private function _index() {        
        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['title'] = $this->meta_title;
        $dt['rsKemasan'] = $this->kemasan_m->get();   
        return $this->load->view('kemasan/index', $dt, true);
    }

    public function create() {
        if ($post = $this->input->post()) {
            $this->validation();
            $this->kemasan_m->insert($post);
            $this->redirect->with('successMessage', 'Data kemasan berhasil ditambahkan.')->to('data-kemasan');
        }
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "menu_key" => "gudang_apotik_data_kemasan",
			"akses_key" => "is_insert",
            "container" => $this->_form(),
            "custom_js" => array(),
            "custom_css" => array()            
        );  
        $this->_render("default",$dt);  
    }

    public function edit($id) {
        $result = $this->find($id);
        if ($post = $this->input->post()) {
            $this->validation();
            $this->kemasan_m->update($id, $post);
            $this->redirect->with('successMessage', 'Data kemasan berhasil diperbarui.')->to('gudang_apotik/kemasan');
        }
        $this->form->setData($result);
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "menu_key" => "gudang_apotik_data_kemasan",
			"akses_key" => "is_update",
            "container" => $this->_form(),
            "custom_js" => array(),
            "custom_css" => array()            
        );  
        $this->_render("default",$dt);  
    }

    private function _form() {
        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['title'] = $this->meta_title;        
        return $this->load->view('kemasan/form', $dt, true);
    }

    public function delete($id) {
        $result = $this->find($id);
        $this->kemasan_m->delete($id);
        $this->redirect->with('successMessage', 'Data kemasan berhasil dihapus.')->to('gudang_apotik/kemasan');
    }

    private function find($id) {
        $result = $this->kemasan_m->find($id);
        if (!$result) {
            $this->redirect->with('errorMessage', 'Data keamsan tidak ditemukan.')->back();
        } else {
            return $result;
        }
    }

    private function validation() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('kemasan', 'Kemasan', 'required');
        if (!$this->form_validation->run()) {
            $this->redirect->withInput()->withValidation()->back();
        }        
    }

}