<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Self_service extends MY_Controller {
    
    var $meta_title = "Self Service Application";
    var $meta_desc = "Self Service Application";
	var $main_title = "Self Service Application";
    var $base_url = "";
	var $base_url_pasien = "";
	var $upload_dir = "";
	var $upload_url = "";
	var $limit = "20";
	var $arr_level_user = "";
    var $menu = "K01";
	
    public function __construct(){
        parent::__construct();
        $this->base_url = $this->base_url_site."self-service/";
		$this->base_url_pasien = $this->base_url_site."pasien/";
		$this->load->model("pasien_model");
		$this->load->model("pelayanan_model");
		$this->load->model("register_model");
		$this->load->model("register_history_model");
		$this->load->model("register_rawat_inap_m");
		$this->load->model("rekam_medis_model" , "rekam_medis");
		$this->load->model("jaminan_kesehatan_model" ,"jaminan_kesehatan");
		$this->load->library(
			array(
				"bpjs/pesertabpjs",
				"bpjs/pendaftaranbpjs",
			)
		);
    }
	
    public function index(){
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "template" =>"template/default_self",
			"menu_key" => "loket_daftar",
			"akses_key" => "is_view",
			"container" => $this->_build_pencarian(),
			"custom_js" => array(
				ASSETS_JS_URL."self_service/pencarian.js",
                ASSETS_URL."plugins/moment/moment.min.js",
				ASSETS_URL."plugins/flexcode/js/jquery.timer.js",
			),
            "custom_css" => array(
				
			),
		);	
		$this->_render_antrian("template/default_self",$dt);
    }

    private function _build_pencarian(){
        $dt = array();
        $ret = $this->load->view("self_service/pencarian" , $dt , true);
        return $ret;
    }

    public function pendaftaran($txtIdPasien){
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "template" =>"template/default_self",
			"menu_key" => "loket_daftar",
			"akses_key" => "is_view",
			"container" => $this->__build_pendaftaran($txtIdPasien),
			"custom_js" => array(
				ASSETS_JS_URL."self_service/pendaftaran.js",
                ASSETS_URL."plugins/moment/moment.min.js",
				ASSETS_URL."plugins/flexcode/js/jquery.timer.js",
			),
            "custom_css" => array(
				
			),
		);	
		$this->_render_antrian("template/default_self",$dt);
	}
	
	public function pendaftaran_loket(){

        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "template" =>"template/default_self",
			"menu_key" => "loket_daftar",
			"akses_key" => "is_view",
			"container" => $this->__build_pendaftaran_loket(),
			"custom_js" => array(
				ASSETS_JS_URL."self_service/pendaftaran_loket.js",
                ASSETS_URL."plugins/moment/moment.min.js",
			),
            "custom_css" => array(
				
			),
		);	

		$this->_render_antrian("template/default_self",$dt);
	}
	
	private function __build_pendaftaran_loket(){

		$this->load->model('pelayanan_model');
		//// Get Jenis Pelayanan By ID Jenis
		$list_pelayanan = $this->pelayanan_model->getListPelayananByIdJenis(15);
		$dt = array();
		$arrDataLoket = array();
		$dt['back_link'] = $this->base_url;
		$dt['list_pelayanan'] = $list_pelayanan;
		$ret = $this->load->view("self_service/pendaftaran_loket" , $dt , true);
		return $ret;
    }

    private function __build_pendaftaran($txtIdPasien){
		$dataPasien = $this->pasien_model->getDetail($txtIdPasien);
		$dt = array();
		$link_poli = $this->base_url_site . "self-service/registrasi/" . $txtIdPasien;
		$arrDataPoli = array(
			"poli_umum" => array(
				"link" => $link_poli . "/1/", 
				"class" => "btn-primary",
				"intIDPoli" => "1",
				"description" => "Ruangan Umum",
				"icon" => "fa fa-user-md",
				"color" => "blue",
			),
			"poli_gigi" => array(
				"link" => $link_poli . "/3/", 
				"intIDPoli" => "3",
				"class" => "btn-danger",
				"description" => "Ruangan Gigi",
				"icon" => "fa fa-smile-o",
				"color" => "yellow",
			),
			"poli_ibu" => array(
				"link" => $link_poli . "/5/", 
				"intIDPoli" => "5",
				"class" => "btn-success",
				"description" => "Ruangan Ibu",
				"icon" => "fa fa-venus",
				"color" => "aqua",
			),
			"poli_anak" => array(
				"link" => $link_poli . "/4/", 
				"intIDPoli" => "4",
				"class" => "btn-warning",
				"description" => "Ruangan Anak",
				"icon" => "fa fa-child",
				"color" => "green",
			),
			"poli_lansia" => array(
				"link" => $link_poli . "/42/", 
				"intIDPoli" => "42",
				"class" => "btn-warning",
				"description" => "Ruangan Lansia",
				"icon" => "fa fa-blind",
				"color" => "teal",
			),
			"poli_gizi" => array(
				"link" => $link_poli . "/9/", 
				"intIDPoli" => "9",
				"class" => "btn-warning",
				"description" => "Ruangan Gizi",
				"icon" => "fa fa-heart-o",
				"color" => "purple",
			),
			"ugd" => array(
				"link" => $this->base_url_site . "/6/", 
				"intIDPoli" => "6",
				"class" => "btn-warning",
				"description" => "UGD",
				"icon" => "fa fa-warning",
				"color" => "red",
			),
		);
		$dt['data_pasien'] = $dataPasien;
		$dt['arr_poli'] = $arrDataPoli;
		$dt['back_link'] = $this->base_url;
		$ret = $this->load->view("self_service/pendaftaran" , $dt , true);
		return $ret;
    }

	public function registrasi() { 

		if(!$this->input->is_ajax_request()){
			echo "Ilegal";die;
		}
		
		$id_kunjungan = "";
		$status = false;
		$message = "Data Kunjungan Gagal";
		$idPasien = $this->input->post("txtIdPasien");
		$idPelayanan = $this->input->post("intIDPoli");
		$dataPasien = $this->pasien_model->getDetail($idPasien);

		$intIdJaminanKesehatan = $dataPasien['intIdJaminanKesehatan'];
		$tanggalLahir = $dataPasien['dtTanggalLahir'];
		$tanggal = explode(" " , $tanggalLahir);
		$usia = personAge($tanggal[0]);	
		$usiaHari = getIntervalDays($tanggal[0]);
		$date_input = date("Y-m-d");
		$inputNoAnggota = $dataPasien['txtNoAnggota'];
		$check = $this->register_model->checkDuplicate($date_input,$idPasien,$inputNoAnggota);
		if (!empty($check)) {
		$retVal = array();
		$retVal['status'] = false;
		$retVal['message'] = "Data Duplikat Pada Antrian : ".$check["intNoAntri"];
		$retVal['id'] = "";
			
		}else{
		$arrayInput = array(
						"txtIdPasien" => $idPasien,
						"intIdPelayanan" => $idPelayanan,
						"intIdJaminanKesehatan" => $dataPasien['intIdJaminanKesehatan'],
						"txtNoJaminanKesehatan" => $dataPasien['txtNoIdJaminanKesehatan'],
						"txtUsiaPasienKunjungan" => $usia,
						"intUsiaPasienHari" => $usiaHari,
						"txtKeteranganKunjungan" => "",
					);
		
		$detailJenisPelayanan = $this->pelayanan_model->getDetail($idPelayanan);
		$idJenisPelayanan = $detailJenisPelayanan['intIdJenisPelayanan'];

		///Default Value For Rawat Jalan 
		$bitStatusKunjungan = 1;
		$bitStatusPoli = 1;
		/// For UGD 
		if($idJenisPelayanan==7){
			$bitStatusPoli = 2;
		}

		/// For Rawat Inap
		if($idJenisPelayanan==9){
			$bitStatusKunjungan = 2;
			$bitStatusPoli = 1;
		}

		/// Start No. AntrianLoket
		if($idPelayanan==1){ //Poli Umum I
			$startAntrian = 300;
		}else if ($idPelayanan==42) { //Poli Umum II
			$startAntrian = 200;
		}else if ($idPelayanan==3) { //Poli Gigi
			$startAntrian = 700;
		}else if ($idPelayanan==4) { //Poli Anak
			$startAntrian = 600;
		}else if ($idPelayanan==5) { //Poli Ibu
			$startAntrian = 500;
		}else if ($idPelayanan==7) { //Laboratorium
			$startAntrian = 100;
		}else if ($idPelayanan==9) { //Poli Gizi
			$startAntrian = 400;
		}else { //Laboratorium
			$startAntrian = 800;
		}

		/// Insert Mode
		///$date_now = date("Y-m-d H:i:s");
		$date_input = $this->input->post("inputTanggal");
		///$date_now = $date_input." ".date("H:i:s"); 
		$date_now = date("Y-m-d H:i:s");
		$date_server = date("Y-m-d");
		$jumlah_kunjungan = $this->register_model->countRegisterByPelayanan($idPelayanan);
		$isKunjunganBaru = $this->register_model->checkKunjunganByIdPasien($idPasien);
		$noAntri = $startAntrian + ($jumlah_kunjungan + 1);
		$id_kunjungan = $this->generateIdKunjungan();
		$arrayInput['txtIdKunjungan'] = $id_kunjungan;
		$arrayInput['intNoAntri'] = $noAntri;
		$arrayInput['dtTanggalKunjungan'] = $date_now;
		$arrayInput['dtCreatedKunjungan'] = $date_now;
		$arrayInput['dtLastUpdateKunjungan'] = $date_now;
		$arrayInput['bitStatusKunjungan'] = $bitStatusKunjungan;
		$arrayInput['bitIsPoli'] = $bitStatusPoli;
		$arrayInput['bitIsBaru'] = $isKunjunganBaru==true ? 1 : 0;
		$resSave = $this->register_model->saveData($arrayInput);
		
		$status = $resSave['status'];
		$message = $status==true ? "Pendaftaran Berhasil " : "Pendaftaran Gagal (Silahkan Hubungi petugas)";
		$retVal = array();

		$retVal['status'] = $status;
		$retVal['message'] = $message;
		$retVal['id_kunjungan'] = $id_kunjungan;
		}
		echo json_encode($retVal);
	}


	public function landing_page($txtIdKunjungan){
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "template" =>"template/default_self",
			"menu_key" => "loket_daftar",
			"akses_key" => "is_view",
			"container" => $this->__build_landing_page($txtIdKunjungan),
			"custom_js" => array(
				ASSETS_JS_URL."self_service/landing_page.js",
                ASSETS_URL."plugins/moment/moment.min.js",
				ASSETS_URL."plugins/flexcode/js/jquery.timer.js",
			),
            "custom_css" => array(
				
			),
		);	
		$this->_render_antrian("template/default_self",$dt);
    }

	private function __build_landing_page($txtIdKunjungan){
		
		$detail_register = $this->register_model->getDetailRegister($txtIdKunjungan);
		
		$dt = array();
		$dt['nama_poli'] = $detail_register['txtNama'];
		$dt['no_antrian'] = $detail_register['intNoAntri'];
		$dt['link_service'] = $this->base_url;	
		$ret = $this->load->view("self_service/landing_page" , $dt , true);
		return $ret;
    }


	public function landing_page_pegawai($intIdPegawai){
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "template" =>"template/default_self",
			"menu_key" => "loket_daftar",
			"akses_key" => "is_view",
			"container" => $this->__build_landing_page_pegawai($intIdPegawai),
			"custom_js" => array(
				ASSETS_JS_URL."self_service/landing_page.js",
                ASSETS_URL."plugins/moment/moment.min.js",
				ASSETS_URL."plugins/flexcode/js/jquery.timer.js",
			),
            "custom_css" => array(
				
			),
		);	
		$this->_render_antrian("template/default_self",$dt);

    }

	public function landing_page_loket($idKunjunganLoket){
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "template" =>"template/default_self",
			"menu_key" => "loket_daftar",
			"akses_key" => "is_view",
			"container" => $this->__build_landing_page_loket($idKunjunganLoket),
			"custom_js" => array(
				ASSETS_JS_URL."self_service/landing_page.js",
                ASSETS_URL."plugins/moment/moment.min.js",
				ASSETS_URL."plugins/flexcode/js/jquery.timer.js",
			),
            "custom_css" => array(
				
			),
		);	
		$this->_render_antrian("template/default_self",$dt);

	}
	
	private function __build_landing_page_loket($intIdLoket){
		
		$this->load->model('register_loket_model');
		$data = $this->register_loket_model->detail($intIdLoket);
		
		$dt = array();
		$dt['data'] = $data;
		$dt['link_service'] = $this->base_url;
		$ret = $this->load->view("self_service/landing_page_loket" , $dt , true);
		return $ret;
	}

	private function __build_landing_page_pegawai($intIdPegawai){
		
		$this->load->model("pegawai_model");
		$dataPegawai = $this->pegawai_model->getDetail($intIdPegawai);
		$dt = array();
		$dt['dataPegawai'] = $dataPegawai;
		$dt['link_service'] = $this->base_url;
		$ret = $this->load->view("self_service/landing_page_pegawai" , $dt , true);
		return $ret;
	}
	
	/// API Desktop Self Service APP
	public function cariPasienRekamMedis(){
		$retVal = array();
		$status = false;
		$txtNoAnggota = $this->input->post("txtNoAnggota");
		$jk_list = $this->config->item("jk_list");
		$dataPasien = $this->pasien_model->getDataPasienNoAnggota($txtNoAnggota);
		$resVal = array();
		if(count($dataPasien) > 0){	
			$status = true;
			foreach($dataPasien as $rowPasien){
				$jenis_kelamin = $jk_list[$rowPasien['charJkPasien']];
				$tanggal_lahir = indonesian_date($rowPasien['dtTanggalLahir']);
				$resVal[] = array(
					"txtNoAnggota"=>$rowPasien['txtNoAnggota'],
					"txtNamaPasien"=>$rowPasien['txtNamaPasien'],
					"txtTanggalLahir"=>$tanggal_lahir,
					"txtJenisKelamin"=>$jenis_kelamin,
					"txtIdPasien"=>$rowPasien['txtIdPasien'],
				);
			}
		}

		$retVal['status'] = $status;
		$retVal['data'] = $resVal;
		echo json_encode($retVal);
	}

	public function detailPasien(){
		$retVal = array(
			"status" => false,
			"message" => "Error Sistem 404",
		);
		$txtIdPasien = $this->input->post("txtIdPasien");
		$dataPasien = $this->pasien_model->getDetail($txtIdPasien);
		if(!empty($dataPasien)){
			$retVal = array(
				"status" => true,
				"message" => "Data Tersedia",
				"data" => $dataPasien,
			);
		}else{
			$retVal = array(
				"status" => false,
				"message" => "Data Tidak Ada",
			);	
		}

		echo json_encode($retVal);
	}

	public function cekBPJSPasien(){

		$kdProviderPKM = $this->config->item("kode_provider_puskesmas");
		$retVal = array(
			"status" => false,
			"message" => "Error Sistem 404",
		);
		$statusResponse = false;
		$txtIdPasien = $this->input->post("txtIdPasien");
		$dataPasien = $this->pasien_model->getDetailService($txtIdPasien);
		if($dataPasien['intIdJaminanKesehatan'] > 0){
			$txtNoBPJS = $dataPasien['txtNoIdJaminanKesehatan'];
			$retDataBPJS = $this->pesertabpjs->getPesertaV2($txtNoBPJS);
			$statusCheckBPJS = $retDataBPJS['status'];
			if(!$statusCheckBPJS){
				$message = "Pengecekan BPJS Gagal, Hubungi Administrator";
				$retVal = array(
					"status" => false,
					"message" => $message,
				);
				$this->setOutputResponse($retVal);
			}else{
				$detailBPJS = $retDataBPJS['data'];
				$kdProviderPeserta = $detailBPJS['kdProviderPst']['kdProvider'];
				$nmProviderPeserta = $detailBPJS['kdProviderPst']['nmProvider'];
	
				if($kdProviderPeserta!=$kdProviderPKM){
					$retVal = array(
						"status" => false,
						"message" => "Faskes Pertama Tidak Sesuai Dengan Faskes Yang Aktif (".$nmProviderPeserta.") Silahkan hubungi LOKET",
					);
					
					$this->setOutputResponse($retVal);
				}else if(($detailBPJS['aktif']!='1')){
					$retVal = array(
						"status" => false,
						"message" => "Status Anda ".$retDataBPJS['ketAktif'],
					);
					$this->setOutputResponse($retVal);
				}else if(($detailBPJS['tunggakan'] > 0)){ 
					$retVal = array(
						"status" => false,
						"message" => "Tunggakan Anda ".$retDataBPJS['tunggakan'],
					);
					$this->setOutputResponse($retVal);
				}else{
					$retVal = array(
						"status" => true,
						"message" => "BPJS Sesuai",
					);

					$dataPasien['jenisKelamin'] = $dataPasien['charJkPasien']=='L' ? "Laki - Laki" : "Perempuan";
					$dataPasien['txtUsiaPasien'] = personAge($dataPasien['dtTanggalLahir']);
					$retVal['detailPasien'] = $dataPasien;

					$this->setOutputResponse($retVal);
				}
			}
		}else{
			$retVal = array(
				"status" => true,
				"message" => "Pasien Umum Tanpa BPJS",
			);
		}
		$retVal['detailPasien'] = $dataPasien;
		$this->setOutputResponse($retVal);
	}

	public function getFaskesPendaftaran(){
		$arrDataPoli = array(
			"poli_umum" => array(
				"intIDPoli" => "1",
				"description" => "Ruangan Umum",
				"color" => "blue",
			),
			"poli_gigi" => array(
				"intIDPoli" => "3",
				"description" => "Ruangan Gigi",
				"color" => "yellow",
			),
			"poli_ibu" => array(
				"intIDPoli" => "5",
				"description" => "Ruangan Ibu",
				"color" => "aqua",
			),
			"poli_anak" => array(
				"intIDPoli" => "4",
				"description" => "Ruangan Anak",
				"color" => "green",
			),
			"poli_lansia" => array(
				"intIDPoli" => "42",			
				"description" => "Ruangan Lansia",				
				"color" => "teal",
			),
			"poli_gizi" => array(
				"intIDPoli" => "9",
				"description" => "Ruangan Gizi",
				"color" => "purple",
			),
			"ugd" => array(				
				"intIDPoli" => "6",
				"description" => "UGD",
				"color" => "red",
			),
		);
		echo json_encode($arrDataPoli);
	}

	public function pendaftaranLoket(){
		$id_kunjungan = "";
		$status = false;
		$message = "Data Kunjungan Gagal";
		$idPasien = $this->input->post("txtIdPasien");
		$idPelayanan = $this->input->post("intIDPoli");
		$dataPasien = $this->pasien_model->getDetail($idPasien);

		$intIdJaminanKesehatan = $dataPasien['intIdJaminanKesehatan'];
		$tanggalLahir = $dataPasien['dtTanggalLahir'];
		$tanggal = explode(" " , $tanggalLahir);
		$usia = personAge($tanggal[0]);	
		$usiaHari = getIntervalDays($tanggal[0]);
		$date_input = date("Y-m-d");
		$inputNoAnggota = $dataPasien['txtNoAnggota'];
		$check = $this->register_model->checkDuplicate($date_input,$idPasien,$inputNoAnggota);
		if (!empty($check)) {
		$retVal = array();
		$retVal['status'] = false;
		$retVal['message'] = "Data Duplikat Pada Antrian : ".$check["intNoAntri"];
		$retVal['id'] = "";
			
		}else{
		$arrayInput = array(
						"txtIdPasien" => $idPasien,
						"intIdPelayanan" => $idPelayanan,
						"intIdJaminanKesehatan" => $dataPasien['intIdJaminanKesehatan'],
						"txtNoJaminanKesehatan" => $dataPasien['txtNoIdJaminanKesehatan'],
						"txtUsiaPasienKunjungan" => $usia,
						"intUsiaPasienHari" => $usiaHari,
						"txtKeteranganKunjungan" => "",
					);
		
		$detailJenisPelayanan = $this->pelayanan_model->getDetail($idPelayanan);
		$idJenisPelayanan = $detailJenisPelayanan['intIdJenisPelayanan'];

		///Default Value For Rawat Jalan 
		$bitStatusKunjungan = 1;
		$bitStatusPoli = 1;
		/// For UGD 
		if($idJenisPelayanan==7){
			$bitStatusPoli = 2;
		}

		/// For Rawat Inap
		if($idJenisPelayanan==9){
			$bitStatusKunjungan = 2;
			$bitStatusPoli = 1;
		}

		/// Start No. AntrianLoket
		if($idPelayanan==1){ //Poli Umum I
			$startAntrian = 300;
		}else if ($idPelayanan==42) { //Poli Umum II
			$startAntrian = 200;
		}else if ($idPelayanan==3) { //Poli Gigi
			$startAntrian = 700;
		}else if ($idPelayanan==4) { //Poli Anak
			$startAntrian = 600;
		}else if ($idPelayanan==5) { //Poli Ibu
			$startAntrian = 500;
		}else if ($idPelayanan==7) { //Laboratorium
			$startAntrian = 100;
		}else if ($idPelayanan==9) { //Poli Gizi
			$startAntrian = 400;
		}else { //Laboratorium
			$startAntrian = 800;
		}

		/// Insert Mode
		///$date_now = date("Y-m-d H:i:s");
		$date_input = $this->input->post("inputTanggal");
		///$date_now = $date_input." ".date("H:i:s"); 
		$date_now = date("Y-m-d H:i:s");
		$date_server = date("Y-m-d");
		$jumlah_kunjungan = $this->register_model->countRegisterByPelayanan($idPelayanan);
		$isKunjunganBaru = $this->register_model->checkKunjunganByIdPasien($idPasien);
		$noAntri = $startAntrian + ($jumlah_kunjungan + 1);
		$id_kunjungan = $this->generateIdKunjungan();
		$arrayInput['txtIdKunjungan'] = $id_kunjungan;
		$arrayInput['intNoAntri'] = $noAntri;
		$arrayInput['dtTanggalKunjungan'] = $date_now;
		$arrayInput['dtCreatedKunjungan'] = $date_now;
		$arrayInput['dtLastUpdateKunjungan'] = $date_now;
		$arrayInput['bitStatusKunjungan'] = $bitStatusKunjungan;
		$arrayInput['bitIsPoli'] = $bitStatusPoli;
		$arrayInput['bitIsBaru'] = $isKunjunganBaru==true ? 1 : 0;
		$resSave = $this->register_model->saveData($arrayInput);
		
		$status = $resSave['status'];
		$message = $status==true ? "Pendaftaran Berhasil " : "Pendaftaran Gagal (Silahkan Hubungi petugas)";
		$retVal = array();

		$retVal['status'] = $status;
		$retVal['message'] = $message;
		$retVal['id_kunjungan'] = $id_kunjungan;
		$retVal['no_antrian'] = $noAntri;
		}
		echo json_encode($retVal);
	}

	public function getDetailKunjungan(){

		$id_kunjungan = $this->input->post("id_kunjungan");
		$detail_register = $this->register_model->getDetailRegister($id_kunjungan);
		$dt = array();
		$dt['nama_poli'] = $detail_register['txtNama'];
		$dt['no_antrian'] = $detail_register['intNoAntri'];
		echo json_encode($dt);
	}

	public function getDataFingerPasien(){

		$this->load->model("finger_model");
		$limit = $this->input->post("limit");
		$offset = $this->input->post("offset");
		$status = false;
		$message = "Error Form Service Please Call Administrator";

		$countData = $this->finger_model->getCountDataFingerPasien();
		$rawData = $this->finger_model->getDataFingerPasien($limit , $offset);

		if(!empty($rawData)){
			$status = true;
			
			$message = array(
				"countData" => $countData,
				"dataFinger" => $rawData,
			);
		}

		$retVal = array(
			"status" => $status,
			"message" => $message
		);
		$this->setOutputResponse($retVal);
	}

	public function getUpdateFingerPasien(){
		$this->load->model("finger_model");
		$date = date('Y-m-d');

		$limit = $this->input->post("limit");
		$offset = $this->input->post("offset");
		$status = false;
		$message = "Error Form Service Please Call Administrator";

		$countData = $this->finger_model->getCountDataUpdateFingerPasien($date);
		$rawData = $this->finger_model->getUpdateLogFingerPasien($date ,$limit , $offset);

		if(!empty($rawData)){
			$status = true;
			
			$message = array(
				"countData" => $countData,
				"dataFinger" => $rawData,
			);
		}

		$retVal = array(
			"status" => $status,
			"message" => $message
		);
		$this->setOutputResponse($retVal);
	}

	public function getDataFingerPegawai(){
		$this->load->model("finger_model");
		$limit = $this->input->post("limit");
		$offset = $this->input->post("offset");
		$status = false;
		$message = "Error Form Service Please Call Administrator";

		$countData = $this->finger_model->getCountDataFingerPegawai();
		$rawData = $this->finger_model->getDataFingerPegawai($limit , $offset);

		if(!empty($rawData)){
			$status = true;
			
			$message = array(
				"countData" => $countData,
				"dataFinger" => $rawData,
			);
		}

		$retVal = array(
			"status" => $status,
			"message" => $message
		);
		$this->setOutputResponse($retVal);
	}

	public function getUpdateFingerPegawai(){
		$this->load->model("finger_model");
		$date = date('Y-m-d');
		
		$limit = $this->input->post("limit");
		$offset = $this->input->post("offset");
		$status = false;
		$message = "Error Form Service Please Call Administrator";

		$countData = $this->finger_model->getCountDataUpdateFingerPegawai($date);
		$rawData = $this->finger_model->getUpdateLogFingerPegawai($date ,$limit , $offset);

		if(!empty($rawData)){
			$status = true;
			
			$message = array(
				"countData" => $countData,
				"dataFinger" => $rawData,
			);
		}

		$retVal = array(
			"status" => $status,
			"message" => $message
		);
		$this->setOutputResponse($retVal);
	}

	public function insertAbsensiPegawai(){
		$this->load->model("pegawai_model");
		$this->load->model("self_service_model" , "absensi_model");

        $dayNowIndex = date("w");
        $intIdPegawai = $this->input->post("intIdPegawai");
        $dateServer = date("Y-m-d");
        $timeServer = date("H:i:s");
        $dateTimeServer = $dateServer . " ".$timeServer;
        $timeStampInsert = strtotime($dateServer." ".$timeServer);

        $getDay = $this->absensi_model->getJamByIntDay($dayNowIndex);
        $jamMasuk = strtotime($dateServer." ".$getDay['dtJamMasuk']);
        $jamPulang = strtotime($dateServer." ".$getDay['dtJamPulang']);

        $checkData = $this->absensi_model->checkPresensiPegawai($intIdPegawai , $dateServer);
        $intIdAbsensi = !empty($checkData) ? $checkData['intIDAbsensi'] : "";
        $arrData = array(
            "intIDPegawai" => $intIdPegawai,
        );
        $status = 1;
        $dtAbsensiMasuk = $dateTimeServer;
        $dtAbsensiKeluar = "0000-00-00 00:00:00";
        if($timeStampInsert <= $jamMasuk){
            //// Masuk
            $dtAbsensiMasuk = $dateTimeServer;
        }

        if(($timeStampInsert >= $jamMasuk) && ($timeStampInsert <= $jamPulang)){
            /// Insert Terlambat
            if(empty($intIdAbsensi)){
                $status = 6;
            }else{
                
                $intIdAbsensi = "";
                $dtAbsensiMasuk = $checkData['dtAbsensiMasuk'];
                $dtAbsensiKeluar = $dateTimeServer;
            }
        }else if($timeStampInsert >= $jamPulang){
            /// Pulang - Update
            
            if(!empty($intIdAbsensi)){
                $dtAbsensiMasuk = $checkData['dtAbsensiMasuk'];
                $dtAbsensiKeluar = $dateTimeServer;
            }else{    
                $status = 1; /// Undefined
                $dtAbsensiMasuk = $dateTimeServer;
                $dtAbsensiKeluar = $dateTimeServer;
            }            
        }

        $arrData['intStatus'] = $status;
        $arrData['dtAbsensiMasuk'] = $dtAbsensiMasuk;
        $arrData['dtAbsensiKeluar'] = $dtAbsensiKeluar;
        $resData = $this->absensi_model->insertUpdate($arrData , $intIdAbsensi);
		///echo json_encode($resData);
		$dataPegawai = $this->pegawai_model->getDetail($intIdPegawai);
		$resData['detailPegawai'] = $dataPegawai;
		$this->setOutputResponse($resData);
	}

	function detailPegawai(){
		$this->load->model("pegawai_model");
		$intIdPegawai = $this->input->post("intIdPegawai");
		
		echo json_encode($dataPegawai);
	}

	private function setOutputResponse($retVal){
		echo json_encode($retVal);
		die;
	}

	
}