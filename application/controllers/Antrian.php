<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Antrian extends MY_Controller {
    
    var $meta_title = "Antrian";
    var $meta_desc = "Antrian";
	var $main_title = "Antrian";
    var $base_url = "";
	var $base_url_pasien = "";
	var $upload_dir = "";
	var $upload_url = "";
	var $limit = "20";
	var $arr_level_user = "";
    var $menu = "K01";
	
    public function __construct(){
        parent::__construct();
        $this->base_url = $this->base_url_site."loket/";
		$this->base_url_pasien = $this->base_url_site."pasien/";
		$this->load->model("pasien_model");
		$this->load->model("pelayanan_model");
        $this->load->model("register_model");
        $this->load->model("register_loket_model");
		$this->load->model("register_history_model");
		$this->load->model("register_rawat_inap_m");
		$this->load->model("rekam_medis_model" , "rekam_medis");
		$this->load->model("jaminan_kesehatan_model" ,"jaminan_kesehatan");
    }
	
    public function dashboard(){
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
			"menu_key" => "loket_daftar",
			"akses_key" => "is_view",
			"container" => $this->_dashboard(),
			"custom_js" => array(
				ASSETS_JS_URL."antrian/dashboard.js"
			),
            "custom_css" => array(
				
			),
		);	
		$this->_render_antrian("default",$dt);
    }

	public function index()
	{
		$dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
			"menu_key" => "loket_daftar",
			"akses_key" => "is_view",
			"container" => $this->_content(),
			"custom_js" => array(
				ASSETS_JS_URL."antrian/content.js"
			),
            "custom_css" => array(
				
			),
		);	
		$this->_render_antrian("default",$dt);	
	}

    

    private function _dashboard(){
        $dt = array();
        $breadcrumbs = array(
                        "Home" =>$this->base_url_site,
                        "Dashboard" => $this->base_url,
                        );
        
        $dataPelayanan = $this->pelayanan_model->getListDataPelayanan();                
        
		$dt['title'] = "Antrian Pengguna Layanan";
        $dt['date_input'] = $this->form_builder->inputHidden("date" , date("Y-m-d"));
        $dt['data_pelayanan'] = $dataPelayanan;
        $dt['tglAntrian'] = indonesian_date(date("Y-m-d"));
        $ret = $this->load->view("antrian/dashboard" , $dt , true);
        return $ret;
    }

	private function _content(){
        $dt = array();
        $breadcrumbs = array(
                        "Home" =>$this->base_url_site,
                        "Antrian" => $this->base_url,
                        );
        $dataPelayanan = $this->pelayanan_model->getListDataPelayanan();
        $arrPelayanan = array();
        foreach($dataPelayanan as $rowPelayanan){
            $arrPelayanan[$rowPelayanan['intIdPelayanan']] = $rowPelayanan['txtNama'];
        }

        $dt['breadcrumbs'] = $this->setBreadcrumbs($breadcrumbs);
        $dt['txtTanggal'] = $this->form_builder->inputHidden("txtTanggal" , date("Y-m-d"));
        $dt['intIdPoli'] = $this->form_builder->inputDropdown("Pelayanan" , "intIdPoli" , "" , $arrPelayanan , array() , "col-sm-12");
        $dt['tglAntrian'] = indonesian_date(date("Y-m-d"));
		$dt['title'] = "Antrian Pengguna Layanan Rawat Jalan";
        $ret = $this->load->view("antrian/content" , $dt , true);
        return $ret;
    }

    

    public function getDataAntrian(){
        if(!$this->input->is_ajax_request()){
            echo "Ilegalll";die;
        }

        $date_service = $this->input->post("txtTanggal");
        $unit_poli = $this->input->post("intPoli");
		$status_list = $this->config->item("status_layanan_list");
        $alert_list = $this->config->item("status_layanan_alert");
		if(!empty($date_service)){
			$date_start = $date_service. " 00:00:00";
			$date_end = $date_service. " 23:59:59";
		}
        //$dataAntrian = $this->register_model->getDataAntrianRawatJalan($date_start , $date_end , $unit_poli);

        $dataAntrian = $this->register_loket_model->dataLoket($date_service , '' , $unit_poli);
        
        
        
        $retVal = array();
        $retVal['data'] = array();
        foreach($dataAntrian as $rowAntrian){
            $bitStatus = $rowAntrian['bitIsPoli'];
            $txtStatus = $status_list[$bitStatus];
            $alert = $alert_list[$bitStatus];
            $label = "<label class='label label-".$alert."'>".$txtStatus."</label>";
            $arrData = array(
                $rowAntrian['intNoAntri'],
                $rowAntrian['txtNama'],
                $label
            );
            $retVal['data'][] = $arrData;
        }
    
        ////$this->setJsonOutput($retVal);
        echo json_encode($retVal);
    }

   

    public function getDashboardAntrian(){
        if(!$this->input->is_ajax_request()){
            echo "Ilegal";die;
        }
        $date = $this->input->post("date");
        $poli = $this->input->post("poli");
        $jumlah = $this->register_loket_model->getJumlahAntrianPerPoli($date , $poli);
        $noAntrian = $this->register_loket_model->getNoAntrianDiLayani($date , $poli);
        $noAntrian = !empty($noAntrian) ? $noAntrian['intNoAntri'] : "0";
        $retVal = array();
        $jumlah = !empty($jumlah) ? $jumlah : 0;
        $retVal['jumlah_tunggu'] = "Jumlah Antrian Menunggu : ".$jumlah;
        $retVal['jumlah_angka'] = $jumlah;
        $retVal['no_antrian_dilayani'] = $noAntrian;
        echo json_encode($retVal);
    }

   

    public function getTotalKunjungan(){
        if(!$this->input->is_ajax_request()){
            echo "Ilegal";die;
        }
        $date = $this->input->post("date");
        $poli = $this->input->post("poli");
        $jumlahPengunjung = $this->register_model->getCountRekapPengunjungRawatJalan($poli , $date , $date);
        $jumlah = !empty($jumlahPengunjung) ? $jumlahPengunjung : 0;
        $retVal = array();
        $retVal['jumlah_dilayani'] = $jumlah;
        echo json_encode($retVal);
    }

	public function getAntrianLoket(){
		if(!$this->input->is_ajax_request()){
			echo  "Die, Its Ilegal";
		}
		$status = false;
		$start = 0;
		//// Date
		$date_start = date("Y-m-d"). " 00:00:00";
		$date_end = date("Y-m-d"). " 23:59:59";
        //$data_register = $this->register_model->getRegisterRawatJalan($date_start , $date_end);
        $data_register = $this->register_loket_model->dataLoket(date("Y-m-d") , '' , '');
		// $jumlah_pengunjung = $this->register_model->countRegisterByPelayanan("",$date_start , $date_end , "");
		// $jumlah_sudah = $this->register_model->countRegisterByPelayanan("",$date_start , $date_end , "3");
		// $jumlah_sedang = $this->register_model->countRegisterByPelayanan("",$date_start , $date_end , "2");
		// $jumlah_antri = $this->register_model->countRegisterByPelayanan("",$date_start , $date_end , "1");
		$status_layanan_list = $this->config->item("status_layanan_list");
		$resData = array();
		if(!empty($data_register)){
			$status = true;
			foreach($data_register as $indexRegister => $rowRegister){
				$status_list = "";
				if($rowRegister['bitIsPoli']==1){ //antri
					$status_list = "danger";
				}else if($rowRegister['bitIsPoli']==2){ //dilayani
					$status_list = "warning";
				}else if($rowRegister['bitIsPoli']==3){ //selesai
					$status_list = "success";
				}else if($rowRegister['bitIsPoli']==4){ //pemeriksaan lanjut
					$status_list = "info";
				}else if($rowRegister['bitIsPoli']==5){ //rujukan
					$status_list = "default";
				}
				$data["noAntri"] = $rowRegister['intNoAntri'];
				$data["namaPoli"] = $rowRegister['txtNama'];
				$data["statusPasien"] = "<label class='label label-".$status_list."'>".$status_layanan_list[$rowRegister['bitIsPoli']]."</label>";
				$resData[] = $data;
			}
		}
		
		$retVal = array();
		$retVal['status'] = $status;
		$retVal['data'] = $resData;
		
		$this->setJsonOutput($retVal);
	}

}