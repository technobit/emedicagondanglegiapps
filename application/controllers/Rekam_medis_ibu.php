<?php
class Rekam_medis_ibu extends MY_Controller {
    var $meta_title = "Rekam Medis Ibu";
    var $meta_desc = "Rekam Medis Ibu";
	var $main_title = "Rekam Medis Ibu";
    var $base_url = "";
	var $limit = "20";
    var $menu = "K01";
    var $date_now = "";
    
	function __construct(){
        parent::__construct();
		$this->load->model("data_penyakit_model" , "data_penyakit");
        $this->load->model("rekam_medis_ibu_model" , "rekmed_ibu");
        $this->load->model("rekam_medis_model" , "rekam_medis");
		$this->load->model("register_model");
		$this->load->model("pasien_model");
		$this->load->model("pelayanan_model");
        $this->base_url = $this->base_url_site."rekam-medis-ibu/";
        $this->date_now = date("Y-m-d H:i:s");
    }
	
    public function detail_rekmed_ibu( $jenisPelayanan , $idRekmedIbu , $idDetailRekmedIbu){
		
		$dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
			"menu_key" => "poli_ibu",
			"akses_key" => "is_insert",	
			"container" => $this->_build_detail_rekam_medis($jenisPelayanan,$idRekmedIbu,$idDetailRekmedIbu),
			"custom_js" => array(
				ASSETS_URL."plugins/select2/select2.js",
				ASSETS_URL."plugins/bootbox/bootbox.js",
				ASSETS_URL."plugins/validate/jquery.validate_1.11.1.min.js",
				ASSETS_JS_URL."pelayanan/poli_ibu/form_detail.js",
			),
            "custom_css" => array(
				ASSETS_URL."plugins/select2/select2.css",
			),
		);	
		
		$this->_render("default",$dt);
	}
	
	private function _build_detail_rekam_medis($jenisPelayanan , $idRekmed , $idRekmedDetailIbu){
		
		$dt = array();
		$dt['title'] = "Detail Pemeriksaan Ibu";
		
		$breadcrumbs = array(
				"Home" =>$this->base_url_site,
				"Rekam Medis Ibu" => $this->base_url,
				"Detail Rekam Medis Ibu" => "#",
        );
		
		$dt['breadcrumbs'] = $this->setBreadcrumbs($breadcrumbs);
		
		$detail_register = $this->register_model->getDetailRegister($idRekmed);
        if(count($detail_register) < 1){
            show_404();
        }
		$idPasien = $detail_register['txtIdPasien'];
		$idPelayanan = $detail_register['intIdPelayanan'];
        $detail_pelayanan = $this->pelayanan_model->getDetail($idPelayanan);
		$detail_pasien = $this->pasien_model->getDetail($idPasien);
		$rekam_medis = $this->rekam_medis->getDetailByIdPasien($idPasien);
		
		$idRekamMedis = "";
		$noRekamMedis = $detail_pasien['txtNoAnggota'];
		
		if(count($rekam_medis) > 0){
			$idRekamMedis = $rekam_medis['txtIdRekamMedis'];
			$noRekamMedis = $rekam_medis['txtNoRekamMedis'];
		}
		
		$nama_poli = $detail_pelayanan['txtNama'];
        $this->meta_title = $nama_poli;
        $this->meta_desc = "Fasilitas ".$nama_poli;
		
		$dt['title'] = "Pemeriksaan Rawat Jalan ".$nama_poli;
		$dt['form_title'] = "No Pengguna Layanan ".$detail_pasien['txtNoAnggota'];
		$dt['jenis_pelayanan'] = $jenisPelayanan;
		
		/// Build Header Form -> Hidden
		$dt['frm_id_pasien'] = $this->form_builder->inputHidden("txtIdPasien" , $detail_pasien['txtIdPasien']);
		$dt['frm_id_rekam_medis'] = $this->form_builder->inputHidden("txtIdRekamMedis" , $idRekamMedis);
		$dt['frm_id_pelayanan'] = $this->form_builder->inputHidden("txtIdPelayanan" , $detail_register['intIdPelayanan']);
		$dt['frm_id_kunjungan'] = $this->form_builder->inputHidden("txtIdKunjungan" , $detail_register['txtIdKunjungan']);
		$dt['frm_id_rekmed_ibu'] = $this->form_builder->inputHidden("txtIdDetailRekmedIbu" , $idRekmedDetailIbu);
		
		///// Builder To View
		$dt['frm_tanggal'] = $this->form_builder->inputText("Tanggal Pemeriksaan" , "txtTanggalPemeriksaan", indonesian_date($detail_register['dtTanggalKunjungan']) , "col-sm-3" , array("readonly"=>"readonly"));
		$dt['frm_no_anggota_pasien'] = $this->form_builder->inputText("No Anggota" , "txtNoAnggotaPasien", $detail_pasien['txtNoAnggota'] , "col-sm-3" , array("readonly"=>"readonly"));
		$dt['frm_nama_pasien'] = $this->form_builder->inputText("Nama Pengguna Layanan" , "txtNamaPasien", $detail_pasien['txtNamaPasien'] , "col-sm-3" , array("readonly"=>"readonly"));
		$dt['frm_usia_pasien'] = $this->form_builder->inputText("Usia Pengguna Layanan" , "txtUsiaPasien", $detail_register['txtUsiaPasienKunjungan'] , "col-sm-3" , array("readonly"=>"readonly"));
		$dt['frm_no_rekam_medis'] = $this->form_builder->inputText("No Rekam Medis" , "txtNoRekamMedis", $noRekamMedis, "col-sm-3" );
		
		$link_pemeriksaan = $jenisPelayanan=='pelayanan' ? $this->base_url_site."pelayanan/poli-ibu/detail/".$idRekmed : $this->base_url_site."rekam-medis/detail-rekam-medis/".$idRekmed;
		$dt['link_form_detail'] = $link_pemeriksaan;
		$retVal = $this->load->view("pelayanan/poli_ibu/form_detail" , $dt , true);
		return $retVal;
	}
	
	//// Detail Pemeriksaan Ibu / Riwayat pemeriksaan
	public function saveDetailRekmedIbu(){
        if(!$this->input->is_ajax_request()){
			echo "Die, Ilegall Access";
			die;
		}
        $idRekmedIbu = $this->input->post("intIdRekamMedisIbu");
		$idPegawai = $this->session->userdata("sipt_user_id");
		$arrPost = array(
						 "txtIdRekamMedis" => $this->input->post("txtIdRekamMedis"),
						 "intIdPegawai" => $idPegawai,
						 "dtTanggalPemeriksaan" => $this->input->post("txtTanggalPemeriksaanIbu"),
						 "intKehamilanKe" => $this->input->post("txtKehamilanKe"),
						 "txtNoIndeks" => $this->input->post("txtNoIndeks"),
						 "bitStatusPemeriksaan" => $this->input->post("bitStatusPeriksa"),
						 );
		
		if(empty($id)){
			$arrPost['dtCreatedDate'] = $this->date_now;
			$arrPost['dtLastUpdate'] = $this->date_now;
			
		}else{
			$arrPost['dtLastUpdate'] = $this->date_now;
		}
		$resVal = $this->rekmed_ibu->SaveUpdate($arrPost , $idRekmedIbu);
        $this->setJsonOutput($resVal);
    }
	
	public function getDetailRekamMedisIbu(){
		if(!$this->input->is_ajax_request()){
			echo "Die, Ilegall Access";
			die;
		}
		
		$status = false;
		$html = "";
		$length =  $this->input->post("length");
		$start = $this->input->post("start");
		$id = $this->input->post("txtIdRekamMedis");
		$pelayanan = $this->input->post("jenisPelayanan") ? "pelayanan" : "rekam-medis";
		$rekmedId = $pelayanan=="pelayanan" ? $this->input->post("txtIdKunjungan") : $this->input->post("txtIdRekamMedis");
		$offset = ($start - 0) * $this->limit;
		$data_rekmed = $this->rekmed_ibu->getDetailRekamMedisIbu($offset , $this->limit , $id);
		$count_data = $this->rekmed_ibu->getCountDetailRekamMedisIbu($id);
		if($count_data > 0) {
			$retVal['draw'] = $this->input->post('draw');
			$retVal['recordsTotal'] = $count_data;
			$retVal['recordsFiltered'] = $count_data;
			$retVal['data'] = array();
			foreach($data_rekmed as $pasien){
				$status = $pasien['bitStatusPemeriksaan']==1 ? "Aktif" : "Non-Aktif";
				$btnHapus = '<button class="btn btn-warning btn-xs btn-flat" onclick="hapusDataRiwayatKehamilan('.$pasien['intIdRekamMedisIbu'].')"><i class="fa fa-trash"></i> Hapus</button>';
				$btnDetail = "<a href='".$this->base_url."detail-rekam-medis-ibu/".$pelayanan."/".$rekmedId."/".$pasien['intIdRekamMedisIbu']."' class='btn btn-info btn-xs btn-flat'><i class=\"fa fa-pencil\"></i> Detail</a>";

				$btnAksi = $btnDetail.$btnHapus;
				$retVal['data'][] = array($pasien['dtTanggalPemeriksaan'],$pasien['intKehamilanKe'], $pasien['txtNoIndeks'], $status,$btnAksi);
			}
		}else{
			$retVal['draw'] = $this->input->post('draw');
			$retVal['recordsTotal'] = 0;
			$retVal['recordsFiltered'] = 0;
			$retVal['data'] = array();
		}
		$this->setJsonOutput($retVal);
	}


	function deleteData(){
		if(!$this->input->is_ajax_request()){
			echo "Die, Ilegall Access";
			die;
		}

		$intIdRekamMedisIbu = $this->input->post("id");
		$retVal = $this->rekmed_ibu->deleteAll($intIdRekamMedisIbu);
		$this->setJsonOutput($retVal);
	}
}