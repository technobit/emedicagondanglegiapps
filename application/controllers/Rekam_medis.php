<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rekam_medis extends MY_Controller {

	var $meta_title = "Rekam Medis";
	var $meta_desc = "Rekam Medis";
	var $main_title = "Rekam Medis";
	var $base_url = "";
	var $upload_dir = "";
	var $upload_url = "";
	var $limit = "20";
	var $arr_level_user = "";
	var $menu = "K01";
	var $date_now = "";
	var $menu_key = "pasien_rekam_medik";

	function __construct(){
		parent::__construct();
		$this->load->model(array(
		"data_penyakit_model" => "data_penyakit",
		"rekam_medis_model" => "rekam_medis",
		"rekam_medis_kondisi",
		"register_model",
		"pasien_model",
		"pegawai_model",
		"pelayanan_model",
		"tindakan_model",
		));
		$this->base_url = $this->base_url_site."rekam-medis/";
		$this->date_now = date("Y-m-d H:i:s");
	}


	public function index()
	{

		$menu = "K01";

		$dt = array(
		"title" => $this->meta_title,
		"description" => $this->meta_desc,
		"container" => $this->_data_rekam_medis(),
		"menu_key" => $this->menu_key,
		"akses_key" => "is_view",
		"custom_js" => array(
		ASSETS_JS_URL."rekam_medis/content.js",
		),
		"custom_css" => array(

		),
		);

		$this->_render("default",$dt);

	}


	public function addRekamMedis($idPasien=""){

		$menu = "RM02";

		$dt = array(
		"title" => $this->meta_title,
		"description" => $this->meta_desc,
		"container" => $this->_contentFormRekamMedis($idPasien),
		"custom_js" => array(
		ASSETS_URL."plugins/select2/select2.js",
		ASSETS_URL."plugins/validate/jquery.validate_1.11.1.min.js",
		ASSETS_JS_URL."rekam_medis/form_content.js",
		),
		"custom_css" => array(
		ASSETS_URL."plugins/select2/select2.css",
		),
		);

		$this->_render("default",$dt);

	}


	private function _contentFormRekamMedis($idPasien){


		$dataPasien = array();

		if(!empty($idPasien)){

			$checkRekamMedis = $this->rekam_medis->getMasterByIdPasien($idPasien);

			if(count($checkRekamMedis) < 1){

				$dataPasien = $this->pasien_model->getDetail($idPasien);

			}
			else{

				$idRekamMedis = $checkRekamMedis['txtIdRekamMedis'];

				$detailRekamMedislink = $this->base_url."detail-rekam-medis/".$idRekamMedis;

				redirect($detailRekamMedislink);

			}

		}

		$arrRekamMedis = array("txtIdPasien" , "txtNamaPasien" , "txtNoAnggota");

		foreach($arrRekamMedis as $rekMed){

			$$rekMed = isset($dataPasien[$rekMed]) ? $dataPasien[$rekMed] : "";

		}


		$breadcrumbs = array(
		"Home" =>$this->base_url_site,
		"Rekam Medis" => $this->base_url,
		"Tambah Rekam Medis" => "#",
		);


		$dt['breadcrumbs'] = $this->setBreadcrumbs($breadcrumbs);

		$dt['title'] = "Tambah Rekam Medis";


		///		Form Rekam medis
		$arrSelected = array($idPasien => $txtNamaPasien);

		$dt['frm_mode_rekam_medis'] = $this->form_builder->inputHidden("modeInsertRekamMedis" , "insertNoRekamMedis");

		$dt['frm_nama_pasien'] = $this->form_builder->inputDropdown("Nama Pengguna Layanan" , "txtIdPasien" ,$idPasien, $arrSelected );

		$dt['frm_no_rekam_medis'] = $this->form_builder->inputText("No Rekam Medis" , "txtNoRekamMedis", $txtNoAnggota, "col-sm-3" );


		$dt['link_rekmed'] = $this->base_url;

		$retVal = $this->load->view("rekam_medis/form_content" , $dt , true);

		return $retVal;

	}


	public function detailRekamMedis($id){

		$menu = "K02";

		$dt = array(
		"title" => $this->meta_title,
		"description" => $this->meta_desc,
		"container" => $this->_detail($id),
		"menu_key" => $this->menu_key,
		"akses_key" => "is_insert",
		"custom_js" => array(
		ASSETS_URL."plugins/select2/select2.js",
		ASSETS_URL."plugins/validate/jquery.validate_1.11.1.min.js",
		ASSETS_JS_URL."rekam_medis/detail.js",
		),
		"custom_css" => array(
		ASSETS_URL."plugins/select2/select2.css",
		),
		);

		$this->_render("default",$dt);

	}


	public function insUpdRekamMedis($poli = "umum",$idRekamMedis , $idDetailRekamMedis = "" ){

		$menu = "K02";
		$dt = array(
		"title" => $this->meta_title,
		"description" => $this->meta_desc,
		"container" => $this->_form_rekam_medis($idRekamMedis , $idDetailRekamMedis, $poli),
		"menu_key" => $this->menu_key,
		"akses_key" => "is_insert",
		"custom_js" => array(
		ASSETS_URL."plugins/select2/select2.js",
		ASSETS_PLUGIN_URL . "jSignature/libs/jSignature.min.js",
		ASSETS_PLUGIN_URL . "jSignature/plugins/jSignature.CompressorBase30.js",
		ASSETS_PLUGIN_URL . "jSignature/plugins/jSignature.CompressorSVG.js",
		ASSETS_URL."plugins/validate/jquery.validate_1.11.1.min.js",
		ASSETS_JS_URL."rekam_medis/form.js",
		),
		"custom_css" => array(
		ASSETS_URL."plugins/select2/select2.css",
		),
		);

		$this->_render("default",$dt);

	}


	private function _form_rekam_medis($idRekamMedis , $idDetailRekamMedis , $poli){

		if (!empty($idDetailRekamMedis)) {
			$detailRekmed = $this->rekam_medis->getDetailRekamMedisByIdDetail($idRekamMedis , $idDetailRekamMedis);
		
		}else{
			$detailRekmed = $this->rekam_medis->detailByRekmed($idRekamMedis);
		
		}
		
		if(empty($detailRekmed)){

			show_404();

		}


		$idPasien = $detailRekmed['txtIdPasien'];

		$dataPasien = $this->pasien_model->getDetail($idPasien);

		$mode = "insert";


		$breadcrumbs = array(
		"Home" =>$this->base_url_site,
		"Rekam Medis" => $this->base_url,
		"Rekam Medis Pengguna Layanan" => $this->base_url."detail-rekam-medis/".$idRekamMedis,
		"Tambah Detail Rekam Medis Pengguna Layanan" => $this->base_url."detail-rekam-medis/".$idRekamMedis,
		);


		$dt['breadcrumbs'] = $this->setBreadcrumbs($breadcrumbs);

		$dt['title'] = "Tambah Rekam Medis";

		$form_rekam_medis = "";


		//D		etail Rekam Medis
		$jenisKelamin = $dataPasien['charJkPasien']=="L" ? "Laki-Laki" : "Perempuan";

		$dt['frm_id_rekam_medis'] = $this->form_builder->inputHidden("txtIdRekamMedis",$detailRekmed['txtIdRekamMedis']);
		$dt['mode_form'] = $this->form_builder->inputHidden("modeRekmed",'1');

		$dt['frm_id_pasien'] = $this->form_builder->inputHidden("inputIdPasien",$dataPasien['txtIdPasien']);

		$dt['frm_no_anggota_pasien'] = $this->form_builder->inputText("No Anggota" , "txtNoAnggota", $dataPasien['txtNoAnggota'] , "col-sm-3" , array("readonly"=>"readonly"));

		$dt['frm_nama_pasien'] = $this->form_builder->inputText("Nama Pengguna Layanan" , "txtNamaPasien", $dataPasien['txtNamaPasien'] , "col-sm-3" , array("readonly"=>"readonly"));

		$dt['frm_no_rekam_medis'] = $this->form_builder->inputText("No Rekam Medis" , "txtNoRekamMedis", $dataPasien['txtNoAnggota'], "col-sm-3" );
		

		$detailDataRekmed = array();
		$detailTindakan = array();
		$detailDiagnosa = array();
		$rujukan = "";
		$valid = "";
		$dataObat = "";
		$detailPasien = $this->pasien_model->getDetail($idPasien);
		$detailKondisi = $this->rekam_medis_kondisi->getDetail($idRekamMedis);
		$link_pernyataan = $this->base_url."cetak-pernyataan/".$idRekamMedis;
		if(!empty($idDetailRekamMedis)){
			$link_pernyataan = $this->base_url."cetak-pernyataan/".$idRekamMedis."/".$idDetailRekamMedis;
			$detailDataRekmed = $this->rekam_medis->getDetailRekamMedisByIdDetail($idRekamMedis, $idDetailRekamMedis);
			$detailDiagnosa = $this->rekam_medis->getDetailDiagnosa($idDetailRekamMedis);
			$detailPengobatan = $this->rekam_medis->getDataResep($idDetailRekamMedis);
			$dataObat = $detailPengobatan;
			$detailTindakan = $this->rekam_medis->getDetailTindakan($idDetailRekamMedis);
			$rujukan = $detailDataRekmed['txtRefIdKunjungan'];
			if ((empty($detailPengobatan)) && (!empty($detailDataRekmed['txtPengobatan']))) {
                $valid = "kosong";
            }else{
                $valid = "";
            }

		}
		$arrayInput = array(
		"intBerat",
		"intTinggi",
		);
		$intIdJaminanKesehatan = $detailPasien['intIdJaminanKesehatan'];
		$dtTanggalLahir = $detailPasien['dtTanggalLahir'];
		$valArray = array(
		"txtIdKunjungan" ,
		"txtIdRekmedDetail" ,
		"intIdPelayanan" ,
		"namaPoli" ,
		"dtTanggalKunjungan",
		"txtPemeriksaan" ,
		"txtAnamnesa" ,
		"txtTinggi" ,
		"txtBerat" ,
		"txtSistole" ,
		"txtDiastole" ,
		"txtOdontogram",
		"bitSetuju",
		"txtRespiratoryRate" ,
		"txtHeartRate" ,
		"txtKesadaran",
		"txtTindakan" ,
		"noKunjungan" ,
		"kdDiag1" ,
		"kdDiag2" ,
		"kdDiag3" ,
		"txtDiag1" ,
		"txtDiag2" ,
		"txtDiag3" ,
		"intIdPegawai",
		"txtKeterangan" ,
		"txtRencanaTindakan" ,
		"txtTandaTangan",
		"txtPengobatan",
		"txtRefIdKunjungan",
		"txtHasilRujukan");
		$arrSelected = array();
		foreach($valArray as $arrFrmMedis) {

			$$arrFrmMedis = isset($detailDataRekmed[$arrFrmMedis]) ? $detailDataRekmed[$arrFrmMedis] : "";

		}
		$kondisi = array();
		foreach ($arrayInput as $key => $value) {

			$$value = isset($detailKondisi[$value]) ? $detailKondisi[$value] : "";

		}
		$detailPelayanan = $this->pelayanan_model->getDetail($intIdPelayanan);
		$intIdJenisPelayanan = $detailPelayanan['intIdJenisPelayanan'];
		if(!empty($txtIdRekmedDetail)){

			$styleInput = array("rows"=>3 , "disabled"=>"disabled");
			$styleRawInput = "disabled=''";
			$styleDisabled = "disabled='true'";
			$styleRawInput = "disabled=''";
			$styleSelect = array("disabled"=>"disabled");
			$styleSelect2 = array("class"=>"form-control select-diagnosa","disabled"=>"disabled" , "id"=>"selectDiagnosa1" , "onchange" => "checkDiagnosaBPJS(1)");
			$styleSelect3 = array("class"=>"form-control" , "id"=>"select-tindakan","disabled"=>"disabled");
			$styleSelect4 = array("class"=>"form-control" , "id"=>"select-pengobatan","disabled"=>"disabled");
			$mode = "edit";

		}else{
			$styleInput = array("rows"=>3 );
			$styleSelect = array();
			$styleSelectOd = array("class"=>"form-control select-odontogram");
			$styleRawInput = "";
			$styleDisabled = "";
			$styleSelect2 = array("class"=>"form-control select-diagnosa" , "id"=>"selectDiagnosa1" , "onchange" => "checkDiagnosaBPJS(1)");
			$styleSelect3 = array( "id"=>"select-tindakan");
			$styleSelect4 = array( "id"=>"select-pengobatan");
			$mode = "insert";
		}

		$data_pelayanan = $this->pelayanan_model->getListDataPelayanan();
		$arrPelayanan = array();
		foreach ($data_pelayanan as $indexPelayanan => $valuePelayanan) {

			$arrPelayanan[$valuePelayanan['intIdPelayanan']] = $valuePelayanan['txtNama'];

		}
		$obatButton = '';
		$arraySelectLayanan = !empty($intIdPelayanan) ? array($intIdPelayanan => $namaPoli) : array();
		$dt['link_pernyataan_frm'] = $this->form_builder->inputHidden("link_pernyataan" , $link_pernyataan);
		
		$dt['pengobatan_lama'] = $valid;
		$dt['dataObat'] = $dataObat;
		$dtTanggalKunjungan = !empty($dtTanggalKunjungan) ? $dtTanggalKunjungan : date("Y-m-d");
		$dt['setuju'] = $bitSetuju==1? TRUE : FALSE;
		$dt['tidak_setuju'] = $bitSetuju==0? TRUE : FALSE;
		$dt['frm_id_rekam_medis'] = $this->form_builder->inputHidden("txtIdRekamMedis" , $idRekamMedis);
		$dt['frm_id_pasien'] = $this->form_builder->inputHidden("txtIdPasien" , $idPasien);
		$dt['frm_id_kunjungan'] = $this->form_builder->inputHidden("txtIdKunjungan" , $txtIdKunjungan);
		$dt['frm_no_kunjungan'] = $this->form_builder->inputHidden("noKunjungan" , $noKunjungan);
		$dt['frm_rekmed_id_detail'] = $this->form_builder->inputHidden("txtIdRekmedDetail" , $txtIdRekmedDetail);
		$dt['styleDisabled'] = $styleDisabled;
		$dt['frm_tanggal_lahir'] = $this->form_builder->inputHidden("dtTanggalLahir" , $dtTanggalLahir);
		$dt['frm_rekmed_rujukan'] = $this->form_builder->inputTextArea("Hasil Rujukan" , "txtHasilRujukan", $txtHasilRujukan , "col-sm-3" ,$styleInput);
		$dt['rujukan'] = $this->form_builder->inputHidden("rujukan" , $rujukan);
		$dt['frm_tgl_rekam_medis'] = $this->form_builder->inputText("Tanggal Rekam Medis" , "txtTanggalPeriksa", $dtTanggalKunjungan, "col-sm-3",$styleInput);
		$dt['frm_rekmed_pelayanan'] = $this->form_builder->inputDropdown("Jenis Pelayanan" , "txtIdPelayanan" ,$intIdPelayanan, $arrPelayanan,array_merge(array('onChange'=>'selectPoli()'),$styleSelect));
		$dt['frm_rekmed_keterangan'] = $this->form_builder->inputTextArea("Keterangan / Tindak lanjut" , "txtKeterangan", $txtKeterangan , "col-sm-3" ,$styleInput);
		

		///		/ Detail Pemeriksaan
		$dt['frm_rekmed_anamnesa'] = $this->form_builder->inputTextArea("Anamnesis" , "txtAnamnesa", $txtAnamnesa, "col-sm-3" ,$styleInput);
		$dt['frm_rekmed_tinggi_badan'] = form_input("txtTinggi" , !empty($intTinggi) ? $intTinggi :  $txtTinggi , 'class="form-control" id="txtTinggi"'.$styleRawInput);
		$dt['frm_rekmed_berat_badan'] = form_input("txtBerat" , !empty($intBerat) ? $intBerat : $txtBerat , 'class="form-control" id="txtBerat"'.$styleRawInput);
		$dt['frm_rekmed_sistole'] = form_input("txtSistole" , !empty($intTekananDarah)?$txtSistole: $txtSistole , 'class="form-control" id="txtSistole"'.$styleRawInput);
		$dt['frm_rekmed_diastole'] = form_input("txtDiastole" , !empty($intTekananDarah)? $txtDiastole : $txtDiastole , 'class="form-control" id="txtDiastole"'.$styleRawInput);
		$dt['frm_rekmed_respiratory_rate'] = form_input("txtRespiratoryRate" , $txtRespiratoryRate , 'class="form-control" id="txtRespiratoryRate"'.$styleRawInput);
		$dt['frm_rekmed_heart_rate'] = form_input("txtHeartRate" , $txtHeartRate , 'class="form-control" id="txtHeartRate"'.$styleRawInput);
		$kesadaran_list = $this->config->item("kesadaran_list");
		$dt['frm_rekmed_kesadaran'] = form_dropdown("txtKesadaran", $kesadaran_list , $txtKesadaran  , 'class="form-control" id="txtKesadaran"'.$styleRawInput);
		$dt['frm_ttd'] = form_hidden("inputTTD" , $txtTandaTangan);
		$dt['txtTandaTangan'] = $txtTandaTangan;
		$dt['frm_rekmed_pemeriksaan'] = $this->form_builder->inputTextArea("Pemeriksaan Umum" , "txtPemeriksaan", $txtPemeriksaan , "col-sm-3",$styleInput);
		$dt['frm_rekmed_pengobatan_lama'] = $this->form_builder->inputTextArea("Pengobatan" , "txtPengobatan", $txtPengobatan , "col-sm-3",$styleInput);

	
		///		Khususon Poli Gigi
		$hideOdontogram = 'style="display:none;"';

		if($intIdJenisPelayanan=='3'){
			$hideOdontogram = "";
		}

			$statusOdontogram = $this->config->item("status_odontogram");
			$rangeRome = $this->config->item("status_odontogram_rome");
			$arrSelectedOdontogram = array();
			foreach($statusOdontogram as $keyOdontogram => $odontogram){

				for ($i = 1; $i <= 8; $i++){

					$arrSelectedOdontogram[$keyOdontogram."_".$i] = $odontogram." ".$i;

				}
			}

			foreach($statusOdontogram as $keyOdontogram => $odontogram){

				for ($i = 1; $i <= 5; $i++){

					$arrSelectedOdontogram[$keyOdontogram."_".$rangeRome[$i]] = $odontogram." ".$rangeRome[$i];

				}
			}

			$txtOdontogramVal = !empty($txtOdontogram) ? $arrSelectedOdontogram[$txtOdontogram] : "";
			$frmAddOthers = '<div class="form-group" id="cont-selectOdontogram" '.$hideOdontogram.'>
								<label for="selectOdontogram" class="col-sm-3 control-label form-label" id="lblselectOdontogram">Status Odontogram</label>
								<div class="col-sm-9">
									<div class="input-group">
									'.form_input("txtOdontogram" , $txtOdontogramVal , 'class="form-control" id="txtOdontogram" readonly=""').'
									'.form_hidden('selectOdontogram' , $txtOdontogram).'
									<span class="input-group-btn">
										<button type="button" class="btn btn-info btn-flat" onclick="checkOdontogram()">Pilih Odontogram</button>
									</span>
									</div>
								</div>
							 </div>';

		

		$dt['frm_add_others'] = $frmAddOthers;

		$addDiagnosa = "";

		$addTindakan = "";

		$addPengobatan = "";
		$dt['frm_rekmed_diagnosa'] = form_dropdown("selectDiagnosa[]" , $arrSelected  , $arrSelected , $styleSelect2);
		$dt['frm_rekmed_bpjs'] = form_input("diagnosaBPJS[]" , "", 'placeholder="Diagnosa BPJS" class="form-control" id="diagnosaBPJS_1" readonly=""');

		$dt['intIdJaminanKesehatan'] = $intIdJaminanKesehatan;

		$dt['frm_rekmed_detail_diagnosa'] = $this->form_builder->inputTextArea("Detail Diagnosa" , "txtDetailDiagnosa[]", "", "col-sm-3" ,$styleInput);

		$dt['frm_no_diagosa'] = $this->form_builder->inputHidden("noDiagnosa" , 1);

		$dt['frm_button_add_diagnosa'] = '';

		$dt['frm_no_pengobatan'] = $this->form_builder->inputHidden("noPengobatan" , 1);

		$dt['frm_button_add_pengobatan'] = '';


		$dt['frm_rekmed_tindakan'] = $this->form_builder->inputDropdown("Tindakan" , "selectTindakan[]" ,$arrSelected , $arrSelected , $styleSelect3);

		$dt['frm_rekmed_jumlah'] = $this->form_builder->inputText("Jumlah" , "txtPengobatan[]", "", "col-sm-3" ,$styleInput);

		$dt['frm_rekmed_detail_tindakan'] = $this->form_builder->inputTextArea("Detail Tindakan" , "txtDetailTindakan[]", "", "col-sm-3" ,$styleInput);

		$dt['frm_no_tindakan'] = $this->form_builder->inputHidden("noTindakan" , 1);

		$dt['frm_button_add_tindakan'] = '';

		$dt['frm_rencana_tindakan'] = $this->form_builder->inputTextArea("Rencana Tindakan" , "txtRencanaTindakan", $txtRencanaTindakan , "col-sm-3" ,$styleInput);

		if(!empty($detailDiagnosa)){

			$arrSelected = array();

			$dataDiagnosa = array_slice($detailDiagnosa,0,1);

			$listDiagnosa = array_slice($detailDiagnosa,1,count($detailDiagnosa));

			$arrSelected = array($dataDiagnosa[0]['id'] => $detailDiagnosa[0]['txtIndonesianName']);

			$detailDiagnosa = $dataDiagnosa[0]['txtDetailDiagnosa'];

			$dt['frm_rekmed_diagnosa'] = form_dropdown("selectDiagnosa[]" , $arrSelected  , $arrSelected , $styleSelect2);
			$dt['frm_rekmed_bpjs'] = form_input("diagnosaBPJS[]" , $kdDiag1."-".$txtDiag1, 'class="form-control" id="diagnosaBPJS_1" readonly=""');
			$dt['frm_no_diagosa'] = $this->form_builder->inputHidden("noDiagnosa" , 1);
			$dt['frm_button_add_diagnosa'] = 'disabled=""';
			///			Diagnosa BPJS

			$intIdJaminanKesehatan = $detailPasien['intIdJaminanKesehatan'];

			if(($intIdJaminanKesehatan==1) || ($intIdJaminanKesehatan==3) || ($intIdJaminanKesehatan==4)){

				$dt['frm_rekmed_diagnosa_bpjs'] = $this->form_builder->inputText("Diagnosa Penyakit - BPJS" , "txtDiagnosaBPJS" ,"" , "col-sm-3" , $styleInput);

			}

			if(count($listDiagnosa) > 0){

				$number = 1;

				foreach($listDiagnosa as $rowDiagnosa){

					$numberDiagnosa = $number + 1;
					$strDiag = "";
					switch($numberDiagnosa){
						case 2 : $strDiag = $kdDiag2."-".$txtDiag2;break;
						case 3 : $strDiag = $kdDiag3."-".$txtDiag3;break;
						default : $strDiag="";
					}
					$arrSelected = array($rowDiagnosa['id'] => $rowDiagnosa['txtIndonesianName']);

					$addDiagnosa .= '<div class="form-group" id="contSelect'.$number.'">
									 <label class="col-sm-3 control-label form-label">Diagnosa Penyakit '.($number+1).'</label>
									 <div class="col-sm-9">
									 '.form_dropdown("selectDiagnosa[]",$arrSelected , $arrSelected , 'id="selectDiagnosa'.$number.'" class="form-control select-diagnosa" disabled="" onchange="checkDiagnosaBPJS('.$numberDiagnosa.')"').'
									 '.form_input("diagnosaBPJS[]" , $strDiag , 'class="form-control" id="diagnosaBPJS_'.$number.'" readonly=""').'
									 </div>
									 </div>';

					$addDiagnosa .=  '<div class="col-sm-9 col-sm-offset-3"><div class="form-group text-left" id="contBtn'.$number.'"><button disabled="" class="btn btn-default btn-flat" style="margin-left:10px;" onclick="hapusForm('.$number.')">Hapus Diagnosa '.$number.'</button></div></div>';

					$number++;

				}

				$dt['frm_no_diagosa'] = $this->form_builder->inputHidden("noDiagnosa" , $number);
			}
		}


		if(!empty($detailTindakan)){

			$dataTindakan = array_slice($detailTindakan,0,1);

			$listTindakan = array_slice($detailTindakan,1,count($detailTindakan));

			$arrSelected = array($dataTindakan[0]['id'] => $dataTindakan[0]['txtTindakan']);

			$detailTindakan = $dataTindakan[0]['txtDeskripsiTindakan'];

			$dt['frm_rekmed_tindakan'] = $this->form_builder->inputDropdown("Tindakan" , "selectTindakan[]" ,$arrSelected, $arrSelected, $styleSelect3);
			$dt['frm_rekmed_detail_tindakan'] = $this->form_builder->inputTextArea("Detail Tindakan" , "txtDetailTindakan[]", $detailTindakan, "col-sm-3" ,$styleInput);

			$dt['frm_no_tindakan'] = $this->form_builder->inputHidden("noTindakan" , 1);

			$dt['frm_button_add_tindakan'] = 'disabled=""';

			if(count($listTindakan) > 0){

				$number = 1;

				foreach($listTindakan as $rowTindakan){

					$arrSelectedTindakan = array($rowTindakan['id'] => $rowTindakan['txtDeskripsiTindakan']);

					$addTindakan .= '<div class="form-group" id="contTindakanSelect'.$number.'">
									 <label class="col-sm-3 control-label form-label">Tindakan '.($number+1).'</label>
									 <div class="col-sm-9">
									 '.form_dropdown("selectTindakan[]",$arrSelected , $arrSelected , 'id="select-tindakan'.$number.'" class="form-control" disabled=""').'
									 </div>
									 </div>';


					$addTindakan .=  '<div class="col-sm-9 col-sm-offset-3"><div class="form-group text-left" id="contTindakanBtn'.$number.'"><button disabled="" class="btn btn-default btn-flat" style="margin-left:10px;" onclick="hapusTindakanForm('.$number.')">Hapus Tindakan '.$number.'</button></div></div>';


					$number++;

				}

				$dt['frm_no_tindakan'] = $this->form_builder->inputHidden("noTindakan" , $number);

			}

		}


		$dt['frm_add_diagosa'] = $addDiagnosa;

		$dt['frm_add_tindakan'] = $addTindakan;
		$dt['frm_add_pengobatan'] = $addPengobatan;


		///		Form Diagnosa BPJS
		$dt['frm_kode_diagnosa_bpjs'] = form_input("kdDiag1" , $kdDiag1 , 'readonly="readonly" class="form-control" id="kdDiag1"');

		$dt['frm_nama_diagnosa_bpjs'] = form_input("nmDiag1" , $txtDiag1 , 'readonly="readonly" class="form-control" id="nmDiag1"');


		///		Hanya Untuk UGD
		$frmApotik = "";

		
			$detailPelayanan = $this->pelayanan_model->getDetail($intIdPelayanan);

			$idJenisPelayanan = $detailPelayanan['intIdJenisPelayanan'];

			if($idJenisPelayanan==7){

				$arrApotik = array("1"=>"Apotik Rawat Jalan" , "2"=>"Apotik Rawat Inap/UGD");

				$frmApotik = $this->form_builder->inputDropdown("Apotik" , "selectApotik" , "", $arrApotik , $styleSelect);

			}


		

		$dataPegawai = $this->pegawai_model->getListPegawai();

		$arrPegawai = array();

		$intIdPegawai = $mode=="insert" ? $this->session->userdata("sipt_id_pegawai") : $intIdPegawai;

		foreach($dataPegawai as $rowPegawai) {

			$arrPegawai[$rowPegawai['intIdPegawai']] = $rowPegawai['txtNamaPegawai'];

		}

		$dt['frm_pegawai'] = $this->form_builder->inputDropdown("Petugas" , "intIdPegawai" , $intIdPegawai , $arrPegawai , $styleSelect);

		$dt['frm_apotik_list'] = $frmApotik;
		$dt['frm_button_add_pengobatan'] = $obatButton;

		$dt['frm_rekmed_mode'] = $mode;
		$dt['link_pernyataan'] = $link_pernyataan;
		$dt['link_rekmed'] = $this->base_url."detail-rekam-medis/".$idRekamMedis;
		$dt['link_cetak'] = $this->base_url."cetak-detail-rekam-medis/".$idRekamMedis;
		$dt['link_cetak_frm'] = $this->form_builder->inputHidden('link_cetak',$this->base_url."cetak-detail-rekam-medis/".$idRekamMedis);
		$dt['link_cetak_detail'] = $this->base_url."cetak-detail-rekam-medis/".$idRekamMedis.'/'.$txtIdRekmedDetail;
		
		$retVal = $this->load->view("rekam_medis/form" , $dt , true);

		return $retVal;

	}


	public function getFormRekamMedis(){

		$this->isAjaxRequest();

		$idRekamMedis = $this->input->post('idRekamMedis');

		$idPasien = $this->input->post('idPasien');

		$idDetailRekamMedis ="";

		$modeForm = $this->input->post('modeForm');

		if($modeForm=='kunjungan'){

			$idKunjungan = $this->input->post('idKunjungan');

			$idPelayanan = $this->input->post('idPelayanan');

			$dataDetailRekmed = $this->rekam_medis->getDetailByIdKunjungan($idKunjungan);


			$idDetailRekamMedis = $dataDetailRekmed['txtIdRekmedDetail'];

		}
		else{

			$idDetailRekamMedis = $this->input->post('idDetailRekamMedis');

		}

		$retVal = $this->_get_form_poli_umum($idRekamMedis , $idDetailRekamMedis , $idPasien , $idPelayanan , $modeForm);

		echo $retVal;

	}




	public function getFormKondisiPasien(){


		$this->isAjaxRequest();

		$idRekamMedis = $this->input->post('idRekamMedis');

		$idPasien = $this->input->post('idPasien');

		$detailKondisi = $this->rekam_medis_kondisi->getDetail($idRekamMedis);

		$mode = !empty($detailKondisi) ? "update" : "insert";

		$arrayInput = array(
		"intTekananDarah",
		"intNadi",
		"intSuhu",
		"intNafas",
		"intSkorNyeri",
		"intSkorJatuh",
		"intBerat",
		"intTinggi",
		"intLingkarKepala",
		"intIMT",
		"intLenganAtas",
		"intFungsional",
		"txtStatusPsikologi",
		"txtHambatanEdukasi",
		"txtKeteranganHambatan1",
		"txtKeteranganHambatan2",
		"txtAlergi",
		"txtRiwayatPenyakitDalam",
		"txtRiwayatPenyakitSekarang",
		"txtRiwayatPenyakitDulu",
		"txtStatusObsetri",
		"txtHPHT",
		"txtTP",
		"txtKeterangan"
		);

		$dt = array();

		$dt['arrIntFungsional'] = array(
		0=>"Tidak Ada",
		1=>"Alat Bantu",
		2=>"Prothesa",
		3=>"Cacat Utuh",
		4=>"ADL - Mandiri",
		5=>"ADL - Dibantu",
		);

		foreach ($arrayInput as $key => $value) {

			# code...
			$dt[$value] = isset($detailKondisi[$value]) ? $detailKondisi[$value] : "";

		}

		$dt['frmIdRekamMedis'] = $this->form_builder->inputHidden("txtidRekamMedisKondisi" , $idRekamMedis);

		$dt['frmModeRekamMedis'] = $this->form_builder->inputHidden("txtMode" , $mode);
		$dt['mode'] = $mode;
		$dt['link_cetak'] = $this->base_url."cetak-Kondisi-rekam-medis/".$idRekamMedis;

		$retVal = $this->load->view("rekam_medis/form_kondisi" , $dt , true);

		echo $retVal;

	}


	public function getFormKehamilan(){

		$this->isAjaxRequest();

		$dt = array();

		///		Indeks Pemeriksaan
		$this->form_builder->form_type = "inline";

		$dt['intIdRekamMedisIbu'] = $this->form_builder->inputHidden("intIdRekamMedisIbu" , "");

		$dt['txtTanggalPemeriksaan'] = $this->form_builder->inputText("Tanggal Pemeriksaan" , "txtTanggalPemeriksaanIbu" , date("Y-m-d") , "col-sm-4" );

		$dt['txtKehamilan'] = $this->form_builder->inputText("Kehamilan Ke" , "txtKehamilanKe" , "" , "col-sm-4" );

		$dt['txtNoIndeks'] = $this->form_builder->inputText("No Indeks / Kode" , "txtNoIndeks" , "" , "col-sm-4" );

		$dt['txtStatusPemeriksaan'] = $this->form_builder->inputDropdown("Status Pemeriksaan" , "bitStatusPeriksa" , "" , $this->config->item("status_list") , array() , "col-sm-4");

		$ret = $this->load->view("rekam_medis/form_ibu" , $dt , true);

		echo $ret;

	}


	public function saveKondisiPasien(){

		$this->isAjaxRequest();

		///		echopre($_POST);

		$modeForm = $this->input->post("txtMode");

		$idKondisi = $this->input->post("txtidRekamMedisKondisi");

		$txtStatusPsikologi = $this->input->post("txtStatusPsikologi");

		if(!empty($txtStatusPsikologi)){

			$txtStatusPsikologi = implode("|" , $txtStatusPsikologi);

		}


		$txtHambatanEdukasi = $this->input->post("txtHambatanEdukasi");

		$txtKeteranganHambatan = $this->input->post("txtKeteranganHambatan");

		if(!empty($txtHambatanEdukasi)){

			$txtHambatanEdukasi = implode("|" , $txtHambatanEdukasi);

		}
		else{

			$txtHambatanEdukasi = "";

		}


		$arrKondisiPasien = array(
		"txtIdRekamMedis" => $this->input->post("txtidRekamMedisKondisi"),
		"intTekananDarah" => $this->input->post("intTekananDarah"),
		"intNadi" => $this->input->post("intNadi"),
		"intSuhu" => $this->input->post("intSuhu"),
		"intNafas" => $this->input->post("intNafas"),
		"intSkorNyeri" => $this->input->post("intSkorNyeri"),
		"intSkorJatuh" => $this->input->post("intSkorJatuh"),
		"intBerat" => $this->input->post("intBerat"),
		"intTinggi" => $this->input->post("intTinggi"),
		"intLingkarKepala" => $this->input->post("intLingkarKepala"),
		"intIMT" => $this->input->post("intIMT"),
		"intLenganAtas" => $this->input->post("intLenganAtas"),
		"intFungsional" => $this->input->post("intFungsional"),
		"txtStatusPsikologi" => $txtStatusPsikologi,
		"txtHambatanEdukasi" => $txtHambatanEdukasi,
		"txtKeteranganHambatan1" => $txtKeteranganHambatan[0],
		"txtKeteranganHambatan2" => $txtKeteranganHambatan[1],
		"txtAlergi" => $this->input->post("txtAlergi"),
		"txtRiwayatPenyakitDalam" => $this->input->post("txtRiwayatPenyakitDalam"),
		"txtRiwayatPenyakitDulu" => $this->input->post("txtRiwayatPenyakitDulu"),
		"txtRiwayatPenyakitSekarang" => $this->input->post("txtRiwayatPenyakitSekarang"),
		"txtStatusObsetri" => $this->input->post("txtStatusObsetri"),
		"txtHPHT" => $this->input->post("txtHPHT"),
		"txtTP" => $this->input->post("txtTP"),
		"txtKeterangan" => $this->input->post("txtKeterangan"),
		);

		//echopre($arrKondisiPasien);
		if($modeForm=="insert"){

			$arrKondisiPasien['dtCreatedDate'] = date("Y-m-d H:i:s");

		}


		$arrKondisiPasien['dtLastUpdate'] = date("Y-m-d H:i:s");

		$arrKondisiPasien['bitStatusKondisi'] = 1;

		$saveData = $this->rekam_medis_kondisi->saveData($arrKondisiPasien , $idKondisi , $modeForm);

		echo json_encode($saveData);

	}
	private function inputKondisi($idRekamMedis,$arrData){

	}

	private function _get_form_poli_umum($idRekamMedis , $idDetailRekamMedis,$idPasien , $IdPelayanan="" , $modeForm="rekam-medis"){


		$detailDataRekmed = array();

		$detailTindakan = array();

		$detailDiagnosa = array();
		$rujukan = "";
		$valid = "";
		$dataObat = "";
		$detailPasien = $this->pasien_model->getDetail($idPasien);
		$detailKondisi = $this->rekam_medis_kondisi->getDetail($idRekamMedis);
		$link_pernyataan = $this->base_url."cetak-pernyataan/".$idRekamMedis;
		if(!empty($idDetailRekamMedis)){
			$link_pernyataan = $this->base_url."cetak-pernyataan/".$idRekamMedis."/".$idDetailRekamMedis;

			$detailDataRekmed = $this->rekam_medis->getDetailRekamMedisByIdDetail($idRekamMedis, $idDetailRekamMedis);
			$detailDiagnosa = $this->rekam_medis->getDetailDiagnosa($idDetailRekamMedis);
			$detailPengobatan = $this->rekam_medis->getDataResep($idDetailRekamMedis);

			$dataObat = $detailPengobatan;
			$detailTindakan = $this->rekam_medis->getDetailTindakan($idDetailRekamMedis);
			$rujukan = $detailDataRekmed['txtRefIdKunjungan'];
			if ((empty($detailPengobatan)) && (!empty($detailDataRekmed['txtPengobatan']))) {
                $valid = "kosong";
            }else{
                $valid = "";
            }

		}

		$arrayInput = array(
		"intBerat",
		"intTinggi",
		);



		$intIdJaminanKesehatan = $detailPasien['intIdJaminanKesehatan'];

		$dtTanggalLahir = $detailPasien['dtTanggalLahir'];

		$valArray = array(
		"txtIdKunjungan" ,
		"txtIdRekmedDetail" ,
		"intIdPelayanan" ,
		"namaPoli" ,
		"dtTanggalKunjungan",
		"txtPemeriksaan" ,
		"txtAnamnesa" ,
		"txtTinggi" ,
		"txtBerat" ,
		"txtSistole" ,
		"txtDiastole" ,
		"txtOdontogram",
		"bitSetuju",
		"txtRespiratoryRate" ,
		"txtHeartRate" ,
		"txtKesadaran",
		"txtTindakan" ,
		"noKunjungan" ,
		"kdDiag1" ,
		"kdDiag2" ,
		"kdDiag3" ,
		"txtDiag1" ,
		"txtDiag2" ,
		"txtDiag3" ,
		"intIdPegawai",
		"txtKeterangan" ,
		"txtRencanaTindakan" ,
		"txtTandaTangan",
		"txtPengobatan",
		"txtRefIdKunjungan",
		"txtHasilRujukan");


		$arrSelected = array();

		foreach($valArray as $arrFrmMedis) {

			$$arrFrmMedis = isset($detailDataRekmed[$arrFrmMedis]) ? $detailDataRekmed[$arrFrmMedis] : "";

		}
		$kondisi = array();
		foreach ($arrayInput as $key => $value) {

			$$value = isset($detailKondisi[$value]) ? $detailKondisi[$value] : "";

		}

		$detailPelayanan = $this->pelayanan_model->getDetail($IdPelayanan);

		$intIdJenisPelayanan = $detailPelayanan['intIdJenisPelayanan'];


		if(!empty($txtIdRekmedDetail)){

			$styleInput = array("rows"=>3 , "disabled"=>"disabled");

			$styleRawInput = "disabled=''";
			
			$styleDisabled = "disabled='true'";

			$styleRawInput = "disabled=''";

			$styleSelect = array("disabled"=>"disabled");

			$styleSelect2 = array("class"=>"form-control select-diagnosa","disabled"=>"disabled" , "id"=>"selectDiagnosa1" , "onchange" => "checkDiagnosaBPJS(1)");

			$styleSelect3 = array("class"=>"form-control" , "id"=>"select-tindakan","disabled"=>"disabled");
			$styleSelect4 = array("class"=>"form-control" , "id"=>"select-pengobatan","disabled"=>"disabled");

			$mode = "edit";

		}
		else{

			$styleInput = array("rows"=>3 );

			$styleSelect = array();

			$styleSelectOd = array("class"=>"form-control select-odontogram");

			$styleRawInput = "";
			
			$styleDisabled = "";

			$styleSelect2 = array("class"=>"form-control select-diagnosa" , "id"=>"selectDiagnosa1" , "onchange" => "checkDiagnosaBPJS(1)");

			$styleSelect3 = array( "id"=>"select-tindakan");

			$styleSelect4 = array( "id"=>"select-pengobatan");

			$mode = "insert";

		}

		$data_pelayanan = $this->pelayanan_model->getListDataPelayanan();

		$arrPelayanan = array();

		foreach ($data_pelayanan as $indexPelayanan => $valuePelayanan) {

			$arrPelayanan[$valuePelayanan['intIdPelayanan']] = $valuePelayanan['txtNama'];

		}
		$obatButton = '';
		$arraySelectLayanan = !empty($intIdPelayanan) ? array($intIdPelayanan => $namaPoli) : array();
		$dt['pengobatan_lama'] = $valid;
		$dt['dataObat'] = $dataObat;
		$dtTanggalKunjungan = !empty($dtTanggalKunjungan) ? $dtTanggalKunjungan : date("Y-m-d");
		$dt['setuju'] = $bitSetuju==1? TRUE : FALSE;
		$dt['tidak_setuju'] = $bitSetuju==0? TRUE : FALSE;
		$dt['frm_id_rekam_medis'] = $this->form_builder->inputHidden("txtIdRekamMedis" , $idRekamMedis);

		$dt['frm_id_pasien'] = $this->form_builder->inputHidden("txtIdPasien" , $idPasien);

		$dt['frm_id_kunjungan'] = $this->form_builder->inputHidden("txtIdKunjungan" , $txtIdKunjungan);
		$dt['frm_no_kunjungan'] = $this->form_builder->inputHidden("noKunjungan" , $noKunjungan);

		$dt['frm_rekmed_id_detail'] = $this->form_builder->inputHidden("txtIdRekmedDetail" , $txtIdRekmedDetail);
		$dt['styleDisabled'] = $styleDisabled;
		$dt['frm_tanggal_lahir'] = $this->form_builder->inputHidden("dtTanggalLahir" , $dtTanggalLahir);
		$dt['frm_rekmed_rujukan'] = $this->form_builder->inputTextArea("Hasil Rujukan" , "txtHasilRujukan", $txtHasilRujukan , "col-sm-3" ,$styleInput);
		$dt['rujukan'] = $this->form_builder->inputHidden("rujukan" , $rujukan);
		if($modeForm=='kunjungan'){

			$dt['frm_tgl_rekam_medis'] = $this->form_builder->inputHidden("txtTanggalPeriksa" , date("Y-m-d H:i:s"));

			$dt['frm_rekmed_pelayanan'] = $this->form_builder->inputHidden("txtIdPelayanan" , $IdPelayanan);

			$dt['frm_rekmed_keterangan'] = $this->form_builder->inputTextArea("Keterangan / Tindak lanjut" , "txtKeterangan", $txtKeterangan , "col-sm-3" ,$styleInput);
			$dt['mode_form_Rekmed'] = $this->form_builder->inputHidden("modeRekmed",'') ;

		}
		else{

			$dt['frm_tgl_rekam_medis'] = $this->form_builder->inputText("Tanggal Rekam Medis" , "txtTanggalPeriksa", $dtTanggalKunjungan, "col-sm-3",$styleInput);

			$dt['frm_rekmed_pelayanan'] = $this->form_builder->inputDropdown("Jenis Pelayanan" , "txtIdPelayanan" ,$intIdPelayanan, $arrPelayanan,$styleSelect);

			$dt['frm_rekmed_keterangan'] = $this->form_builder->inputTextArea("Keterangan / Tindak lanjut" , "txtKeterangan", $txtKeterangan , "col-sm-3" ,$styleInput);
			$dt['mode_form_Rekmed'] = $this->form_builder->inputHidden("modeRekmed",'TRUE') ;
		}

		///		/ Detail Pemeriksaan
		$dt['frm_rekmed_anamnesa'] = $this->form_builder->inputTextArea("Anamnesis" , "txtAnamnesa", $txtAnamnesa, "col-sm-3" ,$styleInput);

		$dt['frm_rekmed_tinggi_badan'] = form_input("txtTinggi" , !empty($intTinggi) ? $intTinggi :  $txtTinggi , 'class="form-control" id="txtTinggi"'.$styleRawInput);

		$dt['frm_rekmed_berat_badan'] = form_input("txtBerat" , !empty($intBerat) ? $intBerat : $txtBerat , 'class="form-control" id="txtBerat"'.$styleRawInput);

		$dt['frm_rekmed_sistole'] = form_input("txtSistole" , !empty($intTekananDarah)?$txtSistole: $txtSistole , 'class="form-control" id="txtSistole"'.$styleRawInput);

		$dt['frm_rekmed_diastole'] = form_input("txtDiastole" , !empty($intTekananDarah)? $txtDiastole : $txtDiastole , 'class="form-control" id="txtDiastole"'.$styleRawInput);

		$dt['frm_rekmed_respiratory_rate'] = form_input("txtRespiratoryRate" , $txtRespiratoryRate , 'class="form-control" id="txtRespiratoryRate"'.$styleRawInput);

		$dt['frm_rekmed_heart_rate'] = form_input("txtHeartRate" , $txtHeartRate , 'class="form-control" id="txtHeartRate"'.$styleRawInput);

		$kesadaran_list = $this->config->item("kesadaran_list");

		$dt['frm_rekmed_kesadaran'] = form_dropdown("txtKesadaran", $kesadaran_list , $txtKesadaran  , 'class="form-control" id="txtKesadaran"'.$styleRawInput);

		$dt['frm_ttd'] = form_hidden("inputTTD" , $txtTandaTangan);

		$dt['txtTandaTangan'] = $txtTandaTangan;

		$dt['frm_rekmed_pemeriksaan'] = $this->form_builder->inputTextArea("Pemeriksaan Umum" , "txtPemeriksaan", $txtPemeriksaan , "col-sm-3",$styleInput);
		$dt['frm_rekmed_pengobatan_lama'] = $this->form_builder->inputTextArea("Pengobatan" , "txtPengobatan", $txtPengobatan , "col-sm-3",$styleInput);

		$frmAddOthers = "";

		///		Khususon Poli Gigi
		if($intIdJenisPelayanan=='3'){

			$statusOdontogram = $this->config->item("status_odontogram");

			$rangeRome = $this->config->item("status_odontogram_rome");

			$arrSelectedOdontogram = array();


			foreach($statusOdontogram as $keyOdontogram => $odontogram){

				for ($i = 1; $i <= 8; $i++){

					$arrSelectedOdontogram[$keyOdontogram."_".$i] = $odontogram." ".$i;

				}

			}


			foreach($statusOdontogram as $keyOdontogram => $odontogram){

				for ($i = 1; $i <= 5; $i++){

					$arrSelectedOdontogram[$keyOdontogram."_".$rangeRome[$i]] = $odontogram." ".$rangeRome[$i];

				}

			}

			$txtOdontogramVal = !empty($txtOdontogram) ? $arrSelectedOdontogram[$txtOdontogram] : "";
			////$frmAddOthers .= $this->form_builder->inputDropdown("Status Odontogram" , "selectOdontogram" , $txtOdontogram , $arrSelectedOdontogram , $styleSelect);
			$frmAddOthers = '<div class="form-group">
								<label for="selectOdontogram" class="col-sm-3 control-label form-label" id="lblselectOdontogram">Status Odontogram</label>
								<div class="col-sm-9">
									<div class="input-group">
									'.form_input("txtOdontogram" , $txtOdontogramVal , 'class="form-control" id="txtOdontogram" readonly=""').'
									'.form_hidden('selectOdontogram' , $txtOdontogram).'
									<span class="input-group-btn">
										<button type="button" class="btn btn-info btn-flat" onclick="checkOdontogram()">Pilih Odontogram</button>
									</span>
									</div>
								</div>
							 </div>';

		}

		$dt['frm_add_others'] = $frmAddOthers;

		$addDiagnosa = "";

		$addTindakan = "";

		$addPengobatan = "";


		///$dt['frm_rekmed_diagnosa'] = $this->form_builder->inputDropdown("Diagnosa Penyakit" , "selectDiagnosa[]" ,$arrSelected , $arrSelected , $styleSelect2);
		///$dt['frm_rekmed_diagnosa'] = $this->form_builder->inputDropdown("Diagnosa Penyakit" , "selectDiagnosa[]" ,$arrSelected , $arrSelected , $styleSelect2);
		$dt['frm_rekmed_diagnosa'] = form_dropdown("selectDiagnosa[]" , $arrSelected  , $arrSelected , $styleSelect2);
		$dt['frm_rekmed_bpjs'] = form_input("diagnosaBPJS[]" , "", 'placeholder="Diagnosa BPJS" class="form-control" id="diagnosaBPJS_1" readonly=""');

		$dt['intIdJaminanKesehatan'] = $intIdJaminanKesehatan;

		$dt['frm_rekmed_detail_diagnosa'] = $this->form_builder->inputTextArea("Detail Diagnosa" , "txtDetailDiagnosa[]", "", "col-sm-3" ,$styleInput);

		$dt['frm_no_diagosa'] = $this->form_builder->inputHidden("noDiagnosa" , 1);

		$dt['frm_button_add_diagnosa'] = '';

		$dt['frm_no_pengobatan'] = $this->form_builder->inputHidden("noPengobatan" , 1);

		$dt['frm_button_add_pengobatan'] = '';


		$dt['frm_rekmed_tindakan'] = $this->form_builder->inputDropdown("Tindakan" , "selectTindakan[]" ,$arrSelected , $arrSelected , $styleSelect3);

		$dt['frm_rekmed_jumlah'] = $this->form_builder->inputText("Jumlah" , "txtPengobatan[]", "", "col-sm-3" ,$styleInput);

		$dt['frm_rekmed_detail_tindakan'] = $this->form_builder->inputTextArea("Detail Tindakan" , "txtDetailTindakan[]", "", "col-sm-3" ,$styleInput);

		$dt['frm_no_tindakan'] = $this->form_builder->inputHidden("noTindakan" , 1);

		$dt['frm_button_add_tindakan'] = '';

		$dt['frm_rencana_tindakan'] = $this->form_builder->inputTextArea("Rencana Tindakan" , "txtRencanaTindakan", $txtRencanaTindakan , "col-sm-3" ,$styleInput);

		if(!empty($detailDiagnosa)){

			$arrSelected = array();

			$dataDiagnosa = array_slice($detailDiagnosa,0,1);

			$listDiagnosa = array_slice($detailDiagnosa,1,count($detailDiagnosa));

			$arrSelected = array($dataDiagnosa[0]['id'] => $detailDiagnosa[0]['txtIndonesianName']);

			$detailDiagnosa = $dataDiagnosa[0]['txtDetailDiagnosa'];

			$dt['frm_rekmed_diagnosa'] = form_dropdown("selectDiagnosa[]" , $arrSelected  , $arrSelected , $styleSelect2);
			$dt['frm_rekmed_bpjs'] = form_input("diagnosaBPJS[]" , $kdDiag1."-".$txtDiag1, 'class="form-control" id="diagnosaBPJS_1" readonly=""');
			$dt['frm_no_diagosa'] = $this->form_builder->inputHidden("noDiagnosa" , 1);
			$dt['frm_button_add_diagnosa'] = 'disabled=""';
			///			Diagnosa BPJS

			$intIdJaminanKesehatan = $detailPasien['intIdJaminanKesehatan'];

			if(($intIdJaminanKesehatan==1) || ($intIdJaminanKesehatan==3) || ($intIdJaminanKesehatan==4)){

				$dt['frm_rekmed_diagnosa_bpjs'] = $this->form_builder->inputText("Diagnosa Penyakit - BPJS" , "txtDiagnosaBPJS" ,"" , "col-sm-3" , $styleInput);

			}

			if(count($listDiagnosa) > 0){

				$number = 1;

				foreach($listDiagnosa as $rowDiagnosa){

					$numberDiagnosa = $number + 1;
					$strDiag = "";
					switch($numberDiagnosa){
						case 2 : $strDiag = $kdDiag2."-".$txtDiag2;break;
						case 3 : $strDiag = $kdDiag3."-".$txtDiag3;break;
						default : $strDiag="";
					}
					$arrSelected = array($rowDiagnosa['id'] => $rowDiagnosa['txtIndonesianName']);

					$addDiagnosa .= '<div class="form-group" id="contSelect'.$number.'">
									 <label class="col-sm-3 control-label form-label">Diagnosa Penyakit '.($number+1).'</label>
									 <div class="col-sm-9">
									 '.form_dropdown("selectDiagnosa[]",$arrSelected , $arrSelected , 'id="selectDiagnosa'.$number.'" class="form-control select-diagnosa" disabled="" onchange="checkDiagnosaBPJS('.$numberDiagnosa.')"').'
									 '.form_input("diagnosaBPJS[]" , $strDiag , 'class="form-control" id="diagnosaBPJS_'.$number.'" readonly=""').'
									 </div>
									 </div>';

					$addDiagnosa .=  '<div class="col-sm-9 col-sm-offset-3"><div class="form-group text-left" id="contBtn'.$number.'"><button disabled="" class="btn btn-default btn-flat" style="margin-left:10px;" onclick="hapusForm('.$number.')">Hapus Diagnosa '.$number.'</button></div></div>';

					$number++;

				}

				$dt['frm_no_diagosa'] = $this->form_builder->inputHidden("noDiagnosa" , $number);
			}
		}


		if(!empty($detailTindakan)){

			$dataTindakan = array_slice($detailTindakan,0,1);

			$listTindakan = array_slice($detailTindakan,1,count($detailTindakan));

			$arrSelected = array($dataTindakan[0]['id'] => $dataTindakan[0]['txtTindakan']);

			$detailTindakan = $dataTindakan[0]['txtDeskripsiTindakan'];

			$dt['frm_rekmed_tindakan'] = $this->form_builder->inputDropdown("Tindakan" , "selectTindakan[]" ,$arrSelected, $arrSelected, $styleSelect3);
			$dt['frm_rekmed_detail_tindakan'] = $this->form_builder->inputTextArea("Detail Tindakan" , "txtDetailTindakan[]", $detailTindakan, "col-sm-3" ,$styleInput);

			$dt['frm_no_tindakan'] = $this->form_builder->inputHidden("noTindakan" , 1);

			$dt['frm_button_add_tindakan'] = 'disabled=""';

			if(count($listTindakan) > 0){

				$number = 1;

				foreach($listTindakan as $rowTindakan){

					$arrSelectedTindakan = array($rowTindakan['id'] => $rowTindakan['txtDeskripsiTindakan']);

					$addTindakan .= '<div class="form-group" id="contTindakanSelect'.$number.'">
									 <label class="col-sm-3 control-label form-label">Tindakan '.($number+1).'</label>
									 <div class="col-sm-9">
									 '.form_dropdown("selectTindakan[]",$arrSelected , $arrSelected , 'id="select-tindakan'.$number.'" class="form-control" disabled=""').'
									 </div>
									 </div>';


					$addTindakan .=  '<div class="col-sm-9 col-sm-offset-3"><div class="form-group text-left" id="contTindakanBtn'.$number.'"><button disabled="" class="btn btn-default btn-flat" style="margin-left:10px;" onclick="hapusTindakanForm('.$number.')">Hapus Tindakan '.$number.'</button></div></div>';


					$number++;

				}

				$dt['frm_no_tindakan'] = $this->form_builder->inputHidden("noTindakan" , $number);

			}

		}


		$dt['frm_add_diagosa'] = $addDiagnosa;

		$dt['frm_add_tindakan'] = $addTindakan;
		$dt['frm_add_pengobatan'] = $addPengobatan;


		///		Form Diagnosa BPJS
		$dt['frm_kode_diagnosa_bpjs'] = form_input("kdDiag1" , $kdDiag1 , 'readonly="readonly" class="form-control" id="kdDiag1"');

		$dt['frm_nama_diagnosa_bpjs'] = form_input("nmDiag1" , $txtDiag1 , 'readonly="readonly" class="form-control" id="nmDiag1"');


		///		Hanya Untuk UGD
		$frmApotik = "";

		if($modeForm=="kunjungan"){


			$detailPelayanan = $this->pelayanan_model->getDetail($IdPelayanan);

			$idJenisPelayanan = $detailPelayanan['intIdJenisPelayanan'];

			if($idJenisPelayanan==7){

				$arrApotik = array("1"=>"Apotik Rawat Jalan" , "2"=>"Apotik Rawat Inap/UGD");

				$frmApotik = $this->form_builder->inputDropdown("Apotik" , "selectApotik" , "", $arrApotik , $styleSelect);

			}


		}

		$dataPegawai = $this->pegawai_model->getListPegawai();

		$arrPegawai = array();

		$intIdPegawai = $mode=="insert" ? $this->session->userdata("sipt_id_pegawai") : $intIdPegawai;

		foreach($dataPegawai as $rowPegawai) {

			$arrPegawai[$rowPegawai['intIdPegawai']] = $rowPegawai['txtNamaPegawai'];

		}

		$dt['frm_pegawai'] = $this->form_builder->inputDropdown("Petugas" , "intIdPegawai" , $intIdPegawai , $arrPegawai , $styleSelect);

		$dt['frm_apotik_list'] = $frmApotik;
		$dt['frm_button_add_pengobatan'] = $obatButton;

		$dt['frm_rekmed_mode'] = $mode;
		$dt['link_pernyataan'] = $link_pernyataan;
		$dt['link_rekmed'] = $this->base_url."detail-rekam-medis/".$idRekamMedis;
		$dt['link_cetak'] = $this->base_url."cetak-detail-rekam-medis/".$idRekamMedis;
		$dt['link_cetak_detail'] = $this->base_url."cetak-detail-rekam-medis/".$idRekamMedis.'/'.$txtIdRekmedDetail;
		$retVal = $this->load->view("rekam_medis/form_umum" , $dt , true);

		return $retVal;

	}


	private function _detail($id){


	/*	$detailRekmed = $this->rekam_medis->detail($id);

		if(empty($detailRekmed)){

			show_404();

		}*/

		if(empty($id)){

			show_404();

		}

		//$idPasien = $detailRekmed['txtIdPasien'];

		$dataPasien = $this->pasien_model->getDetail($id);
		$detailRekmed = $this->rekam_medis->getDataRekamMedisByIdPasien($id);

		$dt = array();

		$breadcrumbs = array(
		"Home" =>$this->base_url_site,
		"Rekam Medis" => $this->base_url,
		"Detail Rekam Medis Pengguna Layanan" => "#",
		);


		///		Build Form Detail Rekam Medis
		$jenisKelamin = $dataPasien['charJkPasien']=="L" ? "Laki-Laki" : "Perempuan";

		$dt['frm_id_rekam_medis'] = $this->form_builder->inputHidden("txtIdRekamMedis",$dataPasien['txtIdPasien']);

		$dt['frm_id_pasien'] = $this->form_builder->inputHidden("inputIdPasien",$dataPasien['txtIdPasien']);

		$dt['frm_no_anggota_pasien'] = $this->form_builder->inputText("No Anggota" , "txtNoAnggota", $dataPasien['txtNoAnggota'] , "col-sm-4" , array("readonly"=>"readonly"));

		$dt['frm_nama_pasien'] = $this->form_builder->inputText("Nama Pengguna Layanan" , "txtNamaPasien", $dataPasien['txtNamaPasien'] , "col-sm-4" , array("readonly"=>"readonly"));

		$dt['frm_nama_kk'] = $this->form_builder->inputText("Nama Kepala Keluarga" , "txtNamaKepKk", $dataPasien['txtNamaKepKk'] , "col-sm-4" , array("readonly"=>"readonly"));

		$dt['frm_jenis_kelamin'] = $this->form_builder->inputText("Jenis Kelamin" , "txtJenisKelamin", $jenisKelamin , "col-sm-4" , array("readonly"=>"readonly"));

		$dt['frm_pekerjaan'] = $this->form_builder->inputText("Pekerjaan" , "txtPekerjaan", $dataPasien['txtPekerjaan'] , "col-sm-4" , array("readonly"=>"readonly"));

		$dt['frm_agama'] = $this->form_builder->inputText("Agama" , "txtAgama", ucwords($dataPasien['txtAgama']), "col-sm-4" , array("readonly"=>"readonly"));

		$dt['frm_alamat'] = $this->form_builder->inputText("Alamat" , "txtAlamatPasien", $dataPasien['txtAlamatPasien'] , "col-sm-4" , array("readonly"=>"readonly"));

		$dt['frm_no_rekam_medis'] = $this->form_builder->inputText("No Rekam Medis" , "txtNoRekamMedis", $detailRekmed['txtNoRekamMedis'], "col-sm-4" );

		///		Build

		$dt['breadcrumbs'] = $this->setBreadcrumbs($breadcrumbs);

		$dt['title'] = "Detail Rekam Medis";

		$dt['link_add_rekam_medis_umum'] = $this->base_url."add-detail-rekam-medis/umum/".$detailRekmed['txtIdRekamMedis'];

		$retVal = $this->load->view("rekam_medis/detail" , $dt , true);

		return $retVal;

	}


	private function _data_rekam_medis(){

		$dt = array();

		$breadcrumbs = array(
		"Home" =>$this->base_url_site,
		"Rekam Medis" => $this->base_url,
		"Data Rekam Medis Pengguna Layanan" => "#",
		);

		$dt['breadcrumbs'] = $this->setBreadcrumbs($breadcrumbs);
		$dt['title'] = "Data Rekam Medis";
		$dt['link_add_rekam_medis'] = $this->base_url."add-rekam-medis/";
		$retVal = $this->load->view("rekam_medis/content" , $dt , true);
		return $retVal;

	}


	///	/ Dependency Function
	public function getDataRekamMedis($search=""){

		$this->isAjaxRequest();

		$status = false;

		$html = "";

		$length =  $this->input->post("length");

		$start = $this->input->post("start");

		$offset = ($start - 0) * $this->limit;

		$data_rekmed = $this->rekam_medis->getListPasienRekamMedis($offset , $this->limit , $search);

		$count_data = $this->rekam_medis->countListPasienRekamMedis($search);

		if($count_data > 0) {

			$retVal['draw'] = $this->input->post('draw');

			$retVal['recordsTotal'] = $count_data;

			$retVal['recordsFiltered'] = $count_data;

			$retVal['data'] = array();
			//echopre($data_rekmed);
			foreach($data_rekmed as $pasien){

				$jenisKelamin = $pasien['charJkPasien']=="L" ? "Laki-Laki" : "Perempuan";

				$btnEdit = "<a href='".$this->base_url."detail-rekam-medis/".$pasien['txtIdPasien']."' class='btn btn-info btn-flat'><i class=\"fa fa-pencil\"></i> Detail</a>";

				$btnAksi = $btnEdit;

				$retVal['data'][] = array($pasien['txtNoAnggota'],$pasien['txtNoRekamMedis'], $pasien['txtNamaPasien'], $pasien['dtTanggalLahir'] , $jenisKelamin , $btnAksi);

			}

		}
		else{

			$retVal['draw'] = $this->input->post('draw');

			$retVal['recordsTotal'] = 0;

			$retVal['recordsFiltered'] = 0;

			$retVal['data'] = array();

		}

		$this->setJsonOutput($retVal);

	}


	private function createRekamMedis($noRekamMedis , $idPasien){

		$idRekamMedis = "";

		$checkByNoRekmed = $this->rekam_medis->getCountNoRekamMedis($noRekamMedis);

		if($checkByNoRekmed > 0){

			///			Update
			$detailRekamMedis = $this->rekam_medis->getMasterByNoRekmed($noRekamMedis);

			$statusRekamMedis = $this->input->post("statusRekamMedis");

			$idRekamMedis = $detailRekamMedis['txtIdRekamMedis'];

			$arrInsertRekmed = array(
			"txtNoRekamMedis" => $noRekamMedis,
			"dtLastUpdate" =>date("Y-m-d H:i:s"),
			"bitStatusRekmed" =>1,
			);

			$retValMaster = $this->rekam_medis->updateRekamMedis($arrInsertRekmed , $idRekamMedis);

		}
		else{

			///			Insert New Rekmed
			$idRekamMedis = $this->generateIdRekmed();

			$arrInsertRekmed = array(
			"txtIdRekamMedis" => $idRekamMedis,
			"txtIdPasien" => $idPasien,
			"txtNoRekamMedis" => $this->input->post("txtNoRekamMedis"),
			"dtCreatedDate" =>date("Y-m-d H:i:s"),
			"dtLastUpdate" =>date("Y-m-d H:i:s"),
			"bitStatusRekmed" =>1,
			);


			$retValMaster = $this->rekam_medis->insertRekamMedis($arrInsertRekmed);

		}


		$retVal['id'] = $idRekamMedis;

		$retVal['status'] = $retValMaster['status'];

		return $retVal;

	}


	public function saveDataRekmed(){

		$this->isAjaxRequest();
		$postData = $this->input->post();
		$status = false;
		
		$message = "Failed To Save";
		$noRekamMedis = $this->input->post("txtNoAnggotaPasien");
		if(empty($noRekamMedis)){
			$noRekamMedis = $this->input->post("txtNoRekamMedis");
		}
		$txtIdRekamMedis = $this->input->post("txtIdRekamMedis");
		$id_kunjungan = $this->input->post("txtIdKunjungan");
		$mode = $this->input->post("modeInsertRekamMedis");
		$idRekmedDetail = $this->input->post("txtIdRekmedDetail");
		$checkByNoRekmed = $this->rekam_medis->getCountNoRekamMedis($noRekamMedis);
		
		$modeRekmed = $this->input->post("modeRekmed");
		
	if($checkByNoRekmed > 0){

			///			Update
			$detailRekamMedis = $this->rekam_medis->getMasterByNoRekmed($noRekamMedis);

			$statusRekamMedis = $this->input->post("statusRekamMedis");

			$idRekamMedis = $detailRekamMedis['txtIdRekamMedis'];

			$arrInsertRekmed = array(
				"txtNoRekamMedis" => $noRekamMedis,
				"dtLastUpdate" =>date("Y-m-d H:i:s"),
				"bitStatusRekmed" =>1,
			);

			$retValMaster = $this->rekam_medis->updateRekamMedis($arrInsertRekmed , $idRekamMedis);

		}
		else{

			///			Insert New Rekmed
			$idRekamMedis = $this->generateIdRekmed();
			$arrInsertRekmed = array(
			"txtIdRekamMedis" => $idRekamMedis,
			"txtIdPasien" => $this->input->post("txtIdPasien"),
			"txtNoRekamMedis" => $this->input->post("txtNoRekamMedis"),
			"dtCreatedDate" =>$this->date_now,
			"dtLastUpdate" =>$this->date_now,
			"bitStatusRekmed" =>1,
			);


			$retValMaster = $this->rekam_medis->insertRekamMedis($arrInsertRekmed);

		}

		$statusMaster = $retValMaster['status'];


		if($mode!="" && $mode="insertNoRekamMedis"){

			$retVal = array();

			$retVal['id_rekmed'] = $idRekamMedis;

			$retVal['status'] = $statusMaster;

			$retVal['message'] = "Data Berhasil Di Simpan";

			echo json_encode($retVal);
			die;

		}


		if($statusMaster){

			$txtTanggalPemeriksaan = $this->input->post('txtTanggalPeriksa');

			$txtTanggals = explode(" ", $txtTanggalPemeriksaan);

			$txtTanggalPeriksa = $txtTanggals[0];

			$txtTanggalLahirPasien = $this->input->post("dtTanggalLahir");

			$usiaPasien = getIntervalDays($txtTanggalPeriksa , $txtTanggalLahirPasien);

			$arrInsertDetailRekmed = array(
			"txtIdRekamMedis"  => $idRekamMedis,
			"txtIdKunjungan"   => $id_kunjungan,
			"intIdPelayanan"   => $this->input->post("txtIdPelayanan"),
			"intUsiaHari"   => $usiaPasien,
			"txtAnamnesa"   => $this->input->post("txtAnamnesa"),
			"txtPemeriksaan"   => $this->input->post("txtPemeriksaan"),
			"txtTinggi"   => $this->input->post("txtTinggi"),
			"txtBerat"   => $this->input->post("txtBerat"),
			"txtSistole"   => $this->input->post("txtSistole"),
			"txtDiastole"   => $this->input->post("txtDiastole"),
			"txtRespiratoryRate"   => $this->input->post("txtRespiratoryRate"),
			"txtHeartRate"   => $this->input->post("txtHeartRate"),
			"txtRencanaTindakan" => $this->input->post("txtRencanaTindakan"),
			"txtKesadaran"   => $this->input->post("txtKesadaran"),
			"txtHasilRujukan" => $this->input->post("txtHasilRujukan"),
			"txtKeterangan"   => $this->input->post("txtKeterangan"),
			"txtOdontogram"   => $this->input->post("selectOdontogram"),
			"bitSetuju"   => $this->input->post("pernyataan"),
			"txtPengobatan"   => !is_array($this->input->post("txtPengobatan")) ? $this->input->post("txtPengobatan") : "",
			"intIdPegawai"   => $this->input->post("intIdPegawai"),
			"dtTanggalPemeriksaan" => $txtTanggalPemeriksaan,
			"txtTandaTangan" => '',
			"bitStatusRekmedDetail"   => 1,
			);

			

			if(empty($idRekmedDetail)){

				$arrInsertDetailRekmed['dtCreatedDate'] = $txtTanggalPemeriksaan!="" ? $txtTanggalPemeriksaan : $this->date_now;

				$arrInsertDetailRekmed['dtLastUpdate'] = $this->date_now;

				$retValDetail = $this->rekam_medis->insertDetailRekamMedis($arrInsertDetailRekmed);

			}
			else{

				$arrInsertDetailRekmed['dtLastUpdate'] = $this->date_now;

				$retValDetail = $this->rekam_medis->updateDetailRekamMedis($arrInsertDetailRekmed, $idRekmedDetail);

			}

		}

		$idDetailRekamMedis = $retValDetail['id'];

		$statusDetail =  $retValDetail['status'];

		$diagnosa = $this->input->post("selectDiagnosa");

		$detailDiagnosa = $this->input->post("txtDetailDiagnosa");

		if(($statusDetail==true) && (!empty($diagnosa))){

			$idDetailRekmed = $retValDetail['id'];

			$checkDiagnosa = $this->rekam_medis->checkDetailDiagnosa($idDetailRekmed);

			if($checkDiagnosa > 0){

				$hapusDiagnosa = $this->rekam_medis->deleteDetailDiagnosa($idDetailRekmed);

			}

			$arrBatchDiagnosa = array();
			if(is_array($diagnosa)){

				for ($indexDiagnosa=0;$indexDiagnosa < count($diagnosa);$indexDiagnosa++){

					if(!empty($diagnosa[$indexDiagnosa])){

						$checkNewCase = $this->rekam_medis->checkNewCaseDetailDiagnosa($idRekamMedis , $diagnosa[$indexDiagnosa]);

						$arrInsertDetailDiagnosa = array(
						"txtIdRekmedDetail"   => $idDetailRekmed,
						"intIdDiagnosaPenyakit" => $diagnosa[$indexDiagnosa],
						"dtCreatedDate"   => date("Y-m-d H:i:s"),
						"bitIsKasusBaru" => $checkNewCase==true ? 1 : 0,
						);

						$arrBatchDiagnosa[] = $arrInsertDetailDiagnosa;
					}

				}
				$retValDiagnosa = $this->rekam_medis->insertBatchDiagnosa($arrBatchDiagnosa);
			}
		}

		///		Insert Data Tindakan
		$tindakan = $this->input->post("selectTindakan");

		//$deskripsi_tindakan = $this->input->post("txtDetailTindakan");

		if(!empty($tindakan)){

			$idDetailRekmed = $retValDetail['id'];

			$checkTindakan = $this->rekam_medis->checkDetailTindakan($idDetailRekmed);

			if($checkTindakan > 0){

				$hapusTindakan = $this->rekam_medis->deleteDetailTindakan($idDetailRekmed);

			}
			$arrInsertTindakan = array();
			if(is_array($tindakan)){

				for ($indexTindakan=0;$indexTindakan < count($tindakan);$indexTindakan++){

					$arrInsertDetailTindakan = array(
					"txtIdRekmedDetail"   => $idDetailRekamMedis,
					"intIdTindakan" => $tindakan[$indexTindakan],
					"txtDeskripsiTindakan" => '',
					);

					$arrInsertTindakan[] = $arrInsertDetailTindakan;

					////$retValTindakan = $this->rekam_medis->insertDetailTindakan($arrInsertDetailTindakan);

				}

			}

			$retValTindakan = $this->rekam_medis->insertBatchTindakan($arrInsertTindakan);

		}


		$Pengobatan = $this->input->post("selectPengobatan");
		$jumlah = $this->input->post("txtPengobatan");
		$dosis = $this->input->post("txtDosis");

		if(!empty($Pengobatan)){

			$idDetailRekmed = $retValDetail['id'];

			$checkPengobatan = $this->rekam_medis->getDataResep($idDetailRekmed);

			if($checkPengobatan > 0){

				$hapusPengobatan = $this->rekam_medis->deleteDetailPengobatan($idDetailRekmed);
				
			}
			$arrInsertPengobatan = array();
			if(is_array($Pengobatan)){
				
				for ($indexPengobatan=0;$indexPengobatan < count($Pengobatan);$indexPengobatan++){

					$arrInsertDetailPengobatan = array(
					"txtIdRekmedDetail"   => $idDetailRekmed,
					"intIdObat" => $Pengobatan[$indexPengobatan],
					"jumlah" => $jumlah[$indexPengobatan],
					"dosis" => $dosis[$indexPengobatan]
					);

					$arrInsertPengobatan[] = $arrInsertDetailPengobatan;

					////$retValPengobatan = $this->rekam_medis->insertDetailPengobatan($arrInsertDetailPengobatan);

				}
				
			}

			$retValPengobatan = $this->rekam_medis->insertBatchPengobatan($arrInsertPengobatan);

		}


		///		Save Diagnosa BPJS
		if(!empty($postData['txtNoJamkes'])){
			$this->saveDataBPJSRawatJalan($postData , $idDetailRekamMedis);
		}


		if($retValDetail['status']){

			$status = true;

			$message = "Data Berhasil Di Simpan";

		}


		$retVal = array();

		$retVal['id_rekmed'] = $idRekamMedis;

		$retVal['id_rekmed_detail'] = $idDetailRekamMedis;

		$retVal['status'] = $status;

		$retVal['message'] = $message;

		$this->session->set_flashdata("alert_rekam_medis" , $retVal);

		$this->setJsonOutput($retVal);

	}


	private function saveDataBPJSRawatJalan($post , $idDetailRekamMedis){


		$noKunjungan = !empty($post['noKunjungan']) ? $post['noKunjungan'] : null;

		$this->load->library(array(
		"bpjs/masterbpjs",
		"bpjs/kunjunganbpjs",
		));

		$this->load->model(array(
		"rekam_medis_bpjs_model",
		));

		$kodeDiagnosaBPJS = $post['diagnosaBPJS'];

		$diagnosaBPJS1 = isset($kodeDiagnosaBPJS[0]) ? extractDiagnosaBPJS($kodeDiagnosaBPJS[0]) : array();
		$diagnosaBPJS2 = isset($kodeDiagnosaBPJS[1]) ? extractDiagnosaBPJS($kodeDiagnosaBPJS[1]) : array();
		$diagnosaBPJS3 = isset($kodeDiagnosaBPJS[2]) ? extractDiagnosaBPJS($kodeDiagnosaBPJS[2]) : array();


		$kdDokter = "038";
		///		Default Constant
		$getDokter = $this->masterbpjs->getDokter(0,1);
		if($getDokter['status']==true){
			$dataDokter = $getDokter['data']['list'][0];
			$kdDokter = $dataDokter['kdDokter'];
		}

		$dateTimerRegister = explode(" " , $post['txtTanggalPeriksa']);
		$dateRegister = $dateTimerRegister[0];
		$tanggalDaftar = reverseDate($dateRegister);

		$arrPostBPJS = array(
		"noKunjungan" => $noKunjungan,
		"noKartu" => $post['txtNoJamkes'],
		"tglDaftar" => $tanggalDaftar,
		"keluhan" => $post['txtAnamnesa'],
		"kdSadar" => !empty($post['txtKesadaran']) ? $post['txtKesadaran'] : 0,
		"sistole" => !empty($post['txtSistole']) ? $post['txtSistole'] : 0,
		"diastole" => !empty($post['txtDiastole']) ? $post['txtDiastole'] : 0,
		"beratBadan" => !empty($post['txtBerat']) ? $post['txtBerat'] : 0,
		"tinggiBadan" => !empty($post['txtTinggi']) ? $post['txtTinggi'] : 0,
		"respRate" => !empty($post['txtRespiratoryRate']) ? $post['txtRespiratoryRate'] : 0,
		"heartRate" => !empty($post['txtHeartRate']) ? $post['txtHeartRate'] : 0,
		"terapi" => $post['txtKeterangan'],
		"kdProviderRujukLanjut" => null,
		"kdStatusPulang" => 3,
		"tglPulang" => $tanggalDaftar,
		"kdDokter" => $kdDokter,
		"kdDiag1" => !empty($diagnosaBPJS1) ? $diagnosaBPJS1['kdDiagnosa'] : null,
		"kdDiag2" => !empty($diagnosaBPJS2) ? $diagnosaBPJS2['kdDiagnosa'] : null,
		"kdDiag3" => !empty($diagnosaBPJS3) ? $diagnosaBPJS3['kdDiagnosa'] : null,
		"kdPoliRujukInternal" => null,
		"kdPoliRujukLanjut" => null,
		);

		if(!empty($noKunjungan)){

			///			Update API
			$resAPI = $this->kunjunganbpjs->editKunjungan($arrPostBPJS);
			$statusAPI = $resAPI['status'];
			$arrPostBPJS['dtLastUpdate'] = date("Y-m-d H:i:s");

		}
		else{

			///			Insert API
			$resAPI = $this->kunjunganbpjs->addKunjungan($arrPostBPJS);
			$statusAPI = $resAPI['status'];
			$arrPostBPJS['dtCreatedDate'] = date("Y-m-d H:i:s");
			$noKunjungan = $statusAPI==true ? $resAPI['data']['message'] : 000;
		}

		$arrPostBPJS['txtDiag1'] = !empty($diagnosaBPJS1) ? $diagnosaBPJS1['nmDiagnosa'] : null;

		$arrPostBPJS['txtDiag2'] = !empty($diagnosaBPJS2) ? $diagnosaBPJS2['nmDiagnosa'] : null;

		$arrPostBPJS['txtDiag3'] = !empty($diagnosaBPJS3) ? $diagnosaBPJS3['nmDiagnosa'] : null;

		$arrPostBPJS['noKunjungan'] = $noKunjungan;

		$arrPostBPJS['dtSendServer'] = $statusAPI==true ? date("Y-m-d H:i:s") : "";

		$arrPostBPJS['txtIdRekamMedisDetail'] = $idDetailRekamMedis;

		$arrPostBPJS['bitStatus'] = $statusAPI==true ? 1 : 0;

		$arrPostBPJS['txtMessage'] = $resAPI['message'];

		$resInsert = $this->rekam_medis_bpjs_model->insert($arrPostBPJS , $idDetailRekamMedis);


	}


	private function generateIdRekmed(){

		$rand_number = rand(1000 , 9999);

		$idrekmed = "RKM" . date("YmdHis").$rand_number;

		$len = strlen($idrekmed);

		return $idrekmed;

	}


	public function hapusRekamMedis(){

		$this->isAjaxRequest();

		$this->_checkValidateUser();

		$status = false;

		$html = "";

		$id_rekam_medis = $this->input->post("txtIdRekamMedis");

		$id_detail = $this->input->post("txtIdDetailRekamMedis");
		$pengobatan = $this->rekam_medis->getDataResep($id_detail);
		$contPengobatan = count($pengobatan);
		if($contPengobatan > 0){

			$delete_pengobatan = $this->rekam_medis->deleteDetailPengobatan($id_detail);

		}
		$delete = $this->rekam_medis->deleteDetailRekamMedis($id_rekam_medis , $id_detail);

		$count_diagnosa = $this->rekam_medis->checkDetailDiagnosa($id_detail);

		if($count_diagnosa > 0){

			$delete_diagnosa = $this->rekam_medis->deleteDetailDiagnosa($id_detail);

		}


		$retVal = array();

		$retVal['status'] = $delete['status'];

		$retVal['message'] = "Data Berhasil Di hapus";

		$this->session->set_flashdata("alert_rekam_medis" , $retVal);

		$this->setJsonOutput($retVal);

	}


	public function getRekamMedisPoliUmum($id_rekam_medis=""){

		$this->isAjaxRequest();

		$this->_checkValidateUser();

		$limit = 1000;

		$page = $this->input->post("start");
		$limit = $this->input->post("length");

		////$dataRekamMedis = $this->rekam_medis->getDetailByIdRekamMedis($id_rekam_medis , $page , $limit);
		$dataRekamMedis = $this->rekam_medis->getDetailByIdRekamMedisV2( $id_rekam_medis,$limit,$page );
		$countRekamMedis= count($this->rekam_medis->getDetailByIdRekamMedisV2( $id_rekam_medis));
		///$countRekamMedis = $this->rekam_medis->countDetailByIdRekamMedis($id_rekam_medis);

		$page_next = $page;

		$retVal= array();
		if(!empty($dataRekamMedis)){

			$retVal['draw'] = $this->input->post('draw');

			$retVal['data'] = array();
			//echopre($dataRekamMedis);
			foreach($dataRekamMedis as $rekMed){

				$idRekmedDetail = $rekMed['txtIdRekmedDetail'];

				$detailPenyakit = $this->rekam_medis->getDetailDiagnosa($idRekmedDetail);

				$detailPengobatan = $this->rekam_medis->getDataResep($idRekmedDetail);
				$pengobatan = "";
				if ((empty($detailPengobatan)) && (!empty($rekMed['txtPengobatan']))) {
				
				$pengobatan = 	"<dl>
									<dt>Pengobatan<dt><dd>".$rekMed['txtPengobatan']."</dd>
								</dl>";
			
				}else {
					$pengobatan = "<dl><dt>Pengobatan<dt></dl>";
				foreach ($detailPengobatan as  $value) {
						$pengobatan .= 	 "<dl><dd>
									".$value['nama_obat']."<br>
									Jumlah : ".$value['jumlah']."<br>
									Dosis : ".$value['dosis']."<br>
									</dd>
									</dl>";
				}
				

				}

				$no = 1;

				$diagnosa = "";

				$icd10 = "";

				foreach($detailPenyakit as $rowPenyakit){

					$diagnosa .= "<dl>
									<dt>Diagnosis<dt><dd>".$rowPenyakit['txtIndonesianName']."</dd>
								</dl>";

					$icd10 .= "<dl>
									<dt><br></dt><dd>".$rowPenyakit['txtCategory']."</dd>
								</dl>";

					$no++;

				}


				//$				tindakan = "";

				$kesadaran_list = $this->config->item("kesadaran_list");

				$tindakan = "<dl>
							<dt>Anamnesis & Pemeriksaan Fisik</dt>
							<dd>
							Anamnesis : ".$rekMed['txtAnamnesa']."</br>
							Sistole : ".$rekMed['txtSistole']." - Diastole : ".$rekMed['txtDiastole']."</br>
							Respiratory Rate : ".$rekMed['txtRespiratoryRate']." - Heart Rate : ".$rekMed['txtHeartRate']."</br>
							Kesadaran : ".$kesadaran_list[$rekMed['txtKesadaran']]."
							</br></br>
							Pemeriksaan Umum :<br> ".$rekMed['txtPemeriksaan']."</br>
							</dd>
							</dl>";

				$detailTindakan = $this->rekam_medis->getDetailTindakan($idRekmedDetail);

				if(!empty($detailTindakan)){

					foreach($detailTindakan as $rowTindakan) {

						$namaTindakan = $rowTindakan['txtTindakan'];

					
						$tindakan .= "<dl>
										<dt>Tindakan<dt><dd>".$namaTindakan."</dd>
										
									   </dl>";

					}

				}

				$ttdPasien = !empty($rekMed['txtTandaTangan']) ? '<img src="'.BASE_URL.$rekMed['txtTandaTangan'].'" width="100" alt="">' : "";
				$btnEdit = "<a href='".$this->base_url."update-detail-rekam-medis/umum/".$rekMed['txtIdRekamMedis']."/".$idRekmedDetail."' class='btn btn-info btn-xs btn-flat'><i class=\"fa fa-pencil\"></i> Detail</a>";
					$btnCetak = "<a target='blank' href='".$this->base_url."cetak-rekam-medis/".$rekMed['txtIdRekamMedis']."/".$idRekmedDetail."' class='btn btn-info btn-xs btn-flat'><i class=\"fa fa-print\"></i> Cetak</a>";

				$btnCetakSemua = "<a target='blank' href='".$this->base_url."cetak-rekam-medis/".$rekMed['txtIdRekamMedis']."' class='btn btn-default btn-xs btn-flat'><i class=\"fa fa-print\"></i> Cetak Semua</a>";
				$btnCetakDetail =  "<a target='blank' href='".$this->base_url."cetak-detail-rekam-medis/".$rekMed['txtIdRekamMedis'].'/'.$idRekmedDetail."' class='btn btn-warning btn-xs btn-flat'><i class=\"fa fa-print\"></i> Cetak</a>";

				$btnAksi = $btnEdit.$btnCetakDetail;

				$rekMed['diagnosaPasien'] = $detailPenyakit;

				$retVal['data'][] = array(indonesian_date($rekMed['dtTanggalKunjungan']),
				$rekMed['namaPoli'] ,
				$tindakan,
				$diagnosa , ///				=> Diagnosa
				$icd10 , ///				=> ICD 10
				$pengobatan ,
				$rekMed['namaPegawai'] ,
				$ttdPasien,
				$btnAksi);


			}
			$retVal['recordsTotal'] = $countRekamMedis;

			$retVal['recordsFiltered'] = $countRekamMedis;

		}
		else{

			$retVal['draw'] = $this->input->post('draw');

			$retVal['data'] = array();
			$retVal['recordsTotal'] = 0;

			$retVal['recordsFiltered'] = 0;
		}

		$this->setJsonOutput($retVal);

	}



	public function getRekamMedisInap($idRekamMedis){

		$this->isAjaxRequest();

		$start = $this->input->post("start");

		$length = $this->input->post("length");

		$dataRawatInap = $this->rekam_medis->getDetailRiwayatRawatInap($idRekamMedis , $start , $length);

		$countRawatInap = $this->rekam_medis->countDetailRiwayatInap($idRekamMedis);


		if($countRawatInap > 0){

			$retVal['draw'] = $this->input->post('draw');

			$retVal['recordsTotal'] = $countRawatInap;

			$retVal['recordsFiltered'] = $countRawatInap;

			$retVal['data'] = array();

			foreach($dataRawatInap as $rowInap) {

				$btnAksi = '<a class="btn btn-success" href="'.$this->base_url_site.'rawat-inap/detail/'.$rowInap['txtIdRekamMedis'].'/'.$rowInap['txtIdRekmedDetail'].'" target="_blank"><i class="fa fa-edit"></i> Detail</a>';

				$rowArray = array(
				$rowInap['dtTanggalKunjungan'],
				$rowInap['nama_penyakit'],
				$rowInap['txtKamar'],
				$rowInap['dtSelesaiKunjungan'],
				$btnAksi
				);

				$retVal['data'][] = $rowArray;

			}

		}
		else{

			$retVal['draw'] = $this->input->post('draw');

			$retVal['recordsTotal'] = 0;

			$retVal['recordsFiltered'] = 0;

			$retVal['data'] = array();

		}

		$this->setJsonOutput($retVal);

	}


	///	/ This Is New Era

	public function data_rekam_medis()
	{

		$menu = "K01";

	
		
	
		$dt = array(
		"title" => $this->meta_title,
		"description" => $this->meta_desc,
		"container" => $this->_build_data_rekam_medis(),
		"menu_key" => "data_rekam_medis",
		"akses_key" => "is_view",
		"custom_js" => array(
		ASSETS_URL."plugins/select2/select2.js",
		ASSETS_JS_URL."rekam_medis/data.js",
		),
		"custom_css" => array(
			ASSETS_JS_URL."rekam_medis/form.js",
			ASSETS_URL."plugins/select2/select2.css",
		),
		);

		$this->_render("default",$dt);

	}



	private function _build_data_rekam_medis(){

		$dt = array();

		$breadcrumbs = array(
		"Home" =>$this->base_url_site,
		"Rekam Medis" => $this->base_url,
		"Data Rekam Medis Pengguna Layanan" => "#",
		);


		$listPelayanan = $this->pelayanan_model->getListDataPoliRawatJalan();

		$arrPelayanan = array("0" => "Semua");

		foreach($listPelayanan as $rowPelayanan) {

			$arrPelayanan[$rowPelayanan['intIdPelayanan']] = $rowPelayanan['txtNama'];

		}


		$dt['breadcrumbs'] = $this->setBreadcrumbs($breadcrumbs);

		$dt['title'] = "Data Rekam Medis Harian";
		$arrSelected = array();
		$dt['frm_rekmed_diagnosa'] = $this->form_builder->inputDropdown("Penyakit","selectDiagnosa" , $arrSelected  , $arrSelected , array() , "col-sm-2");
			
		$dt['intIdPelayanan'] = $this->form_builder->inputDropdown("Pelayanan Asal" , "intIdPelayanan" , "" , $arrPelayanan , array() , "col-sm-2");

		$retVal = $this->load->view("rekam_medis/data" , $dt , true);

		return $retVal;

	}


	///	/
	public function getDataRekamMedisSegment(){

		$this->isAjaxRequest();


		$post = $this->input->post();

		$dtStartDate = $post['dtStartDate'] . " 00:00:00";

		$dtEndDate = $post['dtEndDate'] . " 23:59:00";

		$intIdPelayanan = $post['intIdPelayanan'];
		$penyakit = $post['selectDiagnosa'];

		$start = $post['start'];

		$length = $post['length'];


		$dataRekamMedis = $this->rekam_medis->getDataDetailRekamMedis($dtStartDate , $dtEndDate , $intIdPelayanan , $start , $length, $penyakit);
		$countRekamMedis = $this->rekam_medis->getCountDataDetailRekamMedis($dtStartDate , $dtEndDate , $intIdPelayanan, $penyakit);

		if($countRekamMedis > 0){

			$retVal['draw'] = $this->input->post('draw');

			$retVal['recordsTotal'] = $countRekamMedis;

			$retVal['recordsFiltered'] = $countRekamMedis;

			$retVal['data'] = array();

			foreach($dataRekamMedis as $rekMed){

				$idRekmedDetail = $rekMed['txtIdRekmedDetail'];

				$txtIdRekamMedis = $rekMed['txtIdRekamMedis'];

				$detailPenyakit = $this->rekam_medis->getDetailDiagnosa($idRekmedDetail);
				$detailPengobatan = $this->rekam_medis->getDataResep($idRekmedDetail);
				$pengobatan = "";
			
				if ((empty($detailPengobatan)) && (!empty($rekMed['txtPengobatan']))) {
				
				$pengobatan = 	"<dl>
									<dt>Pengobatan<dt><dd>".$rekMed['txtPengobatan']."</dd>
								</dl>";

			
				}else {
					$pengobatan = "<dl><dt>Pengobatan<dt></dl>";
				foreach ($detailPengobatan as  $value) {
						$pengobatan .= 	 "<dl><dd>
									".$value['nama_obat']."<br>
									Jumlah : ".$value['jumlah']."<br>
									Dosis : ".$value['dosis']."<br>
									</dd>
									</dl>";
				}
				
				}
				
				$no = 1;

				$diagnosa = "";

				$icd10 = "";

				foreach($detailPenyakit as $rowPenyakit){

					$diagnosa .= "<dl>
									<dt>Diagnosis<dt><dd>".$rowPenyakit['txtIndonesianName']."</dd>
								</dl>";

					$icd10 .= "<dl>
									<dt><br></dt><dd>".$rowPenyakit['txtCategory']."</dd>
								</dl>";

					$no++;

				}

				$tindakan = "<dl>
							<dt>Anamnesis & Pemeriksaan Fisik</dt>
							<dd>
							Anamnesis : ".$rekMed['txtAnamnesa']."</br>
							TD : ".$rekMed['txtSistole']."/".$rekMed['txtDiastole']."</br>
							RR : ".$rekMed['txtRespiratoryRate']." - HR : ".$rekMed['txtHeartRate']."</br>
							Pemeriksaan Umum :<br> ".$rekMed['txtPemeriksaan']."</br>
							</dd>
							</dl>";

				$detailTindakan = $this->rekam_medis->getDetailTindakan($idRekmedDetail);

				if(!empty($detailTindakan)){

					foreach($detailTindakan as $rowTindakan) {

						$namaTindakan = $rowTindakan['txtTindakan'];

					
						$tindakan .= "<dl>
										<dt>Tindakan<dt><dd>".$namaTindakan."</dd>
										   </dl>";

					}

				}

				$ttdPasien = !empty($rekMed['txtTandaTangan']) ? '<img src="'.BASE_URL.$rekMed['txtTandaTangan'].'" width="100" alt="">' : "";

				$btnEdit = "<a target='blank' href='".$this->base_url."cetak-rekam-medis/".$txtIdRekamMedis."/".$idRekmedDetail."' class='btn btn-info btn-xs btn-flat'><i class=\"fa fa-print\"></i> Cetak</a>";
				$btnCetakSemua = "<a target='blank' href='".$this->base_url."cetak-rekam-medis/".$txtIdRekamMedis."' class='btn btn-default btn-xs btn-flat'><i class=\"fa fa-print\"></i> Riwayat</a>";
				$btnCetakDetail =  "<a target='blank' href='".$this->base_url."cetak-detail-rekam-medis/".$txtIdRekamMedis.'/'.$idRekmedDetail."' class='btn btn-warning btn-xs btn-flat'><i class=\"fa fa-print\"></i> Form</a>";
				$btnAksi = $btnEdit.$btnCetakSemua.$btnCetakDetail;

				$rekMed['diagnosaPasien'] = $detailPenyakit;

				$retVal['data'][] = array(
				indonesian_date($rekMed['dtTanggalKunjungan']),
				$rekMed['namaPoli'] ,
				$tindakan,
				$diagnosa , ///				=> Diagnosa
				$icd10 , ///				=> ICD 10
				$pengobatan ,
				$rekMed['namaPegawai'] ,
				$btnAksi);

			}

		}
		else{

			$retVal['draw'] = $this->input->post('draw');

			$retVal['recordsTotal'] = 0;

			$retVal['recordsFiltered'] = 0;

			$retVal['data'] = array();

		}

		$this->setJsonOutput($retVal);


	}


	public function previewCetakRekamMedis($idRekamMedis,$intIdRekamMedisDetail = "" , $page = "0") {


		$dt = array(
		"title" => $this->meta_title,
		"description" => $this->meta_desc,
		"container" => $this->_build_cetak_rekam_medis($idRekamMedis , $intIdRekamMedisDetail , $page),
		"menu_key" => $this->menu_key,
		"akses_key" => "is_view",
		"custom_js" => array(),
		"custom_css" => array(),
		);

		$this->_render("default",$dt);

	}


	private function _build_cetak_rekam_medis($idRekamMedis,$intIdRekamMedisDetail , $page){


		$breadcrumbs = array(
		"Home" =>$this->base_url_site,
		"Rekam Medis" => $this->base_url,
		"Cetak Rekam Medis" => "#",
		);

		$detailPasien = $this->pasien_model->detailPasienByRekamMedis($idRekamMedis);

		$dt['breadcrumbs'] = $this->setBreadcrumbs($breadcrumbs);
		$dt['title'] = $this->meta_title;
		$dataRekamMedis = $this->getDetailRekamMedisByID($idRekamMedis , $intIdRekamMedisDetail , $page);
		$dt['detailRekamMedis'] =  $dataRekamMedis;
		$dt['detailPasien'] =  $detailPasien;
		$dt['link_cetakRekamMedis'] = $this->base_url_site."rekam-medis/cetak-rekam-medis/" . $idRekamMedis;
		$dt['id_detail'] = $intIdRekamMedisDetail;
		$dt['link_cetakForm'] = $this->base_url_site."rekam-medis/cetak-detail-rekam-medis/" . $idRekamMedis."/".$intIdRekamMedisDetail;
		return $this->load->view('rekam_medis/preview_cetak', $dt, true);

	}
	public function CetakDetailRekamMedis($idRekamMedis,$intIdRekamMedisDetail = "" , $page = "0") {


		$dt = array(
		"title" => $this->meta_title,
		"description" => $this->meta_desc,
		"container" => $this->_build_detail_rekam_medis($idRekamMedis , $intIdRekamMedisDetail , $page),
		"menu_key" => $this->menu_key,
		"akses_key" => "is_view",
		"custom_js" => array(),
		"custom_css" => array(),
		);

		$this->_render("default",$dt);

	}


	private function _build_detail_rekam_medis($idRekamMedis,$intIdRekamMedisDetail , $page){


		$breadcrumbs = array(
		"Home" =>$this->base_url_site,
		"Rekam Medis" => $this->base_url,
		"Cetak Rekam Medis" => "#",
		);
		if(!empty($intIdRekamMedisDetail)){

			$detailDataRekmed = $this->rekam_medis->getDetailRekamMedisByIdDetail($idRekamMedis, $intIdRekamMedisDetail);

			$detailDiagnosa = $this->rekam_medis->getDetailDiagnosa($intIdRekamMedisDetail);

			$detailTindakan = $this->rekam_medis->getDetailTindakan($intIdRekamMedisDetail);
			$detailPengobatan = $this->rekam_medis->getDataResep($intIdRekamMedisDetail);

		}
		$pelayanan = $this->pelayanan_model->getListDataPelayanan();
		$arrPelayanan = arrayDropdown($pelayanan,'intIdPelayanan','txtNama');
		$statusOdontogram = $this->config->item("status_odontogram");

			$rangeRome = $this->config->item("status_odontogram_rome");

			$arrSelectedOdontogram = array();


			foreach($statusOdontogram as $keyOdontogram => $odontogram){

				for ($i = 1; $i <= 8; $i++){

					$arrSelectedOdontogram[$keyOdontogram."_".$i] = $odontogram." ".$i;

				}

			}


			foreach($statusOdontogram as $keyOdontogram => $odontogram){

				for ($i = 1; $i <= 5; $i++){

					$arrSelectedOdontogram[$keyOdontogram."_".$rangeRome[$i]] = $odontogram." ".$rangeRome[$i];

				}

			}
		$detailPasien = $this->pasien_model->detailPasienByRekamMedis($idRekamMedis);
		$dt['odontogram'] = $arrSelectedOdontogram;
		$dt['breadcrumbs'] = $this->setBreadcrumbs($breadcrumbs);
		$dt['title'] = $this->meta_title;
		$dt['pelayanan'] = $arrPelayanan;
		$dt['detailDiagnosa'] = $detailDiagnosa;
		$dt['detailTindakan'] = $detailTindakan;
		$dt['detailPengobatan'] = $detailPengobatan;
		$dataRekamMedis = $this->getDetailRekamMedisByID($idRekamMedis , $intIdRekamMedisDetail , $page);
		$dt['detailRekamMedis'] =  $detailDataRekmed;
		$dt['detailPasien'] =  $detailPasien;
		$dt['link_cetakRekamMedis'] = $this->base_url_site."rekam-medis/cetak-rekam-medis/" . $idRekamMedis;
		$dt['id_detail'] = $intIdRekamMedisDetail;
		$dt['kesadaran'] = $this->config->item('kesadaran_list');
		$dt['link_cetak_kondisi'] = $this->base_url_site."rekam-medis/cetak-Kondisi-rekam-medis/" . $idRekamMedis;


		return $this->load->view('rekam_medis/preview_detail_cetak', $dt, true);

	}


	public function CetakKondisiRekamMedis($id) {


		$dt = array(
		"title" => $this->meta_title,
		"description" => $this->meta_desc,
		"container" => $this->_build_kondisi_rekam_medis($id),
		"menu_key" => $this->menu_key,
		"akses_key" => "is_view",
		"custom_js" => array(),
		"custom_css" => array(),
		);

		$this->_render("default",$dt);

	}


	private function _build_kondisi_rekam_medis($id){


		$breadcrumbs = array(
		"Home" =>$this->base_url_site,
		"Rekam Medis" => $this->base_url,
		"Cetak Rekam Medis" => "#",
		);

			$detailKondisi = $this->rekam_medis_kondisi->getDetail($id);


		$detailPasien = $this->pasien_model->detailPasienByRekamMedis($id);

		$dt['detailKondisi'] =  $detailKondisi;
		$dt['detailPasien'] =  $detailPasien;
		$dt['kesadaran'] = $this->config->item('kesadaran_list');
		$dt['link_cetakRekamMedis'] = $this->base_url_site."rekam-medis/cetak-rekam-medis/" ;
		$dt['breadcrumbs'] = $this->setBreadcrumbs($breadcrumbs);
		$dt['title'] = $this->meta_title;

		return $this->load->view('rekam_medis/preview_kondisi_cetak', $dt, true);

	}
	public function CetakPernyataan($idRekamMedis,$intIdRekamMedisDetail) {


		$dt = array(
		"title" => $this->meta_title,
		"description" => $this->meta_desc,
		"container" => $this->_build_pernyataan($idRekamMedis,$intIdRekamMedisDetail),
		"menu_key" => $this->menu_key,
		"akses_key" => "is_view",
		"custom_js" => array(),
		"custom_css" => array(),
		);

		$this->_render("default",$dt);

	}


	private function _build_pernyataan($idRekamMedis,$intIdRekamMedisDetail){

		$this->load->model('kecamatan_model');
		$breadcrumbs = array(
		"Home" =>$this->base_url_site,
		"Rekam Medis" => $this->base_url,
		"Cetak Pernyataan Rekam Medis" => "#",
		);
		$detailDataRekmed = $this->rekam_medis->getDetailRekamMedisByIdDetail($idRekamMedis, $intIdRekamMedisDetail);
		$detailTindakan = $this->rekam_medis->getDetailTindakan($intIdRekamMedisDetail);
		$detailPasien = $this->pasien_model->detailPasienByRekamMedis($idRekamMedis);
		$idKec = $detailPasien['intIdKecamatan'];
		$namaKec = "";
		if (!empty($idKec)) {
			$kecamatan = $this->kecamatan_model->getDetailKecamatan($idKec);
			$namaKec = $kecamatan['Nama'];
		}
		
		$dt['nama_kecamatan'] = $namaKec;
		$dt['detailTindakan'] = $detailTindakan;
		$dt['detailPasien'] =  $detailPasien;
		$dt['detailRekamMedis'] =  $detailDataRekmed;
		$dt['breadcrumbs'] = $this->setBreadcrumbs($breadcrumbs);
		$dt['title'] = $this->meta_title;

		return $this->load->view('rekam_medis/preview_pernyataan', $dt, true);

	}

	public function getFormTandaTangan(){
		$this->isAjaxRequest();

		$post = $this->input->post();
		$dt = array();

		$txtIdKunjungan = $post['txtIdKunjungan'];
		$dt['linkPluginJSignature'] = ASSETS_PLUGIN_URL . "jSignature/libs/jSignature.min.js";
		$dt['linkPluginJSignatureFlash'] = ASSETS_PLUGIN_URL . "jSignature/libs/flashcanvas.js";
		$this->load->view("rekam_medis/form_tanda_tangan" , $dt);
	}

	private function getDetailRekamMedisByID($idRekamMedis ,$intIdRekamMedisDetail , $page){

		$retData = array();
		$limit = !empty($page) ? 5 : 1000;
		$dataRekamMedis = $this->rekam_medis->getDetailByIdRekamMedis($idRekamMedis,$page , $limit ,$intIdRekamMedisDetail);
		foreach($dataRekamMedis as $rekMed) {
			$idRekmedDetail = $rekMed['txtIdRekmedDetail'];
			$detailPenyakit = $this->rekam_medis->getDetailDiagnosa($idRekmedDetail);
			$pengobatan = 	"<dl>
								<dt>Pengobatan<dt><dd>".$rekMed['txtPengobatan']."</dd>
							</dl>";

			$no = 1;
			$diagnosa = "";
			$icd10 = "";

			foreach($detailPenyakit as $rowPenyakit){
				$diagnosa .= "<dl>
								<dt>Diagnosis<dt><dd>".$rowPenyakit['txtIndonesianName']."</dd>
							</dl>";
				$icd10 .= $rowPenyakit['txtCategory'];
				$no++;

			}
			$tindakan = "<dl>
						<dt>Anamnesis & Pemeriksaan Fisik</dt>
						<dd>
						Anamnesis : ".$rekMed['txtAnamnesa']."</br>
						TD : ".$rekMed['txtSistole']."/".$rekMed['txtDiastole']."</br>
						RR : ".$rekMed['txtRespiratoryRate']." - HR : ".$rekMed['txtHeartRate']."</br>
						Pemeriksaan Umum :<br> ".$rekMed['txtPemeriksaan']."</br>
						</dd>
						</dl>";
			$detailTindakan = $this->rekam_medis->getDetailTindakan($idRekmedDetail);
			if(!empty($detailTindakan)){
				foreach($detailTindakan as $rowTindakan) {
					$namaTindakan = $rowTindakan['txtTindakan'];
					$detailTindakan = $rowTindakan['txtDeskripsiTindakan']!="" ? $rowTindakan['txtDeskripsiTindakan'] : "";
					$tindakan .= "<dl>
									<dt>Tindakan<dt><dd>".$namaTindakan."</dd>
									<dt>Detail Tindakan<dt><dd>".$detailTindakan."</dd>
									</dl>";
				}
			}
			$ttdPasien = !empty($rekMed['txtTandaTangan']) ? '<img src="'.BASE_URL.$rekMed['txtTandaTangan'].'" width="100" alt="">' : "";
			$retData[] = array(
			"tanggal" => indonesian_date($rekMed['dtTanggalKunjungan']),
			"nama_poli" => $rekMed['namaPoli'] ,
			"tindakan" => $tindakan,
			"diagnosa" => $diagnosa , ///			=> Diagnosa
			"kode_icd" =>$icd10 , ///			=> ICD 10
			"pengobatan"=>$pengobatan ,
			"nama_pegawai" => $rekMed['namaPegawai'] ,
			"tanda_tangan" => $ttdPasien
			);
		}
		return $retData;
	}

	public function convertToImage(){
		$this->isAjaxRequest();
		$this->load->library("convbase");
		$image = $this->input->post("imageString");
		$txtIdKunjungan = $this->input->post("idKunjungan");
		$folderImage = "temp/".date('Y/m/');
		$resImage = $this->convbase->convertToImage($image , $folderImage ,$txtIdKunjungan.".png");
		$retImage = str_replace(ASSETS_UPLOAD_DIR , ASSETS_UPLOAD_URL , $resImage);
		echo $retImage . "?".rand(0,9);
	}

	public function getFormOdontogram($selectedOdontogram=""){
		$dt = array();
		$dt['status_odontogram'] = $this->config->item("status_odontogram");
		$dt['status_odontogram_rome'] = $this->config->item("status_odontogram_rome");
		$dt['selected_odontogram'] = $selectedOdontogram;
		$this->load->view("rekam_medis/form_gigi_odontogram" , $dt);
	}
	public function save_mode_form(){
		$this->isAjaxRequest();
		$postData = $this->input->post();
		$status = false;
		
		$message = "Failed To Save";
		$noRekamMedis = $this->input->post("txtNoAnggotaPasien");
		if(empty($noRekamMedis)){
			$noRekamMedis = $this->input->post("txtNoRekamMedis");
		}
		$txtIdRekamMedis = $this->input->post("txtIdRekamMedis");
		$id_kunjungan = $this->input->post("txtIdKunjungan");
		$mode = $this->input->post("modeInsertRekamMedis");
		$idRekmedDetail = $this->input->post("txtIdRekmedDetail");
		$checkByNoRekmed = $this->rekam_medis->getCountNoRekamMedis($noRekamMedis);
		
		$modeRekmed = $this->input->post("modeRekmed");
		//echopre($postData);die;
		//save kunjungan jika mode form rekam medis 
		if ((!empty($modeRekmed))&&(empty($id_kunjungan))) {
		 	$idPasien =	$this->input->post('txtIdPasien');
			$dataPasien = $this->pasien_model->getDetail($idPasien);
			$tanggalLahir = $dataPasien['dtTanggalLahir'];
			$tanggal = explode(" " , $tanggalLahir);
			$usia = personAge($tanggal[0]);
			$usiaHari = getIntervalDays($tanggal[0]);
			$date_now = date("Y-m-d H:i:s");
			$arrayInput = array(
						"txtIdPasien" => $idPasien,
						"intIdPelayanan" => $this->input->post("txtIdPelayanan"),
						"intIdJaminanKesehatan" => $dataPasien['intIdJaminanKesehatan'],
						"txtNoJaminanKesehatan" => $dataPasien['txtNoIdJaminanKesehatan'],
						"txtUsiaPasienKunjungan" => $usia,
						"intUsiaPasienHari" => $usiaHari,
						"txtKeteranganKunjungan" => '',
					);
			$idKunjungan = $this->generateIdKunjungan();
			$arrayInput['txtIdKunjungan'] = $idKunjungan;
			$arrayInput['intNoAntri'] = '';
			$arrayInput['dtTanggalKunjungan'] = $date_now;
			$arrayInput['dtCreatedKunjungan'] = $date_now;
			$arrayInput['dtLastUpdateKunjungan'] = $date_now;
			$arrayInput['bitStatusKunjungan'] = '';
			$arrayInput['bitIsApotik'] = '3';
			$arrayInput['bitIsPoli'] ='3';
			$arrayInput['bitIsBaru'] = 1 ;
			$resSave = $this->register_model->saveData($arrayInput);
			$id_kunjungan = $idKunjungan;
		}
		//
		
		if($checkByNoRekmed > 0){

			///			Update
			$detailRekamMedis = $this->rekam_medis->getMasterByNoRekmed($noRekamMedis);

			$statusRekamMedis = $this->input->post("statusRekamMedis");

			$idRekamMedis = $detailRekamMedis['txtIdRekamMedis'];

			$arrInsertRekmed = array(
				"txtNoRekamMedis" => $noRekamMedis,
				"dtLastUpdate" =>date("Y-m-d H:i:s"),
				"bitStatusRekmed" =>1,
			);

			$retValMaster = $this->rekam_medis->updateRekamMedis($arrInsertRekmed , $idRekamMedis);

		}
		else{

			///			Insert New Rekmed
			$idRekamMedis = $this->generateIdRekmed();
			$arrInsertRekmed = array(
			"txtIdRekamMedis" => $idRekamMedis,
			"txtIdPasien" => $this->input->post("txtIdPasien"),
			"txtNoRekamMedis" => $this->input->post("txtNoRekamMedis"),
			"dtCreatedDate" =>$this->date_now,
			"dtLastUpdate" =>$this->date_now,
			"bitStatusRekmed" =>1,
			);


			$retValMaster = $this->rekam_medis->insertRekamMedis($arrInsertRekmed);

		}

		$statusMaster = $retValMaster['status'];


		if($mode!="" && $mode="insertNoRekamMedis"){

			$retVal = array();

			$retVal['id_rekmed'] = $idRekamMedis;

			$retVal['status'] = $statusMaster;

			$retVal['message'] = "Data Berhasil Di Simpan";

			echo json_encode($retVal);
			die;

		}


		if($statusMaster){

			$txtTanggalPemeriksaan = $this->input->post('txtTanggalPeriksa');

			$txtTanggals = explode(" ", $txtTanggalPemeriksaan);

			$txtTanggalPeriksa = $txtTanggals[0];

			$txtTanggalLahirPasien = $this->input->post("dtTanggalLahir");

			$usiaPasien = getIntervalDays($txtTanggalPeriksa , $txtTanggalLahirPasien);

			$arrInsertDetailRekmed = array(
			"txtIdRekamMedis"  => $idRekamMedis,
			"txtIdKunjungan"   => $id_kunjungan,
			"intIdPelayanan"   => $this->input->post("txtIdPelayanan"),
			"intUsiaHari"   => $usiaPasien,
			"txtAnamnesa"   => $this->input->post("txtAnamnesa"),
			"txtPemeriksaan"   => $this->input->post("txtPemeriksaan"),
			"txtTinggi"   => $this->input->post("txtTinggi"),
			"txtBerat"   => $this->input->post("txtBerat"),
			"txtSistole"   => $this->input->post("txtSistole"),
			"txtDiastole"   => $this->input->post("txtDiastole"),
			"txtRespiratoryRate"   => $this->input->post("txtRespiratoryRate"),
			"txtHeartRate"   => $this->input->post("txtHeartRate"),
			"txtRencanaTindakan" => $this->input->post("txtRencanaTindakan"),
			"txtKesadaran"   => $this->input->post("txtKesadaran"),
			"txtHasilRujukan" => $this->input->post("txtHasilRujukan"),
			"txtKeterangan"   => $this->input->post("txtKeterangan"),
			"txtOdontogram"   => $this->input->post("selectOdontogram"),
			"bitSetuju"   => $this->input->post("pernyataan"),
			"txtPengobatan"   => !is_array($this->input->post("txtPengobatan")) ? $this->input->post("txtPengobatan") : "",
			"intIdPegawai"   => $this->input->post("intIdPegawai"),
			"dtTanggalPemeriksaan" => $txtTanggalPemeriksaan,
			"txtTandaTangan" => str_replace(BASE_URL , "" , $this->input->post("inputTTD")),
			"bitStatusRekmedDetail"   => 1,
			);

			

			if(empty($idRekmedDetail)){

				$arrInsertDetailRekmed['dtCreatedDate'] = $txtTanggalPemeriksaan!="" ? $txtTanggalPemeriksaan : $this->date_now;

				$arrInsertDetailRekmed['dtLastUpdate'] = $this->date_now;

				$retValDetail = $this->rekam_medis->insertDetailRekamMedis($arrInsertDetailRekmed);

			}
			else{

				$arrInsertDetailRekmed['dtLastUpdate'] = $this->date_now;

				$retValDetail = $this->rekam_medis->updateDetailRekamMedis($arrInsertDetailRekmed, $idRekmedDetail);

			}

		}

		$idDetailRekamMedis = $retValDetail['id'];

		$statusDetail =  $retValDetail['status'];

		$diagnosa = $this->input->post("selectDiagnosa");

		$detailDiagnosa = $this->input->post("txtDetailDiagnosa");

		if(($statusDetail==true) && (!empty($diagnosa))){

			$idDetailRekmed = $retValDetail['id'];

			$checkDiagnosa = $this->rekam_medis->checkDetailDiagnosa($idDetailRekmed);

			if($checkDiagnosa > 0){

				$hapusDiagnosa = $this->rekam_medis->deleteDetailDiagnosa($idDetailRekmed);

			}

			$arrBatchDiagnosa = array();
			if(is_array($diagnosa)){

				for ($indexDiagnosa=0;$indexDiagnosa < count($diagnosa);$indexDiagnosa++){

					if(!empty($diagnosa[$indexDiagnosa])){

						$checkNewCase = $this->rekam_medis->checkNewCaseDetailDiagnosa($idRekamMedis , $diagnosa[$indexDiagnosa]);

						$arrInsertDetailDiagnosa = array(
						"txtIdRekmedDetail"   => $idDetailRekmed,
						"intIdDiagnosaPenyakit" => $diagnosa[$indexDiagnosa],
						"dtCreatedDate"   => date("Y-m-d H:i:s"),
						"bitIsKasusBaru" => $checkNewCase==true ? 1 : 0,
						);

						$arrBatchDiagnosa[] = $arrInsertDetailDiagnosa;
					}

				}
				$retValDiagnosa = $this->rekam_medis->insertBatchDiagnosa($arrBatchDiagnosa);
			}
		}

		///		Insert Data Tindakan
		$tindakan = $this->input->post("selectTindakan");

		//$deskripsi_tindakan = $this->input->post("txtDetailTindakan");

		if(!empty($tindakan)){

			$idDetailRekmed = $retValDetail['id'];

			$checkTindakan = $this->rekam_medis->checkDetailTindakan($idDetailRekmed);

			if($checkTindakan > 0){

				$hapusTindakan = $this->rekam_medis->deleteDetailTindakan($idDetailRekmed);

			}
			$arrInsertTindakan = array();
			if(is_array($tindakan)){

				for ($indexTindakan=0;$indexTindakan < count($tindakan);$indexTindakan++){

					$arrInsertDetailTindakan = array(
					"txtIdRekmedDetail"   => $idDetailRekamMedis,
					"intIdTindakan" => $tindakan[$indexTindakan],
					"txtDeskripsiTindakan" => '',
					);

					$arrInsertTindakan[] = $arrInsertDetailTindakan;

					////$retValTindakan = $this->rekam_medis->insertDetailTindakan($arrInsertDetailTindakan);

				}

			}

			$retValTindakan = $this->rekam_medis->insertBatchTindakan($arrInsertTindakan);

		}


		$Pengobatan = $this->input->post("selectPengobatan");
		$jumlah = $this->input->post("txtPengobatan");
		$dosis = $this->input->post("txtDosis");

		if(!empty($Pengobatan)){

			$idDetailRekmed = $retValDetail['id'];

			$checkPengobatan = $this->rekam_medis->getDataResep($idDetailRekmed);

			if($checkPengobatan > 0){

				$hapusPengobatan = $this->rekam_medis->deleteDetailPengobatan($idDetailRekmed);
				
			}
			$arrInsertPengobatan = array();
			if(is_array($Pengobatan)){
				
				for ($indexPengobatan=0;$indexPengobatan < count($Pengobatan);$indexPengobatan++){

					$arrInsertDetailPengobatan = array(
					"txtIdRekmedDetail"   => $idDetailRekmed,
					"intIdObat" => $Pengobatan[$indexPengobatan],
					"jumlah" => $jumlah[$indexPengobatan],
					"dosis" => $dosis[$indexPengobatan]
					);

					$arrInsertPengobatan[] = $arrInsertDetailPengobatan;

					////$retValPengobatan = $this->rekam_medis->insertDetailPengobatan($arrInsertDetailPengobatan);

				}
				
			}

			$retValPengobatan = $this->rekam_medis->insertBatchPengobatan($arrInsertPengobatan);

		}


		///		Save Diagnosa BPJS
		if(!empty($postData['txtNoJamkes'])){
			$this->saveDataBPJSRawatJalan($postData , $idDetailRekamMedis);
		}


		if($retValDetail['status']){

			$status = true;

			$message = "Data Berhasil Di Simpan";

		}


		$retVal = array();

		$retVal['id_rekmed'] = $idRekamMedis;

		$retVal['id_rekmed_detail'] = $idDetailRekamMedis;

		$retVal['status'] = $status;

		$retVal['message'] = $message;

		$this->session->set_flashdata("alert_rekam_medis" , $retVal);

		$this->setJsonOutput($retVal);
	}





}
