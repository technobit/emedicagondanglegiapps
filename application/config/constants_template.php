<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Main Config
|--------------------------------------------------------------------------
|
| Display Config That your mother needs to Cook Some Food, Yeah Like It Would Help. LOL
| Trust me this apps is mother fucker holly-gravure-big-boobs broh... :v
*/

define('USER_LUMAJANG', "43993");
define('ID_KECAMATAN', "44170");

define('HOST_URL', "http://local-rs-gondanglegi.emedica.co.id/");
	
define('BASE_URL', "http://local-rs-gondanglegi.emedica.co.id/");


define('APP_NAME' , 'EmedicaPKM');

if($_SERVER["HTTP_HOST"]=='localhost'){
	
	define('APP_STATUS_AKSES' , '1');
	///	/ 1 = Devel , 0 = Production
	define('JAM_MASUK' , '07:00');
	
	define('JAM_PULANG' , '17:00');
	
}
else{
	
	define('APP_STATUS_AKSES' , '0');
	///	/ 1 = Devel , 0 = Production
	define('JAM_MASUK' , '07:00');
	
	define('JAM_PULANG' , '13:00');
	
}


define('APP_KELURAHAN' , 'Bululawang');

define('APP_KOTA' , 'Malang');

define('APP_VERSION' , '1.3.3');


//// Devel Mode
if($_SERVER["HTTP_HOST"]=='localhost'){
define('PRINTER_LOKET' , "smb://Guest@DESKTOP-CCQSBK0/epson_thermal");

define('PRINTER_KASIR' , "smb://Guest@DESKTOP-CCQSBK0/epson_thermal");

define('PRINTER_SELF_SERVICE' , "smb://Guest@DESKTOP-CCQSBK0/epson_thermal");
}else{
define('PRINTER_LOKET' , "smb://Guest@LoketBululawang/epson_loket");

define('PRINTER_KASIR' , "smb://Guest@KasirBululawang/epson_kasir");

define('PRINTER_SELF_SERVICE' , "smb://Guest@selfService/selfMedica");
}




define('LINK_VERIFICATION' , "Emedica://open?id=");

define('BPJS_API_LINK_DEVEL' , "http://api.bpjs-kesehatan.go.id/pcare-rest-dev/");

define('BPJS_API_LINK_PRODUCTION' , "http://api.bpjs-kesehatan.go.id/pcare-rest/");

define('BPJS_STATUS_PRODUCTION' , 1);

define('BPJS_STATUS_API' , true);
///1 = Active, 0 = Non Active 
define('CONS_ID' , '4035');

define('CONS_KEY' , '3yB331E09C');

define('PCARE_USER' , 'pkm_bululawang');

define('PCARE_PASS' , '"Pkm5ehatbugar"');

define('PCARE_DOCTOR_ID' , 'bululawan6');


define('ASSETS_URL', BASE_URL."assets/");

define('ASSETS_CSS_URL', ASSETS_URL."custom/css/");

define('ASSETS_JS_URL', ASSETS_URL."custom/js/");

define('ASSETS_IMAGE_URL', ASSETS_URL."img/");

define('ASSETS_PLUGIN_URL', ASSETS_URL."plugins/");

define('ASSETS_UPLOAD_URL' , ASSETS_URL."uploads/");

define('ASSETS_UPLOAD_DIR' , FCPATH."assets/uploads/");



/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/

defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);



/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/

defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);

defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);

defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);

defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);



/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');

defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');

defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb');
// truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESCTRUCTIVE') OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b');
// truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');

defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');

defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');

defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');



/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/

defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0);
// no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1);
// generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3);
// configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4);
// file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5);
// unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6);
// unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7);
// invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8);
// database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9);
// lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125);
// highest automatically-assigned error code