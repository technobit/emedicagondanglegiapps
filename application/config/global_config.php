<?php
$config['status_list'] = array("1"=>"Aktif" , "0"=>"Non Aktif");
$config['jk_list'] = array("L"=>"Laki-Laki" , "P"=>"Perempuan");
$config['jaminan_list'] = array("1"=>"Laki-Laki" , "P"=>"Perempuan");
$config['periksa_list'] = array("","1"=> "Pemeriksaan Lanjut" , "2"=>"Rujukan");
$config['group_type'] = array("1"=> "Pegawai" , "2"=>"Pengguna Layanan");
$config['pendidikan_list'] = array("1"=> "Pegawai" , "2"=>"Pengguna Layanan");
$config['kondisi_keluar'] = array(
      "1"=> "Sembuh" ,
      "2"=> "Berobat Jalan",
      "3"=> "Pulang Paksa",
      "4"=> "Rujukan",
      );

$config['kode_provider_puskesmas'] = "13242401";
//$config['pernyataan_puskesmas'] = " Pengguna layanan telah diberikan penjelasan oleh pihak puskesmas dan menyetujui segala resiko atau akibat, efek samping dari tindakan yang dimaksud.";
$config['pernyataan_puskesmas'] = "Pengguna Layanan Telah Memahami tentang rencana layanan medis dan rencana layanan terpadu yang disampaikan oleh petugas.";

$config['kesadaran_list'] = array(
      ""=> "-Pilih Kesadaran-" ,
      "01"=> "Compos Mentis" ,
      "02"=> "Somnolence",
      "03"=> "Sopor",
      "04"=> "Coma",
      );
$config['pekerjaan'] = array(
      ""=> "-Pilih Jenis Pekerjaan-" ,
      "formal"=> "Formal" ,
      "non_formal"=> "Non Formal",
      );
$config['status_pulang_rawat_jalan'] = array(
      "3" => "Berobat Jalan",
      "4" => "Rujuk Lanjut",
      "5" => "Rujuk Internal",
);

$config['status_pulang_rawat_inap'] = array(
      "0" => "Sembuh",
      "1" => "Meninggal",
      "2" => "Pulang Paksa",
      "4" => "Rujuk Lanjut",
      "5" => "Rujuk Internal",
      "9" => "Lain-Lain",
);
$config['jenis_lokasi'] = array(
      "" => "-Pilih Jenis Lokasi-",
      "4" => "SD",
      "1" => "Posyandu",
      "2" => "UPS",
      "3" => "BDS",
      );
$config['status_layanan_list'] = array(
                               "0"=>"Batal",
                               "1"=>"Antri",
                               "2"=>"Sedang Di Layani",
                               "3"=>"Sudah Di Layani",
                               "4"=>"Pemeriksaan Lanjut",
                               "5"=>"Rujukan Pelayanan",
                               "6"=>"Rujukan Pelayanan",
                               );
$config['status_layanan_alert'] = array(
                               "0"=>"",
                               "1"=>"danger",
                               "2"=>"warning",
                               "3"=>"success",
                               "4"=>"info",
                               "5"=>"default",
                               "6"=>"default",
                               );


$config['bpjs_jenis_kelompok'] = array(
      "00"=>"Non-Prolanis",
      "01"=>"Diabetes Melitus",
      "02"=>"Hipertensi",
);
$config['bpjs_jenis_kegiatan'] = array(
      "01"=>"Senam",
      "10"=>"Penyuluhan",
      "11"=>"Penyuluhan dan Senam",
      
);
$config['status_rawat_inap'] = array(
      "1"=>"Belum Registrasi",
      "2"=>"Dalam Perawatan",
      "3"=>"Selesai Perawatan",
);

$config['status_absensi'] = array(
      "1"=>"Hadir",
      "2"=>"Izin",
      "3"=>"Alpa",
      "4"=>"Cuti",
      "5"=>"Dinas Luar",
);

$config['status_odontogram'] = array(
      "a_ka"=>"Atas Kanan",
      "a_ki"=>"Atas Kiri",
      "b_ka"=>"Bawah Kanan",
      "b_ki"=>"Bawah Kiri",
);

$config['status_odontogram_rome'] = array(
      "1"=>"I",
      "2"=>"II",
      "3"=>"III",
      "4"=>"IV",
      "5"=>"V",
);

$config['agama_list'] = array(
                              "islam"=>"Islam" ,
                              "kristen"=>"Kristen" ,
                              "katolik"=>"Katolik" ,
                              "konghucu"=>"Konghucu" ,
                              "hindu"=>"Hindu" ,
                              "budha"=>"Budha" ,
                              );
$config['jabatan_list'] = array(
                              "0"=>"Tidak Ada",
                              "1"=>"Kepala",
                              "2"=>"Staff" ,
                              "3"=>"Dokter",
                              "4"=>"Bidan",
                              "5"=>"Perawat",
                              );
$config['pelayanan_list'] = array("1"=>"Ruangan Umum",
                                  "2"=>"Ruangan Umum II",
                                  "3"=>"Ruangan Gigi & Mulut",
                                  "4"=>"Ruangan KIA",
                                  "5"=>"Ruangan Gizi",
                                  "6"=>"Laboraturium",
                            );

$config['jenis_pelayanan'] = array("1"=>"Ruangan Umum",
                                   "2"=>"Ruangan Gigi",
                                   "3"=>"Ruangan Ibu",
                                   "4"=>"Ruangan Anak",
                                   "5"=>"Ruangan Gizi",
                                   "6"=>"Laboraturium",
                                   "7"=>"Apotik",
                                   "8"=>"Kasir");
$config['jenis_pemeriksaan_lab'] = array(
                                    "1" => "Hematologi",
                                    "2" => "Faal Hati",
                                    "3" => "Widal",
                                    "4" => "Faal Ginjal",
                                    "5" => "Lemak",
                                    "6" => "Faeces",
                                    "7" => "Urin",
                                    "8" => "Test Kehamilan",
                                    "9" => "Gula Darah",
                                    "10" => "Asam Urat",
                                    "11" => "Cholesterol",
                                    "12" => "HIV",
                                    "13" => "Malaria",
                                    "14" => "Sputun",
                                    "15" => "Golongan Darah",
                                    "16" => "Hepatitis",
                                    );

$config['month_list'] = array("1"=>"Januari",
                              "2"=>"Februari",
                              "3"=>"Maret",
                              "4"=>"April",
                              "5"=>"Mei",
                              "6"=>"Juni",
                              "7"=>"Juli",
                              "8"=>"Agustus",
                              "9"=>"September",
                              "10"=>"Oktober",
                              "11"=>"November",
                              "12"=>"Desember",
);
$config['month'] = array("01"=>"Januari",
                              "02"=>"Februari",
                              "03"=>"Maret",
                              "04"=>"April",
                              "05"=>"Mei",
                              "06"=>"Juni",
                              "07"=>"Juli",
                              "08"=>"Agustus",
                              "09"=>"September",
                              "10"=>"Oktober",
                              "11"=>"November",
                              "12"=>"Desember",
);

$config['day_list'] = array(
      "0" => "Minggu",
      "1" => "Senin",
      "2" => "Selasa",
      "3" => "Rabu",
      "4" => "Kamis",
      "5" => "Jumat",
      "6" => "Sabtu",

);
/// Config Status Upload BPJS

$config['bpjs_upload_status'] = array(
      "" => "-Semua-",
      "1" => "Terupload",
      "0" => "Belum Terupload"
);
$config['jenis_pasien_imunisasi'] = array(
      "" => "Pilih Jenis Pengguna Layanan",
      "1" => "Anak",
      "2" => "Bayi",
      "3" => "Balita",
      "4" => "WUS Tidak Hamil",
      "5" => "WUS Hamil",
      "6" => "Pengguna Layanan Biasa",
      "7" => "Caten",
      "8" => "Siswa/Siswi",
);
$config['status_nikah'] = array(
      "1" => "Belum Menikah",
      "2" => "Menikah",
      "3" => "Janda",
      "4" => "Duda",
      
);

$config["menu_nav"] = array(
      "loket" =>
      array("class" => "fa fa-list-alt",
                  "desc" => "Loket",
                  "main_url" => "loket/",
                  "sub_menu" => "sub_loket"
                  ),
      "rekapitulasi" =>
      array("class" => "fa fa-table",
                  "desc" => "Rekapitulasi",
                  "main_url" => "",
                  "sub_menu" => "sub_rekapitulasi"
                  ),
      "users" =>
      array("class" => "fa fa-user-secret",
                  "desc" => "Pengguna",
                  "main_url" => "/pengguna/",
                  "sub_menu" => "sub_pengguna"
                  ),
     
      "master_data" =>
      array("class" => "fa fa-database",
                  "desc" => "Master Data",
                  "main_url" => "pengaturan/pengaturan/",
                  "sub_menu" => "sub_master"
                  ),
);

$config["submenu_nav"] = array("sub_loket" => array(
            "loket_umum" => array(
                  "class" => "fa fa-folder",
                  "url" => "loket/antrian/78",
                  "desc" => "Loket Umum",
                  "subsub_menu" => ""
            ),
            "loket_bpjs" => array(
                  "class" => "fa fa-folder",
                  "url" => "loket/antrian/79",
                  "desc" => "Loket BPJS",
                  "subsub_menu" => ""
            ),
            "loket_rawat_inap" => array(
                  "class" => "fa fa-folder",
                  "url" => "loket/antrian/80",
                  "desc" => "Loket Rawat Inap",
                  "subsub_menu" => ""
            ),

      ),
      "sub_rekapitulasi"=>array(
            "rekap_data_antrian" => array(
                  "class" => "fa fa-table",
                  "desc" => "Rekap Data Antrian",
                  "url" => "rekapitulasi/loket/data_antrian/",
                  "subsub_menu" => ""
            ),
            "rekap_jumlah_antrian" => array(
                  "class" => "fa fa-table",
                  "desc" => "Rekap Jumlah Antrian",
                  "url" => "rekapitulasi/loket/jumlah_antrian/",
                  "subsub_menu" => ""
            ),

      ),
      
      "sub_pengguna" => array(
            "master_pegawai" => array(
                  "class" => "fa fa-tasks",
                  "url" => "data-pegawai/",
                  "desc" => "Data Pegawai",
                  "subsub_menu" => ""
            ),
            "user_pengguna" => array(
                  "class" => "fa fa-users",
                  "url" => "pengguna/",
                  "desc" => "Data Pengguna",
                  "subsub_menu" => ""
            ),
            "user_level" => array(
                  "class" => "fa fa-sitemap",
                  "url" => "level/",
                  "desc" => "Tingkatan Pengguna",
                  "subsub_menu" => ""
            ),
      ),

);


