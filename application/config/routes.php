<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$poli_slug = "(poli-anak|poli-gigi|poli-ibu|poli-umum|poli-gizi)";
$pendaftaran_slug = "loket|insert|ugd|rawat-inap";
///Loket

$route['loket/(daftar)/(:any)/(:num)'] = 'loket/detail/$1/$2/$3';
$route['loket/antrian'] = 'loket/register_loket';
$route['loket/antrian/(:num)'] = 'loket/register_loket/$1';
$route['loket/detail/(:num)'] = 'loket/detail/$1';
$route['loket/rekap'] = 'loket/register_history';
$route['loket/get-data-antrian'] = 'loket/getDataAntrian';
$route['loket/get-data-rawat-inap'] = 'loket/getDataRawatInap';
$route['loket/update-status-antrian'] = 'loket/updateStatusAntrian';
$route['loket/save-referral-antrian'] = 'loket/saveReferalLoket';
$route['loket/periksa-poli-selesai'] = 'loket/periksaPoliSelesai';
$route['loket/rawat-inap-selesai'] = 'loket/periksaRawatInapSelesai';
$route['loket/periksa-lab-selesai'] = 'loket/periksaLabSelesai';
$route['loket/get-frm-rujukan'] = 'loket/getFormRujukan';
$route['loket/hapus-data-antrian'] = 'loket/hapusDataAntrian';
$route['loket/(:num)'] = 'loket/index/$1';
$route['loket'] = 'loket/index';

///Pasien
$route['pasien/pendaftaran/('.$pendaftaran_slug.')'] = 'pasien/addPasien/$1';
$route['pasien/pendaftaran/('.$pendaftaran_slug.'|detail)/(:num)'] = 'pasien/addPasien/$1/$2';
$route['pasien/pendaftaran/('.$pendaftaran_slug.'|detail)/(:any)'] = 'pasien/addPasien/$1/$2';
$route['pasien/pendaftaran/('.$pendaftaran_slug.'|detail)/(:any)/(:num)'] = 'pasien/addPasien/$1/$2/$3';
$route['pasien/get-data-pasien/(:any)'] = 'pasien/getDataPasien/$1';
$route['pasien/delete-data-pasien'] = 'pasien/deletePasien';
$route['pasien/get-data-pasien'] = 'pasien/getDataPasien';
$route['pasien/get-no-anggota-pasien/(:any)'] = 'pasien/getNoAnggotaPasien/$1';
$route['pasien/get-list-data-pasien'] = 'pasien/getListDataPasien';
$route['pasien/save-pasien'] = 'pasien/saveDataPasien';
$route['pasien/cek-no-anggota'] = 'pasien/checkNoAnggota';
$route['pasien'] = 'pasien/index';
 
///Pelayanan
$route['pelayanan/poliklinik/(:num)'] = 'poliklinik/index/$1';
$route['pelayanan/poliklinik/detail/(:any)'] = 'poliklinik/detail/$1';
$route['pelayanan/poli-ibu/(:num)'] = 'poliklinik/poli_ibu/$1';
$route['pelayanan/poli-ibu/detail/(:any)'] = 'poliklinik/detail/$1';
$route['pelayanan/laboratorium/(:num)'] = 'laboratorium/index/$1';
$route['pelayanan/laboratorium/detail/(:any)'] = 'laboratorium/detail/$1';

///=== Data Pegawai
$route['data-pegawai/get-list-pegawai'] = 'master_pegawai/getListData';
$route['data-pegawai/tambah-data'] = 'master_pegawai/form';
$route['data-pegawai/delete'] = 'master_pegawai/hapusData';
$route['data-pegawai/simpan-data'] = 'master_pegawai/simpanData';
$route['data-pegawai/edit/(:any)'] = 'master_pegawai/form/$1';
$route['data-pegawai/retrieve-data'] = 'master_pegawai/getData';
$route['data-pegawai/retrieve-data/(:any)'] = 'master_pegawai/getData/$1';
$route['data-pegawai/retrieve-data/(:any)/(:any)'] = 'master_pegawai/getData/$1/$2';
$route['data-pegawai'] = 'master_pegawai/index';

///=== Data Pelayanan
$route['data-pelayanan/get-list-pelayanan'] = 'master/pelayanan/getListPelayanan';
$route['data-pelayanan/tambah-data'] = 'master/pelayanan/form';
$route['data-pelayanan/delete'] = 'master/pelayanan/hapusDataPelayanan';
$route['data-pelayanan/simpan-data'] = 'master/pelayanan/simpanDataPelayanan';
$route['data-pelayanan/edit/(:any)'] = 'master/pelayanan/form/$1';
$route['data-pelayanan/retrieve-data'] = 'master/pelayanan/getData';
$route['data-pelayanan/retrieve-data/(:any)'] = 'master/pelayanan/getData/$1';
$route['data-pelayanan/retrieve-data/(:any)/(:any)'] = 'master/pelayanan/getData/$1/$2';
$route['data-pelayanan'] = 'master/pelayanan/index';

///Users
$route['pengguna/get-list-tindakan'] = 'users/pengguna/getListData';
$route['pengguna/tambah-data'] = 'users/pengguna/form';
$route['pengguna/delete'] = 'users/pengguna/hapusData';
$route['pengguna/simpan-data'] = 'users/pengguna/simpanData';
$route['pengguna/edit/(:any)'] = 'users/pengguna/form/$1';
$route['pengguna/edit-password/(:any)'] = 'users/pengguna/form_password/$1';
$route['pengguna/simpan-data'] = 'users/pengguna/simpanData';
$route['pengguna/check-password'] = 'users/pengguna/changePassword';
$route['pengguna/retrieve-data'] = 'users/pengguna/getData';
$route['pengguna'] = 'users/pengguna/index';

$route['level/tambah-data'] = 'users/level/form';
$route['level/delete'] = 'users/level/hapusData';
$route['level/simpan-data'] = 'users/level/simpanData';
$route['level/edit/(:any)'] = 'users/level/form/$1';
$route['level/detail-menu/simpan-data'] = 'users/level/simpanDataMenu';
$route['level/detail-menu/(:any)'] = 'users/level/detail_menu/$1';
$route['level/retrieve-data'] = 'users/level/getData';
$route['level'] = 'users/level/index';
 
/// Rekapitulasi 
$route['rekapitulasi/loket/data_antrian'] = 'mrekapitulasi/rekapitulasi_loket_data/index';
$route['rekapitulasi/loket/jumlah_antrian'] = 'mrekapitulasi/rekapitulasi_loket_jumlah/index';

$route['rekapitulasi/kunjungan-per-poli'] = 'mrekapitulasi/rekapitulasi_pengunjung/index';
$route['rekapitulasi/rekapitulasi_loket'] = 'mrekapitulasi/rekapitulasi_loket/index';
$route['rekapitulasi/rekapitulasi_jumlah_loket'] = 'mrekapitulasi/rekapitulasi_jumlah_loket/index';
$route['rekapitulasi/data-jaminan-kesehatan'] = 'mrekapitulasi/data_jamkes/index';
$route['rekapitulasi/rekap-pekerjaan'] = 'mrekapitulasi/rekap_pekerjaan/index';
$route['rekapitulasi/rekap-data-pekerjaan'] = 'mrekapitulasi/rekap_data_pekerjaan/index';

$route['rekapitulasi/jumlah-kunjungan'] = 'mrekapitulasi/rekapitulasi_jumlah_pengunjung/index';
$route['rekapitulasi/jaminan-kesehatan'] = 'mrekapitulasi/rekapitulasi_pengunjung_jamkes/index';
$route['rekapitulasi/data-penyakit'] = 'mrekapitulasi/rekapitulasi_data_penyakit/index';
$route['rekapitulasi/data-tindakan'] = 'mrekapitulasi/rekapitulasi_data_tindakan/index';
$route['rekapitulasi/rekap-data-tindakan'] = 'mrekapitulasi/rekapitulasi_rekap_data_tindakan/index';

// Cetak
$route['cetak/cetak-no-antrian'] = 'printpage/cetakNoAntrian';
$route['cetak/cetak-no-antrian/(:any)'] = 'printpage/cetakNoAntrian/$1';
$route['cetak/cetak-no-antrian-loket'] = 'printpage/cetakNoAntrianLoket';
$route['cetak/cetak-struct-rawat-jalan/(:any)'] = 'printpage/printInvoiceRawatJalan/$1';

/// Fingerscan
$route['finger/register/(:any)'] = 'fingerscan/registerPasien/$1';
$route['finger/register-pegawai/(:any)'] = 'fingerscan/registerPegawai/$1';
$route['finger/register-ulang-pegawai/(:any)'] = 'fingerscan/registerUlangPegawai/$1';
$route['finger/process_register'] = 'fingerscan/process_register';
$route['finger/insertFingerPegawai'] = 'fingerscan/insertFingerPegawai';
$route['finger/get_activation_ac'] = 'fingerscan/get_activation_ac';
$route['finger/message/(:any)'] = 'fingerscan/message/$1';

/// Suara
$route['suara/test.mp3'] = 'suara/index';
$route['suara/antrian/(:any).mp3'] = 'suara/antrian/$1';

/// Self Service  - 21 Desember 2016
$route['self-service'] = 'Self_service/pendaftaran_loket';
$route['self-service/pendaftaran/(:any)'] = 'Self_service/pendaftaran/$1';
$route['self-service/pendaftaran-loket'] = 'Self_service/pendaftaran_loket';
$route['self-service/registrasi'] = 'Self_service/registrasi';
$route['self-service/landing-page/(:any)'] = 'Self_service/landing_page/$1';
$route['self-service/landing-page-pegawai/(:num)'] = 'Self_service/landing_page_pegawai/$1';
$route['self-service/landing-page-loket/(:num)'] = 'Self_service/landing_page_loket/$1';

//Login
$route['logout'] = 'login/logout';
$route['login'] = 'login/index';

$route['dashboard'] = 'dashboard/index';
$route['default_controller'] = 'dashboard';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
