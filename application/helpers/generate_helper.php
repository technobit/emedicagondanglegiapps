<?php

if(!function_exists("getIdVerification")){
    function getIdVerification(){
        $uniqueID = "VRF".date("YmdHis") . rand(0,99);
        return $uniqueID;
    }
}

if(!function_exists("getIdVerificationPegawai")){
    function getIdVerificationPegawai(){
        $uniqueID = "VRFPGW".date("YmdHis") . rand(0,99);
        return $uniqueID;
    }
}

if(!function_exists("getIdRekamMedis")){
    function getIdRekamMedis(){
        $rand_number = rand(1000 , 9999);
		$idrekmed = "RKM" . date("YmdHis").$rand_number;
		return $idrekmed;
    }
}

