<?php

function selectJenisKelamin($placeholder = null) {
    if ($placeholder) {
        $options[''] = $placeholder;
    }
    $options['L'] = 'Laki-laki';
    $options['P'] = 'Perempuan';
    return $options;
}

function selectDesa($placeholder = null) {
    $CI = &get_instance();
    $result = $CI->db->get('desa');
    $options = array();
    if ($placeholder) {
        $options[''] = $placeholder;
    }
    foreach ($result->result() as $row) {
        $options[$row->id] = $row->kode_desa . ' - ' . $row->nama_desa;
    }
    return $options;
}

function selectKemasan($placeholder = null) {
    $CI = &get_instance();
    $result = $CI->db->get('kemasan');
    return selectSrc($result, 'id', 'kemasan', $placeholder);
}

function selectJenisObat($placeholder = null) {
    $CI = &get_instance();
    $result = $CI->db->get('jenis_obat');
    return selectSrc($result, 'id', 'jenis_obat', $placeholder);
}

function selectJamkes($placeholder = null) {
    $CI = &get_instance();
    $result = $CI->db->get('jaminan_kesehatan');
    return selectSrc($result, 'intIdJaminanKesehatan', 'txtNamaJaminan', $placeholder);
}

function selectObat($placeholder = null) {
    $CI = &get_instance();
    $result = $CI->db->get('obat');
    return selectSrc($result, 'id', 'nama_obat', $placeholder);    
}
function selectPegawai($placeholder = null) {
    $CI = &get_instance();
    $result = $CI->db->get('pegawai');
    return selectSrc($result, 'intIdPegawai', 'txtNamaPegawai', $placeholder);    
}
function select_jenis_imunisasi($placeholder = null) {
    $CI = &get_instance();
    $result = $CI->db->get('jenis_imunisasi');
    return selectSrc($result, 'id_jenis_imunisasi', 'jenis_imunisasi', $placeholder);    
}

function selectObatApotik($id_apotik, $placeholder = null) {
    $CI = &get_instance();
    $result = $CI->db->select('obat.id, obat.nama_obat')->join('obat_apotik', 'obat_apotik.id_obat=obat.id' , 'left')
    ->where('obat_apotik.id_apotik', $id_apotik)
    ->get('obat');
    return selectSrc($result, 'id', 'nama_obat', $placeholder);    
}

function selectTahun($placeholder =  null, $start = 2014, $limit = null) {
    $options = array();
    if ($placeholder) {
        $options[''] = $placeholder;
    }
    if (!$limit) {
        $limit = date('Y');
    }    
    for ($i = $start; $i <= $limit; $i++) {
        $options[$i] = $i;
    }
    return $options;
}

function selectApotik($placeholder = null) {
    $options = array();
    if ($placeholder) {
        $options[''] = $placeholder;
    }
    $options['1'] = 'Rawat Jalan';
    $options['2'] = 'UGD';
    return $options;
}

function selectSrc($data, $valueField, $textField, $placeholder = null)
{
    
    $options = array();
    if ($placeholder) {
        $options[''] = $placeholder;
    }

    foreach ($data->result() as $row) {
        $options[$row->$valueField] = $row->$textField;
    }

    return $options;
}
