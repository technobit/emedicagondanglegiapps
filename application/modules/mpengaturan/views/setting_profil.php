<section class="content-header">
  <h1 class="title"><?=$title?></h1>
  <?=$breadcrumbs?>
</section>
<section class="content">
   <form class="form-horizontal" id="frm-pengaturan-profil" enctype="multipart/form-data" >
  <div class="row">
    <div class="col-xs-12">
    <!-- Cari Data Pasien -->
    <div class="box box-success">
      <div class="box-header with-border">
      <h3 class="box-title">Pengaturan</h3>
      </div>
    <!-- /.box-header -->
    <!-- form start -->
       
          <div class="box-body">
              <div class="col-sm-6">
              <?=$nmFaskes?>
              <?=$nmPimpinan?>
              <div class="form-group">
                  <label for="nmFaskes" class="col-sm-3 control-label form-label">Upload Logo</label>
                 <div class="col-sm-9">
                <input name="logo" id="logo" type="file">
            </div>
          </div>
            </div>
            <div class="col-sm-6" style="height: 200px;width: 50%;">
              <img src="<?=$link_image?>" id="image" style="height: 200px;width: 400px;" >
             
              </div>
           
               
          </div>
        
    </div>
      
        <!-- /.box-header -->
       
        <!-- /.box-body -->
      </div>
      </div>
      <div class="box box-success">
      <div class="box-header with-border">
      <h3 class="box-title">Wilayah</h3>
      </div>
    <!-- /.box-header -->
    <!-- form start -->
       
          <div class="box-body">
              <div class="col-sm-6">
              <?=$selectProvinsi?>
              <?=$selectKabupaten?>
              <?=$selectKecamatan?>
           </div>
             <div class="col-sm-12 text-center">
			<button id="saveBtn" type="button" class="btn btn-primary btn-lg"><i class="fa fa-save"></i> Simpan</button>
			<a href="<?=base_url('pengaturan/pengaturan')?>" id="cancel" class="btn btn-danger btn-lg"><i class="fa fa-ban"></i> Batal</a>
			 </div>
          </div>
        </form>
    </div>
      
        <!-- /.box-header -->
       
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>