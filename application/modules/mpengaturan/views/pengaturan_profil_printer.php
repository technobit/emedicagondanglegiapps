<section class="content-header">
  <h1 class="title"><?=$title?></h1>
  <?=$breadcrumbs?>
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
    <!-- Cari Data Pasien -->
    <div class="box box-success">
      <div class="box-header with-border">
      <h3 class="box-title">Pengaturan</h3>
      </div>
    <!-- /.box-header -->
    <!-- form start -->
        <form class="form-horizontal" id="frm-pengaturan-print">
          <div class="box-body">
               
              <div class="form-group">
                    <label class="col-sm-3 control-label form-label">Printer Loket</label>
                    <div class="col-sm-3">
                    <?=$printer_loket?>
                    </div>
                    
              </div>
              <div class="form-group">
                    <label class="col-sm-3 control-label form-label">Printer Self Service</label>
                    <div class="col-sm-3">
                    <?=$printer_self_service?>
                    </div>
                    
              </div>
              <div class="form-group">
                    <label class="col-sm-3 control-label form-label">Printer Self Kasir</label>
                    <div class="col-sm-3">
                    <?=$printer_self_kasir?>
                    </div>
                   
              </div>
               <div class="col-sm-offset-3 col-sm-7">
                    <button type="button" id="saveBtn" class="btn btn-success btn-flat"><i class="fa fa-send"></i> Simpan</button>
                </div>
          </div>
        </form>
    </div>
      <div class="box box-primary">
        <div class="box-header">
          <h3 class="box-title">List Profil Printer</h3>
          <div class="box-tools pull-right">
          <button class="btn btn-warning" id="btnAddProfil" onclick="addProfile()"><i class="fa fa-plus"></i> Tambah</button>  
          </div>
         
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table class="table table-hover" id="data-grid">
            <thead>
              <tr>
                <th>Profile Name</th>
                <th>Access Profile</th>
                <th>Computer Name</th>
                <th>Printer Name</th>
                <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
              
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>