<section class="content-header">
  <h1 class="title"style="font-size:35px;"><?=$title?></h1>
   <?=$breadcrumbs?>
</section>
<section class="content">
  <div class="row">
  
      <div class="col-sm-12">
          <!-- small box -->
         
                  
      <div class="col-sm-4">
        <!-- small box -->
        <a href="<?=$linkSetiingBpjs?>">
          <div class="small-box bg-blue">
            <div class="inner">
              <p style="font-size:18px;font-weight:bold">Pengaturan</p>
              <h4 style="font-size: 50px; font-weight: bold; padding: 5px 0;">Setting BPJS</h4>
            </div>
            <div class="icon">
              <i class="fa fa-credit-card"></i>
            </div>
            <div class="small-box-footer"></div>
          </div>  
        </a>
      </div>
      <div class="col-sm-4">
        <!-- small box -->
        <a href="<?=$linkSetiingFaskes?>" >
          <div class="small-box bg-green">
            <div class="inner">
              <p style="font-size:18px;font-weight:bold">Pengaturan</p>
              <h4 style="font-size: 50px; font-weight: bold; padding: 5px 0;">Profil Faskes</h4>
            </div>
            <div class="icon">
              <i class="fa fa-hospital-o"></i>
            </div>
            <div class="small-box-footer"></div>
          </div>  
        </a>
      </div>
      
      <div class="col-sm-4">
        <!-- small box -->
        <a href="<?=$linkSetiingFinger?>" >
          <div class="small-box bg-yellow">
            <div class="inner">
              <p style="font-size:18px;font-weight:bold">Pengaturan</p>
              <h4 style="font-size: 50px; font-weight: bold; padding: 5px 0;">Fingerspot Device</h4>
            </div>
            <div class="icon">
              <i class="fa fa-hand-o-up"></i>
            </div>
            <div class="small-box-footer"></div>
          </div>  
        </a>
      </div>
      <div class="col-sm-4">
        <!-- small box -->
        <a href="<?=$linkSettingPrinter?>" >
          <div class="small-box bg-red">
            <div class="inner">
              <p style="font-size:18px;font-weight:bold">Pengaturan</p>
              <h4 style="font-size: 50px; font-weight: bold; padding: 5px 0;">Printer</h4>
            </div>
            <div class="icon">
              <i class="fa fa-print"></i>
            </div>
            <div class="small-box-footer"></div>
          </div>  
        </a>
      </div>

    </div>
   
    </div>
  </div>

</section>