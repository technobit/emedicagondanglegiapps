<section class="content-header">
  <h1 class="title"><?=$title?></h1>
  <?=$breadcrumbs?>
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
    <!-- Cari Data Pasien -->
    
      <div class="box box-primary">
        <div class="box-header">
          <h3 class="box-title">List Profil Printer</h3>
          <div class="box-tools pull-right">
          <button class="btn btn-warning" id="btnAddProfil" onclick="addDevice()"><i class="fa fa-plus"></i> Tambah</button>  
          </div>
         
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table class="table table-hover" id="data-grid">
            <thead>
              <tr>
                <th>Serial Number</th>
                <th>Device Name</th>
                <th>Verification Code</th>
                <th>Activation Code</th>
                <th>Verification Key</th>
                <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
              
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>