<section class="content-header">
  <h1 class="title"><?=$title?></h1>
  <?=$breadcrumbs?>
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
    <!-- Cari Data Pasien -->
    <div class="box box-success">
      <div class="box-header with-border">
      <h3 class="box-title">Pengaturan</h3>
      </div>
    <!-- /.box-header -->
    <!-- form start -->
        <form class="form-horizontal" id="frm-pengaturan-bpjs">
          <div class="box-body">
                <div class="col-sm-6">
                    <?=$cons_id?>
                    <?=$cons_key?>
                    <?=$pcare_username?>
                    <?=$pcare_password?>
                </div>
               <div class="col-sm-12 text-center">
			<button id="saveBtn" type="button" class="btn btn-primary btn-lg"><i class="fa fa-save"></i> Simpan</button>
			<a href="<?=base_url('pengaturan/pengaturan')?>" id="cancel" class="btn btn-danger btn-lg"><i class="fa fa-ban"></i> Batal</a>
			 </div>
        </form>
    </div>
      
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>