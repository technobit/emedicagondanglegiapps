<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Pengaturan extends MY_Controller {

    var $meta_title = "Pengaturan ";
    var $meta_desc = "Pengaturan";
    var $main_title = "Pengaturan";
    var $menu_key = "pengaturan";
    var $dtBreadcrumbs = array();  

    public function __construct() {
        parent::__construct();
        
        $this->load->model('Pengaturan_profil_printer_model','pengaturan_profil');
    }
    public function index(){
        $this->dtBreadcrumbs = array(
            "Home" =>base_url(),
            "Pengaturan" => base_url('pengaturan/pengaturan'),
           
        );
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "menu_key" => $this->menu_key,
			"akses_key" => "is_view",
            "container" => $this->_build_content(),
            "custom_js" => array(
                ASSETS_JS_URL."pengaturan_printer/pengaturan_profil_printer.js"
            ),
            "custom_css" => array(),
        );  
        
        $this->_render("default",$dt);             
    }
      private function _build_content(){
        $dt = array();
        $dt['breadcrumbs'] =  $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['title'] = $this->meta_title;
        $dt['linkSettingPrinter'] = base_url('pengaturan/pengaturan-profile-printer');
        $dt['linkSetiingFaskes'] = base_url('pengaturan/pengaturan-profil-faskes');
        $dt['linkSetiingBpjs'] = base_url('pengaturan/pengaturan-bpjs-api');
        $dt['linkSetiingFinger'] = base_url('pengaturan/pengaturan-finger');
		$ret = $this->load->view("pengaturan" , $dt , true);
		return $ret;
    }
    public function test(){
        $role_id = 2;  
$menu = $this->cache->memcached->get('menu_'.$role_id);
if (!$menu){
  $this->load->driver('cache');
  $menu = $this->cache->memcached->save('foo', 'bar', 10);
  
}
echopre($menu);
    }
      

}