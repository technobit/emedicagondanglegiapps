<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Setting_bpjs extends MY_Controller {

    var $meta_title = "Pengaturan API BPJS";
    var $meta_desc = "Pengaturan API BPJS";
    var $main_title = "Pengaturan API BPJS";
    var $menu_key = "pengaturan";
    var $dtBreadcrumbs = array();  

    public function __construct() {
        parent::__construct();
        
        $this->load->model('setting_bpjs_model','pengaturan_profil');
       
    }
    public function index(){
        $this->dtBreadcrumbs = array(
            "Home" =>base_url(),
            "Pengaturan BPJS" =>'#',
           
        );
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "menu_key" => $this->menu_key,
			"akses_key" => "is_view",
            "container" => $this->_build_content(),
            "custom_js" => array(
               
                ASSETS_JS_URL."pengaturan/pengaturan_bpjs.js",
                ASSETS_URL."plugins/select2/select2.js",
            ),
            "custom_css" => array(
                ASSETS_URL."plugins/select2/select2.min.css",
            ),
        );  
        
        $this->_render("default",$dt);             
    }
      private function _build_content(){
        $dt = array();
        $arrVal = array(
            'id_profil',
            'cons_id',
            'cons_key',
            'pcare_username',
            'pcare_password',
            'id_pengaturan_bpjs'
            );
         $dataFrm = $this->pengaturan_profil->getData();
          
         foreach($arrVal as $arrFrm) {
			$$arrFrm = isset($dataFrm[$arrFrm]) ? $dataFrm[$arrFrm] : "";
        }
        $dt['breadcrumbs'] =  $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['cons_id'] = $this->form_builder->inputText("Cons ID" , "cons_id" , $cons_id  , "col-sm-3" , array("class"=> "form-control"));
        $dt['cons_key'] = $this->form_builder->inputText("Cons Key" , "cons_key" , $cons_key , "col-sm-3" , array("class"=> "form-control"));
        $dt['pcare_username'] = $this->form_builder->inputText("Pcare User" , "pcare_username" , $pcare_username  , "col-sm-3" , array("class"=> "form-control"));
        $dt['pcare_password'] = $this->form_builder->inputText("Pcare Password" , "pcare_password" , $pcare_password , "col-sm-3" , array("class"=> "form-control"));
        $dt['title'] = $this->meta_title;
		$ret = $this->load->view("setting_bpjs" , $dt , true);
		return $ret;
    }
    
     public function simpanData(){
		if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";die;
        }
      
        $status = false;
		$html = "";
		$arrInsert = array(
						 "cons_id" =>$this->input->post("cons_id"),
						 "cons_key"=> $this->input->post("cons_key"),
						 "pcare_username"=>$this->input->post("pcare_username"),
                         "pcare_password" =>$this->input->post("pcare_password"),
						 
						 );
        $resVal = $this->pengaturan_profil->saveData($arrInsert);
        $this->setJsonOutput($resVal);
	}   
   
   

}