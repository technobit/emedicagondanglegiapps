<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Setting_profil extends MY_Controller {

    var $meta_title = "Pengaturan Profil";
    var $meta_desc = "Pengaturan Profil";
    var $main_title = "Pengaturan Profil";
    var $menu_key = "pengaturan";
    var $dtBreadcrumbs = array();  

    public function __construct() {
        parent::__construct();
        
        $this->load->model('profil_printer_model','pengaturan_profil');
        $this->load->model("kecamatan_model");
    }
    public function index(){
        $this->dtBreadcrumbs = array(
            "Home" =>base_url(),
            "Pengaturan" => base_url('pengaturan/pengaturan'),
           
        );
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "menu_key" => $this->menu_key,
			"akses_key" => "is_view",
            "container" => $this->_build_content(),
            "custom_js" => array(
               
                ASSETS_JS_URL."pengaturan/pengaturan_profil.js",
                ASSETS_URL."plugins/select2/select2.js",
            ),
            "custom_css" => array(
                ASSETS_URL."plugins/select2/select2.min.css",
            ),
        );  
        
        $this->_render("default",$dt);             
    }
      private function _build_content(){
        $dt = array();
        $arrVal = array(
            'id_profil',
            'nama_faskes',
            'logo_faskes',
            'pimpinan_faskes',
            'id_provinsi',
            'id_kabupaten'
            );
         $dataFrm = $this->pengaturan_profil->getData();
         $dataProvinsi = $this->pengaturan_profil->getProvinsi();
         $arrSelectedKab = array();
         $arrSelectedKec = array();
         
         foreach($arrVal as $arrFrm) {
			$$arrFrm = isset($dataFrm[$arrFrm]) ? $dataFrm[$arrFrm] : "";
        }
         if ((!empty($id_provinsi))&&(!empty($id_kabupaten))) {
             $dataKab = $this->kecamatan_model->getDataKabupaten($id_provinsi);
             $dataKec = $this->kecamatan_model->getDataKecamatan($id_kabupaten);
             $arrSelectedKab = arrayDropdown($dataKab,'IDKabupaten','Nama');
             $arrSelectedKec = arrayDropdown($dataKec,'IDKecamatan','Nama');
         }
		$arrProvinsi = arrayDropdown($dataProvinsi,'IDProvinsi','Nama');
        $dt['breadcrumbs'] =  $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['link_image'] = $logo_faskes;
        $dt['nmFaskes'] = $this->form_builder->inputText("Nama Faskes" , "nmFaskes" , $nama_faskes  , "col-sm-3" , array("class"=> "form-control"));
        $dt['nmPimpinan'] = $this->form_builder->inputText("Nama Pimpinan" , "nmPimpinan" , $pimpinan_faskes , "col-sm-3" , array("class"=> "form-control"));
        $dt['selectProvinsi'] = $this->form_builder->inputDropdown("Provinsi" , "selectProvinsi" ,$id_provinsi, $arrProvinsi , array("onchange"=>"getKabupaten()"));
		$dt['selectKabupaten'] = $this->form_builder->inputDropdown("Kabupaten" , "selectKebupaten" ,$id_kabupaten,  $arrSelectedKab , array("onchange"=>"getKecamatan()"));
		$dt['selectKecamatan'] = $this->form_builder->inputDropdown("Kecamatan" , "selectKecamatan" ,'', $arrSelectedKec);
		
        $dt['title'] = $this->meta_title;
		$ret = $this->load->view("setting_profil" , $dt , true);
		return $ret;
    }
    
     public function simpanDataProfil(){
		if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";die;
        }
      
       if(!empty($_FILES['logo']['name'])){
            $upload = $this->_do_upload();
            $img = ASSETS_IMAGE_URL.$upload;
        }else{
             $img = "";
        }
       
        //echopre($img);die;
        $status = false;
		$html = "";
		$arrInsert = array(
						 "nama_faskes" =>$this->input->post("nmFaskes"),
						 "logo_faskes"=> $img,
						 "pimpinan_faskes"=>$this->input->post("nmPimpinan"),
                         "id_provinsi" =>$this->input->post("selectProvinsi"),
						 "id_kabupaten"=> $this->input->post("selectKebupaten"),
						 );
        $resVal = $this->pengaturan_profil->saveDataProfil($arrInsert);
        $this->setJsonOutput($resVal);
	}   
     private function _do_upload(){
        $config['upload_path']          = 'assets/img/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 500; //set max size allowed in Kilobyte
        $config['max_width']            = 5000; // set max width image allowed
        $config['max_height']           = 5000; // set max height allowed
        $config['file_name']            = round(microtime(true) * 1000); //just milisecond timestamp fot unique name
 
        $this->load->library('upload', $config);
 
        if(!$this->upload->do_upload('logo')) //upload and validate
        {
            $data['inputerror'][] = 'logo';
            $data['error_string'][] = 'Upload error: '.$this->upload->display_errors('',''); //show ajax error
            $data['status'] = FALSE;
            echo json_encode($data);
            exit();
        }
        return $this->upload->data('file_name');
    }
    public function saveDataWilayah(){
        if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";die;
        }
       // echopre($this->input->post());die;
        $arrInsert = array(
						 
						 
						 );
        $resVal = $this->pengaturan_profil->saveDataProfil($arrInsert);
        $this->setJsonOutput($resVal);
    }
    public function getDataKecamatan(){
        
		if(!$this->input->is_ajax_request()){
			echo "Die, Ilegall Access";
			die;
		}
		
		
		$status = false;
		$html = "<option value='0'>-Pilih Kecamatan-</option>";
		
		$id = $this->input->post("idKabupaten");
		$dataKabupaten = $this->kecamatan_model->getDataKecamatan($id);
		if(count($dataKabupaten) > 0){
			$status = true;
			foreach($dataKabupaten as $val){
				$html .= "<option value='".$val['IDKecamatan']."'>".$val['Nama']."</option>";
			}
		}
		$retVal['status'] = $status;
		$retVal['html'] = $html;
		echo json_encode($retVal);
	}
    public function getDataKabupaten(){
        
		if(!$this->input->is_ajax_request()){
			echo "Die, Ilegall Access";
			die;
		}
		
		$this->load->model("kecamatan_model");
		$status = false;
		$html = "<option value='0'>-Pilih Kabupaten-</option>";
		
		$idKecamatan = $this->input->post("idProvinsi");
		$dataKabupaten = $this->kecamatan_model->getDataKabupaten($idKecamatan);
		if(count($dataKabupaten) > 0){
			$status = true;
			foreach($dataKabupaten as $val){
				$html .= "<option value='".$val['IDKabupaten']."'>".$val['Nama']."</option>";
			}
		}
		$retVal['status'] = $status;
		$retVal['html'] = $html;
		echo json_encode($retVal);
	}
  

}