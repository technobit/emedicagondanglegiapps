<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Pengaturan_profil_printer extends MY_Controller {

    var $meta_title = "Pengaturan Profil Printer";
    var $meta_desc = "Pengaturan Profil Printer";
    var $main_title = "Pengaturan Profil Printer";
    var $menu_key = "pengaturan_printer";
    var $dtBreadcrumbs = array();  

    public function __construct() {
        parent::__construct();
        
        $this->load->model('Pengaturan_profil_printer_model','pengaturan_profil');
    }
    public function index(){
        $this->dtBreadcrumbs = array(
            "Home" =>base_url(),
            "Pengaturan" => base_url('pengaturan/pengaturan'),
            "Pengaturan Profil Printer" => "#",
        );
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "menu_key" => $this->menu_key,
			"akses_key" => "is_view",
            "container" => $this->_build_content(),
            "custom_js" => array(
                ASSETS_JS_URL."pengaturan/pengaturan_profil_printer.js"
            ),
            "custom_css" => array(),
        );  
        
        $this->_render("default",$dt);             
    }
      private function _build_content(){
        $dt['breadcrumbs'] =  $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dataProfilrPrinter = $this->pengaturan_profil->getData();
        $dataPengaturanPrinter = $this->pengaturan_profil->getDataPengaturanPrinter();
        $listPrinter = arrayDropdown($dataProfilrPrinter,'id_profil_printer','profile_name',array(""=>'Pilih Printer'));
        $dt['printer_loket'] = form_dropdown('printer_loket',$listPrinter,$dataPengaturanPrinter['id_printer_loket'],'id="printer_loket" style="" class="form-control"');
        $dt['printer_self_service'] = form_dropdown('printer_self_service',$listPrinter,$dataPengaturanPrinter['id_printer_self_service'],'id="printer_self_service" style="" class="form-control"');
        $dt['printer_self_kasir'] = form_dropdown('printer_self_kasir',$listPrinter,$dataPengaturanPrinter['id_printer_kasir'],'id="printer_self_kasir" style="" class="form-control"');;
        $dt['title'] = $this->meta_title;
        
        $ret = $this->load->view("pengaturan_profil_printer" , $dt , true);
        return $ret;
    }
       public function getData(){
        if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";die;
        }

        
        $dataProfilrPrinter = $this->pengaturan_profil->getData();
        $countDataProfilrPrinter = count($dataProfilrPrinter);

        $retVal['draw'] = $this->input->post('draw');
        $retVal['recordsTotal'] = 0;
        $retVal['recordsFiltered'] = 0;
        $retVal['data'] = array();
               // echopre($dataPengunjung);
        if(!empty($dataProfilrPrinter)){
            $retVal['draw'] = $this->input->post('draw');
			$retVal['recordsTotal'] = $countDataProfilrPrinter;
			$retVal['recordsFiltered'] = $countDataProfilrPrinter;
			$retVal['data'] = array();
            $no = 0;
            foreach($dataProfilrPrinter as $row){
               $no++;
                $btnEdit = '<button type="button" class="btn btn-flat btn-success" onclick="addProfile('.$row['id_profil_printer'].')" id="btnEdit"><i class="fa fa-pencil"></i> Edit</button>';
                $btnDel = '<button type="button" class="btn btn-flat btn-danger" onclick="hapusProfile('.$row['id_profil_printer'].')" id="btnDel"><i class="fa fa-trash"> </i> Hapus</button>'; 
				$retVal['data'][] = array($row['profile_name'],
                                          $row['access_profile'],
                                          $row['computer_name'],
                                          $row['printer_name'],
                                          $btnEdit.$btnDel
                                          
									);
			}
        }

        $this->setJsonOutput($retVal);
    }
     public function Form_add_profile($idProfile = ""){
        $dt = array();
        
        if (!empty($idProfile)) {
              $dataProfilrPrinter = $this->pengaturan_profil->Detail($idProfile);
        }
        
        $valArray = array(
		"id_profil_printer" ,
		"profile_name" ,
		"access_profile" ,
		"computer_name" ,
		"printer_name");
		$arrSelected = array();
		foreach($valArray as $arrFrm) {

			$$arrFrm = isset($dataProfilrPrinter[$arrFrm]) ? $dataProfilrPrinter[$arrFrm] : "";

		}
        //$dataPengunjung = $this->rekapitulasi_model->getDataJamkes($idPelayanan , $tglAwal , $tglAkhir ,$jamkes, $start , $length);
        $dt['profile_name'] = $this->form_builder->inputText("Profile Name" , "profile_name" , $profile_name , "col-sm-3" , array("class"=> "form-control"));
        $dt['access_profile'] = $this->form_builder->inputText("Access Profile" , "access_profile" , $access_profile , "col-sm-3" , array("class"=> "form-control"));
        $dt['computer_name'] = $this->form_builder->inputText("Computer Name" , "computer_name" , $computer_name , "col-sm-3" , array("class"=> "form-control"));
        $dt['printer_name'] = $this->form_builder->inputText("Printer Name" , "printer_name" , $printer_name , "col-sm-3" , array("class"=> "form-control"));
        $dt["id_profil_printer"] = $this->form_builder->inputHidden('id_profil_printer',$id_profil_printer);
        $ret = $this->load->view("Form_add_profile" , $dt );
        }     
        public function simpanData(){
		if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";die;
        }
		
		$status = false;
		$html = "";
		$id = $this->input->post("id_profil_printer");
		$arrInsert = array(
						 "profile_name" =>$this->input->post("profile_name"),
						 "access_profile"=>$this->input->post("access_profile"),
						 "computer_name"=>$this->input->post("computer_name"),
						 "printer_name"=>$this->input->post("printer_name"),
						 
						 );
		$resVal = $this->pengaturan_profil->saveData($arrInsert , $id);
		$this->setJsonOutput($resVal);
	}   
    public function simpanDataPengaturan(){
		if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";die;
        }
		//echopre($this->input->post());die;
		$status = false;
		$html = "";
		//$id = $this->input->post("id_pengaturan_printer");
		$arrInsert = array(
						 "id_printer_loket" =>$this->input->post("printer_loket"),
						 "id_printer_self_service"=>$this->input->post("printer_self_service"),
						 "id_printer_kasir"=>$this->input->post("printer_self_kasir"),
						 
						 
						 );
		$resVal = $this->pengaturan_profil->saveDataPengaturanPrinter($arrInsert);
		$this->setJsonOutput($resVal);
	}  
    
     public function getlistDropdown(){
        
		if(!$this->input->is_ajax_request()){
			echo "Die, Ilegall Access";
			die;
		}
		
		
		$status = false;
		$html = "<option value=''>-Pilih Print-</option>";
		
		$id = $this->input->post("idKabupaten");
		$dataProfilrPrinter = $this->pengaturan_profil->getData();
		if(count($dataProfilrPrinter) > 0){
			$status = true;
			foreach($dataProfilrPrinter as $val){
				$html .= "<option value='".$val['id_profil_printer']."'>".$val['profile_name']."</option>";
			}
		}
		$retVal['status'] = $status;
		$retVal['html'] = $html;
		echo json_encode($retVal);
	} 
    public function deleteData($id){
        if(!$this->input->is_ajax_request()){
			echo "Ilegal";die;
		}
		$status = false;
		$message = "Data Gagal Di hapus";
		$resDetail = $this->pengaturan_profil->delete($id);
		if($resDetail['status']){
			$status = true;
			$message = "Data Berhasil Di Hapus";	
		}
		$retVal['status'] = $status;
		$retVal['message'] = $message;
		$this->setJsonOutput($retVal);
    }
    public function test(){
        $role_id = 2;  
        $menu = $foo = $this->cache->get('menu_'.$role_id);
       if (!$menu){
        $menu_data = array('1' => 'oke', '1' => 'oke', '2' => 'oqqke',);
        $this->cache->save('menu_'.$role_id, $menu_data);
}
    }
}