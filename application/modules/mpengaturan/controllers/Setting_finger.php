<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Setting_finger extends MY_Controller {

    var $meta_title = "Pengaturan Fingerspot Device";
    var $meta_desc = "Pengaturan Fingerspot Device";
    var $main_title = "Pengaturan Fingerspot Device";
    var $menu_key = "pengaturan";
    var $dtBreadcrumbs = array();  

    public function __construct() {
        parent::__construct();
        
        $this->load->model('setting_finger_model','pengaturan_profil');
    }
    public function index(){
        $this->dtBreadcrumbs = array(
            "Home" =>base_url(),
            "Pengaturan" => base_url('pengaturan/pengaturan'),
            "Pengaturan Profil Printer" => "#",
        );
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "menu_key" => $this->menu_key,
			"akses_key" => "is_view",
            "container" => $this->_build_content(),
            "custom_js" => array(
                ASSETS_JS_URL."pengaturan/pengaturan_finger.js"
            ),
            "custom_css" => array(),
        );  
        
        $this->_render("default",$dt);             
    }
      private function _build_content(){
        $dt['breadcrumbs'] =  $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['title'] = $this->meta_title;
        
        $ret = $this->load->view("setting_finger" , $dt , true);
        return $ret;
    }
       public function getData(){
        if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";die;
        }

        
        $arrData = $this->pengaturan_profil->getData();
        $countData = count($arrData);

        $retVal['draw'] = $this->input->post('draw');
        $retVal['recordsTotal'] = 0;
        $retVal['recordsFiltered'] = 0;
        $retVal['data'] = array();
               // echopre($dataPengunjung);
        if(!empty($arrData)){
            $retVal['draw'] = $this->input->post('draw');
			$retVal['recordsTotal'] = $countData;
			$retVal['recordsFiltered'] = $countData;
			$retVal['data'] = array();
            $no = 0;
            foreach($arrData as $row){
               $no++;
                $btnEdit = '<button type="button" class="btn btn-flat btn-success" onclick="addDevice(\''.$row['sn'].'\')" id="btnEdit"><i class="fa fa-pencil"></i> Edit</button>';
                $btnDel = '<button type="button" class="btn btn-flat btn-danger" onclick="hapusDevice(\''.$row['sn'].'\')" id="btnDel"><i class="fa fa-trash"> </i> Hapus</button>'; 
				$retVal['data'][] = array($row['sn'],
                                          $row['device_name'],
                                          $row['vc'],
                                          $row['ac'],
                                          $row['vkey'],
                                          $btnEdit.$btnDel
                                          
									);
			}
        }

        $this->setJsonOutput($retVal);
    }
     public function Form_device($id = ""){
        $dt = array();
        $attr = '';
        if (!empty($id)) {
              $dataFrm = $this->pengaturan_profil->Detail($id);
              $attr = 'readonly';
            
        }
        
        $valArray = array(
		"sn" ,
		"device_name" ,
		"vc" ,
		"ac" ,
		"vkey");
		
		foreach($valArray as $arrFrm) {

			$$arrFrm = isset($dataFrm[$arrFrm]) ? $dataFrm[$arrFrm] : "";
            
		}
       $dt['hidden'] = $this->form_builder->inputHidden('hidden',$id);
        //$dataPengunjung = $this->rekapitulasi_model->getDataJamkes($idPelayanan , $tglAwal , $tglAkhir ,$jamkes, $start , $length);
        $dt['sn'] = $this->form_builder->inputText("Serial Number" , "sn" , $sn , "col-sm-3" , array("class"=> "form-control",$attr => $attr));
        $dt['device_name'] = $this->form_builder->inputText("Device Name" , "device_name" , $device_name , "col-sm-3" , array("class"=> "form-control"));
        $dt['vc'] = $this->form_builder->inputText("Verification Code" , "vc" , $vc , "col-sm-3" , array("class"=> "form-control"));
        $dt['ac'] = $this->form_builder->inputText("Activation Code" , "ac" , $ac , "col-sm-3" , array("class"=> "form-control"));
        $dt["vkey"] = $this->form_builder->inputText("Verification Key" , "vkey" , $vkey , "col-sm-3" , array("class"=> "form-control"));
        
        $ret = $this->load->view("Form_add_fingerspot" , $dt );
        }
        
             
        public function simpanData(){
		if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";die;
        }
		
		$status = false;
		$html = "";
		$id = $this->input->post("sn");
        $hidden = $this->input->post("hidden");
        $arrInsert = array();
        if (!empty($hidden)) {
            $arrInsert = array(
						 "device_name" =>$this->input->post("device_name"),
						 "vc"=>$this->input->post("vc"),
						 "ac"=>$this->input->post("ac"),
						 "vkey"=>$this->input->post("vkey"),
						 );
        }else{
            $arrInsert = array(
                         "sn" =>$id,
						 "device_name" =>$this->input->post("device_name"),
						 "vc"=>$this->input->post("vc"),
						 "ac"=>$this->input->post("ac"),
						 "vkey"=>$this->input->post("vkey"),
						 
						 );
        }
		
		$resVal = $this->pengaturan_profil->saveData($arrInsert , $id, $hidden);
		$this->setJsonOutput($resVal);
	}   
   
   public function deleteData($id){
        if(!$this->input->is_ajax_request()){
			echo "Ilegal";die;
		}
		$status = false;
		$message = "Data Gagal Di hapus";
		$resDetail = $this->pengaturan_profil->delete($id);
		if($resDetail['status']){
			$status = true;
			$message = "Data Berhasil Di Hapus";	
		}
		$retVal['status'] = $status;
		$retVal['message'] = $message;
		$this->setJsonOutput($retVal);
    }
    public function test(){
        $role_id = 2;  
        $menu = $foo = $this->cache->get('menu_'.$role_id);
       if (!$menu){
        $menu_data = array('1' => 'oke', '1' => 'oke', '2' => 'oqqke',);
        $this->cache->save('menu_'.$role_id, $menu_data);
}
    }
}