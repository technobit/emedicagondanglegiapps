<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengaturan_profil_printer_model extends MY_Model {

	var $table = "pengaturan_profil_printer";
   var $primary_key = "id_profil_printer";
    
    public function __construct(){
        parent::__construct();
        
    }

	public function getData(){

		$this->db->from($this->table);
		$res = $this->db->get();
		return $res->result_array();

	}

	
	public function saveData($arrData = array(),$id = ""){
		$message = "";
		$status = false;
		
		if(!empty($id)){
			$this->db->where($this->primary_key , $id);
			$query = $this->db->update($this->table , $arrData);
			$message = $this->getMessage("insert" , $query);
		}else{
			$this->db->set($arrData);
			$query = $this->db->insert($this->table);
			$message = $this->getMessage("update" , $query);
			$id = $this->db->insert_id();
		}		
		$status = $query;
		$retVal['status'] = $status;
		$retVal['message'] = $message;
		$retVal['id'] = $id;
		return $retVal;
	}

	public function Detail($id){
		$this->db->from($this->table);
		$this->db->where($this->primary_key , $id);
		$res = $this->db->get();
		return $res->row_array();
	}

	public function delete($id){
		$this->db->where($this->primary_key , $id);
		$q = $this->db->delete($this->table);
		
		if(!$q){
			$retVal['message'] = "Data Gagal Di Hapus";
			$retVal['status'] = false;
		}else{
			$retVal['message'] = "Data Berhasil Di Hapus";
			$retVal['status'] = true;
		}
		
		return $retVal;
	}
		public function getDataPengaturanPrinter(){

		$this->db->from('pengaturan_printer');
		$res = $this->db->get();
		return $res->row_array();

	}
	public function saveDataPengaturanPrinter($arrData = array()){
		$message = "";
		$status = false;
		$pengaturan = $this->getDataPengaturanPrinter();
		if(!empty($pengaturan)){
			
			$this->db->where('id_pengaturan_printer' , $pengaturan['id_pengaturan_printer']);
			$query = $this->db->update('pengaturan_printer' , $arrData);
			$message = $this->getMessage("insert" , $query);
			$id = $pengaturan['id_pengaturan_printer'];
		}else{
			$this->db->set($arrData);
			$query = $this->db->insert('pengaturan_printer');
			$message = $this->getMessage("update" , $query);
			$id = $this->db->insert_id();
		}		
		$status = $query;
		$retVal['status'] = $status;
		$retVal['message'] = $message;
		$retVal['id'] = $id;
		return $retVal;
	}
	
	
}

