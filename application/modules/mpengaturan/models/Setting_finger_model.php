<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting_finger_model extends MY_Model {

	var $table = "fingerspot_device";
   var $primary_key = "sn";
    
    public function __construct(){
        parent::__construct();
        
    }

	public function getData(){

		$this->db->from($this->table);
		$res = $this->db->get();
		return $res->result_array();

	}
	public function saveData($arrData = array(), $id, $hidden){
		$message = "";
		$status = false;
	
		if (!empty($hidden)) {
			$this->db->where($this->primary_key , $id);
			$query = $this->db->update($this->table , $arrData);
			$message = $this->getMessage("insert" , $query);
			$idid = $id;
		}else {
			$this->db->set($arrData);
			$query = $this->db->insert($this->table);
			$message = $this->getMessage("update" , $query);
			$idid = $this->db->insert_id();
		}
		$status = $query;
		$retVal['status'] = $status;
		$retVal['message'] = $message;
		$retVal['id'] = $idid;
		return $retVal;
	}
	
	

	public function Detail($id){
		$this->db->from($this->table);
		$this->db->where($this->primary_key , $id);
		$res = $this->db->get();
		return $res->row_array();
	}

	public function delete($id){
		$this->db->where($this->primary_key , $id);
		$q = $this->db->delete($this->table);
		
		if(!$q){
			$retVal['message'] = "Data Gagal Di Hapus";
			$retVal['status'] = false;
		}else{
			$retVal['message'] = "Data Berhasil Di Hapus";
			$retVal['status'] = true;
		}
		
		return $retVal;
	}
	public function getProvinsi(){

		$this->db->from('provinsi');
		$res = $this->db->get();
		return $res->result_array();

	}
	
}

