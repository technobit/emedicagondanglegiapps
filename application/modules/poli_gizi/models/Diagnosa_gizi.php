<?php 

class Diagnosa_gizi extends CI_Model{
    
    var $table = "data_diagnosa_gizi";
    var $primary_key = "intIdDiagnosaGizi";

    function __construct(){
        parent::__construct();
    }

    function getData(){
        $this->db->select("*");
        $ret = $this->db->get($this->table);
        return $ret->result_array();
    }

    function getListDiagnosa($where , $offset=0 , $limit=10){
        $this->db->select("intIdDiagnosaGizi as id,txtKodeDiagnosa , txtDiagnosaGizi");
        if(!empty($where)){
            $this->db->like("txtKodeDiagnosa" , $where);
            $this->db->or_like("txtDiagnosaGizi" , $where);
        }
        $this->db->from($this->table);
        $this->db->limit($limit);
        $this->db->offset($offset);
        $data = $this->db->get();
        return $data->result_array();
    }

    function getCountListDiagnosa($where){
        $this->db->select("intIdDiagnosaGizi as id,txtKodeDiagnosa , txtDiagnosaGizi");
        if(!empty($where)){
            $this->db->like("txtKodeDiagnosa" , $where);
            $this->db->or_like("txtDiagnosaGizi" , $where);
        }
        $this->db->from($this->table);
        $data = $this->db->count_all_results();
        return $data;
    }

    function detail($id){
        $this->db->select("*");
        $this->db->where($this->primary_key , $id);
        $ret = $this->db->get($this->table);
        return $ret->row_array();
    }

    function delete($id){
        $this->db->where($this->primary_key , $id);
        $ret = $this->db->delete($this->table);
        $status = $ret;
        $message = $status!=true ? "Data Gagal Di Hapus" : "Data Berhasil Di Hapus";
        $retVal = array(
            "status" => $status,
            "message" => $message,
        );
        return $retVal;
    }

    function insert($arrPost , $id){

        $retVal = array();
        if(!empty($id)){
            $this->db->where($this->primary_key , $id);
            $res = $this->db->update($this->table , $arrPost);
        }else{
            $this->db->set($arrPost);
            $res = $this->db->insert($this->table);
            $id = $this->db->insert_id();
        }

        $status = $res;
        $message = $status==true ? "Data Berhasil Di Simpan" : "Data Gagal Di Simpan";
        $retVal = array("status" => $status , "message" => $message);
        return $retVal;
    }

    function getDiagnosaGizi($txtIdRekmedDetail){

        $this->db->select("*");
        $this->db->from("rekam_medis_diagnosa_gizi");
        $this->db->join("data_diagnosa_gizi" , "rekam_medis_diagnosa_gizi.intIdDiagnosaGizi = data_diagnosa_gizi.intIdDiagnosaGizi");
        $this->db->where("rekam_medis_diagnosa_gizi.txtIdRekmedDetail" , $txtIdRekmedDetail);
        $data = $this->db->get();
        $result = $data->result_array();
        return $result;
    }
}
