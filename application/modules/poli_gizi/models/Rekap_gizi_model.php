<?php 

class Rekap_gizi_model extends CI_Model{
    var $table = "";
    

    function __construct(){
        parent::__construct();
    }

    function getDataReportGizi($dtStart , $dtEnd , $intIdPelayanan = 9){
        $this->db->select("
        R.txtIdKunjungan,
        R.dtTanggalKunjungan,
        P.txtNoAnggota,
        P.txtNamaPasien,
        R.txtUsiaPasienKunjungan,
        K.Nama,
        RMD.txtTinggi,
        RMD.txtBerat,
        RMG.*,
        DDG.txtKodeDiagnosa,
        DDG.txtDiagnosaGizi");
        $this->db->from("register R");
        $this->db->join("pasien P" , "P.txtIdPasien = R.txtIdPasien");
        $this->db->join("kelurahan K" , "P.intIdKelurahan = K.IDKelurahan");
        $this->db->join("rekam_medis_detail RMD" , "RMD.txtIdKunjungan = R.txtIdKunjungan");
        $this->db->join("rekam_medis_gizi RMG" , "RMG.txtIdRekmedDetail = RMD.txtIdRekmedDetail");
        $this->db->join("rekam_medis_diagnosa_gizi RMDG" , "RMDG.txtIdRekmedDetail = RMG.txtIdRekmedDetail");
        $this->db->join("data_diagnosa_gizi DDG" , "RMDG.intIdDiagnosaGizi = DDG.intIdDiagnosaGizi");
        $this->db->where("R.intIdPelayanan" , $intIdPelayanan);
        $this->db->where("R.dtTanggalKunjungan BETWEEN '".$dtStart."' AND '".$dtEnd."'");
        $ret = $this->db->get();
        return $ret->result_array();
    }

}

