<?php 

class Rekam_medis_gizi extends CI_Model{

    var $table = "rekam_medis_gizi";
    var $primary_key = "intIdRekamMedisGizi";

    function __construct(){
        parent::__construct();
    }

    function getDetailRekamMedisGizi($txtIdKunjungan) {

        $this->db->select("*");
        $this->db->from($this->table);
        $this->db->join("rekam_medis_detail" , "rekam_medis_detail.txtIdRekmedDetail = rekam_medis_gizi.txtIdRekmedDetail" , "left");
        $this->db->join("rekam_medis" , "rekam_medis_detail.txtIdRekamMedis = rekam_medis.txtIdRekamMedis" , "left");
        $this->db->join("pasien" , "pasien.txtIdPasien = rekam_medis.txtIdPasien" , "left");
        $this->db->where("rekam_medis_detail.txtIdKunjungan" , $txtIdKunjungan);
        $resQuery = $this->db->get();
        $retVal = $resQuery->row_array();
        return $retVal;
    }

    function getMonitoringGizi($txtIdRekamMedis) {
        $this->db->select("rekam_medis_gizi.txtMonitoring, rekam_medis_detail.dtTanggalPemeriksaan , rekam_medis_detail.txtIdKunjungan");
        $this->db->from($this->table);
        $this->db->join("rekam_medis_detail" , "rekam_medis_detail.txtIdRekmedDetail = rekam_medis_gizi.txtIdRekmedDetail" , "left");
        $this->db->join("rekam_medis" , "rekam_medis_detail.txtIdRekamMedis = rekam_medis.txtIdRekamMedis" , "left");
        $this->db->join("pasien" , "pasien.txtIdPasien = rekam_medis.txtIdPasien" , "left");
        $this->db->where("rekam_medis.txtIdRekamMedis" , $txtIdRekamMedis);
        $resQuery = $this->db->get();
        $retVal = $resQuery->result_array();
        return $retVal;
    }

    function insertRekamMedisGizi($arrPost , $id) {
        
        $retVal = array();
        $check = $this->checkRekmedGizi($id);
        if($check > 0){
            $this->db->where("txtIdRekmedDetail" , $id);
            $res = $this->db->update($this->table , $arrPost);
        }else{
            $this->db->set($arrPost);
            $res = $this->db->insert($this->table);
            $id = $this->db->insert_id();
        }

        $status = $res;
        $message = $status==true ? "Data Berhasil Di Simpan" : "Data Gagal Di Simpan";
        $retVal = array("status" => $status , "message" => $message);
        return $retVal;
    
    }

    function insertDiagnosaRekmedGizi($arrPost,$id){
        $checkDiagnosaGizi = $this->checkDiagnosaGizi($id);
        if($checkDiagnosaGizi > 0){
            $this->deleteDiagnosaGizi($id);
        }

        $resInsert = $this->db->insert_batch("rekam_medis_diagnosa_gizi",$arrPost);
        $status = !empty($resInsert) ? true : false;
        $message = $status!=true ? "Data Gagal Di Hapus" : "Data Berhasil Di Hapus";
        $retVal = array(
            "status" => $status,
            "message" => $message,
        );
        return $retVal;

    }

    function checkRekmedGizi($id){
        $this->db->where("txtIdRekmedDetail" , $id);
        $this->db->from($this->table);
        $data = $this->db->count_all_results();
        return $data;
    }

    function checkDiagnosaGizi($id){
        $this->db->where("txtIdRekmedDetail" , $id);
        $this->db->from("rekam_medis_diagnosa_gizi");
        $data = $this->db->count_all_results();
        return $data;
    }

    function deleteDiagnosaGizi($id){
        $this->db->where("txtIdRekmedDetail" , $id);
        $ret = $this->db->delete("rekam_medis_diagnosa_gizi");
        $status = $ret;
        $message = $status!=true ? "Data Gagal Di Hapus" : "Data Berhasil Di Hapus";
        $retVal = array(
            "status" => $status,
            "message" => $message,
        );
        return $retVal;
    }

    
}
