<form class="form-horizontal clearfix" method="POST" action="?" id="frm-gizi">
    <?=$this->form_builder->inputHidden("txtIdRekmedDetail" , $txtIdRekmedDetail)?>
    <?=$this->form_builder->inputHidden("txtTanggalPeriksa" , date("Y-m-d H:i:s"))?>
    <?=$this->form_builder->inputHidden("dtTanggalLahir" , $dtTanggalLahir)?>
    <?=$this->form_builder->inputHidden("modeForm" , $modeForm)?>
    <!--Antropometri-->
    <div class="form-group">
        <label class="col-sm-3 control-label form-label" for="txtPemeriksaan">Antropometri</label>
        <div class="col-sm-3">
            <label class="control-label form-label">Tinggi Badan</label>
            <div class="input-group">
                <?=form_input('txtTinggi', $txtTinggi, 'class="form-control" id="txtTinggi"')?>
                <span class="input-group-addon">cm</span>
            </div>
        </div>
        <div class="col-sm-3">
            <label class="control-label form-label">Berat Badan</label>
            <div class="input-group">
                <?=form_input('txtBerat', $txtTinggi, 'class="form-control" id="txtBerat"')?>
                <span class="input-group-addon">kg</span>
            </div>
        </div>
        <div class="col-sm-6 col-sm-offset-3">
            <label class="control-label form-label">IMT</label>
            <div class="input-group col-sm-12">
                <?=form_input('txtAntIMT', $txtAntIMT, 'readonly="" class="form-control" id="txtAntIMT"')?>
                <span class="input-group-btn">
                      <button type="button" onclick="hitungIMTGizi()" class="btn btn-info btn-flat">Hitung IMT</button>
                </span>
            </div>
        </div>
    </div>

    <!--Biokimia-->
    <div class="form-group">
        <label class="col-sm-3 control-label form-label" for="txtPemeriksaan">Biokimia</label>
        <div class="col-sm-3">
            <label class="control-label form-label">HB</label>
            <div class="input-group">
                <?=form_input('txtBioHB', $txtBioHB, 'class="form-control" id="txtBioHB"')?>
            </div>
        </div>
        <div class="col-sm-3">
            <label class="control-label form-label">Trombosit</label>
            <div class="input-group">
                <?=form_input('txtBioTrombosit', $txtBioTrombosit, 'class="form-control" id="txtBioTrombosit"')?>
            </div>
        </div>
        <div class="col-sm-3 col-sm-offset-3">
            <label class="control-label form-label">Lekosit</label>
            <div class="input-group">
                <?=form_input('txtBioLekosit', $txtBioLekosit, 'class="form-control" id="txtBioLekosit"')?>
            </div>
        </div>
        <div class="col-sm-3">
            <label class="control-label form-label">GDS</label>
            <div class="input-group">
                <?=form_input('txtBioGDS', $txtBioGDS, 'class="form-control" id="txtBioGDS"')?>
            </div>
        </div>
        <div class="col-sm-3 col-sm-offset-3">
            <label class="control-label form-label">GDP</label>
            <div class="input-group">
                <?=form_input('txtBioGDP', $txtBioGDP, 'class="form-control" id="txtBioGDP"')?>
            </div>
        </div>
        <div class="col-sm-3">
            <label class="control-label form-label">Chol</label>
            <div class="input-group">
                <?=form_input('txtBioChol', $txtBioChol, 'class="form-control" id="txtBioChol"')?>
            </div>
        </div>
        <div class="col-sm-3 col-sm-offset-3">
            <label class="control-label form-label">Urin Acid</label>
            <div class="input-group">
                <?=form_input('txtBioUricAcid', $txtBioUricAcid, 'class="form-control" id="txtBioUricAcid"')?>
            </div>
        </div>
        <div class="col-sm-3">
            <label class="control-label form-label">Lain - Lain</label>
            <div class="input-group">
                <?=form_input('txtBioLain', $txtBioLain, 'class="form-control" id="txtBioLain"')?>
            </div>
        </div>
    </div>

    <?=$this->form_builder->inputTextArea("Riwayat Gizi" , "txtRiwayatGizi" , "")?>
    <div class="form-group">
        <label class="col-sm-3 control-label form-label" for="txtPemeriksaan">Biokimia</label>
        <div class="col-sm-9">
            <label class="control-label form-label">RPD</label>
            <div class="input-group col-sm-12">
                <?=form_input('txtPersRPD', $txtPersRPD, 'class="form-control" id="txtPersRPD"')?>
            </div>
        </div>
        <div class="col-sm-9 col-sm-offset-3">
            <label class="control-label form-label">Alergi</label>
            <div class="input-group col-sm-12">
                <?=form_input('txtPersAlergi', $txtPersAlergi, 'class="form-control" id="txtPersAlergi"')?>
            </div>
        </div>
        <div class="col-sm-9 col-sm-offset-3">
            <label class="control-label form-label">Mual</label>
            <div class="input-group col-sm-12">
                <?=form_input('txtPersMual', $txtPersMual, 'class="form-control" id="txtPersMual"')?>
            </div>
        </div>
        <div class="col-sm-9 col-sm-offset-3">
            <label class="control-label form-label">Muntah</label>
            <div class="input-group col-sm-12">
                <?=form_input('txtPersMuntah', $txtPersMuntah, 'class="form-control" id="txtPersMuntah"')?>
            </div>
        </div>
        <div class="col-sm-9 col-sm-offset-3">
            <label class="control-label form-label">Lain-Lain</label>
            <div class="input-group col-sm-12">
                <?=form_input('txtPersDLL', $txtPersDLL, 'class="form-control" id="txtPersDLL"')?>
            </div>
        </div>
    </div>
    <div class="diagnosa-gizi-cont">
    <?php 
        if(($modeForm=="edit") && (!empty($detailDiagnosaGizi))) :
        $noDiagnosaGizi = 1;
        foreach($detailDiagnosaGizi as $rowDiagnosaGizi) :
    ?>
    <div class="form-group form-diagnosa-gizi" id="frmDiagnosaGizi<?=$noDiagnosaGizi?>">
            <label for="intIdDiagnosaGizi<?=$noDiagnosaGizi?>" class="col-sm-3 control-label form-label">Diagnosa Gizi <?=$noDiagnosaGizi?></label>
            <div class="col-sm-9">
            <?=form_dropdown("intIdDiagnosaGizi[]" , array($rowDiagnosaGizi['intIdDiagnosaGizi'] => $rowDiagnosaGizi['txtDiagnosaGizi']) , array($rowDiagnosaGizi['intIdDiagnosaGizi'] => $rowDiagnosaGizi['txtDiagnosaGizi']) , 'class="form-control select-diagnosa-gizi" id="intIdDiagnosaGizi<?=$noDiagnosaGizi?>"')?>
            </div>
    </div>
    <?php 
        $noDiagnosaGizi++;
        endforeach;
        else :
    ?>
    <div class="form-group form-diagnosa-gizi" id="frmDiagnosaGizi1">
            <label for="intIdDiagnosaGizi1" class="col-sm-3 control-label form-label">Diagnosa Gizi</label>
            <div class="col-sm-9">
            <?=form_dropdown("intIdDiagnosaGizi[]" , array() , "" , 'class="form-control select-diagnosa-gizi" id="intIdDiagnosaGizi1"')?>
            </div>
        </div>
    <?php 
        endif;
    ?>
        
    </div>
    
    <div class="text-right" style="margin-bottom:10px;">
        <button type="button" id="btnAddDiagnosaGizi" class="btn btn-default btn-flat">Tambah</button>
        <button type="button" id="btnDelDiagnosaGizi" class="btn btn-warning btn-flat">Hapus</button>
    </div>

    <?=$this->form_builder->inputTextArea("Rencana Intervensi" , "txtRencanaIntervensi" , $txtRencanaIntervensi)?>
    <?=$this->form_builder->inputTextArea("Jenis Diet" , "txtJenisDiet" , $txtJenisDiet)?>
    <?=$this->form_builder->inputTextArea("Monitoring" , "txtMonitoring" , $txtMonitoring)?>
    <?=$this->form_builder->inputDropdown("Dokter" , "intIdPegawai" , $selectedPegawai , $dataListPegawai)?>
    <div class="col-sm-offset-3 col-sm-9">
      <button class="btn btn-primary" id="btnSaveRekmed" type="button"><i class="fa fa-save"></i> Simpan</button>
      <button class="btn btn-info" onclick="enableForm()" style="display: none;" id="btnEditRekmed" type="button"><i class="fa fa-pencil"></i> Edit</button>
      <button class="btn btn-warning" onclick="disableForm()" id="btnCancelEditRekmed" style="display: none;" type="button"><i class="fa fa-save"></i> Batal</button>
    </div>
</form>
<script>

function formatListDiagnosaGizi (repo) {
      if (repo.loading) return repo.text;
      var markups = "<div class='select2-result-repository clearfix'>" +
          "<div class='select2-result-repository__meta'>" +
          "<div class='select2-result-repository__title'>" + repo.txtKodeDiagnosa  + "</div>"+
          "<div class='select2-result-repository__description'>" + repo.txtDiagnosaGizi + "</div>"
          +"</div></div>";
      return markups;
}

function formatListDiagnosaGiziSelection (repo) {
      return repo.txtDiagnosaGizi || repo.text;
}

$(function(){

    var modeForm = $('input[name=modeForm]').val();
    if(modeForm=='edit'){
        disableForm();
    }
     $('#frm-gizi').validate({
        ignore : "",
        rules : {
            'txtTinggi' : {
                required : true,
            },
            'txtBerat' : {
                required : true,
            }
        },
    });

    $('.select-diagnosa-gizi').select2({
        ajax: {
        url: global_url+"poli_gizi/diagnosa/getListDiagnosaGizi/",
        dataType: 'json',
        delay: 250,
        data: function (params) {
            return {
              q: params.term, // search term
              page: params.page
            };
        },
        processResults: function (data, params) {
        params.page = params.page || 1;
        return {
          results: data.items,
          pagination: {
            more: (params.page * 20) < data.total_count
        }
        };
        },
        cache : true,
  },
  escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
  minimumInputLength: 1,
  templateResult: formatListDiagnosaGizi, // omitted for brevity, see the source of this page
  templateSelection: formatListDiagnosaGiziSelection, // omitted for brevity, see the source of this page
  placeholder: 'Ketikan Diagnosa / Kode Diagnosa',
});

    $('#btnSaveRekmed').click(function(){
        if($('#frm-gizi').valid()){
            saveDiagnosaGizi();
        }
    });

    $('#btnAddDiagnosaGizi').click(function(){
        
        var num = $('.form-diagnosa-gizi').length;
        var newNum = new Number(num + 1);
        var newSection = $('#frmDiagnosaGizi'+num).clone().attr("id" , "frmDiagnosaGizi" + newNum);
        newSection.children(":first").attr("for" , "intIdDiagnosaGizi" + newNum).html("Diagnosa Gizi " + newNum);
        ///newSection.children(":nth-child(2)").children(':first').attr("id" , "intIdDiagnosaGizi" + newNum).val("");
        newSection.children(":nth-child(2)").html('<select name="intIdDiagnosaGizi[]" id="intIdDiagnosaGizi'+newNum+'" class="form-control select-diagnosa-gizi">');
        ////console.log('intIdDiagnosaGizi' + newNum);                                      
        $('#btnDelDiagnosaGizi').removeAttr("disabled" , "");
        $('.diagnosa-gizi-cont').last().append(newSection);
        initiateDiagnosaGizi('intIdDiagnosaGizi' + newNum);
    });

    $('#btnDelDiagnosaGizi').click(function(){
        var num = $('.form-diagnosa-gizi').length;
        $('#frmDiagnosaGizi'+num).remove();
        if(num-1 == 1){
            $('#btnDelDiagnosaGizi').attr("disabled" , "disabled");
        }
    });
}); 

function saveDiagnosaGizi(){
    $.ajax({
        url : global_url + 'poli_gizi/Detail/saveDataRekamMedisGizi',
        type : "POST",
        data : $('#frm-detail-register').serialize()+"&"+$('#frm-gizi').serialize(),
        success : function message(response){
            var data = jQuery.parseJSON(response);
            alertPopUp(data['status'] , data['message'] , "");
            if (data['status']==true) {
                $('input[name=txtIdRekmedDetail]').val(data['txtIdRekmedDetail']);
                disableForm();
            }
        }
    });
}

function enableForm(){
  $('#frm-gizi input').removeAttr("disabled");
  $('#frm-gizi select').removeAttr("disabled");
  $('#frm-gizi textarea').removeAttr("disabled");
  $('#frm-gizi button').removeAttr("disabled");
  $('#btnSaveRekmed').css("display" , "inline");
  $('#btnCancelEditRekmed').css("display" , "inline");
  $('#btnEditRekmed').css("display" , "none");
  $('#btnCancelRekmed').css("display" , "none");
}

function disableForm(){
  $('#frm-gizi input').attr("disabled", "disabled");
  $('#frm-gizi select').attr("disabled", "disabled");
  $('#frm-gizi textarea').attr("disabled", "disabled");
  $('#frm-gizi button').attr("disabled", "disabled");
  $('#btnSaveRekmed').css("display" , "none");
  $('#btnCancelEditRekmed').css("display" , "none");
  $('#btnEditRekmed').css("display" , "inline");
  $('#btnCancelRekmed').css("display" , "inline");
  $('#btnEditRekmed').removeAttr("disabled");
}

function initiateDiagnosaGizi(idSelect){
$('#'+idSelect).select2({
        ajax: {
        url: global_url+"poli_gizi/diagnosa/getListDiagnosaGizi/",
        dataType: 'json',
        delay: 250,
        data: function (params) {
            return {
              q: params.term, // search term
              page: params.page
            };
        },
        processResults: function (data, params) {
        params.page = params.page || 1;
        return {
          results: data.items,
          pagination: {
            more: (params.page * 20) < data.total_count
        }
        };
        },
        cache : true,
  },
  escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
  minimumInputLength: 1,
  templateResult: formatListDiagnosaGizi, // omitted for brevity, see the source of this page
  templateSelection: formatListDiagnosaGiziSelection, // omitted for brevity, see the source of this page
  placeholder: 'Ketikan Diagnosa / Kode Diagnosa',
});
}

function hitungIMTGizi(){
        var intBerat = $('#txtBerat').val();
        var intTinggi = $('#txtTinggi').val();
        if((intBerat!="") && (intTinggi!="")){
            
            var IMTValue = intBerat / Math.pow((intTinggi / 100) , 2);
            var kondisiGizi = "";
            if(IMTValue < 17.0){
                kondisiGizi = "Gizi Kurang - Sangat Kurus";
            }else if(IMTValue < 18.5){
                kondisiGizi = "Gizi Kurang - Kurus";
            }else if(IMTValue < 25.0){
                kondisiGizi = "Gizi Baik - Normal";
            }else if(IMTValue < 27.0){
                kondisiGizi = "Gizi Lebih - Gemuk";
            }else{
                kondisiGizi = "Gizi Lebih - Sangat Gemuk";
            }
            $('#txtAntIMT').val(IMTValue.toFixed(2) + ' - ('+kondisiGizi+')');
        }else{
            alert("Mohon Isi Berat Dan Tinggi Badan Terlebih Dahulu");
        }
        
    }
</script>