<section class="content-header">
  <h1 class="title"><?=$title?></h1>
  <?=$breadcrumbs?>
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
    <!-- Hasil Pencarian Data Pasien -->
    <div class="box box-success">
    <!-- form start -->
        <form class="form-horizontal" id="frm-filter">
          <div class="box-body">
            <div class="form-group">
                <label class="col-md-2 control-label">Tanggal</label>
                <div class="col-md-5">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        <input value="<?=date('Y-m-01')?>" id="filterTanggalAwal" class="form-control datepicker" type="text">
                    </div>
                </div>  
                <div class="col-md-5">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        <input value="<?=date('Y-m-d')?>" id="filterTanggalAkhir" class="form-control datepicker" type="text">
                    </div>
            </div>                           
            </div>
            <div class="col-sm-9 col-sm-offset-2" style="padding-left: 8px;">
                <button class="btn btn-primary btn-flat" id="sendBtn" type="button"><i class="fa fa-send"></i> Kirim</button>
            </div>
          </div>
        </form>
    </div>
      <div class="box box-primary">
        <!-- /.box-header -->
        <div class="box-header">
            <div class="box-tools">
                <button id="downloadExcel" class="btn btn-primary btn-flat"><i class="fa fa-download"></i> Download Excel</button>
            </div>
        </div>
        <div class="box-body table-responsive">
          <table class="table table-hover" id="table-rekap">
                <thead>
                  <tr>
                    <th rowspan="2">Tanggal</th>
                    <th rowspan="2">No Anggota</th>
                    <th rowspan="2">Nama</th>
                    <th rowspan="2">Usia Pengguna Layanan</th>
                    <th rowspan="2">Alamat</th>
                    <th colspan="3">Antropometri</th>
                    <th colspan="8">Biokimia</th>
                    <th colspan="5">Riwayat Personal</th>
                    <th rowspan="2">Riwayat Gizi</th>
                    <th rowspan="2">Diagnosa Gizi</th>
                    <th rowspan="2">Jenis Diet</th>
                  </tr>
                  <tr>
                    <th>Tinggi Badan</th>
                    <th>Berat Badan</th>
                    <th>IMT</th>
                    <th>HB</th>
                    <th>Trombosit</th>
                    <th>Lekosit</th>
                    <th>GDS</th>
                    <th>GDP</th>
                    <th>Chol</th>
                    <th>Uric Acid</th>
                    <th>Lain</th>
                    <th>RPD</th>
                    <th>Alergi</th>
                    <th>Mual</th>
                    <th>Muntah</th>
                    <th>Dll</th>
                  </tr>
                </thead>
                <tbody>
                  
                </tbody>
              </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>