<section class="content-header">
  <h1 class="title"><?=$title?></h1>
  <?=$breadcrumbs?>
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
    <!-- Hasil Pencarian Data Pasien -->
    <div class="box box-success">
    <!-- form start -->
        <form class="form-horizontal" id="frm-detail-register">
          <div class="box-body">
            <?=$frm_id_rekam_medis?>
            <?=$frm_id_pasien?>
            <?=$frm_id_pelayanan?>
            <?=$frm_id_kunjungan?>
            <?=$frm_tanggal?>
            <?=$frm_no_anggota_pasien?>
            <?=$frm_nama_pasien?>
            <?=$frm_usia_pasien?>
            <?=$frm_jaminan_kesehatan?>
            <?=$frm_no_jaminan_kesehatan?>
            <?=$frm_no_rekam_medis?>
            <?=$frm_txt_kunjungan?>
          </div>
        </form>
    </div>
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Form Rekam Medis Gizi</h3>
            <div class="box-tools pull-right">
              <button class="btn btn-success btn-flat" onclick="setPeriksaPoliSelesai()" id="btnPeriksaSelesai" type="button"><i class="fa fa-check"></i> Pemeriksaan Selesai</button>
            </div>
         </div>
        <div class="box-body">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a data-toggle="tab" href="#tab_1">Penanganan Rawat Jalan</a></li>
              <li><a id="getRekamMedis" data-toggle="tab" href="#tab_2">Riwayat Rekam Medis</a></li>
              <li><a id="getMonitoringGizi" data-toggle="tab" href="#tab_6">Monitoring Gizi</a></li>
              <li><a id="detailKondisiPasien" data-toggle="tab" href="#tab_3">Detail Kondisi Pengguna Layanan</a></li>
              <li><a data-toggle="tab" href="#tab_4">Rujukan / Pemeriksaan Lanjut</a></li>
              <li><a data-toggle="tab" href="#tab_5">Pemeriksaan Lab</a></li>
            </ul>
            <div class="tab-content">
              <!-- /.tab-pane Form Rawat Jalan-->
              <div id="tab_1" class="tab-pane active">
                <div id="frm-poli-gizi">
                  
                </div>
              </div>
              <!-- /.tab-pane Rekam Medis-->
              <div id="tab_2" class="tab-pane">
                <div class="clearfix">
                <table class="table table-hover" id="table-rekam-medis">
                  <thead>
                    <tr>
                      <th>Tanggal</th>
                      <th>Ruangan</th>
                      <th>Pemeriksaan</th>
                      <th>Diagnosa</th>
                      <th>Kode ICD 10</th>
                      <th>Terapi / Pengobatan</th>
                      <th>Dokter</th>
                      <th>Tanda Tangan</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                      
                  </tbody>
                  </table>
                </div>
              </div>

              <div id="tab_3" class="tab-pane">
                <div id="frm-detail-kondisi">
                  
                </div>                
              </div>
              <!-- /.tab-pane -->
              <div id="tab_4" class="tab-pane">
                <div id="frm-kunjungan">
                  
                </div>                
              </div>
              <div id="tab_5" class="tab-pane">
                <table class="table table-hover" id="tbl-detail-lab">
                  <thead>
                    <tr>
                      <th>Tanggal Pemeriksaan</th>
                      <th>Jenis Pemeriksaan</th>
                      <th>Hasil Pemeriksaan</th>
                    </tr>
                  </thead>
                  <tbody>
                      
                  </tbody>
                  </table>
              </div>
              <div id="tab_6" class="tab-pane">
                <table class="table table-hover" id="tbl-detail-gizi">
                
                  <thead>
                    <tr>
                      <th>Tanggal Pemeriksaan</th>
                      <th>Monitoring Dan Evaluasi</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                      
                  </tbody>
                  </table>
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
        </div>
        </div>
            
        
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>