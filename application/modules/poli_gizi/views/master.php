<section class="content-header">
  <h1 class="title"><?=$title?></h1>
  <?=$breadcrumbs?>
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-4">
        <div class="box box-success">
            <div class="box-header">
                <h3 class="box-title" id="form-title">Form Gizi</h3>
            </div>
            <div class="box-body">
                <form method="POST" id="form-diagnosa-gizi">
                <?php
                $this->form_builder->form_type = "form-inline";
                ?>
                <?=$this->form_builder->inputHidden("intIdDiagnosaGizi")?>
                <?=$this->form_builder->inputText("Kode Diagnosa" ,"txtKodeDiagnosa" , "")?>
                <?=$this->form_builder->inputText("Diagnosa Gizi" ,"txtDiagnosaGizi" , "")?>
                </form>
            </div>
            <div class="box-footer">
                <button type="button" id="btnSaveDiagnosa" class="btn btn-default"><i class="fa fa-save"></i> Simpan</button>
              </div>
        </div>    
    </div>
    <div class="col-xs-8">
    <!-- Hasil Pencarian Data Pasien -->
      <div class="box box-primary">
        <!-- /.box-header -->
        <div class="box-header">
          <h3 class="box-title">Data Diagnosa Gizi</h3>
        </div>
        <div class="box-body">
          <table class="table table-hover" id="table-data">
                <thead>
                  <tr>
                    <th>Kode</th>
                    <th>Diagnosa Gizi</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  
                </tbody>
              </table>
          
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>