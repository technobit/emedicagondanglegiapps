<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Rekapitulasi extends MY_Controller{

    var $meta_title = "Rekapitulasi - Ruangan Gizi";
    var $meta_desc = "Rekapitulasi - Ruangan Gizi";
    var $menu_key = "rekap_poli_gizi";
    var $main_title = "Rekapitulasi - Ruangan Gizi";
    var $base_url = "";

    function __construct(){
        parent::__construct();
        $this->load->model("diagnosa_gizi");
        $this->load->model("rekap_gizi_model");
        $this->base_url = $this->base_url_site."data-diagnosa-gizi/";

        $this->load->library("Excel");
    }

    public function index(){
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
			"menu_key" => $this->menu_key,
			"akses_key" => "is_view",
			"container" => $this->__build_content(),
			"custom_js" => array(
				ASSETS_JS_URL."pelayanan/poli_gizi/rekap.js",
			),
            "custom_css" => array(
				
			),
		);	
		$this->_render("default",$dt);	
    }

    private function __build_content(){
        $breadcrumbs = array(
				"Home" =>$this->base_url_site,
				"Rekapitulasi" => $this->base_url,
				"Rekap Ruangan Gizi" => "#",
        );
		
		$dt['breadcrumbs'] = $this->setBreadcrumbs($breadcrumbs);
		$dt['title'] = $this->meta_title;
        $ret = $this->load->view("rekap" , $dt, true);
		return $ret;
        
    }

    public function getDataReportGizi(){
        if(!$this->input->is_ajax_request()){
            echo "ilegal";die;
        }

        $retVal['data'] = array();
        $post = $this->input->post();
        $dtStart = $post['dtStart']. " 00:00:00";
        $dtEnd = $post['dtEnd']." 23:59:59";

        $dataResult = $this->rekap_gizi_model->getDataReportGizi($dtStart , $dtEnd);
        ///echopre($dataResult);die;
        $dataReport = $this->reformatArray($dataResult);
        ///echopre($dataReport);die;
        foreach($dataReport as $rowReport) {

            $diagnosaGizi = $rowReport['diagnosa'];
            $txtDiagnosaGizi = "";
            foreach($diagnosaGizi as $kodeDiagnosa => $rowDiagnosa) {
                $txtDiagnosaGizi .= $kodeDiagnosa ." - ".$rowDiagnosa."<br>";
            }
            $retVal['data'][] = array(
                indonesian_date($rowReport['dtTanggalKunjungan']),
                $rowReport['txtNoAnggota'],
                $rowReport['txtNamaPasien'],
                $rowReport['txtUsiaPasienKunjungan'],
                $rowReport['Nama'],
                $rowReport['txtTinggi'],
                $rowReport['txtBerat'],    
                $rowReport['txtAntIMT'],
                $rowReport['txtBioHB'],
                $rowReport['txtBioTrombosit'],
                $rowReport['txtBioLekosit'],
                $rowReport['txtBioGDS'],
                $rowReport['txtBioGDP'],
                $rowReport['txtBioChol'],
                $rowReport['txtBioUricAcid'],
                $rowReport['txtBioLain'],
                $rowReport['txtPersRPD'],
                $rowReport['txtPersAlergi'],
                $rowReport['txtPersMual'],
                $rowReport['txtPersMuntah'],
                $rowReport['txtPersDLL'],
                $rowReport['txtRiwayatGizi'],
                $txtDiagnosaGizi,
                $rowReport['txtJenisDiet'],
            );
        }
        echo json_encode($retVal);
    }

    public function getData(){
        if(!$this->input->is_ajax_request()){
            echo "ilegal";die;
        }

        $retVal['data'] = array();
        $dataDiagnosa = $this->diagnosa_gizi->getData();
        foreach($dataDiagnosa as $rowDiagnosa) {
            $btnEdit = '<button class="btn btn-primary btn-xs btn-flat" onclick="detailDiagnosa('.$rowDiagnosa['intIdDiagnosaGizi'].')">Edit</button>';
            $btnHapus = '<button class="btn btn-warning btn-xs btn-flat" onclick="deleteDiagnosa('.$rowDiagnosa['intIdDiagnosaGizi'].')">Delete</button>';
            $btnAksi = $btnEdit . $btnHapus;
            $retVal['data'][] = array(
                $rowDiagnosa['txtKodeDiagnosa'],
                $rowDiagnosa['txtDiagnosaGizi'],
                $btnAksi
            );
        }

        echo json_encode($retVal);
    }

    public function downloadRekapPoliGizi($dtStart , $dtEnd){
        
        $dtStart = $dtStart. " 00:00:00";
        $dtEnd = $dtEnd." 23:59:59";

        $dataResult = $this->rekap_gizi_model->getDataReportGizi($dtStart , $dtEnd);
        ///echopre($dataResult);die;
        $dataReport = $this->reformatArray($dataResult);
        ///echopre($dataReport);die;
        $this->excel->setActiveSheetIndex(0);
        $this->excel->getActiveSheet()->setTitle('Sheet 1');
        $this->excel->getActiveSheet()->setCellValue('A1', 'Puskesmas '.APP_KELURAHAN . " - Rekapitulasi Poli Gizi");
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(20);
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->mergeCells('A1:N1');
        $this->excel->getActiveSheet()->setCellValue('A3', 'Tanggal Awal');
        $this->excel->getActiveSheet()->setCellValue('A4', 'Tanggal Akhir');
        $this->excel->getActiveSheet()->setCellValue('B3', $dtStart);
        $this->excel->getActiveSheet()->setCellValue('B4', $dtEnd);        

        $this->excel->getActiveSheet()->setCellValue('A6', "No");
        $this->excel->getActiveSheet()->mergeCells('A6:A7');
        $this->excel->getActiveSheet()->setCellValue('B6', "No Anggota");
        $this->excel->getActiveSheet()->mergeCells('B6:B7');
        $this->excel->getActiveSheet()->setCellValue('C6', "Nama");
        $this->excel->getActiveSheet()->mergeCells('C6:C7');
        $this->excel->getActiveSheet()->setCellValue('D6', "Usia Pasien");
        $this->excel->getActiveSheet()->mergeCells('D6:D7');
        $this->excel->getActiveSheet()->setCellValue('E6', "Alamat");
        $this->excel->getActiveSheet()->mergeCells('E6:E7');

        $this->excel->getActiveSheet()->setCellValue('F6', "Antropometri");
        $this->excel->getActiveSheet()->mergeCells('F6:H6');
        $this->excel->getActiveSheet()->setCellValue('I6', "Biokimia");
        $this->excel->getActiveSheet()->mergeCells('I6:P6');
        $this->excel->getActiveSheet()->setCellValue('Q6', "Riwayat Personal");
        $this->excel->getActiveSheet()->mergeCells('Q6:U6');
        $this->excel->getActiveSheet()->setCellValue('V6', "Riwayat Gizi");
        $this->excel->getActiveSheet()->mergeCells('V6:V7');
        $this->excel->getActiveSheet()->setCellValue('W6', "Diagnosa Gizi");
        $this->excel->getActiveSheet()->mergeCells('W6:W7');
        $this->excel->getActiveSheet()->setCellValue('X6', "Jenis Diet");
        $this->excel->getActiveSheet()->mergeCells('X6:X7');

        /// Antropometri Header 
        $this->excel->getActiveSheet()->setCellValue('F7', "Tinggi Badan");
        $this->excel->getActiveSheet()->setCellValue('G7', "Berat Badan");
        $this->excel->getActiveSheet()->setCellValue('H7', "IMT");

        /// BioKimia Header 
        $this->excel->getActiveSheet()->setCellValue('I7', "HB");
        $this->excel->getActiveSheet()->setCellValue('J7', "Trombosit");
        $this->excel->getActiveSheet()->setCellValue('K7', "Lekosit");
        $this->excel->getActiveSheet()->setCellValue('L7', "GDS");
        $this->excel->getActiveSheet()->setCellValue('M7', "GDP");
        $this->excel->getActiveSheet()->setCellValue('N7', "Chol");
        $this->excel->getActiveSheet()->setCellValue('O7', "Uric Acid");
        $this->excel->getActiveSheet()->setCellValue('P7', "Lain");

        /// Riwayat Personal Header 
        $this->excel->getActiveSheet()->setCellValue('Q7', "RPD");
        $this->excel->getActiveSheet()->setCellValue('R7', "Alergi");
        $this->excel->getActiveSheet()->setCellValue('S7', "Mual");
        $this->excel->getActiveSheet()->setCellValue('T7', "Muntah");
        $this->excel->getActiveSheet()->setCellValue('U7', "DLL");

        $indexNo = 8;
        $no = 1;

        foreach($dataReport as $rowReport) {
            $diagnosaGizi = $rowReport['diagnosa'];
            $txtDiagnosaGizi = "";
            foreach($diagnosaGizi as $kodeDiagnosa => $rowDiagnosa) {
                $txtDiagnosaGizi .= $kodeDiagnosa ." - ".$rowDiagnosa."<br>";
            }
            
            $this->excel->getActiveSheet()->setCellValue('A'.$indexNo, $no);
            $this->excel->getActiveSheet()->setCellValue('B'.$indexNo, $rowReport['txtNoAnggota']);
            $this->excel->getActiveSheet()->setCellValue('C'.$indexNo, $rowReport['txtNamaPasien']);
            $this->excel->getActiveSheet()->setCellValue('D'.$indexNo, $rowReport['txtUsiaPasienKunjungan']);
            $this->excel->getActiveSheet()->setCellValue('E'.$indexNo, $rowReport['Nama']);
            $this->excel->getActiveSheet()->setCellValue('F'.$indexNo, $rowReport['txtTinggi']);
            $this->excel->getActiveSheet()->setCellValue('G'.$indexNo, $rowReport['txtBerat']);
            $this->excel->getActiveSheet()->setCellValue('H'.$indexNo, $rowReport['txtAntIMT']);
            $this->excel->getActiveSheet()->setCellValue('I'.$indexNo, $rowReport['txtBioHB']);
            $this->excel->getActiveSheet()->setCellValue('J'.$indexNo, $rowReport['txtBioTrombosit']);
            $this->excel->getActiveSheet()->setCellValue('K'.$indexNo, $rowReport['txtBioLekosit']);
            $this->excel->getActiveSheet()->setCellValue('L'.$indexNo, $rowReport['txtBioGDS']);
            $this->excel->getActiveSheet()->setCellValue('M'.$indexNo, $rowReport['txtBioGDP']);
            $this->excel->getActiveSheet()->setCellValue('N'.$indexNo, $rowReport['txtBioChol']);
            $this->excel->getActiveSheet()->setCellValue('O'.$indexNo, $rowReport['txtBioUricAcid']);
            $this->excel->getActiveSheet()->setCellValue('P'.$indexNo, $rowReport['txtBioLain']);

            $this->excel->getActiveSheet()->setCellValue('Q'.$indexNo, $rowReport['txtPersRPD']);
            $this->excel->getActiveSheet()->setCellValue('R'.$indexNo, $rowReport['txtPersAlergi']);
            $this->excel->getActiveSheet()->setCellValue('S'.$indexNo, $rowReport['txtPersMual']);
            $this->excel->getActiveSheet()->setCellValue('T'.$indexNo, $rowReport['txtPersMuntah']);
            $this->excel->getActiveSheet()->setCellValue('U'.$indexNo, $rowReport['txtPersDLL']);

            $this->excel->getActiveSheet()->setCellValue('V'.$indexNo, $rowReport['txtRiwayatGizi']);
            $this->excel->getActiveSheet()->setCellValue('W'.$indexNo, $txtDiagnosaGizi);
            $this->excel->getActiveSheet()->setCellValue('X'.$indexNo, $rowReport['txtJenisDiet']);
            $indexNo++;
            $no++;
        }
        $fileName = "Rekap_Ruangan_Gizi";
        $this->getOutput($fileName);
    }

    private function reformatArray($dataResult){
        $dataReport = array();
        if(!empty($dataResult)){
            foreach($dataResult as $indexReport => $rowReport){
                $diagnosaGizi = $rowReport['txtDiagnosaGizi'] . "-" . $rowReport['txtKodeDiagnosa'];
                $dataReport[$rowReport['txtIdKunjungan']]["dtTanggalKunjungan"] = $rowReport['dtTanggalKunjungan'];
                $dataReport[$rowReport['txtIdKunjungan']]["txtNoAnggota"] = $rowReport['txtNoAnggota'];
                $dataReport[$rowReport['txtIdKunjungan']]["txtNamaPasien"] = $rowReport['txtNamaPasien'];
                $dataReport[$rowReport['txtIdKunjungan']]["txtUsiaPasienKunjungan"] = $rowReport['txtUsiaPasienKunjungan'];
                $dataReport[$rowReport['txtIdKunjungan']]["Nama"] = $rowReport['Nama'];
                $dataReport[$rowReport['txtIdKunjungan']]["txtTinggi"] = $rowReport['txtTinggi'];
                $dataReport[$rowReport['txtIdKunjungan']]["txtBerat"] = $rowReport['txtBerat'];
                $dataReport[$rowReport['txtIdKunjungan']]["txtAntIMT"] = $rowReport['txtAntIMT'];
                $dataReport[$rowReport['txtIdKunjungan']]["txtBioHB"] = $rowReport['txtBioHB'];
                $dataReport[$rowReport['txtIdKunjungan']]["txtBioLekosit"] = $rowReport['txtBioLekosit'];
                $dataReport[$rowReport['txtIdKunjungan']]["txtBioGDP"] = $rowReport['txtBioGDP'];
                $dataReport[$rowReport['txtIdKunjungan']]["txtBioUricAcid"] = $rowReport['txtBioUricAcid'];
                $dataReport[$rowReport['txtIdKunjungan']]["txtBioTrombosit"] = $rowReport['txtBioTrombosit'];
                $dataReport[$rowReport['txtIdKunjungan']]["txtBioGDS"] = $rowReport['txtBioGDS'];
                $dataReport[$rowReport['txtIdKunjungan']]["txtBioChol"] = $rowReport['txtBioChol'];
                $dataReport[$rowReport['txtIdKunjungan']]["txtBioLain"] = $rowReport['txtBioLain'];
                $dataReport[$rowReport['txtIdKunjungan']]["txtRiwayatGizi"] = $rowReport['txtRiwayatGizi'];
                $dataReport[$rowReport['txtIdKunjungan']]["txtPersRPD"] = $rowReport['txtPersRPD'];
                $dataReport[$rowReport['txtIdKunjungan']]["txtPersAlergi"] = $rowReport['txtPersAlergi'];
                $dataReport[$rowReport['txtIdKunjungan']]["txtPersMual"] = $rowReport['txtPersMual'];
                $dataReport[$rowReport['txtIdKunjungan']]["txtPersMuntah"] = $rowReport['txtPersMuntah'];
                $dataReport[$rowReport['txtIdKunjungan']]["txtPersDLL"] = $rowReport['txtPersDLL'];
                $dataReport[$rowReport['txtIdKunjungan']]["txtRencanaIntervensi"] = $rowReport['txtRencanaIntervensi'];
                $dataReport[$rowReport['txtIdKunjungan']]["txtJenisDiet"] = $rowReport['txtJenisDiet'];
                $dataReport[$rowReport['txtIdKunjungan']]["txtMonitoring"] = $rowReport['txtMonitoring'];
                $dataReport[$rowReport['txtIdKunjungan']]["diagnosa"][$rowReport['txtKodeDiagnosa']] = $diagnosaGizi;
            }
        }

        return $dataReport;
        
    }

    private function getOutputGizi( $filename){
        ///
        $filename = date('YmdHis')."-".$filename;
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'.xls"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $objWriter->save('php://output');
    }

}