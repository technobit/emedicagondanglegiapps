<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Detail extends MY_Controller {   

    var $meta_title = "Ruangan Gizi";
    var $meta_desc = "Ruangan Gizi";
	var $main_title = "Ruangan Gizi";
    var $base_url = "";
    var $menu_key = "poli_gizi";
	var $intIdPoli = 9;
    public function __construct(){
        parent::__construct();
        $this->load->model("pelayanan_model");
        $this->load->model("register_model");
        $this->load->model("pasien_model");
		$this->load->model("pegawai_model");
        $this->load->model("rekam_medis_gizi");
		$this->load->model("diagnosa_gizi");
        $this->load->model("rekam_medis_model" , "rekam_medis");
		$this->base_url = $this->base_url_site."poli-gizi/";
    }    

    public function index($txtIdKunjungan) {

        $dt = array(
			"container" => $this->__build_detail( $txtIdKunjungan ),
			"menu_key" => $this->menu_key,
			"akses_key" => "is_insert",	
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
			"custom_js" => array(
				ASSETS_URL."plugins/select2/select2.js",
				ASSETS_URL."plugins/validate/jquery.validate_1.11.1.min.js",
                ASSETS_JS_URL."pelayanan/poli_gizi/form.js",
			),
            "custom_css" => array(
				ASSETS_URL."plugins/select2/select2.css",
			),
		);	
		$this->_render("default",$dt);	    
    }

    public function __build_detail($txtIdKunjungan){

        $detailPasien = $this->register_model->getAllDetailPasienRegistered($txtIdKunjungan);
		if(count($detailPasien) < 1){
            show_404();
        }
		$statusPelayananPoli = $detailPasien['bitIsPoli'];

		if($statusPelayananPoli==1){
			/// Update To Sedang Di Layani
			$this->updateStatusRegisterPoli($txtIdKunjungan , 2);	
		}
		
		$idPasien = $detailPasien['txtIdPasien'];
		$idPelayanan = $detailPasien['intIdPelayanan'];
        $idRekamMedis = $detailPasien['txtIdRekamMedis'];
		$noRekamMedis = $detailPasien['txtNoAnggota'];
		
		$nama_poli = $detailPasien['txtNama'];
        $this->meta_title = $nama_poli;
        $this->meta_desc = "Fasilitas ".$nama_poli;
		
		//// Update Status Menjadi Sedang Di Layani
		$dt['title'] = "Pemeriksaan Rawat Jalan ".$nama_poli;
		$dt['form_title'] = "No Pengguna Layanan ".$detailPasien['txtNoAnggota'];
		
		/// Build Header Form -> Hidden
		$dt['frm_id_pasien'] = $this->form_builder->inputHidden("txtIdPasien" , $detailPasien['txtIdPasien']);
		$dt['frm_id_rekam_medis'] = $this->form_builder->inputHidden("txtIdRekamMedis" , $idRekamMedis);
		$dt['frm_id_pelayanan'] = $this->form_builder->inputHidden("txtIdPelayanan" , $detailPasien['intIdPelayanan']);
		$dt['frm_id_kunjungan'] = $this->form_builder->inputHidden("txtIdKunjungan" , $txtIdKunjungan);
		
		///// Builder To View
		$dt['frm_tanggal'] = $this->form_builder->inputText("Tanggal Pemeriksaan" , "txtTanggalPemeriksaan", indonesian_date($detailPasien['dtTanggalKunjungan']) , "col-sm-3" , array("readonly"=>"readonly"));
		$dt['frm_no_anggota_pasien'] = $this->form_builder->inputText("No Anggota" , "txtNoAnggotaPasien", $detailPasien['txtNoAnggota'] , "col-sm-3" , array("readonly"=>"readonly"));
		$dt['frm_nama_pasien'] = $this->form_builder->inputText("Nama Pengguna Layanan" , "txtNamaPasien", $detailPasien['txtNamaPasien'] , "col-sm-3" , array("readonly"=>"readonly"));
		$dt['frm_usia_pasien'] = $this->form_builder->inputText("Usia Pengguna Layanan" , "txtUsiaPasien", $detailPasien['txtUsiaPasienKunjungan'] , "col-sm-3" , array("readonly"=>"readonly"));
		$dt['frm_jaminan_kesehatan'] = $this->form_builder->inputText("Jaminan Kesehatan" , "txtJamkes", $detailPasien['txtNamaJaminan'] , "col-sm-3" , array("readonly"=>"readonly"));
		$dt['frm_no_jaminan_kesehatan'] = $this->form_builder->inputText("No Jaminan Kesehatan" , "txtNoJamkes", $detailPasien['txtNoJaminanKesehatan'] , "col-sm-3" , array("readonly"=>"readonly"));
		$dt['frm_no_rekam_medis'] = $this->form_builder->inputText("No Rekam Medis" , "txtNoRekamMedis", $noRekamMedis, "col-sm-3" , array("readonly"=>"readonly"));
		$dt['frm_txt_kunjungan'] = $this->form_builder->inputTextArea("Keterangan Kunjungan" , "txtKetKunjungan", $detailPasien['txtKeteranganKunjungan'], "col-sm-3", array("readonly"=>"readonly"));
		
		$breadcrumbs = array(
                        "Home" =>$this->base_url_site,
                        $nama_poli => $this->base_url,
                        "Data Pelayanan ".$nama_poli => "#",
        );
        $dt['breadcrumbs'] = $this->setBreadcrumbs($breadcrumbs);
		$ret = $this->load->view("form" , $dt,true);
        return $ret;  
    }

    private function updateStatusRegisterPoli($idKunjungan , $statusPelayanan = 2){

		$arrUpdate = array(
			"bitIsPoli" => $statusPelayanan,
			"dtLastUpdateKunjungan"=> date("Y-m-d H:i:s")
		);
		$resUpdate = $this->register_model->update($arrUpdate , $idKunjungan);
		return $resUpdate;
	}

    //// Get Form Poli Gizi
    public function getFormPoliGizi(){

        if(!$this->input->is_ajax_request()){
            echo "ilegal";die;
        }

		$dt = array();
        $txtIdKunjungan = $this->input->post("idKunjungan");
        $detailGizi = $this->rekam_medis_gizi->getDetailRekamMedisGizi($txtIdKunjungan);
		$detailPasien = $this->register_model->getAllDetailPasienRegistered($txtIdKunjungan);
		$modeForm = "insert";
		$arrForm = array(
			"txtIdRekmedDetail" ,
			"txtTanggalPeriksa" ,
			"dtTanggalLahir" ,
			"txtTinggi" ,
			"txtBerat" ,
			"txtAntIMT",
			"txtBioHB",
			"txtBioTrombosit",
			"txtBioLekosit",
			"txtBioGDS",
			"txtBioGDP",
			"txtBioChol",
			"txtBioUricAcid",
			"txtBioLain",
			"txtRiwayatGizi",
			"txtPersAlergi",
			"txtPersMual",
			"txtPersMuntah",
			"txtPersDLL",
			"txtPersRPD",
			"txtRencanaIntervensi",
			"txtJenisDiet",
			"intIdPegawai",
			"txtMonitoring",
		);

		foreach($arrForm as $rowForm){
			$dt[$rowForm] = isset($detailGizi[$rowForm]) ? $detailGizi[$rowForm] : "";
		}
		$detailDiagnosaGizi = array();
		$selectedPegawai = $detailGizi['intIdPegawai'];
		if(!empty($detailGizi)){
			///echopre($dt);die;
			$modeForm = "edit";
			$txtIdRekmedDetail = $detailGizi['txtIdRekmedDetail'];
			$detailDiagnosaGizi = $this->diagnosa_gizi->getDiagnosaGizi($txtIdRekmedDetail);
			$$selectedPegawai = $this->session->userdata("sipt_id_pegawai"); 
        }
		///echopre($detailPasien);
		$listPegawai = $this->pegawai_model->getListPegawai();
		$dt['dataListPegawai'] = arrayDropdown($listPegawai , "intIdPegawai" , "txtNamaPegawai");
		$dt['detailDiagnosaGizi'] = $detailDiagnosaGizi;
		$dt['modeForm'] = $modeForm;
		$dt['selectedPegawai'] = $selectedPegawai;
		////$dt['dtTanggalLahir'] = $detailGizi['dtTanggalLahir'];
        $this->load->view("form_gizi" , $dt);
    }

	public function saveDataRekamMedisGizi(){
		if(!$this->input->is_ajax_request()){
            echo "ilegal";die;
        }

		$status = false;
		$message = 'Data Gagal Di Simpan';
		$retVal = array();
		$post = $this->input->post();
		$txtIdRekamMedis = $post['txtIdRekamMedis'];
		if(!empty($post)){
			//// Save Rekmed Detail
			
			$txtTanggalPemeriksaan = $post['txtTanggalPeriksa'];
			$txtTanggals = explode(" ", $txtTanggalPemeriksaan);
			$txtTanggalPeriksa = $txtTanggals[0];
			$txtTanggalLahirPasien = $post["dtTanggalLahir"];
			$usiaPasien = getIntervalDays($txtTanggalPeriksa , $txtTanggalLahirPasien);

			$txtIdRekmedDetail = $post['txtIdRekmedDetail'];
			$arrDetailRekamMedis = array(
				"txtIdRekamMedis"  => $txtIdRekamMedis,
				"txtIdKunjungan"   => $post['txtIdKunjungan'],
				"intIdPelayanan"   => $post['txtIdPelayanan'],
				"intUsiaHari"   => $usiaPasien,
				"txtAnamnesa"   => "",
				"txtPemeriksaan"   => "",
				"txtTinggi"   => $post['txtTinggi'],
				"txtBerat"   => $post['txtBerat'],
				"txtSistole"   => "",
				"txtDiastole"   => "",
				"txtRespiratoryRate"  => "",
				"txtHeartRate"   => "",
				"txtKeterangan"   => "",
				"txtOdontogram"   => "",
				"txtPengobatan"   => "",
				"intIdPegawai"   => $this->input->post("intIdPegawai"),
				"dtTanggalPemeriksaan" => $txtTanggalPemeriksaan,
				"bitStatusRekmedDetail"   => 1,
			);

			
			if(empty($txtIdRekmedDetail)){
				$arrDetailRekamMedis['dtCreatedDate'] = $txtTanggalPemeriksaan!="" ? $txtTanggalPemeriksaan : date("Y-m-d H:i:s");
				$arrDetailRekamMedis['dtLastUpdate'] = date("Y-m-d H:i:s");
				$retValDetail = $this->rekam_medis->insertDetailRekamMedis($arrDetailRekamMedis);	
			}else{
				$arrDetailRekamMedis['dtLastUpdate'] = date("Y-m-d H:i:s");
				$retValDetail = $this->rekam_medis->updateDetailRekamMedis($arrDetailRekamMedis, $txtIdRekmedDetail);	
			}
			$txtIdRekmedDetail = !empty($retValDetail['id']) ? $retValDetail['id'] : $txtIdRekmedDetail;
			
			//// Save Rekmed Gizi 
			$arrRekmedGizi = array(
				"txtIdRekmedDetail" => $txtIdRekmedDetail,
				"txtAntIMT" => $post['txtAntIMT'],
				"txtBioHB" => $post['txtBioHB'],
				"txtBioLekosit" => $post['txtBioLekosit'],
				"txtBioGDP" => $post['txtBioGDP'],
				"txtBioUricAcid" => $post['txtBioUricAcid'],
				"txtBioTrombosit" => $post['txtBioTrombosit'],
				"txtBioGDS" => $post['txtBioGDS'],
				"txtBioChol" => $post['txtBioChol'],
				"txtBioLain" => $post['txtBioLain'],
				"txtRiwayatGizi" => $post['txtRiwayatGizi'],
				"txtPersRPD" => $post['txtPersRPD'],
				"txtPersAlergi" => $post['txtPersAlergi'],
				"txtPersMual" => $post['txtPersMual'],
				"txtPersMuntah" => $post['txtPersMuntah'],
				"txtPersDLL" => $post['txtPersDLL'],
				"txtRencanaIntervensi" => $post['txtRencanaIntervensi'],
				"txtJenisDiet" => $post['txtJenisDiet'],
				"txtMonitoring" => $post['txtMonitoring'],
			);
			$retValGizi = $this->rekam_medis_gizi->insertRekamMedisGizi($arrRekmedGizi , $txtIdRekmedDetail);
			
			$statusGizi = $retValGizi['status'];
			$message = $retValGizi['message'];
			$status = $statusGizi;
			//// Save Diagnosa Gizi
			if(($statusGizi==true) && (!empty($post['intIdDiagnosaGizi']))){
				$intIdDiagnosa = $post['intIdDiagnosaGizi'];
				$arrPostDiagnosa = array();
				foreach($intIdDiagnosa as $rowDiagnosa) {
					$arrPostDiagnosa[] = array(
						"txtIdRekmedDetail" => $txtIdRekmedDetail,
						"intIdDiagnosaGizi" => $rowDiagnosa
					);
				}
				$retValDiagnosaGizi = $this->rekam_medis_gizi->insertDiagnosaRekmedGizi($arrPostDiagnosa , $txtIdRekmedDetail);
			}
		}
		$retVal['status'] = $status;
		$retVal['message'] = $message;
		$retVal['txtIdRekamMedisDetail'] = $txtIdRekmedDetail;
		echo json_encode($retVal);
	}

    public function getMonitoringGizi(){
		if(!$this->input->is_ajax_request()){
            echo "ilegal";die;
        }

		$txtIdRekamMedis = $this->input->post("txtIdRekamMedis");
		$data = $this->rekam_medis_gizi->getMonitoringGizi($txtIdRekamMedis);
		
		$retVal['data'] = array();
		if(!empty($data)){
			foreach($data as $rowGizi){
				$btnAksi = '<button class="btn btn-default btn-flat" onclick="getDetailMonitoringGizi(\''.$rowGizi['txtIdKunjungan'].'\')"><i class="fa fa-edit"></i> Detail</button>';
				$retVal['data'][] = array(
					indonesian_date($rowGizi['dtTanggalPemeriksaan']),
					$rowGizi['txtMonitoring'],
					$btnAksi
				);
			}
		}
		echo json_encode($retVal);
    }

}