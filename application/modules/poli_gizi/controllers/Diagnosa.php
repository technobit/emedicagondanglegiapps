<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Diagnosa extends MY_Controller{

    var $meta_title = "Master Data - Diagnosa Gizi";
    var $meta_desc = "Master Data - Diagnosa Gizi";
    var $menu_key = "master_diagnosa_gizi";
    var $main_title = "Pengguna Layanan";
    var $base_url = "";

    function __construct(){
        parent::__construct();
        $this->load->model("diagnosa_gizi");
        $this->base_url = $this->base_url_site."data-diagnosa-gizi/";
    }

    public function index(){
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
			"menu_key" => "master_diagnosa_gizi",
			"akses_key" => "is_view",
			"container" => $this->__build_content(),
			"custom_js" => array(
                ASSETS_URL."plugins/validate/jquery.validate_1.11.1.min.js",
				ASSETS_JS_URL."master_data/diagnosa_gizi/content.js",
			),
            "custom_css" => array(
				
			),
		);	
		$this->_render("default",$dt);	
    }

    private function __build_content(){
        $breadcrumbs = array(
				"Home" =>$this->base_url_site,
				"Master Data" => $this->base_url,
				"Data Diagnosa Gizi" => "#",
        );
		
		$dt['breadcrumbs'] = $this->setBreadcrumbs($breadcrumbs);
		$dt['title'] = $this->meta_title;

        $ret = $this->load->view("master" , $dt, true);
		return $ret;
        
    }

    public function getData(){
        if(!$this->input->is_ajax_request()){
            echo "ilegal";die;
        }

        $retVal['data'] = array();
        $dataDiagnosa = $this->diagnosa_gizi->getData();
        foreach($dataDiagnosa as $rowDiagnosa) {
            $btnEdit = '<button class="btn btn-primary btn-xs btn-flat" onclick="detailDiagnosa('.$rowDiagnosa['intIdDiagnosaGizi'].')">Edit</button>';
            $btnHapus = '<button class="btn btn-warning btn-xs btn-flat" onclick="deleteDiagnosa('.$rowDiagnosa['intIdDiagnosaGizi'].')">Delete</button>';
            $btnAksi = $btnEdit . $btnHapus;
            $retVal['data'][] = array(
                $rowDiagnosa['txtKodeDiagnosa'],
                $rowDiagnosa['txtDiagnosaGizi'],
                $btnAksi
            );
        }

        echo json_encode($retVal);
    }

    public function insert(){
        if(!$this->input->is_ajax_request()){
            echo "ilegal";die;
        }

        $retVal = array("status" => false ,"message" => "Data Gagal Di Simpan");
        $post = $this->input->post();

        $primaryID = $post['intIdDiagnosaGizi'];
        $arrPost = array(
            "txtKodeDiagnosa" => $post['txtKodeDiagnosa'],
            "txtDiagnosaGizi" => $post['txtDiagnosaGizi'],
        );
        $retVal = $this->diagnosa_gizi->insert($arrPost , $primaryID);
        echo json_encode($retVal);
    }

    public function detail(){
        if(!$this->input->is_ajax_request()){
            echo "ilegal";die;
        }

        $id = $this->input->post("id");
        $detail = $this->diagnosa_gizi->detail($id);
        echo json_encode($detail);
    }

    public function delete(){
        if(!$this->input->is_ajax_request()){
            echo "ilegal";die;
        }

        $id = $this->input->post("id");
        $detail = $this->diagnosa_gizi->delete($id);
        echo json_encode($detail);
    }

    public function getListDiagnosaGizi(){
        if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";
        }
        $where = $this->input->get("q");
        $limit = 20;
        $page = !empty($_GET['page']) ? $this->input->get("page") : 1;
        $start = ($page - 1) * $limit;
        $data = $this->diagnosa_gizi->getListDiagnosa($where , $start , $limit);
        $jumlah = $this->diagnosa_gizi->getCountListDiagnosa($where);
        $retVal = array();
        $retVal['total_count'] = $jumlah;
        $retVal['incomplete_results'] = false;
        $retVal['items'] = $data;
        echo json_encode($retVal);
    }
}