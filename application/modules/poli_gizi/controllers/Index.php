<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Index extends MY_Controller {   

    var $meta_title = "Ruangan Gizi";
    var $meta_desc = "Ruangan Gizi";
	var $main_title = "Ruangan Gizi";
    var $base_url = "";
    var $menu_key = "poli_gizi";
	var $intIdPoli = 9;
    public function __construct(){
        parent::__construct();
        $this->load->model("pelayanan_model");
        $this->load->model("register_model");
        $this->load->model("pasien_model");
        $this->load->model("rekam_medis_model" , "rekam_medis");
		$this->base_url = $this->base_url_site."poli-gizi/";
    }    

    public function index($idPelayanan){
        $dt = array(
			"container" => $this->__build_content( $idPelayanan ),
			"menu_key" => $this->menu_key,
			"akses_key" => "is_view",	
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
			"custom_js" => array(
				ASSETS_JS_URL."pelayanan/poli_gizi/index.js",
				ASSETS_URL."plugins/validate/jquery.validate_1.11.1.min.js",
			),
            "custom_css" => array(
			),
		);	
		$this->_render("default",$dt);	
    }

    private function __build_content($idPelayanan){
        $breadcrumbs = array(
                        "Home" =>$this->base_url_site,
                        "Poliklinik" => $this->base_url,
                        "Data Pelayanan Poli Gizi" => "#",
                    );
		$dt['breadcrumbs'] = $this->setBreadcrumbs($breadcrumbs);
		$dt['title'] = "Ruangan Gizi";
        $dt['frm_tanggal'] = $this->form_builder->inputText("Tanggal" , "txtTanggal" , date("Y-m-d") , "col-sm-2" , array("class"=> "form-control datepicker"));
        $ret = $this->load->view("content" , $dt , true);
        return $ret;

    }

    public function detail($txtIdKunjungan) {

        $dt = array(
			"container" => $this->__build_detail( $txtIdKunjungan ),
			"menu_key" => $this->menu_key,
			"akses_key" => "is_insert",	
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
			"custom_js" => array(
				ASSETS_URL."plugins/select2/select2.js",
				ASSETS_URL."plugins/validate/jquery.validate_1.11.1.min.js",
			),
            "custom_css" => array(
				ASSETS_URL."plugins/select2/select2.css",
			),
		);	
		$this->_render("default",$dt);	    
    }

	public function getDataRegisterGizi(){
		if(!$this->input->is_ajax_request()){
			echo "Ilegall";
			die;
		}

		$dtPelayanan = $this->input->post("dtStart");
		$bitIsPoli = $this->input->post("bitIsPoli");

		$dtStart = $dtPelayanan . " 00:00:00";
		$dtEnd = $dtPelayanan . " 23:59:00";

		$dataKunjungan = array();
		switch($bitIsPoli){	
			case "4" : $dataKunjungan = $this->register_model->getDataRujukan($dtPelayanan , $this->intIdPoli);break;
			default : $dataKunjungan = $this->register_model->getLatestDataRegister( 0 , 1000 , $dtStart , $dtEnd , $this->intIdPoli , $bitIsPoli);
		}

		$status_layanan_list = $this->config->item("status_layanan_list");
		$retVal['data'] = array();

		if(!empty($dataKunjungan)){

		foreach($dataKunjungan as $rowRegister){
			if($rowRegister['bitIsPoli']==1){ //antri
					$status_list = "danger";
				}else if($rowRegister['bitIsPoli']==2){ //dilayani
					$status_list = "warning";
				}else if($rowRegister['bitIsPoli']==3){ //selesai
					$status_list = "success";
				}else if($rowRegister['bitIsPoli']==4){ //pemeriksaan lanjut
					$status_list = "info";
				}else if($rowRegister['bitIsPoli']==5){ //rujukan
					$status_list = "default";
				}

			if($bitIsPoli!=4){
				
				$btnEdit= '<a class="btn btn-success btn-xs" href="'.$this->base_url.'detail/'.$rowRegister['txtIdKunjungan'].'"><span class="fa fa-commenting"> </span> Detail</a>';	
				$btnCall = '<button type="button" class="btn btn-info btn-xs" onclick="panggilAntrian(\''.$rowRegister['txtIdKunjungan'].'\')"><span class="fa fa-bullhorn	"> </span> Panggil Antrian</button>';
				$btnAksi = $btnEdit.$btnCall;
				$resData = array(
					$rowRegister['intNoAntri'],
					$rowRegister['txtNamaPasien'],
					$rowRegister['txtNoAnggota'],
					$rowRegister['txtUsiaPasienKunjungan'],	
					"<label class='label label-".$status_list."'>".$status_layanan_list[$rowRegister['bitIsPoli']]."</label>",
					$btnAksi
				);
	
			}else{
				$arrSelectMode = array("1"=> "Pemeriksaan Lanjut" , "2"=>"Rujukan Dalam", "3" => "Rujukan Luar");
				$btnEdit= '<a class="btn btn-success btn-xs" href="'.$this->base_url.'detail/'.$rowRegister['txtIdKunjungan'].'"><span class="fa fa-commenting"> </span> Detail</a>';	
				$btnCall = '<button type="button" class="btn btn-info btn-xs" onclick="panggilAntrian(\''.$rowRegister['txtIdKunjungan'].'\')"><span class="fa fa-bullhorn	"> </span> Panggil Antrian</button>';
				$btnAksi = $btnEdit.$btnCall;
				$resData = array(
					$rowRegister['intNoAntri'],
					$rowRegister['txtNamaPasien'],
					$rowRegister['txtNoAnggota'],
					$rowRegister['txtUsiaPasienKunjungan'],	
					$arrSelectMode[$rowRegister['bitPeriksaLanjut']],
					$rowRegister['txtNamaPelayanan'],	
					"<label class='label label-".$status_list."'>".$status_layanan_list[$rowRegister['bitIsPoli']]."</label>",
				);
			}

			$retVal['data'][] = $resData;
		}
				
		}
		echo json_encode($retVal);
	}

    public function __build_detail($txtIdKunjungan){

        $detailPasien = $this->register_model->getAllDetailPasienRegistered($id_kunjungan);
        echopre($detailPasien);
		if(count($detail_register) < 1){
            show_404();
        }
		$statusPelayananPoli = $detail_register['bitIsPoli'];

		if($statusPelayananPoli==1){
			/// Update To Sedang Di Layani
			$this->updateStatusRegisterPoli($id_kunjungan , 2);	
		}
		
		$idPasien = $detail_register['txtIdPasien'];
		$idPelayanan = $detail_register['intIdPelayanan'];
        $detail_pelayanan = $this->pelayanan_model->getDetail($idPelayanan);

		$menuKey = $detail_pelayanan['txtSlug'];
		if($menuKey=='ugd'){
			$menuKey = 'ugd_pelayanan';
		}
		$this->menu_key = $menuKey;
		$this->jenis_poli = $detail_pelayanan['intIdJenisPelayanan'];

		$detail_pasien = $this->pasien_model->getDetail($idPasien);

		//// Harus Di Hapus
		$jamkes_list = $this->jaminan_kesehatan->getListDataJaminan();
		$jamkes_list["0"] = "Umum / Tidak Ada";
		$jamkes_list[""] = "Umum / Tidak Ada";
		
		$rekam_medis = $this->rekam_medis->getDetailByIdPasien($idPasien);
		$idRekamMedis = "";
		$noRekamMedis = $detail_pasien['txtNoAnggota'];
		
		if(count($rekam_medis) > 0){
			$idRekamMedis = $rekam_medis['txtIdRekamMedis'];
			$noRekamMedis = $rekam_medis['txtNoRekamMedis'];
		}
		
		$nama_poli = $detail_pelayanan['txtNama'];
        $this->meta_title = $nama_poli;
        $this->meta_desc = "Fasilitas ".$nama_poli;
		
		//// Update Status Menjadi Sedang Di Layani
		$dt['title'] = "Pemeriksaan Rawat Jalan ".$nama_poli;
		$dt['form_title'] = "No Pengguna Layanan ".$detail_pasien['txtNoAnggota'];
		
		/// Build Header Form -> Hidden
		$dt['frm_id_pasien'] = $this->form_builder->inputHidden("txtIdPasien" , $detail_pasien['txtIdPasien']);
		$dt['frm_id_rekam_medis'] = $this->form_builder->inputHidden("txtIdRekamMedis" , $idRekamMedis);
		$dt['frm_id_pelayanan'] = $this->form_builder->inputHidden("txtIdPelayanan" , $detail_register['intIdPelayanan']);
		$dt['frm_id_kunjungan'] = $this->form_builder->inputHidden("txtIdKunjungan" , $detail_register['txtIdKunjungan']);
		
		///// Builder To View
		$dt['frm_tanggal'] = $this->form_builder->inputText("Tanggal Pemeriksaan" , "txtTanggalPemeriksaan", indonesian_date($detail_register['dtTanggalKunjungan']) , "col-sm-3" , array("readonly"=>"readonly"));
		$dt['frm_no_anggota_pasien'] = $this->form_builder->inputText("No Anggota" , "txtNoAnggotaPasien", $detail_pasien['txtNoAnggota'] , "col-sm-3" , array("readonly"=>"readonly"));
		$dt['frm_nama_pasien'] = $this->form_builder->inputText("Nama Pengguna Layanan" , "txtNamaPasien", $detail_pasien['txtNamaPasien'] , "col-sm-3" , array("readonly"=>"readonly"));
		$dt['frm_usia_pasien'] = $this->form_builder->inputText("Usia Pengguna Layanan" , "txtUsiaPasien", $detail_register['txtUsiaPasienKunjungan'] , "col-sm-3" , array("readonly"=>"readonly"));
		$dt['frm_jaminan_kesehatan'] = $this->form_builder->inputText("Jaminan Kesehatan" , "txtJamkes", $jamkes_list[$detail_register['intIdJaminanKesehatan']] , "col-sm-3" , array("readonly"=>"readonly"));
		$dt['frm_no_jaminan_kesehatan'] = $this->form_builder->inputText("No Jaminan Kesehatan" , "txtNoJamkes", $detail_register['txtNoJaminanKesehatan'] , "col-sm-3" , array("readonly"=>"readonly"));
		$dt['frm_no_rekam_medis'] = $this->form_builder->inputText("No Rekam Medis" , "txtNoRekamMedis", $noRekamMedis, "col-sm-3" , array("readonly"=>"readonly"));
		$dt['frm_txt_kunjungan'] = $this->form_builder->inputTextArea("Keterangan Kunjungan" , "txtKetKunjungan", $detail_register['txtKeteranganKunjungan'], "col-sm-3", array("readonly"=>"readonly"));
		
		$form_rekmed = $this->_build_form_poliklinik($detail_pasien,$detail_register);
		$breadcrumbs = array(
                        "Home" =>$this->base_url_site,
                        $nama_poli => $this->base_url,
                        "Data Pelayanan ".$nama_poli => "#",
        );
        $dt['breadcrumbs'] = $this->setBreadcrumbs($breadcrumbs);
		$dt['frm_rekmed_poli'] = $form_rekmed;
		$ret = $this->load->view("pelayanan/form" , $dt,true);
        return $ret;  
    }

    //// Get Form Poli Gizi

    public function getFormPoliGizi(){

    }

    public function getMonitoringGizi(){

    }

	


    



}