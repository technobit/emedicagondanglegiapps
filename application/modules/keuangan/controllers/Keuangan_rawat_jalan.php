<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Keuangan_rawat_jalan extends MY_Controller {

    var $meta_title = "Keuangan Rawat Jalan";
    var $meta_desc = "Keuangan Rawat Jalan";
    var $main_title = "Keuangan Rawat Jalan";
    var $menu_key = "keuangan_rawat_jalan";
    var $dtBreadcrumbs = array();  

    public function __construct() {
        parent::__construct();
        $this->dtBreadcrumbs = array(
            "Home" =>base_url(),
            "Keuangan" => base_url('keuangan'),
            "Rawat Jalan" => base_url('keuangan/rawat_jalan'),
        );
        $this->load->model('register_model');
        $this->load->model('item_keuangan_m');
        $this->load->model('keuangan_rawat_jalan_m');
    }
    public function index() {        
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "menu_key" => $this->menu_key,
			"akses_key" => "is_view",
            "container" => $this->_index(),
            "custom_js" => array(
                ASSETS_JS_URL."keuangan/rawat_jalan/index.js"
            ),
            "custom_css" => array(),
        );  
        $this->_render("default",$dt);             
    }

    private function _index() {        
        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['title'] = $this->meta_title;
        return $this->load->view('keuangan_rawat_jalan/index', $dt, true);
    }

    public function getNew($status = 1) {        
        $tanggal_awal = $this->input->get('tanggal_awal');
        $tanggal_akhir = $this->input->get('tanggal_akhir');
        $response['data'] = array();
        $dataKeuangan = $this->keuangan_rawat_jalan_m->getDataKeuangan($tanggal_awal , $tanggal_akhir , $status);
        foreach($dataKeuangan as $rowKeuangan) {    
            $id = $rowKeuangan['txtIdKunjungan'];
            $resTindakan = $this->keuangan_rawat_jalan_m->getTindakan($id)->result_array();
            $hslTindakan = "";
            foreach($resTindakan as $rowTindakan){
                $hslTindakan .= $rowTindakan['txtTindakan']." - ".$rowTindakan['txtDeskripsiTindakan']."\n";
            }
            $btnAksi = '<a href="'.base_url('keuangan/rawat_jalan/pelayanan/' . $rowKeuangan['txtIdKunjungan']).'" class="btn btn-info btn-flat btn-sm"><i class="fa fa-refresh"></i> Pelayanan</a>';   

            if($status==3){
                $btnAksi .='<a href="'.base_url('keuangan/rawat_jalan/total/' . $rowKeuangan['txtIdKunjungan']).'" class="btn btn-default btn-flat btn-sm"><i class="fa fa-money"></i> Tagihan</a> ';    
            }
            if(($status==1) || ($status==2)){
                $arrData = array(
                    $rowKeuangan['intNoAntri'],
                    $rowKeuangan['txtNamaPasien'],
                    $rowKeuangan['txtNamaPelayanan'],
                    $rowKeuangan['txtUsiaPasienKunjungan'],
                    $hslTindakan,
                    $btnAksi
                );
            }else if($status==3){
                $dataResultKeuangan = $this->keuangan_rawat_jalan_m->getKeuangan($id)->result_array();
                $hslKeuangan = "";
                foreach($dataResultKeuangan as $rowItemKeuangan) { 
                    $hslKeuangan .= "- ".$rowItemKeuangan['nama_item']."<br>" ;
                }
                $arrData = array(
                    $rowKeuangan['intNoAntri'],
                    $rowKeuangan['txtNamaPasien'],
                    $rowKeuangan['txtNamaPelayanan'],
                    $rowKeuangan['txtUsiaPasienKunjungan'],
                    $rowKeuangan['total_tagihan'],
                    $hslKeuangan,
                    $btnAksi
                );
            }
            
        $response['data'][] = $arrData;
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($response));
    }

    /// Old Function

    public function get($status = 1) {        
        $this->load->library('datatables');
        $tanggal_awal = $this->input->get('tanggal_awal');
        $tanggal_akhir = $this->input->get('tanggal_akhir');
        $this->db->select("register.txtIdPasien,register.txtIdKunjungan,register.txtUsiaPasienKunjungan ,register.intNoAntri, pasien.txtNamaPasien,pasien.txtNoAnggota,pasien.dtTanggalLahir,pelayanan.txtNama as txtNamaPelayanan,register.bitStatusKunjungan,register.bitIsPoli,register.bitIsApotik,register.bitIsPayment, keuangan_rawat_jalan.total_tagihan, pegawai.txtNamaPegawai");
        $this->db->from('register');
        $this->db->join("pasien" , "register.txtIdPasien =  pasien.txtIdPasien" , "left");
        $this->db->join("pelayanan" , "register.intIdPelayanan = pelayanan.intIdPelayanan" , "left");
        $this->db->join("jaminan_kesehatan" , "register.intIdJaminanKesehatan = jaminan_kesehatan.intIdJaminanKesehatan" , "left");        
        $this->db->join("keuangan_rawat_jalan", "keuangan_rawat_jalan.id_kunjungan = register.txtIdKunjungan" , "left");
        $this->db->join('users_profil', 'users_profil.intIdUser = keuangan_rawat_jalan.created_by' , "left");
        $this->db->join('pegawai', 'pegawai.intIdPegawai = users_profil.intIdPegawai' , "left");
        $this->db->where("register.bitIsPayment", $status);
        $this->db->where("left(register.dtTanggalKunjungan, 10)>='".$tanggal_awal."'");
        $this->db->where("left(register.dtTanggalKunjungan, 10)<='".$tanggal_akhir."'");
        $query = $this->db->get_compiled_select();
                                    
        $response = $this->datatables->collection($query)
        ->orderableColumns('intNoAntri, txtNamaPasien, txtNamaPelayanan, txtUsiaPasienKunjungan')
        ->searchableColumns('intNoAntri, txtNamaPasien, txtNamaPelayanan, txtUsiaPasienKunjungan')        
        ->editColumn('total_tagihan', function($row) {
            return toNumber($row->total_tagihan);
        })
        ->addColumn('action', function($row) {
            $html = '';            
            $html .= '<a href="'.base_url('keuangan/rawat_jalan/pelayanan/' . $row->txtIdKunjungan).'" class="btn btn-info btn-sm"><i class="fa fa-refresh"></i> Pelayanan</a> ';                           
            if ($row->bitIsPayment == 3) {
                $html .='<a href="'.base_url('keuangan/rawat_jalan/total/' . $row->txtIdKunjungan).'" class="btn btn-default btn-sm"><i class="fa fa-money"></i> Tagihan</a> ';                           
            }
            return $html;
        })
        ->render();
        $this->output->set_content_type('application/json')->set_output(json_encode($response));
    }

    public function get_panel() {
        if ($this->input->get('tanggal_awal')) {
            $tanggal_awal = $this->input->get('tanggal_awal');
        } else {
            $tanggal_awal = date('Y-m-d');        
        }
        if ($this->input->get('tanggal_akhir')) {
            $tanggal_akhir = $this->input->get('tanggal_akhir');
        } else {
            $tanggal_akhir = date('Y-m-d');        
        }
        $response = array(
            'antrian_hari_ini' => $this->keuangan_rawat_jalan_m->getAntrian($tanggal_awal, $tanggal_akhir)->row()->total_antrian,
            'uang_masuk_hari_ini' => $this->keuangan_rawat_jalan_m->getUangMasuk($tanggal_awal, $tanggal_akhir)->row()->total_tagihan
        );
        $this->output->set_content_type('application/json')->set_output(json_encode($response));
    }

    public function pelayanan($id) {
         
        if ($post = $this->input->post()) {
            
            $post['created_by'] = $this->session->userdata('sipt_user_id');
            $this->keuangan_rawat_jalan_m->createKeuangan($id, $post);
            $this->register_model->changeStatusPayment($id, 3);
            $this->redirect->with('successMessage', 'Pembayaran keuangan berhasil disimpan.')->to('keuangan/rawat_jalan/total/' . $id);
        }       
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "container" => $this->_pelayanan($id),
            "menu_key" => $this->menu_key,
			"akses_key" => "is_view",
            "custom_js" => array(
                ASSETS_JS_URL."keuangan/rawat_jalan/pelayanan.js"
            ),
            "custom_css" => array(),
        );  
        $detail_register = $this->register_model->getDetailRegister($id);
        $statusPelayananKeuangan = $detail_register['bitIsPayment'];
        
        if($statusPelayananKeuangan==1){
            $this->register_model->changeStatusPayment($id, 2);
        }
        $this->_render("default",$dt);             
    }

    private function _pelayanan($id) {        
        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['title'] = $this->meta_title;
        $result = $this->register_model->getDetailKeuangan($id);
        
        $resTindakan = $this->keuangan_rawat_jalan_m->getTindakan($id)->result_array();
        $hslTindakan = "";
        foreach($resTindakan as $rowTindakan){
            $hslTindakan .= $rowTindakan['txtTindakan']." - ".$rowTindakan['txtDeskripsiTindakan']."\n";
        }            
        $dt['detailPasien'] = $this->register_model->getAllDetailPasienRegistered($id);
        
        $dt['rsTindakan'] = $this->keuangan_rawat_jalan_m->getTindakan($id)->result();
        $dt['hslTindakan'] = $hslTindakan;
        $result['layanan'] = $this->keuangan_rawat_jalan_m->getKeuangan($id)->result_array();
        $this->form->setData($result);      
        return $this->load->view('keuangan_rawat_jalan/pelayanan', $dt, true);
    }

    public function delete($id) {
        $this->keuangan_rawat_jalan_m->deleteKeuangan($id);
        $this->redirect->with('successMessage', 'Pembayaran keuangan berhasil dihapus.')->back();
    }

     public function total($id) {
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "container" => $this->_total($id),
            "menu_key" => $this->menu_key,
            "akses_key" => "is_view",
            "custom_js" => array(),
            "custom_css" => array(),
        );  
        $this->_render("default",$dt); 
    }

    public function _total($id) {
        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['title'] = $this->meta_title;
        $dt['result'] = $this->register_model->getDetailKeuangan($id);     
        $dt['rs_layanan'] = $this->keuangan_rawat_jalan_m->getKeuangan($id)->result();
        return $this->load->view('keuangan_rawat_jalan/total', $dt, true);
    }

    public function invoice($id) {
        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['title'] = $this->meta_title;
        $dt['result'] = $this->register_model->getDetailKeuangan($id);     
        $dt['rs_layanan'] = $this->keuangan_rawat_jalan_m->getKeuangan($id)->result();
        $this->load->view('keuangan_rawat_jalan/invoice', $dt);
    }

    public function tambah_layanan() {
        $this->load->model('item_keuangan_m');
        $jamkes = $this->input->post('jamkes');
        $layanan = $this->input->post('layanan');        
        $result = $this->item_keuangan_m->findByKey($layanan)->result();
        $this->load->model('jaminan_kesehatan_model');
        $rsJamkses = $this->jaminan_kesehatan_model->find($jamkes);         
        if ($rsJamkses->bitBebasBiaya <> 0) {
            $result[0]->jasa_pelayanan = 0;
            $result[0]->jasa_sarana = 0;
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($result));        
    }

    public function cari_layanan() {
        $this->load->view('keuangan_rawat_jalan/cari_layanan');
    }

    public function get_cari_layanan() {
        $this->load->library('datatables');
        if ($key = $this->input->get('key')) {
            $this->db->like('nama_item', $key);            
        }
        $query = $this->db->get_compiled_select('item_keuangan');
        $response = $this->datatables->collection($query)
        ->orderableColumns('nama_item')      
        ->editColumn('jasa_sarana', function($row) {
            return toNumber($row->jasa_sarana);
        })
        ->editColumn('jasa_pelayanan', function($row) {
            return toNumber($row->jasa_pelayanan);
        })
        ->addColumn('tarif', function($row) {
            return toNumber(($row->jasa_sarana + $row->jasa_pelayanan));
        })
        ->render();
        $this->output->set_content_type('application/json')->set_output(json_encode($response));
    }

}