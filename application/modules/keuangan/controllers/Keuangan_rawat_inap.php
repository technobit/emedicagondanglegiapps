<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Keuangan_rawat_inap extends MY_Controller {

    var $meta_title = "Keuangan Rawat Inap";
    var $meta_desc = "Keuangan Rawat Inap";
    var $main_title = "Keuangan Rawat Inap";
    var $menu_key = "keuangan_item";
    var $dtBreadcrumbs = array();  

    public function __construct() {
        parent::__construct();
        $this->dtBreadcrumbs = array(
            "Home" =>base_url(),
            "Keuangan" => base_url('keuangan'),
            "Rawat Inap" => base_url('keuangan/rawat_inap'),
        );
        $this->load->model('register_model');
        $this->load->model('item_keuangan_m');
        $this->load->model('keuangan_rawat_inap_m');
    }
    public function index() {        
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "menu_key" => $this->menu_key,
			"akses_key" => "is_view",
            "container" => $this->_index(),
            "custom_js" => array(
                ASSETS_JS_URL."keuangan/rawat_inap/index.js"
            ),
            "custom_css" => array(),
        );  
        $this->_render("default",$dt);             
    }

    private function _index() {        
        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['title'] = $this->meta_title;
        return $this->load->view('keuangan_rawat_inap/index', $dt, true);
    }

    public function get($status = 2) {       
        $tanggal = $this->input->get('tanggal');
        if ($tanggal) {
            $this->db->where('left(dtSelesaiKunjungan, 10) = "'. $tanggal.'"');
        }
        $this->load->library('datatables');        
        $this->db->select("register.txtIdPasien,register.txtIdKunjungan,register.txtUsiaPasienKunjungan ,register.intNoAntri, pasien.txtNamaPasien,pasien.txtNoAnggota,pasien.dtTanggalLahir,pelayanan.txtNama as txtNamaPelayanan, jaminan_kesehatan.txtNamaJaminan, register.dtTanggalKunjungan, register.dtSelesaiKunjungan,register.bitStatusKunjungan,register.bitIsPoli,register.bitIsApotik,register.bitIsPayment, keuangan_rawat_jalan.total_tagihan, pegawai.txtNamaPegawai, kamar.txtKamar");
        $this->db->from('register');
        $this->db->join("pasien" , "register.txtIdPasien =  pasien.txtIdPasien" , "left");
        $this->db->join("rekam_medis_detail" , "register.txtIdKunjungan =  rekam_medis_detail.txtIdKunjungan" , "inner");
        $this->db->join("pelayanan" , "register.intIdPelayanan = pelayanan.intIdPelayanan" , "left");
        $this->db->join("jaminan_kesehatan" , "register.intIdJaminanKesehatan = jaminan_kesehatan.intIdJaminanKesehatan" , "left");        
        $this->db->join("keuangan_rawat_jalan", "keuangan_rawat_jalan.id_kunjungan = register.txtIdKunjungan" , "left");
        $this->db->join('users_profil', 'users_profil.intIdUser = keuangan_rawat_jalan.created_by' , "left");
        $this->db->join('pegawai', 'pegawai.intIdPegawai = users_profil.intIdPegawai' , "left");        
        $this->db->join('register_kamar', 'register_kamar.txtIdKunjungan = register.txtIdKunjungan');
        $this->db->join('kamar', 'kamar.intIdKamar = register_kamar.intIdKamar');
        $this->db->where("register.bitIsPoli", $status);        
        $this->db->where("register.bitStatusKunjungan", 2);
        $query = $this->db->get_compiled_select();
        $response = $this->datatables->collection($query)
        ->orderableColumns('txtNoAnggota, txtNamaPasien, txtNamaJaminan, txtKamar, dtTanggalKunjungan, dtSelesaiKunjungan, bitIsPoli')
        ->searchableColumns('txtNoAnggota, txtNamaPasien, txtNamaJaminan, txtKamar, dtTanggalKunjungan, dtSelesaiKunjungan, bitIsPoli')                
        ->editColumn('bitIsPoli', function($row) {
            if ($row->bitIsPoli == 2) {
                return 'Sedang Dirawat';
            } else {
                return 'Lunas';
            }
        })
        ->addColumn('action', function($row) {
                return '<a href="'.base_url('keuangan/rawat_inap/pelayanan/' . $row->txtIdKunjungan).'" class="btn btn-info btn-sm"><i class="fa fa-refresh"></i> Detail</a> ';                           
        })
        ->render();
        $this->output->set_content_type('application/json')->set_output(json_encode($response));
    }

    public function get_panel() {
        if ($this->input->get('tanggal')) {
            $tanggal = $this->input->get('tanggal');
        } else {
            $tanggal = date('Y-m-d');        
        }
        $response = array(
            'antrian_hari_ini' => $this->keuangan_rawat_inap->getAntrian($tanggal)->row()->total_antrian,
            'uang_masuk_hari_ini' => $this->keuangan_rawat_inap->getUangMasuk($tanggal)->row()->total_tagihan
        );
        $this->output->set_content_type('application/json')->set_output(json_encode($response));
    }

    public function riwayat($id) {
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "container" => $this->_riwayat($id),
            "menu_key" => $this->menu_key,
            "akses_key" => "is_view",
            "custom_js" => array(),
            "custom_css" => array(),
        );          
        $this->_render("default",$dt);   
    }

    public function _riwayat($id) {
        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['title'] = $this->meta_title;
        $result = $this->register_model->getDetailKeuanganRawatInap($id);     
        $dt['rsRiwayat'] = $this->keuangan_rawat_inap_m->get_riwayat($id)->result();
        $dt['rsTindakan'] = $this->keuangan_rawat_inap_m->getTindakan($id)->result();
        $dt['idKunjungan'] = $id;                
        $this->form->setData($result);
        return $this->load->view('keuangan_rawat_inap/riwayat', $dt, true);
    }

    public function pelayanan($id) { 
        if ($post = $this->input->post()) {
            $post['created_by'] = $this->session->userdata('sipt_user_id');
            $this->keuangan_rawat_inap_m->createKeuangan($id, $post);            
            $this->redirect->with('successMessage', 'Pembayaran keuangan berhasil disimpan.')->to('keuangan/rawat_inap?invoice='.$id);
        }       
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "container" => $this->_pelayanan($id),
            "menu_key" => $this->menu_key,
			"akses_key" => "is_view",
            "custom_js" => array(
                ASSETS_JS_URL."keuangan/rawat_inap/pelayanan.js"
            ),
            "custom_css" => array(),
        );          
        $this->_render("default",$dt);             
    }

    private function _pelayanan($id) {        
        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['title'] = $this->meta_title;
        $result = $this->register_model->getDetailKeuanganRawatInap($id);     
        $dt['rsTindakan'] = $this->keuangan_rawat_inap_m->getTindakan($id)->result();                
        $dt['detailPasien'] = $this->register_model->getAllDetailPasienRegistered($id);
        $this->form->setData($result);      
        return $this->load->view('keuangan_rawat_inap/pelayanan', $dt, true);
    }

    public function edit($id) { 
        if ($post = $this->input->post()) {
            $post['created_by'] = $this->session->userdata('sipt_user_id');
            $this->keuangan_rawat_inap_m->editKeuangan($id, $post);            
            $this->redirect->with('successMessage', 'Pembayaran keuangan berhasil disimpan.')->to('keuangan/rawat_inap?invoice='.$id);
        }       
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "container" => $this->_edit($id),
            "menu_key" => $this->menu_key,
            "akses_key" => "is_view",
            "custom_js" => array(
                ASSETS_JS_URL."keuangan/rawat_inap/pelayanan.js"
            ),
            "custom_css" => array(),
        );          
        $this->_render("default",$dt);             
    }

    private function _edit($id) {        
        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['title'] = $this->meta_title;
        $find = $this->keuangan_rawat_inap_m->find($id);
        $result = $this->register_model->findDetailKeuanganRawatInap($find->id_kunjungan, $id);     
        $dt['rsTindakan'] = $this->keuangan_rawat_inap_m->getTindakan($find->id_kunjungan)->result();                
        $result['layanan'] = $this->keuangan_rawat_inap_m->getKeuangan($find->id_kunjungan, $id)->result_array();
        $this->form->setData($result);      
        return $this->load->view('keuangan_rawat_inap/pelayanan', $dt, true);
    }

    public function invoice($id) {
        $dt['result'] = $this->register_model->getDetailKeuanganRawatInap($id);     
        $dt['rsRiwayat'] = $this->keuangan_rawat_inap_m->get_riwayat($id)->result();
        foreach ($dt['rsRiwayat'] as $key => $riwayat) {
            $dt['rsRiwayat'][$key]->layanan = $this->keuangan_rawat_inap_m->getKeuangan($id, $riwayat->id)->result();
        }
        $this->load->view('keuangan_rawat_inap/invoice', $dt);
    }

    public function invoice_partial($idKunjungan , $idItem){
        $dt['result'] = $this->register_model->getDetailKeuanganRawatInap($idKunjungan);
        $dt['dataKeuangan'] = $this->keuangan_rawat_inap_m->get_detail_keuangan($idItem);
        $this->load->view('keuangan_rawat_inap/total_partial', $dt);
    }

    public function total($id) {
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "container" => $this->_total($id),
            "menu_key" => $this->menu_key,
            "akses_key" => "is_view",
            "custom_js" => array(
                
            ),
            "custom_css" => array(),
        );  
        $this->_render("default",$dt); 
    }

    public function _total($id) {
        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['title'] = $this->meta_title;
        $dt['result'] = $this->register_model->getDetailKeuanganRawatInap($id);     
        $dt['rsRiwayat'] = $this->keuangan_rawat_inap_m->get_riwayat($id)->result();
        foreach ($dt['rsRiwayat'] as $key => $riwayat) {
            $dt['rsRiwayat'][$key]->layanan = $this->keuangan_rawat_inap_m->getKeuangan($id, $riwayat->id)->result();
        }
        return $this->load->view('keuangan_rawat_inap/total', $dt, true);
    }

    public function total_partial($idKunjungan , $idItem) {
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "container" => $this->_total_partial($idKunjungan , $idItem),
            "menu_key" => $this->menu_key,
            "akses_key" => "is_view",
            "custom_js" => array(
                ASSETS_JS_URL."keuangan/rawat_inap/cetak_struk.js"
            ),
            "custom_css" => array(),
        );  
        $this->_render("default",$dt); 
    }

    public function _total_partial($idKunjungan ,$idItem) {
        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['title'] = $this->meta_title;
        $dt['result'] = $this->register_model->getDetailKeuanganRawatInap($idKunjungan);
        $dt['rsRiwayat'] = $this->keuangan_rawat_inap_m->get_riwayat($idKunjungan)->result();     
        $dt['dataKeuangan'] = $this->keuangan_rawat_inap_m->get_detail_keuangan($idItem)->result();
        $dt['idItem'] = $idItem;
        return $this->load->view('keuangan_rawat_inap/total_partial', $dt, true);
    }

    public function delete($id) {
        $this->keuangan_rawat_inap_m->deleteKeuangan($id);
        $this->redirect->with('successMessage', 'Pembayaran keuangan berhasil dihapus.')->back();
    }

    public function tambah_layanan() {
        $this->load->model('item_keuangan_m');
        $jamkes = $this->input->post('jamkes');
        $layanan = $this->input->post('layanan');        
        $result = $this->item_keuangan_m->findByKey($layanan)->result();
        $result['tgl_tindakan'] = date('Y-m-d');
        $this->output->set_content_type('application/json')->set_output(json_encode($result));        
    }

    public function cari_layanan() {
        $this->load->view('keuangan_rawat_inap/cari_layanan');
    }

    public function get_cari_layanan() {
        $this->load->library('datatables');
        if ($key = $this->input->get('key')) {
            $this->db->like('nama_item', $key);            
        }
        $query = $this->db->get_compiled_select('item_keuangan');
        $response = $this->datatables->collection($query)
        ->orderableColumns('nama_item')      
        ->editColumn('jasa_sarana', function($row) {
            return toNumber($row->jasa_sarana);
        })
        ->editColumn('jasa_pelayanan', function($row) {
            return toNumber($row->jasa_pelayanan);
        })
        ->addColumn('tarif', function($row) {
            return toNumber(($row->jasa_sarana + $row->jasa_pelayanan));
        })
        ->render();
        $this->output->set_content_type('application/json')->set_output(json_encode($response));
    }

}