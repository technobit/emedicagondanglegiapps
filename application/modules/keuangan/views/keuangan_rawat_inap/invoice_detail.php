<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>Invoice</title>
  <link href="<?=ASSETS_URL?>/img/icon.png" rel="shortcut icon" />
  <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
  <!-- Bootstrap 3.3.2 -->
  <link href="<?=ASSETS_URL?>bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css" />
  <!-- Theme style -->
  <link href="<?=ASSETS_URL?>dist/css/AdminLTE.css" rel="stylesheet" type="text/css" />
  <link href="<?=ASSETS_URL?>plugins/font-awesome-4.6.3/css/font-awesome.min.css" rel="stylesheet" >
  <link href="<?=ASSETS_URL?>dist/css/skins/_all-skins.css" rel="stylesheet" type="text/css" />
  <link href="<?=ASSETS_URL?>plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
  <!-- new post -->
  <!-- datepicker -->
  <link href="<?=ASSETS_URL?>plugins/datepicker/datepicker3.css" rel="stylesheet" type="text/css" />
  <link href="<?=ASSETS_URL?>custom/css/global.css" rel="stylesheet" type="text/css" />
  <!-- jQuery 2.1.3 -->
  <script src="<?=ASSETS_URL?>plugins/jQuery/jQuery-2.1.4.min.js"></script>
  <!-- Bootstrap datetime Picker -->
  <!-- audio player-->
  <script src='<?=ASSETS_URL?>plugins/playsound/jquery.playSound.js'></script>
  <script>
    var global_url = '<?=base_url();?>';
  </script>

</head>
<body onload="window.print()" class="skin-green-light sidebar-mini">
<section class="invoice">
<!-- title row -->
<div class="row">
  <div class="col-xs-12">
    <h2 class="page-header">
      <i class="fa fa-globe"></i> Bukti Pembayaran
      <small class="pull-right"><a href="<?= base_url('keuangan/rawat_jalan/invoice/' . $result['txtIdKunjungan']) ?>" target="_blank"><i class="fa fa-print"></i> Print</a></small>
    </h2>
  </div>
  <!-- /.col -->
</div>
<!-- info row -->
<div class="row invoice-info">
  <div class="col-sm-6 invoice-col">
    Data Pengguna Layanan
    <address> 
      <strong><?= $result['txtNamaPasien'] ?></strong><br>
      No Anggota : <?= $result['txtNoAnggota'] ?><br>
      No KTP : <?= $result['txtNoKtp'] ?><br>
      Jenis Kelamin : <?= ($result['charJkPasien'] == 'L') ? 'Laki-laki' : 'Perempuan' ?><br>
      Jaminan Kesehatan : <?= $result['txtNamaJaminan'] ?><br>
    </address>
  </div>
  <!-- /.col -->
  <!-- /.col -->
  <div class="col-sm-6 invoice-col text-right">
    <b>Invoice #<?= $result['txtIdKunjungan'] ?></b><br>
    <br>        
    <b>Tanggal Cetak:</b> <?= indonesian_date(date('Y-m-d')) ?><br>       
    <b>Bebas Biaya ?:</b>
    <?php if ($result['bitBebasBiaya'] == 1) { ?>
      <label class="label label-success">Ya</label>
    <?php } else { ?>
      <label class="label label-danger">Tidak</label>
    <?php } ?>
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->

<!-- Table row -->
<div class="row">
  <div class="col-xs-12 table-responsive">
    <table class="table">
      <thead>
        <tr>
          <th>Item Keuangan</th>
          <th width="100px" class="text-right">Jasa Sarana</th>
          <th width="100px" class="text-right">Jasa Pelayanan</th>
          <th width="100px" class="text-right">Total Tagihan</th>                                            
        </tr>
      </thead>
      <tbody>
       <?php $total_tagihan = 0 ?>       
          <?php foreach($dataKeuangan as $layanan) { ?>
            <tr>                                          
              <td><?= $layanan->nama_item ?></td>
              <td width="100px" class="text-right"><?= toCurrency($layanan->jasa_sarana) ?></td>
              <td width="100px" class="text-right"><?= toCurrency($layanan->jasa_pelayanan) ?></td>
              <td width="100px" class="text-right"><?= toCurrency($layanan->jasa_sarana + $layanan->jasa_pelayanan) ?></td>              
            </tr>
          <?php } ?>
        <?php $total_tagihan+=($layanan->jasa_sarana + $layanan->jasa_pelayanan) ?>      
      </tbody>
    </table>
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->

<div class="row">
  <!-- accepted payments column -->
  <div class="col-xs-3 text-center">
    <b>Pengguna Layanan/Wali</b>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    (..................................)
  </div>
  <div class="col-xs-3 text-center">
    <b>Petugas</b>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    (..................................)
  </div>
  <!-- /.col -->
  <div class="col-xs-6">
    <p class="lead">Grand Total</p>

    <div class="table-responsive">
      <table class="table">
        <tr>
          <th style="width:50%">Subtotal:</th>
          <td class="text-right"><?= toCurrency($total_tagihan) ?></td>
        </tr>
        <tr>
          <th style="width:50%">Jumlah Pembayaran :</th>
          <td class="text-right"><?= toCurrency($result['total_bayar']) ?></td>
        </tr>
        <tr>
          <th style="width:50%">Kembalian :</th>
          <td class="text-right"><?= toCurrency($result['total_bayar'] - $total_tagihan) ?></td>
        </tr>
        <?php if ($result['bitBebasBiaya'] == 1) { ?>
          <tr>
            <th style="width:50%">Bebas Biaya:</th>
            <td class="text-right"><?= toCurrency($total_tagihan) ?></td>
          </tr>
        <?php } ?>
      </table>
    </div>
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->    
</section>
</body>

<!-- Bootstrap 3.3.2 JS -->
<script src="<?=ASSETS_URL?>bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<!-- Slimscroll -->
<script src="<?=ASSETS_URL?>plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<!-- FastClick -->
<script src='<?=ASSETS_URL?>plugins/fastclick/fastclick.min.js'></script>
<!-- DataTables -->
<script src='<?=ASSETS_URL?>plugins/datatables/jquery.dataTables.min.js'></script>
<script src='<?=ASSETS_URL?>plugins/datatables/dataTables.bootstrap.min.js'></script>
<!-- Date Picker-->
<script src='<?=ASSETS_URL?>plugins/datepicker/bootstrap-datepicker.js'></script>
<script src='<?=ASSETS_URL?>plugins/moment/moment.min.js'></script>
<!-- bootbox-->
<script src='<?=ASSETS_URL?>plugins/bootbox/bootbox.js'></script>

<!-- AdminLTE App -->
<script src="<?=ASSETS_URL?>dist/js/app.min.js" type="text/javascript"></script>
<!-- Globak App -->
<script src="<?=ASSETS_JS_URL?>global.js" type="text/javascript"></script>  
</html>