<section class="content-header">
    <h1 class="title"><?=$title?></h1>
    <?=$breadcrumbs?>
</section>
<section class="content">
<div class="row"> 
  <div class="col-md-12 text-right">    
    <a href="<?= base_url('keuangan/rawat_jalan') ?>" class="btn btn-default"><i class="fa fa-list"></i> List Antrian</a>
  </div>
</div>
<section class="invoice">
<!-- title row -->
<div class="row">
  <div class="col-xs-12">
    <h2 class="page-header">
      <i class="fa fa-globe"></i> Bukti Pembayaran
      <small class="pull-right">
      <a href="<?= base_url('keuangan/rawat_jalan/invoice/' . $result['txtIdKunjungan']) ?>" target="_blank"><i class="fa fa-print"></i> Print</a>
      <a href="<?= base_url('cetak/cetak-struct-rawat-jalan/' . $result['txtIdKunjungan']) ?>" target="_blank"><i class="fa fa-print"></i> Print Struk</a>
      </small>
    </h2>
  </div>
  <!-- /.col -->
</div>
<!-- info row -->
<div class="row invoice-info">
  <div class="col-sm-6 invoice-col">
    Data Pengguna Layanan
    <address> 
      <strong><?= $result['txtNamaPasien'] ?></strong><br>
      No Anggota : <?= $result['txtNoAnggota'] ?><br>
      No KTP : <?= $result['txtNoKtp'] ?><br>
      Jenis Kelamin : <?= ($result['charJkPasien'] == 'L') ? 'Laki-laki' : 'Perempuan' ?><br>
      Jaminan Kesehatan : <?= $result['txtNamaJaminan'] ?><br>
    </address>
  </div>
  <!-- /.col -->
  <!-- /.col -->
  <div class="col-sm-6 invoice-col text-right">
    <b>Invoice #<?= $result['txtIdKunjungan'] ?></b><br>
    <br>        
    <b>Tanggal Cetak:</b> <?= indonesian_date(date('Y-m-d')) ?><br>       
    <b>Bebas Biaya ?:</b>
    <?php if ($result['bitBebasBiaya'] == 1) { ?>
      <label class="label label-success">Ya</label>
    <?php } else { ?>
      <label class="label label-danger">Tidak</label>
    <?php } ?>
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->

<!-- Table row -->
<div class="row">
  <div class="col-xs-12 table-responsive">
    <table class="table">
      <thead>
        <tr>
          <th>Item Keuangan</th>
          <th width="100px" class="text-right">Jasa Sarana</th>
          <th width="100px" class="text-right">Jasa Pelayanan</th>
          <th>Jumlah</th>
          <th width="100px" class="text-right">Total Tagihan</th>                                             
        </tr>
      </thead>
      <tbody>
       <?php $total_tagihan = 0 ?>       
          <?php foreach($rs_layanan as $layanan) { 
            $total_tagihan1 = ($layanan->jasa_sarana + $layanan->jasa_pelayanan) * $layanan->jumlah; 
            ?>
            <tr>                                          
              <td><?= $layanan->nama_item ?></td>
              <td width="100px" class="text-right"><?= toCurrency($layanan->jasa_sarana) ?></td>
              <td width="100px" class="text-right"><?= toCurrency($layanan->jasa_pelayanan) ?></td>
              <td><?= $layanan->jumlah ?></td>
              <td width="100px" class="text-right"><?= toCurrency($total_tagihan1) ?></td>
            </tr>
          <?php 
          $total_tagihan+=($total_tagihan1);
          } ?>
        
      </tbody>
    </table>
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->

<div class="row">
  <!-- accepted payments column -->
  <div class="col-xs-3 text-center">
    <b>Pengguna Layanan/Wali</b>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    (..................................)
  </div>
  <div class="col-xs-3 text-center">
    <b>Petugas</b>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    (..................................)
  </div>
  <!-- /.col -->
  <div class="col-xs-6">
    <p class="lead">Grand Total</p>

    <div class="table-responsive">
      <table class="table">
        <tr>
          <th style="width:50%">Subtotal :</th>
          <td class="text-right"><?= toCurrency($total_tagihan) ?></td>
        </tr>
        <tr>
          <th style="width:50%">Jumlah:</th>
          <td class="text-right"><?= toCurrency($result['total_bayar']) ?></td>
        </tr>
        <?php if ($result['bitBebasBiaya'] == 1) { ?>
          <tr>
            <th style="width:50%">Bebas Biaya :</th>
            <td class="text-right"><?= toCurrency($total_tagihan) ?></td>
          </tr>
        <?php } ?>
        <tr>
          <th style="width:50%">Kembalian :</th>
          <td class="text-right">
            <?php if ($result['bitBebasBiaya'] == 1) { ?>
              0             
            <?php } else { ?>
              <?= toCurrency($result['total_bayar'] - $total_tagihan) ?>
            <?php } ?>
          </td>
        </tr>
      </table>
    </div>
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->    
</section>
</section>