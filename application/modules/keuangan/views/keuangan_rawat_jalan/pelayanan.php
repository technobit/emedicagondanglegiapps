<section class="content-header">
    <h1 class="title"><?=$title?></h1>
    <?=$breadcrumbs?>
</section>
<section class="content">
  <?= $this->form->open(null, 'id="form-pelayanan" class="form-horizontal" data-apotik="'.$this->uri->segment(2).'"') ?>
    <div class="row">
      <div class="col-md-8">        
        <div class="box box-primary">
          <div class="box-header">
            <h3 class="box-title">Tindakan</h3>
          </div>
          <div class="box-body">
            <div class="form-group">            
              <div class="col-md-12">
                <?= $this->form->textarea('txtTindakan', $hslTindakan, 'class="form-control" style="height:150px;" readonly') ?>
              </div>
            </div> 
          </div>
        </div>
        <div class="box box-primary">
          <div class="box-header">
            <h3 class="box-title">Item Keuangan</h3>
          </div>
          <div class="box-body">                          
                           
            <div class="form-group">
              <div class="col-md-8">
                <div class="input-group">
                  <?= $this->form->text('txtLayanan', null, 'placeholder="Nama Layanan" id="txtLayanan" class="form-control input-lg"') ?>   
                  <div class="input-group-btn">           
                    <button type="button" id="btnTambahLayanan" class="btn btn-success btn-lg btn-flat"><i class="fa fa-plus"></i></button>
                    <button type="button" id="btnCariLayanan" class="btn btn-primary btn-lg btn-flat"><i class="fa fa-search"></i> Cari Layanan</button>
                  </div>
                </div>
              </div>
            </div>
            <table id="tblLayanan" class="table table-bordered table-condensed table-hover">
              <thead>
                <tr>
                  <th>Nama Layanan</th>
                  <th width="100px" class="text-right">Jasa Saranan</th>
                  <th width="110px" class="text-right">Jasa Pelayanan</th>
                  <th width="90px" class="text-right">Tarif</th>
                  <th width="50px" class="text-center">Jml</th>
                  <th width="80px" class="text-right">Total</th>
                  <th width="1px" class="text-center">Aksi</th>
                </tr>
              </thead>
              <tbody>     
                <?php if (formValue('layanan')) { ?>    
                  <?php $rowsLayanan = 1; ?>          
                  <?php foreach(formValue('layanan') as $layanan) { ?>
                    <?php
                      $tarif = $layanan['jasa_sarana'] + $layanan['jasa_pelayanan'];
                      $total = $tarif * $layanan['jumlah'];
                    ?>
                    <tr data-row="<?= $rowsLayanan ?>">
                      <td><input type="hidden" name="layanan[<?= $rowsLayanan ?>][id]" value="<?= $layanan['id'] ?>"><input type="hidden" name="layanan[<?= $rowsLayanan ?>][nama_item]" value="<?= $layanan['nama_item'] ?>"><?= $layanan['nama_item'] ?></td>  
                      <td><input type="text" name="layanan[<?= $rowsLayanan ?>][jasa_sarana]" value="<?= $layanan['jasa_sarana'] ?>" class="form-control text-right input-sm" onkeyup="countTarifTotal(<?= $rowsLayanan ?>)"/></td>  
                      <td><input type="text" name="layanan[<?= $rowsLayanan ?>][jasa_pelayanan]" value="<?= $layanan['jasa_pelayanan'] ?>" class="form-control text-right input-sm" onkeyup="countTarifTotal(<?= $rowsLayanan ?>)"/></td>
                      <td data-name="tarif" class="text-right"><?= toNumber($tarif) ?></td>
                      <td><input type="text" name="layanan[<?= $rowsLayanan ?>][jml]" value="<?= $layanan['jumlah'] ?>" class="form-control text-center input-sm" onkeyup="countTarifTotal(<?= $rowsLayanan ?>)"/></td>
                      <td data-name="total" class="text-right"><?= toNumber($total) ?></td>
                      <td><button type="button" onclick="hapusLayanan(<?= $rowsLayanan ?>)" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button></td>
                    </tr>
                    <?php $rowsLayanan++ ?>
                  <?php } ?>
                <?php } ?>         
              </tbody>
            </table>
            <br>
            <div class="form-group text-center">
                <button type="button" id="btnBayar" class="btn btn-primary btn-lg"><i class="fa fa-print"></i> Bayar</button>   
                <a href="<?= base_url('keuangan/rawat_inap') ?>" class="btn btn-danger btn-lg"><i class="fa fa-ban"></i> Batal</a>             
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="box box-primary">
          <div class="box-body">
            <div class="text-right">
              <input type="hidden" name="total_tagihan" id="grandTotal" value="0">
              <span style="font-size: 60px" id="grandTotalLabel">0</span>
            </div>
          </div>
        </div>
        <div class="box box-primary">
          <div class="box-header">
              <h4 class="box-title">Data Pengguna Layanan</h4>
          </div>
          <div class="box-body">  
            <div class="form-group">
              <label class="col-md-4 control-label">Tanggal</label>
              <div class="col-md-8">                
                <?= $this->form->text('tgl_bayar', date('Y-m-d'), 'id="tgl_bayar" class="form-control"') ?>
              </div>
            </div>       
            <div class="form-group">
              <label class="col-md-4 control-label">No. Anggota</label>
              <div class="col-md-8">
                <input type="hidden" name="kunjungan" id="kunjungan" value="<?= $this->uri->segment(4) ?>"/>
                <?= $this->form->text('txtNoAnggota', $detailPasien['txtNoAnggota'], 'class="form-control" readonly') ?>
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-4 control-label">Nama Pengguna Layanan</label>
              <div class="col-md-8">
                <?= $this->form->text('txtNamaPasien', $detailPasien['txtNamaPasien'], 'class="form-control" readonly') ?>
              </div>
            </div> 
            <div class="form-group">
              <label class="col-md-4 control-label">No. KTP/NIK</label>
              <div class="col-md-8">
                <?= $this->form->text('txtNoKtp', $detailPasien['txtNoKtp'], 'class="form-control" readonly') ?>
              </div>
            </div> 
            <div class="form-group">
              <label class="col-md-4 control-label">Jenis Kelamin</label>
              <div class="col-md-8">
                <?= $this->form->select('charJkPasien', selectJenisKelamin(), $detailPasien['charJkPasien'], 'class="form-control" disabled') ?>
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-4 control-label">Jaminan Kesehatan</label>
              <div class="col-md-8">                
                <?= $this->form->select('intIdJaminanKesehatan', selectJamkes(), $detailPasien['txtNamaJaminan'], 'class="form-control" disabled') ?>
              </div>
            </div>           
          </div>                          
        </div>
        <div class="box box-primary">
          <div class="box-header">
            <h3 class="box-title">Data layanan</h3>
          </div>
          <div class="box-body">
            <div class="form-group">
              <label class="col-md-4 control-label">Dari</label>
              <div class="col-md-8">
                <?= $this->form->text('txtNama', $detailPasien['txtNama'], 'class="form-control" readonly') ?>
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-4 control-label">Dokter</label>
              <div class="col-md-8">
                <?= $this->form->text('txtNamaPegawai', null, 'class="form-control" readonly') ?>
              </div>
            </div>           
          </div>
        </div>
      </div>    
    </div>    
    <div class="modal fade" id="frmBayar" tabindex="-1" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Bayar</h4>
          </div>
          <div class="modal-body">              
            <div class="form-horizontal">
              <div class="form-group">                                
                <div class="col-md-12">
                  <input type="text" name="total_tagihan2" id="tagihan" class="form-control text-right" style="font-size:100px;height: 120px;" readonly>   
                </div>
              </div>
              <div class="form-group">                                
                <div class="col-md-12">
                  <input type="text" name="total_bayar" id="bayar" class="form-control text-right" style="font-size:100px;height: 120px;">   
                </div>
              </div>
              <div class="form-group">
                <div class="col-md-4">
                  <?= $this->form->checkbox('bitBebasBiaya', '1') ?>
                  <span>Bebas Biaya?</span>
                </div>
                <label class="col-md-2 control-label">Kembalian</label>
                <div class="col-md-6">
                  <input type="text" id="kembalian" class="form-control text-right" readonly>
                </div>                
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-success" onclick="return simpanPembayaran()">Simpan</button>
          </div>
        </div>
      </div>
    </div>
  <?= $this->form->close() ?>
</section>