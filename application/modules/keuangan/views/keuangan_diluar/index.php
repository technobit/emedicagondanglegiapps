<section class="content-header">
    <h1 class="title"><?=$title?></h1>
    <?=$breadcrumbs?>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3 id="uang_masuk_hari_ini"></h3>
                    <p>Uang Masuk Hari ini</p>
                </div>
                <div class="icon">
                    <i class="fa fa-money"></i>
                </div>
            </div>
        </div>        
    </div>
    <div class="row">        
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-body">
                   <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-md-2 control-label">Tanggal</label>
                            <div class="col-md-10">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input type="text" value="<?= date('Y-m-d') ?>" id="filterTanggal" class="form-control" />
                                </div>
                            </div>                           
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-2 col-md-10">
                                <button id="btnTampilkan" class="btn btn-primary">Tampilkan</button>
                            </div>
                        </div>
                   </div>
                </div>
            </div>
            <div class="box box-primary">
                <div class="box-body">
                    <div class="toolbar">
                        <a href="<?= base_url('keuangan/diluar/create') ?>" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Pembayaran</a>
                    </div><br>
                    <table id="tableKeuangan" class="table table-bordered table-condensed table-hover"> 
                        <thead>
                            <tr>
                                <th>No.Bukti</th>
                                <th>Tanggal</th>
                                <th>Petugas</th>
                                <th class="text-right">Total</th>                                                                                               
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>            
        </div>
    </div>
</section>
