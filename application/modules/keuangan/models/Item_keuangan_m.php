<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Item_keuangan_m extends Base_m {

    protected $table = 'item_keuangan';
    protected $fillable = array('nama_item', 'jasa_sarana', 'jasa_pelayanan', 'aktif', 'keterangan', 'created_by');
    protected $timestamp = true;

    public function findByKey($key) {
    	return $this->db->like('id', $key)
    	->or_like('nama_item', $key)
    	->get('item_keuangan');
    }

}