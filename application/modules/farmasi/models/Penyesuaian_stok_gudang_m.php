<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Penyesuaian_stok_gudang_m extends Base_m {
    protected $table = 'penyesuaian_stok_gudang';
    protected $fillable = array('id_apotik', 'tgl_penyesuaian:date', 'no_penyesuaian');
    protected $timestamp = true;

    public function getObat($id) {
        return $this->db->select('penyesuaian_stok_gudang.*, penyesuaian_stok_gudang_detail.*, obat.id as id_obat, obat.kode_obat, obat.nama_obat, kemasan.id as id_kemasan, kemasan.kemasan')        
        ->join('penyesuaian_stok_gudang_detail', 'penyesuaian_stok_gudang.id = penyesuaian_stok_gudang_detail.id_penyesuaian_stok_gudang')
        ->join('obat', 'obat.id = penyesuaian_stok_gudang_detail.id_obat')
        ->join('kemasan', 'kemasan.id = obat.id_kemasan', 'left')
        ->where('penyesuaian_stok_gudang.id', $id)
        ->get('penyesuaian_stok_gudang');
    }

    public function createPO($record) {
        $record['no_penyesuaian'] = $this->generateID();
    	$this->insert($record);
    	$id_penyesuaian_stok_gudang = $this->db->insert_id();
        $this->load->model('obat_m');
    	foreach ($record['obat'] as $row) {
    		$this->db->insert('penyesuaian_stok_gudang_detail', array(
    			'id_penyesuaian_stok_gudang' => $id_penyesuaian_stok_gudang,
    			'id_obat' => $row['id_obat'],
                'jml' => $row['jml'],
                'jml_akhir' => $row['jml_akhir'],
                'perubahan' => $row['jml_akhir'] - $row['jml']
    		));
    		$this->db->where('id', $row['id_obat'])->set('stok', $row['jml_akhir'], false)->update('obat');
            $this->obat_m->update_stok_periode($row['id_obat']);
    	}
    	return true;
    }

    public function deletePO($id) {
        $rsObat = $this->db->where('id_penyesuaian_stok_gudang', $id)->get('penyesuaian_stok_gudang_detail')->result();
        foreach ($rsObat as $obat) {
            $this->db->set('stok', 'stok + '.$obat->jml, false)->where('id', $obat->id_obat)->update('obat');
            $this->db->where('id', $obat->id)->delete('penyesuaian_stok_gudang_detail');
        }
        $this->db->where('id', $id)->delete('penyesuaian_stok_gudang');
    }

    public function generateID() {
        $prefix = 'PO/'. date('m') . '/' . date('Y') . '/';
        $digit = '000';
        $lastID =  $this->db->select_max('no_penyesuaian')
        ->where('left(no_penyesuaian,'.(strlen($prefix)).')', $prefix)
        ->get($this->table)->row()->no_penyesuaian;
        if ($lastID) {            
            $counter = substr($lastID, -strlen($digit)) + 1;
            return $prefix.substr($digit.$counter, -strlen($digit));
        } else {
            return $prefix.substr($digit.'1', -strlen($digit));
        }
    }

}