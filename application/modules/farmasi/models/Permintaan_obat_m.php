<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Permintaan_obat_m extends Base_m {
    protected $table = 'permintaan_obat';
    protected $fillable = array('id_apotik', 'tgl_permintaan:date', 'no_permintaan', 'diterima_oleh', 'tgl_disetujui:date', 'disetujui_oleh', 'status');
    protected $timestamp = true;

    public function getObat($id) {
        return $this->db->select('permintaan_obat_detail.*, obat.id as id_obat, obat.kode_obat, obat.nama_obat, kemasan.id as id_kemasan, kemasan.kemasan')        
        ->join('obat', 'obat.id = permintaan_obat_detail.id_obat')
        ->join('kemasan', 'kemasan.id = obat.id_kemasan', 'left')
        ->where('id_permintaan_obat', $id)
        ->get('permintaan_obat_detail');
    }

    public function createPO($id_apotik, $record) {      
        $record['id_apotik'] = $id_apotik;   
        $record['no_permintaan'] = $this->generateID();
        $this->insert($record);
        $id_permintaan_obat = $this->db->insert_id();
        foreach ($record['obat'] as $row) {
            $this->db->insert('permintaan_obat_detail', array(
                'id_permintaan_obat' => $id_permintaan_obat,
                'id_obat' => $row['id_obat'],
                'jml' => $row['jml'],
                'jml_disetujui' => $row['jml']
            ));        
        }
        return true;
    }

    public function updatePO($id, $record) {            
        $permintaan = $this->find($id);
        $this->deletePO($id);
        $this->createPO($permintaan->id_apotik, $record);
        $id_permintaan_obat = $this->db->insert_id();
        foreach ($record['obat'] as $row) {
            $this->db->insert('permintaan_obat_detail', array(
                'id_permintaan_obat' => $id_permintaan_obat,
                'id_obat' => $row['id_obat'],
                'jml' => $row['jml'],
                'jml_disetujui' => $row['jml']
            ));            
        }
        return true;
    }

    public function deletePO($id) {
        $permintaan_obat = $this->find($id);        
        $rsObat = $this->db->where('id_permintaan_obat', $id)->get('permintaan_obat_detail')->result();
        $this->load->model('obat_m');
        foreach ($rsObat as $obat) {        
            if ($permintaan_obat->status == 2) {
                $this->db->set('stok', 'stok + '.$obat->jml, false)->where('id', $obat->id_obat)->update('obat');
                $this->db->set('stok', 'stok - '.$obat->jml, false)
                ->where('id_apotik', $permintaan_obat->id_apotik)
                ->where('id_obat', $obat->id_obat)
                ->update('obat_apotik');       
                $this->obat_m->update_stok_periode($row['id_obat']);    
                $this->obat_m->update_stok_apotik_periode($permintaan_obat->id_apotik, $row['id_obat']); 
            }
            $this->db->where('id', $obat->id)->delete('pengambilan_obat_detail');
        }
        $this->db->where('id', $id)->delete('permintaan_obat');
    }

    public function konfirmasi($id, $record) {
        $permintaan = $this->find($id);
        $this->update($id, array(
            'tgl_disetujui' => $record['tgl_disetujui'],
            'status' => 2
        ));
        $this->load->model('obat_m');
        foreach ($record['obat'] as $row) {
            $this->db->where('id_obat', $row['id_obat'])
            ->where('id_permintaan_obat', $id)
            ->update('permintaan_obat_detail', array(
                'jml_disetujui' => $row['jml_disetujui']
            ));
            $this->db->where('id', $row['id_obat'])->set('stok', 'stok - '.$row['jml_disetujui'], false)->update('obat');
            $result = $this->db->where('id_obat', $row['id_obat'])->get('obat_apotik')->row();
            if ($result) {
                $this->db->where('id_obat', $row['id_obat'])
                ->where('id_apotik', $permintaan->id_apotik)
                ->set('stok', 'stok + '.$row['jml_disetujui'], false)
                ->update('obat_apotik');
            } else {
                $this->db->insert('obat_apotik', array(
                    'id_obat' => $row['id_obat'],
                    'id_apotik' => $permintaan->id_apotik,
                    'stok' => $row['jml_disetujui']
                ));
            }
            $this->obat_m->update_stok_periode($row['id_obat']);
            $this->obat_m->update_stok_apotik_periode($permintaan->id_apotik, $row['id_obat']);
        }
    }

    public function generateID() {
        $prefix = 'PO/'. date('m') . '/' . date('Y') . '/';
        $digit = '000';
        $lastID =  $this->db->select_max('no_permintaan')
        ->where('left(no_permintaan,'.(strlen($prefix)).')', $prefix)
        ->get($this->table)->row()->no_permintaan;
        if ($lastID) {            
            $counter = substr($lastID, -strlen($digit)) + 1;
            return $prefix.substr($digit.$counter, -strlen($digit));
        } else {
            return $prefix.substr($digit.'1', -strlen($digit));
        }
    }

}