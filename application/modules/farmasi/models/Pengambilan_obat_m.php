<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Pengambilan_obat_m extends Base_m {
    protected $table = 'pengambilan_obat';
    protected $fillable = array('tgl_pengambilan:date', 'no_pengambilan', 'id_desa', 'diterima_oleh');
    protected $timestamp = true;

    public function getObat($id) {
        return $this->db->select('pengambilan_obat_detail.*, obat.id as id_obat, obat.kode_obat, obat.nama_obat, kemasan.id as id_kemasan, kemasan.kemasan')        
        ->join('obat', 'obat.id = pengambilan_obat_detail.id_obat')
        ->join('kemasan', 'kemasan.id = obat.id_kemasan', 'left')
        ->where('id_pengambilan_obat', $id)
        ->get('pengambilan_obat_detail');
    }

    public function createPO($record) {         
        $record['no_pengambilan'] = $this->generateID();
    	$this->insert($record);
    	$id_pengambilan_obat = $this->db->insert_id();
        $this->load->model('obat_m');
    	foreach ($record['obat'] as $row) {
    		$this->db->insert('pengambilan_obat_detail', array(
    			'id_pengambilan_obat' => $id_pengambilan_obat,
    			'id_obat' => $row['id_obat'],
    			'jml' => $row['jml']
    		));
    		$this->db->where('id', $row['id_obat'])->set('stok', 'stok - '.$row['jml'], false)->update('obat');            
            $this->obat_m->update_stok_periode($row['id_obat']);
    	}
    	return true;
    }

    public function updatePO($id, $record) {            
        $this->deletePO($id);
        $this->insert($record);
        $id_pengambilan_obat = $this->db->insert_id();
        $this->load->model('obat_m');
        foreach ($record['obat'] as $row) {
            $this->db->insert('pengambilan_obat_detail', array(
                'id_pengambilan_obat' => $id_pengambilan_obat,
                'id_obat' => $row['id_obat'],
                'jml' => $row['jml']
            ));
            $this->db->where('id', $row['id_obat'])->set('stok', 'stok - '.$row['jml'], false)->update('obat');
            $this->obat_m->update_stok_periode($row['id_obat']);
        }
        return true;
    }

    public function deletePO($id) {
        $rsObat = $this->db->where('id_pengambilan_obat', $id)->get('pengambilan_obat_detail')->result();
        $this->load->model('obat_m');
        foreach ($rsObat as $obat) {
            $this->db->set('stok', 'stok + '.$obat->jml, false)->where('id', $obat->id_obat)->update('obat');
            $this->db->where('id', $obat->id)->delete('pengambilan_obat_detail');
            $this->obat_m->update_stok_periode($obat->id);
        }
        $this->db->where('id', $id)->delete('pengambilan_obat');
    }

    public function generateID() {
        $prefix = 'PO/'. date('m') . '/' . date('Y') . '/';
        $digit = '000';
        $lastID =  $this->db->select_max('no_pengambilan')
        ->where('left(no_pengambilan,'.(strlen($prefix)).')', $prefix)
        ->get($this->table)->row()->no_pengambilan;
        if ($lastID) {            
            $counter = substr($lastID, -strlen($digit)) + 1;
            return $prefix.substr($digit.$counter, -strlen($digit));
        } else {
            return $prefix.substr($digit.'1', -strlen($digit));
        }
    }

}