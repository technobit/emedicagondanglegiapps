<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Obat_m extends Base_m {
    protected $table = 'obat';
    protected $fillable = array('kode_obat', 'nama_obat', 'id_kemasan' , 'id_jenis_obat', 'stok', 'stok_optimal', 'keterangan');


    public function getCountListObat($input){
        $this->db->select('obat.*')
        ->join('kemasan', 'kemasan.id = obat.id_kemasan')
        ->join('jenis_obat', 'jenis_obat.id = obat.id_jenis_obat')
        ->like('obat.nama_obat' , $input)
        
        ->from($this->table)
        ->count_all_results();
    }

    public function getListObat($input , $offset , $limit){
        return $this->db->select('obat.*')
        ->join('kemasan', 'kemasan.id = obat.id_kemasan')
        ->join('jenis_obat', 'jenis_obat.id = obat.id_jenis_obat')
        ->limit($limit)
        ->offset($offset)
        ->like('obat.nama_obat' , $input)
        ->get('obat')->result_array();
    }

    public function createObat($record) {
        $record['kode_obat'] = $this->generateID();
        $this->insert($record);
        $id_obat = $this->db->insert_id();
        $this->update_stok_periode($id_obat);
    }    

    public function findByKey($key) {
        return $this->db->select('obat.*, kemasan.kemasan')
        ->like('kode_obat', $key)
        ->or_like('nama_obat', $key)
        ->join('kemasan', 'kemasan.id = obat.id_kemasan')
        ->get('obat');
    }


    public function add_stok($id, $record) {
        $this->db->insert('tambah_stok', array(
            'tgl'  => date('Y-m-d', strtotime($record['tgl'])),
            'id_obat' => $id,
            'jml' => $record['jml'],
            'stok_awal' => $record['stok'],
            'stok_akhir' => $record['stok_akhir'],
            'created_at' => date('Y-m-d H:i:s')
        ));
        $this->db->where('id', $id)->set('stok', 'stok+'.$record['jml'], false)->update('obat');
        return true;
    }

    public function findObatApotik($id_apotik, $id_obat) {
        return $this->db->select('obat.*, obat_apotik.stok as stok_apotik')
        ->join('(select * from obat_apotik where id_apotik="'.$id_apotik.'") as obat_apotik', 'obat.id=obat_apotik.id_obat', 'left')        
        ->where('id_obat', $id_obat)
        ->get($this->table)
        ->row();
    }

    public function opname($id_apotik, $id_obat, $post) {
        $obat_apotik = $this->db->where('id_apotik', $id_apotik)
        ->where('id_obat', $id_obat)
        ->get('obat_apotik')
        ->row();
        if ($obat_apotik) {
            $this->db->where('id_apotik', $id_apotik)
            ->where('id_obat', $id_obat)
            ->set('stok', $post['stok_akhir'])
            ->update('obat_apotik');
        } else {
            $this->db->insert('obat_apotik',array(
                'id_apotik' => $id_apotik,
                'id_obat' => $id_obat,
                'stok' => $post['stok_akhir']
            ));
        }
        $this->db->insert('obat_opname', array(
            'tgl' => date('Y-m-d', strtotime($post['tgl'])),
            'id_apotik' => $id_apotik,
            'id_obat' => $id_obat,
            'stok_awal' => $post['stok_apotik'],
            'stok_akhir' => $post['stok_akhir']
        ));
        return true;
    }

    public function update_stok_periode($id_obat, $periode = null) {
        if (!$periode) {
            $periode = date('Y-m-01');
        } else {
            $periode = date('Y-m-01', strtotime($periode));
        }
        $obat = $this->find($id_obat);
        $obat_periode = $this->db->where('id_obat', $id_obat)
        ->where('periode', $periode)
        ->get('obat_periode')
        ->row();
        if ($obat_periode) {
            $this->db->where('id_obat', $id_obat)
            ->where('periode', $periode)
            ->update('obat_periode', array(
                'stok' => $obat->stok
            ));
        } else {
            $this->db->insert('obat_periode', array(
                'id_obat' => $id_obat,
                'periode' => $periode,
                'stok' => $obat->stok
            ));
        }
    }

    public function update_stok_apotik_periode($id_apotik, $id_obat, $periode = null) {
        if (!$periode) {
            $periode = date('Y-m-01');
        } else {
            $periode = date('Y-m-01', strtotime($periode));
        }
        $obat = $this->findObatApotik($id_apotik, $id_obat);
        $obat_periode = $this->db->where('id_apotik', $id_apotik)
        ->where('id_obat', $id_obat)
        ->where('periode', $periode)
        ->get('obat_apotik_periode')
        ->row();
        if ($obat_periode) {
            $this->db->where('id_apotik', $id_apotik)
            ->where('id_obat', $id_obat)
            ->where('periode', $periode)
            ->update('obat_apotik_periode', array(
                'stok' => $obat->stok_apotik
            ));
        } else {
            $this->db->insert('obat_apotik_periode', array(
                'id_apotik' => $id_apotik,
                'id_obat' => $id_obat,
                'periode' => $periode,
                'stok' => $obat->stok_apotik
            ));
        }
    }

    public function generateID() {
        $prefix = 'OB';
        $digit = '0000';
        $lastID =  $this->db->select_max('kode_obat')
        ->where('left(kode_obat,'.(strlen($prefix)).')', $prefix)
        ->get($this->table)->row()->kode_obat;
        if ($lastID) {            
            $counter = substr($lastID, -strlen($digit)) + 1;
            return $prefix.substr($digit.$counter, -strlen($digit));
        } else {
            return $prefix.substr($digit.'1', -strlen($digit));
        }
    }

}