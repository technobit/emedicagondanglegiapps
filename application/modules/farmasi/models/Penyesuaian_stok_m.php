<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Penyesuaian_stok_m extends Base_m {
    protected $table = 'penyesuaian_stok';
    protected $fillable = array('id_apotik', 'tgl_penyesuaian:date', 'no_penyesuaian');
    protected $timestamp = true;

    public function getObat($id) {
        return $this->db->select('penyesuaian_stok.*, penyesuaian_stok_detail.*, obat.id as id_obat, obat.kode_obat, obat.nama_obat, kemasan.id as id_kemasan, kemasan.kemasan')        
        ->join('penyesuaian_stok_detail', 'penyesuaian_stok.id = penyesuaian_stok_detail.id_penyesuaian_stok')
        ->join('obat', 'obat.id = penyesuaian_stok_detail.id_obat')
        ->join('kemasan', 'kemasan.id = obat.id_kemasan')
        ->where('penyesuaian_stok.id', $id)
        ->get('penyesuaian_stok');
    }

    public function createPO($id_apotik, $record) {
        $record['id_apotik'] = $id_apotik;         
        $record['no_penyesuaian'] = $this->generateID();
    	$this->insert($record);
    	$id_penyesuaian_stok = $this->db->insert_id();
        $this->load->model('obat_m');
    	foreach ($record['obat'] as $row) {
    		$this->db->insert('penyesuaian_stok_detail', array(
    			'id_penyesuaian_stok' => $id_penyesuaian_stok,
    			'id_obat' => $row['id_obat'],
                'jml' => $row['jml'],
                'jml_akhir' => $row['jml_akhir'],
                'perubahan' => $row['jml_akhir'] - $row['jml']
    		));
    		$this->db->where('id_apotik', $id_apotik)->where('id_obat', $row['id_obat'])->set('stok', $row['jml_akhir'], false)->update('obat_apotik');
            $this->obat_m->update_stok_apotik_periode($id_apotik, $row['id_obat']);             
    	}
    	return true;
    }

    /*public function deletePO($id) {
        $rsObat = $this->db->where('id_penyesuaian_stok', $id)->get('penyesuaian_stok_detail')->result();
        foreach ($rsObat as $obat) {
            $this->db->set('stok', 'stok + '.$obat->jml, false)->where('id', $obat->id_obat)->update('obat_apotik');
            $this->db->where('id', $obat->id)->delete('penyesuaian_stok_detail');
        }
        $this->db->where('id', $id)->delete('penyesuaian_stok');
    }*/

    public function generateID() {
        $prefix = 'PO/'. date('m') . '/' . date('Y') . '/';
        $digit = '000';
        $lastID =  $this->db->select_max('no_penyesuaian')
        ->where('left(no_penyesuaian,'.(strlen($prefix)).')', $prefix)
        ->get($this->table)->row()->no_penyesuaian;
        if ($lastID) {            
            $counter = substr($lastID, -strlen($digit)) + 1;
            return $prefix.substr($digit.$counter, -strlen($digit));
        } else {
            return $prefix.substr($digit.'1', -strlen($digit));
        }
    }

}