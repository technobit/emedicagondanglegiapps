<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rekam_medis_model extends CI_Model {
	
	///	Master Identifier
	var $table = "rekam_medis";
	
	///	Detail Rekam medis Umum + Anak
	var $table_detail = "rekam_medis_detail";
	var $table_diagnosa = "rekam_medis_diagnosa";
	
	///	Detail Rekam medis Poli Gigi
	var $table_detail_gigi = "rekam_medis_gigi";
	var $table_diagnosa_gigi = "rekam_medis_diagnosa_gigi";
	var $table_tindakan = "rekam_medis_tindakan";
	
	var $primary_key = "txtIdRekamMedis";
	var $secondar_key = "txtNoRekamMedis";
	
	public function __construct(){
		parent::__construct();
	}
	
	public function getListPasienRekamMedis($offset=0 , $limit=10 , $search = ""){
		
		if(!empty($search)){
			$this->db->like("txtNoRekamMedis",$search);
			$this->db->or_like("txtNoAnggota",$search);
			$this->db->or_like("txtNamaPasien",$search);
		}
		
		$this->db->select("rekam_medis.txtNoRekamMedis,pasien.txtNoAnggota,pasien.txtNamaPasien,pasien.dtTanggalLahir,pasien.charJkPasien,rekam_medis.txtIdRekamMedis");
		$this->db->from($this->table);
		$this->db->join("pasien" , "rekam_medis.txtIdPasien = pasien.txtIdPasien","inner");
		$this->db->limit($limit);
		$this->db->offset($offset);
		$data = $this->db->get();
		return $data->result_array();
	}
	
	public function countListPasienRekamMedis($search){
		if(!empty($search)){
			$this->db->like("txtNoRekamMedis",$search);
			$this->db->or_like("txtNoAnggota",$search);
			$this->db->or_like("txtNamaPasien",$search);
		}
		
		$this->db->select("rekam_medis.txtNoRekamMedis,pasien.txtNoAnggota,pasien.txtNamaPasien,pasien.dtTanggalLahir,pasien.charJkPasien,rekam_medis.txtIdRekamMedis");
		$this->db->from($this->table);
		$this->db->join("pasien" , "rekam_medis.txtIdPasien = pasien.txtIdPasien","inner");
		$data = $this->db->count_all_results();
		return $data;
	}
	
	public function getMasterByNoRekmed($noRekmed){
		$this->db->select($this->table.".*");
		$this->db->from($this->table);
		$this->db->where($this->secondar_key , $noRekmed);
		$data = $this->db->get();
		return $data->row_array();
	}
	
	public function getMasterByIdPasien($idPasien){
		$this->db->select($this->table.".*");
		$this->db->from($this->table);
		$this->db->where("txtIdPasien" , $idPasien);
		$data = $this->db->get();
		return $data->row_array();
	}
	
	public function getCountNoRekamMedis($noRekmed){
		$this->db->select($this->table.".*");
		$this->db->from($this->table);
		$this->db->where($this->secondar_key , $noRekmed);
		$data = $this->db->count_all_results();
		return $data;
	}
	
	public function getDetailByIdPasien($idPasien){
		$this->db->select($this->table.'.* ');
		$this->db->from($this->table);
		$this->db->where("txtIdPasien" , $idPasien);
		$data = $this->db->get();
		return $data->row_array();
	}
	
	public function detail($idRekmed){
		$this->db->select($this->table.'.* ');
		$this->db->from($this->table);
		$this->db->where($this->primary_key , $idRekmed);
		$data = $this->db->get();
		return $data->row_array();
	}
	
	public function insertRekamMedis($arrData , $debug = false){
		$this->db->set($arrData);
		if($debug){
			$retVal = $this->db->get_compiled_insert($this->table);
		}
		else{
			$res = $this->db->insert($this->table);
			if(!$res){
				$retVal['error_stat'] = "Failed To Insert";
				$retVal['status'] = false;
			}
			else{
				$retVal['error_stat'] = "Success To Insert";
				$retVal['status'] = true;
			}
			
		}
		return $retVal;
	}
	
	public function updateRekamMedis($array , $id){
		
		$this->db->where($this->primary_key , $id);
		$query = $this->db->update($this->table, $array);
		if(!$query){
			$retVal['error_stat'] = "Failed To Insert";
			$retVal['status'] = false;
		}
		else{
			$retVal['error_stat'] = "Success To Update";
			$retVal['status'] = true;
			$retVal['id'] = $id;
		}
		
		return $retVal;
	}
	
	///	Detail Rekam Medis Untuk poli Umum Dan Anak-anak
				public function insertDetailRekamMedis($arrData , $table = "",$debug = false ){
		$table_insert = $this->table_detail;
		if($table=="gigi"){
			$table_insert = $this->table_detail_gigi;
		}
		$this->db->set($arrData);
		if($debug){
			$retVal = $this->db->get_compiled_insert($table_insert);
		}
		else{
			$res = $this->db->insert($table_insert);
			if(!$res){
				$retVal['error_stat'] = "Failed To Insert";
				$retVal['status'] = false;
				$retVal['id'] = "";
			}
			else{
				$retVal['error_stat'] = "Success To Insert";
				$retVal['status'] = true;
				$retVal['id'] = $this->db->insert_id();
			}
			
		}
		return $retVal;
	}
	
	public function updateDetailRekamMedis($arrData ,$id , $table = ""){
		$table_insert = $this->table_detail;
		
		if($table=="gigi"){
			$table_insert = $this->table_detail_gigi;
		}
		
		$this->db->where("txtIdRekmedDetail", $id);
		$query = $this->db->update($table_insert, $arrData);
		
		if(!$query){
			$retVal['error_stat'] = "Failed To Insert";
			$retVal['status'] = false;
			$retVal['id'] = $id;
		}
		else{
			$retVal['error_stat'] = "Success To Insert";
			$retVal['status'] = true;
			$retVal['id'] = $id;
		}
		return $retVal;
	}
	
	public function deleteDetailRekamMedis($idRekamMedis , $idDetailRekmed , $table = ""){
		$table_insert = $this->table_detail;
		if($table=="gigi"){
			$table_insert = $this->table_detail_gigi;
		}
		$this->db->where("txtIdRekmedDetail" , $idDetailRekmed);
		$this->db->where("txtIdRekamMedis" , $idRekamMedis);
		$q = $this->db->delete($table_insert);
		if(!$q){
			$retVal['error_stat'] = "Failed To Delete";
			$retVal['status'] = false;
		}
		else{
			$retVal['error_stat'] = "Success To Delete";
			$retVal['status'] = true;
		}
		return $retVal;
	}
	
	///	Detail Diagnosa
				public function checkDetailDiagnosa($idDetailRekmed , $table=""){
		$table_insert = $this->table_diagnosa;
		if($table=="gigi"){
			$table_insert = $this->table_diagnosa_gigi;
		}
		$this->db->select($table_insert.".intIdRekamMedisDiagnosa");
		$this->db->from($table_insert);
		$this->db->where("txtIdRekmedDetail" , $idDetailRekmed);
		$data = $this->db->count_all_results();
		return $data;
	}
	
	public function getIdRekamMedis($idDetailRekmed){
		
		$this->db->select("rekam_medis_detail.txtIdRekamMedis");
		$this->db->from("rekam_medis_detail");
		$this->db->where("txtIdRekmedDetail" , $idDetailRekmed);
		$data = $this->db->get();
		$resVal = $data->row_array();
		$txtIdRekmed = $resVal['txtIdRekamMedis'];
		return $txtIdRekmed;
	}
	
	public function insertDetailDiagnosa($arrData , $table = ""){
		$table_insert = $this->table_diagnosa;
		if($table=="gigi"){
			$table_insert = $this->table_diagnosa_gigi;
		}
		
		$this->db->set($arrData);
		$res = $this->db->insert($table_insert);
		if(!$res){
			$retVal['error_stat'] = "Failed To Insert";
			$retVal['status'] = false;
			$retVal['id'] = 0;
		}
		else{
			$retVal['error_stat'] = "Success To Insert";
			$retVal['status'] = true;
			$retVal['id'] = $this->db->insert_id();
		}
		return $retVal;
	}

	public function insertBatchDiagnosa($arrData){
		$table_insert = $this->table_diagnosa;
		$res = $this->db->insert_batch($table_insert , $arrData);
		if(!$res){
			$retVal['error_stat'] = "Failed To Insert";
			$retVal['status'] = false;
		}else{
			$retVal['error_stat'] = "Success To Insert";
			$retVal['status'] = true;
		}
		return $retVal;
	}
	
	public function deleteDetailDiagnosa($idDetailRekmed , $table = ""){
		$table_insert = $this->table_diagnosa;
		if($table=="gigi"){
			$table_insert = $this->table_diagnosa_gigi;
		}
		
		$this->db->where("txtIdRekmedDetail" , $idDetailRekmed);
		$q = $this->db->delete($table_insert);
		if(!$q){
			$retVal['error_stat'] = "Failed To Delete";
			$retVal['status'] = false;
		}
		else{
			$retVal['error_stat'] = "Success To Delete";
			$retVal['status'] = true;
		}
		
		return $retVal;
	}
	///	Detaik Tindakan
	public function checkDetailTindakan($idDetailRekmed , $table=""){
		
		
		
		/*
		$table_insert = $this->table_diagnosa;
		if($table=="gigi"){
			$table_insert = $this->table_diagnosa_gigi;
		}
		*/
		$table_insert = $this->table_tindakan;
		$this->db->select($table_insert.".intIdRekamMedisDiagnosa");
		$this->db->from($table_insert);
		$this->db->where("txtIdRekmedDetail" , $idDetailRekmed);
		$data = $this->db->count_all_results();
		return $data;
	}
	
	public function insertDetailTindakan($arrData , $table = ""){
		
		
		
		/*
		$table_insert = $this->table_diagnosa;
		if($table=="gigi"){
			$table_insert = $this->table_diagnosa_gigi;
		}
		*/
		$table_insert = $this->table_tindakan;
		$this->db->set($arrData);
		$res = $this->db->insert($table_insert);
		if(!$res){
			$retVal['error_stat'] = "Failed To Insert";
			$retVal['status'] = false;
		}
		else{
			$retVal['error_stat'] = "Success To Insert";
			$retVal['status'] = true;
		}
		return $retVal;
	}

	public function insertBatchTindakan($arrData){
		$table_insert = $this->table_tindakan;
		$res = $this->db->insert_batch($table_insert , $arrData);
		if(!$res){
			$retVal['error_stat'] = "Failed To Insert";
			$retVal['status'] = false;
		}else{
			$retVal['error_stat'] = "Success To Insert";
			$retVal['status'] = true;
		}
		return $retVal;
	}
	
	public function deleteDetailTindakan($idDetailRekmed , $table = ""){
		
		
		
		/*
		$table_insert = $this->table_diagnosa;
		if($table=="gigi"){
			$table_insert = $this->table_diagnosa_gigi;
		}
		*/
								$table_insert = $this->table_tindakan;
		$this->db->where("txtIdRekmedDetail" , $idDetailRekmed);
		$q = $this->db->delete($table_insert);
		if(!$q){
			$retVal['error_stat'] = "Failed To Delete";
			$retVal['status'] = false;
		}
		else{
			$retVal['error_stat'] = "Success To Delete";
			$retVal['status'] = true;
		}
		
		return $retVal;
	}
	
	public function getDetailTindakan($idRekmedDetail){
		$this->db->select("
						  tindakan.intIdTindakan as id,
						  tindakan.txtTindakan,
						  tindakan.txtDeskripsi,
						  ".$this->table_tindakan.".txtDeskripsiTindakan,
						  ".$this->table_tindakan.".bitIsUM,
						  ".$this->table_tindakan.".bitIsIH,
						  ".$this->table_tindakan.".bitIsAP,
						  ".$this->table_tindakan.".bitIsAS
						  ");
		$this->db->from($this->table_tindakan);
		$this->db->where("txtIdRekmedDetail" , $idRekmedDetail);
		$this->db->join("tindakan" , $this->table_tindakan.".intIdTindakan = tindakan.intIdTindakan","left");
		$data = $this->db->get();
		return $data->result_array();
	}
	
	///	For Table And View Rekam Medis Poli Umum
	public function getDetailByIdKunjungan($idKunjungan){
		$this->db->select("
						  rekam_medis_detail.txtIdRekmedDetail,
						  rekam_medis_detail.txtPemeriksaan,
						  rekam_medis_detail.txtTindakan,
						  rekam_medis_detail.txtKeterangan,
						  rekam_medis_detail.txtPengobatan,
						  rekam_medis_diagnosa.txtDetailDiagnosa,
						  data_penyakit.intIdPenyakit,
						  data_penyakit.txtCategory,
						  data_penyakit.txtSubCategory,
						  data_penyakit.txtIndonesianName,
						  ");
		$this->db->from($this->table_detail);
		$this->db->where("txtIdKunjungan" , $idKunjungan);
		$this->db->join("rekam_medis_diagnosa" , "rekam_medis_detail.txtIdRekmedDetail = rekam_medis_diagnosa.txtIdRekmedDetail","left");
		$this->db->join("data_penyakit" , "rekam_medis_diagnosa.intIdDiagnosaPenyakit = data_penyakit.intIdPenyakit","left");
		$data = $this->db->get();
		return $data->row_array();
	}
	
	public function getDetailByIdRekamMedis($idRekamMedis , $offset = 0 , $limit = 5, $intIdRekamMedisDetail="" ){
		
		$this->db->select("
						  pelayanan.txtNama as namaPoli,
						  pegawai.txtNamaPegawai as namaPegawai,
						  rekam_medis_detail.txtIdRekmedDetail,
						  rekam_medis_detail.intIdPelayanan,
						  rekam_medis_detail.txtAnamnesa,
						  rekam_medis_detail.txtPemeriksaan,
						  rekam_medis_detail.txtSistole,
						  rekam_medis_detail.txtDiastole,
						  rekam_medis_detail.txtRespiratoryRate,
						  rekam_medis_detail.txtHeartRate,
						  rekam_medis_detail.txtKesadaran,
						  rekam_medis_detail.intIdPelayanan,
						  rekam_medis_detail.txtTindakan,
						  rekam_medis_detail.txtKeterangan,
						  rekam_medis_detail.txtPengobatan,
						  rekam_medis_detail.txtTandaTangan,
						  register.txtUsiaPasienKunjungan,
						  register.dtTanggalKunjungan,
						  ");
		$this->db->from($this->table_detail);
		$this->db->where($this->primary_key , $idRekamMedis);
		
		if(!empty($intIdRekamMedisDetail)){
			$this->db->where("rekam_medis_detail.txtIdRekmedDetail" , $intIdRekamMedisDetail);
		}
		
		$this->db->join("register" , "rekam_medis_detail.txtIdKunjungan = register.txtIdKunjungan","left");
		$this->db->join("pegawai" , "rekam_medis_detail.intIdPegawai = pegawai.intIdPegawai","left");
		$this->db->join("pelayanan" , "rekam_medis_detail.intIdPelayanan = pelayanan.intIdPelayanan","left");
		$this->db->where("rekam_medis_detail.bitStatusRekmedDetail" , 1);
		$this->db->order_by("rekam_medis_detail.dtCreatedDate" , "DESC");
		$this->db->limit($limit);
		$this->db->offset($offset);
		$data = $this->db->get();
		return $data->result_array();
	}
	
	public function getDataDetailRekamMedis($dtStart , $dtEnd , $intIDPelayanan , $limit , $offset) {
		$this->db->select("
						  pelayanan.txtNama as namaPoli,
						  pegawai.txtNamaPegawai as namaPegawai,
						  rekam_medis_detail.txtIdRekamMedis,
						  rekam_medis_detail.txtIdRekmedDetail,
						  rekam_medis_detail.intIdPelayanan,
						  rekam_medis_detail.txtAnamnesa,
						  rekam_medis_detail.txtPemeriksaan,
						  rekam_medis_detail.txtSistole,
						  rekam_medis_detail.txtDiastole,
						  rekam_medis_detail.txtRespiratoryRate,
						  rekam_medis_detail.txtHeartRate,
						  rekam_medis_detail.intIdPelayanan,
						  rekam_medis_detail.txtTindakan,
						  rekam_medis_detail.txtKeterangan,
						  rekam_medis_detail.txtPengobatan,
						  rekam_medis_detail.txtTandaTangan,
						  register.txtUsiaPasienKunjungan,
						  register.dtTanggalKunjungan,
						  ");
		
		$this->db->from("register");
		///		$this->db->where($this->primary_key , $idRekamMedis);
		$this->db->join("rekam_medis_detail" , "rekam_medis_detail.txtIdKunjungan = register.txtIdKunjungan");
		$this->db->join("pegawai" , "rekam_medis_detail.intIdPegawai = pegawai.intIdPegawai","left");
		$this->db->join("pelayanan" , "rekam_medis_detail.intIdPelayanan = pelayanan.intIdPelayanan","left");
		$this->db->where("register.dtTanggalKunjungan BETWEEN '".$dtStart."' AND '".$dtEnd."'");
		
		if($intIDPelayanan!=0){
			$this->db->where("register.intIdPelayanan" , $intIDPelayanan);
		}
		
		$this->db->where("rekam_medis_detail.bitStatusRekmedDetail" , 1);
		$this->db->order_by("rekam_medis_detail.dtCreatedDate" , "DESC");
		$this->db->limit($limit);
		$this->db->offset($offset);
		$this->db->group_by("register.txtIdKunjungan");
		$data = $this->db->get();
		return $data->result_array();
	}
	
	public function getCountDataDetailRekamMedis($dtStart , $dtEnd , $intIDPelayanan){
		
		$this->db->from($this->table_detail);
		$this->db->join("register" , "rekam_medis_detail.txtIdKunjungan = register.txtIdKunjungan","left");
		$this->db->join("pegawai" , "rekam_medis_detail.intIdPegawai = pegawai.intIdPegawai","left");
		$this->db->join("pelayanan" , "rekam_medis_detail.intIdPelayanan = pelayanan.intIdPelayanan","left");
		$this->db->where("register.dtTanggalKunjungan BETWEEN '".$dtStart."' AND '".$dtEnd."'");
		if($intIDPelayanan!=0){
			$this->db->where("register.intIdPelayanan" , $intIDPelayanan);
		}
		$this->db->where("rekam_medis_detail.bitStatusRekmedDetail" , 1);
		$data = $this->db->count_all_results();
		return $data;
	}
	
	public function getDetailRawatInap($intIdRekamMedis , $intIdRekamMedisDetail){
		$this->db->select("*");
		$this->db->from("rekam_medis_detail");
		$this->db->join("rekam_medis" , "rekam_medis.txtIdRekamMedis = rekam_medis_detail.txtIdRekamMedis","inner");
		$this->db->join("register" , "rekam_medis_detail.txtIdKunjungan = register.txtIdKunjungan","inner");
		$this->db->join("pasien" , "pasien.txtIdPasien = register.txtIdPasien","inner");
		$this->db->join("register_kamar" , "register_kamar.txtIdKunjungan = register.txtIdKunjungan","inner");
		$this->db->join("kamar" , "register_kamar.intIdKamar = kamar.intIdKamar","inner");
		$this->db->join("jaminan_kesehatan" , "register.intIdJaminanKesehatan = jaminan_kesehatan.intIdJaminanKesehatan","inner");
		$this->db->where("rekam_medis_detail.txtIdRekamMedis" , $intIdRekamMedis);
		$this->db->where("rekam_medis_detail.txtIdRekmedDetail" , $intIdRekamMedisDetail);
		$data = $this->db->get();
		return $data->row_array();
	}
	
	///	/ Rawat getDetailRawatInap
	
	public function getDetailRiwayatRawatInap($idRekamMedis , $start , $length){
		$this->db->select("register.dtTanggalKunjungan , data_penyakit.txtIndonesianName as nama_penyakit, kamar.txtKamar, register.dtSelesaiKunjungan,rekam_medis_detail.txtIdRekamMedis,rekam_medis_detail.txtIdRekmedDetail");
		$this->db->from($this->table_detail);
		$this->db->where($this->primary_key , $idRekamMedis);
		$this->db->join("register" , "rekam_medis_detail.txtIdKunjungan = register.txtIdKunjungan","left");
		$this->db->join("register_kamar" , "register_kamar.txtIdKunjungan = register.txtIdKunjungan","left");
		$this->db->join("kamar" , "kamar.intIdKamar = register_kamar.intIdKamar","left");
		$this->db->join("rekam_medis_diagnosa" , "rekam_medis_detail.txtIdRekmedDetail = rekam_medis_diagnosa.txtIdRekmedDetail","left");
		$this->db->join("data_penyakit" , "rekam_medis_diagnosa.intIdDiagnosaPenyakit = data_penyakit.intIdPenyakit","left");
		$this->db->join("pegawai" , "rekam_medis_detail.intIdPegawai = pegawai.intIdPegawai","left");
		$this->db->join("pelayanan" , "rekam_medis_detail.intIdPelayanan = pelayanan.intIdPelayanan","left");
		$this->db->where("register.bitStatusKunjungan" , 2);
		$this->db->where("rekam_medis_detail.bitStatusRekmedDetail" , 2);
		$this->db->order_by("rekam_medis_detail.dtCreatedDate" , "DESC");
		$this->db->limit($length);
		$this->db->offset($start);
		$data = $this->db->get();
		return $data->result_array();
	}
	
	public function countDetailRiwayatInap($idRekamMedis){
		$this->db->select("register.dtTanggalKunjungan , data_penyakit.txtIndonesianName as nama_penyakit, kamar.txtKamar, register.dtSelesaiKunjungan,rekam_medis_detail.txtIdRekamMedis,rekam_medis_detail.txtIdRekmedDetail");
		$this->db->from($this->table_detail);
		$this->db->where($this->primary_key , $idRekamMedis);
		$this->db->join("register" , "rekam_medis_detail.txtIdKunjungan = register.txtIdKunjungan","left");
		$this->db->join("register_kamar" , "register_kamar.txtIdKunjungan = register.txtIdKunjungan","left");
		$this->db->join("kamar" , "kamar.intIdKamar = register_kamar.intIdKamar","left");
		$this->db->join("rekam_medis_diagnosa" , "rekam_medis_detail.txtIdRekmedDetail = rekam_medis_diagnosa.txtIdRekmedDetail","left");
		$this->db->join("data_penyakit" , "rekam_medis_diagnosa.intIdDiagnosaPenyakit = data_penyakit.intIdPenyakit","left");
		$this->db->join("pegawai" , "rekam_medis_detail.intIdPegawai = pegawai.intIdPegawai","left");
		$this->db->join("pelayanan" , "rekam_medis_detail.intIdPelayanan = pelayanan.intIdPelayanan","left");
		$this->db->where("register.bitStatusKunjungan" , 2);
		$this->db->where("rekam_medis_detail.bitStatusRekmedDetail" , 2);
		$data = $this->db->count_all_results();
		return $data;
	}
	
	
	public function countDetailByIdRekamMedis($idRekamMedis){
		$this->db->select("rekam_medis_detail.txtIdRekmedDetail");
		$this->db->from($this->table_detail);
		$this->db->where($this->primary_key , $idRekamMedis);
		$this->db->join("pegawai" , "rekam_medis_detail.intIdPegawai = pegawai.intIdPegawai","left");
		$this->db->join("pelayanan" , "rekam_medis_detail.intIdPelayanan = pelayanan.intIdPelayanan","left");
		$this->db->join("register" , "rekam_medis_detail.txtIdKunjungan = register.txtIdKunjungan","left");
		$data = $this->db->count_all_results();
		return $data;
	}
	
	public function getDetailRekamMedisByIdDetail($idRekamMedis , $idDetailRekamMedis){
		$this->db->select("
						  pelayanan.txtNama as namaPoli,
						  pegawai.txtNamaPegawai as namaPegawai,
						  rekam_medis_detail.txtIdRekamMedis,
						  rekam_medis_detail.txtIdRekmedDetail,
						  rekam_medis_detail.intIdPelayanan,
						  rekam_medis_detail.intIdPegawai,
						  rekam_medis_detail.txtPemeriksaan,
						  rekam_medis_detail.txtAnamnesa,
						  rekam_medis_detail.txtTinggi,
						  rekam_medis_detail.txtBerat,
						  rekam_medis_detail.txtSistole,
						  rekam_medis_detail.txtDiastole,
						  rekam_medis_detail.txtOdontogram,
						  rekam_medis_detail.txtRespiratoryRate,
						  rekam_medis_detail.txtHeartRate,
						  rekam_medis_detail.txtKesadaran,
						  rekam_medis_detail.txtTindakan,
						  rekam_medis_detail.txtKeterangan,
						  rekam_medis_detail.txtPengobatan,
						  rekam_medis_detail.txtTandaTangan,
						  rekam_medis_bpjs.noKunjungan,
						  rekam_medis_bpjs.kdDiag1,
						  rekam_medis_bpjs.kdDiag2,
						  rekam_medis_bpjs.kdDiag3,
						  rekam_medis_bpjs.txtDiag1,
						  rekam_medis_bpjs.txtDiag2,
						  rekam_medis_bpjs.txtDiag3,
						  register.txtUsiaPasienKunjungan,
						  register.dtTanggalKunjungan,
						  pasien.dtTanggalLahir
						  ");
		$this->db->from($this->table_detail);
		$this->db->where($this->primary_key , $idRekamMedis);
		$this->db->where("txtIdRekmedDetail" , $idDetailRekamMedis);
		$this->db->join("register" , "rekam_medis_detail.txtIdKunjungan = register.txtIdKunjungan","left");
		$this->db->join("pasien" , "register.txtIdPasien = pasien.txtIdPasien","left");
		$this->db->join("pegawai" , "rekam_medis_detail.intIdPegawai = pegawai.intIdPegawai","left");
		$this->db->join("pelayanan" , "rekam_medis_detail.intIdPelayanan = pelayanan.intIdPelayanan","left");
		$this->db->join("rekam_medis_bpjs" , "rekam_medis_detail.txtIdRekmedDetail = rekam_medis_bpjs.txtIdRekamMedisDetail","left");
		$this->db->order_by("rekam_medis_detail.dtCreatedDate" , "DESC");
		$data = $this->db->get();
		return $data->row_array();
	}
	
	public function getDetailDiagnosa($idRekmedDetail, $table = ""){
		$this->db->select("
						  data_penyakit.intIdPenyakit as id,
						  data_penyakit.txtCategory,
						  data_penyakit.txtSubCategory,
						  data_penyakit.txtIndonesianName,
						  rekam_medis_diagnosa.txtDetailDiagnosa
						  ");
		$this->db->from($this->table_diagnosa);
		$this->db->where("txtIdRekmedDetail" , $idRekmedDetail);
		$this->db->join("data_penyakit" , "rekam_medis_diagnosa.intIdDiagnosaPenyakit = data_penyakit.intIdPenyakit","left");
		$data = $this->db->get();
		return $data->result_array();
	}
	
	public function getDetailRekamMedisByIdKunjungan($idRekamMedis,$idKunjungan){
		$this->db->from($this->table_detail);
		$this->db->where('txtIdRekamMedis' , $idRekamMedis);
		$this->db->where('txtIdKunjungan' , $idRekamMedis);
		$data = $this->db->get();
		return $data->row_array();
	}
	
	public function checkNewCaseDetailDiagnosa($txtIdRekmed , $intIdDiagnosaPenyakit){
		$this->db->from("rekam_medis");
		$this->db->join("rekam_medis_detail" , "rekam_medis_detail.txtIdRekamMedis = rekam_medis.txtIdRekamMedis");
		$this->db->join("rekam_medis_diagnosa" , "rekam_medis_detail.txtIdRekmedDetail = rekam_medis_diagnosa.txtIdRekmedDetail");
		$this->db->where('rekam_medis.txtIdRekamMedis' , $txtIdRekmed);
		$this->db->where('rekam_medis_diagnosa.intIdDiagnosaPenyakit' , $intIdDiagnosaPenyakit);
		$this->db->like("rekam_medis_diagnosa.dtCreatedDate" , date("Y"));
		$count = $this->db->count_all_results();
		$retVal = $count < 1 ? true : false;
		return $retVal;
	}
	
	
	
	public function deleteRekamMedisRawatInap($txtIdRekmedDetail){
		$query = $this->db->query("CALL deleteRekamMedisInap('".$txtIdRekmedDetail."')");
		$retVal = array();
		$retVal['status'] = $query;
		if($query){
			$retVal['message'] = "Data Rekam medis berhasil Di Hapus";
		}
		else{
			$retVal['message'] = "Data Rekam medis Gagal Di Hapus";
		}
		return $retVal;
	}
	
}

