
<section class="content-header">
    <h1 class="title"><?=$title?></h1>
    <?=$breadcrumbs?>
</section>
<section class="content">
  <?= $this->form->open(null, 'id="form-pelayanan" class="form-horizontal" data-apotik="'.$this->uri->segment(2).'"') ?>
    <div class="row">
      <div class="col-md-8">
        <!--div class="box box-primary">
          <div class="box-header">
            <h3 class="box-title">Resep</h3>
          </div>
          <div class="box-body">
            <div class="form-group">            
              <div class="col-md-12">
                <?= $this->form->textarea('txtPengobatan', $txtPengobatan, 'class="form-control" style="height:150px;" readonly') ?>
              </div>
            </div> 
          </div>
        </div-->
        <div class="box box-primary">
          <div class="box-header">
            <h3 class="box-title">Pemberian Obat</h3>
          </div>
          <div class="box-body">
            <div class="form-group">
              <div class="col-md-8">
                <!--<div class="input-group">
                  <?= $this->form->text('txtObat', null, 'placeholder="Nama / Kode Obat" id="txtObat" class="form-control input-lg"') ?>   
                  <div class="input-group-btn">           
                    <button type="button" id="btnTambahObat" class="btn btn-success btn-lg btn-flat"><i class="fa fa-plus"></i></button>
                    <button type="button" id="btnCariObat" class="btn btn-primary btn-lg btn-flat"><i class="fa fa-search"></i> Cari Obat</button>
                  </div>
                </div>-->                
              </div>
            </div>
            <table id="tblObat" class="table table-bordered table-condensed table-hover">
              <thead>
                <tr>
                  <th>Obat</th>                  
                  <th width="70px">Jumlah</th>
                  <th width="125px">Keterangan</th>
                  <th width="1px" class="text-center">Aksi</th>
                </tr>
              </thead>
              <tbody>
                
                <?php 
                if (formValue('obat')) { ?>    
                  <?php $rowsObat = 0; ?>          
                  <?php foreach(formValue('obat') as $obat) { 
                    
                    ?>
                    <tr data-row="<?= $rowsObat ?>">
                      <td>
                        <?= $this->form->select('obat['.$rowsObat.'][id_obat]', selectObat('--Pilih--'), $obat['id_obat'], 'class="form-control select2"') ?>
                      </td>
                      <td>
                        <input type="text" name="obat[<?= $rowsObat ?>][jml]" value="<?= $obat['jml'] ?>" class="form-control text-center input-sm"/>
                      </td>
                      <td>
                        <input type="text" name="obat[<?= $rowsObat ?>][aturan_minum]" value="<?= $obat['aturan_minum'] ?>" class="form-control text-center input-sm"/>
                      </td>
                      <td>
                        <button type="button" onclick="hapusObat(<?= $rowsObat ?>)" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button>
                      </td>
                    </tr>
                    <?php $rowsObat++ ?>
                  <?php 
                  
                  } ?>
                <?php } ?>         
              </tbody>
              <tfoot>
                <tr>
                  <td colspan="4" class="text-right">
                    <button type="button" class="btn btn-success btn-sm btn-flat" onclick="tambahObat()"><i class="fa fa-plus"></i> Tambah Obat</button>
                  </td>
                </tr>
              </tfoot>
            </table>
            <br>
            <div class="form-group text-center">
                <button type="submit" class="btn btn-primary btn-lg"><i class="fa fa-check"></i> Konfirmasi</button>   
                <a href="<?= base_url('apotik/'.$this->uri->segment(2).'/pelayanan') ?>" class="btn btn-danger btn-lg"><i class="fa fa-ban"></i> Batal</a>             
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="box box-primary">
          <div class="box-header">
              <h4 class="box-title">Data Pengguna Layanan</h4>
          </div>
          <div class="box-body">          
            <div class="form-group">
              <label class="col-md-4 control-label">No. Anggota</label>
              <div class="col-md-8">
                <?= $this->form->text('txtNoAnggota', $dataPasien['txtNoAnggota'], 'class="form-control" readonly') ?>
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-4 control-label">Nama Pengguna Layanan</label>
              <div class="col-md-8">
                <?= $this->form->text('txtNamaPasien', $dataPasien['txtNamaPasien'], 'class="form-control" readonly') ?>
              </div>
            </div> 
            <div class="form-group">
              <label class="col-md-4 control-label">No. KTP/NIK</label>
              <div class="col-md-8">
                <?= $this->form->text('txtNoKtp', $dataPasien['txtNoKtp'], 'class="form-control" readonly') ?>
              </div>
            </div> 
            <div class="form-group">
              <label class="col-md-4 control-label">Jenis Kelamin</label>
              <div class="col-md-8">
                <?= $this->form->select('charJkPasien', selectJenisKelamin(), $dataPasien['charJkPasien'], 'class="form-control" disabled') ?>
              </div>
            </div> 
          </div>                          
        </div>
        <div class="box box-primary">
          <div class="box-header">
            <h3 class="box-title">Data layanan</h3>
          </div>
          <div class="box-body">
            <div class="form-group">
              <label class="col-md-4 control-label">Dari</label>
              <div class="col-md-8">
                <?= $this->form->text('txtNama', $txtNama, 'class="form-control" readonly') ?>
              </div>
            </div>
            <!--<div class="form-group">
              <label class="col-md-4 control-label">Dokter</label>
              <div class="col-md-8">
                <?= $this->form->text('txtNamaPegawai', null, 'class="form-control" readonly') ?>
              </div>
            </div>-->           
          </div>
        </div>
      </div>    
    </div>    
  <?= $this->form->close() ?>
</section>
<script type="text/javascript">
var rowsObat = <?= isset($rowsObat) ? $rowsObat : 0 ?>;
function tambahObat() {  
  rowsObat++;
  html = '<tr data-row="'+rowsObat+'">';
    html +='<td>';
      html +='<select name="obat['+rowsObat+'][id_obat]" class="form-control select2">';
        <?php foreach(selectObat('--Pilih--') as $key => $row) { ?>
          html +='<option value="<?= $key ?>"><?= $row ?></option>';
        <?php } ?>
      html +='</select>';
    html +='</td>';    
    html +='<td><input type="text" name="obat['+rowsObat+'][jml]" value="1" class="form-control text-center input-sm"/></td>';      
    html +='<td><input type="text" name="obat['+rowsObat+'][aturan_minum]" value="" class="form-control text-center input-sm"/></td>';      
    html +='<td><button type="button" onclick="hapusObat('+rowsObat+')" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button></td>';
  html += '</tr>';
  $('#tblObat tbody').append(html);
  $('.select2').select2();
}
</script>