<section class="content-header">
    <h1 class="title"><?=$title?></h1>
    <?=$breadcrumbs?>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Tambah Obat</h3>
                </div>
                <div class="box-body">
                  <?php getview('layouts/partials/validation') ?>        
                  <?= $this->form->open(null, 'class="form-horizontal"') ?>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Tanggal</label>
                        <div class="col-md-10">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                                <?= $this->form->date('tgl', date('d-m-Y'), 'id="tgl" class="form-control"') ?>
                            </div>                    
                        </div>                 
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Kode Obat</label>
                        <div class="col-md-4">
                            <?= $this->form->text('kode_obat', null, 'class="form-control" readonly') ?>
                        </div> 
                        <label class="col-md-2 control-label">Nama Obat</label>
                        <div class="col-md-4">
                            <?= $this->form->text('nama_obat', null, 'class="form-control" readonly') ?>
                        </div> 
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Kemasan</label>
                        <div class="col-md-4">
                            <?= $this->form->select('id_kemasan', selectKemasan(), null, 'class="form-control" readonly') ?>
                        </div> 
                        <label class="col-md-2 control-label">Stok Awal</label>
                        <div class="col-md-4">
                            <?= $this->form->text('stok', null, 'id="stok" class="form-control text-right" readonly') ?>
                        </div> 
                    </div>    
                    <div class="form-group">
                        <label class="col-md-offset-6 col-md-2 control-label">Jumlah Penambahan</label>
                        <div class="col-md-4">
                            <?= $this->form->text('jml', null, 'id="jml" class="form-control text-right"') ?>
                        </div> 
                    </div>  
                    <div class="form-group">
                        <label class="col-md-offset-6 col-md-2 control-label">Stok Akhir</label>
                        <div class="col-md-4">
                            <?= $this->form->text('stok_akhir', formValue('stok'), 'id="stok_akhir" class="form-control text-right" readonly') ?>
                        </div> 
                    </div>            
                    <div class="form-group text-center">
                        <button type="submit" class="btn btn-primary btn-lg"><i class="fa fa-print"></i> Simpan</button>   
                        <a href="<?= base_url('gudang_apotik/obat') ?>" class="btn btn-danger btn-lg"><i class="fa fa-ban"></i> Batal</a>             
                    </div>
                  <?= $this->form->close() ?>
                </div>
            </div>
        </div>
    </div>
</section>