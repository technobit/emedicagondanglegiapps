<div class="form-group">
    <?= $this->form->text('txtCariObat', $this->input->get('key'), 'placeholder="Cari berdasarkan nama / kode" id="txtCariObat" class="form-control"') ?>
</div>
<table id="dataTable" class="table table-bordered table-condensed table-hover">
    <thead>
        <tr>
            <th>Nama Obat</th>
            <th>Kode Obat</th>
            <th>Kema</th>
            <th>Stok</th>
            <th width="1px"></th>
        </tr>
    </thead>
</table>

<script>
$(function() {
    var searchTimer;
    var dataTable;

    dataTable = $('#dataTable').dataTable({
        processing : true,
        serverSide : true,
        ajax : {
            url : '<?= base_url('gudang_apotik/pengambilan_obat/get_cari_obat?key='.$this->input->get('key')) ?>',
            type : 'post'
        },
        columns : [
            {data : 'nama_obat'},
            {data : 'kode_obat'},
            {data : 'kemasan'},
            {data : 'stok', class : 'text-right'},
            {data : 'id', orderable : false, render : function(data, type, row) {
                    return '<button type="button" class="btn btn-xs btn-primary" onclick="pilihObat(\''+row['kode_obat']+'\')"><i class="fa fa-check"></i></button>'
                }
            }
        ],
        autoWidth : false,
        filter : false
    });    

    $('#txtCariObat').keyup(function() {
        clearTimeout(searchTimer);
        searchTimer = setTimeout(function() {
            dataTable.api().ajax.url('<?= base_url('apotik/pelayanan_resep/get_cari_obat?key=') ?>'+$('#txtCariObat').val()).load();
        }, 500)
    });

});

function pilihObat(kode_obat) {
    tambahObat(kode_obat);
    $('.bootbox').modal('hide');
}
</script>