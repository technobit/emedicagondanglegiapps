<div class="box box-primary">
        <div class="box-body">
            <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a data-toggle="tab" href="#tab_1">Penanganan Rawat Jalan</a></li>
              <li><a id="getRekamMedis" data-toggle="tab" href="#tab_2">Riwayat Rekam Medis</a></li>
            </ul>
            <div class="tab-content">
              <!-- /.tab-pane Form Rawat Jalan-->
              <div id="tab_1" class="tab-pane active">
                <div class="alert alert-danger alert-dismissable" id="alert_message" style="display: none;">
                  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                  <h4><i class="icon fa fa-ban" id="alert_message_icon"></i> Peringatan!</h4>
                  <div id="alert_message_content"></div>
                </div>
                <form class="form-horizontal clearfix" method="POST" action="?" id="frm-detail-rekmed">
                <?=$frm_rekmed_id_detail?>
                <?=$frm_rekmed_keterangan?>
                <?=$frm_rekmed_pemeriksaan?>
                <?=$frm_rekmed_diagnosa?>
                <?=$frm_rekmed_detail_diagnosa?>
                <div id="additional-rekmed"></div>
                <div class="col-sm-9 col-sm-offset-3">
                <div class="form-group text-right">
                    <button class="btn btn-success btn-flat" id="addRekmed" type="button"><i class="fa fa-plus"></i> Add Diagnosa</button>
                </div>    
                </div>
                <?=$frm_rekmed_tindakan?>
                <?=$frm_rekmed_pengobatan?>
                <div class="col-sm-9 col-sm-offset-3">
                  <?php if($frm_rekmed_mode=='insert') : ?>
                  <button class="btn btn-primary" id="btnSaveRekmed" type="button"><i class="fa fa-save"></i> Simpan</button>
                  <button class="btn btn-warning" id="btnCancelRekmed" type="button"><i class="fa fa-ban"></i> Batal</button>
                  <a href="<?=$link_form_id_pelayanan?>" id="btnBack" style="display: none;" class="btn btn-primary"><i class="fa fa-list"></i>Kembali</a>
                  <a href="#" id="btnEditUpdate" class="btn btn-warning" style="display: none;"><i class="fa fa-pencil"></i> Edit</a>
                  <?php else : ?>
                  <button class="btn btn-primary" id="btnSaveRekmed" style="display: none;" type="button"><i class="fa fa-save"></i> Simpan</button>
                  <button class="btn btn-warning" id="btnCancelRekmed" style="display: none;" type="button"><i class="fa fa-ban"></i> Batal</button>
                  <a href="<?=$link_form_id_pelayanan?>" id="btnBack" class="btn btn-primary"><i class="fa fa-list"></i> Kembali</a>
                  <a href="#" id="btnEditUpdate" class="btn btn-warning" style="display: none;"><i class="fa fa-pencil"></i> Edit</a>
                  <?php endif;?>
                </div>
                </form>
              </div>
              <!-- /.tab-pane Rekam Medis-->
              <div id="tab_2" class="tab-pane">
                <div class="clearfix">
                <table class="table table-hover" id="table-rekam-medis">
                  <thead>
                    <tr>
                      <th>Tanggal</th>
                      <th>Poli</th>
                      <th>Tindakan</th>
                      <th>Diagnosa</th>
                      <th>Kode ICD 10</th>
                      <th>Terapi / Pengobatan</th>
                      <th>Dokter</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                      
                  </tbody>
                  </table>
                </div>
              </div>
              <!-- /.tab-pane -->
              <div id="tab_3" class="tab-pane">
                
              </div>
             
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
        </div>
</div>