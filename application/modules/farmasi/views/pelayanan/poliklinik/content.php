<section class="content-header clearfix">
  <h1 class="title"><?=$title?></h1>
  <?=$breadcrumbs?>
</section>
<section class="content clearfix">
  <div class="row">
    <div class="col-lg-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <p>Jam Operasional Puskesmas</p>
              <h3 id="jam-sekarang"></h3>
            </div>
          </div>
      </div>
    <div class="col-lg-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <p>Jumlah Antrian</p>
              <h3 id="jumlah-pengunjung">0</h3>
            </div>
          </div>
      </div>
    <div class="col-xs-12">
    <!-- Hasil Pencarian Data Pasien -->
    <div class="box box-success">
    <!-- form start -->
        <form class="form-horizontal" id="frm-poli-pasien">
          <div class="box-body">
            <?=$frm_tanggal?>
            <?=$frm_id_poliumum?>
            <div class="col-sm-10 col-sm-offset-2" style="padding-left: 3px;">
                <button class="btn btn-primary btn-flat" id="registerButton" >Kirim</button>
            </div>
          </div>
        </form>
    </div>
      <div class="box box-primary">
        <!-- /.box-header -->
        <div class="box-body table-responsive">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a data-toggle="tab" href="#tab_1">Daftar Antrian</a></li>
              <li onclick="getDataSedangDiLayani()"><a data-toggle="tab" href="#tab_2">Sedang Di Layani</a></li>
              <li onclick="getDataSelesai()"><a data-toggle="tab" href="#tab_3">Telah Di Layani</a></li>
            </ul>
            <div class="tab-content">
              <!-- /.tab-pane Form Rawat Jalan-->
              <div id="tab_1" class="tab-pane active">
                <table class="table table-hover">
                <thead>
                  <tr>
                    <th>No Antrian</th>
                    <th>Nama Pengguna Layanan</th>
                    <th>No Registrasi</th>
                    <th>Usia</th>
                    <th>Status Pelayanan</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody id="result-register-pasien">
                  
                </tbody>
              </table>
              </div>
              <!-- /.tab-pane -->
              <div id="tab_2" class="tab-pane">
                <table class="table table-hover">
                <thead>
                  <tr>
                    <th>No Antrian</th>
                    <th>Nama Pengguna Layanan</th>
                    <th>No Registrasi</th>
                    <th>Usia</th>
                    <th>Status Pelayanan</th>
                  </tr>
                </thead>
                <tbody id="result-register-sedang">
                  
                </tbody>
              </table>
              </div>
              <div id="tab_3" class="tab-pane">
                <table class="table table-hover">
                <thead>
                  <tr>
                    <th>No Antrian</th>
                    <th>Nama Pengguna Layanan</th>
                    <th>No Registrasi</th>
                    <th>Usia</th>
                    <th>Status Pelayanan</th>
                  </tr>
                </thead>
                <tbody id="result-register-selesai">
                  
                </tbody>
              </table>
              </div>
            </div>
            <!-- /.tab-content -->
          </div>
          
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>