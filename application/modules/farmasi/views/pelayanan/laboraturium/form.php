
<section class="content-header">
  <h1 class="title"><?=$title?></h1>
  <?=$breadcrumbs?>
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
    <!-- Hasil Pencarian Data Pasien -->
    <div class="box box-success">
    <!-- form start -->
        <form class="form-horizontal" id="frm-detail-register">
          <div class="box-body">
            <?=$frm_id_rekam_medis?>
            <?=$frm_id_pasien?>
            <?=$frm_id_pelayanan?>
            <?=$frm_id_kunjungan?>
            <?=$frm_id_kunjungan_referral?>
            <?=$frm_tanggal?>
            <?=$frm_no_anggota_pasien?>
            <?=$frm_nama_pasien?>
            <?=$frm_usia_pasien?>
            <?=$frm_jaminan_kesehatan?>
            <?=$frm_no_jaminan_kesehatan?>
            <?=$frm_no_rekam_medis?>
            <?=$frm_txt_ket_kunjungan?>
          </div>
        </form>
    </div>
        
      <!-- /.box -->
    </div>
    <!-- /.col -->
    <div class="col-xs-4">
      <div class="box box-primary">
        <div class="box-header">
          <h3 class="box-title">Form Input Periksa</h3>
        </div>
        <div class="box-body">
        <form method="POST" id="frm-jenis-periksa">
        <div id="msg-lab-result"></div>
        <?=$intIdLaboratorium?>
        <?=$frm_jenis_pemeriksaan?>
        <div id="frm-input-periksa">
           
        </div>
        <button id="btnSimpanForm" class="btn btn-primary btn-flat" type="button"><i class="fa fa-save"></i> Simpan</button>
        <button class="btn btn-danger btn-flat" type="button"><i class="fa fa-ban"></i> Batal</button>
</form>
        </div>
      </div>
    </div>
    <div class="col-xs-8">
      <div class="box box-primary">
          <div class="box-header">
            <h3 class="box-title">Form Pemeriksaan</h3>
            <div class="box-tools">
                <button class="btn btn-success btn-flat" id="selesaiPemeriksaan"><i class="fa fa-check"></i> Selesai</button>
            </div>
          </div>
          <div class="box-body">
            <table class="table" id="tbl-detail-periksa">
                      <thead>
                        <tr>
                          <th>Tanggal Pemeriksaan</th>
                          <th>Jenis Pemeriksaan</th>
                          <th>Detail Pemeriksaan</th>
                          <th>Aksi</th>
                        </tr>
                      </thead>
                      <tbody id="result-pemeriksaan">
                        
                      </tbody>
                    </table>
          </div>
        </div>
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
     