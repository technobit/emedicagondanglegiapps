<?php 
    foreach ($arr_form as $key => $value) :
        # code...
?>
    <div class="form-group">
        <label class="control-label form-label" for="<?=$key?>"><?=$value['label']?></label>
        <?php 
        if($value['type']=='text'){
        ?>
        <div class="input-group">
        <?php 
        echo form_input("inputLab[".$key."][txtValue]" , isset($arr_result[$key]) ? $arr_result[$key] : ""  , 'id="'.$key.'" class="form-control"');
        if(!empty($value['satuan'])){
            echo '<span class="input-group-addon">'.$value['satuan'].'</span>';
        }
        ?>  
        </div>
        <?php 
        }else if($value['type']=='dropdown'){
            $list_dropdown = !empty($value['list_value']) ? $value['list_value'] : array(); 
            echo form_dropdown("inputLab[".$key."][txtValue]" , $list_dropdown , array() , 'id="'.$key.'" class="form-control"');
        }
        ?>
        <?php 
        if(!empty($value['parameter'])){
            echo '<span class="help-block">Standard = '.$value['parameter'].'</span>';
        }
        echo form_hidden("inputLab[".$key."][txtSatuan]" , $value['satuan']);
        echo form_hidden("inputLab[".$key."][txtLabel]" , $value['label']);
        echo form_hidden("inputLab[".$key."][txtParamater]" , $value['parameter']);
        ?> 
    </div>
<?php 
    endforeach;
?>