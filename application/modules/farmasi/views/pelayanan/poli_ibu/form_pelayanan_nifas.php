<div class="clearfix">
<div class="alert alert-danger alert-dismissable" id="alert_message_nifas" style="display: none;">
    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
    <h4><i class="icon fa fa-ban" id="alert_message_nifas_icon"></i> Peringatan!</h4>
    <div id="alert_message_nifas_content"></div>
</div>
<form method="POST" action="?" class="" id="frm-pelayanan-nifas">
    <div class="col-md-6">
        <?=$intIdNifasIbu?>
        <?=$intIdRekamMedisIbu?>
        <?=$txtKeluhanPasien?>
        <?=$txtTD?>
        <?=$txtNadi?>
        <?=$txtRR?>
        <?=$txtSuhu?>
        <?=$txtKontraksiRahim?>
        <?=$txtPendarahan?>
    </div>
    <div class="col-md-6">
        <?=$txtLochia?>
        <?=$txtBAB?>
        <?=$txtBAK?>
        <?=$txtASI?>
        <?=$txtTindakan?>
    </div>
    <div class="text-center col-md-12">
        <button class="btn btn-success btn-flat" type="button" id="simpanDataPelayananNifas"><i class="fa fa-save"></i> <?=$buttonLabel?></button>
    </div>
</form>
</div>
<script>
    var frmPelayananNifas = $('#frm-pelayanan-nifas');
    var urlPost = global_url+"rekam-medis-ibu/save-data-pelayanan-nifas-ibu/";
    $(function(){
        frmPelayananNifas.validate({
            ignore : "",
            rules : {
                txtKeluhanPasien : {
                    required : true,
                },
                txtTindakan : {
                    required : true,
                }
            }
        });
        
        $('#simpanDataPelayananNifas').click(function(){
            if (frmPelayananNifas.valid()) {
                //code
                saveDataPelayananNifas();
            }
            
        });
    });
    
function saveDataPelayananNifas() {
    //code
    $.ajax({
       url : urlPost,
       type : "POST",
       data : frmPelayananNifas.serialize(),
       dataType : "html",
       success: function msg(res){
            var data = jQuery.parseJSON(res);
            var status = data['status'];
            var message = data['message'];
            messageBox(status , message , 'alert_message_nifas');
            if (status==true) {
             //code
             getDataPelayananNifas();
             $('.bootbox').modal("hide");
            }
       }
    }); 

}
</script>