<section class="content-header">
  <h1 class="title"><?=$title?></h1>
  <?=$breadcrumbs?>
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
    <!-- Hasil Pencarian Data Pasien -->
    <div class="box box-success">
    <!-- form start -->
        <form class="form-horizontal" id="frm-detail-register">
          <div class="box-body">
            <?=$frm_id_rekam_medis?>
            <?=$frm_id_pasien?>
            <?=$frm_id_pelayanan?>
            <?=$frm_id_kunjungan?>
            <?=$frm_id_rekmed_ibu?>
            <?=$frm_tanggal?>
            <?=$frm_no_anggota_pasien?>
            <?=$frm_nama_pasien?>
            <?=$frm_usia_pasien?>
            <?=$frm_no_rekam_medis?>
          </div>
        </form>
    </div>  
        <div class="box box-primary">
        <!-- /.box-header -->
        <div class="box-header">  
          <div class="box-tools pull-right">
            <a href="<?=$link_form_detail?>" class="btn btn-success btn-flat">Kembali Ke Pemeriksaan</a>
          </div>
        </div>
        <div class="box-body">
            <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a data-toggle="tab" href="#tab_1">Riwayat Kehamilan</a></li>
              <li onclick="getFormRiwayatSekarang()"><a id="getKehamilanSekarang" data-toggle="tab" href="#tab_2">Riwayat Kehamilan Sekarang</a></li>
              <li onclick="getFormPemeriksaan()"><a id="getPemeriksaan" data-toggle="tab" href="#tab_3">Pemeriksaan</a></li>
              <li onclick="getFormRencanaPersalinan()"><a id="getRencanaPersalinan" data-toggle="tab" href="#tab_4">Rencana Persalinan</a></li>
              <li><a id="getPelayananANC" data-toggle="tab" href="#tab_5">Pelayanan ANC</a></li>
              <li onclick="getFormCatatanPersalinan()"><a id="getPersalinan" data-toggle="tab" href="#tab_6">Catatan Persalinan</a></li>
              <li><a id="getPelayananNifas" data-toggle="tab" href="#tab_7">Catatan Pelayanan Nifas</a></li>
              <?php
              if($jenis_pelayanan=="pelayanan") : 
              ?>
              <li onclick="getFormPelayanan()"><a id="getFormPelayanan" data-toggle="tab" href="#tab_8">Pelayanan</a></li>
              <li><a id="getPemeriksaanLanjut" data-toggle="tab" href="#tab_9">Pemeriksaan Lanjut</a></li>
              <li><a id="getPeriksaLab" data-toggle="tab" href="#tab_10">Laboratorium</a></li>
              <?php
              endif;
              ?>
            </ul>
            <div class="tab-content">
              <!-- /.tab-pane Form Riwayat Kehamilan-->
              <div id="tab_1" class="tab-pane active">
                <div class="pull-right">
                    <button class="btn btn-primary" id="btnAddRiwayatKehamilan"><i class="fa fa-plus"></i> Tambah Riwayat</button>
                    <button class="btn btn-success" id="btnRefreshRiwayatKehamilan"><i class="fa fa-refresh"></i> Perbarui Riwayat</button>
                </div>
                <table class="table" id="table-riwayat-kehamilan">
                    <thead>
                        <tr>
                            <th>Kehamilan Ke</th>
                            <th>Komplikasi</th>
                            <th>Persalinan</th>
                            <th>Tempat Persalinan</th>
                            <th>Komplikasi Persalinan</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                </table>
              </div>
              <!-- /.tab-pane Form Riwayat Kehamilan Sekarang-->
              <div id="tab_2" class="tab-pane">
                <div id="frm-riwayat-sekarang-cont">
                  
                </div>
              </div>
              <!-- /.tab-pane Pemeriksaan-->
              <div id="tab_3" class="tab-pane">
                 <div id="frm-pemeriksaan-cont">
                  
                </div>
              </div>
              <!-- /.tab-pane Persalinan-->
              <div id="tab_4" class="tab-pane">
                <div id="frm-rencana-persalinan-cont">
                  
                </div>
              </div>
              <!-- /.tab-pane ANC-->
              <div id="tab_5" class="tab-pane">
                <div class="pull-right">
                    <button class="btn btn-primary" id="btnAddPelayananANC"><i class="fa fa-plus"></i> Tambah Data Pelayanan ANC</button>
                    <button class="btn btn-success" id="btnRefreshDataANC"><i class="fa fa-refresh"></i> Perbarui Data</button>
                </div>
                <table class="table" id="table-pelayanan-anc">
                    <thead>
                        <tr>
                            <th>Tanggal</th>
                            <th>Keluhan</th>
                            <th>SKOR (KSPR)</th>
                            <th>Pemeriksa</th>                            
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                </table>
              </div>
              <!-- /.tab-pane Persalinan-->
              <div id="tab_6" class="tab-pane">
                <div id="frm-catatan-persalinan-cont">
                  
                </div>
              </div>
              <!-- /.tab-pane Pelayanan Nifas-->
              <div id="tab_7" class="tab-pane">
                <div class="pull-right">
                    <button class="btn btn-primary" id="btnAddPelayananNifas"><i class="fa fa-plus"></i> Tambah Data Pelayanan Nifas</button>
                    <button class="btn btn-success" id="btnRefreshDataNifas"><i class="fa fa-refresh"></i> Perbarui Data</button>
                </div>
                <table class="table" id="table-pelayanan-nifas">
                    <thead>
                        <tr>
                            <th>Tanggal</th>
                            <th>Keluhan</th>
                            <th>Pemeriksa</th>
                            <th>Terapi / Tindakan</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                </table>
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>