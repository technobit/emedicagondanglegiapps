<div class="alert alert-danger alert-dismissable" id="alert_message4" style="display: none;">
    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
    <h4><i class="icon fa fa-ban" id="alert_message4_icon"></i> Peringatan!</h4>
    <div id="alert_message4_content"></div>
</div>
<div class="col-md-12">
<h4 class="">Form Pemeriksaan</h4>    
</div>
<form method="POST" action="?" class="form-horizontal" id="frm-rencana-persalinan">
    <?=$intIdRekamMedisIbu?>
    <?=$txtTanggalUpdate?>
    <?=$txtFormMode?>
    <?=$txtGolDarahIbu?>
    <?=$txtPenolong?>
    <?=$txtTempat?>
    <?=$txtPendamping?>
    <?=$txtCalonDonor?>
    <?=$txtStickerP4K?>
    <?=$dtTanggalPasang?>
    <?=$txtKesimpulan?>
    <div class="text-center">
        <button class="btn btn-success btn-flat" type="button" id="simpanDataRencana"><i class="fa fa-save"></i> <?=$buttonLabel?></button>
    </div>
</form>
<script>
    var idForm = "frm-rencana-persalinan";
    var linkSimpan = global_url+"rekam-medis-ibu/save-data-rencana-persalinan-ibu/";
    $(function(){
        $('#'+idForm).validate({
            ignore : "",
            rules : {
                txtGolDarahIbu : {
                    required : true,
                },
                txtPenolong : {
                    required : true,
                },
                txtTempat : {
                    required : true,
                }
            }
        });
        
        $('#simpanDataRencana').click(function(){
            if ($('#'+idForm).valid()) {
                //code
                saveDataRencanaPersalinan();
            }
            
        });
        
        $('#dtTanggalPasang').datepicker({
            format : "yyyy-mm-dd",
        });
    });
    
    

function saveDataRencanaPersalinan() {
    //code
    $.ajax({
       url : linkSimpan,
       type : "POST",
       data : $('#'+idForm).serialize(),
       dataType : "html",
       success: function msg(res){
            var data = jQuery.parseJSON(res);
            var status = data['status'];
            var message = data['message'];
            messageBox(status , message , 'alert_message4');
       }
    }); 

}
</script>