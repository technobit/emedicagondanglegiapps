<div class="clearfix">
<div class="alert alert-danger alert-dismissable" id="alert_persalinan" style="display: none;">
    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
    <h4><i class="icon fa fa-ban" id="alert_persalinan_icon"></i> Peringatan!</h4>
    <div id="alert_persalinan_content"></div>
</div>
<form method="POST" action="?" class="" id="frm-catatan-persalinan">
    <div class="col-md-6">
        <?=$intIdRekamMedisIbu?>
        <?=$txtFormMode?>
        <?=$txtTempatPersalinan?>
        <?=$txtTanggalPersalinan?>
        <?=$txtPenolong?>
        <?=$txtNamaPenolong?>
        <?=$txtProsesPersalinan?>
        <?=$txtKeadaanIbu?>
    </div>
    <div class="col-md-6">
        <?=$txtKomplikasiPersalinan?>
        <?=$txtBeratBBL?>
        <?=$txtPBBBL?>
        <?=$txtLK?>
        <?=$txtIsHidup?>
    </div>
    <div class="text-center col-md-12">
        <button class="btn btn-success btn-flat" type="button" id="simpanDataCatatanPersalinan"><i class="fa fa-save"></i> <?=$buttonLabel?></button>
    </div>
</form>
</div>
<script>
    var FormInput = $('#frm-catatan-persalinan');
    $(function(){
        FormInput.validate({
            ignore : "",
            rules : {
                txtTanggalPersalinan : {
                    required : true,
                },
            }
        });
        $('#simpanDataCatatanPersalinan').click(function(){
            if (FormInput.valid()) {
                //code
                saveDataCatatanPersalinan();
            }
            
        });
        $('#txtTanggalPersalinan').datepicker({
            format : "yyyy-mm-dd",
        });
    });
    
function saveDataCatatanPersalinan() {
    //code
    $.ajax({
       url : global_url+"rekam-medis-ibu/save-catatan-persalinan-ibu/",
       type : "POST",
       data : FormInput.serialize(),
       dataType : "html",
       success: function msg(res){
            var data = jQuery.parseJSON(res);
            var status = data['status'];
            var message = data['message'];
            messageBox(status , message , 'alert_persalinan');
       }
    }); 

}
</script>