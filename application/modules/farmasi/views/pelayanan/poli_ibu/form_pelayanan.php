<div class="clearfix">
<div class="alert alert-danger alert-dismissable" id="alert_pelayanan" style="display: none;">
    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
    <h4><i class="icon fa fa-ban" id="alert_pelayanan_icon"></i> Peringatan!</h4>
    <div id="alert_pelayanan_content"></div>
</div>
<form method="POST" action="?" class="" id="frm-rawat-jalan-ibu">
    <div class="col-md-6">
        <?=$txtTindakan?>
        <?=$txtPemeriksaan?>
        <?=$txtPengobatan?>
        <?=$bitStatusKunjungan?>
    </div>
    <div class="text-center col-md-12">
        <button class="btn btn-success btn-flat" type="button" id="simpanDataPelayanan"><i class="fa fa-save"></i> <?=$buttonLabel?></button>
    </div>
</form>
</div>
<script>
    var frmPelayananIbu = $('#frm-rawat-jalan-ibu');
    var urlPost = global_url+"rekam-medis-ibu/save-data-rawat-jalan-ibu/";
    $(function(){
        frmPelayananIbu.validate({
            ignore : "",
            rules : {
                txtTindakan : {
                    required : true,
                },
                txtPengobatan : {
                    required : true,
                }
            }
        });
        
        $('#simpanDataPelayanan').click(function(){
            if (frmPelayananIbu.valid()) {
                //code
                saveDataPelayananRawatJalan();
            }
            
        });
    });
    
function saveDataPelayananRawatJalan() {
    //code
    $.ajax({
       url : urlPost,
       type : "POST",
       data : frmPelayananIbu.serialize(),
       dataType : "html",
       success: function msg(res){
            var data = jQuery.parseJSON(res);
            var status = data['status'];
            var message = data['message'];
            messageBox(status , message , 'alert_pelayanan');
       }
    }); 

}
</script>