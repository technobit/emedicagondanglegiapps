<div class="clearfix">
<div class="alert alert-danger alert-dismissable" id="alert_message5" style="display: none;">
    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
    <h4><i class="icon fa fa-ban" id="alert_message5_icon"></i> Peringatan!</h4>
    <div id="alert_message5_content"></div>
</div>
<form method="POST" action="?" class="" id="frm-pelayanan-anc">
    <div class="col-md-6">
        <?=$intIdRekmedDetailAnc?>
        <?=$intIdRekamMedisIbu?>
        <?=$txtKeluhanPasien?>
        <?=$bitIsBukuKia?>
        <?=$txtBeratBadan?>
        <?=$txtTD?>
        <?=$txtNadi?>
        <?=$txtRR?>
        <?=$txtAbdomen?>
        <?=$txtOedemTungkai?>
        <?=$txtTFU?>
    </div>
    <div class="col-md-6">
        <?=$txtLetakJanin?>
        <?=$txtDJJ?>
        <?=$txtGerakJanin?>
        <?=$txtUmurKehamilan?>
        <?=$txtPenyuluhan?>
        <?=$txtPenunjang?>
        <?=$txtSkor?>
        <?=$txtKesimpulan?>
        <?=$txtTerapi?>
        <?=$txtRujukan?>
    </div>
    <div class="text-center col-md-12">
        <button class="btn btn-success btn-flat" type="button" id="simpanDataPelayananANC"><i class="fa fa-save"></i> <?=$buttonLabel?></button>
    </div>
</form>
</div>
<script>
    $(function(){
        $('#frm-pelayanan-anc').validate({
            ignore : "",
            rules : {
                txtKeluhanPasien : {
                    required : true,
                },
                txtBeratBadan : {
                    required : true,
                }
            }
        });
        
        $('#simpanDataPelayananANC').click(function(){
            if ($('#frm-pelayanan-anc').valid()) {
                //code
                saveDataPelayananANC();
            }
            
        });
    });
    
function saveDataPelayananANC() {
    //code
    $.ajax({
       url : global_url+"rekam-medis-ibu/save-data-pelayanan-anc-ibu/",
       type : "POST",
       data : $('#frm-pelayanan-anc').serialize(),
       dataType : "html",
       success: function msg(res){
            var data = jQuery.parseJSON(res);
            var status = data['status'];
            var message = data['message'];
            messageBox(status , message , 'alert_message5');
            if (status==true) {
             //code
             getDataPelayananANC();
             $('.bootbox').modal("hide");
            }
       }
    }); 

}
</script>