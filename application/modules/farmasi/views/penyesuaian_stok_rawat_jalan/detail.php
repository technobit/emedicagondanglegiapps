<section class="content-header">
    <h1 class="title"><?=$title?></h1>
    <?=$breadcrumbs?>
</section>
<section class="content">
  <?= $this->form->open(null, 'class="form-horizontal"') ?>
    <div class="row">
      <div class="col-md-8">            
        <div class="box box-primary">
          <div class="box-header">
            <h3 class="box-title">Daftar Obat</h3>
          </div>
          <div class="box-body">
            <?php getview('layouts/partials/message') ?>   
            <?php getview('layouts/partials/validation') ?>      
            <table id="tblObat" class="table table-bordered table-condensed table-hover">
              <thead>
                <tr>
                  <th>Obat</th>
                  <th>Kemasan</th>
                  <th width="70px">Jumlah Awal</th>                
                  <th width="70px">Jumlah Penyesuaian</th>                
                </tr>
              </thead>
              <tbody>
                <?php if (formValue('obat')) { ?>    
                  <?php $rowsObat = 1; ?>          
                  <?php foreach(formValue('obat') as $obat) { ?>
                    <tr data-row="<?= $rowsObat ?>">
                      <td>                        
                        <?= $obat['kode_obat'] ?> - <?= $obat['nama_obat'] ?>
                      </td>
                      <td>                        
                        <?= $obat['kemasan'] ?>
                      </td>
                      <td class="text-center">                        
                        <?= $obat['jml'] ?>
                      </td>
                      <td class="text-center">                        
                        <?= $obat['jml_akhir'] ?>
                      </td>
                    </tr>
                    <?php $rowsObat++ ?>
                  <?php } ?>
                <?php } ?>
              </tbody>
            </table>
            <br>          
            <div class="form-group text-center">                
                <a href="<?= base_url('apotik/rawat_jalan/penyesuaian/stok') ?>" class="btn btn-default btn-lg"><i class="fa fa-reply"></i> Kembali</a>             
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="box box-primary">
          <div class="box-header">
              <h4 class="box-title">Data Penyesuaian</h4>
          </div>
          <div class="box-body">          
            <div class="form-group">
              <label class="col-md-4 control-label">Tanggal Penyesuaian</label>
              <div class="col-md-8">
                <div class="input-group">
                  <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                  <?= $this->form->date('tgl_penyesuaian', date('Y-m-d'), 'class="form-control" readonly') ?>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-4 control-label">No. PO</label>
              <div class="col-md-8">
                <?= $this->form->text('no_penyesuaian', null, 'placeholder="Auto Generate" class="form-control" readonly') ?>
              </div>
            </div> 
          </div>                          
        </div>      
      </div>    
    </div>    
  <?= $this->form->close() ?>
</section>