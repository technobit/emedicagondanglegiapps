<section class="content-header">
    <h1 class="title"><?=$title?></h1>
    <?=$breadcrumbs?>
</section>
<section class="content">
  <?= $this->form->open(null, 'id="form-penerimaan" class="form-horizontal" data-apotik="'.$this->uri->segment(2).'"') ?>
    <div class="row">
      <div class="col-md-8">            
        <div class="box box-primary">
          <div class="box-header">
            <h3 class="box-title">Daftar Obat</h3>
          </div>
          <div class="box-body">
            <?php getview('layouts/partials/message') ?>   
            <?php getview('layouts/partials/validation') ?>   
            <!--<div class="form-group">
              <div class="col-md-8">
                <div class="input-group">
                  <?= $this->form->text('txtObat', null, 'placeholder="Nama / Kode Obat" id="txtObat" class="form-control input-lg"') ?>   
                  <div class="input-group-btn">           
                    <button type="button" id="btnTambahObat" class="btn btn-success btn-lg btn-flat"><i class="fa fa-plus"></i></button>
                    <button type="button" id="btnCariObat" class="btn btn-primary btn-lg btn-flat"><i class="fa fa-search"></i> Cari Obat</button>
                  </div>
                </div>
              </div>
            </div>-->
            <table id="tblObat" class="table table-bordered table-condensed table-hover">
              <thead>
                <tr>
                  <th>Obat</th>                  
                  <th width="70px">Jumlah Awal</th>                
                  <th width="70px">Jumlah Penyesuaian</th>                
                  <th width="1px" class="text-center">Aksi</th>
                </tr>
              </thead>
              <tbody>
                <?php if (formValue('obat')) { ?>    
                  <?php $rowsObat = 1; ?>          
                  <?php foreach(formValue('obat') as $obat) { ?>
                    <tr data-row="<?= $rowsObat ?>">
                      <td>
                        <?= $this->form->select('obat['.$rowsObat.'][id_obat]', selectObatApotik(1,'--Pilih--'), $obat['id_obat'], 'class="form-control select2" onchange="getObat('.$rowsObat.')"') ?>
                      </td>
  
                      <td>
                        <input type="text" name="obat[<?= $rowsObat ?>][jml]" value="<?= $obat['jml'] ?>" class="form-control text-center input-sm" readonly/>
                        <input type="text" name="obat[<?= $rowsObat ?>][jml_akhir]" value="<?= $obat['jml_akhir'] ?>" class="form-control text-center input-sm"/>
                      </td>
                      <td>
                        <button type="button" onclick="hapusObat(<?= $rowsObat ?>)" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button>
                      </td>
                    </tr>
                    <?php $rowsObat++ ?>
                  <?php } ?>
                <?php } ?>
              </tbody>
              <tfoot>
                <tr>
                  <td colspan="4" class="text-right">
                    <button type="button" class="btn btn-success btn-sm btn-flat" onclick="tambahObat()"><i class="fa fa-plus"></i> Tambah Obat</button>                
                  </td>
                </tr>
              </tfoot>
            </table>
            <br>          
            <div class="form-group text-center">
                <button type="submit" class="btn btn-primary btn-lg"><i class="fa fa-print"></i> Simpan</button>   
                <a href="<?= base_url('gudang_apotik/penyesuaian/stok') ?>" class="btn btn-danger btn-lg"><i class="fa fa-ban"></i> Batal</a>             
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="box box-primary">
          <div class="box-header">
              <h4 class="box-title">Data Penyesuaian</h4>
          </div>
          <div class="box-body">          
            <div class="form-group">
              <label class="col-md-4 control-label">Tanggal</label>
              <div class="col-md-8">
                <div class="input-group">
                  <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                  <?= $this->form->date('tgl_penyesuaian', date('Y-m-d'), 'id="tgl_penyesuaian" class="form-control"') ?>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-4 control-label">No. PO</label>
              <div class="col-md-8">
                <?= $this->form->text('no_permintaan', null, 'placeholder="Auto Generate" class="form-control" readonly') ?>
              </div>
            </div>              
          </div>                          
        </div>      
      </div>    
    </div>    
  <?= $this->form->close() ?>
</section>
<script>
var rowsObat = <?= isset($rowsObat) ? $rowsObat : 0 ?>;
function tambahObat() {  
  rowsObat++;
  html = '<tr data-row="'+rowsObat+'">';
    html +='<td>';
      html +='<select name="obat['+rowsObat+'][id_obat]" class="form-control select2" onchange="getObat('+rowsObat+')">';
        <?php foreach(selectObat('--Pilih--') as $key => $row) { ?>
          html +='<option value="<?= $key ?>"><?= $row ?></option>';
        <?php } ?>
      html +='</select>';
    html +='</td>';    
    html +='<td><input type="text" name="obat['+rowsObat+'][jml]" value="" class="form-control text-center input-sm" readonly/></td>';      
    html +='<td><input type="text" name="obat['+rowsObat+'][jml_akhir]" value="" class="form-control text-center input-sm"/></td>';      
    html +='<td><button type="button" onclick="hapusObat('+rowsObat+')" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button></td>';
  html += '</tr>';
  $('#tblObat tbody').append(html);
  $('.select2').select2();
}
</script>