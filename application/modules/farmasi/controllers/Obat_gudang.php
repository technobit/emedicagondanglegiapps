<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Obat_gudang extends MY_Controller {

    var $meta_title = "Data Obat Gudang";
    var $meta_desc = "Data Obat Gudang";
    var $main_title = "Data Obat Gudang";
    var $dtBreadcrumbs = array();  
    var $menu_key = "gudang_apotik_stok";

    public function __construct() {
        parent::__construct();
        $this->dtBreadcrumbs = array(
            "Home" =>base_url(),
            "Gudang Apotik" => base_url('gudang_apotik'),
            "Stok Obat" => base_url('gudang_apotik/stok_bat'),
        );
        $this->load->model('obat_m');
    }
    public function index() {        
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "container" => $this->_index(),
            "menu_key" => $this->menu_key,
            "akses_key" => "is_view",	
            "custom_js" => array(
                ASSETS_JS_URL."gudang_apotik/obat/gudang.js"
            ),
            "custom_css" => array(),
        );  
        $this->_render("default",$dt);             
    }

    private function _index() {        
        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['title'] = $this->meta_title;
        return $this->load->view('obat_gudang/index', $dt, true);
    }

    public function get($id = null) {
        $this->load->library('datatables');
        $this->db->like('nama_obat', $this->input->get('nama'));
        if ($this->input->get('jenis_obat')) {
            $this->db->where('id_jenis_obat', $this->input->get('jenis_obat'));
        }
        $query = $this->db->select('obat.*, kemasan.kemasan, jenis_obat.jenis_obat, (select sum(stok) from obat_apotik where id_obat = obat.id) as stok_apotik')
        ->join('kemasan', 'kemasan.id = obat.id_kemasan')
        ->join('jenis_obat', 'jenis_obat.id = obat.id_jenis_obat', 'left')
        ->get_compiled_select('obat');
        $response = $this->datatables->collection($query)
        ->orderableColumns('nama_obat, kode_obat, kemasan, jenis_obat, stok, stok_optimal')
        ->searchableColumns('nama_obat, kode_obat, kemasan, jenis_obat, stok, stok_optimal')
        ->render();
        $this->output->set_content_type('application/json')->set_output(json_encode($response));
    }

}