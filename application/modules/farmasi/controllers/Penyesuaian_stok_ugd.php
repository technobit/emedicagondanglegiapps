<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Penyesuaian_stok_ugd extends MY_Controller {

    var $meta_title = "Penyesuaian Stok UGD";
    var $meta_desc = "Penyesuaian Stok UGD";
    var $main_title = "Penyesuaian Stok UGD";
    var $dtBreadcrumbs = array();  

    public function __construct() {
        parent::__construct();
         $this->dtBreadcrumbs = array(
            "Home" =>base_url(),
            "Apotik UGD" => base_url('apotik/' . $this->uri->segment(2)),
            "Penyesuaian Stok" => base_url('apotik/' . $this->uri->segment(2) . '/penyesuaian/stok'),
        );
        $this->load->model('penyesuaian_stok_m');
    }

    public function get() {
        print_r($this->penyesuaian_stok_m->getObat('1')->result_array());
    }

    public function gudang() {        
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "container" => $this->_gudang(),
            "menu_key" => "gudang_apotik_penerimaan",
            "akses_key" => "is_view",
            "custom_js" => array(
                ASSETS_JS_URL."penyesuaian_stok_ugd/index.js"
            ),
            "custom_css" => array(),
        );  
        $this->_render("default",$dt);      
    }

    public function _gudang() {
        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['title'] = $this->meta_title;        
        return $this->load->view('penyesuaian_stok_ugd/gudang', $dt, true);
    } 

    public function get_gudang() {
        $this->load->library('datatables');
        $query = 'select * from penyesuaian_stok where id_apotik = 2';
        $response = $this->datatables->collection($query)
        ->orderableColumns('tgl_penyesuaian, no_penyesuaian')
        ->searchableColumns('no_penyesuaian')
        ->editColumn('tgl_penyesuaian', function($row) {
            return indonesian_date($row->tgl_penyesuaian);
        })
        ->addColumn('action', function($row) {
            return '<a href="'.base_url('apotik/ugd/penyesuaian/stok/detail/' . $row->id).'" class="btn btn-info btn-sm"><i class="fa fa-info"></i> Detail</a> ';            
        })
        ->render();
        $this->output->set_content_type('application/json')->set_output(json_encode($response));
    }

    public function create($id) {      
        if ($post = $this->input->post()) {            
            $this->penyesuaian_stok_m->createPO($id, $post);
            $this->redirect->with('successMessage', 'Penyesuaian stok berhasil disimpan.')->to('penyesuaian_stok_ugd/gudang');
        }
        $this->menu_key = $id==1 ? "rawat_jalan_permintaan_obat" : "ugd_permintaan_obat";
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "menu_key" => $this->menu_key,
            "akses_key" => "is_insert",
            "container" => $this->_form(),
            "custom_js" => array(
                ASSETS_URL."plugins/select2/select2.js",
                ASSETS_URL."plugins/bootbox/bootbox.js",
                ASSETS_JS_URL."penyesuaian_stok_ugd/form.js"
            ),
            "custom_css" => array(
                ASSETS_URL."plugins/select2/select2.min.css"
            ),
        );  
        $this->_render("default",$dt);      
    }    

    public function _form() {
        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['title'] = $this->meta_title;
        return $this->load->view('penyesuaian_stok_ugd/form', $dt, true);
    }   

    public function detail($id) {      
        $result = $this->find($id);
        $result->obat = $this->penyesuaian_stok_m->getObat($result->id)->result_array();        
        $this->form->setData($result);
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "container" => $this->_detail(),
            "menu_key" => "gudang_apotik_permintaan",
            "akses_key" => "is_insert",
            "custom_js" => array(
                ASSETS_URL."plugins/select2/select2.js",
                ASSETS_URL."plugins/bootbox/bootbox.js"                
            ),
            "custom_css" => array(
                ASSETS_URL."plugins/select2/select2.min.css"
            ),
        );  
        $this->_render("default",$dt);     
    }    

    public function _detail() {
        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['title'] = $this->meta_title;
        return $this->load->view('penyesuaian_stok_ugd/detail', $dt, true);
    }

    public function delete($id_apotik, $id) {
        $this->find($id);
        $this->penyesuaian_stok_m->deletePO($id);
        $this->redirect->with('successMessage', 'Penyesuaian stok berhasil dihapus.')->back();
    }

    public function cari_obat($id_apotik) {
        $this->load->view('penyesuaian_stok_ugd/cari_obat');
    }

    public function get_cari_obat($id_apotik) {
        $this->load->library('datatables');
        if ($key = $this->input->get('key')) {
            $this->db->like('kode_obat', $key);
            $this->db->or_like('nama_obat', $key);
        }
        $query = $this->db->select('obat.*, obat_apotik.stok as stok_apotik, kemasan.kemasan')
        ->join('(select * from obat_apotik where id_apotik="'.$id_apotik.'") obat_apotik', 'obat_apotik.id_obat = obat.id', 'left')
        ->join('kemasan', 'kemasan.id = obat.id_kemasan')
        ->get_compiled_select('obat');
        $response = $this->datatables->collection($query)
        ->orderableColumns('nama_obat, kode_obat, kemasan, obat.stok, stok_apotik.stok')        
        ->render();
        $this->output->set_content_type('application/json')->set_output(json_encode($response));
    }

    public function tambah_obat($id_apotik) {
        $this->load->model('obat_m');       
        $this->load->model('apotik_model');       
        $obat = $this->input->post('obat');
        $result = $this->obat_m->findByKey($obat)->result();
        $result = $this->apotik_model->findByKey($obat)->result();
        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

    public function get_obat($id) {
        $result = $this->db->where('id_apotik', 2)
        ->where('id_obat', $id)             
        ->get('obat_apotik')
        ->row();     
        $result = !empty($result) ? $result['stok'] : ($result['stok']=0);    
        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

    private function find($id) {
        $result = $this->penyesuaian_stok_m->find($id);
        if (!$result) {
            //$this->redirect->with('errorMessage', 'Data pengambilan obat tidak ditemukan.')->back();
        } else {
            return $result;
        }
    }    

}