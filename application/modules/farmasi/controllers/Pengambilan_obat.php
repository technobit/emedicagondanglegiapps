<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Pengambilan_obat extends MY_Controller {

    var $meta_title = "Pengambilan Obat Polindes";
    var $meta_desc = "Pengambilan Obat Polindes";
    var $main_title = "Pengambilan Obat Polindes";
    var $dtBreadcrumbs = array();  

    public function __construct() {
        parent::__construct();
         $this->dtBreadcrumbs = array(
            "Home" =>base_url(),
            "Gudang Apotik" => base_url('gudang_apotik'),
            "Pengambilan Obat Polindes" => base_url('gudang_apotik/obat'),
        );
        $this->load->model('pengambilan_obat_m');
    }
    public function index() {        
        $dt = array(
            "container" => $this->_index(),
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "menu_key" => "gudang_apotik_pengambilan",
			"akses_key" => "is_view",
            "custom_js" => array(
                ASSETS_JS_URL."gudang_apotik/pengambilan_obat/index.js"
            ),
            "custom_css" => array(),
        );  
        $this->_render("default",$dt);      
    }

    public function _index() {
        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['title'] = $this->meta_title;
        return $this->load->view('pengambilan_obat/index', $dt, true);
    }

    public function get() {
        $this->load->library('datatables');
        $query = $this->db->select('pengambilan_obat.*, desa.kode_desa, desa.nama_desa')->join('desa', 'desa.id = pengambilan_obat.id_desa')->get_compiled_select('pengambilan_obat');
        $response = $this->datatables->collection($query)
        ->orderableColumns('tgl_pengambilan, no_pengambilan, nama_desa, diterima_oleh')
        ->searchableColumns('no_pengambilan, kode_desa, nama_desa, diterima_oleh')
        ->editColumn('tgl_pengambilan', function($row) {
            return indonesian_date($row->tgl_pengambilan);
        })
        ->addColumn('action', function($row) {
                return '<a href="'.base_url('gudang_apotik/pengambilan_obat/edit/' . $row->id).'" class="btn btn-warning btn-sm"><i class="fa fa-pencil"></i> Ubah</a> '.
                           '<a onclick="return confirm(\'Apakah anda yakin akan menghapus data ini?\')" href="'.base_url('gudang_apotik/pengambilan_obat/delete/' . $row->id).'" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> Hapus</a>';
        })
        ->render();
        $this->output->set_content_type('application/json')->set_output(json_encode($response));
    }

    public function create() {      
        if ($post = $this->input->post()) {
            $this->validation();
            $this->pengambilan_obat_m->createPO($post);
            $this->redirect->with('successMessage', 'Pengambilan obat berhasil disimpan.')->to('gudang_apotik/pengambilan_obat');
        }
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "container" => $this->_form(),
            "menu_key" => "gudang_apotik_pengambilan",
			"akses_key" => "is_insert",
            "custom_js" => array(
                ASSETS_URL."plugins/select2/select2.js",
                ASSETS_URL."plugins/bootbox/bootbox.js",
                ASSETS_JS_URL."gudang_apotik/pengambilan_obat/form.js"
            ),
            "custom_css" => array(
                ASSETS_URL."plugins/select2/select2.min.css"
            ),
        );  
        $this->_render("default",$dt);      
    }    

    public function edit($id) {      
        $result = $this->find($id);
        $result->obat = $this->pengambilan_obat_m->getObat($result->id)->result_array();
        if ($post = $this->input->post()) {
            $this->validation();
            $this->pengambilan_obat_m->updatePO($id, $post);
            $this->redirect->with('successMessage', 'Pengambilan obat berhasil diperbarui.')->to('gudang_apotik/pengambilan_obat');
        }
        $this->form->setData($result);
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "container" => $this->_form(),
            "menu_key" => "gudang_apotik_pengambilan",
			"akses_key" => "is_update",
            "custom_js" => array(
                ASSETS_URL."plugins/select2/select2.js",
                ASSETS_URL."plugins/bootbox/bootbox.js",
                ASSETS_JS_URL."gudang_apotik/pengambilan_obat/form.js"
            ),
            "custom_css" => array(
                ASSETS_URL."plugins/select2/select2.min.css"
            ),
        );  
        $this->_render("default",$dt);     
    }    

    public function _form() {
        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['title'] = $this->meta_title;
        return $this->load->view('pengambilan_obat/form', $dt, true);
    }

    public function delete($id) {
        $this->find($id);
        $this->pengambilan_obat_m->deletePO($id);
        $this->redirect->with('successMessage', 'Pengambilan obat berhasil dihapus.')->back();
    }

    public function cari_obat() {
        $this->load->view('pengambilan_obat/cari_obat');
    }

    public function get_cari_obat() {
        $this->load->library('datatables');
        if ($key = $this->input->get('key')) {
            $this->db->like('kode_obat', $key);
            $this->db->or_like('nama_obat', $key);
        }
        $query = $this->db->select('obat.*, kemasan.kemasan')->join('kemasan', 'kemasan.id = obat.id_kemasan')->get_compiled_select('obat');
        $response = $this->datatables->collection($query)
        ->orderableColumns('nama_obat, kode_obat, kemasan, stok')        
        ->render();
        $this->output->set_content_type('application/json')->set_output(json_encode($response));
    }

    public function tambah_obat() {
        $this->load->model('obat_m');       
        $obat = $this->input->post('obat');
        $result = $this->obat_m->findByKey($obat)->result();
        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

    private function find($id) {
        $result = $this->pengambilan_obat_m->find($id);
        if (!$result) {
            $this->redirect->with('errorMessage', 'Data pengambilan obat tidak ditemukan.')->back();
        } else {
            return $result;
        }
    }

    private function validation() {
        $this->load->library('form_validation');        
        $this->form_validation->set_rules('id_desa', 'Desa', 'required');        
        $this->form_validation->set_rules('diterima_oleh', 'Diterima Oleh', 'required');                     
        if (!$this->form_validation->run()) {
            $this->redirect->withInput()->withValidation()->back();
        }        
    }

}