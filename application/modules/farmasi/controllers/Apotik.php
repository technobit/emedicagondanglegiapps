<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Apotik extends MY_Controller {
    
    var $meta_title = "Apotik";
    var $meta_desc = "Apotik";
    var $main_title = "Apotik";
    var $base_url = "";
    var $base_url_pasien = "";
    var $upload_dir = "";
    var $upload_url = "";
    var $limit = "20";
    var $arr_level_user = "";
    var $menu = "K01";
	var $status_layanan = "";
	    
    public function __construct(){
        parent::__construct();
        $this->base_url = $this->base_url_site."apotik/rawat_jalan/";
		$this->base_url_pasien = $this->base_url_site."pasien/";
		$this->load->model("pasien_model");
		$this->load->model("register_model");
		$this->load->model("jaminan_kesehatan_model" ,"jaminan_kesehatan");
		$this->js_folder = ASSETS_JS_URL."apotik/";
		$this->status_layanan = $this->config->item("status_layanan_list");
    }
    
    public function index()
	{
        $menu = "A01";
		$dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
			"container" => $this->_data_apotik(),
			"menu_key" => "dashboard",
			"akses_key" => "is_view",
			"custom_js" => array(
				
			),
            "custom_css" => array(
				
			),
		);	
		$this->_render("default",$dt);	
	}
	
	public function pelayanan($jenisApotik)
	{
        $menu = "A02";
		$dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
			"container" => $this->_data_pelayanan($jenisApotik),
			"menu_key" => "dashboard",
			"akses_key" => "is_view",
			"custom_js" => array(
				$this->js_folder."pelayanan.js",
			),
            "custom_css" => array(
				
			),
		);	
		$this->_render("default",$dt);	
	}
    
    private function _data_apotik(){
         $dt = array();
		 $breadcrumbs = array(
						 "Home" =>$this->base_url_site,
						 "Apotik" => $this->base_url,
						 "Data Obat" => "#",
						 );
		 
		 $dt['breadcrumbs'] = $this->setBreadcrumbs($breadcrumbs);
		 $dt['frm_tanggal'] = $this->form_builder->inputText("Tanggal" , "txtTanggal" , date("Y-m-d") , "col-sm-2" , array("class"=> "form-control datepicker"));
		 $dt['title'] = "Data Obat Apotik";
		 $ret = $this->load->view("apotik/content" , $dt , true);
		 return $ret;
    }
	
	private function _data_pelayanan($jenisApotik){
         $dt = array();
		 $breadcrumbs = array(
						 "Home" =>$this->base_url_site,
						 "Apotik" => $this->base_url,
						 "Pelayanan Apotik" => "#",
						 );
		 
		 $dt['breadcrumbs'] = $this->setBreadcrumbs($breadcrumbs);
		 $dt['title'] = "Data Antrian Apotik";
		 $dt['frm_tanggal'] = $this->form_builder->inputText("Tanggal" , "txtTanggal" , date("Y-m-d") , "col-sm-2" , array("class"=> "form-control datepicker"));
		 $dt['frm_jenis_apotik'] = $this->form_builder->inputHidden("JenisApotik" , $jenisApotik);
		 $ret = $this->load->view("apotik/pelayanan" , $dt , true);
		 return $ret;
    }
	
	public function getDataApotik(){
		if(!$this->input->is_ajax_request()){
			echo "Ilegal Input";
			die;	
		}
		
		$limit = 20;
		$length =  $this->input->post("length");
		$start = $this->input->post("start");
		$status = $this->input->post("status");
		$jenisApotik = $this->input->post("jenisApotik");
		$tanggal = $this->input->post("tanggal");
		
		$data_result = $this->register_model->getDataAntrianApotik( $status , $jenisApotik,$tanggal , $start , $limit );
		$count_data = $this->register_model->getCountDataAntrianApotik( $status , $jenisApotik , $tanggal);
		
		if($count_data > 0) {
			$retVal['draw'] = $this->input->post('draw');
			$retVal['recordsTotal'] = $count_data;
			$retVal['recordsFiltered'] = $count_data;
			$retVal['data'] = array();
			foreach($data_result as $row){
				$status_layanan = $this->status_layanan[$row['bitIsApotik']];
				$btnEdit = "<a href='".$this->base_url."pelayanan/resep/".$row['txtIdKunjungan']."' class='btn btn-info btn-flat'><i class=\"fa fa-pencil\"></i> Detail</a>";
				$btnAksi = $btnEdit;
				$retVal['data'][] = array($row['intNoAntri'],
										  $row['txtNamaPasien'] ,
										  $row['txtNamaPelayanan'] ,
										  $row['txtUsiaPasienKunjungan'] ,
										  $status_layanan,
										  $btnAksi);
			}
		}else{
			$retVal['draw'] = $this->input->post('draw');
			$retVal['recordsTotal'] = 0;
			$retVal['recordsFiltered'] = 0;
			$retVal['data'] = array();
		}
		$this->setJsonOutput($retVal);
	}
    
}