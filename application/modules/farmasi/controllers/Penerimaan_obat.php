<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Penerimaan_obat extends MY_Controller {

    var $meta_title = "Penerimaan Obat";
    var $meta_desc = "Penerimaan Obat";
    var $main_title = "Penerimaan Obat";
    var $dtBreadcrumbs = array();  

    public function __construct() {
        parent::__construct();
         $this->dtBreadcrumbs = array(
            "Home" =>base_url(),
            "Gudang Apotik" => base_url('gudang_apotik'),
            "Penerimaan Obat" => base_url('gudang_apotik/penerimaan/obat'),
        );
        $this->load->model('penerimaan_obat_m');
    }

    public function gudang() {        
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "container" => $this->_gudang(),
            "menu_key" => "gudang_apotik_penerimaan",
            "akses_key" => "is_view",
            "custom_js" => array(
                ASSETS_JS_URL."gudang_apotik/penerimaan_obat/index.js"
            ),
            "custom_css" => array(),
        );  
        $this->_render("default",$dt);      
    }

    public function _gudang() {
        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['title'] = $this->meta_title;        
        return $this->load->view('penerimaan_obat/gudang', $dt, true);
    } 

    public function get_gudang() {
        $this->load->library('datatables');
        $query = $this->db->get_compiled_select('penerimaan_obat');
        $response = $this->datatables->collection($query)
        ->orderableColumns('tgl_penerimaan, no_penerimaan')
        ->searchableColumns('no_penerimaan')
        ->editColumn('tgl_penerimaan', function($row) {
            return indonesian_date($row->tgl_penerimaan);
        })
        ->addColumn('action', function($row) {
            return '<a href="'.base_url('gudang_apotik/penerimaan/obat/detail/' . $row->id).'" class="btn btn-info btn-sm"><i class="fa fa-info"></i> Detail</a> '. 
                       '<a onclick="return confirm(\'Apakah anda yakin akan menghapus data ini?\')" href="'.base_url('gudang_apotik/penerimaan/obat/delete/' . $row->id).'" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> Hapus</a>';
            
        })
        ->render();
        $this->output->set_content_type('application/json')->set_output(json_encode($response));
    }

    public function create($id) {      
        if ($post = $this->input->post()) {            
            $this->penerimaan_obat_m->createPO($id, $post);
            $this->redirect->with('successMessage', 'Penerimaan obat berhasil disimpan.')->to('gudang_apotik/penerimaan/obat/');
        }
        $this->menu_key = $id==1 ? "rawat_jalan_permintaan_obat" : "ugd_permintaan_obat";
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "menu_key" => $this->menu_key,
            "akses_key" => "is_insert",
            "container" => $this->_form(),
            "custom_js" => array(
                ASSETS_URL."plugins/select2/select2.js",
                ASSETS_URL."plugins/bootbox/bootbox.js",
                ASSETS_JS_URL."gudang_apotik/penerimaan_obat/form.js"
            ),
            "custom_css" => array(
                ASSETS_URL."plugins/select2/select2.min.css"
            ),
        );  
        $this->_render("default",$dt);      
    }    

    /*public function edit($id_apotik, $id) {      
        $result = $this->find($id);        
        if ($post = $this->input->post()) {            
            $this->penerimaan_obat_m->updatePO($id, $post);
            $this->redirect->with('successMessage', 'Pengambilan obat berhasil diperbarui.')->to('apotik/'.$this->uri->segment(2).'/permintaan_obat');
        }
        $this->menu_key = $id==1 ? "rawat_jalan_permintaan_obat" : "ugd_permintaan_obat";
        $result->obat = $this->penerimaan_obat_m->getObat($result->id)->result_array();
        $this->form->setData($result);
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "container" => $this->_form(),
            "menu_key" => $this->menu_key,
            "akses_key" => "is_update",
            "custom_js" => array(
                ASSETS_URL."plugins/select2/select2.js",
                ASSETS_URL."plugins/bootbox/bootbox.js",
                ASSETS_JS_URL."gudang_apotik/penerimaan_obat/form.js"
            ),
            "custom_css" => array(
                ASSETS_URL."plugins/select2/select2.min.css"
            ),
        );  
        $this->_render("default",$dt);     
    }*/   

    public function _form() {
        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['title'] = $this->meta_title;
        return $this->load->view('penerimaan_obat/form', $dt, true);
    }   

    public function detail($id) {      
        $result = $this->find($id);
        $result->obat = $this->penerimaan_obat_m->getObat($result->id)->result_array();        
        $this->form->setData($result);
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "container" => $this->_detail(),
            "menu_key" => "gudang_apotik_permintaan",
            "akses_key" => "is_insert",
            "custom_js" => array(
                ASSETS_URL."plugins/select2/select2.js",
                ASSETS_URL."plugins/bootbox/bootbox.js"                
            ),
            "custom_css" => array(
                ASSETS_URL."plugins/select2/select2.min.css"
            ),
        );  
        $this->_render("default",$dt);     
    }    

    public function _detail() {
        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['title'] = $this->meta_title;
        return $this->load->view('penerimaan_obat/detail', $dt, true);
    }

    public function delete($id_apotik, $id) {
        $this->find($id);
        $this->penerimaan_obat_m->deletePO($id);
        $this->redirect->with('successMessage', 'Penerimaan obat berhasil dihapus.')->back();
    }

    public function cari_obat($id_apotik) {
        $this->load->view('penerimaan_obat/cari_obat');
    }

    public function get_cari_obat($id_apotik) {
        $this->load->library('datatables');
        if ($key = $this->input->get('key')) {
            $this->db->like('kode_obat', $key);
            $this->db->or_like('nama_obat', $key);
        }
        $query = $this->db->select('obat.*, obat_apotik.stok as stok_apotik, kemasan.kemasan')
        ->join('(select * from obat_apotik where id_apotik="'.$id_apotik.'") obat_apotik', 'obat_apotik.id_obat = obat.id', 'left')
        ->join('kemasan', 'kemasan.id = obat.id_kemasan')
        ->get_compiled_select('obat');
        $response = $this->datatables->collection($query)
        ->orderableColumns('nama_obat, kode_obat, kemasan, obat.stok, stok_apotik.stok')        
        ->render();
        $this->output->set_content_type('application/json')->set_output(json_encode($response));
    }

    public function tambah_obat($id_apotik) {
        $this->load->model('obat_m');       
        $obat = $this->input->post('obat');
        $result = $this->obat_m->findByKey($obat)->result();
        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

    private function find($id) {
        $result = $this->penerimaan_obat_m->find($id);
        if (!$result) {
            //$this->redirect->with('errorMessage', 'Data pengambilan obat tidak ditemukan.')->back();
        } else {
            return $result;
        }
    }    

}