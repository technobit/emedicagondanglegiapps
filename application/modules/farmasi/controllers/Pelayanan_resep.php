<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Pelayanan_resep extends MY_Controller {

    var $meta_title;
    var $meta_desc;
    var $main_title;
    var $dtBreadcrumbs = array(); 
    var $menu_key = ""; 

    public function __construct() {
        parent::__construct();
        $this->meta_title = "Pelayanan Resep " . ucwords(str_replace("_", " ", $this->uri->segment(2)));
        $this->meta_desc = "Pelayanan Resep " . ucwords(str_replace("_", " ", $this->uri->segment(2)));
        $this->main_title = "Pelayanan Resep " . ucwords(str_replace("_", " ", $this->uri->segment(2)));
         $this->dtBreadcrumbs = array(
            "Home" =>base_url(),
            "Apotik " . ucwords(str_replace("_", " ", $this->uri->segment(2))) => base_url('apotik/' . $this->uri->segment(2)),
            "Pelayanan Resep" => base_url('apotik/'.$this->uri->segment(2).'/resep'),
        );
        $this->load->model('register_model');
        $this->load->model('rekam_medis_obat_m');
        $this->load->model('rekam_medis_model');
        $this->load->model('pasien_model');
    }

    public function index($id_apotik, $id) {
        $this->menu_key = $id_apotik==1 ? "rawat_jalan_pelayanan" : "ugd_pelayanan_obat";      

        if ($post = $this->input->post()) {            
            $this->rekam_medis_obat_m->createObat($id_apotik, $id, $post);
            $this->redirect->with('successMessage', 'Pelayanan resep berhasil disimpan.')->to('apotik/'.$this->uri->segment(2).'/pelayanan');
        }
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "container" => $this->_form($id_apotik, $id),
            "menu_key" => $this->menu_key,
			"akses_key" => "is_view",
            "custom_js" => array(
                ASSETS_URL."plugins/select2/select2.js",
                ASSETS_URL."plugins/bootbox/bootbox.js",
                ASSETS_JS_URL."apotik/pelayanan_resep/index.js"
            ),
            "custom_css" => array(
                ASSETS_URL."plugins/select2/select2.min.css"
            ),
        );
                  
        $detail_register = $this->register_model->getDetailRegister($id);
        $statusPelayananPoli = $detail_register['bitIsPoli'];
        
        if($statusPelayananPoli==1){
            $this->register_model->changeStatusApotik($id, 2);
        }
        $this->_render("default",$dt);      
    }    

    public function _form($id_apotik, $id) {
        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['title'] = $this->meta_title;
        $result = $this->register_model->getDetailRegister($id);
        $detail_pasien = $this->pasien_model->getDetail($result['txtIdPasien']);
        $detail_rekmed = $this->rekam_medis_model->getDetailByIdKunjungan($id);
        $dt['txtPengobatan'] = $detail_rekmed['txtPengobatan'];
        $dt['txtNama'] = $result['txtNama'];
        $result['obat'] = $this->rekam_medis_obat_m->getObat($id_apotik, $id)->result_array();
        $this->form->setData($result);      
        $dt['dataPasien'] = $detail_pasien;
        return $this->load->view('pelayanan_resep/form', $dt, true);
    }

    public function delete($id_apotik, $id) {
        $this->find($id);
        $this->rekam_medis_obat_m->deleteObat($id);
        $this->redirect->with('successMessage', 'Pengambilan obat berhasil dihapus.')->back();
    }

    public function cari_obat($id_apotik) {
        $this->load->view('pelayanan_resep/cari_obat');
    }

    public function get_cari_obat($id_apotik) {
        $this->load->library('datatables');
        if ($key = $this->input->get('key')) {
            $this->db->like('kode_obat', $key);
            $this->db->or_like('nama_obat', $key);
        }
        $query = $this->db->select('obat.*, kemasan.kemasan, obat_apotik.stok as stok_apotik')
        ->join('kemasan', 'kemasan.id = obat.id_kemasan')
        ->join('(select * from obat_apotik where id_apotik="'.$id_apotik.'") obat_apotik', 'obat_apotik.id_obat = obat.id', 'left')
        ->get_compiled_select('obat');
        $response = $this->datatables->collection($query)
        ->orderableColumns('nama_obat, kode_obat, kemasan, stok')        
        ->render();
        $this->output->set_content_type('application/json')->set_output(json_encode($response));
    }

    public function tambah_obat($id_apotik) {
        $this->load->model('obat_m');       
        $obat = $this->input->post('obat');
        $result = $this->obat_m->findByKey($obat)->result();
        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

}