<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laboratorium_model extends MY_Model {

	var $table = "laboratorium";
    var $table_detail = "laboratorium_detail";
	var $primary_key = "intIdLaboratorium";
    
    public function __construct(){
        parent::__construct();
        
    }

	public function getDataByIdKunjungan($idKunjungan , $offset=0 , $limit=10, $fieldPeriksa){

		$this->db->from($this->table);
		$this->db->join($this->table_detail , $this->table.".intIdLaboratorium = ".$this->table_detail.".intIdLaboratorium" , "inner");
		$this->db->where($this->table.".".$fieldPeriksa , $idKunjungan);
		$res = $this->db->get();
		return $res->result_array();

	}

	public function getDataRekap($startDate , $endDate , $jenisPemeriksaan) { 
		
		$this->db->select("
			register.dtTanggalKunjungan,
			pasien.txtNoAnggota,
			pasien.txtNamaPasien,
			pasien.charJkPasien,
			kecamatan.Nama as AlamatKecamatan,
			laboratorium.txtIdKunjungan,
			laboratorium.txtIdPasien,
			laboratorium.intIdJenisPemeriksaan,
			laboratorium_detail.*
		");
		$this->db->from($this->table);
		$this->db->join('register' , $this->table.".txtIdReferralKunjungan = register.txtIdKunjungan" , "inner");
		$this->db->join('pasien' , $this->table.".txtIdPasien = pasien.txtIdPasien" , "inner");
		$this->db->join('kecamatan' , "pasien.intIdKecamatan = kecamatan.IDKecamatan" , "inner");
		$this->db->join($this->table_detail , $this->table.".intIdLaboratorium = ".$this->table_detail.".intIdLaboratorium" , "inner");
		$this->db->where("register.dtTanggalKunjungan BETWEEN '".$startDate."' AND '".$endDate."'");
		if($jenisPemeriksaan!=0){
			$this->db->where($this->table.".intIdJenisPemeriksaan" , $jenisPemeriksaan);
		}
		
		$res = $this->db->get();
		return $res->result_array();
	}

	public function getCountDataByIdKunjungan($idKunjungan){
		$this->db->from($this->table);
		$this->db->join($this->table_detail , $this->table.".intIdLaboratorium = ".$this->table_detail.".intIdLaboratorium" , "inner");
		$this->db->where($this->table.".txtIdKunjungan" , $idKunjungan);
		$res = $this->db->count_all_results();
		return $res;
	}

	public function getCountDataByIdPasien($idPasien){
		$this->db->from($this->table);
		$this->db->join($this->table_detail , $this->table.".intIdLaboratorium = ".$this->table_detail.".intIdLaboratorium" , "inner");
		$this->db->where($this->table.".txtIdPasien" , $idPasien);
		$res = $this->db->count_all_results();
		return $res;
	}

	public function getDetailPeriksaByIdLab($idLaboratorium){
		$this->db->from($this->table);
		$this->db->join($this->table_detail , $this->table.".intIdLaboratorium = ".$this->table_detail.".intIdLaboratorium" , "inner");
		$this->db->where($this->table.".".$this->primary_key , $idLaboratorium);
		$res = $this->db->get();
		return $res->result_array();
	}
    
	public function saveData($arrData = array(),$id = ""){
		$message = "";
		$status = false;
		
		if(!empty($id)){
			$this->db->where($this->primary_key , $id);
			$query = $this->db->update($this->table , $arrData);
			$message = $this->getMessage("insert" , $query);
		}else{
			$this->db->set($arrData);
			$query = $this->db->insert($this->table);
			$message = $this->getMessage("update" , $query);
			$id = $this->db->insert_id();
		}		
		$status = $query;
		$retVal['status'] = $status;
		$retVal['message'] = $message;
		$retVal['id'] = $id;
		return $retVal;
	}

	public function insertBatchDetail($arrInput , $id){
		$count = $this->checkDetail($id);
		if($count > 0){
			$this->delete_detail($id);
		}
		$query = $this->db->insert_batch($this->table_detail,$arrInput);
		$message = (!$query) ? "Data Gagal Di Masukkan" : "Data Berhasil Di Masukkan";
		$retVal = array();
		$retVal['status'] = $query;
		$retVal['message'] = $message;
		return $retVal;
	}



	public function checkDetail($id){
		$this->db->from($this->table_detail);
		$this->db->where($this->primary_key , $id);
		$count = $this->db->count_all_results();
		return $count;
	}

	public function delete($id){
		$this->db->where($this->primary_key , $id);
		$q = $this->db->delete($this->table);
		
		if(!$q){
			$retVal['message'] = "Data Gagal Di Hapus";
			$retVal['status'] = false;
		}else{
			$retVal['message'] = "Data Berhasil Di Hapus";
			$retVal['status'] = true;
		}
		
		return $retVal;
	}

	public function delete_detail($id){
		$this->db->where($this->primary_key , $id);
		$q = $this->db->delete($this->table_detail);
		
		if(!$q){
			$retVal['message'] = "Data Gagal Di Hapus";
			$retVal['status'] = false;
		}else{
			$retVal['message'] = "Data Berhasil Di Hapus";
			$retVal['status'] = true;
		}
		
		return $retVal;
	}
	
}

