<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pasien_Model extends CI_Model {

	var $table = "pasien";
	var $primary_key = "txtIdPasien";
    
    public function __construct(){
        parent::__construct();
        
    }
	
	public function getListDataPasien($search,$offset = 0,$limit = 10){
		if(!empty($search)){
			$this->db->like("txtNoAnggota" , $search);
			$this->db->or_like("txtNamaPasien" , $search);
		}
		
        $this->db->select("txtIdPasien as id, txtNamaPasien, txtNoAnggota");
		$this->db->from($this->table);
		$this->db->where("bitStatusPasien" , 1);
		$this->db->limit($limit);
		$this->db->offset($offset);
        $data = $this->db->get();
        return $data->result_array();
	}
	
	public function getCountListDataPasien($search){
		if(!empty($search)){
			$this->db->like("txtNoAnggota" , $search);
			$this->db->or_like("txtNamaPasien" , $search);
		}
		
        $this->db->select("txtIdPasien as id, txtNamaPasien, txtNoAnggota");
		$this->db->from($this->table);
		$this->db->where("bitStatusPasien" , 1);
        $data = $this->db->count_all_results();
        return $data;
	}
    
    public function getDataPasien($offset = 0 , $limit = 10, $search = ""){
		if(!empty($search)){
			$this->db->like("txtNoAnggota" , $search);
			$this->db->or_like("txtNoKtp" , $search);
			$this->db->or_like("txtNamaPasien" , $search);
		}
        $this->db->select($this->table.'.* ');
		$this->db->from($this->table);
		$this->db->limit($limit);
		$this->db->offset($offset);
        $data = $this->db->get();
        return $data->result_array();
    }

	public function getDataPasienNoAnggota($txtNoAnggota){

		if(is_numeric($txtNoAnggota)){
			$this->db->where("txtNoAnggota" , $txtNoAnggota);
		}else{
			$this->db->or_like("txtNamaPasien" , $txtNoAnggota);
		}
		$this->db->select($this->table.'.* ');
		$this->db->from($this->table);
		$data = $this->db->get();
        return $data->result_array();
	}
	
	public function countGetDataPasien($search=""){
		if(!empty($search)){
			$this->db->like("txtNoAnggota" , $search);
			$this->db->or_like("txtNoKtp" , $search);
			$this->db->or_like("txtNamaPasien" , $search);
		}
        $this->db->select($this->table.'.* ');
		$this->db->from($this->table);
        $data = $this->db->count_all_results();
        return $data;
	}
	
	public function getPasienById($id){
		$this->db->select($this->table.'.* ');
		$this->db->like("txtNoAnggota" , $id);
		$this->db->or_like("txtNoKtp" , $id);
		$this->db->or_like("txtNoKk" , $id);
		$this->db->or_like("txtNamaPasien" , $id);
		$this->db->from($this->table);
		$data = $this->db->get();
        return $data->result_array();
	}

    public function getCountDataIndex($search=""){
    	if(!empty($search)){
			$this->db->where($search);
		}
		$this->db->select($this->table.'.* ');	
		$this->db->from($this->table);
		$data = $this->db->count_all_results();	
        return $data;
    }
	
	
	public function getDetail($id){
		$this->db->where($this->primary_key , $id);
		$query = $this->db->get($this->table,1);
		$resVal = "";
		if($query->num_rows() > 0){
			$resVal = $query->row_array();	
		}else{
			$resVal = false;
		}
		return $resVal;
	}
	
	public function saveData($arrData = array(),$debug=false){
		
		$this->db->set($arrData);
		if($debug){
			$retVal = $this->db->get_compiled_insert($this->table);
		}else{
			$res = $this->db->insert($this->table);
			if(!$res){
				$retVal['error_stat'] = "Failed To Insert";
				$retVal['status'] = false;
			}else{
				$retVal['error_stat'] = "Success To Insert";
				$retVal['status'] = true;
				$retVal['id'] = $this->db->insert_id();
			}
			
		}
		return $retVal;
	}
	
	public function update($array , $id){
		
		$this->db->where($this->primary_key , $id);
		$query = $this->db->update($this->table, $array);
		if(!$query){
			
			$retVal['error_stat'] = "Failed To Insert";
			$retVal['status'] = false;
		}else{
			$retVal['error_stat'] = "Success To Update";
			$retVal['status'] = true;
			$retVal['id'] = $id;
		}
		
		return $retVal;
	}
	
	public function delete($id){
		$this->db->where($this->primary_key, $id);
		$q = $this->db->delete($this->table);
		
		if(!$q){
			$retVal['error_stat'] = "Failed To Delete";
			$retVal['status'] = false;
		}else{
			$retVal['error_stat'] = "Success To Delete";
			$retVal['status'] = true;
		}
		
		return $retVal;
	}

	public function cekNoAnggota($noAnggota){
		$this->db->where("txtNoAnggota", $noAnggota);
		$this->db->from($this->table);
		$ret = $this->db->count_all_results();
		return $ret;
	}

	public function detailPasienByRekamMedis($noRekamMedis){
		$this->db->from($this->table);
		$this->db->join("rekam_medis" , "rekam_medis.txtIdPasien = pasien.txtIdPasien" , "left");
		$this->db->join("jaminan_kesehatan" , "jaminan_kesehatan.intIdJaminanKesehatan = pasien.intIdJaminanKesehatan" , "left");
		$this->db->join("kelurahan" , "kelurahan.IDKelurahan = pasien.intIdKelurahan" , "left");
		$this->db->where("rekam_medis.txtIdRekamMedis" , $noRekamMedis);
		$ret = $this->db->get();
		return $ret->row_array();
	}
	
}

