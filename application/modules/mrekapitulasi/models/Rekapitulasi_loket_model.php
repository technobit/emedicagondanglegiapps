<?php 

class Rekapitulasi_loket_model extends MY_Model{

    protected $table = 'register_loket';
    protected $primary_key= 'intIdKunjunganLoket';

    function checkKunjungan($id_pelayanan = ""){
        $date_start = date("Y-m-d")." 00:00:00";	
        $date_end = date("Y-m-d H:i:s");	
        $this->db->select('intNoAntri');
        $this->db->from($this->table);
        if(!empty($id_pelayanan)){
            $this->db->where("intIdPelayanan" , $id_pelayanan);
        }
        $this->db->where($this->table.".dtTanggalKunjungan BETWEEN '".$date_start."' AND '".$date_end."'");
        
        $data = $this->db->count_all_results();
        return $data;
    }

    function dataLoket($txtDate , $intIdStatus , $idLoket){
        $date_start = $txtDate." 00:00:00";	
        $date_end = $txtDate." 23:59:00";	
        $this->db->select('*');
        $this->db->join('pelayanan' , $this->table.'.intIdPelayanan = pelayanan.intIdPelayanan');
        $this->db->from($this->table);
        $this->db->where($this->table.".dtTanggalKunjungan BETWEEN '".$date_start."' AND '".$date_end."'");
        $this->db->where($this->table.".intIdPelayanan" , $idLoket);
        if(!empty($intIdStatus)){
            $this->db->where('bitIsPoli' , $intIdStatus);
        }
        $this->db->order_by('bitIsPoli','ASC');
        $this->db->order_by('dtTanggalKunjungan','ASC');
        $data = $this->db->get();
        return $data->result_array();
    }

    function getDataRekapLoket($idLoket ,$intIdStatus , $date_start , $date_end , $start , $offset){
        $date_start .= " 00:00:00";	
        $date_end .= " 23:59:00";	
        $this->db->select('*');
        $this->db->join('pelayanan' , $this->table.'.intIdPelayanan = pelayanan.intIdPelayanan');
        $this->db->from($this->table);
        $this->db->where($this->table.".dtTanggalKunjungan BETWEEN '".$date_start."' AND '".$date_end."'");
        if(!empty($idLoket)){
            $this->db->where($this->table.".intIdPelayanan" , $idLoket);
        }
        if(!empty($intIdStatus)){
            $this->db->where('bitIsPoli' , $intIdStatus);
        }
        $this->db->order_by('dtTanggalKunjungan','DESC');
        $this->db->offset($offset);
        $this->db->limit($start);
        $data = $this->db->get();
        return $data->result_array();
    }

    function getDataRekapPelayananLoket($date_start , $date_end){
        $date_start .= " 00:00:00";	
        $date_end .= " 23:59:00";	
        $add_query = " AND register_loket.dtTanggalKunjungan BETWEEN '".$date_start."' AND '".$date_end."' ";
        $query = "SELECT pelayanan.`txtNama` , 
        (
            SELECT COUNT(register_loket.`intIdKunjunganLoket`) FROM register_loket 
            WHERE register_loket.`bitIsPoli` = '1' AND register_loket.`intIdPelayanan` = pelayanan.`intIdPelayanan`
            ".$add_query."
        ) AS antrian,
        (
            SELECT COUNT(register_loket.`intIdKunjunganLoket`) FROM register_loket 
            WHERE register_loket.`bitIsPoli` = '2' AND register_loket.`intIdPelayanan` = pelayanan.`intIdPelayanan`
            ".$add_query."
        ) AS sedang_dilayani,
        (
            SELECT COUNT(register_loket.`intIdKunjunganLoket`) FROM register_loket 
            WHERE register_loket.`bitIsPoli` = '3' AND register_loket.`intIdPelayanan` = pelayanan.`intIdPelayanan`
            ".$add_query."

        ) AS selesai_dilayani
        FROM pelayanan";
        
        $data = $this->db->query($query);
        return $data->result_array();
    }

    function rekapCountDataLoket($idLoket ,$intIdStatus , $date_start , $date_end){
        
        $date_start .= " 00:00:00";	
        $date_end .= " 23:59:00";	
        
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where($this->table.".dtTanggalKunjungan BETWEEN '".$date_start."' AND '".$date_end."'");
        if(!empty($idLoket)){
            $this->db->where($this->table.".intIdPelayanan" , $idLoket);
        }
        if(!empty($intIdStatus)){
            $this->db->where('bitIsPoli' , $intIdStatus);
        }
        $data = $this->db->count_all_results();
        return $data;
    }

    function insert($arrData){
        $resVal = array();
        $this->db->set($arrData);
        $res = $this->db->insert($this->table);
        $resVal['status'] = $res;
        $resVal['message'] = $res==true ? 'Registrasi Loket Berhasil' : 'Registrasi loket Gagal';
        $resVal['id'] = $this->db->insert_id();
        return $resVal;
    }

    function update($arrData , $id){
        $this->db->where($this->primary_key , $id);
        $ret = $this->db->update($this->table,$arrData);
        $resVal['status'] = $ret;
        $resVal['message'] = $ret==true ? 'Registrasi Loket Berhasil' : 'Registrasi loket Gagal';
        $resVal['id'] = $id;
        return $resVal;
    }

    function detail($id){
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->join('pelayanan' , $this->table.'.intIdPelayanan = pelayanan.intIdPelayanan');
        $this->db->where($this->primary_key , $id);
        $data = $this->db->get();
        return $data->row_array();
    }

    function delete($id){
        $this->db->where($this->primary_key , $id);
        $data = $this->db->delete($this->table);
        $retVal = array();
		if(!$data){
			$retVal['message'] = "Data Gagal Di Hapus";
			$retVal['status'] = false;
		}else{
			$retVal['message'] = "Data Berhasil Di Hapus";
			$retVal['status'] = true;
		}
		
		return $retVal;

    }
}