<?php 
class Rekapitulasi_model extends CI_Model{

    var $table = "register_bpjs";
    var $primary_key = "intIdRegisterBPJS";
    var $secondery_key = "txtIdKunjungan";
    function __construct(){
        parent::__construct();
    }

    function getDataRekapLoket($idPelayanan , $startDate , $endDate , $offset , $limit){
        $startDate .= " 00:00:00";
		$endDate .= " 23:59:00";
		$this->db->select("register.txtIdPasien,
						   register.txtIdKunjungan,
						   register.txtUsiaPasienKunjungan,
						   register.bitIsBaru,
						   register.intUsiaPasienHari,
						   register.bitStatusKunjungan,
						   register.bitIsPoli,
						   register.txtKeteranganKunjungan,
						   pasien.txtNamaPasien,
						   pasien.charJkPasien,
						   pasien.txtNoAnggota,
						   pasien.txtWilayahPasien,
						   pasien.intIdKelurahan,
						   pasien.txtTtdPasien,
						   kelurahan.Nama as NamaKelurahan,
						   jaminan_kesehatan.txtNamaJaminan as txtJaminanKesehatan,
						   pelayanan.txtNama as txtNamaPelayanan,
						   ");
		$this->db->from("register");
		$this->db->where_in("register.bitIsPoli" , array(1,2,3,4));
		$this->db->where_in("register.bitStatusKunjungan" , array(1 , 4));
		$this->db->join('pasien', 'pasien.txtIdPasien = register.txtIdPasien');
		$this->db->join('pelayanan', 'pelayanan.IntIdPelayanan = register.IntIdPelayanan');
		$this->db->join('jaminan_kesehatan', 'register.intIdJaminanKesehatan = jaminan_kesehatan.intIdJaminanKesehatan' , 'left');
		$this->db->join('kelurahan', 'kelurahan.IDKelurahan = pasien.intIdKelurahan', 'left');
		if(!empty($idPelayanan)){
			$this->db->where("register.intIdPelayanan" , $idPelayanan);
		}

		$this->db->where("register.dtTanggalKunjungan BETWEEN '".$startDate."' AND '".$endDate."'");
		if($limit!='-1'){
			$this->db->offset($offset);
			$this->db->limit($limit);
		}

		$this->db->group_by("register.txtIdKunjungan");
		$data = $this->db->get();
        return $data->result_array();
	
    }

    public function getCountDataRekapLoket($idPelayanan , $startDate , $endDate ){
        $startDate .= " 00:00:00";
		$endDate .= " 23:59:00";
		
		$this->db->from("register");
		$this->db->where_in("register.bitIsPoli" , array(1,2,3,4));
		$this->db->where_in("register.bitStatusKunjungan" , array(1 , 4));
		$this->db->join('pasien', 'pasien.txtIdPasien = register.txtIdPasien');
		$this->db->join('pelayanan', 'pelayanan.IntIdPelayanan = register.IntIdPelayanan');
		$this->db->join('jaminan_kesehatan', 'register.intIdJaminanKesehatan = jaminan_kesehatan.intIdJaminanKesehatan' , 'left');
		$this->db->join('kelurahan', 'kelurahan.IDKelurahan = pasien.intIdKelurahan', 'left');
		if(!empty($idPelayanan)){
			$this->db->where("register.intIdPelayanan" , $idPelayanan);
		}

		$this->db->where("register.dtTanggalKunjungan BETWEEN '".$startDate."' AND '".$endDate."'");
		$data = $this->db->count_all_results();
        return $data;
    }

	public function getJumlahRekapLoket($idPelayanan , $startDate , $endDate){
		$startDate .= " 00:00:00";
		$endDate .= " 23:59:00";
		$strQuery = "";
		if(!empty($idPelayanan)){
			$strQuery = " AND p.intIdPelayanan = '".$idPelayanan."'";
		}
		$query = "
		SELECT p.`txtNama`, 
		(SELECT COUNT(R1.`txtIdKunjungan`) FROM register R1 WHERE R1.dtTanggalKunjungan BETWEEN '".$startDate."' AND '".$endDate."'  
		AND R1.intIdPelayanan = p.`intIdPelayanan` AND R1.bitIsBaru = 1) AS jumlah_kunjungan_baru,
		(SELECT COUNT(R1.`txtIdKunjungan`) FROM register R1 WHERE R1.dtTanggalKunjungan BETWEEN '".$startDate."' AND '".$endDate."'  
		AND R1.intIdPelayanan = p.`intIdPelayanan` AND R1.bitIsBaru = 0) AS jumlah_kunjungan_lama,
		(SELECT COUNT(R1.`txtIdKunjungan`) FROM register R1 
		INNER JOIN pasien pa ON R1.txtIdPasien = pa.txtIdPasien
		WHERE R1.dtTanggalKunjungan BETWEEN '".$startDate."' AND '".$endDate."'
		AND R1.intUsiaPasienHari < 20075
		AND pa.charJkPasien = 'L' 
		AND R1.intIdPelayanan = p.`intIdPelayanan` 
		) AS jumlah_kunjungan_laki_muda,
		(SELECT COUNT(R1.`txtIdKunjungan`) FROM register R1 
		INNER JOIN pasien pa ON R1.txtIdPasien = pa.txtIdPasien
		WHERE R1.dtTanggalKunjungan BETWEEN '".$startDate."' AND '".$endDate."'
		AND R1.intUsiaPasienHari > 20075
		AND pa.charJkPasien = 'L' 
		AND R1.intIdPelayanan = p.`intIdPelayanan` 
		) AS jumlah_kunjungan_laki_tua,
		(SELECT COUNT(R1.`txtIdKunjungan`) FROM register R1 
		INNER JOIN pasien pa ON R1.txtIdPasien = pa.txtIdPasien
		WHERE R1.dtTanggalKunjungan BETWEEN '".$startDate."' AND '".$endDate."'
		AND R1.intUsiaPasienHari < 20075
		AND pa.charJkPasien = 'P' 
		AND R1.intIdPelayanan = p.`intIdPelayanan` 
		) AS jumlah_kunjungan_perempuan_muda,
		(SELECT COUNT(R1.`txtIdKunjungan`) FROM register R1 
		INNER JOIN pasien pa ON R1.txtIdPasien = pa.txtIdPasien
		WHERE R1.dtTanggalKunjungan BETWEEN '".$startDate."' AND '".$endDate."'
		AND R1.intUsiaPasienHari > 20075
		AND pa.charJkPasien = 'P' 
		AND R1.intIdPelayanan = p.`intIdPelayanan` 
		) AS jumlah_kunjungan_perempuan_tua
		FROM pelayanan p
		WHERE p.`intIdJenisPelayanan` != '8' AND p.`intIdJenisPelayanan` != '13'
		".$strQuery."
		";		

		$data = $this->db->query($query);
		return $data->result_array();
	}

	public function getPeringkatDiagnosaPenyakit( $startDate , $endDate , $intIdPelayanan){
		$stringQuery = "CALL get_rekap_data_penyakit_v2(".$intIdPelayanan.",'".$startDate."' , '".$endDate."')";
		$data = $this->db->query($stringQuery);
		///// To Execute FUckn Procedure You Have TO Execute This SHit
		$res = $data->next_result();
		$resVal =$data->result_array();
		return $resVal;
	}
	public function getDataJamkes($idPelayanan , $startDate , $endDate , $jamkes="", $offset , $limit){
        $startDate .= " 00:00:00";
		$endDate .= " 23:59:00";
		$this->db->select("*");
		$this->db->from("register");
		$this->db->where_in("register.bitIsPoli" , 3);
		$this->db->where("register.bitStatusKunjungan" , 1);
		$this->db->join('pasien', 'pasien.txtIdPasien = register.txtIdPasien');
		$this->db->join('pelayanan', 'pelayanan.IntIdPelayanan = register.IntIdPelayanan');
		$this->db->join('jaminan_kesehatan', 'register.intIdJaminanKesehatan = jaminan_kesehatan.intIdJaminanKesehatan' , 'left');
		$this->db->join('kelurahan', 'kelurahan.IDKelurahan = pasien.intIdKelurahan', 'left');
		$this->db->join('rekam_medis', 'rekam_medis.txtIdPasien = register.txtIdPasien','inner');
		$this->db->join('rekam_medis_detail', 'rekam_medis_detail.txtIdRekamMedis = rekam_medis.txtIdRekamMedis', 'inner');
		$this->db->where("jaminan_kesehatan.intIdJaminanKesehatan != 0");
		$this->db->where("jaminan_kesehatan.intIdJaminanKesehatan != 2");
		if(!empty($idPelayanan)){
			$this->db->where("register.intIdPelayanan" , $idPelayanan);
		}
		if(!empty($jamkes)){
			$this->db->where("jaminan_kesehatan.intIdJaminanKesehatan" , $jamkes);
		}
		$this->db->where("register.dtTanggalKunjungan BETWEEN '".$startDate."' AND '".$endDate."'");
		if($limit!='-1'){
			$this->db->offset($offset);
			$this->db->limit($limit);
		}

		$this->db->group_by("register.txtIdKunjungan");
		$data = $this->db->get();
		
        return $data->result_array();
	
    }
	public function CountDataJamkes($idPelayanan , $startDate , $endDate, $jamkes=""){
        $startDate .= " 00:00:00";
		$endDate .= " 23:59:00";
		$this->db->select("*");
		$this->db->from("register");
		$this->db->where_in("register.bitIsPoli" , 3);
		$this->db->where("register.bitStatusKunjungan" , 1);
		$this->db->join('pasien', 'pasien.txtIdPasien = register.txtIdPasien');
		$this->db->join('pelayanan', 'pelayanan.IntIdPelayanan = register.IntIdPelayanan');
		$this->db->join('jaminan_kesehatan', 'register.intIdJaminanKesehatan = jaminan_kesehatan.intIdJaminanKesehatan' , 'left');
		$this->db->join('kelurahan', 'kelurahan.IDKelurahan = pasien.intIdKelurahan', 'left');
		$this->db->join('rekam_medis', 'rekam_medis.txtIdPasien = register.txtIdPasien','inner');
		$this->db->join('rekam_medis_detail', 'rekam_medis_detail.txtIdRekamMedis = rekam_medis.txtIdRekamMedis', 'inner');
		$this->db->where("jaminan_kesehatan.intIdJaminanKesehatan != 0");
		$this->db->where("jaminan_kesehatan.intIdJaminanKesehatan != 2");
		if(!empty($jamkes)){
			$this->db->where("jaminan_kesehatan.intIdJaminanKesehatan" , $jamkes);
		}
		if(!empty($idPelayanan)){
			$this->db->where("register.intIdPelayanan" , $idPelayanan);
		}
		$this->db->where("register.dtTanggalKunjungan BETWEEN '".$startDate."' AND '".$endDate."'");
		
		$this->db->group_by("register.txtIdKunjungan");
		$data = $this->db->get();
		return $data->num_rows();
		
		
	
    }
	public function getTtd($data){
		$this->db->select("txtTandaTangan");
		$this->db->from("rekam_medis_detail");
		$this->db->where("txtIdRekamMedis",$data);
		$this->db->order_by("dtTanggalPemeriksaan desc");
		$data = $this->db->get();
		return $data->row_array();
	}
	public function getJamkes(){
		$this->db->select("intIdJaminanKesehatan,txtNamaJaminan");
		$this->db->from("jaminan_kesehatan");
		$this->db->where("intIdJaminanKesehatan != 0");
		$this->db->where("intIdJaminanKesehatan != 2");
		$data = $this->db->get();
		return $data->result_array();
	}
	public function getDatajam($start_date = "" ,$end_date = "" , $kamar=""){
	 	$date_start = $start_date." 00:00:00";
	    $date_end = $end_date." 23:59:00";		
		$this->db->select("jaminan_kesehatan.txtNamaJaminan as nama,jaminan_kesehatan.intIdJaminanKesehatan as id");
		$this->db->from("register");
		$this->db->join("pasien" , "register.txtIdPasien =  pasien.txtIdPasien" , "left");
		$this->db->join("rekam_medis_detail" , "rekam_medis_detail.txtIdKunjungan =  register.txtIdKunjungan" , "inner");
		$this->db->join("register_kamar" , "register.txtIdKunjungan =  register_kamar.txtIdKunjungan" , "inner");
		$this->db->join("kamar" , "register_kamar.intIdKamar =  kamar.intIdKamar" , "left");
		$this->db->join("jaminan_kesehatan" , "register.intIdJaminanKesehatan = jaminan_kesehatan.intIdJaminanKesehatan" , "left");
		$this->db->join("resume_medis" , "resume_medis.txtIdRekmedDetail = rekam_medis_detail.txtIdRekmedDetail" , "left");
		$this->db->where("register.bitStatusKunjungan" , 2);
		
		$this->db->where_in("register.bitIsPoli" ,  array(2,3));
		$this->db->where("register.dtTanggalKunjungan BETWEEN '".$date_start."' AND '".$date_end."'");
		if (!empty($kamar)) {
			$this->db->where("kamar.intIdKamar" , $kamar);
		}
		
		$this->db->group_by("id");
		$this->db->order_by("id" , "DESC");
		$data = $this->db->get();
        return $data->result_array();
		
	}
	public function getJumlah($start_date = "" ,$end_date = "" , $kamar="",$idJam=""){
	 	$date_start = $start_date." 00:00:00";
	    $date_end = $end_date." 23:59:00";		
		$this->db->select("jaminan_kesehatan.intIdJaminanKesehatan as id");
		$this->db->from("register");
		$this->db->join("pasien" , "register.txtIdPasien =  pasien.txtIdPasien" , "left");
		$this->db->join("rekam_medis_detail" , "rekam_medis_detail.txtIdKunjungan =  register.txtIdKunjungan" , "inner");
		$this->db->join("register_kamar" , "register.txtIdKunjungan =  register_kamar.txtIdKunjungan" , "inner");
		$this->db->join("kamar" , "register_kamar.intIdKamar =  kamar.intIdKamar" , "left");
		$this->db->join("jaminan_kesehatan" , "register.intIdJaminanKesehatan = jaminan_kesehatan.intIdJaminanKesehatan" , "left");
		$this->db->join("resume_medis" , "resume_medis.txtIdRekmedDetail = rekam_medis_detail.txtIdRekmedDetail" , "left");
		$this->db->where("register.bitStatusKunjungan" , 2);
		
			$this->db->where("register.intIdJaminanKesehatan" , $idJam);
		
		$this->db->where_in("register.bitIsPoli" ,  array(2,3));
		$this->db->where("register.dtTanggalKunjungan BETWEEN '".$date_start."' AND '".$date_end."'");
		if (!empty($kamar)) {
			$this->db->where("kamar.intIdKamar" , $kamar);
		}
	
		$this->db->order_by("id" , "DESC");
		$data = $this->db->get();
        return $data->result_array();
		
	}
	
	public function getRekapDataRawatInap($start_date = "" ,$end_date = "" , $kamar=""){
	 	$date_start = $start_date." 00:00:00";
	    $date_end = $end_date." 23:59:00";		
		$this->db->select("*");
		$this->db->from("register");
		$this->db->join("pasien" , "register.txtIdPasien =  pasien.txtIdPasien" , "left");
		$this->db->join("rekam_medis_detail" , "rekam_medis_detail.txtIdKunjungan =  register.txtIdKunjungan" , "inner");
		$this->db->join("register_kamar" , "register.txtIdKunjungan =  register_kamar.txtIdKunjungan" , "inner");
		$this->db->join("kamar" , "register_kamar.intIdKamar =  kamar.intIdKamar" , "left");
		$this->db->join("jaminan_kesehatan" , "register.intIdJaminanKesehatan = jaminan_kesehatan.intIdJaminanKesehatan" , "left");
		$this->db->join("resume_medis" , "resume_medis.txtIdRekmedDetail = rekam_medis_detail.txtIdRekmedDetail" , "left");
		$this->db->where("register.bitStatusKunjungan" , 2);
		$this->db->where_in("register.bitIsPoli" ,  array(2,3));
		$this->db->where("register.dtTanggalKunjungan BETWEEN '".$date_start."' AND '".$date_end."'");
		if (!empty($kamar)) {
			$this->db->where("kamar.intIdKamar" , $kamar);
		}
		$this->db->order_by("register.dtTanggalKunjungan" , "DESC");
		$data = $this->db->get();
        return $data->result_array();
		
	}
	public function getRekapPengunjungPenyakitMenular($idPelayanan , $startDate , $endDate , $arrPenyakit = ""){
		$startDate = $startDate." 00:00:00";
		$endDate = $endDate." 23:59:00";
		$this->db->select("data_penyakit.*, register.*, pasien.*, rekam_medis_diagnosa.*");
		$this->db->from("register");
		$this->db->join('pasien', 'pasien.txtIdPasien = register.txtIdPasien');
		$this->db->join('rekam_medis_detail', 'rekam_medis_detail.txtIdKunjungan = register.txtIdKunjungan' , 'left');
		$this->db->join('rekam_medis_diagnosa', 'rekam_medis_diagnosa.txtIdRekmedDetail = rekam_medis_detail.txtIdRekmedDetail' , 'left');
		$this->db->join('data_penyakit', 'data_penyakit.intIdPenyakit = rekam_medis_diagnosa.intIdDiagnosaPenyakit' , 'left');
		
		$this->db->where("register.dtTanggalKunjungan BETWEEN '".$startDate."' AND '".$endDate."'");

		if($idPelayanan!=0){
			$this->db->where("register.intIdPelayanan" , $idPelayanan);
		}

		if(!empty($arrPenyakit)){
			$this->db->where_in("data_penyakit.intIdPenyakit" , $arrPenyakit);
		}

	
		$data = $this->db->get();
	   	return $data->result_array();	
	}
	public function getRekapPekerjaan($idPelayanan , $startDate , $endDate , $arrPekerjaan = ""){
		$startDate = $startDate." 00:00:00";
		$endDate = $endDate." 23:59:00";
		$this->db->select("*");
		$this->db->from("register");
		$this->db->join('pasien', 'pasien.txtIdPasien = register.txtIdPasien');
		$this->db->join('pekerjaan', 'pekerjaan.id_pekerjaan = pasien.intIdPekerjaan' , 'left');
		
		$this->db->where("register.dtTanggalKunjungan BETWEEN '".$startDate."' AND '".$endDate."'");
		$this->db->where("register.bitIsPoli", '3');
		if($idPelayanan!=0){
			$this->db->where("register.intIdPelayanan" , $idPelayanan);
		}

		if(!empty($arrPekerjaan)){
			$this->db->where("pekerjaan.jenis_pekerjaan" , $arrPekerjaan);
		}
		$this->db->group_by('pekerjaan.id_pekerjaan');
		$data = $this->db->get();
	   	return $data->result_array();	
	}
	public function CountRekapPekerjaan($idPelayanan , $startDate , $endDate , $arrPekerjaan = "",$id_pekerjaan){
		$startDate = $startDate." 00:00:00";
		$endDate = $endDate." 23:59:00";
		$this->db->select("*");
		$this->db->from("register");
		$this->db->join('pasien', 'pasien.txtIdPasien = register.txtIdPasien');
		$this->db->join('pekerjaan', 'pekerjaan.id_pekerjaan = pasien.intIdPekerjaan' , 'left');
		
		$this->db->where("register.dtTanggalKunjungan BETWEEN '".$startDate."' AND '".$endDate."'");
		$this->db->where("register.bitIsPoli", '3');
		if($idPelayanan!=0){
			$this->db->where("register.intIdPelayanan" , $idPelayanan);
		}

		if(!empty($arrPekerjaan)){
			$this->db->where("pekerjaan.jenis_pekerjaan" , $arrPekerjaan);
		}
		$this->db->where("pekerjaan.id_pekerjaan" , $id_pekerjaan);
		$data = $this->db->get();
	   	return $data->num_rows();	
	}
		public function getRekapDataPekerjaan($idPelayanan , $startDate , $endDate , $arrPekerjaan = ""){
		$startDate = $startDate." 00:00:00";
		$endDate = $endDate." 23:59:00";
		$this->db->select("*");
		$this->db->from("register");
		$this->db->join('pasien', 'pasien.txtIdPasien = register.txtIdPasien');
		$this->db->join('pekerjaan', 'pekerjaan.id_pekerjaan = pasien.intIdPekerjaan' , 'left');
		$this->db->join('rekam_medis_detail', 'rekam_medis_detail.txtIdKunjungan = register.txtIdKunjungan' , 'left');
		$this->db->join('rekam_medis_diagnosa', 'rekam_medis_diagnosa.txtIdRekmedDetail = rekam_medis_detail.txtIdRekmedDetail' , 'left');
		$this->db->join('data_penyakit', 'data_penyakit.intIdPenyakit = rekam_medis_diagnosa.intIdDiagnosaPenyakit' , 'left');
		
		$this->db->where("register.dtTanggalKunjungan BETWEEN '".$startDate."' AND '".$endDate."'");
		$this->db->where("register.bitIsPoli", '3');
		if($idPelayanan!=0){
			$this->db->where("register.intIdPelayanan" , $idPelayanan);
		}

		if(!empty($arrPekerjaan)){
			$this->db->where("pekerjaan.jenis_pekerjaan" , $arrPekerjaan);
		}
		$data = $this->db->get();
	   	return $data->result_array();	
	}
	public function getRekapDataTindakan($startDate , $endDate , $idPelayanan, $pernyataan = ""){
		$startDate = $startDate." 00:00:00";
		$endDate = $endDate." 23:59:00";
		$this->db->select("*");
		$this->db->from("register");
		$this->db->join('pasien', 'pasien.txtIdPasien = register.txtIdPasien');
		$this->db->join('rekam_medis_detail', 'rekam_medis_detail.txtIdKunjungan = register.txtIdKunjungan' , 'left');
		$this->db->join('pelayanan', 'pelayanan.IntIdPelayanan = register.IntIdPelayanan');
		$this->db->join('rekam_medis_tindakan', 'rekam_medis_tindakan.txtIdRekmedDetail = rekam_medis_detail.txtIdRekmedDetail');
		$this->db->join('tindakan', 'tindakan.intIdTindakan = rekam_medis_tindakan.intIdTindakan');
		$this->db->where("register.dtTanggalKunjungan BETWEEN '".$startDate."' AND '".$endDate."'");

		if($idPelayanan!=0){
			$this->db->where("register.intIdPelayanan" , $idPelayanan);
		}

		if($pernyataan == 0 ){
			$this->db->where("rekam_medis_detail.bitSetuju" , $pernyataan);
		}else{
			$this->db->where("rekam_medis_detail.bitSetuju" , $pernyataan);
		}

		
		$data = $this->db->get();
	   	return $data->result_array();	
	}
	public function getTindakanByDetail($id){
		$this->db->select("*");
			$this->db->from("rekam_medis_tindakan");
		$this->db->join('tindakan', 'tindakan.intIdTindakan = rekam_medis_tindakan.intIdTindakan');
		$this->db->where("rekam_medis_tindakan.txtIdRekmedDetail",$id);
		$data = $this->db->get();
	   	return $data->result_array();	
	}

}