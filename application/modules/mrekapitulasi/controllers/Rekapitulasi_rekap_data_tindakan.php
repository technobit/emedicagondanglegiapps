 <?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Rekapitulasi_rekap_data_tindakan extends MY_Controller {

    var $meta_title = "Rekapitulasi";
    var $meta_desc = "Item Keuangan";
    var $main_title = "Item Keuangan";
    var $menu_key = "dashboard";
    var $dtBreadcrumbs = array();  

    public function __construct() {
        parent::__construct();
        $this->load->library("Excel");
        $this->load->model(
            array(
                'pelayanan_model',
                'rekapitulasi_model',
                'register_model',
                'rekam_medis_obat_m' => 'rekam_medis_obat'
            )
        );
    }

  public function index(){
        $this->dtBreadcrumbs = array(
            "Home" =>base_url(),
            "Rekapitulasi" => base_url(),
            "Data Tindakan" => "#",
        );

        $this->meta_title = "Rekapitulasi Data Rekap Tindakan";
        $dt = array(
            "title" => $this->meta_title,
            "description" => "Rekapitulasi Data Tindakan",
            "menu_key" => "rekap_data_tindakan",
			"akses_key" => "is_view",
            "container" => $this->_build_data_tindakan(),
            "custom_js" => array(
                ASSETS_JS_URL."rekapitulasi/data_rekap_tindakan.js"
            ),
            "custom_css" => array(),
        );  
        
        $this->_render("default",$dt);
    }
     private function _build_data_tindakan(){
        $listPelayanan = $this->pelayanan_model->getListDataPelayanan();
        $arrPelayanan = array("0"=>"Semua");
        foreach ($listPelayanan as $valuePelayanan) {
            # code...
            $arrPelayanan[$valuePelayanan['intIdPelayanan']] = $valuePelayanan['txtNama'];
        }

        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['frmPelayanan'] = $this->form_builder->inputDropdown("Ruangan" , "cmbPoli" , "" , $arrPelayanan);
        $dt['title'] = "Rekapitulasi Data Tindakan";
        $ret = $this->load->view("data_rekap_tindakan" , $dt , true);
        return $ret;
    }
       public function getRekapDataTindakan(){
        if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";die;
        }
        $arrResult = array();
        $pernyataan = $this->input->post("pernyataan");
        $idPelayanan =  $this->input->post("id_pelayanan");
		$start_date = $this->input->post("start_date");
        $end_date = $this->input->post("end_date");
        $dataRows = $this->rekapitulasi_model->getRekapDataTindakan($start_date , $end_date , $idPelayanan,$pernyataan);
        $retVal['data'] = array();
        foreach ($dataRows as $key => $value) {
			# code...
			$resPeriksa = '	<li>'.$value['txtTindakan'].'</li>';
			$arrResult[$value['txtIdRekmedDetail']]['dtTanggalKunjungan'] = $value['dtTanggalKunjungan'];
			$arrResult[$value['txtIdRekmedDetail']]['txtNoAnggota'] = $value['txtNoAnggota'];
			$arrResult[$value['txtIdRekmedDetail']]['txtNamaPasien'] = $value['txtNamaPasien'];
		    $arrResult[$value['txtIdRekmedDetail']]['txtUsiaPasienKunjungan'] = $value['txtUsiaPasienKunjungan'];
			$arrResult[$value['txtIdRekmedDetail']]['charJkPasien'] = $value['charJkPasien'];
			$arrResult[$value['txtIdRekmedDetail']]['txtNama'] = $value['txtNama'];
			$arrResult[$value['txtIdRekmedDetail']]['tindakan'][$value['txtTindakan']] = $resPeriksa;
		    $arrResult[$value['txtIdRekmedDetail']]['bitSetuju'] = $value['bitSetuju'];
			
        }
        
		foreach($arrResult as $row){
            $personAge = $row['txtUsiaPasienKunjungan'];
            $jenisKelamin = $row['charJkPasien']=="L" ? "Laki-Laki" : "Perempuan";
         
			$hasilPeriksa = $row['tindakan'];
			$htmlResult = "<ul>";
			foreach ($hasilPeriksa as $rowDetailPeriksa) {
				# code...
				$htmlResult .= $rowDetailPeriksa;
			}
			$htmlResult .= "</ul>";
			///echo $htmlResult;
			
				$arrData = array(
                                    indonesian_date($row['dtTanggalKunjungan']),
                                    $row['txtNoAnggota'],
                                    $row['txtNamaPasien'],
                                    $personAge,
                                    $jenisKelamin,
                                    $row['txtNama'],
                                    $htmlResult,
                                    $row['bitSetuju']==1?'Setuju':'Tidak Setuju',
                                );
			
			
			$retVal['data'][] = $arrData;
			 
		}
      
        $this->setJsonOutput($retVal);
    }
    public function downloadExcel($jenis_rekapitulasi , $idPelayanan , $start_date , $end_date , $start = 0 , $length = 10){
        $namaPoli = "Semua Ruangan";
        if($idPelayanan!=0){
            $dataPoli =  $this->pelayanan_model->getDetail($idPelayanan);
            $namaPoli = $dataPoli['txtNama'];
        }
       if($jenis_rekapitulasi=="data-rekap-tindakan"){
            $titleRecap = "Rekapitulasi Jumlah Registrasi Loket";
            $this->setHeaderExcel($titleRecap , $start_date , $end_date);
            $this->Download($idPelayanan,$start_date , $end_date);
            $filename = "Rekap_Jumlah_Loket";
        }
        $this->getOutput($filename);
    }

    private function Download($idPelayanan , $start_date , $end_date){
        $this->excel->getActiveSheet()->setCellValue('A6', "Tanggal");
        $this->excel->getActiveSheet()->setCellValue('B6', "No Anggota");
        $this->excel->getActiveSheet()->setCellValue('C6', "Nama");
        $this->excel->getActiveSheet()->setCellValue('D6', "Usia");
        $this->excel->getActiveSheet()->setCellValue('E6', "Jenis Kelamin");
        $this->excel->getActiveSheet()->setCellValue('F6', "Pelayanan");
        $this->excel->getActiveSheet()->setCellValue('G6', "Tindakan");
        $this->excel->getActiveSheet()->setCellValue('H6', "Persetujuan");
        
       /* $dataRows =  $this->rekapitulasi_model->getRekapDataTindakan($start_date , $end_date , $idPelayanan,$pernyataan);
      // echopre($dataRows);
      // die;
        $indexNo = 7;
         foreach($dataRows as $row){
             $indexNo++;
              $personAge = $row['txtUsiaPasienKunjungan'];
            $jenisKelamin = $row['charJkPasien']=="L" ? "Laki-Laki" : "Perempuan";
            $str = "";
            $no = 0;
            $tindakan = $this->rekapitulasi_model->getTindakanByDetail($row['txtIdRekmedDetail']);
            $this->excel->getActiveSheet()->setCellValue('A'.$indexNo,indonesian_date($row['dtTanggalKunjungan']));
            $this->excel->getActiveSheet()->setCellValue('B'.$indexNo,$row['txtNoAnggota']);
            $this->excel->getActiveSheet()->setCellValue('C'.$indexNo,$row['txtNamaPasien']);
            $this->excel->getActiveSheet()->setCellValue('D'.$indexNo,$personAge);
            $this->excel->getActiveSheet()->setCellValue('E'.$indexNo,$jenisKelamin);
            $this->excel->getActiveSheet()->setCellValue('F'.$indexNo,$row['txtNama']);
            $this->excel->getActiveSheet()->setCellValue('G'.$indexNo,$str);
            $this->excel->getActiveSheet()->setCellValue('H'.$indexNo,$row['bitSetuju']==1?'Setuju':'Tidak Setuju');
            
        }*/
    }


}