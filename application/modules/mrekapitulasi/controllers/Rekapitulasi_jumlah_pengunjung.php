<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Rekapitulasi_jumlah_pengunjung extends MY_Controller {

    var $meta_title = "Rekapitulasi";
    var $meta_desc = "Item Keuangan";
    var $main_title = "Item Keuangan";
    var $menu_key = "dashboard";
    var $dtBreadcrumbs = array();  

    public function __construct() {
        parent::__construct();
        $this->load->library("Excel");
        $this->load->model(
            array(
                'pelayanan_model',
                'rekapitulasi_model',
                'register_model',
                'rekam_medis_obat_m' => 'rekam_medis_obat'
            )
        );
    }
      public function index(){
        $this->dtBreadcrumbs = array(
            "Home" =>base_url(),
            "Rekapitulasi" => base_url(),
            "Data Jumlah Pengunjung" => "#",
        );
        
        $this->meta_title = "Rekapitulasi Jumlah Kunjungan";
        $dt = array(
            "title" => "Rekapitulasi Jumlah Kunjungan",
            "description" => "Rekapitulasi Jumlah Kunjungan",
            "menu_key" => "rekap_jumlah_pengunjung",
			"akses_key" => "is_view",
            "container" => $this->_build_jumlah_pengunjung(),
            "custom_js" => array(
                ASSETS_JS_URL."rekapitulasi/rekap_jumlah_pengunjung.js"
            ),
            "custom_css" => array(),
        );  
        
        $this->_render("default",$dt);
    }
    private function _build_jumlah_pengunjung(){

        $listPelayanan = $this->pelayanan_model->getListDataPelayanan();
        $arrPelayanan = array("0"=>"Semua");
        foreach ($listPelayanan as $valuePelayanan) {
            # code...
            $arrPelayanan[$valuePelayanan['intIdPelayanan']] = $valuePelayanan['txtNama'];
        }

        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['frmPelayanan'] = $this->form_builder->inputDropdown("Ruangan" , "cmbPoli" , "" , $arrPelayanan);
        $dt['title'] = $this->meta_title;
        $ret = $this->load->view("rekap_jumlah_pengunjung" , $dt , true);
        return $ret;
    }
    public function getDataRekapJumlahPengunjung(){
        if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";die;
        }

        $idPelayanan =  $this->input->post("cmbPoli");
		$start_date = $this->input->post("start_date");
        $end_date = $this->input->post("end_date");
        $status = false;
        $data = array();
        $dataJumlahKunjungan = $this->register_model->getJumlahKunjungan($start_date , $end_date, $idPelayanan);
        if(!empty($dataJumlahKunjungan)){
            $status = true;
            $data = $dataJumlahKunjungan;
        }
        $retVal = array();
        $retVal['status'] = $status;
        $retVal['data'] = $data;
        echo json_encode($retVal);
    }
     public function downloadExcel($jenis_rekapitulasi , $idPelayanan , $start_date , $end_date , $start = 0 , $length = 10){
        $namaPoli = "Semua Ruangan";
        if($idPelayanan!=0){
            $dataPoli =  $this->pelayanan_model->getDetail($idPelayanan);
            $namaPoli = $dataPoli['txtNama'];
        }
            $titleRecap = "Rekapitulasi Pengunjung Rawat Jalan ".$namaPoli;
            $this->setHeaderExcel($titleRecap , $start_date , $end_date);
            $this->DownloadJumlahPengunjung($idPelayanan , $start_date , $end_date);
            $filename = "Rekap_Jumlah_Pengunjung_Ruangan_".str_replace(" ","_",$namaPoli);
            
            $this->getOutput($filename);
        }
        private function DownloadJumlahPengunjung($idPelayanan , $start_date , $end_date){
        $no = 1;
         
        $this->excel->getActiveSheet()->setCellValue('A6', "Nama Poli");
        $this->excel->getActiveSheet()->setCellValue('B6', "Jumlah Kedatangan");
        $this->excel->getActiveSheet()->setCellValue('C6', "Status");
        $this->excel->getActiveSheet()->setCellValue('C7', "Antrian");
        $this->excel->getActiveSheet()->setCellValue('D7', "Sedang Di Layani");
        $this->excel->getActiveSheet()->setCellValue('E7', "Selesai Di Layani");
        $this->excel->getActiveSheet()->setCellValue('F7', "Rujukan");

        $dataJumlahKunjungan = $this->register_model->getJumlahKunjungan($start_date , $end_date, $idPelayanan);
        $indexNo = 7;
        foreach ($dataJumlahKunjungan as $rows) {
            # code...
            $this->excel->getActiveSheet()->setCellValue('A'.($indexNo + $no), $rows['NAMA_PELAYANAN']);
            $this->excel->getActiveSheet()->setCellValue('B'.($indexNo + $no), $rows['jumlah']);
            $this->excel->getActiveSheet()->setCellValue('C'.($indexNo + $no), $rows['jumlah_antri']);
            $this->excel->getActiveSheet()->setCellValue('D'.($indexNo + $no), $rows['jumlah_sedang_dilayani']);
            $this->excel->getActiveSheet()->setCellValue('E'.($indexNo + $no), $rows['jumlah_selesai']);
            $this->excel->getActiveSheet()->setCellValue('F'.($indexNo + $no), $rows['jumlah_rujukan']);
            $no++;
        }
    }


}
