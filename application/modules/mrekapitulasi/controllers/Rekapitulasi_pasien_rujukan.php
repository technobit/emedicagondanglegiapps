<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Rekapitulasi_pasien_rujukan extends MY_Controller {

    var $meta_title = "Rekapitulasi";
    var $meta_desc = "Item Keuangan";
    var $main_title = "Item Keuangan";
    var $menu_key = "dashboard";
    var $dtBreadcrumbs = array();  

    public function __construct() {
        parent::__construct();
        $this->load->library("Excel");
        $this->load->model(
            array(
                'pelayanan_model',
                'rekapitulasi_model',
                'register_model',
                'rekam_medis_obat_m' => 'rekam_medis_obat'
            )
        );
    }

    public function index() {   
        $this->dtBreadcrumbs = array(
            "Home" =>base_url(),
            "Rekapitulasi" => base_url(),
            "Data Rujukan" => "#",
        );

        $this->meta_title = "Rekapitulasi Data Pengguna Layanan Rujukan";
        $dt = array(
            "title" => $this->meta_title,
            "description" => "Rekapitulasi Data Pengguna Layanan Rujukan",
            "menu_key" => "rekap_pasien_rujukan",
			"akses_key" => "is_view",
            "container" => $this->_build_pasien_rujukan(),
            "custom_js" => array(
                ASSETS_JS_URL."rekapitulasi/rekap_pasien_rujukan.js"
            ),
            "custom_css" => array(),
        );  
        
        $this->_render("default",$dt);
    }

    private function _build_pasien_rujukan(){
        
        $arrPelayanan = array("0"=>"Semua");
        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['title'] = $this->meta_title;
        $ret = $this->load->view("rekap_pasien_rujukan" , $dt , true);
        return $ret;
    }
      public function getDataPasienRujukan(){
        if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";die;
        }

		$start_date = $this->input->post("start_date");
        $end_date = $this->input->post("end_date");
        $dataRujukan = $this->register_model->getDataPasienRujukan($start_date , $end_date);
        $retVal['data'] = array();
        foreach($dataRujukan as $rowRujukan){
        $tanggal = indonesian_date($rowRujukan['dtTanggalKunjungan']);
        $jKelamin = $rowRujukan['charJkPasien']=="L" ? "Laki-Laki" : "Perempuan";
        $retVal['data'][] = array(
                                $tanggal,
                                $rowRujukan['txtNamaPasien'],
                                $rowRujukan['txtUsiaPasienKunjungan'],
                                $jKelamin,
                                $rowRujukan['txtNamaJaminan'],
                                $rowRujukan['txtNoJaminanKesehatan'],
                                $rowRujukan['Kecamatan'],
                                $rowRujukan['NamaPelayanan'],
                                $rowRujukan['txtPoliLuar'],
                                $rowRujukan['txtnamaIndo'],
                            );
        }
        $this->setJsonOutput($retVal);
    }
      public function downloadExcel($jenis_rekapitulasi , $idPelayanan , $start_date , $end_date , $start = 0 , $length = 10){
        $namaPoli = "Semua Poli";
        if($idPelayanan!=0){
            $dataPoli =  $this->pelayanan_model->getDetail($idPelayanan);
            $namaPoli = $dataPoli['txtNama'];
        }
           $titleRecap = "Rekapitulasi Data Pengguna Layanan Rujukan";
            $this->setHeaderExcel($titleRecap , $start_date , $end_date);
            $this->DownloadRekapDataPasienRujukan($start_date , $end_date);
            $filename = "Rekap_Data_Pengguna_Layanan_Rujukan";
            
            $this->getOutput($filename);
        }
          private function DownloadRekapDataPasienRujukan( $start_date , $end_date){
        $this->excel->getActiveSheet()->setCellValue('A6', "Tanggal");
        $this->excel->getActiveSheet()->setCellValue('B6', "Nama");
        $this->excel->getActiveSheet()->setCellValue('C6', "Usia");
        $this->excel->getActiveSheet()->setCellValue('D6', "Jenis Kelamin");
        $this->excel->getActiveSheet()->setCellValue('E6', "Jaminan Kesehatan");
        $this->excel->getActiveSheet()->setCellValue('F6', "No Jamkes");
        $this->excel->getActiveSheet()->setCellValue('G6', "Kecamatan");
        $this->excel->getActiveSheet()->setCellValue('H6', "Instansi");
        $this->excel->getActiveSheet()->setCellValue('I6', "Ruangan");
        $this->excel->getActiveSheet()->setCellValue('J6', "Nama Penyakit");
        $dataResult = $this->register_model->getDataPasienRujukan($start_date , $end_date);
        $indexNo = 7;
        foreach ($dataResult as $rowRujukan) {
            # code...
            $tanggal = indonesian_date($rowRujukan['dtTanggalKunjungan']);
            $jKelamin = $rowRujukan['charJkPasien']=="L" ? "Laki-Laki" : "Perempuan";
            $this->excel->getActiveSheet()->setCellValue('A'.($indexNo), $tanggal);
            $this->excel->getActiveSheet()->setCellValue('B'.($indexNo), $rowRujukan['txtNamaPasien']);
            $this->excel->getActiveSheet()->setCellValue('C'.($indexNo), $rowRujukan['txtUsiaPasienKunjungan']);
            $this->excel->getActiveSheet()->setCellValue('D'.($indexNo), $jKelamin);
            $this->excel->getActiveSheet()->setCellValue('E'.($indexNo), $rowRujukan['txtNamaJaminan']);
            $this->excel->getActiveSheet()->setCellValue('F'.($indexNo), $rowRujukan['txtNoJaminanKesehatan']);
            $this->excel->getActiveSheet()->setCellValue('G'.($indexNo), $rowRujukan['Kecamatan']);
            $this->excel->getActiveSheet()->setCellValue('H'.($indexNo), $rowRujukan['NamaPelayanan']);
            $this->excel->getActiveSheet()->setCellValue('I'.($indexNo), $rowRujukan['txtPoliLuar']);
            $this->excel->getActiveSheet()->setCellValue('J'.($indexNo), $rowRujukan['txtnamaIndo']);
            $indexNo++;
        }
    }

}