<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Rekapitulasi_loket_data extends MY_Controller {

    var $meta_title = "Rekapitulasi";
    var $meta_desc = "Item Keuangan";
    var $main_title = "Item Keuangan";
    var $menu_key = "dashboard";
    var $dtBreadcrumbs = array();  

    public function __construct() {
        parent::__construct();
        $this->load->library("Excel");
        $this->load->model(
            array(
                'pelayanan_model',
                'rekapitulasi_loket_model',
                'register_model',
                'rekam_medis_obat_m' => 'rekam_medis_obat'
            )
        );
    }
    
    public function index(){
        $this->dtBreadcrumbs = array(
            "Home" =>base_url(),
            "Rekapitulasi" => base_url(),
            "Rekap Loket" => "#",
        );

        $this->meta_title = "Data Rekap Loket";
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "menu_key" => "rekap_data_antrian",
			"akses_key" => "is_view",
            "container" => $this->_build_rekap_loket(),
            "custom_js" => array(
                ASSETS_JS_URL."rekapitulasi/rekap_loket_data.js"
            ),
            "custom_css" => array(),
        );  
        
        $this->_render("default",$dt);             
    }
      private function _build_rekap_loket(){
        $listPelayanan = $this->pelayanan_model->getListDataPelayanan();
        $arrPelayanan = array("0"=>"Semua");
        foreach ($listPelayanan as $valuePelayanan) {
            # code...
            $arrPelayanan[$valuePelayanan['intIdPelayanan']] = $valuePelayanan['txtNama'];
        }

        $arrStatusAntrian = array(
			"0" => "Semua",
			"1" => "Antri",
			"2" => "Sedang Di Layani",
			"3" => "Sudah Di Layani",
		);

        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['frmPelayanan'] = $this->form_builder->inputDropdown("Jenis Loket" , "cmbPoli" , "" , $arrPelayanan);
        $dt['frmStatusAntrian'] = $this->form_builder->inputDropdown("Status Antrian" , "cmbStatusAntrian" , "" , $arrStatusAntrian);
        $dt['title'] = $this->meta_title;
        $ret = $this->load->view("rekap_loket_data" , $dt , true);
        return $ret;
    }
       public function getDataRekapLoket(){
        if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";die;
        }

        $idPelayanan =  $this->input->post("id_pelayanan");
        $idStatus =  $this->input->post("id_status");
		$start_date = $this->input->post("start_date");
        $end_date = $this->input->post("end_date");
        $length =  $this->input->post("length");
        $start = $this->input->post("start");
        $status_layanan_list = $this->config->item("status_layanan_list");

        $dataPengunjung = $this->rekapitulasi_loket_model->getDataRekapLoket($idPelayanan , $idStatus, $start_date , $end_date , $start , $length);
      
        $countDataPengunjung = $this->rekapitulasi_loket_model->rekapCountDataLoket($idPelayanan ,$idStatus, $start_date , $end_date );
        
        $retVal['draw'] = $this->input->post('draw');
        $retVal['recordsTotal'] = 0;
        $retVal['recordsFiltered'] = 0;
        $retVal['data'] = array();
        
        
        
        if(!empty($countDataPengunjung)){
            $retVal['draw'] = $this->input->post('draw');
			$retVal['recordsTotal'] = $countDataPengunjung;
			$retVal['recordsFiltered'] = $countDataPengunjung;
			$retVal['data'] = array();
			foreach($dataPengunjung as $row){
                
				$retVal['data'][] = array(
                    $row['intNoAntri'],
                    indonesian_date($row['dtTanggalKunjungan'] , 'j F Y H:i'),
                    !empty($row['dtSelesaiKunjungan']) ? indonesian_date($row['dtSelesaiKunjungan'] , 'j F Y H:i') : "",
                    $row['txtNama'],
                    $row['txtKeterangan'],
                    $status_layanan_list[$row['bitIsPoli']],
				);
			}
        }

        $this->setJsonOutput($retVal);
    }
     public function downloadExcel($jenis_rekapitulasi , $idPelayanan , $idStatus , $start_date , $end_date , $start = 0 , $length = 10){
        $namaPoli = "Semua Ruangan";
        if($idPelayanan!=0){
            $dataPoli =  $this->pelayanan_model->getDetail($idPelayanan);
            $namaPoli = $dataPoli['txtNama'];
        }
            $titleRecap = "Rekapitulasi Data Loket";
            $this->setHeaderExcel($titleRecap , $start_date , $end_date);
            $this->DownloadRekapLoket($idPelayanan,$start_date , $end_date , $start , $length);
            $filename = "Rekap_Data_Loket";
            
            $this->getOutput($filename);
        }
        private function DownloadRekapLoket($idPelayanan , $start_date , $end_date , $start , $length){

        $no = 1;
        $this->excel->getActiveSheet()->setCellValue('A6', "No");
        $this->excel->getActiveSheet()->setCellValue('B6', "Nomor Antrian");
        $this->excel->getActiveSheet()->setCellValue('C6', "Waktu Kunjungan");
        $this->excel->getActiveSheet()->setCellValue('D6', "Waktu Selesai Kunjungan");
        $this->excel->getActiveSheet()->setCellValue('E6', "Jenis Loket");
        $this->excel->getActiveSheet()->setCellValue('F6', "Keterangan");
        $this->excel->getActiveSheet()->setCellValue('G6', "Status Kunjungan");
        $dataPengunjung = $this->rekapitulasi_loket_model->getDataRekapLoket($idPelayanan , $idStatus, $start_date , $end_date , $start , $length);
        $status_layanan_list = $this->config->item("status_layanan_list");
        $indexNo = 7;
        foreach($dataPengunjung as $row){
            

            $this->excel->getActiveSheet()->setCellValue('A'.($indexNo + $no), $no);
            $this->excel->getActiveSheet()->setCellValue('B'.($indexNo + $no), indonesian_date($row['dtTanggalKunjungan'] , 'j F Y H:i'));
            $this->excel->getActiveSheet()->setCellValue('C'.($indexNo + $no), $row['txtNamaPasien']);
            $this->excel->getActiveSheet()->setCellValue('D'.($indexNo + $no), !empty($row['dtSelesaiKunjungan']) ? indonesian_date($row['dtSelesaiKunjungan'] , 'j F Y H:i') : "");
            $this->excel->getActiveSheet()->setCellValue('E'.($indexNo + $no), $row['txtNama']);
            $this->excel->getActiveSheet()->setCellValue('F'.($indexNo + $no), $row['txtKeterangan']);
            $this->excel->getActiveSheet()->setCellValue('G'.($indexNo + $no), $status_layanan_list[$row['bitIsPoli']]);
            $no++;
        }
    }
  
  

}