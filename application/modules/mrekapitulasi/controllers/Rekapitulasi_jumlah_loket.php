<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Rekapitulasi_jumlah_loket extends MY_Controller {

    var $meta_title = "Rekapitulasi";
    var $meta_desc = "Item Keuangan";
    var $main_title = "Item Keuangan";
    var $menu_key = "dashboard";
    var $dtBreadcrumbs = array();  

    public function __construct() {
        parent::__construct();
        $this->load->library("Excel");
        $this->load->model(
            array(
                'pelayanan_model',
                'rekapitulasi_model',
                'register_model',
                'rekam_medis_obat_m' => 'rekam_medis_obat'
            )
        );
    }
       public function index(){
        $this->dtBreadcrumbs = array(
            "Home" =>base_url(),
            "Rekapitulasi" => base_url(),
            "Rekap Loket" => "#",
        );

        $this->meta_title = "Rekap Register Loket";
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "menu_k`ey" => "dashboard",
			"akses_key" => "is_view",
            "container" => $this->_build_rekap_jumlah_loket(),
            "custom_js" => array(
                ASSETS_JS_URL."rekapitulasi/rekap_jumlah_loket.js"
            ),
            "custom_css" => array(),
        );  
        
        $this->_render("default",$dt);
    }
     private function _build_rekap_jumlah_loket(){
        $listPelayanan = $this->pelayanan_model->getListDataPelayanan();
        $arrPelayanan = arrayDropdown($listPelayanan , "intIdPelayanan" , "txtNama" , array("0"=>"Semua"));
        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['frmPelayanan'] = $this->form_builder->inputDropdown("Ruangan" , "cmbPoli" , "" , $arrPelayanan);
        $dt['title'] = $this->meta_title;
        $ret = $this->load->view("rekap_jumlah_loket" , $dt , true);
        return $ret;

    }
      public function getDataJumlahLoket(){
        $this->isAjaxRequest();
        $idPelayanan =  $this->input->post("id_pelayanan");
		$start_date = $this->input->post("start_date");
        $end_date = $this->input->post("end_date");

        $dataJumlahLoket = $this->rekapitulasi_model->getJumlahRekapLoket($idPelayanan , $start_date , $end_date);
        $retVal['data'] = array();
        if(!empty($dataJumlahLoket)){   
			foreach($dataJumlahLoket as $row){
				$retVal['data'][] = array($row['txtNama'],
                                          $row['jumlah_kunjungan_baru'],
                                          $row['jumlah_kunjungan_lama'],
                                          $row['jumlah_kunjungan_laki_muda'],
                                          $row['jumlah_kunjungan_laki_tua'],
                                          $row['jumlah_kunjungan_perempuan_muda'],
                                          $row['jumlah_kunjungan_perempuan_tua']
									);
			}
        }

        $this->setJsonOutput($retVal);
    }
     public function downloadExcel($jenis_rekapitulasi , $idPelayanan , $start_date , $end_date , $start = 0 , $length = 10){
        $namaPoli = "Semua Ruangan";
        if($idPelayanan!=0){
            $dataPoli =  $this->pelayanan_model->getDetail($idPelayanan);
            $namaPoli = $dataPoli['txtNama'];
        }
           $titleRecap = "Rekapitulasi Jumlah Registrasi Loket";
            $this->setHeaderExcel($titleRecap , $start_date , $end_date);
            $this->DownloadJumlahRekapLoket($idPelayanan,$start_date , $end_date);
            $filename = "Rekap_Jumlah_Loket";
            
            $this->getOutput($filename);
        }
    
 private function DownloadJumlahRekapLoket($idPelayanan , $start_date , $end_date){

        $no = 1;
        $this->excel->getActiveSheet()->setCellValue('A6', "No");
        $this->excel->getActiveSheet()->mergeCells('A6:A8');
        $this->excel->getActiveSheet()->setCellValue('B6', "Pelayanan");
        $this->excel->getActiveSheet()->mergeCells('B6:B8');
        $this->excel->getActiveSheet()->setCellValue('C6', "Jenis Kunjungan");
        $this->excel->getActiveSheet()->mergeCells('C6:D7');
        $this->excel->getActiveSheet()->setCellValue('C8', "Baru");
        $this->excel->getActiveSheet()->setCellValue('D8', "Lama");
        $this->excel->getActiveSheet()->setCellValue('E6', "Umur");
        $this->excel->getActiveSheet()->mergeCells('E6:H6');
        $this->excel->getActiveSheet()->setCellValue('E7', "Laki - Laki");
        $this->excel->getActiveSheet()->mergeCells('E7:F7');
        $this->excel->getActiveSheet()->setCellValue('G7', "Perempuan");
        $this->excel->getActiveSheet()->mergeCells('G7:H7');
        $this->excel->getActiveSheet()->setCellValue('E8', "0-50");
        $this->excel->getActiveSheet()->setCellValue('F8', "50 >");
        $this->excel->getActiveSheet()->setCellValue('G8', "0-50");
        $this->excel->getActiveSheet()->setCellValue('H8', "50 >");


        $dataJumlahLoket = $this->rekapitulasi_model->getJumlahRekapLoket($idPelayanan , $start_date , $end_date);
        $indexNo = 8;
        foreach($dataJumlahLoket as $row){
            $this->excel->getActiveSheet()->setCellValue('A'.($indexNo + $no), $no);
            $this->excel->getActiveSheet()->setCellValue('B'.($indexNo + $no), $row['txtNama']);
            $this->excel->getActiveSheet()->setCellValue('C'.($indexNo + $no), $row['jumlah_kunjungan_baru']);
            $this->excel->getActiveSheet()->setCellValue('D'.($indexNo + $no), $row['jumlah_kunjungan_lama']);
            $this->excel->getActiveSheet()->setCellValue('E'.($indexNo + $no), $row['jumlah_kunjungan_laki_muda']);
            $this->excel->getActiveSheet()->setCellValue('F'.($indexNo + $no), $row['jumlah_kunjungan_laki_tua']);
            $this->excel->getActiveSheet()->setCellValue('G'.($indexNo + $no), $row['jumlah_kunjungan_perempuan_muda']);
            $this->excel->getActiveSheet()->setCellValue('H'.($indexNo + $no), $row['jumlah_kunjungan_perempuan_tua']);
            $no++;
        }
    }    
 }