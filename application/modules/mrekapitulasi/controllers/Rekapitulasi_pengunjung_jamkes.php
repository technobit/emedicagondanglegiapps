<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Rekapitulasi_pengunjung_jamkes extends MY_Controller {

    var $meta_title = "Rekapitulasi";
    var $meta_desc = "Item Keuangan";
    var $main_title = "Item Keuangan";
    var $menu_key = "dashboard";
    var $dtBreadcrumbs = array();  

    public function __construct() {
        parent::__construct();
        $this->load->library("Excel");
        $this->load->model(
            array(
                'pelayanan_model',
                'rekapitulasi_model',
                'register_model',
                'rekam_medis_obat_m' => 'rekam_medis_obat'
            )
        );
    }

   public function index(){
        $this->dtBreadcrumbs = array(
            "Home" =>base_url(),
            "Rekapitulasi" => base_url(),
            "Data Penggunaan Jaminan Kesehatan" => "#",
        );

        $this->meta_title = "Rekapitulasi Jaminan Kesehatan";
        $dt = array(
            "title" => "Rekapitulasi Jaminan Kesehatan",
            "description" => "Rekapitulasi Jaminan Kesehatan",
            "menu_key" => "rekap_jaminan_kesehatan",
			"akses_key" => "is_view",
            "container" => $this->_build_rekap_pengunjung_jamkes(),
            "custom_js" => array(
                ASSETS_JS_URL."rekapitulasi/rekap_jaminan_kesehatan.js"
            ),
            "custom_css" => array(),
        );  
        
        $this->_render("default",$dt);
    }
 private function _build_rekap_pengunjung_jamkes(){

        $listPelayanan = $this->pelayanan_model->getListDataPelayanan();
        $arrPelayanan = array("0"=>"Semua");
        foreach ($listPelayanan as $valuePelayanan) {
            # code...
            $arrPelayanan[$valuePelayanan['intIdPelayanan']] = $valuePelayanan['txtNama'];
        }

        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['frmPelayanan'] = $this->form_builder->inputDropdown("Ruangan" , "cmbPoli" , "" , $arrPelayanan);
        $dt['title'] = $this->meta_title;
        $ret = $this->load->view("rekap_jaminan_kesehatan" , $dt , true);
        return $ret;
    }
  public function getDataRekapJaminanKesehatan(){
        if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";die;
        }

        $idPelayanan =  $this->input->post("cmbPoli");
		$start_date = $this->input->post("date1");
        $end_date = $this->input->post("date2");
        $status = false;
        $data = "";
        $dataResult = $this->register_model->getJumlahKunjunganByJaminanKesehatan($start_date , $end_date, $idPelayanan);
        if(!empty($dataResult)){
            $status = true;
            
            foreach ($dataResult as $key => $value) {
                # code...
                $data .= "<tr>";
                $data .= "<td>".$value['NamaJaminan']."</td>";
                $data .= "<td>".$value['jumlah']."</td>";
                $data .= "</tr>";
            }
        }else{
                $data .= "<tr>";
                $data .= "<td colspan='2' class='text-center'>Data Tidak Ada</td>";
                $data .= "</tr>";
        }
        $retVal = array();
        $retVal['status'] = $status;
        $retVal['data'] = $data;
        echo json_encode($retVal);
    }

  

    public function downloadExcel($jenis_rekapitulasi , $idPelayanan , $start_date , $end_date , $start = 0 , $length = 10){
        $namaPoli = "Semua Ruangan";
        if($idPelayanan!=0){
            $dataPoli =  $this->pelayanan_model->getDetail($idPelayanan);
            $namaPoli = $dataPoli['txtNama'];
        }
        if($jenis_rekapitulasi=='rekap-pengunjung'){
            $titleRecap = "Rekapitulasi Pengunjung Rawat Jalan ".$namaPoli;
            $this->setHeaderExcel($titleRecap , $start_date , $end_date);
            $this->DownloadRekapPengunjung($idPelayanan , $start_date , $end_date , $start , $length);
            $filename = "Rekap_Pengunjung_Poli_".str_replace(" ","_",$namaPoli);
        }else if($jenis_rekapitulasi=='jumlah-pengunjung'){
            $titleRecap = "Rekapitulasi Pengunjung Rawat Jalan ".$namaPoli;
            $this->setHeaderExcel($titleRecap , $start_date , $end_date);
            $this->DownloadJumlahPengunjung($idPelayanan , $start_date , $end_date);
            $filename = "Rekap_Jumlah_Pengunjung_Poli_".str_replace(" ","_",$namaPoli);
        }else if($jenis_rekapitulasi=="rekap-jaminan-kesehatan"){
            $titleRecap = "Rekapitulasi Pengunjung Rawat Jalan ".$namaPoli;
            $this->setHeaderExcel($titleRecap , $start_date , $end_date);
            $this->DownloadRekapJamkes($idPelayanan , $start_date , $end_date);
            $filename = "Rekap_Jaminan_Kesehatan_Poli_".str_replace(" ","_",$namaPoli);
        }else if($jenis_rekapitulasi=="rekap-data-penyakit"){
            $titleRecap = "Rekapitulasi Data Penyakit ".$namaPoli;
            $this->setHeaderExcel($titleRecap , $start_date , $end_date);
            $this->DownloadRekapDataPenyakit($idPelayanan , $start_date , $end_date);
            $filename = "Rekap_Data_Penyakit_Poli_".str_replace(" ","_",$namaPoli);
        }else if($jenis_rekapitulasi=="rekap-data-tindakan"){
            $titleRecap = "Rekapitulasi Data Tindakan ".$namaPoli;
            $this->setHeaderExcel($titleRecap , $start_date , $end_date);
            $this->DownloadRekapDataTindakan($idPelayanan , $start_date , $end_date);
            $filename = "Rekap_Data_Tindakan_Poli_".str_replace(" ","_",$namaPoli);
        }else if($jenis_rekapitulasi=="rekap-data-rujukan"){
            $titleRecap = "Rekapitulasi Data Rujukan";
            $this->setHeaderExcel($titleRecap , $start_date , $end_date);
            $this->DownloadRekapDataRujukan($start_date , $end_date);
            $filename = "Rekap_Data_Rujukan";
        }else if($jenis_rekapitulasi=="rekap-data-penyakit-tertentu"){
            $titleRecap = "Rekapitulasi Data Penyakit Tertentu";
            $this->setHeaderExcel($titleRecap , $start_date , $end_date);
            $this->DownloadRekapDataPenyakitTertentu($idPelayanan,$start_date , $end_date , $start);
            $filename = "Rekap_Data_Penyakit_Tertentu";
        }else if($jenis_rekapitulasi=="rekap-obat-apotik"){
            $arrApotik = array(
                1 => "Rawat Jalan",
                2 => "Unit Gawat Darurat / Rawat Inap",
            );
            $nameApotik = $arrApotik[$idPelayanan];
            $nameApotikFile = str_replace(" " , "_",$nameApotik);
            $titleRecap = "Rekapitulasi Pemakaian Obat ".$nameApotik;
            $this->setHeaderExcel($titleRecap , $start_date , $end_date);
            $this->DownloadRekapPemakaianObat($idPelayanan,$start_date , $end_date , $start , $length);
            $filename = "Rekap_Pemakaian_Obat_".$nameApotikFile;
        }else if($jenis_rekapitulasi=="rekap-pasien-rujukan"){
            $titleRecap = "Rekapitulasi Data Pasien Rujukan";
            $this->setHeaderExcel($titleRecap , $start_date , $end_date);
            $this->DownloadRekapDataPasienRujukan($start_date , $end_date);
            $filename = "Rekap_Data_Pasien_Rujukan";
        }else if($jenis_rekapitulasi=="rekap-loket"){
            $titleRecap = "Rekapitulasi Data Loket";
            $this->setHeaderExcel($titleRecap , $start_date , $end_date);
            $this->DownloadRekapLoket($idPelayanan,$start_date , $end_date , $start , $length);
            $filename = "Rekap_Data_Loket";
        }else if($jenis_rekapitulasi=="rekap-jumlah-loket"){
            $titleRecap = "Rekapitulasi Jumlah Registrasi Loket";
            $this->setHeaderExcel($titleRecap , $start_date , $end_date);
            $this->DownloadJumlahRekapLoket($idPelayanan,$start_date , $end_date);
            $filename = "Rekap_Jumlah_Loket";
        }
        $this->getOutput($filename);
    }

  
    private function DownloadRekapJamkes($idPelayanan , $start_date , $end_date){
        $this->excel->getActiveSheet()->setCellValue('A6', "Nama Poli");
        $this->excel->getActiveSheet()->setCellValue('B6', "Jaminan Kesehatan");
        $dataJumlahKunjungan = $this->register_model->getJumlahKunjungan($start_date , $end_date, $idPelayanan);
        $no = 1;
        $indexNo = 7;
        $dataResult = $this->register_model->getJumlahKunjunganByJaminanKesehatan($start_date , $end_date, $idPelayanan);
        foreach ($dataResult as $rows) {
            # code...
            $this->excel->getActiveSheet()->setCellValue('A'.($indexNo + $no), $rows['NamaJaminan']);
            $this->excel->getActiveSheet()->setCellValue('B'.($indexNo + $no), $rows['jumlah']);
            $no++;
        }
    }

 

}