<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Rekap_pekerjaan extends MY_Controller {

    var $meta_title = "Rekapitulasi";
    var $meta_desc = "Pekerjaan";
    var $main_title = "pekerjaan";
    var $menu_key = "dashboard";
    var $dtBreadcrumbs = array();  

    public function __construct() {
        parent::__construct();
        $this->load->library("Excel");
        $this->load->model(
            array(
                'pelayanan_model',
                'rekapitulasi_model',
                'register_model',
                'rekam_medis_obat_m' => 'rekam_medis_obat'
            )
        );
    }
    public function index(){
        $this->dtBreadcrumbs = array(
            "Home" =>base_url(),
            "Rekapitulasi" => base_url(),
            "Rekap Pekerjaan" => "#",
        );

        $this->meta_title = "Rekap Pekerjaan";
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "menu_key" => "rekap_pekerjaan",
			"akses_key" => "is_view",
            "container" => $this->_build_rekap(),
            "custom_js" => array(
                ASSETS_JS_URL."rekapitulasi/rekap_pekerjaan.js"
            ),
            "custom_css" => array(),
        );  
        
        $this->_render("default",$dt);             
    }
      private function _build_rekap(){
        $listPelayanan = $this->pelayanan_model->getListDataPelayanan();
        $arrPelayanan = array("0"=>"Semua");
        foreach ($listPelayanan as $valuePelayanan) {
            # code...
            $arrPelayanan[$valuePelayanan['intIdPelayanan']] = $valuePelayanan['txtNama'];
        }
        $arrJenis = $this->config->item('pekerjaan');
        $dt['frmPelayanan'] = $this->form_builder->inputDropdown("Jenis Pekerjaan" , "jenis" , "" , $arrJenis);
        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['frmJenis'] = $this->form_builder->inputDropdown("Ruangan" , "cmbPoli" , "" , $arrPelayanan);
        $dt['title'] = $this->meta_title;
        $ret = $this->load->view("rekap_pekerjaan" , $dt , true);
        return $ret;
    }
       public function getDataRekap(){
        if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";die;
        }

        $idPelayanan =  $this->input->post("id_pelayanan");
		$start_date = $this->input->post("start_date");
        $end_date = $this->input->post("end_date");
        $jenis =  $this->input->post("jenis");
		$jen_pekerjaan = $this->config->item('pekerjaan');

         $dataPengunjung = $this->rekapitulasi_model->getRekapPekerjaan($idPelayanan , $start_date , $end_date , $jenis);
       
         	$retVal['data'] = array();
			foreach($dataPengunjung as $row){
                $jumlah = $this->rekapitulasi_model->CountRekapPekerjaan($idPelayanan , $start_date , $end_date , $jenis,$row['id_pekerjaan']);
               
                   $retVal['data'][] = array(!empty($row['jenis_pekerjaan'])?$jen_pekerjaan[$row['jenis_pekerjaan']]:'Tidak Ada',
                                          !empty($row['pekerjaan'])?$row['pekerjaan']:'Tidak Ada',
										  $jumlah,
                                         
									);
			}
       
        $this->setJsonOutput($retVal);
    }
     public function downloadExcel($idPelayanan , $start_date , $end_date ,$jenis="", $start = 0 , $length = 10){
        $namaPoli = "Semua Ruangan";
        if($idPelayanan!=0){
            $dataPoli =  $this->pelayanan_model->getDetail($idPelayanan);
            $namaPoli = $dataPoli['txtNama'];
        }
            $titleRecap = "Rekapitulasi Pekerjaan";
            $this->setHeaderExcel($titleRecap , $start_date , $end_date);
            $this->DownloadRekapLoket($idPelayanan,$start_date , $end_date ,$jenis, $start , $length);
            $filename = "Rekap_Pekerjaan";
            
            $this->getOutput($filename);
        }
         private function DownloadRekapLoket($idPelayanan , $start_date , $end_date ,$jenis, $start , $length){

        $no = 1;
        $this->excel->getActiveSheet()->setCellValue('A6', "Jenis Pekerjaan");
        $this->excel->getActiveSheet()->setCellValue('B6',"Pekerjaan");
        $this->excel->getActiveSheet()->setCellValue('C6', "jumlah");
        

        $jen_pekerjaan = $this->config->item('pekerjaan');
        $dataPengunjung = $this->rekapitulasi_model->getRekapPekerjaan($idPelayanan , $start_date , $end_date , $jenis);
        $indexNo = 8;
        foreach($dataPengunjung as $row){
            $jumlah = $this->rekapitulasi_model->CountRekapPekerjaan($idPelayanan , $start_date , $end_date , $jenis,$row['id_pekerjaan']);
             
            $this->excel->getActiveSheet()->setCellValue('A'.($indexNo + $no), $jen_pekerjaan[$row['jenis_pekerjaan']]);
            $this->excel->getActiveSheet()->setCellValue('B'.($indexNo + $no), !empty($row['pekerjaan'])?$row['pekerjaan']:'Tidak Ada');
            $this->excel->getActiveSheet()->setCellValue('C'.($indexNo + $no), $jumlah);
           
            $no++;
        }
    }
  
  
  

}