<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Rekapitulasi_data_penyakit extends MY_Controller {

    var $meta_title = "Rekapitulasi";
    var $meta_desc = "Item Keuangan";
    var $main_title = "Item Keuangan";
    var $menu_key = "dashboard";
    var $dtBreadcrumbs = array();  

    public function __construct() {
        parent::__construct();
        $this->load->library("Excel");
        $this->load->model(
            array(
                'pelayanan_model',
                'rekapitulasi_model',
                'register_model',
                'rekam_medis_obat_m' => 'rekam_medis_obat'
            )
        );
    }
 public function index(){
        $this->dtBreadcrumbs = array(
            "Home" =>base_url(),
            "Rekapitulasi" => base_url(),
            "Data Penyakit" => "#",
        );

        $this->meta_title = "Rekapitulasi Jumlah Pengunjung";
        $dt = array(
            "title" => "Rekapitulasi Jumlah Pengunjung",
            "description" => "Rekapitulasi Jumlah Pengunjung",
            "menu_key" => "rekap_jumlah_pengunjung",
			"akses_key" => "is_view",
            "container" => $this->_build_data_penyakit(),
            "custom_js" => array(
                ASSETS_JS_URL."rekapitulasi/rekap_data_penyakit.js"
            ),
            "custom_css" => array(),
        );  
        
        $this->_render("default",$dt);
    }
     public function getRekapDataPenyakit(){
        if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";die;
        }

        $idPelayanan =  $this->input->post("id_pelayanan");
		$start_date = $this->input->post("start_date");
        $end_date = $this->input->post("end_date");
        $dataPengunjung = $this->register_model->getRekapDataPenyakit($start_date , $end_date , $idPelayanan);
        $retVal['data'] = array();
        foreach($dataPengunjung as $row){
            $kodeICDX = $row['txtSubCategory']!="0" ? $row['txtCategory'].".".$row['txtSubCategory'] : $row['txtCategory'];
            $retVal['data'][] = array(
                                    $kodeICDX,
                                    $row['txtIndonesianName'],
                                    $row['jumlah']
                                );
        }
        $this->setJsonOutput($retVal);
    }
       private function _build_data_penyakit(){

        $listPelayanan = $this->pelayanan_model->getListDataPelayanan();
        $arrPelayanan = array("0"=>"Semua");
        foreach ($listPelayanan as $valuePelayanan) {
            # code...
            $arrPelayanan[$valuePelayanan['intIdPelayanan']] = $valuePelayanan['txtNama'];
        }

        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['frmPelayanan'] = $this->form_builder->inputDropdown("Ruangan" , "cmbPoli" , "" , $arrPelayanan);
        $dt['title'] = "Rekapitulasi Data Penyakit";
        $ret = $this->load->view("rekap_data_penyakit" , $dt , true);
        return $ret;
    }
        public function downloadExcel($jenis_rekapitulasi , $idPelayanan , $start_date , $end_date , $start = 0 , $length = 10){
        $namaPoli = "Semua Ruangan";
        if($idPelayanan!=0){
            $dataPoli =  $this->pelayanan_model->getDetail($idPelayanan);
            $namaPoli = $dataPoli['txtNama'];
        }
            $titleRecap = "Rekapitulasi Data Penyakit ".$namaPoli;
            $this->setHeaderExcel($titleRecap , $start_date , $end_date);
            $this->DownloadRekapDataPenyakit($idPelayanan , $start_date , $end_date);
            $filename = "Rekap_Data_Penyakit_Ruangan_".str_replace(" ","_",$namaPoli);
            $this->getOutput($filename);
        }
         
     private function DownloadRekapDataPenyakit($idPelayanan , $start_date , $end_date){
        $this->excel->getActiveSheet()->setCellValue('A6', "Kode Penyakit");
        $this->excel->getActiveSheet()->setCellValue('B6', "Penyakit");
        $this->excel->getActiveSheet()->setCellValue('C6', "Jumlah");
        $dataResult = $this->register_model->getRekapDataPenyakit($start_date , $end_date , $idPelayanan);
        $indexNo = 7;
        foreach ($dataResult as $rows) {
            # code...
            $kodeICDX = $rows['txtSubCategory']!="0" ? $rows['txtCategory'].".".$rows['txtSubCategory'] : $rows['txtCategory'];
            $this->excel->getActiveSheet()->setCellValue('A'.($indexNo), $kodeICDX);
            $this->excel->getActiveSheet()->setCellValue('B'.($indexNo), $rows['txtIndonesianName']);
            $this->excel->getActiveSheet()->setCellValue('C'.($indexNo), $rows['jumlah']);
            $indexNo++;
        }
    }

}
 