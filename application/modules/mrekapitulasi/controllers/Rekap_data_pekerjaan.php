<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Rekap_data_pekerjaan extends MY_Controller {

    var $meta_title = "Rekapitulasi";
    var $meta_desc = "Data Pekerjaan";
    var $main_title = "data pekerjaan";
    var $menu_key = "dashboard";
    var $dtBreadcrumbs = array();  

    public function __construct() {
        parent::__construct();
        $this->load->library("Excel");
        $this->load->model(
            array(
                'pelayanan_model',
                'rekapitulasi_model',
                'register_model',
                'rekam_medis_obat_m' => 'rekam_medis_obat'
            )
        );
    }
    public function index(){
        $this->dtBreadcrumbs = array(
            "Home" =>base_url(),
            "Rekapitulasi" => base_url(),
            "Rekap Data Pekerjaan" => "#",
        );

        $this->meta_title = "Rekap Data Pekerjaan";
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "menu_key" => "rekap_data_pekerjaan",
			"akses_key" => "is_view",
            "container" => $this->_build_rekap(),
            "custom_js" => array(
                ASSETS_JS_URL."rekapitulasi/rekap_data_pekerjaan.js"
            ),
            "custom_css" => array(),
        );  
        
        $this->_render("default",$dt);             
    }
      private function _build_rekap(){
        $listPelayanan = $this->pelayanan_model->getListDataPelayanan();
        $arrPelayanan = array("0"=>"Semua");
        foreach ($listPelayanan as $valuePelayanan) {
            # code...
            $arrPelayanan[$valuePelayanan['intIdPelayanan']] = $valuePelayanan['txtNama'];
        }
        $arrJenis = $this->config->item('pekerjaan');
        $dt['frmPelayanan'] = $this->form_builder->inputDropdown("Jenis Pekerjaan" , "jenis" , "" , $arrJenis);
        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['frmJenis'] = $this->form_builder->inputDropdown("Ruangan" , "cmbPoli" , "" , $arrPelayanan);
        $dt['title'] = $this->meta_title;
        $ret = $this->load->view("rekap_data_pekerjaan" , $dt , true);
        return $ret;
    }
       public function getDataRekap(){
        if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";die;
        }

        $idPelayanan =  $this->input->post("id_pelayanan");
		$start_date = $this->input->post("start_date");
        $end_date = $this->input->post("end_date");
        $jenis =  $this->input->post("jenis");
		$dataPengunjung = $this->rekapitulasi_model->getRekapDataPekerjaan($idPelayanan , $start_date , $end_date , $jenis);
        $retVal['data'] = array();
			foreach($dataPengunjung as $row){
                 $kodeICDX = $row['txtCategory']; 
                  $jenisKelamin = $row['charJkPasien']=="L" ? "Laki-Laki" : "Perempuan";
        
                   $retVal['data'][] = array($row['txtNoAnggota'],
										  $row['txtNamaPasien'],
                                          $row['txtUsiaPasienKunjungan'] ,
                                          $jenisKelamin,
										  !empty($row['pekerjaan'])?$row['pekerjaan']:'Tidak Ada',
                                          $row['txtIndonesianName'],
                                          $kodeICDX,
                                         
									);
			}
       
        $this->setJsonOutput($retVal);
    }
    public function downloadExcel($idPelayanan , $start_date , $end_date , $jenis="", $start = 0 , $length = 10){
          $namaPoli = "Semua Ruangan";
        if($idPelayanan!=0){
            $dataPoli =  $this->pelayanan_model->getDetail($idPelayanan);
            $namaPoli = $dataPoli['txtNama'];
        }
            $titleRecap = "Rekapitulasi Data Pekerjaan";
            $this->setHeaderExcel($titleRecap , $start_date , $end_date);
            $this->DownloadRekap($idPelayanan,$start_date , $end_date ,$jenis);
            $filename = "Rekap_Data_Pekerjaan";
            
            $this->getOutput($filename);
        }
         private function DownloadRekap($idPelayanan , $start_date , $end_date ,$jenis){

        $no = 1;
        $this->excel->getActiveSheet()->setCellValue('A6', "No Anggota");
        $this->excel->getActiveSheet()->setCellValue('B6',"Nama");
        $this->excel->getActiveSheet()->setCellValue('C6', "Usia");
        $this->excel->getActiveSheet()->setCellValue('D6', "Jenis Kelamin");
        $this->excel->getActiveSheet()->setCellValue('E6',"Pekerjaan");
        $this->excel->getActiveSheet()->setCellValue('F6', "Diagnosis");
        $this->excel->getActiveSheet()->setCellValue('G6', "Kode ICD-X");
        


        $dataPengunjung = $this->rekapitulasi_model->getRekapDataPekerjaan($idPelayanan , $start_date , $end_date , $jenis);
        $indexNo = 8;
        foreach($dataPengunjung as $row){
             $kodeICDX = $row['txtCategory'];
             $jenisKelamin = $row['charJkPasien']=="L" ? "Laki-Laki" : "Perempuan";
        
            $this->excel->getActiveSheet()->setCellValue('A'.($indexNo + $no), $row['txtNoAnggota']);
            $this->excel->getActiveSheet()->setCellValue('B'.($indexNo + $no), $row['txtNamaPasien']);
            $this->excel->getActiveSheet()->setCellValue('C'.($indexNo + $no), $row['txtUsiaPasienKunjungan']);
            $this->excel->getActiveSheet()->setCellValue('D'.($indexNo + $no), $jenisKelamin);
            $this->excel->getActiveSheet()->setCellValue('E'.($indexNo + $no), !empty($row['pekerjaan'])?$row['pekerjaan']:'Tidak Ada');
            $this->excel->getActiveSheet()->setCellValue('F'.($indexNo + $no), $row['txtIndonesianName']);
            $this->excel->getActiveSheet()->setCellValue('G'.($indexNo + $no), $kodeICDX);
           
            $no++;
        }
    }
   public function downloadPdf($idPelayanan , $start_date , $end_date , $jenis="", $start = 0 , $length = 10){
       $namaPoli = "Semua Ruangan";
       $dt = array();
        if($idPelayanan!=0){
            $dataPoli =  $this->pelayanan_model->getDetail($idPelayanan);
            $namaPoli = $dataPoli['txtNama'];
        }
       $titleRecap = "Rekapitulasi Data Pekerjaan ".$namaPoli;
       $filename = "Rekap_Data_Pekerjaan";
       $dataPengunjung = $this->rekapitulasi_model->getRekapDataPekerjaan($idPelayanan , $start_date , $end_date , $jenis);
       $dt['title'] = $titleRecap;
       $dt['tgl_awal'] = $start_date;
       $dt['tgl_akhir'] = $end_date;
       $dt['data'] = $dataPengunjung;
       $ret = $this->load->view("print/pdf_data_pekerjaan" , $dt , true);
       generate_pdf($ret, $filename);
  }
  
  

}