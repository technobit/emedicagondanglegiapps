<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Rekapitulasi_data_penyakit extends MY_Controller {

    var $meta_title = "Rekapitulasi";
    var $meta_desc = "Item Keuangan";
    var $main_title = "Item Keuangan";
    var $menu_key = "dashboard";
    var $dtBreadcrumbs = array();  

    public function __construct() {
        parent::__construct();
        $this->load->library("Excel");
        $this->load->model(
            array(
                'pelayanan_model',
                'rekapitulasi_model',
                'register_model',
                'rekam_medis_obat_m' => 'rekam_medis_obat'
            )
        );
    }
 public function index(){
        $this->dtBreadcrumbs = array(
            "Home" =>base_url(),
            "Rekapitulasi" => base_url(),
            "Data Penyakit" => "#",
        );

        $this->meta_title = "Rekapitulasi Jumlah Pengunjung";
        $dt = array(
            "title" => "Rekapitulasi Jumlah Pengunjung",
            "description" => "Rekapitulasi Jumlah Pengunjung",
            "menu_key" => "rekap_jumlah_pengunjung",
			"akses_key" => "is_view",
            "container" => $this->_build_data_penyakit(),
            "custom_js" => array(
                ASSETS_JS_URL."rekapitulasi/rekap_data_penyakit.js"
            ),
            "custom_css" => array(),
        );  
        
        $this->_render("default",$dt);
    }
     public function getRekapDataPenyakit(){
        if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";die;
        }

        $idPelayanan =  $this->input->post("id_pelayanan");
		$start_date = $this->input->post("start_date");
        $end_date = $this->input->post("end_date");
        $dataPengunjung = $this->rekapitulasi_model->getPeringkatDiagnosaPenyakit($start_date , $end_date , $idPelayanan);
        $retVal['data'] = array();
        foreach($dataPengunjung as $row){
            $kodeICDX = $row['txtCategory'];
            $retVal['data'][] = array(
                                    $row['txtEnglishName'],
                                    $kodeICDX,
                                    $row['Register_0_31_laki'],
                                    $row['Register_0_31_pere'],
                                    ($row['Register_0_31_laki'] + $row['Register_0_31_pere']),
                                    $row['Register_31_365_laki'],
                                    $row['Register_31_365_pere'],
                                    ($row['Register_31_365_laki'] + $row['Register_31_365_pere']),
                                    $row['Register_366_1824_laki'],
                                    $row['Register_366_1824_pere'],
                                    ($row['Register_366_1824_laki'] + $row['Register_366_1824_pere']),
                                    $row['Register_1825_5474_laki'],
                                    $row['Register_1825_5474_pere'],
                                    ($row['Register_1825_5474_laki'] + $row['Register_1825_5474_pere']),
                                    $row['Register_5475_9124_laki'],
                                    $row['Register_5475_9124_pere'],
                                    ($row['Register_5475_9124_laki'] + $row['Register_5475_9124_pere']),
                                    $row['Register_9125_16424_laki'],
                                    $row['Register_9125_16424_pere'],
                                    ($row['Register_9125_16424_laki'] + $row['Register_9125_16424_pere']),
                                    $row['Register_16425_21899_laki'],
                                    $row['Register_16425_21899_pere'],
                                    ($row['Register_16425_21899_laki'] + $row['Register_16425_21899_pere']),
                                    $row['Register_21900_23359_laki'],
                                    $row['Register_21900_23359_pere'],
                                    ($row['Register_21900_23359_laki'] + $row['Register_21900_23359_pere']),
                                    $row['Register_23360_laki'],
                                    $row['Register_23360_pere'],
                                    ($row['Register_23360_laki'] + $row['Register_23360_pere']),
                                    $row['Register_baru_laki'],
                                    $row['Register_baru_pere'],
                                    ($row['Register_baru_laki'] + $row['Register_baru_pere']),
                                    $row['Register_lama_laki'],
                                    $row['Register_lama_pere'],
                                    ($row['Register_lama_laki'] + $row['Register_lama_pere']),
                                    $row['Register_total_laki'],
                                    $row['Register_total_pere'],
                                    $row['Total'],
                                );
        }
        $this->setJsonOutput($retVal);
    }
       private function _build_data_penyakit(){

        $listPelayanan = $this->pelayanan_model->getListDataPelayanan();
        $arrPelayanan = array("0"=>"Semua");
        foreach ($listPelayanan as $valuePelayanan) {
            # code...
            $arrPelayanan[$valuePelayanan['intIdPelayanan']] = $valuePelayanan['txtNama'];
        }

        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['frmPelayanan'] = $this->form_builder->inputDropdown("Ruangan" , "cmbPoli" , "" , $arrPelayanan);
        $dt['title'] = "Rekapitulasi Data Penyakit";
        $ret = $this->load->view("rekap_data_penyakit_v2" , $dt , true);
        return $ret;
    }
        public function downloadExcel($jenis_rekapitulasi , $idPelayanan , $start_date , $end_date , $start = 0 , $length = 10){
        $namaPoli = "Semua Ruangan";
        if($idPelayanan!=0){
            $dataPoli =  $this->pelayanan_model->getDetail($idPelayanan);
            $namaPoli = $dataPoli['txtNama'];
        }
            $titleRecap = "Rekapitulasi Data Penyakit ".$namaPoli;
            
            $this->setHeaderExcel($titleRecap , $start_date , $end_date);
            $this->DownloadRekapDataPenyakit($idPelayanan , $start_date , $end_date);
            $filename = "Rekap_Data_Penyakit_Ruangan_".str_replace(" ","_",$namaPoli);
            $this->getOutput($filename);
        }
         
     private function DownloadRekapDataPenyakitOld($idPelayanan , $start_date , $end_date){
        $this->excel->getActiveSheet()->setCellValue('A6', "Kode Penyakit");
        $this->excel->getActiveSheet()->setCellValue('B6', "Penyakit");
        $this->excel->getActiveSheet()->setCellValue('C6', "Jumlah");
        $dataResult = $this->register_model->getRekapDataPenyakit($start_date , $end_date , $idPelayanan);
        $indexNo = 7;
        foreach ($dataResult as $rows) {
            # code...
            $kodeICDX = $rows['txtCategory'];
            $this->excel->getActiveSheet()->setCellValue('A'.($indexNo), $kodeICDX);
            $this->excel->getActiveSheet()->setCellValue('B'.($indexNo), $rows['txtIndonesianName']);
            $this->excel->getActiveSheet()->setCellValue('C'.($indexNo), $rows['jumlah']);
            $indexNo++;
        }
    }

    private function DownloadRekapDataPenyakit($idPelayanan , $start_date , $end_date){
        $this->excel->getActiveSheet()->setCellValue('A6', "Kode Penyakit");
        $this->excel->getActiveSheet()->mergeCells('A6:A8');
        $this->excel->getActiveSheet()->setCellValue('B6', "Penyakit");
        $this->excel->getActiveSheet()->mergeCells('B6:B8');
        $this->excel->getActiveSheet()->setCellValue('C6', "Jumlah Kasus Baru Menurut Golongan Umur");
        $this->excel->getActiveSheet()->mergeCells('C6:AC6');
        $this->excel->getActiveSheet()->setCellValue('C7', "< 1 Bl");
        $this->excel->getActiveSheet()->mergeCells('C7:E7');
        $this->excel->getActiveSheet()->setCellValue('F7', "1Bl - 1Th");
        $this->excel->getActiveSheet()->mergeCells('F7:H7');
        $this->excel->getActiveSheet()->setCellValue('I7', "1-4Th");
        $this->excel->getActiveSheet()->mergeCells('I7:K7');
        $this->excel->getActiveSheet()->setCellValue('L7', "5-14Th");
        $this->excel->getActiveSheet()->mergeCells('L7:N7');
        $this->excel->getActiveSheet()->setCellValue('O7', "15-24Th");
        $this->excel->getActiveSheet()->mergeCells('O7:Q7');
        $this->excel->getActiveSheet()->setCellValue('R7', "25-44Th");
        $this->excel->getActiveSheet()->mergeCells('R7:T7');
        $this->excel->getActiveSheet()->setCellValue('U7', "45-59Th");
        $this->excel->getActiveSheet()->mergeCells('U7:W7');
        $this->excel->getActiveSheet()->setCellValue('X7', "60-64Th");
        $this->excel->getActiveSheet()->mergeCells('X7:Z7');
        $this->excel->getActiveSheet()->setCellValue('AA7', ">65Th");
        $this->excel->getActiveSheet()->mergeCells('AA7:AC7');
        $this->excel->getActiveSheet()->setCellValue('AD6', "Jumlah");
        $this->excel->getActiveSheet()->mergeCells('AD6:AF6');
        $this->excel->getActiveSheet()->setCellValue('AD7', "Kasus Baru");
        $this->excel->getActiveSheet()->mergeCells('AD7:AF7');
        $this->excel->getActiveSheet()->setCellValue('AG6', "Jumlah");
        $this->excel->getActiveSheet()->mergeCells('AG6:AI6');
        $this->excel->getActiveSheet()->setCellValue('AG7', "Kasus Baru");
        $this->excel->getActiveSheet()->mergeCells('AG7:AI7');
        $this->excel->getActiveSheet()->setCellValue('AJ6', "Jumlah");
        $this->excel->getActiveSheet()->mergeCells('AJ6:AL6');
        $this->excel->getActiveSheet()->setCellValue('AJ7', "Kunjungan");
        $this->excel->getActiveSheet()->mergeCells('AJ7:AL7');

        $this->excel->getActiveSheet()->setCellValue('C8' , 'L');
        $this->excel->getActiveSheet()->setCellValue('D8' , 'P');
        $this->excel->getActiveSheet()->setCellValue('E8' , 'Total');

        $this->excel->getActiveSheet()->setCellValue('F8' , 'L');
        $this->excel->getActiveSheet()->setCellValue('G8' , 'P');
        $this->excel->getActiveSheet()->setCellValue('H8' , 'Total');

        $this->excel->getActiveSheet()->setCellValue('I8' , 'L');
        $this->excel->getActiveSheet()->setCellValue('J8' , 'P');
        $this->excel->getActiveSheet()->setCellValue('K8' , 'Total');

        $this->excel->getActiveSheet()->setCellValue('L8' , 'L');
        $this->excel->getActiveSheet()->setCellValue('M8' , 'P');
        $this->excel->getActiveSheet()->setCellValue('N8' , 'Total');

        $this->excel->getActiveSheet()->setCellValue('O8' , 'L');
        $this->excel->getActiveSheet()->setCellValue('P8' , 'P');
        $this->excel->getActiveSheet()->setCellValue('Q8' , 'Total');

        $this->excel->getActiveSheet()->setCellValue('R8' , 'L');
        $this->excel->getActiveSheet()->setCellValue('S8' , 'P');
        $this->excel->getActiveSheet()->setCellValue('T8' , 'Total');

        $this->excel->getActiveSheet()->setCellValue('U8' , 'L');
        $this->excel->getActiveSheet()->setCellValue('V8' , 'P');
        $this->excel->getActiveSheet()->setCellValue('W8' , 'Total');

        $this->excel->getActiveSheet()->setCellValue('X8' , 'L');
        $this->excel->getActiveSheet()->setCellValue('Y8' , 'P');
        $this->excel->getActiveSheet()->setCellValue('Z8' , 'Total');

        $this->excel->getActiveSheet()->setCellValue('AA8' , 'L');
        $this->excel->getActiveSheet()->setCellValue('AB8' , 'P');
        $this->excel->getActiveSheet()->setCellValue('AC8' , 'Total');

        $this->excel->getActiveSheet()->setCellValue('AD8' , 'L');
        $this->excel->getActiveSheet()->setCellValue('AE8' , 'P');
        $this->excel->getActiveSheet()->setCellValue('AF8' , 'Total');

        $this->excel->getActiveSheet()->setCellValue('AG8' , 'L');
        $this->excel->getActiveSheet()->setCellValue('AH8' , 'P');
        $this->excel->getActiveSheet()->setCellValue('AI8' , 'Total');

        $this->excel->getActiveSheet()->setCellValue('AJ8' , 'L');
        $this->excel->getActiveSheet()->setCellValue('AK8' , 'P');
        $this->excel->getActiveSheet()->setCellValue('AL8' , 'Total');

        $dataPengunjung = $this->rekapitulasi_model->getPeringkatDiagnosaPenyakit($start_date , $end_date , $idPelayanan);
        $indexNo = 9;
        foreach ($dataPengunjung as $rows) {
            $this->excel->getActiveSheet()->setCellValue('A'.$indexNo, $rows['txtCategory']);
            $this->excel->getActiveSheet()->setCellValue('B'.$indexNo, $rows['txtEnglishName']);
            $this->excel->getActiveSheet()->setCellValue('C'.$indexNo, $rows['Register_0_31_laki']);
            $this->excel->getActiveSheet()->setCellValue('D'.$indexNo, $rows['Register_0_31_pere']);
            $this->excel->getActiveSheet()->setCellValue('E'.$indexNo, ($rows['Register_0_31_laki'] + $rows['Register_0_31_pere']));
            $this->excel->getActiveSheet()->setCellValue('F'.$indexNo, $rows['Register_31_365_laki']);
            $this->excel->getActiveSheet()->setCellValue('G'.$indexNo, $rows['Register_31_365_pere']);
            $this->excel->getActiveSheet()->setCellValue('H'.$indexNo, ($rows['Register_31_365_laki'] + $rows['Register_31_365_pere']));
            $this->excel->getActiveSheet()->setCellValue('I'.$indexNo, $rows['Register_366_1824_laki']);
            $this->excel->getActiveSheet()->setCellValue('J'.$indexNo, $rows['Register_366_1824_pere']);
            $this->excel->getActiveSheet()->setCellValue('K'.$indexNo, ($rows['Register_366_1824_laki'] + $rows['Register_366_1824_pere']));
            $this->excel->getActiveSheet()->setCellValue('L'.$indexNo, $rows['Register_1825_5474_laki']);
            $this->excel->getActiveSheet()->setCellValue('M'.$indexNo, $rows['Register_1825_5474_pere']);
            $this->excel->getActiveSheet()->setCellValue('N'.$indexNo, ($rows['Register_1825_5474_laki'] + $rows['Register_1825_5474_pere']));
            $this->excel->getActiveSheet()->setCellValue('O'.$indexNo, $rows['Register_5475_9124_laki']);
            $this->excel->getActiveSheet()->setCellValue('P'.$indexNo, $rows['Register_5475_9124_pere']);
            $this->excel->getActiveSheet()->setCellValue('Q'.$indexNo, ($rows['Register_5475_9124_laki'] + $rows['Register_5475_9124_pere']));
            $this->excel->getActiveSheet()->setCellValue('R'.$indexNo, $rows['Register_9125_16424_laki']);
            $this->excel->getActiveSheet()->setCellValue('S'.$indexNo, $rows['Register_9125_16424_pere']);
            $this->excel->getActiveSheet()->setCellValue('T'.$indexNo, ($rows['Register_9125_16424_laki'] + $rows['Register_9125_16424_pere']));
            $this->excel->getActiveSheet()->setCellValue('U'.$indexNo, $rows['Register_16425_21899_laki']);
            $this->excel->getActiveSheet()->setCellValue('V'.$indexNo, $rows['Register_16425_21899_pere']);
            $this->excel->getActiveSheet()->setCellValue('W'.$indexNo, ($rows['Register_16425_21899_laki'] + $rows['Register_16425_21899_pere']));
            $this->excel->getActiveSheet()->setCellValue('X'.$indexNo, $rows['Register_21900_23359_laki']);
            $this->excel->getActiveSheet()->setCellValue('Y'.$indexNo, $rows['Register_21900_23359_pere']);
            $this->excel->getActiveSheet()->setCellValue('Z'.$indexNo, ($rows['Register_21900_23359_laki'] + $rows['Register_21900_23359_pere']));
            $this->excel->getActiveSheet()->setCellValue('AA'.$indexNo, $rows['Register_23360_laki']);
            $this->excel->getActiveSheet()->setCellValue('AB'.$indexNo, $rows['Register_23360_pere']);
            $this->excel->getActiveSheet()->setCellValue('AC'.$indexNo, ($rows['Register_23360_laki'] + $rows['Register_23360_pere']));
            $this->excel->getActiveSheet()->setCellValue('AD'.$indexNo, $rows['Register_baru_laki']);
            $this->excel->getActiveSheet()->setCellValue('AE'.$indexNo, $rows['Register_baru_pere']);
            $this->excel->getActiveSheet()->setCellValue('AF'.$indexNo, ($rows['Register_baru_laki'] + $rows['Register_baru_pere']));
            $this->excel->getActiveSheet()->setCellValue('AG'.$indexNo, $rows['Register_lama_laki']);
            $this->excel->getActiveSheet()->setCellValue('AH'.$indexNo, $rows['Register_lama_pere']);
            $this->excel->getActiveSheet()->setCellValue('AI'.$indexNo, ($rows['Register_baru_laki'] + $rows['Register_baru_pere']));
            $this->excel->getActiveSheet()->setCellValue('AJ'.$indexNo, $rows['Register_total_laki']);
            $this->excel->getActiveSheet()->setCellValue('AK'.$indexNo, $rows['Register_total_pere']);
            $this->excel->getActiveSheet()->setCellValue('AL'.$indexNo, $rows['Total']);
            $indexNo++;
        }
    }

}
 