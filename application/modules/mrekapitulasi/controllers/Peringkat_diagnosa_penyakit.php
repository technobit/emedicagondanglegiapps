<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Peringkat_Diagnosa_Penyakit extends MY_Controller {

    var $meta_title = "Rekapitulasi";
    var $meta_desc = "Item Keuangan";
    var $main_title = "Item Keuangan";
    var $menu_key = "dashboard";
    var $dtBreadcrumbs = array();  

    public function __construct() {
        parent::__construct();
        $this->load->library("Excel");
        $this->load->model(
            array(
                'pelayanan_model',
                'rekapitulasi_model',
                'register_model',
                'rekam_medis_obat_m' => 'rekam_medis_obat'
            )
        );
    } 
public function index(){
        $this->dtBreadcrumbs = array(
            "Home" =>base_url(),
            "Rekapitulasi" => base_url(),
            "Data Pasien Penyakit Menular Tertentu" => "#",
        );
        
        $this->meta_title = "Rekapitulasi Data Pasien Penyakit Tertentu";
        $dt = array(
            "title" => "Rekapitulasi Data Pasien Penyakit Tertentu",
            "description" => "Rekapitulasi Data Pasien Penyakit Tertentu",
            "menu_key" => "rekap_data_penyakit_tertentu",
			"akses_key" => "is_view",
            "container" => $this->_build_pasien_ewars(),
            "custom_js" => array(
                ASSETS_URL."plugins/select2/select2.js",
                ASSETS_JS_URL."rekapitulasi/rekap_pasien_ewars.js"
            ),
            "custom_css" => array(
                ASSETS_URL."plugins/select2/select2.css",
            ),
        );  
        
        $this->_render("default",$dt);
    }
  private function _build_pasien_ewars(){
        $listPelayanan = $this->pelayanan_model->getListDataPelayanan();
        $arrPelayanan = array("0"=>"Semua");
        foreach ($listPelayanan as $valuePelayanan) {
            # code...
            $arrPelayanan[$valuePelayanan['intIdPelayanan']] = $valuePelayanan['txtNama'];
        }

        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['frmPelayanan'] = $this->form_builder->inputDropdown("Ruangan" , "cmbPoli" , "" , $arrPelayanan);
        $dt['title'] = $this->meta_title;
        $arrPenyakit = array();
        $dt['frmPenyakit'] = $this->form_builder->inputDropdown("Penyakit" , "cmbPenyakit" , "" , $arrPenyakit , array("multiple"=>""));
        $ret = $this->load->view("peringkat_diagnosa_penyakit" , $dt , true);
        return $ret;
     }
     public function getPeringkatDiagnosa(){

        if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";die;
        }
 
        $idPelayanan =  $this->input->post("id_pelayanan");
		$start_date = $this->input->post("start_date");
        $end_date = $this->input->post("end_date");
        $dataPengunjung = $this->register_model->getRekapPengunjungPenyakitMenular($idPelayanan , $start_date , $end_date , $dataPenyakit);
       //echopre($dataPengunjung);
        
        $retVal['data'] = array();
       
        $this->setJsonOutput($retVal);
     }
     
     public function downloadExcel($idPelayanan , $start_date , $end_date ,$dataPenyakit){
        $namaPoli = "Semua Ruangan";
        if($idPelayanan!=0){
            $dataPoli =  $this->pelayanan_model->getDetail($idPelayanan);
            $namaPoli = $dataPoli['txtNama'];
        }
           $titleRecap = "Rekapitulasi Data Pengguna Layanan Penyakit Tertentu";
            $this->setHeaderExcel($titleRecap , $start_date , $end_date);
            $this->DownloadRekapDataPasienPenyakitTertentu($idPelayanan,$start_date , $end_date , $dataPenyakit);
            $filename = "Rekap_Data_Pengguna_Layanan_Penyakit_Tertentu";
            
            $this->getOutput($filename);
        }
        private function DownloadRekapDataPasienPenyakitTertentu($idPelayanan , $start_date , $end_date , $dataPenyakit ){
        $arrPenyakit = explode("%" , $dataPenyakit);
        
       $this->excel->getActiveSheet()->setCellValue('A6', "Tanggal Periksa");
        $this->excel->getActiveSheet()->setCellValue('B6', "No Anggota");
        $this->excel->getActiveSheet()->setCellValue('C6', "Nama Pengguna Layanan");
         $this->excel->getActiveSheet()->setCellValue('D6', "Usia");
        $this->excel->getActiveSheet()->setCellValue('E6', "Jenis Kelamin");
        $this->excel->getActiveSheet()->setCellValue('F6', "Code ICD");
         $this->excel->getActiveSheet()->setCellValue('G6', "Nama Penyakit");
        $this->excel->getActiveSheet()->setCellValue('H6', "Jenis Kasus");

        $dataResult = $this->register_model->getRekapPengunjungPenyakitMenular($idPelayanan , $start_date , $end_date ,$arrPenyakit);
        $indexNo = 7;
        foreach ($dataResult as $rows) {
           
              $jenisKasus = $rows['bitIsKasusBaru']=="1" ? "Baru" : ($rows['bitIsKasusBaru']=="0" ? "Lama" : "");
              
            $jenisKelamin = $rows['charJkPasien']=="L" ? "Laki-Laki" : "Perempuan";
            $kodeICDX = $rows['txtCategory']; 
            $this->excel->getActiveSheet()->setCellValue('A'.($indexNo), $rows['dtTanggalKunjungan']);
            $this->excel->getActiveSheet()->setCellValue('B'.($indexNo), $rows['txtNoAnggota']);
            $this->excel->getActiveSheet()->setCellValue('C'.($indexNo), $rows['txtNamaPasien']);
            $this->excel->getActiveSheet()->setCellValue('D'.($indexNo), $rows['txtUsiaPasienKunjungan']);
            $this->excel->getActiveSheet()->setCellValue('E'.($indexNo),  $jenisKelamin);
            $this->excel->getActiveSheet()->setCellValue('F'.($indexNo),  $kodeICDX);
            $this->excel->getActiveSheet()->setCellValue('G'.($indexNo), $rows['txtIndonesianName']);
            $this->excel->getActiveSheet()->setCellValue('H'.($indexNo), $jenisKasus);
            $indexNo++;
        }

    }

}