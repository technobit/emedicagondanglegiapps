<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Rekapitulasi_data_rujukan extends MY_Controller {

    var $meta_title = "Rekapitulasi";
    var $meta_desc = "Item Keuangan";
    var $main_title = "Item Keuangan";
    var $menu_key = "dashboard";
    var $dtBreadcrumbs = array();  

    public function __construct() {
        parent::__construct();
        $this->load->library("Excel");
        $this->load->model(
            array(
                'pelayanan_model',
                'rekapitulasi_model',
                'register_model',
                'rekam_medis_obat_m' => 'rekam_medis_obat'
            )
        );
    }
 public function index(){
        $this->dtBreadcrumbs = array(
            "Home" =>base_url(),
            "Rekapitulasi" => base_url(),
            "Data Rujukan" => "#",
        );

        $this->meta_title = "Rekapitulasi Jumlah Rujukan";
        $dt = array(
            "title" => $this->meta_title,
            "description" => "Rekapitulasi Jumlah Rujukan",
            "menu_key" => "rekap_data_rujukan",
			"akses_key" => "is_view",
            "container" => $this->_build_data_rujukan(),
            "custom_js" => array(
                ASSETS_JS_URL."rekapitulasi/rekap_data_rujukan.js"
            ),
            "custom_css" => array(),
        );  
        
        $this->_render("default",$dt);
    }

    private function _build_data_rujukan(){
        $listPelayanan = $this->pelayanan_model->getListDataPelayanan();
        $arrPelayanan = array("0"=>"Semua");
        foreach ($listPelayanan as $valuePelayanan) {
            # code...
            $arrPelayanan[$valuePelayanan['intIdPelayanan']] = $valuePelayanan['txtNama'];
        }

        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['frmPelayanan'] = $this->form_builder->inputDropdown("Ruangan" , "cmbPoli" , "" , $arrPelayanan);
        $dt['title'] = $this->meta_title;
        $ret = $this->load->view("rekap_data_rujukan" , $dt , true);
        return $ret;
    }
    public function getDataRujukan(){
        if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";die;
        }

		$start_date = $this->input->post("start_date");
        $end_date = $this->input->post("end_date");
        $dataRujukan = $this->register_model->getRekapRujukan($start_date , $end_date);
        $retVal['data'] = array();
        foreach($dataRujukan as $rowRujukan){
        $retVal['data'][] = array(
                                $rowRujukan['txtNama'],
                                $rowRujukan['jumlah']
                            );
        }
        $this->setJsonOutput($retVal);
    } 
     public function downloadExcel($jenis_rekapitulasi , $idPelayanan , $start_date , $end_date , $start = 0 , $length = 10){
        $namaPoli = "Semua Ruangan";
        if($idPelayanan!=0){
            $dataPoli =  $this->pelayanan_model->getDetail($idPelayanan);
            $namaPoli = $dataPoli['txtNama'];
        }
        $titleRecap = "Rekapitulasi Data Rujukan";
            $this->setHeaderExcel($titleRecap , $start_date , $end_date);
            $this->DownloadRekapDataRujukan($start_date , $end_date);
            $filename = "Rekap_Data_Rujukan";
             $this->getOutput($filename);
     }
      private function DownloadRekapDataRujukan( $start_date , $end_date){
        $this->excel->getActiveSheet()->setCellValue('A6', "Tempat Rujukan");
        $this->excel->getActiveSheet()->setCellValue('B6', "Jumlah");
        $dataResult = $this->register_model->getRekapRujukan($start_date , $end_date);
        $indexNo = 7;
        foreach ($dataResult as $rows) {
            # code...
            $this->excel->getActiveSheet()->setCellValue('A'.($indexNo), $rows['txtNama']);
            $this->excel->getActiveSheet()->setCellValue('B'.($indexNo), $rows['jumlah']);
            $indexNo++;
        }
    }
}