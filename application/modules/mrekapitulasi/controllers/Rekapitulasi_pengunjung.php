<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Rekapitulasi_pengunjung extends MY_Controller {

    var $meta_title = "Rekapitulasi";
    var $meta_desc = "Item Keuangan";
    var $main_title = "Item Keuangan";
    var $menu_key = "dashboard";
    var $dtBreadcrumbs = array();  

    public function __construct() {
        parent::__construct();
        $this->load->library("Excel");
        $this->load->model(
            array(
                'pelayanan_model',
                'rekapitulasi_model',
                'register_model',
                'rekam_medis_model',
                'rekam_medis_obat_m' => 'rekam_medis_obat'
            )
        );
    }
      public function index(){
        $this->dtBreadcrumbs = array(
            "Home" =>base_url(),
            "Rekapitulasi" => base_url(),
            "Data Pengunjung" => "#",
        );

        $this->meta_title = "Rekapitulasi Data Pengunjung pada Pelayanan";
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "menu_key" => "rekap_pengunjung_per_poli",
			"akses_key" => "is_view",
            "container" => $this->_build_rekap_pengunjung(),
            "custom_js" => array(
                ASSETS_JS_URL."rekapitulasi/rekap_pengunjung.js"
            ),
            "custom_css" => array(),
        );  
        
        $this->_render("default",$dt);             
    }
     private function _build_rekap_pengunjung(){

        $listPelayanan = $this->pelayanan_model->getListDataPelayanan();
        $arrPelayanan = array("0"=>"Semua");
        foreach ($listPelayanan as $valuePelayanan) {
            # code...
            $arrPelayanan[$valuePelayanan['intIdPelayanan']] = $valuePelayanan['txtNama'];
        }
 
        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['frmPelayanan'] = $this->form_builder->inputDropdown("Ruangan" , "cmbPoli" , "" , $arrPelayanan);
        $dt['title'] = $this->meta_title;
        $ret = $this->load->view("rekap_pengunjung" , $dt , true);
        return $ret;
    }
       public function getDataRekapPengunjung(){
        if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";die;
        }

        $idPelayanan =  $this->input->post("id_pelayanan");
		$start_date = $this->input->post("start_date");
        $end_date = $this->input->post("end_date");
        $length =  $this->input->post("length");
		$start = $this->input->post("start");

        $dataPengunjung = $this->register_model->getRekapPengunjungRawatJalan($idPelayanan , $start_date , $end_date , $start , $length);
        $countDataPengunjung = $this->register_model->getCountRekapPengunjungRawatJalan($idPelayanan , $start_date , $end_date );
       // echopre($dataPengunjung);
        if($countDataPengunjung > 0) {
            $retVal['draw'] = $this->input->post('draw');
			$retVal['recordsTotal'] = $countDataPengunjung;
			$retVal['recordsFiltered'] = $countDataPengunjung;
			$retVal['data'] = array();
			foreach($dataPengunjung as $row){
                $rowPengobatan = $this->rekam_medis_model->getDataResep($row['txtIdRekmedDetail']);
                $pengobatan = "";
                if ((empty($rowPengobatan))&&(!empty($row['txtPengobatan']))) {
                      $pengobatan = $row['txtPengobatan'];
                
                }else{
                  foreach ($rowPengobatan as $value) {
                   $pengobatan .= 	 "<dl>
									<dd>".$value['nama_obat']."</dd>
								</dl>";
                }
                }
                $jenisKelamin = $row['charJkPasien']=="L" ? "Laki-Laki" : "Perempuan";
                $jenisKunjungan = $row['bitIsBaru']=="1" ? "Baru" : "Lama";
                $jenisKasus = $row['bitIsKasusBaru']=="1" ? "Baru" : ($row['bitIsKasusBaru']=="0" ? "Lama" : "");
                $jenisWilayah = $row['txtWilayahPasien']=="dalam" ? "Dalam" : "Luar";
                $kodeICDX = $row['txtSubCategory']!="0" ? $row['txtCategory'].".".$row['txtSubCategory'] : $row['txtCategory']; 
                $ttdPasien = !empty($row['txtTtdPasien']) ? "<img src=".BASE_URL.$row['txtTtdPasien']. " width='100' alt=''>" : ""; 
				$retVal['data'][] = array($row['txtNoAnggota'],
										  $row['txtNamaPasien'],
                                          $row['txtUsiaPasienKunjungan'] ,
                                          $jenisKelamin,
                                          $row['NamaKelurahan'] ,
                                          $row['txtJaminanKesehatan'] ,
                                          $jenisKunjungan ,
										  $jenisWilayah ,
                                          $jenisKasus ,
                                          $kodeICDX ,
                                          $pengobatan ,
                                          $row['txtKeteranganKunjungan'] ,
                                          $row['txtNamaPelayanan'] ,
									);
			}
        }else{
            $retVal['draw'] = $this->input->post('draw');
			$retVal['recordsTotal'] = 0;
			$retVal['recordsFiltered'] = 0;
			$retVal['data'] = array();
        }
        $this->setJsonOutput($retVal);
    }
     public function downloadExcel($jenis_rekapitulasi , $idPelayanan , $start_date , $end_date , $start = 0 , $length = 10){
        $namaPoli = "Semua Ruangan";
        if($idPelayanan!=0){
            $dataPoli =  $this->pelayanan_model->getDetail($idPelayanan);
            $namaPoli = $dataPoli['txtNama'];
        }
            $titleRecap = "Rekapitulasi Pengunjung Rawat Jalan ".$namaPoli;
            $this->setHeaderExcel($titleRecap , $start_date , $end_date);
            $this->DownloadRekapPengunjung($idPelayanan , $start_date , $end_date , $start , $length);
            $filename = "Rekap_Pengunjung_Ruangan_".str_replace(" ","_",$namaPoli);
            $this->getOutput($filename);
        }
        private function DownloadRekapPengunjung($idPelayanan , $start_date , $end_date , $start , $length){
        
        $no = 1;
        $this->excel->getActiveSheet()->setCellValue('A6', "No");
        $this->excel->getActiveSheet()->setCellValue('B6', "No Anggota");
        $this->excel->getActiveSheet()->setCellValue('C6', "Nama Pengguna Layanan");
        $this->excel->getActiveSheet()->setCellValue('D6', "Usia Pengguna Layanan");
        $this->excel->getActiveSheet()->setCellValue('E6', "Jenis Kelamin");
        $this->excel->getActiveSheet()->setCellValue('F6', "Alamat");
        $this->excel->getActiveSheet()->setCellValue('G6', "Jenis Pembayaran");
        $this->excel->getActiveSheet()->setCellValue('H6', "Jenis Kunjungan");
        $this->excel->getActiveSheet()->setCellValue('I6', "Jenis Wilayah");
        $this->excel->getActiveSheet()->setCellValue('J6', "Jenis Kasus");
        $this->excel->getActiveSheet()->setCellValue('K6', "Kode ICD-X");
        $this->excel->getActiveSheet()->setCellValue('L6', "Pengobatan");
        $this->excel->getActiveSheet()->setCellValue('M6', "Keterangan");
        $this->excel->getActiveSheet()->setCellValue('N6', "Nama Pelayanan");
        
        $indexNo = 6;
        $dataPengunjung = $this->register_model->getRekapPengunjungRawatJalan($idPelayanan , $start_date , $end_date , $start , $length);
        foreach ($dataPengunjung as $row) {
            # code...
              $rowPengobatan = $this->rekam_medis_model->getDataResep($row['txtIdRekmedDetail']);
                $pengobatan = "";
                if (!empty($rowPengobatan)) {
                foreach ($rowPengobatan as $value) {
                   $pengobatan .= 	 $value['nama_obat'].",";
                }
                }else{
                    $pengobatan = $row['txtPengobatan'];
                }
            $jenisKelamin = $row['charJkPasien']=="L" ? "Laki-Laki" : "Perempuan";
            $jenisKunjungan = $row['bitIsBaru']=="1" ? "Baru" : "Lama";
            ///$jenisKasus = $row['bitIsKasusBaru']=="1" ? "Baru" : "Lama";
            $jenisKasus = $row['bitIsKasusBaru']=="1" ? "Baru" : ($row['bitIsKasusBaru']=="0" ? "Lama" : "");
            $jenisWilayah = $row['txtWilayahPasien']=="dalam" ? "Dalam" : "Luar";
            $kodeICDX = $row['txtCategory'];

            $this->excel->getActiveSheet()->setCellValue('A'.($indexNo + $no), $no);
            $this->excel->getActiveSheet()->setCellValue('B'.($indexNo + $no), $row['txtNoAnggota']);
            $this->excel->getActiveSheet()->setCellValue('C'.($indexNo + $no), $row['txtNamaPasien']);
            $this->excel->getActiveSheet()->setCellValue('D'.($indexNo + $no), $row['txtUsiaPasienKunjungan']);
            $this->excel->getActiveSheet()->setCellValue('E'.($indexNo + $no), $jenisKelamin);
            $this->excel->getActiveSheet()->setCellValue('F'.($indexNo + $no), $row['NamaKelurahan']);
            $this->excel->getActiveSheet()->setCellValue('G'.($indexNo + $no), $row['txtJaminanKesehatan']);
            $this->excel->getActiveSheet()->setCellValue('H'.($indexNo + $no), $jenisKunjungan);
            $this->excel->getActiveSheet()->setCellValue('I'.($indexNo + $no), $jenisWilayah);
            $this->excel->getActiveSheet()->setCellValue('J'.($indexNo + $no), $jenisKasus);
            $this->excel->getActiveSheet()->setCellValue('K'.($indexNo + $no), $kodeICDX);
            $this->excel->getActiveSheet()->setCellValue('L'.($indexNo + $no), $pengobatan);
            $this->excel->getActiveSheet()->setCellValue('M'.($indexNo + $no), $row['txtKeteranganKunjungan']);
            $this->excel->getActiveSheet()->setCellValue('N'.($indexNo + $no), $row['txtNamaPelayanan']);
            $no++;
        }

    }
    public function printOut($jenis_rekapitulasi , $idPelayanan , $start_date , $end_date , $start = 0 , $length = 10){
        $namaPoli = "Semua Ruangan";
        if($idPelayanan!=0){
            $dataPoli =  $this->pelayanan_model->getDetail($idPelayanan);
            $namaPoli = $dataPoli['txtNama'];
        }
             $titleRecap = "Rekapitulasi Pengunjung Rawat Jalan ".$namaPoli;
            $dataPengunjung = $this->register_model->getRekapPengunjungRawatJalan($idPelayanan , $start_date , $end_date , $start , $length);
        $countDataPengunjung = $this->register_model->getCountRekapPengunjungRawatJalan($idPelayanan , $start_date , $end_date );
            
        $dt['dataPengunjung'] = $dataPengunjung;
        $dt['countDataPengunjung'] = $countDataPengunjung;
        $dt['start_date'] = $start_date;
        $dt['end_date'] = $end_date;
        $dt['titleRecap'] = $titleRecap;
        $dt['title'] = $this->meta_title;
        $ret = $this->load->view("print/rekap_pengunjung" , $dt );
        }
        
        
    // Create Cronjob For Create View Table
    public function createTableViewLaporan(){
        
        
    }


}