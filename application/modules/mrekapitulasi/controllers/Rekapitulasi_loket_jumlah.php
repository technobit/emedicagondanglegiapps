<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Rekapitulasi_loket_jumlah extends MY_Controller {

    var $meta_title = "Rekapitulasi";
    var $meta_desc = "Item Keuangan";
    var $main_title = "Item Keuangan";
    var $menu_key = "dashboard";
    var $dtBreadcrumbs = array();  

    public function __construct() {
        parent::__construct();
        $this->load->library("Excel");
        $this->load->model(
            array(
                'pelayanan_model',
                'rekapitulasi_loket_model',
                'register_model',
                'rekam_medis_obat_m' => 'rekam_medis_obat'
            )
        );
    }
    
    public function index(){
        $this->dtBreadcrumbs = array(
            "Home" =>base_url(),
            "Rekapitulasi" => base_url(),
            "Rekap Loket Jumlah" => "#",
        );

        $this->meta_title = "Data Rekap Loket Jumlah";
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "menu_key" => "rekap_jumlah_antrian",
			"akses_key" => "is_view",
            "container" => $this->_build_rekap_loket(),
            "custom_js" => array(
                ASSETS_JS_URL."rekapitulasi/rekap_loket_jumlah.js"
            ),
            "custom_css" => array(),
        );  
        
        $this->_render("default",$dt);             
    }
      private function _build_rekap_loket(){
        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['title'] = $this->meta_title;
        $ret = $this->load->view("rekap_loket_jumlah" , $dt , true);
        return $ret;
    }
    
    public function getDataRekapLoket(){
        if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";die;
        }

		$start_date = $this->input->post("start_date");
        $end_date = $this->input->post("end_date");
        $dataPengunjung = $this->rekapitulasi_loket_model->getDataRekapPelayananLoket($start_date , $end_date);
        
        $retVal['data'] = array();
        if(!empty($dataPengunjung)){
            $retVal['data'] = array();
			foreach($dataPengunjung as $row){
                $total_antrian = $row['antrian'] + $row['sedang_dilayani'] + $row['selesai_dilayani'];
				$retVal['data'][] = array(
                    $row['txtNama'],
                    $row['antrian'],
                    $row['sedang_dilayani'],
                    $row['selesai_dilayani'],
                    $total_antrian
				);
			}
        }

        $this->setJsonOutput($retVal);
    }
     public function downloadExcel($jenis_rekapitulasi , $idPelayanan , $idStatus , $start_date , $end_date , $start = 0 , $length = 10){
        $namaPoli = "Semua Ruangan";
        if($idPelayanan!=0){
            $dataPoli =  $this->pelayanan_model->getDetail($idPelayanan);
            $namaPoli = $dataPoli['txtNama'];
        }
            $titleRecap = "Rekapitulasi Data Loket";
            $this->setHeaderExcel($titleRecap , $start_date , $end_date);
            $this->DownloadRekapLoket($idPelayanan,$start_date , $end_date , $start , $length);
            $filename = "Rekap_Data_Loket";
            
            $this->getOutput($filename);
        }
        private function DownloadRekapLoket($idPelayanan , $start_date , $end_date , $start , $length){

        $no = 1;
        $this->excel->getActiveSheet()->setCellValue('A6', "No");
        $this->excel->getActiveSheet()->setCellValue('B6', "Nomor Antrian");
        $this->excel->getActiveSheet()->setCellValue('C6', "Waktu Kunjungan");
        $this->excel->getActiveSheet()->setCellValue('D6', "Waktu Selesai Kunjungan");
        $this->excel->getActiveSheet()->setCellValue('E6', "Jenis Loket");
        $this->excel->getActiveSheet()->setCellValue('F6', "Keterangan");
        $this->excel->getActiveSheet()->setCellValue('G6', "Status Kunjungan");
        $dataPengunjung = $this->rekapitulasi_loket_model->getDataRekapLoket($idPelayanan , $idStatus, $start_date , $end_date , $start , $length);
        $status_layanan_list = $this->config->item("status_layanan_list");
        $indexNo = 7;
        foreach($dataPengunjung as $row){
            

            $this->excel->getActiveSheet()->setCellValue('A'.($indexNo + $no), $no);
            $this->excel->getActiveSheet()->setCellValue('B'.($indexNo + $no), indonesian_date($row['dtTanggalKunjungan'] , 'j F Y H:i'));
            $this->excel->getActiveSheet()->setCellValue('C'.($indexNo + $no), $row['txtNamaPasien']);
            $this->excel->getActiveSheet()->setCellValue('D'.($indexNo + $no), !empty($row['dtSelesaiKunjungan']) ? indonesian_date($row['dtSelesaiKunjungan'] , 'j F Y H:i') : "");
            $this->excel->getActiveSheet()->setCellValue('E'.($indexNo + $no), $row['txtNama']);
            $this->excel->getActiveSheet()->setCellValue('F'.($indexNo + $no), $row['txtKeterangan']);
            $this->excel->getActiveSheet()->setCellValue('G'.($indexNo + $no), $status_layanan_list[$row['bitIsPoli']]);
            $no++;
        }
    }
  
  

}