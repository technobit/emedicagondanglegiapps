<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Data_jamkes extends MY_Controller {

    var $meta_title = "Rekapitulasi";
    var $meta_desc = "Item Keuangan";
    var $main_title = "Item Keuangan";
    var $menu_key = "rekap_kunjungan_jamkes";
    var $dtBreadcrumbs = array();  

    public function __construct() {
        parent::__construct();
        $this->load->library("Excel");
        $this->load->model(
            array(
                'pelayanan_model',
                'rekapitulasi_model',
                'register_model',
                'rekam_medis_obat_m' => 'rekam_medis_obat'
            )
        );
    }
    public function index(){
        $this->dtBreadcrumbs = array(
            "Home" =>base_url(),
            "Rekapitulasi" => base_url(),
            "Rekap Kunjungan Jaminan Kesehatan" => "#",
        );

        $this->meta_title = "Data Kunjungan Jaminan Kesehatan";
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "menu_key" => $this->menu_key,
			"akses_key" => "is_view",
            "container" => $this->_build_rekap_loket(),
            "custom_js" => array(
                ASSETS_JS_URL."rekapitulasi/data_jamkes.js"
            ),
            "custom_css" => array(),
        );  
        
        $this->_render("default",$dt);             
    }
      private function _build_rekap_loket(){
        $listPelayanan = $this->pelayanan_model->getListDataPelayanan();
        $listJamkes = $this->rekapitulasi_model->getJamkes();
        $arrPelayanan = array("0"=>"Semua");
        foreach ($listPelayanan as $valuePelayanan) {
            # code...
            $arrPelayanan[$valuePelayanan['intIdPelayanan']] = $valuePelayanan['txtNama'];
        }
         $arrjamkes = array(""=>"Semua");
        foreach ($listJamkes as $value) {
            # code...
            $arrjamkes[$value['intIdJaminanKesehatan']] = $value['txtNamaJaminan'];
        }
        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['frmPelayanan'] = $this->form_builder->inputDropdown("Ruangan" , "cmbPoli" , "" , $arrPelayanan);
        $dt['frmJamkes'] = $this->form_builder->inputDropdown("Jaminan Kesehatan" , "jamkes" , "" , $arrjamkes);
        $dt['title'] = $this->meta_title;
        $ret = $this->load->view("data_jamkes" , $dt , true);
        return $ret;
    }
       public function getDataRekapLoket(){
        if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";die;
        }

        
        $jamkes = $this->input->post("jamkes");
        $idPelayanan =  $this->input->post("id_pelayanan");
		$start_date = $this->input->post("start_date");
        $end_date = $this->input->post("end_date");
        $length =  $this->input->post("length");
		$start = $this->input->post("start");

        $dataPengunjung = $this->rekapitulasi_model->getDataJamkes($idPelayanan , $start_date , $end_date ,$jamkes, $start , $length);
        $countDataPengunjung = $this->rekapitulasi_model->CountDataJamkes($idPelayanan , $start_date , $end_date,$jamkes );

        $retVal['draw'] = $this->input->post('draw');
        $retVal['recordsTotal'] = 0;
        $retVal['recordsFiltered'] = 0;
        $retVal['data'] = array();
               // echopre($dataPengunjung);
        if(!empty($countDataPengunjung)){
            $retVal['draw'] = $this->input->post('draw');
			$retVal['recordsTotal'] = $countDataPengunjung;
			$retVal['recordsFiltered'] = $countDataPengunjung;
			$retVal['data'] = array();
            $no = 0;
            //echopre($dataPengunjung);
			foreach($dataPengunjung as $row){
               $no++;
                $ttd = $this->rekapitulasi_model->getTtd($row['txtIdRekamMedis']);
                $ttdPasien = !empty($ttd['txtTandaTangan']) ? "<img src=".BASE_URL.$ttd['txtTandaTangan']. " width='100' alt=''>" : ""; 
				$retVal['data'][] = array($no,
                                          indonesian_date($row['dtTanggalKunjungan']),
                                          $row['txtNamaPasien'],
                                          $row['txtNoJaminanKesehatan'],
                                          $row['txtNoKk'],
                                          $row['Nama'],
									);
			}
        }

        $this->setJsonOutput($retVal);
    }
     public function downloadExcel($idPelayanan , $start_date , $end_date ,$jamkes , $start = 0 , $length = 10){
        $namaPoli = "Semua Ruangan";
        if($idPelayanan!=0){
            $dataPoli =  $this->pelayanan_model->getDetail($idPelayanan);
            $namaPoli = $dataPoli['txtNama'];
        }
            $titleRecap = "Rekapitulasi Data Kunjungan Jaminan Kesehatan";
            $this->setHeaderExcel($titleRecap , $start_date , $end_date);
            $this->DownloadRekapLoket($idPelayanan,$start_date , $end_date ,$jamkes, $start , $length);
            $filename = "Rekap_Data_jamkes";
            
            $this->getOutput($filename);
        }
         private function DownloadRekapLoket($idPelayanan , $start_date , $end_date ,$jamkes, $start , $length){

        $no = 1;
        $this->excel->getActiveSheet()->setCellValue('A6', "No");
        $this->excel->getActiveSheet()->setCellValue('B6',"Tanggal");
        $this->excel->getActiveSheet()->setCellValue('C6', "Nama Peserta");
        $this->excel->getActiveSheet()->setCellValue('D6',"Nomor JKN");
        $this->excel->getActiveSheet()->setCellValue('E6', "NIK");
        $this->excel->getActiveSheet()->setCellValue('F6', "Alamat");
        


        $dataPengunjung = $this->rekapitulasi_model->getDataJamkes($idPelayanan , $start_date , $end_date ,$jamkes, $start , $length);
        $indexNo = 8;
        foreach($dataPengunjung as $row){
            $this->excel->getActiveSheet()->setCellValue('A'.($indexNo + $no), $no);
            $this->excel->getActiveSheet()->setCellValue('B'.($indexNo + $no), indonesian_date($row['dtTanggalKunjungan']));
            $this->excel->getActiveSheet()->setCellValue('C'.($indexNo + $no), $row['txtNamaPasien']);
            $this->excel->getActiveSheet()->setCellValue('D'.($indexNo + $no), $row['txtNoJaminanKesehatan']);
            $this->excel->getActiveSheet()->setCellValue('E'.($indexNo + $no), $row['txtNoKk']);
            $this->excel->getActiveSheet()->setCellValue('F'.($indexNo + $no), $row['nama']);
           $no++;
        }
    }
  
    public function printOut($idPelayanan , $start_date , $end_date ,$jamkes, $start = 0 , $length = 10){
        $namaPoli = "Semua Ruangan";
        if($idPelayanan!=0){
            $dataPoli =  $this->pelayanan_model->getDetail($idPelayanan);
            $namaPoli = $dataPoli['txtNama'];
        }
            $titleRecap = "Rekapitulasi Data Kunjungan Jaminan Kesehatan".$namaPoli;
            $dataPengunjung = $this->rekapitulasi_model->getDataJamkes($idPelayanan , $start_date , $end_date ,$jamkes, $start , $length);
            
        $dt['dataPengunjung'] = $dataPengunjung;
        $dt['start_date'] = $start_date;
        $dt['end_date'] = $end_date;
        $dt['titleRecap'] = $titleRecap;
        $dt['title'] = $this->meta_title;
      $ret = $this->load->view("print/data_jamkes" , $dt );
        }     
  

}