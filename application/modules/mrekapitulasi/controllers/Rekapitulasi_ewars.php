<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Rekapitulasi_ewars extends MY_Controller {

    var $meta_title = "Rekapitulasi";
    var $meta_desc = "Item Keuangan";
    var $main_title = "Item Keuangan";
    var $menu_key = "dashboard";
    var $dtBreadcrumbs = array();  

    public function __construct() {
        parent::__construct();
        $this->load->library("Excel");
        $this->load->model(
            array(
                'pelayanan_model',
                'rekapitulasi_model',
                'register_model',
                'rekam_medis_obat_m' => 'rekam_medis_obat'
            )
        );
    } 
public function index(){
        $this->dtBreadcrumbs = array(
            "Home" =>base_url(),
            "Rekapitulasi" => base_url(),
            "Data Penyakit Menular Tertentu" => "#",
        );
        
        $this->meta_title = "Rekapitulasi Data Penyakit Tertentu";
        $dt = array(
            "title" => "Rekapitulasi Data Penyakit Tertentu",
            "description" => "Rekapitulasi Data Penyakit Tertentu",
            "menu_key" => "rekap_data_penyakit_tertentu",
			"akses_key" => "is_view",
            "container" => $this->_build_rekap_ewars(),
            "custom_js" => array(
                ASSETS_URL."plugins/select2/select2.js",
                ASSETS_JS_URL."rekapitulasi/rekap_ewars.js"
            ),
            "custom_css" => array(
                ASSETS_URL."plugins/select2/select2.css",
            ),
        );  
        
        $this->_render("default",$dt);
    }
  private function _build_rekap_ewars(){
        $listPelayanan = $this->pelayanan_model->getListDataPelayanan();
        $arrPelayanan = array("0"=>"Semua");
        foreach ($listPelayanan as $valuePelayanan) {
            # code...
            $arrPelayanan[$valuePelayanan['intIdPelayanan']] = $valuePelayanan['txtNama'];
        }

        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['frmPelayanan'] = $this->form_builder->inputDropdown("Ruangan" , "cmbPoli" , "" , $arrPelayanan);
        $dt['title'] = $this->meta_title;
        $arrPenyakit = array();
        $dt['frmPenyakit'] = $this->form_builder->inputDropdown("Penyakit" , "cmbPenyakit" , "" , $arrPenyakit , array("multiple"=>""));
        $ret = $this->load->view("rekap_data_penyakit_menular" , $dt , true);
        return $ret;
    }
     public function getRekapDataPenyakitEwars(){

        if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";die;
        }
 
        $idPelayanan =  $this->input->post("id_pelayanan");
		$start_date = $this->input->post("start_date");
        $end_date = $this->input->post("end_date");
        $listPenyakit = $this->input->post("data_penyakit");
        if(empty($listPenyakit)){
        $retVal['data'] = array();
        echo json_encode($retVal);
        die;
        }
        
        $dataPenyakit = $listPenyakit;
        $dataResult = $this->register_model->getRekapDataPenyakit($start_date , $end_date , $idPelayanan,$dataPenyakit);
        $retVal['data'] = array();
        foreach($dataResult as $row){
            $kodeICDX = $row['txtCategory'];
            $retVal['data'][] = array(
                                    $kodeICDX,
                                    $row['txtIndonesianName'],
                                    $row['jumlah']
                                );
        }
        $this->setJsonOutput($retVal);
    }
     public function downloadExcel($jenis_rekapitulasi , $idPelayanan , $start_date , $end_date , $start = 0 , $length = 10){
        $namaPoli = "Semua Ruangan";
        if($idPelayanan!=0){
            $dataPoli =  $this->pelayanan_model->getDetail($idPelayanan);
            $namaPoli = $dataPoli['txtNama'];
        }
           $titleRecap = "Rekapitulasi Data Penyakit Tertentu";
            $this->setHeaderExcel($titleRecap , $start_date , $end_date);
            $this->DownloadRekapDataPenyakitTertentu($idPelayanan,$start_date , $end_date , $start);
            $filename = "Rekap_Data_Penyakit_Tertentu";
            
            $this->getOutput($filename);
        }
        private function DownloadRekapDataPenyakitTertentu($idPelayanan , $start_date , $end_date , $dataPenyakit ){
        $arrPenyakit = explode("%" , $dataPenyakit);
        
        $this->excel->getActiveSheet()->setCellValue('A6', "Kode Penyakit");
        $this->excel->getActiveSheet()->setCellValue('B6', "Penyakit");
        $this->excel->getActiveSheet()->setCellValue('C6', "Jumlah");
        $dataResult = $this->register_model->getRekapDataPenyakit($start_date , $end_date , $idPelayanan,$arrPenyakit);
        $indexNo = 7;
        foreach ($dataResult as $rows) {
            # code...
            $kodeICDX = $rows['txtCategory'];
            $this->excel->getActiveSheet()->setCellValue('A'.($indexNo), $kodeICDX);
            $this->excel->getActiveSheet()->setCellValue('B'.($indexNo), $rows['txtIndonesianName']);
            $this->excel->getActiveSheet()->setCellValue('C'.($indexNo), $rows['jumlah']);
            $indexNo++;
        }

    }

}