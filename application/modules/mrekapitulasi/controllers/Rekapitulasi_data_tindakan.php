 <?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Rekapitulasi_data_tindakan extends MY_Controller {

    var $meta_title = "Rekapitulasi";
    var $meta_desc = "Item Keuangan";
    var $main_title = "Item Keuangan";
    var $menu_key = "dashboard";
    var $dtBreadcrumbs = array();  

    public function __construct() {
        parent::__construct();
        $this->load->library("Excel");
        $this->load->model(
            array(
                'pelayanan_model',
                'rekapitulasi_model',
                'register_model',
                'rekam_medis_obat_m' => 'rekam_medis_obat'
            )
        );
    }

  public function index(){
        $this->dtBreadcrumbs = array(
            "Home" =>base_url(),
            "Rekapitulasi" => base_url(),
            "Data Tindakan" => "#",
        );

        $this->meta_title = "Rekapitulasi Data Tindakan";
        $dt = array(
            "title" => $this->meta_title,
            "description" => "Rekapitulasi Data Tindakan",
            "menu_key" => "rekap_data_tindakan",
			"akses_key" => "is_view",
            "container" => $this->_build_data_tindakan(),
            "custom_js" => array(
                ASSETS_JS_URL."rekapitulasi/rekap_data_tindakan.js"
            ),
            "custom_css" => array(),
        );  
        
        $this->_render("default",$dt);
    }
     private function _build_data_tindakan(){
        $listPelayanan = $this->pelayanan_model->getListDataPelayanan();
        $arrPelayanan = array("0"=>"Semua");
        foreach ($listPelayanan as $valuePelayanan) {
            # code...
            $arrPelayanan[$valuePelayanan['intIdPelayanan']] = $valuePelayanan['txtNama'];
        }

        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['frmPelayanan'] = $this->form_builder->inputDropdown("Ruangan" , "cmbPoli" , "" , $arrPelayanan);
        $dt['title'] = "Rekapitulasi Data Tindakan";
        $ret = $this->load->view("rekap_data_tindakan" , $dt , true);
        return $ret;
    }
       public function getRekapDataTindakan(){
        if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";die;
        }

        $idPelayanan =  $this->input->post("id_pelayanan");
		$start_date = $this->input->post("start_date");
        $end_date = $this->input->post("end_date");
        $dataRows = $this->register_model->getRekapTindakan($start_date , $end_date , $idPelayanan);
        $retVal['data'] = array();
        foreach($dataRows as $row){
            ///$kodeICDX = $row['txtSubCategory']!="0" ? $row['txtCategory'].".".$row['txtSubCategory'] : $row['txtCategory'];
            $retVal['data'][] = array(
                                    $row['txtTindakan'],
                                    $row['txtDeskripsi'],
                                    $row['jumlah']
                                );
        }
        $this->setJsonOutput($retVal);
    }
    public function downloadExcel($jenis_rekapitulasi , $idPelayanan , $start_date , $end_date , $start = 0 , $length = 10){
        $namaPoli = "Semua Ruangan";
        if($idPelayanan!=0){
            $dataPoli =  $this->pelayanan_model->getDetail($idPelayanan);
            $namaPoli = $dataPoli['txtNama'];
        }
       
           $titleRecap = "Rekapitulasi Data Tindakan ".$namaPoli;
            $this->setHeaderExcel($titleRecap , $start_date , $end_date);
            $this->DownloadRekapDataTindakan($idPelayanan , $start_date , $end_date);
            $filename = "Rekap_Data_Tindakan_Ruangan_".str_replace(" ","_",$namaPoli);
            $this->getOutput($filename);
     }
         private function DownloadRekapDataTindakan($idPelayanan , $start_date , $end_date){
        $this->excel->getActiveSheet()->setCellValue('A6', "Tindakan");
        $this->excel->getActiveSheet()->setCellValue('B6', "Deskripsi");
        $this->excel->getActiveSheet()->setCellValue('C6', "Jumlah");
        $dataRows = $this->register_model->getRekapTindakan($start_date , $end_date , $idPelayanan);
        $indexNo = 7;
         foreach($dataRows as $row){
           # code...
            $this->excel->getActiveSheet()->setCellValue('A'.($indexNo),  $row['txtTindakan']);
            $this->excel->getActiveSheet()->setCellValue('B'.($indexNo),  $row['txtDeskripsi']);
            $this->excel->getActiveSheet()->setCellValue('C'.($indexNo), $row['jumlah']);
            $indexNo++;
        }
        
    }
    
 
}