<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Kunjungan_rawat_inap extends MY_Controller {

    var $meta_title = "Rekapitulasi";
    var $meta_desc = "Item Keuangan";
    var $main_title = "Item Keuangan";
    var $menu_key = "dashboard";
    var $dtBreadcrumbs = array();  

    public function __construct() {
        parent::__construct();
        $this->load->library("Excel");
        $this->load->model(
            array(
                'pelayanan_model',
                'rekapitulasi_model',
                'register_model',
                'kamar_model',
            )
        );
    }

   public function index(){
        $this->dtBreadcrumbs = array(
            "Home" =>base_url(),
            "Rekapitulasi" => base_url(),
            "Data Pengunjung Rawat Inap" => "#",
        );

        $this->meta_title = "Data Kunjungan rawat Inap";
        $dt = array(
            "title" => "Data Kunjungan Rawat Inap",
            "description" => "Data Kunjungan Rawat Inap",
            "menu_key" => "rekap_kunjungan_rawat_inap",
			"akses_key" => "is_view",
            "container" => $this->_build_kunjungan(),
            "custom_js" => array(
                ASSETS_JS_URL."rekapitulasi/kunjungan_rawat_inap.js"
            ),
            "custom_css" => array(),
        );  
        
        $this->_render("default",$dt);
    }
 private function _build_kunjungan(){

        $listPelayanan = $this->kamar_model->getListDataKamar();
        $arrPelayanan = array("0"=>"Semua");
        foreach ($listPelayanan as $valuePelayanan) {
            # code...
            $arrPelayanan[$valuePelayanan['intIdKamar']] = $valuePelayanan['txtKamar'];
        }

        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['frmPelayanan'] = $this->form_builder->inputDropdown("Kamar" , "kamar" , "" , $arrPelayanan);
        $dt['title'] = $this->meta_title;
        $ret = $this->load->view("Kunjungan_rawat_inap" , $dt , true);
        return $ret;
    }
  public function getData(){
        if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";die;
        }

        $kamar =  $this->input->post("kamar");
		$start_date = $this->input->post("date1");
        $end_date = $this->input->post("date2");
        $status = false;
        $data = "";
         $retVal['data'] = array();
        $dataResult = $this->rekapitulasi_model->getRekapDataRawatInap($start_date , $end_date, $kamar);
        $status_keluar = $this->config->item('kondisi_keluar');
        $status_layanan = $this->config->item('status_layanan_list');
        // echopre($dataResult);
        if(!empty($dataResult)){
           
          $retVal['data'] = array();
            foreach ($dataResult as $value) {
            $jenisKelamin = $value['charJkPasien'] == 'L' ? 'Laki Laki' : 'Perempuan';
            $tanggalMasuk = indonesian_date($value['dtTanggalKunjungan']);
			$tanggalKeluar = indonesian_date($value['dtKeluar']);
            $diff = "";
            if (!empty($value['dtKeluar'])) {
                $date1 = strtotime($tanggalMasuk); // or your date as well
                $date2 = strtotime($tanggalKeluar);
                $datediff = $date2 - $date1;

                $diff = floor($datediff / (60 * 60 * 24));
            }
 
              $retVal['data'][] = array(
                    $value['txtNoAnggota'],
                    $value['txtNamaPasien'],
                    $value['txtUsiaPasienKunjungan'],
                    $jenisKelamin,
                    $value['txtAlamatPasien'],
                    $value['txtKamar'],
                    !empty($value['dtTanggalKunjungan']) ? indonesian_date($value['dtTanggalKunjungan']): "",
                    indonesian_date($value['dtKeluar']),
                    $diff,
                    !empty($value['intKeadaanKeluar']) ? $status_keluar[$value['intKeadaanKeluar']]:"Dirawat",
                    $value['txtNamaJaminan'],
                    $value['txtNoJaminanKesehatan'],
                    $value['txtDiagnosaKeluar'],
                );
               
            }
        }
        
          $this->setJsonOutput($retVal);
    }

  

    public function downloadExcel($kamar , $start_date , $end_date , $start = 0 , $length = 10){
        $namaKamar = "Semua Kamar";
        if (!empty($kamar)) {
            $data = $this->kamar_model->getDetailKamar($kamar);
            $namaKamar = $data['txtKamar'];
        }
        
            $titleRecap = "Rekapitulasi Pengunjung Rawat Inap ".$namaKamar;
            $this->setHeaderExcel($titleRecap , $start_date , $end_date);
            $this->DownloadRekap($kamar , $start_date , $end_date , $start , $length);
            $filename = "Rekap_Pengunjung_Kamar_".str_replace(" ","_",$namaKamar);
        
        $this->getOutput($filename);
    }

    private function DownloadRekap($kamar , $start_date , $end_date, $start , $length){
        $this->excel->getActiveSheet()->setCellValue('A6', "No Anggota");
        $this->excel->getActiveSheet()->setCellValue('B6', "Nama");
          $this->excel->getActiveSheet()->setCellValue('C6', "Usia");
        $this->excel->getActiveSheet()->setCellValue('D6', "Jenis Kelamin");
          $this->excel->getActiveSheet()->setCellValue('E6', "Alamat");
        $this->excel->getActiveSheet()->setCellValue('F6', "Kamar");
          $this->excel->getActiveSheet()->setCellValue('G6', "Tanggal Masuk");
        $this->excel->getActiveSheet()->setCellValue('H6', "Tanggal keluar");
          $this->excel->getActiveSheet()->setCellValue('I6', "Jumlah Hari Rawat Inap");
        $this->excel->getActiveSheet()->setCellValue('J6', "Status Keluar");
          $this->excel->getActiveSheet()->setCellValue('K6', "Jaminan Kesehatan");
        $this->excel->getActiveSheet()->setCellValue('L6', "No Jaminan Kesehatan");
          $this->excel->getActiveSheet()->setCellValue('M6', "Diagnosa");
        
        $no = 1;
        $indexNo = 7;
        $dataResult = $this->rekapitulasi_model->getRekapDataRawatInap($start_date , $end_date, $kamar);
        $status_keluar = $this->config->item('kondisi_keluar');
        $status_layanan = $this->config->item('status_layanan_list');
        foreach ($dataResult as $rows) {
            $jenisKelamin = $rows['charJkPasien'] == 'L' ? 'Laki Laki' : 'Perempuan';
            $tanggalMasuk = indonesian_date($rows['dtTanggalKunjungan']);
			$tanggalKeluar = indonesian_date($rows['dtKeluar']);
           
 
            $this->excel->getActiveSheet()->setCellValue('A'.($indexNo + $no), $rows['txtNoAnggota']);
            $this->excel->getActiveSheet()->setCellValue('B'.($indexNo + $no), $rows['txtNamaPasien']);
            $this->excel->getActiveSheet()->setCellValue('C'.($indexNo + $no), $rows['txtUsiaPasienKunjungan']);
            $this->excel->getActiveSheet()->setCellValue('D'.($indexNo + $no), $jenisKelamin);
             $this->excel->getActiveSheet()->setCellValue('E'.($indexNo + $no), $rows['txtAlamatPasien']);
            $this->excel->getActiveSheet()->setCellValue('F'.($indexNo + $no), $rows['txtKamar']);
            $this->excel->getActiveSheet()->setCellValue('G'.($indexNo + $no), !empty($rows['dtTanggalKunjungan']) ? indonesian_date($rows['dtTanggalKunjungan']): "");
            $this->excel->getActiveSheet()->setCellValue('H'.($indexNo + $no),  !empty($rows['dtKeluar']) ? indonesian_date($rows['dtKeluar']): "");
            $this->excel->getActiveSheet()->setCellValue('I'.($indexNo + $no),  $rows['dtTanggalKunjungan']);
            $this->excel->getActiveSheet()->setCellValue('J'.($indexNo + $no),  !empty($rows['intKeadaanKeluar']) ? $status_keluar[$rows['intKeadaanKeluar']]:"Dirawat");
            $this->excel->getActiveSheet()->setCellValue('K'.($indexNo + $no), $rows['txtNamaJaminan']);
            $this->excel->getActiveSheet()->setCellValue('L'.($indexNo + $no), $rows['txtNoJaminanKesehatan']);
            $this->excel->getActiveSheet()->setCellValue('M'.($indexNo + $no), $rows['txtDiagnosaKeluar']);
            
            $no++;
        }
    }

 
}