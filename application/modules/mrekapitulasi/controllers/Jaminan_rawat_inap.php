<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Jaminan_rawat_inap extends MY_Controller {

    var $meta_title = "Rekapitulasi";
    var $meta_desc = "Item Keuangan";
    var $main_title = "Item Keuangan";
    var $menu_key = "dashboard";
    var $dtBreadcrumbs = array();  

    public function __construct() {
        parent::__construct();
        $this->load->library("Excel");
        $this->load->model(
            array(
                'pelayanan_model',
                'rekapitulasi_model',
                'register_model',
                'kamar_model',
            )
        );
    }

   public function index(){
        $this->dtBreadcrumbs = array(
            "Home" =>base_url(),
            "Rekapitulasi" => base_url(),
            "Data Penggunaan Jaminan Kesehatan Rawat Inap" => "#",
        );

        $this->meta_title = "Rekapitulasi Jaminan Kesehatan Rawat Inap";
        $dt = array(
            "title" => "Rekapitulasi Jaminan Kesehatan Rawat Inap",
            "description" => "Rekapitulasi Jaminan Kesehatan Rawat Inap",
            "menu_key" => "Kunjungan_rawat_inap",
			"akses_key" => "is_view",
            "container" => $this->_build_kunjungan(),
            "custom_js" => array(
                ASSETS_JS_URL."rekapitulasi/jaminan_rawat_inap.js"
            ),
            "custom_css" => array(),
        );  
        
        $this->_render("default",$dt);
    }
 private function _build_kunjungan(){

        $listPelayanan = $this->kamar_model->getListDataKamar();
        $arrPelayanan = array("0"=>"Semua");
        foreach ($listPelayanan as $valuePelayanan) {
            # code...
            $arrPelayanan[$valuePelayanan['intIdKamar']] = $valuePelayanan['txtKamar'];
        }

        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['frmPelayanan'] = $this->form_builder->inputDropdown("Kamar" , "kamar" , "" , $arrPelayanan);
        $dt['title'] = $this->meta_title;
        $ret = $this->load->view("jaminan_rawat_inap" , $dt , true);
        return $ret;
    }
  public function getData(){
        if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";die;
        }

        $kamar =  $this->input->post("kamar");
		$start_date = $this->input->post("date1");
        $end_date = $this->input->post("date2");
        $status = false;
        $data = "";
         $retVal['data'] = array();
        $dataResult = $this->rekapitulasi_model->GetDatajam($start_date , $end_date, $kamar);
        
        // echopre($dataResult);
        if(!empty($dataResult)){
           
          $retVal['data'] = array();
            foreach ($dataResult as $value) {
            $dataJumlah = $this->rekapitulasi_model->getJumlah($start_date , $end_date, $kamar,$value['id']);
          //echopre($dataJumlah);
            $jumlah_pasien = count($dataJumlah);
 
              $retVal['data'][] = array(
                    $value['nama'],
                    $jumlah_pasien,
                    $value['id'],
                    );
               
            }
        }else{
            $retVal['data'][] = array(
                0,0,0
            );
        }
        
          $this->setJsonOutput($retVal);
    }

  

    public function downloadExcel($kamar , $start_date , $end_date , $start = 0 , $length = 10){
        $namaKamar = "Semua Kamar";
        if (!empty($kamar)) {
            $data = $this->kamar_model->getDetailKamar($kamar);
            $namaKamar = $data['txtKamar'];
        }
        
            $titleRecap = "Rekapitulasi Jumlah Jaminan Rawat Inap ".$namaKamar;
            $this->setHeaderExcel($titleRecap , $start_date , $end_date);
            $this->DownloadRekap($kamar , $start_date , $end_date , $start , $length);
            $filename = "Rekap_Jaminan_Kamar_".str_replace(" ","_",$namaKamar);
        
        $this->getOutput($filename);
    }

    private function DownloadRekap($kamar , $start_date , $end_date, $start , $length){
        $this->excel->getActiveSheet()->setCellValue('A6', "Jaminan Kesehatan");
        $this->excel->getActiveSheet()->setCellValue('B6', "Jumlah");
          $this->excel->getActiveSheet()->setCellValue('C6', "Jumlah Hari Rawat Inap");
        
        $no = 1;
        $indexNo = 7;
        $dataResult = $this->rekapitulasi_model->GetDatajam($start_date , $end_date, $kamar);
        foreach ($dataResult as $rows) {
            $dataJumlah = $this->rekapitulasi_model->getJumlah($start_date , $end_date, $kamar,$rows['id']);
            $jumlah_pasien = count($dataJumlah);
            $this->excel->getActiveSheet()->setCellValue('A'.($indexNo + $no), $rows['nama']);
            $this->excel->getActiveSheet()->setCellValue('B'.($indexNo + $no), $jumlah_pasien);
            $this->excel->getActiveSheet()->setCellValue('C'.($indexNo + $no), $rows['id']);
            
            $no++;
        }
    }

 
}