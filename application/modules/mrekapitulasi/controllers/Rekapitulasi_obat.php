<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Rekapitulasi_obat extends MY_Controller {

    var $meta_title = "Rekapitulasi";
    var $meta_desc = "Item Keuangan";
    var $main_title = "Item Keuangan";
    var $menu_key = "dashboard";
    var $dtBreadcrumbs = array();  

    public function __construct() {
        parent::__construct();
        $this->load->library("Excel");
        $this->load->model(
            array(
                'pelayanan_model',
                'rekapitulasi_model',
                'register_model',
                'rekam_medis_obat_m' => 'rekam_medis_obat'
            )
        );
    }

    public function index() {    
        $this->dtBreadcrumbs = array(
            "Home" =>base_url(),
            "Rekapitulasi" => base_url(),
            "Data Penyakit Menular Tertentu" => "#",
        );
        
        $this->meta_title = "Rekapitulasi Obat";
        $dt = array(
            "title" => "Rekapitulasi Data Obat Apotik",
            "description" => "Rekapitulasi Data Obat Apotik",
            "menu_key" => "rekap_data_obat_apotik",
			"akses_key" => "is_view",
            "container" => $this->_build_rekap_apotik(),
            "custom_js" => array(
                ASSETS_URL."plugins/select2/select2.js",
                ASSETS_JS_URL."rekapitulasi/rekap_obat.js"
            ),
            "custom_css" => array(
                ASSETS_URL."plugins/select2/select2.css",
            ),
        );  
        
        $this->_render("default",$dt);
    }

    private function _build_rekap_apotik(){
        $dt = array();
        $arrApotik = array(
            1 => "Rawat Jalan",
            2 => "Unit Gawat Darurat / Rawat Inap",
        );
        ///$dt['listApotik'] = form_dropdown("intIdApotik" , $arrApotik , "" , 'id="intIdApotik" class="form-control"');
        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['title'] = $this->meta_title;
        $dt['listApotik'] = $this->form_builder->inputDropdown("Apotik" , "intIdApotik" , "" , $arrApotik);
        $ret = $this->load->view("rekap_obat_apotik" , $dt , true);
        return $ret;
    }
    public function getRekapitulasiObat(){
        if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";die;
        }

        $start_date =  $this->input->post("start_date");
		$end_date = $this->input->post("end_date");
        $listApotik = $this->input->post("intIdApotik");
        $length =  $this->input->post("length");
		$start = $this->input->post("start");
		$dataPemakaian = $this->rekam_medis_obat->getRekapitulasiPemakaianObat($listApotik , $start_date , $end_date , $length , $start);
        $countDataPemakaian = $this->rekam_medis_obat->getCountRekapPemakaianObat($listApotik , $start_date , $end_date);
        if($countDataPemakaian > 0) {
            $retVal['draw'] = $this->input->post('draw');
			$retVal['recordsTotal'] = $countDataPemakaian;
			$retVal['recordsFiltered'] = $countDataPemakaian;
			$retVal['data'] = array();
			foreach($dataPemakaian as $row){ 
				$retVal['data'][] = array($row['kode_obat'],
										  $row['nama_obat'],
                                          $row['jumlah'] ,
									);
			}
        }else{
            $retVal['draw'] = $this->input->post('draw');
			$retVal['recordsTotal'] = 0;
			$retVal['recordsFiltered'] = 0;
			$retVal['data'] = array();
        }
        $this->setJsonOutput($retVal);
    }
    public function downloadExcel($jenis_rekapitulasi , $idPelayanan , $start_date , $end_date , $start = 0 , $length = 10){
        $namaPoli = "Semua Ruangan";
        if($idPelayanan!=0){
            $dataPoli =  $this->pelayanan_model->getDetail($idPelayanan);
            $namaPoli = $dataPoli['txtNama'];
        }
           $arrApotik = array(
                1 => "Rawat Jalan",
                2 => "Unit Gawat Darurat / Rawat Inap",
            );
            $nameApotik = $arrApotik[$idPelayanan];
            $nameApotikFile = str_replace(" " , "_",$nameApotik);
            $titleRecap = "Rekapitulasi Pemakaian Obat ".$nameApotik;
            $this->setHeaderExcel($titleRecap , $start_date , $end_date);
            $this->DownloadRekapPemakaianObat($idPelayanan,$start_date , $end_date , $start , $length);
            $filename = "Rekap_Pemakaian_Obat_".$nameApotikFile;
            
            $this->getOutput($filename);
        }
        private function DownloadRekapPemakaianObat( $idApotik,$start_date , $end_date , $offset , $limit){
        $this->excel->getActiveSheet()->setCellValue('A6', "Kode Obat");
        $this->excel->getActiveSheet()->setCellValue('B6', "Nama Obat");
        $this->excel->getActiveSheet()->setCellValue('C6', "Jumlah");
        $dataResult = $this->rekam_medis_obat->getRekapitulasiPemakaianObat($idApotik , $start_date , $end_date , $limit , $offset);
        $indexNo = 7;
        foreach ($dataResult as $rows) {
            # code...
            $this->excel->getActiveSheet()->setCellValue('A'.($indexNo), $rows['kode_obat']);
            $this->excel->getActiveSheet()->setCellValue('B'.($indexNo), $rows['nama_obat']);
            $this->excel->getActiveSheet()->setCellValue('C'.($indexNo), $rows['jumlah']);
            $indexNo++;
        }
    }
}
