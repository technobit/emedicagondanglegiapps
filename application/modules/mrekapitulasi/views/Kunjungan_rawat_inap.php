<section class="content-header">
  <h1 class="title"><?=$title?></h1>
  <?=$breadcrumbs?>
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
    <!-- Cari Data Pasien -->
    <div class="box box-success">
      <div class="box-header with-border">
      <h3 class="box-title">Filter</h3>
      </div>
    <!-- /.box-header -->
    <!-- form start -->
        <form class="form-horizontal" id="frm-rekap-pengunjung">
          <div class="box-body">
                <?=$frmPelayanan?>
                <div class="form-group">
                    <label class="col-sm-3 control-label form-label">Tanggal</label>
                    <div class="col-sm-3">
                        <div class="input-group date">
                            <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                            </div>
                            <input value="<?=date("Y-m-d")?>" type="text" name="start_date" class="form-control datepicker pull-right" id="datepicker1">
                        </div>
                    </div>
                    <div class="col-sm-1">
                        <label class="control-label form-label">s.d.</label>
                    </div>
                    <div class="col-sm-3">
                        <div class="input-group date">
                            <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                            </div>
                            <input value="<?=date("Y-m-d")?>" type="text" name="end_date" class="form-control datepicker pull-left" id="datepicker2">
                        </div>
                    </div>
                </div>
                <div class="col-sm-offset-3 col-sm-7">
                    <button type="button" id="sendBtn" class="btn btn-success btn-flat"><i class="fa fa-send"></i> Cari</button>
                </div>
          </div>
        </form>
    </div>
      <div class="box box-primary">
        <div class="box-header">
          <h3 class="box-title">Data Jumlah Pengunjung Rawat Inap</h3>
          <div class="box-tools pull-right">
          <button class="btn btn-warning" id="btnExportExcel"><i class="fa fa-download"></i> Download Excel</button>          
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          
          <table class="table table-hover" id="table-jumlah-pengunjung-rawat-inap">
            <thead>
              <tr>
                <th>No Anggota</th>
                <th>Nama</th>
                <th>Usia</th>
                <th>Jenis Kelamin</th>
                <th>Alamat</th>
                <th>Kamar</th>
                <th>Tanggal Masuk</th>
                <th>Tanggal Keluar</th>
                <th>Jumlah Hari Rawat Inap</th>
                <th>Status Keluar</th>
                <th>Jaminan Kesehatan</th>
                <th>No Jaminan Kesehatan</th>
                <th>Diagnosa</th>
              </tr>
            </thead>
            <tbody>
              
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>