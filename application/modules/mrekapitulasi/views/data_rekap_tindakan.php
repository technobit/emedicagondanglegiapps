<section class="content-header">
  <h1 class="title"><?=$title?></h1>
  <?=$breadcrumbs?>
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
    <!-- Cari Data Pasien -->
    <div class="box box-success">
      <div class="box-header with-border">
      <h3 class="box-title">Filter</h3>
      </div>
    <!-- /.box-header -->
    <!-- form start -->
        <form class="form-horizontal" id="frm-rekap-pengunjung">
          <div class="box-body">
                
                <div class="form-group">
                    <label class="col-sm-3 control-label form-label">Tanggal</label>
                    <div class="col-sm-3">
                        <div class="input-group date">
                            <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                            </div>
                            <input value="<?=date("Y-m-d")?>" type="text" name="start_date" class="form-control datepicker pull-right" id="datepicker1">
                        </div>
                    </div>
                    <div class="col-sm-1">
                        <label class="control-label form-label">s.d.</label>
                    </div>
                    <div class="col-sm-3">
                        <div class="input-group date">
                            <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                            </div>
                            <input value="<?=date("Y-m-d")?>" type="text" name="end_date" class="form-control datepicker pull-left" id="datepicker2">
                        </div>
                    </div>
                    </div>
                    <?=$frmPelayanan?>
                      <div class="form-group">
        <label class="col-sm-3 control-label form-label" >Persetujuan</label>
  <div class="form-group">
      <?=form_hidden('pernyataan', '1','id="pernyataan"');?>
                            <div class="col-sm-3">
                                <div class="checkbox">
                                <label>
                                    <?=form_radio("persetujuan" , "setuju", true, "id='frm_setuju'");?>
                                    Setuju
                                </label>
                                </div>
                            </div>
                            <div class="form-group">
                            <div class="col-sm-3">
                                <div class="checkbox">
                                <label>
                                    <?=form_radio("persetujuan" , "setuju", false, "id='frm_tidak_setuju'");?>
                                    Tidak Setuju
                                </label>
                                </div>
                            </div>
</div>
                </div>
                <div class="col-sm-offset-3 col-sm-7">
                    <button type="button" id="sendBtn" class="btn btn-success btn-flat"><i class="fa fa-send"></i> Cari</button>
                </div>
          </div>
        </form>
    </div>
      <div class="box box-primary">
        <div class="box-header">
          <h3 class="box-title">Data Tindakan</h3>
          <div class="box-tools pull-right">
          <button class="btn btn-warning" id="btnExportExcel"><i class="fa fa-download"></i> Download Excel</button>          
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
        
          <table class="table table-hover" id="table-data">
            <thead>
              <tr>
                 <th>Tanggal</th>
                <th>No Anggota</th>
                <th>Nama</th>
                <th>Usia</th>
                <th>Jenis Kelamin</th>
                <th>Pelayanan</th>
                <th>Tindakan</th>
                 <th>Persetujuan</th>
                
              </tr>
            </thead>
            <tbody>
              
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>