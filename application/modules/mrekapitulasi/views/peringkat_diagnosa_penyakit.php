<section class="content-header">
  <h1 class="title"><?=$title?></h1>
  <?=$breadcrumbs?>
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
    <!-- Cari Data Pasien -->
    <div class="box box-success">
      <div class="box-header with-border">
      <h3 class="box-title">Filter</h3>
      </div>
    <!-- /.box-header -->
    <!-- form start -->
        <form class="form-horizontal" id="frm-rekap-pengunjung">
          <div class="box-body">
                <?=$frmPelayanan?>
                <div class="form-group">
                    <label class="col-sm-3 control-label form-label">Tanggal</label>
                    <div class="col-sm-3">
                        <div class="input-group date">
                            <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                            </div>
                            <input value="<?=date("Y-m-d")?>" type="text" name="start_date" class="form-control datepicker pull-right" id="datepicker1">
                        </div>
                    </div>
                    <div class="col-sm-1">
                        <label class="control-label form-label">s.d.</label>
                    </div>
                    <div class="col-sm-3">
                        <div class="input-group date">
                            <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                            </div>
                            <input value="<?=date("Y-m-d")?>" type="text" name="end_date" class="form-control datepicker pull-left" id="datepicker2">
                        </div>
                    </div>
                </div>
                <div class="col-sm-offset-3 col-sm-7">
                    <button type="button" id="sendBtn" class="btn btn-success btn-flat"><i class="fa fa-send"></i> Cari</button>
                </div>
          </div>
        </form>
    </div>
      <div class="box box-primary">
        <div class="box-header">
          <h3 class="box-title">Data Penyakit</h3>
          <div class="box-tools pull-right">
          <button class="btn btn-warning" id="btnExportExcel"><i class="fa fa-download"></i> Download Excel</button>          
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive">
          <table class="table table-hover" id="table-data-laporan">
            <thead>
              <tr>
                <th rowspan="3">Nama Penyakit</th>
                <th rowspan="3">Kode ICD - X</th>
                <th colspan="27">Jumlah Penderita Menurut Golongan Umur</th>                            
                <th rowspan="2" colspan="3">Jumlah Kasus Baru</th>
                <th rowspan="2" colspan="3">Jumlah  Kasus Lama</th>
                <th rowspan="2" colspan="3">Total</th>
              </tr>
              <tr>
                <th colspan="3">< 1 Bl</th>
                <th colspan="3">1 Bl - 1 Th</th>
                <th colspan="3">1 - 4 Th</th>
                <th colspan="3">5 - 14 Th</th>
                <th colspan="3">15 - 24 Th</th>
                <th colspan="3">25 - 44 Th</th>
                <th colspan="3">45 - 59 Th</th>
                <th colspan="3">60 - 64 Th</th>
                <th colspan="3">> 65 Tahun</th>
              </tr>
              <tr>
                <th>L</th>
                <th>P</th>
                <th>Total</th>
                <th>L</th>
                <th>P</th>
                <th>Total</th>
                <th>L</th>
                <th>P</th>
                <th>Total</th>
                <th>L</th>
                <th>P</th>
                <th>Total</th>
                <th>L</th>
                <th>P</th>
                <th>Total</th>
                <th>L</th>
                <th>P</th>
                <th>Total</th>
                <th>L</th>
                <th>P</th>
                <th>Total</th>
                <th>L</th>
                <th>P</th>
                <th>Total</th>
                <th>L</th>
                <th>P</th>
                <th>Total</th>
                <th>L</th>
                <th>P</th>
                <th>Total</th>
                <th>L</th>
                <th>P</th>
                <th>Total</th>
                <th>L</th>
                <th>P</th>
                <th>Total</th>
              </tr>
            </thead>
            <tbody>
              
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>