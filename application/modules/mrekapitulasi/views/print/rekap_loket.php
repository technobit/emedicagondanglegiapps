<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>Print Rekapitulasi Loket </title>
   <link href="<?=ASSETS_URL?>/img/icon.png" rel="shortcut icon" />
  <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.2 -->
    <link href="<?=ASSETS_URL?>bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="<?=ASSETS_URL?>dist/css/AdminLTE.css" rel="stylesheet" type="text/css" />
    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins 
         folder instead of downloading all of them to reduce the load. -->
    <link href="<?=ASSETS_URL?>dist/css/skins/_all-skins.css" rel="stylesheet" type="text/css" />
    <link href="<?=ASSETS_URL?>custom/css/global.css" rel="stylesheet" type="text/css" />
    <link href="<?=ASSETS_URL?>plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="<?=ASSETS_URL?>plugins/datepicker/datepicker3.css" rel="stylesheet" type="text/css" />
    <!-- new post -->
    <!-- date and time picker -->
    <!-- Bootstrap datetime Picker -->
</head>
<body onload="window.print()">
<section class="invoice">
<div class="row text-center" style="">
    <div class="col-sm-12 invoice-col header-skr">
      
      <h1><b>Puskesmas Bululawang</b><h1>
    </div>
    
    
</div>

</div>
<div class="row invoice-info header-profile">
<div class="col-sm-3 invoice-col">
<h4><b><?=$titleRecap?></b></h4>
</div>

</div>
<div class="row invoice-info header-profile">
  <div class="col-sm-3 invoice-col">
    <address> 
      Tanggal Awal<br>
      Tanggal Akhir<br>
     
    </address>
    
    
  </div>
  <div class="col-sm-3 invoice-col">
  <address> 
      : <?=$start_date?><br>
      : <?=$end_date;?><br>
      
    </address>
  </div>
  <div class="col-sm-6 invoice-col">
  </div>
  <!-- /.col -->
  <!-- /.col -->
  <!-- /.col -->
</div>
<div class="row table-responsive" style="margin-bottom:-20px;">
  
    <table class="table table-skr">
      <thead>
              <tr>
                <th style="vertical-align:middle;" rowspan="3">No Anggota</th>
                <th style="vertical-align:middle;" rowspan="3">Nama Pengguna Layanan</th>
                <th rowspan="1" colspan="2">Jenis Kunjungan</th>
                <th rowspan="1" colspan="4">Usia </th>
                <th style="vertical-align:middle;" rowspan="3">Alamat</th>
                <th style="vertical-align:middle;" rowspan="3">Jenis Pembayaran</th>
                <th style="vertical-align:middle;" rowspan="3">Pelayanan</th>                
                <th style="vertical-align:middle;" rowspan="3">Keterangan</th>
                <!--th style="vertical-align:middle;" rowspan="3">Tanda Tangan</th-->
              </tr>
              <tr>
                <th style="vertical-align:middle;" rowspan="2">Baru</th>
                <th style="vertical-align:middle;" rowspan="2">Lama</th>
                <th style="vertical-align:middle;" colspan="2">Laki-Laki</th>
                <th style="vertical-align:middle;" colspan="2">Perempuan</th>
              </tr>
              <tr>
                <th>0-50</th>
                <th>55 ></th>
                <th>0-50</th>
                <th>55 ></th>
              </tr>
            </thead>
            <tbody>
              <?php foreach($dataPengunjung as $row){
                $jkBaru = $row['bitIsBaru']=="1" ? 'V' : "";
                $jkLama = $row['bitIsBaru']=="0" ? 'V' : "";
                $lkMuda = (($row['charJkPasien']=="L") && ($row['intUsiaPasienHari'] <= 20075)) ? $row['txtUsiaPasienKunjungan'] : "";
                $lkTua = (($row['charJkPasien']=="L") && ($row['intUsiaPasienHari'] >= 20075)) ? $row['txtUsiaPasienKunjungan'] : "";;
                $prMuda = (($row['charJkPasien']=="P") && ($row['intUsiaPasienHari'] <= 20075)) ? $row['txtUsiaPasienKunjungan'] : "";
                $prTua = (($row['charJkPasien']=="P") && ($row['intUsiaPasienHari'] >= 20075)) ? $row['txtUsiaPasienKunjungan'] : "";;
                $ttdPasien = !empty($row['txtTtdPasien']) ? "<img src=".BASE_URL.$row['txtTtdPasien']. " width='100' alt=''>" : "";?> 
                                        <tr>
                                        <td><?=$row['txtNoAnggota']?></td>
                                         <td> <?=$row['txtNamaPasien']?></td>
                                          <td><?=$jkBaru?></td>
                                          <td><?=$jkLama?></td>
                                          <td><?=$lkMuda?></td>
                                          <td><?=$lkTua?></td>
                                          <td><?=$prMuda?></td>
                                          <td><?=$prTua?></td>
                                          <td><?=$row['NamaKelurahan']?></td>
                                          <td><?=$row['txtJaminanKesehatan']?></td>
                                          <td><?=$row['txtNamaPelayanan']?></td>
                                          <td><?=$row['txtKeteranganKunjungan']?></td>
                                          </tr>               
      <?php } ?>
            </tbody>
          </table>
        </div>

</section>