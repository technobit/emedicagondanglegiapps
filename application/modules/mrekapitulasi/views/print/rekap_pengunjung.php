<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>Print Rekapitulasi Loket </title>
   <link href="<?=ASSETS_URL?>/img/icon.png" rel="shortcut icon" />
  <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.2 -->
    <link href="<?=ASSETS_URL?>bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="<?=ASSETS_URL?>dist/css/AdminLTE.css" rel="stylesheet" type="text/css" />
    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins 
         folder instead of downloading all of them to reduce the load. -->
    <link href="<?=ASSETS_URL?>dist/css/skins/_all-skins.css" rel="stylesheet" type="text/css" />
    <link href="<?=ASSETS_URL?>custom/css/global.css" rel="stylesheet" type="text/css" />
    <link href="<?=ASSETS_URL?>plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="<?=ASSETS_URL?>plugins/datepicker/datepicker3.css" rel="stylesheet" type="text/css" />
    <!-- new post -->
    <!-- date and time picker -->
    <!-- Bootstrap datetime Picker -->
</head>
<body onload="window.print()">
<section class="invoice">
<div class="row text-center" style="">
    <div class="col-sm-12 invoice-col header-skr">
      
      <h1><b>Puskesmas Bululawang</b><h1>
    </div>
    
    
</div>

</div>
<div class="row invoice-info header-profile">
<div class="col-sm-3 invoice-col">
<h4><b><?=$titleRecap?></b></h4>
</div>

</div>
<div class="row invoice-info header-profile">
  <div class="col-sm-3 invoice-col">
    <address> 
      Tanggal Awal<br>
      Tanggal Akhir<br>
     
    </address>
    
    
  </div>
  <div class="col-sm-3 invoice-col">
  <address> 
      : <?=$start_date?><br>
      : <?=$end_date?><br>
      
    </address>
  </div>
  <div class="col-sm-6 invoice-col">
  </div>
  <!-- /.col -->
  <!-- /.col -->
  <!-- /.col -->
</div>
<div class="row table-responsive" style="margin-bottom:-20px;">
  
    <table class="table table-skr">
    <thead>
    <tr>
                <th>No Anggota</th>
                <th>Nama Pengguna Layanan</th>
                <th>Usia Pengguna Layanan</th>
                <th>Jenis Kelamin</th>
                <th>Alamat</th>
                <th>Jenis Pembayaran</th>
                <th>Jenis Kunjungan</th>
                <th>Jenis Wilayah Kunjungan</th>
                <th>Jenis Kasus</th>
                <th>Kode Penyakit - ICD X</th>
                <th>Pengobatan</th>
                <th>Keterangan</th>
                <th>Pelayanan</th>                

              </tr>
              </thead>
            <tbody>
              <?php 
                 if($countDataPengunjung > 0) {
           
      foreach($dataPengunjung as $row){
                $jenisKelamin = $row['charJkPasien']=="L" ? "Laki-Laki" : "Perempuan";
                $jenisKunjungan = $row['bitIsBaru']=="1" ? "Baru" : "Lama";
                $jenisKasus = $row['bitIsKasusBaru']=="1" ? "Baru" : ($row['bitIsKasusBaru']=="0" ? "Lama" : "");
                $jenisWilayah = $row['txtWilayahPasien']=="dalam" ? "Dalam" : "Luar";
                $kodeICDX = $row['txtCategory']; 
                $ttdPasien = !empty($row['txtTtdPasien']) ? "<img src=".BASE_URL.$row['txtTtdPasien']. " width='100' alt=''>" : ""; 
                ?> 
                                        <tr>
                                          <td><?=$row['txtNoAnggota']?></td>
                                          <td><?=$row['txtNamaPasien']?></td>
                                          <td><?=$row['txtUsiaPasienKunjungan']?></td>
                                          <td><?=$jenisKelamin?></td>
                                          <td><?=$row['NamaKelurahan']?></td>
                                          <td><?=$row['txtJaminanKesehatan']?></td>
                                          <td><?=$jenisKunjungan?></td>
                                          <td><?=$jenisWilayah?></td>
                                          <td><?=$jenisKasus?></td>
                                          <td><?=$kodeICDX?></td>
                                          <td><?=$row['txtPengobatan']?></td>
                                          <td><?=$row['txtKeteranganKunjungan']?></td>
                                          <td><?=$row['txtNamaPelayanan']?></td>
                                          </tr>               
      <?php } } ?>
            </tbody>
          </table>
        </div>

</section>