<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>Print Rekapitulasi Loket </title>
   <link href="<?=ASSETS_URL?>/img/icon.png" rel="shortcut icon" />
  <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.2 -->
    <link href="<?=ASSETS_URL?>bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="<?=ASSETS_URL?>dist/css/AdminLTE.css" rel="stylesheet" type="text/css" />
    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins 
         folder instead of downloading all of them to reduce the load. -->
    <link href="<?=ASSETS_URL?>dist/css/skins/_all-skins.css" rel="stylesheet" type="text/css" />
    <link href="<?=ASSETS_URL?>custom/css/global.css" rel="stylesheet" type="text/css" />
    <link href="<?=ASSETS_URL?>plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="<?=ASSETS_URL?>plugins/datepicker/datepicker3.css" rel="stylesheet" type="text/css" />
    <!-- new post -->
    <!-- date and time picker -->
    <!-- Bootstrap datetime Picker -->
</head>
<body onload="window.print()">
<section class="invoice">
<div class="row text-center" style="">
    <div class="col-sm-12 invoice-col header-skr">
      
      <h1><b>Puskesmas Bululawang</b><h1>
    </div>
    
    
</div>

</div>
<div class="row invoice-info header-profile">
<div class="col-sm-3 invoice-col">
<h4><b><?=$titleRecap?></b></h4>
</div>

</div>
<div class="row invoice-info header-profile">
  <div class="col-sm-3 invoice-col">
    <address> 
      Tanggal Awal<br>
      Tanggal Akhir<br>
     
    </address>
    
    
  </div>
  <div class="col-sm-3 invoice-col">
  <address> 
      : <?=$start_date?><br>
      : <?=$end_date;?><br>
      
    </address>
  </div>
  <div class="col-sm-6 invoice-col">
  </div>
  <!-- /.col -->
  <!-- /.col -->
  <!-- /.col -->
</div>
<div class="row table-responsive" style="margin-bottom:-20px;">
  
    <table class="table table-skr">
      <thead>
               <tr>
                <th>No</th>
                <th>Tanggal</th>
                <th>Nama Peserta</th>
                <th>Nomor JKN</th>
                <th>NIK</th>
                <th>Alamat</th>
              </tr>
            </thead>
            <tbody>
              <?php 
              $no = 0;
              foreach($dataPengunjung as $row){
              $no++;
               $ttd = $this->rekapitulasi_model->getTtd($row['txtIdRekamMedis']);
                $ttdPasien = !empty($ttd['txtTandaTangan']) ? "<img src=".BASE_URL.$ttd['txtTandaTangan']. " width='100' alt=''>" : ""; 
				
              ?> 
                                        <tr>
                                          <td><?=$no?></td>
                                          <td><?=indonesian_date($row['dtTanggalKunjungan'])?></td>
                                          <td><?=$row['txtNamaPasien']?></td>
                                          <td><?=$row['txtNoJaminanKesehatan']?></td>
                                          <td><?=$row['txtNoKk']?></td>
                                          <td><?=$row['txtAlamatPasien']?></td>
                                          </tr>               
      <?php } ?>
            </tbody>
          </table>
        </div>

</section>