<?php
class Identitas extends MY_Controller {
    var $base_url = "";
	  var $limit = "20";
    var $menu = "K01";
    var $date_now = "";

	function __construct(){
        parent::__construct();
		$this->load->model("Mrawat_inap_identitas_m" , "Rawat_inap_identitas");
        $this->load->model("register_model");
        $this->load->model("pasien_model");
        $this->base_url = $this->base_url_site."rawat-inap/";
        $this->date_now = date("Y-m-d H:i:s");
    }


    public function saveData(){
		if(!$this->input->is_ajax_request()){
			echo "Die, Ilegall Access";
			die;
		}

		$id = $this->input->post("intIdRekmedDetailAnc");
		$arrPost = array(
						"txtKeluhanPasien" => $this->input->post("txtKeluhanPasien"),
						"bitIsBukuKia"=> $this->input->post("bitIsBukuKia"),
						"txtBeratBadan"=> $this->input->post("txtBeratBadan"),
						"txtTD"=> $this->input->post("txtTD"),
						"txtNadi"=> $this->input->post("txtNadi"),
						"txtRR"=> $this->input->post("txtRR"),
						"txtAbdomen"=> $this->input->post("txtAbdomen"),
						"txtOedemTungkai"=> $this->input->post("txtOedemTungkai"),
						"txtTFU"=> $this->input->post("txtTFU"),
						"txtLetakJanin"=> $this->input->post("txtLetakJanin"),
						"txtDJJ"=> $this->input->post("txtDJJ"),
						"txtGerakJanin"=> $this->input->post("txtGerakJanin"),
						"txtUmurKehamilan"=> $this->input->post("txtUmurKehamilan"),
						"txtPenyuluhan"=> $this->input->post("txtPenyuluhan"),
						"txtPenunjang"=> $this->input->post("txtPenunjang"),
						"txtSkor"=> $this->input->post("txtSkor"),
						"txtKesimpulan"=> $this->input->post("txtKesimpulan"),
						"txtTerapi"=> $this->input->post("txtTerapi"),
						"txtRujukan"=> $this->input->post("txtRujukan")
					);
		if(empty($id)){
			$arrPost['dtCreatedDate'] = $this->date_now;
			$arrPost['dtLastUpdate'] = $this->date_now;
			$arrPost['intIdPegawai'] = $this->session->userdata("sipt_user_id");
			$arrPost['intIdRekamMedisIbu'] = $this->input->post("intIdRekamMedisIbu");
		}else{
			$arrPost['intIdPegawai'] = $this->session->userdata("sipt_user_id");
			$arrPost['dtLastUpdate'] = $this->date_now;
		}

		$retVal = $this->pelayanan_anc->saveUpdate($arrPost , $id);
		$this->setJsonOutput($retVal);

	}

    public function getForm($idKunjungan = ""){
		if(!$this->input->is_ajax_request()){
			echo "Die, Ilegall Access";
			die;
		}
        $mode = "insert";
        $detail_register = $this->register_model->getDetailRegister($idKunjungan);
        $idPasien = $detail_register['txtIdPasien'];
        $detail_pasien = $this->pasien_model->getDetail($idPasien);
        $this->form_builder->form_type = "inline";
        $personAge = personAge($detail_pasien['dtTanggalLahir']);
        $tanggalMasuk = date("Y-m-d");
        ///echopre($detail_pasien);die;
        $dt['frm_id_pasien'] = $this->form_builder->inputHidden("txtIdPasien" , $idPasien);
        $dt['frm_nama_pasien'] = $this->form_builder->inputText("Nama Pengguna Layanan" , "txtNamaPasien",$detail_pasien['txtNamaPasien'] , "col-sm-3" , array("readonly" => "readonly"));
        $dt['frm_umur_pasien'] = $this->form_builder->inputText("Umur Pengguna Layanan" , "txtUmurPasien",$personAge , "col-sm-3" , array("readonly" => "readonly"));

        ///$dt['frm_jk_pasien'] = $this->form_builder->inputText("Jenis Kelamin" , "txtUmurPasien","");
        $dt['frm_jk_pasien'] = $this->form_builder->inputDropdown("Jenis Kelamin" , "txtJenisKelamin" , $detail_pasien['charJkPasien'] , $this->config->item("jk_list"), array("readonly" => "readonly"));
        $dt['frm_telpon_pasien'] = $this->form_builder->inputText("No Telpon" , "txtNoTelpon",$detail_pasien['txtNoHandphone'] ,"col-sm-3" , array("readonly" => "readonly"));
        ///$dt['frm_agama'] = $this->form_builder->inputText("Agama" , "txtAgama","" );
        ///$dt['frm_alamat'] = $this->form_builder->inputText("Alamat" , "txtAlamat",$detail_pasien['txtAlamat']);
        $dt['frm_agama'] = $this->form_builder->inputDropdown("Agama" , "txtAgama" , $detail_pasien['txtAgama'] , $this->config->item("agama_list") , array("readonly" => "readonly"));
        $dt['frm_pendidikan'] = $this->form_builder->inputText("Pendidikan" , "txtPendidikan","" );
        $dt['frm_tanggal_masuk'] = $this->form_builder->inputText("Tanggal Masuk" , "txtTanggalMasuk",$tanggalMasuk);
        /// Form Keluarga Pasien
		
        $dt['frm_nama_keluarga'] = $this->form_builder->inputText("Nama Keluarga" , "txtNamaKeluarga","" );
        $dt['frm_usia_keluarga'] = $this->form_builder->inputText("Umur Kerabat" , "txtUmurKeluarga","" );
        $dt['frm_jk_keluarga'] = $this->form_builder->inputDropdown("Jenis Kelamin" , "txtJenisKelaminKeluarga" , "" , $this->config->item("jk_list"));
        $dt['frm_alamat_keluarga'] = $this->form_builder->inputText("Alamat" , "txtAlamatKeluarga","" );
        $dt['frm_telpon_keluarga'] = $this->form_builder->inputText("No Telpon Kerabat" , "txtNoTelponKeluarga","" );
        ///$dt['frm_agama_keluarga'] = $this->form_builder->inputText("Agama" , "txtAgama","" );
        $dt['frm_agama_keluarga'] = $this->form_builder->inputDropdown("Agama" , "txtAgama" , $detail_pasien['txtAgama'] , $this->config->item("agama_list"));
        $dt['frm_pendidikan_keluarga'] = $this->form_builder->inputText("Pendidikan" , "txtPendidikan","" );
        $dt['frm_hubungan_keluarga'] = $this->form_builder->inputText("Hubungan Status Keluarga" , "txtHubungan","" );
        $dt['buttonLabel'] = $mode == "insert" ? "Simpan" : "Update";
		$retVal = $this->load->view("form_identitas" , $dt , true);
		echo $retVal;
	}
}
