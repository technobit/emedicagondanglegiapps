<?php
class Diagnosis extends MY_Controller {
    var $base_url = "";
	var $limit = "20";
    var $menu = "K01";
    var $date_now = "";
    
	function __construct(){
        parent::__construct();
		$this->load->model("rekam_medis_model" , "rekam_medis");
		$this->load->model("rekam_medis_diagnosa_m" , "rekam_medis_diagnosa");
        $this->base_url = $this->base_url_site."rekam-medis/";
        $this->date_now = date("Y-m-d H:i:s");
    }
    
    
    public function getFrmDiagnosis($idDetailRekamMedis = ""){
		if(!$this->input->is_ajax_request()){
			echo "Die, Ilegall Access";
			die;
		}

		$labelSave = "Simpan";
        $detailDiagnosa = $this->rekam_medis->getDetailDiagnosa($idDetailRekamMedis);
		///echopre($detailDiagnosa);die;
		
        $addDiagnosa = "";
		$addDiagnosaKeluarga = "";
		$addDiagnosaSosial = "";
        if(!empty($detailDiagnosa)){
            $labelSave = "Update";
        }

        $arrSelected = array();
		$arrSelectedKeluarga = array();
		$arrSelectedSosial = array();

		//// Diagnosa
		$dt['frm_id_detail_rekmed'] = $this->form_builder->inputHidden("txtIdRekmedDetail" , $idDetailRekamMedis);
        $dt['frm_rekmed_diagnosa'] = $this->form_builder->inputDropdown("Diagnosa Penyakit" , "selectDiagnosa[]" ,$arrSelected , $arrSelected , array("class"=>"form-control select-diagnosa"));
		$dt['frm_rekmed_detail_diagnosa'] = $this->form_builder->inputTextArea("Detail Diagnosa" , "txtDetailDiagnosa[]", "", "col-sm-3" );
		$dt['frm_no_diagosa'] = $this->form_builder->inputHidden("noDiagnosa" , 1);
		$dt['frm_button_add_diagnosa'] = '';
		$dt['frm_add_diagosa'] = $addDiagnosa;

		if(!empty($detailDiagnosa)){
			$arrSelected = array();
			$dataDiagnosa = array_slice($detailDiagnosa,0,1);
			$listDiagnosa = array_slice($detailDiagnosa,1,count($detailDiagnosa));
			$arrSelected = array($dataDiagnosa[0]['id'] => $detailDiagnosa[0]['txtIndonesianName']);
			$detailDiagnosa = $dataDiagnosa[0]['txtDetailDiagnosa'];

			$dt['frm_rekmed_diagnosa'] = $this->form_builder->inputDropdown("Diagnosa Penyakit" , "selectDiagnosa[]" ,$arrSelected, $arrSelected, array("class"=>"form-control select-diagnosa"));
			$dt['frm_rekmed_detail_diagnosa'] = $this->form_builder->inputTextArea("Detail Diagnosa" , "txtDetailDiagnosa[]", $detailDiagnosa, "col-sm-3" );
			$dt['frm_no_diagosa'] = $this->form_builder->inputHidden("noDiagnosa" , 1);
			
			if(count($listDiagnosa) > 0){
				$number = 1;
				foreach($listDiagnosa as $rowDiagnosa){
					$arrSelected = array($rowDiagnosa['id'] => $rowDiagnosa['txtIndonesianName']);	
					$addDiagnosa .= '<div class="form-group" id="contSelect'.$number.'">
									 <label class="col-sm-3 control-label form-label">Diagnosa Penyakit '.($number+1).'</label>
									 <div class="col-sm-9">
									 '.form_dropdown("selectDiagnosa[]",$arrSelected , $arrSelected , 'id="selectDiagnosa'.$number.'" class="form-control select-diagnosa"').'
									 </div>
									 </div>';
					$addDiagnosa .= '<div class="form-group" id="contText'.$number.'">
									 <label for="txtDetailDiagnosa1" class="col-sm-3 control-label form-label">Detail Diagnosa</label>
									 <div class="col-sm-9">
									 <textarea name="txtDetailDiagnosa[]" cols="3" rows="2" id="txtDetailDiagnosa'.$number.'" class="form-control">'.$rowDiagnosa['txtDetailDiagnosa'].'</textarea>
									 </div>
									 </div>';
					$addDiagnosa .=  '<div class="col-sm-9 col-sm-offset-3"><div class="form-group text-left" id="contBtn'.$number.'"><button type="button" class="btn btn-default btn-flat" style="margin-left:10px;" onclick="hapusForm(\'1\','.$number.')">Hapus Diagnosa '.$number.'</button></div></div>';
					
					$number++;
				}
				$dt['frm_no_diagosa'] = $this->form_builder->inputHidden("noDiagnosa" , $number);
			}	
			$dt['frm_add_diagosa'] = $addDiagnosa;
		}

		$detailDiagnosaKeluarga = $this->rekam_medis_diagnosa->getDetail($idDetailRekamMedis , 1);
		///echopre($detailDiagnosaKeluarga);
		//// Diagnosa Keluarga
		$dt['frm_rekmed_diagnosa_keluarga'] = $this->form_builder->inputDropdown("Diagnosa Penyakit Keluarga" , "selectDiagnosaKeluarga[]" ,$arrSelectedKeluarga , $arrSelectedKeluarga , array("class"=>"form-control select-diagnosa"));
		$dt['frm_rekmed_detail_diagnosa_keluarga'] = $this->form_builder->inputTextArea("Detail Diagnosa Keluarga" , "txtDetailDiagnosaKeluarga[]", "", "col-sm-3" );
		$dt['frm_no_diagosa_keluarga'] = $this->form_builder->inputHidden("noDiagnosaKeluarga" , 1);
		$dt['frm_button_add_diagnosa_keluarga'] = '';
		$dt['frm_add_diagosa_keluarga'] = "";

		if(!empty($detailDiagnosaKeluarga)){
			$arrSelected = array();
			$dataDiagnosaKeluarga = array_slice($detailDiagnosaKeluarga,0,1);
			///echopre($dataDiagnosaKeluarga)
			$listDiagnosaKeluarga = array_slice($detailDiagnosaKeluarga,1,count($detailDiagnosaKeluarga));
			$arrSelectedKeluarga = array($dataDiagnosaKeluarga[0]['id'] => $dataDiagnosaKeluarga[0]['txtIndonesianName']);
			$detailDiagnosaKeluarga = $dataDiagnosaKeluarga[0]['txtDetailDiagnosa'];

			$dt['frm_rekmed_diagnosa_keluarga'] = $this->form_builder->inputDropdown("Diagnosa Penyakit Keluarga" , "selectDiagnosaKeluarga[]" ,$arrSelectedKeluarga, $arrSelectedKeluarga, array("class"=>"form-control select-diagnosa"));
			$dt['frm_rekmed_detail_diagnosa_keluarga'] = $this->form_builder->inputTextArea("Detail Diagnosa Keluarga" , "txtDetailDiagnosaKeluarga[]", $detailDiagnosaKeluarga, "col-sm-3" );
			
			if(count($listDiagnosaKeluarga) > 0){
				$number = 1;
				foreach($listDiagnosaKeluarga as $rowDiagnosa){
					$arrSelectedKeluarga = array($rowDiagnosa['id'] => $rowDiagnosa['txtIndonesianName']);	
					$addDiagnosaKeluarga .= '<div class="form-group" id="contSelectKeluarga'.$number.'">
									 <label class="col-sm-3 control-label form-label">Diagnosa Penyakit Keluarga'.($number+1).'</label>
									 <div class="col-sm-9">
									 '.form_dropdown("selectDiagnosaKeluarga[]",$arrSelectedKeluarga , $arrSelectedKeluarga , 'id="selectDiagnosaKeluarga'.$number.'" class="form-control select-diagnosa"').'
									 </div>
									 </div>';
					$addDiagnosaKeluarga .= '<div class="form-group" id="contTextKeluarga'.$number.'">
									 <label for="txtDetailDiagnosa1" class="col-sm-3 control-label form-label">Detail Diagnosa</label>
									 <div class="col-sm-9">
									 <textarea name="txtDetailDiagnosaKeluarga[]" cols="3" rows="2" id="txtDetailDiagnosaKeluarga'.$number.'" class="form-control">'.$rowDiagnosa['txtDetailDiagnosa'].'</textarea>
									 </div>
									 </div>';
					$addDiagnosaKeluarga .=  '<div class="col-sm-9 col-sm-offset-3"><div class="form-group text-left" id="contBtnKeluarga'.$number.'"><button type="button" class="btn btn-default btn-flat" style="margin-left:10px;" onclick="hapusForm(\'1\','.$number.')">Hapus Diagnosa '.$number.'</button></div></div>';
					
					$number++;
				}
				$dt['frm_no_diagosa_keluarga'] = $this->form_builder->inputHidden("noDiagnosaKeluarga" , $number);
			}	
			$dt['frm_add_diagosa_keluarga'] = $addDiagnosaKeluarga;
		}

		/// Diagnosa Sosial
		$detailDiagnosaSosial = $this->rekam_medis_diagnosa->getDetail($idDetailRekamMedis , 2);

		$dt['frm_rekmed_diagnosa_sosial'] = $this->form_builder->inputDropdown("Diagnosa Penyakit Lingkungan" , "selectDiagnosaSosial[]" ,$arrSelectedSosial , $arrSelectedSosial , array("class"=>"form-control select-diagnosa"));
		$dt['frm_rekmed_detail_diagnosa_sosial'] = $this->form_builder->inputTextArea("Detail Diagnosa Lingkungan" , "txtDetailDiagnosaSosial[]", "", "col-sm-3" );
		$dt['frm_no_diagosa_sosial'] = $this->form_builder->inputHidden("noDiagnosaSosial" , 1);
		$dt['frm_button_add_diagnosa_sosial'] = '';
		$dt['frm_add_diagosa_sosial'] = $addDiagnosaSosial;

		if(!empty($detailDiagnosaSosial)){
			$dataDiagnosaSosial = array_slice($detailDiagnosaSosial,0,1);
			///echopre($dataDiagnosaKeluarga)
			$listDiagnosaSosial = array_slice($detailDiagnosaSosial,1,count($detailDiagnosaSosial));
			$arrSelectedSosial = array($dataDiagnosaSosial[0]['id'] => $dataDiagnosaSosial[0]['txtIndonesianName']);
			$detailDiagnosaSosial = $dataDiagnosaSosial[0]['txtDetailDiagnosa'];

			$dt['frm_rekmed_diagnosa_sosial'] = $this->form_builder->inputDropdown("Diagnosa Penyakit Lingkungan" , "selectDiagnosaSosial[]" ,$arrSelectedSosial, $arrSelectedSosial, array("class"=>"form-control select-diagnosa"));
			$dt['frm_rekmed_detail_diagnosa_sosial'] = $this->form_builder->inputTextArea("Detail Diagnosa Lingkungan" , "txtDetailDiagnosaSosial[]", $detailDiagnosaSosial, "col-sm-3" );
			
			if(count($listDiagnosaSosial) > 0){
				$number = 1;
				foreach($listDiagnosaSosial as $rowDiagnosa){
					$arrSelectedSosial = array($rowDiagnosa['id'] => $rowDiagnosa['txtIndonesianName']);	
					$addDiagnosaSosial .= '<div class="form-group" id="contSelectSosial'.$number.'">
									 <label class="col-sm-3 control-label form-label">Diagnosa Lingkungan Keluarga'.($number+1).'</label>
									 <div class="col-sm-9">
									 '.form_dropdown("selectDiagnosaSosial[]",$arrSelectedSosial , $arrSelectedSosial , 'id="selectDiagnosaKeluarga'.$number.'" class="form-control select-diagnosa"').'
									 </div>
									 </div>';
					$addDiagnosaSosial .= '<div class="form-group" id="contTextDiagnosa'.$number.'">
									 <label for="txtDetailDiagnosa1" class="col-sm-3 control-label form-label">Detail Diagnosa</label>
									 <div class="col-sm-9">
									 <textarea name="txtDetailDiagnosaSosial[]" cols="3" rows="2" id="txtDetailDiagnosaSosial'.$number.'" class="form-control">'.$rowDiagnosa['txtDetailDiagnosa'].'</textarea>
									 </div>
									 </div>';
					$addDiagnosaSosial .=  '<div class="col-sm-9 col-sm-offset-3"><div class="form-group text-left" id="contBtnKeluarga'.$number.'"><button type="button" class="btn btn-default btn-flat" style="margin-left:10px;" onclick="hapusForm(\'3\','.$number.')">Hapus Diagnosa '.$number.'</button></div></div>';
					
					$number++;
				}
				$dt['frm_no_diagosa_sosial'] = $this->form_builder->inputHidden("noDiagnosaSosial" , $number);
			}	
			$dt['frm_add_diagosa_sosial'] = $addDiagnosaSosial;
		}

		$dt['buttonLabel'] = $labelSave;
		$retVal = $this->load->view("rawat_inap/form_diagnosa" , $dt , true);
		echo $retVal;
	}
    
    public function saveData(){
		if(!$this->input->is_ajax_request()){
			echo "Die, Ilegall Access";
			die;
		}

		//// Data Diagnosa
		$idDetailRekmed = $this->input->post("txtIdRekmedDetail");
		$diagnosa = $this->input->post("selectDiagnosa");
		$detailDiagnosa = $this->input->post("txtDetailDiagnosa");
		$idRekamMedis = $this->rekam_medis->getIdRekamMedis($idDetailRekmed);

		$checkDiagnosa = $this->rekam_medis->checkDetailDiagnosa($idDetailRekmed);
		if($checkDiagnosa > 0){
			$hapusDiagnosa = $this->rekam_medis->deleteDetailDiagnosa($idDetailRekmed);	
		}
		
		if(is_array($diagnosa)){
			for($indexDiagnosa=0;$indexDiagnosa < count($diagnosa);$indexDiagnosa++){
				if(!empty($diagnosa[$indexDiagnosa])){
					$checkNewCase = $this->rekam_medis->checkNewCaseDetailDiagnosa($idRekamMedis , $diagnosa[$indexDiagnosa]);
					$arrInsertDetailDiagnosa = array(
										"txtIdRekmedDetail"   => $idDetailRekmed,
										"intIdDiagnosaPenyakit" => $diagnosa[$indexDiagnosa],
										"txtDetailDiagnosa"   => $detailDiagnosa[$indexDiagnosa],
										"dtCreatedDate"   => date("Y-m-d H:i:s"),
										"bitIsKasusBaru" => $checkNewCase==true ? 1 : 0,
									);
					$retValDiagnosa = $this->rekam_medis->insertDetailDiagnosa($arrInsertDetailDiagnosa);			
				}
			}
		}else{
			$checkNewCase = $this->rekam_medis->checkNewCaseDetailDiagnosa($idRekamMedis , $diagnosa);
			$arrInsertDetailDiagnosa = array(
									"txtIdRekmedDetail"   => $idDetailRekmed,
									"intIdDiagnosaPenyakit" => $diagnosa,
									"txtDetailDiagnosa"   => $detailDiagnosa,
									"dtCreatedDate"   => date("Y-m-d H:i:s"),
									"bitIsKasusBaru" => $checkNewCase==true ? 1 : 0,
								);
			$retValDiagnosa = $this->rekam_medis->insertDetailDiagnosa($arrInsertDetailDiagnosa);		
		}

		//// Diagnosa Keluarga
		$diagnosaKeluarga = $this->input->post("selectDiagnosaKeluarga");
		$detailDiagnosaKeluarga = $this->input->post("txtDetailDiagnosaKeluarga");
		$checkDiagnosaKeluarga = $this->rekam_medis_diagnosa->checkDataDiagnosa($idDetailRekmed , 1);
		if($checkDiagnosaKeluarga > 0){
			$hapusDiagnosa = $this->rekam_medis_diagnosa->delete($idDetailRekmed , 1);	
		}
		if(!empty($diagnosaKeluarga)){
			for($indexDiagnosa=0;$indexDiagnosa < count($diagnosaKeluarga);$indexDiagnosa++){
				if(!empty($diagnosaKeluarga[$indexDiagnosa])){
					$arrInsertDetailDiagnosaKeluarga = array(
										"txtIdRekmedDetail"   => $idDetailRekmed,
										"intIdDiagnosaPenyakit" => $diagnosaKeluarga[$indexDiagnosa],
										"txtDetailDiagnosa"   => $detailDiagnosaKeluarga[$indexDiagnosa],
										"dtCreatedDate"   => date("Y-m-d H:i:s"),
										"bitJenisDiagnosa" => 1, /// For Keluarga
									);
					$retValDiagnosaKeluarga = $this->rekam_medis_diagnosa->saveData($arrInsertDetailDiagnosaKeluarga);			
				}
			}
		}

		//// Diagnosa Sosial
		$diagnosaSosial = $this->input->post("selectDiagnosaSosial");
		$detailDiagnosaSosial = $this->input->post("txtDetailDiagnosaSosial");
		$checkDiagnosaSosial = $this->rekam_medis_diagnosa->checkDataDiagnosa($idDetailRekmed , 2);
		if($checkDiagnosaSosial > 0){
			$hapusDiagnosa = $this->rekam_medis_diagnosa->delete($idDetailRekmed , 2);	
		}
		if(!empty($diagnosaSosial)){
			for($indexDiagnosa=0;$indexDiagnosa < count($diagnosaSosial);$indexDiagnosa++){
				if(!empty($diagnosaSosial[$indexDiagnosa])){
					$arrInsertDetailDiagnosaSosial = array(
										"txtIdRekmedDetail"   => $idDetailRekmed,
										"intIdDiagnosaPenyakit" => $diagnosaSosial[$indexDiagnosa],
										"txtDetailDiagnosa"   => $detailDiagnosaSosial[$indexDiagnosa],
										"dtCreatedDate"   => date("Y-m-d H:i:s"),
										"bitJenisDiagnosa" => 2, /// For Keluarga
									);
					$retValDiagnosaSosial = $this->rekam_medis_diagnosa->saveData($arrInsertDetailDiagnosaSosial);			
				}
			}
		}
		$retVal = array();
		$status = $retValDiagnosa['status'];
		$retVal['status'] = $retValDiagnosa['status'];
		$retVal['message'] = $status==true ? "Data Berhasil Di Simpan" : "Data Gagal Di Simpan";
		echo json_encode($retVal);
        }
}