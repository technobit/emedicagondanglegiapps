<?php
class Diagnosis extends MY_Controller {
    var $base_url = "";
	var $limit = "20";
    var $menu = "K01";
    var $date_now = "";
    
	function __construct(){
        parent::__construct();
		$this->load->model("rekam_medis_model" , "rekam_medis");
		$this->load->model("mrekam_medis_diagnosa_inap" , "rekam_medis_diagnosa");
        $this->base_url = $this->base_url_site."rekam-medis/";
        $this->date_now = date("Y-m-d H:i:s");
    }
    
    
    public function getFrmDiagnosis($idDetailRekamMedis = ""){
		if(!$this->input->is_ajax_request()){
			echo "Die, Ilegall Access";
			die;
		}
        $mode = 0;
        $link_cetak = $this->base_url_site."rawat-inap/cetak-frm-diagnosis/".$idDetailRekamMedis;
        $labelSave = "Simpan";
        $detaildata = $this->rekam_medis_diagnosa->getData($idDetailRekamMedis);
        if(!empty($detaildata)){
            $labelSave = "Update";
            $link_cetak = $this->base_url_site."rawat-inap/cetak-frm-diagnosis/".$idDetailRekamMedis;
            $mode = "1";
        }

        $arrData = array(
            "txtKeluhan",
            "txtRiwayatSekarang",
            "txtRiwayatDulu",
            "txtRiwayatKeluarga",
            "txtRiwayatSosial",
        );        

        foreach($arrData as $rowData){
            $$rowData = isset($detaildata[$rowData]) ? $detaildata[$rowData] : ""; 
        }

        $dt['txtIdRekmedDetail'] = $this->form_builder->inputHidden("txtIdRekmedDetail" , $idDetailRekamMedis);
        $dt['mode_frm'] = $this->form_builder->inputHidden("mode" , $mode,array('id'=>'mode'));
        $dt['txtKeluhan'] = $this->form_builder->inputTextArea("Keluhan" , "txtKeluhan" , $txtKeluhan);
        $dt['txtRiwayatSekarang'] = $this->form_builder->inputTextArea("Riwayat Penyakit Sekarang" , "txtRiwayatSekarang" , $txtRiwayatSekarang);    
        $dt['txtRiwayatDulu'] = $this->form_builder->inputTextArea("Riwayat Penyakit Dahulu" , "txtRiwayatDulu" , $txtRiwayatDulu);    
        $dt['txtRiwayatKeluarga'] = $this->form_builder->inputTextArea("Riwayat Penyakit Keluarga" , "txtRiwayatKeluarga" , $txtRiwayatKeluarga);
        $dt['txtRiwayatSosial'] = $this->form_builder->inputTextArea("Lain-Lain" , "txtRiwayatSosial" , $txtRiwayatSosial);
		$dt['buttonLabel'] = $labelSave;
        $dt['mode'] = $mode;
        $dt['link_cetak'] = $link_cetak;
		$retVal = $this->load->view("form_diagnosis_v2" , $dt , true);
		echo $retVal;
	}
    
    public function saveData(){
		if(!$this->input->is_ajax_request()){
			echo "Die, Ilegall Access";
			die;
		}

        $post = $this->input->post();
        
		//// Data Diagnosa
		$idDetailRekmed = $this->input->post("txtIdRekmedDetail");
        $arrPost = array(
            "txtIdRekmedDetail" => $idDetailRekmed,
            "txtKeluhan" => $post['txtKeluhan'],
            "txtRiwayatSekarang"=> $post['txtRiwayatSekarang'],
            "txtRiwayatDulu"=> $post['txtRiwayatDulu'],
            "txtRiwayatKeluarga"=> $post['txtRiwayatKeluarga'],
            "txtRiwayatSosial"=> $post['txtRiwayatSosial'],
        );

        $retValDiagnosa = $this->rekam_medis_diagnosa->saveUpdate($arrPost , $idDetailRekmed);
        
		echo json_encode($retValDiagnosa);
        }
     public function cetak_form($idDetailRekamMedis) {
		
		
		$dt = array(
		"title" => "Rawat Inap - Cetak Anamnesis",
		"description" => "Rawat Inap - Cetak Anamnesis",
		"container" => $this->_build_cetak_form($idDetailRekamMedis),
		"menu_key" => "cetak_assesment_planning",
		"akses_key" => "is_view",
		"custom_js" => array(),
		"custom_css" => array(),
		);
		
		$this->_render("default",$dt);
		
	}
	
	
	private function _build_cetak_form($idDetailRekamMedis){
		
		
		$breadcrumbs = array(
		"Home" =>$this->base_url_site,
		"Rawat Inap" => $this->base_url,
		"Cetak Anamnesis" => "#",
		);
         $idRekamMedis = $this->rekam_medis->getIdRekamMedisByDetail($idDetailRekamMedis);
		$resDetailInfo = $this->rekam_medis->getDetailRawatInap($idRekamMedis['txtIdRekamMedis'] , $idDetailRekamMedis);
        $detailData = $this->rekam_medis_diagnosa->getData($idDetailRekamMedis);
        $dtTanggalKunjungan = explode(" ",$resDetailInfo['dtTanggalKunjungan']);
        $tanggalKunjungan = $dtTanggalKunjungan;
        $jenisKelamin = $resDetailInfo['charJkPasien']=="L" ? "Laki-Laki" : "Perempuan";
        $dt['tanggalKunjungan'] = $tanggalKunjungan;
        $dt['jenisKelamin'] = $jenisKelamin;
        $dt['breadcrumbs'] = $this->setBreadcrumbs($breadcrumbs);
		$dt['title'] = "Rawat Inap - Cetak Anamnesis";
        $dt['detailPasien'] = $resDetailInfo;		
		$dt['detailData'] =  $detailData;
		return $this->load->view('print_out/form_diagnosis_v2', $dt, true);
		
	}
}