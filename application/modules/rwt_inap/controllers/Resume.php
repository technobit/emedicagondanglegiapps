<?php
class Resume extends MY_Controller {
    var $base_url = "";
	var $limit = "20";
    var $menu = "K01";
    var $date_now = "";
    
	function __construct(){
        parent::__construct();
		$this->load->model("rekam_medis_model" , "rekam_medis");
		$this->load->model("rekam_medis_diagnosa_m" , "rekam_medis_diagnosa");
        $this->load->model("resume_medis_model");
        $this->load->model("pegawai_model");
        $this->load->model("pelayanan_model");
		$this->load->model("data_penyakit_model");
        $this->base_url = $this->base_url_site."rekam-medis/";
        $this->date_now = date("Y-m-d H:i:s");
    }
    
    
    public function getFrmResume($idDetailRekamMedis = ""){
		if(!$this->input->is_ajax_request()){
			echo "Die, Ilegall Access";
			die;
		}

        $date_now = date("Y-m-d H:i:s");
		$labelSave = "Simpan";

        $detailResume = $this->resume_medis_model->getDetail($idDetailRekamMedis);
        $addDiagnosa = "";
		$mode = 0;
		$link_cetak = $this->base_url_site."rawat-inap/cetak-resume-medis/".$idDetailRekamMedis;
        if(!empty($detailResume)){
            $labelSave = "Update";
            $link_cetak = $this->base_url_site."rawat-inap/cetak-resume-medis/".$idDetailRekamMedis;
        	$mode = 2;
        }

        $arrForm = array(
            "intIdPegawai",
            "dtKeluar",
            "txtAnamnesis",
            "txtPeriksaFisik",
            "txtPeriksaPenunjang",
            "txtTerapi",
			"txtDiagnosaMasuk",
			"txtDiagnosaKeluar",
            "txtPrognosis",
            "intKeadaanKeluar",
            "intIdLayananRujukan",
            "txtSaran"
        );

        foreach($arrForm as $rowForm){
            $$rowForm = isset($detailResume[$rowForm]) ? $detailResume[$rowForm] : ""; 
        }

        $dt['frm_id_detail_rekmed'] = $this->form_builder->inputHidden("txtIdRekmedDetail" , $idDetailRekamMedis);
        $dt['dtKeluar'] = $this->form_builder->inputText("Tanggal Keluar" , "dtKeluar" ,$dtKeluar);
        $arrDokter = array();
        $dataPegawai = $this->pegawai_model->getListPegawai();
        foreach($dataPegawai as $rowPegawai){
            $arrDokter[$rowPegawai['intIdPegawai']] = $rowPegawai['txtNamaPegawai'];
        }

        $dt['intIdPegawai'] = $this->form_builder->inputDropdown("Petugas" , "intIdPegawai" , $intIdPegawai , $arrDokter);
        $dt['txtAnamnesis'] = $this->form_builder->inputTextArea("Anamnesis" , "txtAnamnesis" ,$txtAnamnesis);
        $dt['txtPeriksaFisik'] = $this->form_builder->inputTextArea("Pemeriksaan Fisik" , "txtPeriksaFisik" ,$txtPeriksaFisik);
        $dt['txtPeriksaPenunjang'] = $this->form_builder->inputTextArea("Pemeriksaan Penunjang" , "txtPeriksaPenunjang" ,$txtPeriksaPenunjang);
        $dt['txtTerapi'] = $this->form_builder->inputTextArea("Terapi" , "txtTerapi" ,$txtTerapi);
        $dt['txtPrognosis'] = $this->form_builder->inputTextArea("Prognosis" , "txtPrognosis" ,$txtPrognosis);
        $arrKeadaan = $this->config->item("kondisi_keluar");
        $dt['link_cetak'] = $link_cetak;
		$dt['mode'] = $mode;
        $dt['intKeadaanKeluar'] = form_dropdown("intKeadaanKeluar" , $arrKeadaan ,$intKeadaanKeluar, 'id="intKeadaanKeluar" class="form-control" onchange="checkRujukan()"');

        $listRumahSakit = $this->pelayanan_model->getListPelayananByIdJenis(8);
        
        $arrPelayanan = array("0" => "-Pilih Rujukan-");
        foreach($listRumahSakit as $rowData){
            $arrPelayanan[$rowData['intIdPelayanan']] = $rowData['txtNama'];
        }
        ///echopre($listRumahSakit);
        $dt['intIdLayananRujukan'] = form_dropdown("intIdLayananRujukan" , $arrPelayanan , $intIdLayananRujukan , 'id="intIdLayananRujukan" class="form-control" disabled=""');
        $dt['txtSaran'] = $this->form_builder->inputTextArea("Saran" , "txtSaran" ,$txtSaran);

		$arrSelectedDiagnosaMasuk = array();
		$arrSelectedDiagnosaKeluar = array();
        $detailDiagnosa = $this->rekam_medis->getDetailDiagnosa($idDetailRekamMedis);
		//// Diagnosa

		if(!empty($txtDiagnosaMasuk)){
			$detailDiagnosaMasuk = $this->data_penyakit_model->getDetail($txtDiagnosaMasuk);
			$arrSelectedDiagnosaMasuk['intIdPenyakit'] = $detailDiagnosaMasuk['txtEnglishName'];
		}

		if(!empty($txtDiagnosaKeluar)){
			$detailDiagnosaKeluar = $this->data_penyakit_model->getDetail($txtDiagnosaKeluar);
			$arrSelectedDiagnosaKeluar['intIdPenyakit'] = $detailDiagnosaKeluar['txtEnglishName'];
			
		}
		
        $dt['frm_rekmed_diagnosa'] = $this->form_builder->inputDropdown("Diagnosis Masuk" , "selectDiagnosa" ,$txtDiagnosaMasuk , $arrSelectedDiagnosaMasuk , array("class"=>"form-control select-diagnosa","style"=>"width:100%;"));
		$dt['frm_rekmed_diagnosa_keluar'] = $this->form_builder->inputDropdown("Diagnosis Keluar" , "selectDiagnosaKeluar" ,$txtDiagnosaKeluar , $arrSelectedDiagnosaKeluar , array("class"=>"form-control select-diagnosa","style"=>"width:100%;"));
		$dt['frm_add_diagosa'] = $addDiagnosa;

		
        $arrDokter = array();
		$dt['buttonLabel'] = $labelSave;
		$retVal = $this->load->view("form_resume" , $dt , true);
		echo $retVal;
	}
    
    public function saveData(){
		if(!$this->input->is_ajax_request()){
			echo "Die, Ilegall Access";
			die;
		}

        $idDetailRekmed = $this->input->post("txtIdRekmedDetail");
        $arrPostResume = array(
            "txtIdRekmedDetail"=>$this->input->post("txtIdRekmedDetail"),
            "intIdPegawai"=>$this->input->post("intIdPegawai"),
            "dtKeluar"=>$this->input->post("dtKeluar"),
            "txtAnamnesis"=>$this->input->post("txtAnamnesis"),
            "txtPeriksaFisik"=>$this->input->post("txtPeriksaFisik"),
            "txtPeriksaPenunjang"=>$this->input->post("txtPeriksaPenunjang"),
			"txtDiagnosaMasuk"=>$this->input->post("selectDiagnosa"),
			"txtDiagnosaKeluar"=>$this->input->post("selectDiagnosaKeluar"),
            "txtTerapi"=>$this->input->post("txtTerapi"),
            "txtPrognosis"=>$this->input->post("txtPrognosis"),
            "intKeadaanKeluar"=>$this->input->post("intKeadaanKeluar"),
            "intIdLayananRujukan"=>$this->input->post("intIdLayananRujukan"),
            "txtSaran"=>$this->input->post("txtSaran"),
            "bitStatusResume"=>1
        );

        /// Input Diagnosa
        $diagnosa = $this->input->post("selectDiagnosaKeluar");
        $idRekamMedis = $this->rekam_medis->getIdRekamMedis($idDetailRekmed);
        $checkDiagnosa = $this->rekam_medis->checkDetailDiagnosa($idDetailRekmed);
		if($checkDiagnosa > 0){
			$hapusDiagnosa = $this->rekam_medis->deleteDetailDiagnosa($idDetailRekmed);	
		}
		
		$checkNewCase = $this->rekam_medis->checkNewCaseDetailDiagnosa($idRekamMedis , $diagnosa);
		$arrInsertDetailDiagnosa = array(
								"txtIdRekmedDetail"   => $idDetailRekmed,
								"intIdDiagnosaPenyakit" => $diagnosa,
								"dtCreatedDate"   => date("Y-m-d H:i:s"),
								"bitIsKasusBaru" => $checkNewCase==true ? 1 : 0,
							);
		$retValDiagnosa = $this->rekam_medis->insertDetailDiagnosa($arrInsertDetailDiagnosa);	
		//// Data Diagnosa
		$resVal = $this->resume_medis_model->saveUpdate($arrPostResume , $idDetailRekmed);
		$retVal['status'] = $resVal['status'];
		$retVal['message'] = $resVal['message'];
		echo json_encode($retVal);
        }
        public function cetak_form($idDetailRekamMedis) {
		
		
		$dt = array(
		"title" => "Rawat Inap - Resume Medis",
		"description" => "Rawat Inap - Resume Medis",
		"container" => $this->_build_cetak_form($idDetailRekamMedis),
		"menu_key" => "cetak_assesment_planning",
		"akses_key" => "is_view",
		"custom_js" => array(),
		"custom_css" => array(),
		);
		
		$this->_render("default",$dt);
		
	}
	
	
	private function _build_cetak_form($idDetailRekamMedis){
		
		
		$breadcrumbs = array(
		"Home" =>$this->base_url_site,
		"Rawat Inap" => $this->base_url,
		"Cetak Anamnesis" => "#",
		);
		 $detailDiagnosa = $this->rekam_medis->getDetailDiagnosa($idDetailRekamMedis);
		$listRumahSakit = $this->pelayanan_model->getListPelayananByIdJenis(8);
        $dt['arrPelayanan'] = arrayDropdown($listRumahSakit, 'intIdPelayanan','txtNama');
		 $dataPegawai = $this->pegawai_model->getListPegawai();
		$dt['arrPegawai'] = arrayDropdown($dataPegawai, 'intIdPegawai','txtNamaPegawai');
         $idRekamMedis = $this->rekam_medis->getIdRekamMedisByDetail($idDetailRekamMedis);
		$resDetailInfo = $this->rekam_medis->getDetailRawatInap($idRekamMedis['txtIdRekamMedis'] , $idDetailRekamMedis);
        $detailData = $this->resume_medis_model->getDetail($idDetailRekamMedis);
        $dtTanggalKunjungan = explode(" ",$resDetailInfo['dtTanggalKunjungan']);
        $tanggalKunjungan = $dtTanggalKunjungan;
        $jenisKelamin = $resDetailInfo['charJkPasien']=="L" ? "Laki-Laki" : "Perempuan";
        	$dt['rekmed_diagnosa'] =  $detailDiagnosa[0]['txtIndonesianName'];
			$dt['rekmed_detail_diagnosa'] =$detailDiagnosa[0]['txtDetailDiagnosa'];
		
		$dt['tanggalKunjungan'] = $tanggalKunjungan;
        $dt['jenisKelamin'] = $jenisKelamin;
        $dt['breadcrumbs'] = $this->setBreadcrumbs($breadcrumbs);
		$dt['title'] = "Rawat Inap - Resume Medis";
        $dt['detailPasien'] = $resDetailInfo;		
		$dt['detailData'] =  $detailData;
		$dt['arrKeadaan'] = $this->config->item("kondisi_keluar");

		return $this->load->view('print_out/form_resume', $dt, true);
		
	}
}
