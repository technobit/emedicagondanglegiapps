<?php
class Tindakan extends MY_Controller {

    var $base_url = "";
	var $limit = "20";
    var $menu = "K01";
    var $date_now = "";
    
	function __construct(){
        parent::__construct();
		$this->load->model("rekam_medis_model" , "rekam_medis");
		///$this->load->model("pengobatan_model");
        $this->load->model("mrekam_medis_tindakan_m" , "tindakan_model");
        $this->base_url = $this->base_url_site."rawat-inap/";
        $this->date_now = date("Y-m-d H:i:s");
    }
    
    public function getForm($idDetailRekamMedis = "" , $id = ""){
		if(!$this->input->is_ajax_request()){
			echo "Die, Ilegall Access";
			die;
		}
        
        $labelSave = "Simpan";
        $detailTindakan = $this->tindakan_model->getDetail($id);
        if(!empty($detailObat)){
            $labelSave = "Update";
        }

        $arrData = array(
            "dtTanggal",
            "intIdTindakan",
            "txtTindakan",
            "txtDeskripsiTindakan",
        );        

        foreach($arrData as $rowData){
            $$rowData = isset($detailTindakan[$rowData]) ? $detailTindakan[$rowData] : ""; 
        }

        $dtTanggal = !empty($dtTanggal) ? $dtTanggal : date("Y-m-d H:i:s");
        $arrTindakan = !empty($txtTindakan) ? array($intIdTindakan => $txtTindakan) : array();
        $dtJam = !empty($dtJam) ? $dtJam : date("H:i");
        $dt['intIdRekmedTindakanInap'] = $this->form_builder->inputHidden("intIdRekmedTindakanInap" , $id); 
        $dt['txtIdRekmedDetail'] = $this->form_builder->inputHidden("txtIdRekmedDetail" , $idDetailRekamMedis);
        $dt['dtTanggal'] = $this->form_builder->inputText("Tanggal" , "dtTanggal" , $dtTanggal);
        $dt['intIdTindakan'] = $this->form_builder->inputDropdown("Nama Tindakan" , "intIdTindakan" , $intIdTindakan , $arrTindakan , array("class"=>"form-control select-tindakan" , "style"=>"width:100%;"));;
        $dt['txtDeskripsiTindakan'] = $this->form_builder->inputTextArea("Deskripsi Tindakan" , "txtDeskripsiTindakan" , $txtDeskripsiTindakan);
		$dt['buttonLabel'] = $labelSave;
        
		$retVal = $this->load->view("form_tindakan" , $dt , true);
		echo $retVal;
	}
    
    public function saveData(){
		if(!$this->input->is_ajax_request()){
			echo "Die, Ilegall Access";
			die;
		}
        ///echopre($_POST);
        $intIdRekmedTindakanInap = $this->input->post("intIdRekmedTindakanInap");
        $txtIdRekmedDetail = $this->input->post("txtIdRekmedDetail");
        $dtTanggalJam = $this->input->post("dtTanggal");
        $arrPost = array(
            "txtIdRekmedDetail" => $txtIdRekmedDetail,
            "dtTanggal" => $dtTanggalJam,
            "intIdTindakan" => $this->input->post("intIdTindakan"), 
            "txtDeskripsiTindakan" => $this->input->post("txtDeskripsiTindakan"),
            "bitStatusTindakan" => 0
        );
        
        $resVal = $this->tindakan_model->saveUpdate($arrPost , $intIdRekmedTindakanInap);
        $retVal = array();
        $retVal['status'] = $resVal['status'];
        $retVal['message'] = $resVal['message'];
        echo json_encode($retVal);
    }

    public function getDataTindakan(){
        if(!$this->input->is_ajax_request()){
			echo "Die, Ilegall Access";
			die;
		}

        $txtIdRekmedDetail = $this->input->post("txtIdRekmedDetail");
        $dataTindakan = $this->tindakan_model->getData($txtIdRekmedDetail);
        
        $retVal['data'] = array();
        foreach($dataTindakan as $rowTindakan) {
            
            $id = $rowTindakan['intIdRekmedTindakanInap'];
            $btnEdit = '<button onclick="getFormDataTindakan('.$txtIdRekmedDetail.','.$id.')" class="btn btn-flat btn-primary">Detail</button>';
            $btnHapus = '<button onclick="hapusDataTindakan('.$id.')" class="btn btn-flat btn-warning">Hapus</button>';
            $btnAksi = $btnEdit.$btnHapus;
            $arrData = array(
                $rowTindakan['dtTanggal'],
                $rowTindakan['txtTindakan'],
                $rowTindakan['txtDeskripsiTindakan'],
                $btnAksi
            );
            $retVal['data'][] = $arrData;
        }
        $this->setJsonOutput($retVal);
    }

    public function deleteData(){
        if(!$this->input->is_ajax_request()){
			echo "Die, Ilegall Access";
			die;
		}
        $id = $this->input->post("intId");
        $resHapus = $this->tindakan_model->delete($id);
        echo json_encode($resHapus);
    }
    public function cetak_form($idDetailRekamMedis) {
		
		
		$dt = array(
		"title" => "Rawat Inap - Cetak Tindakan",
		"description" => "Rawat Inap - Cetak Tindakan",
		"container" => $this->_build_cetak_form($idDetailRekamMedis),
		"menu_key" => "cetak_assesment_planning",
		"akses_key" => "is_view",
		"custom_js" => array(),
		"custom_css" => array(),
		);
		
		$this->_render("default",$dt);
		
	}
	
	
	private function _build_cetak_form($idDetailRekamMedis){
		
		
		$breadcrumbs = array(
		"Home" =>$this->base_url_site,
		"Rawat Inap" => $this->base_url,
		"Cetak Tindakan" => "#",
		);
         $idRekamMedis = $this->rekam_medis->getIdRekamMedisByDetail($idDetailRekamMedis);
		$resDetailInfo = $this->rekam_medis->getDetailRawatInap($idRekamMedis['txtIdRekamMedis'] , $idDetailRekamMedis);
        $detailData = $this->tindakan_model->getData($idDetailRekamMedis);
        $dtTanggalKunjungan = explode(" ",$resDetailInfo['dtTanggalKunjungan']);
        $tanggalKunjungan = $dtTanggalKunjungan;
        $jenisKelamin = $resDetailInfo['charJkPasien']=="L" ? "Laki-Laki" : "Perempuan";
        $dt['tanggalKunjungan'] = $tanggalKunjungan;
        $dt['jenisKelamin'] = $jenisKelamin;
        $dt['breadcrumbs'] = $this->setBreadcrumbs($breadcrumbs);
		$dt['title'] = "Rawat Inap - Cetak Tindakan";
        $dt['detailPasien'] = $resDetailInfo;		
		$dt['detailData'] =  $detailData;
       
        
		return $this->load->view('print_out/cetak_tindakan', $dt, true);
		
	}
}