<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mrekam_medis_keluarga_m extends CI_Model {

	var $table = "rekam_medis_keluarga";
	var $primary_key = "intIdKeluarga";
    
    public function __construct(){
        parent::__construct();
        
    }
    
	public function getDetail($id){
		$this->db->where($this->primary_key , $id);
		$query = $this->db->get($this->table,1);
		$resVal = "";
		if($query->num_rows() > 0){
			$resVal = $query->row_array();	
		}else{
			$resVal = false;
		}
		return $resVal;
	}
	
	public function saveData($arrData = array(), $id=""){
		$retVal = array();
        if(!empty($id)){
            $this->db->where($this->primary_key , $id);
		    $res = $this->db->update($this->table, $arrData);
        }else{
            $this->db->set($arrData);
            $res = $this->db->insert($this->table);
            $id = $this->db->insert_id();
        }
        $status = $res;
        $message = $status==true ? "Data Berhasil Di Simpan" : "Data Gagal Di Simpan";
        $retVal['message'] = $message;
        $retVal['status'] = $status;
        $retVal['id'] = $id;
        return $retVal;
	}
	
	public function delete($id){
		$this->db->where($this->primary_key , $id);
		$q = $this->db->delete($this->table);
		
		if(!$q){
			$retVal['error_stat'] = "Data Gagal Di Hapus";
			$retVal['status'] = false;
		}else{
			$retVal['error_stat'] = "Data Berhasil Di Hapus";
			$retVal['status'] = true;
		}
		
		return $retVal;
	}
	
}

