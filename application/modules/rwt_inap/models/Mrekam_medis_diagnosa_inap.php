<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mrekam_medis_diagnosa_inap extends CI_Model {
	
	/// Master Identifier
	var $table = "rekam_medis_diagnosa_inap";	
	var $primary_key = "intIdDiagnosaInap";
	var $secondar_key = "txtIdRekmedDetail";
    
    public function __construct(){
        parent::__construct();
    }

	public function getData($idRekamMedisDetail){
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->where($this->secondar_key , $idRekamMedisDetail);
		$data = $this->db->get();
		return $data->row_array();
	}
	
    public function getCountData($idRekamMedis){
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->where($this->secondar_key , $idRekamMedis);
		$data = $this->db->count_all_results();
		return $data;
	}
	
	public function saveUpdate($array , $id){
		$query = false;
        $jumlah = $this->getCountData($id);
        //echo $jumlah;die;
		if($jumlah<1){
			$this->db->set($array);
			$query = $this->db->insert($this->table);
			$id = $this->db->insert_id();
		}else{
			$this->db->where($this->secondar_key , $id);
			$query = $this->db->update($this->table, $array);
		}
		$retVal = array();
		if(!$query){
			$retVal['message'] = "Data Gagal Di perbarui";
			$retVal['status'] = false;
			$retVal['id'] = 0;
		}else{
			$retVal['message'] = "Data Berhasil Di Perbarui";
			$retVal['status'] = true;
			$retVal['id'] = $id;
		}
		return $retVal;
	}
	
	public function delete($id){
		$this->db->where($this->primary_key , $id);
		$q = $this->db->delete($this->table);
		
		if(!$q){
			$retVal['message'] = "Data Gagal Di Hapus";
			$retVal['status'] = false;
		}else{
			$retVal['message'] = "Data Berhasil Di Hapus";
			$retVal['status'] = true;
		}
		return $retVal;
	}
	
	public function getDetail($id){
		$this->db->select("*");
		$this->db->from($this->table);
		$this->db->where($this->primary_key , $id);
		$data = $this->db->get();
		return $data->row_array();
	}    

}