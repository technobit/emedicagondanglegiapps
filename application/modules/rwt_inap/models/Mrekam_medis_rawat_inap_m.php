<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mrekam_medis_rawat_inap_m extends CI_Model {
	
	/// Master Identifier
	var $table = "rekam_medis_rawat_inap";
	var $primary_key = "intIdRekamMedisRawatInap";
	var $secondar_key = "txtIdRekamMedis";
    
    public function __construct(){
        parent::__construct();
    }
	
	public function saveUpdate($array , $id){
		$query = false;
		if(empty($id)){
			$this->db->set($array);
			$query = $this->db->insert($this->table);
			$id = $this->db->insert_id();
		}else{
			$this->db->where($this->primary_key , $id);
			$query = $this->db->update($this->table, $array);
		}
		$retVal = array();
		if(!$query){
			$retVal['message'] = "Data Gagal Di perbarui";
			$retVal['status'] = false;
			$retVal['id'] = 0;
		}else{
			$retVal['message'] = "Data Berhasil Di Perbarui";
			$retVal['status'] = true;
			$retVal['id'] = $id;
		}
		
		return $retVal;
	}
	
	public function delete($id){
		$this->db->where($this->primary_key , $id);
		$q = $this->db->delete($this->table);
		
		if(!$q){
			$retVal['message'] = "Data Gagal Di Hapus";
			$retVal['status'] = false;
		}else{
			$retVal['message'] = "Data Berhasil Di Hapus";
			$retVal['status'] = true;
		}
		return $retVal;
	}
	
	public function getDetail($id){
		$this->db->select("*");
		$this->db->from("rekam_medis_rawat_inap RMRI");
		$this->db->join("rekam_medis RM" , "RM.txtIdRekamMedis = RMRI.txtIdRekamMedis");
		$this->db->join("register R" , "R.txtIdKunjungan = RMRI.txtIdKunjungan");
		$this->db->join("kamar K" , "K.intIdKamar = RMRI.intIdKamar");
		$this->db->join("pasien P" , "P.txtIdPasien = R.txtIdPasien");
		$this->db->where("intIdRekamMedisRawatInap" , $id);
		$data = $this->db->get();
		return $data->row_array();
	}

	
}