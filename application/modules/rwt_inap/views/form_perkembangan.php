<form class="form-horizontal clearfix" method="POST" action="?" id="frm-perkembangan">
    <?=$txtIdRekmedDetail?>
    <?=$intIdPerkembangan?>
    <?=$dtTanggalPemeriksaan?>
    <?=$txtInstruksiDokter?>
    <?=$txtCatatanPerawat?>
    <div class="col-md-12 text-center">
        <button id="btnSimpanPerkembangan" class="btn btn-primary" type="button"><i class="fa fa-save"></i> <?=$buttonLabel?></button>
    </div>
</form>
<script>
$(function(){
    $('#frm-perkembangan').validate({
        ignore : "",
        rules : {
            "dtTanggalPemeriksaan" : {
                required : true,
            }
        }
    });

    $('#dtTanggalPemeriksaan').datetimepicker({
        format : "YYYY-MM-DD HH:mm:ss"
    });

    $('#btnSimpanPerkembangan').click(function(){
        if($('#frm-perkembangan').valid()){
            saveDataPerkembangan();
        }
    });
});

function saveDataPerkembangan(){
    $.ajax({
        url : global_url + "rawat-inap/save-perkembangan",
        type : "POST",
        data : $('#frm-perkembangan').serialize(),
        dataType : "html",
        success : function msg(result){
            var data = jQuery.parseJSON(result);
            var status = data['status'];
            var message = data['message'];
            alertPopUp(status , message , "");
            getDataPerkembangan();
            $('.bootbox').modal("hide");
        }
    });
}
</script>