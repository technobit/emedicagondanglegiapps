<form class="form-horizontal clearfix" method="POST" action="?" id="frm-obat">
    <?=$txtIdRekmedDetail?>
    <?=$intIdRekmedObat?>
    <?=$dtTanggal?>
    <?=$intIdObat?>
    <?=$txtDosis?>
    <?=$txtTime?>
    <div class="col-md-12 text-center">
        <button id="btnSimpanObat" class="btn btn-primary" type="button"><i class="fa fa-save"></i> <?=$buttonLabel?></button>
    </div>
</form>
<script>

function formatRepo (repo) {
      if (repo.loading) return repo.text;
      var markups = "<div class='select2-result-repository clearfix'>" +
          "<div class='select2-result-repository__meta'>" +
          "<div class='select2-result-repository__title'>" + repo.nama_obat + "</div>"+
          "</div></div>";
      ///markups = repo.txtIndonesianName;
      return markups;
}
    
function formatRepoSelection (repo) {
      return repo.nama_obat || repo.text;
}

$(function(){
    $.fn.modal.Constructor.prototype.enforceFocus = function() {};
    $('#frm-obat').validate({
        ignore : "",
        rules : {
            "dtTanggalPemeriksaan" : {
                required : true,
            },
            "txtDosis" : {
                required : true,
            },
            "txtTime" : {
                required : true,
            },
        }
    });

    $('#dtTanggal').datetimepicker({
        format : "YYYY-MM-DD HH:mm:ss"
    });

    $('#btnSimpanObat').click(function(){
        if($('#frm-obat').valid()){
            saveDataObat();
        }
    });

    $('.select-obat').select2();
/*$('.select-obat').select2({
  ajax: {
  surl: global_url+"data-obat/get-list-obat/",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                q: params.term, // search term
                page: params.page
                };
            },
            processResults: function (data, params) {
            params.page = params.page || 1;
            return {
            results: data.items,
            pagination: {
                more: (params.page * 20) < data.total_count
            }
            };
            },
            cache : true,
    },
    escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
    minimumInputLength: 1,
    templateResult: formatRepo, // omitted for brevity, see the source of this page
    templateSelection: formatRepoSelection, // omitted for brevity, see the source of this page
    placeholder: 'Ketikan Nama Obat',
    });*/
});



function saveDataObat(){
    $.ajax({
        url : global_url + "rawat-inap/save-obat",
        type : "POST",
        data : $('#frm-obat').serialize(),
        dataType : "html",
        success : function msg(result){
            var data = jQuery.parseJSON(result);
            var status = data['status'];
            var message = data['message'];
            alertPopUp(status , message , "");
            getDataPengobatan();
            $('.bootbox').modal("hide");
            
        }
    });
}
</script>