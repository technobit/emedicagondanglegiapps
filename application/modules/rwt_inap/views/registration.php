<section class="content-header">
  <h1 class="title"><?=$title?></h1>
  <?=$breadcrumbs?>
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
    <div class="box box-success">
        <div class="box-header">          
            <h3 class="box-title">Registrasi Rawat Inap</h2>            
        </div>
        <div class="box-body">
        <form class="form-horizontal" id="form-filter-rawat-inap">
            <?=$frm_tanggal?>
            <?=$frm_id_poliumum?>
            <div class="col-md-offset-2 col-md-6">
                <?=$btnRegister?>
            </div>
        </form>
        </div>
    </div>
      <div class="box box-primary">
        <!-- /.box-header -->
        <div class="box-header">          
            <h3 class="box-title">Registrasi Rawat Inap</h2>            
        </div>
        <div class="box-body">
          
          <table class="table table-hover" id="table-registration">
            <thead>
                <tr>
                    <th>No Anggota</th>
                    <th>Nama Pengguna Layanan</th>
                    <th>Usia Pengguna Layanan</th>
                    <th>Status Pelayanan</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
            
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>