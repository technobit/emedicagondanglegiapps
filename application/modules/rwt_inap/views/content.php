<section class="content-header">
  <h1 class="title"><?=$title?></h1>
  <?=$breadcrumbs?>
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
    <!-- Hasil Pencarian Data Pasien -->
      <div class="box box-primary">
        <!-- /.box-header -->
        <div class="box-header">          
            <h3 class="box-title">Data Pengguna Layanan Rawat Inap</h2>
        </div>
        <div class="box-body table-responsive">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a data-toggle="tab" href="#tab_1">Pendaftaran</a></li>
              <li><a data-toggle="tab" href="#tab_2">Sedang Di Rawat</a></li>
              <li><a data-toggle="tab" href="#tab_3">Riwayat Rawat Inap</a></li>
            </ul>
            <div class="tab-content">
              <!-- /.tab-pane Form Rawat Jalan-->
              <div id="tab_1" class="tab-pane active">
                <div class="form-filter">
                    <form id="frm-filter-data-rawat-inap" class="form-horizontal">
                    <?=$frm_tanggal?>
                    <?=$frm_id_poliumum?>
                    <div class="col-sm-10 col-sm-offset-2" style="padding-left: 5px;">
                        <button type="button" class="btn btn-primary btn-flat" id="btnRawatInapRegister"><i class="fa fa-search"></i> Cari</button>
                    </div>
                    </form>
                </div>
                <table class="table table-hover" id="table-registration">
                <thead>
                  <tr>
                    <th>No Registrasi</th>
                    <th>Nama Pengguna Layanan</th>
                    <th>Usia</th>
                    <th>Status Pelayanan</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody id="list-rawat-inap">
                  
                </tbody>
              </table>
              </div>
              <!-- /.tab-pane -->
              <div id="tab_2" class="tab-pane">
                <div class="form-filter">
                    <form id="frm-filter-data-rawat-inap-selesai" class="form-horizontal">
                    <?=$frm_kelas_kamar?>
                    <?=$frm_id_poliumum?>
                    <div class="col-sm-10 col-sm-offset-2" style="padding-left: 5px;">
                        <button type="button" class="btn btn-primary btn-flat" id="btnRawatInap"><i class="fa fa-search"></i> Cari</button>
                    </div>
                    </form>
                </div>
                <table class="table table-hover" id="table-sedang-dirawat">
                <thead>
                  <tr>
                    <th>No Registrasi</th>
                    <th>Nama Pengguna Layanan</th>
                    <th>Usia</th>
                    <th>Tanggal Masuk</th>
                    <th>Ruang Kamar</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody id="list-rawat-inap-selesai">
                  
                </tbody>
              </table>
              </div>
              <div id="tab_3" class="tab-pane">
                <div class="form-filter">
                    <form id="frm-filter-data-rawat-inap-selesai" class="form-horizontal">
                    <?=$frm_kelas_kamar_riwayat?>
                    <div class="form-group">
                    <label class="col-sm-2 control-label form-label">Tanggal</label>
                    <div class="col-sm-3">
                        <div class="input-group date">
                            <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                            </div>
                            <input value="<?=date('Y-m')."-01"?>" name="start_date" class="form-control datepicker pull-right" id="datepicker1" type="text">
                        </div>
                    </div>
                    <div class="col-sm-1">
                        <label class="control-label form-label">s.d.</label>
                    </div>
                    <div class="col-sm-3">
                        <div class="input-group date">
                            <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                            </div>
                            <input value="<?=date('Y-m-d')?>" name="end_date" class="form-control datepicker pull-left" id="datepicker2" type="text">
                        </div>
                    </div>
                    </div>
                    <div class="col-sm-10 col-sm-offset-2" style="padding-left: 5px;">
                        <button type="button" class="btn btn-primary btn-flat" id="btnRawatInapRiwayat"><i class="fa fa-search"></i> Cari</button>
                    </div>
                    </form>
                </div>
                <table class="table table-hover" id="table-rawat-inap-selesai">
                <thead>
                  <tr>
                    <th>No Registrasi</th>
                    <th>Nama Pengguna Layanan</th>
                    <th>Usia</th>
                    <th>Tanggal Masuk</th>
                    <th>Tanggal Keluar</th>
                    <th>Ruang Kamar</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody id="list-rawat-inap-selesai">
                  
                </tbody>
              </table>
              </div>
            </div>
            <!-- /.tab-content -->
          </div>
          
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>