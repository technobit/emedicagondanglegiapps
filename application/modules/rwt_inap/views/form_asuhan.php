<form class="form-horizontal clearfix" method="POST" action="?" id="frm-asuhan">
    <?=$txtIdRekmedDetail?>
    <?=$intIdKeperawatan?>
    <?=$dtTanggalPemeriksaan?>
    <?=$txtDiagnosaKeperawatan?>
    <?=$txtIntervensi?>
    <?=$txtImplementasi?>
    <div class="col-md-12 text-center">
        <button id="btnSimpanAsuhan" class="btn btn-primary" type="button"><i class="fa fa-save"></i> <?=$buttonLabel?></button>
    </div>
</form>
<script>
$(function(){
    $('#frm-asuhan').validate({
        ignore : "",
        rules : {
            "dtTanggalPemeriksaan" : {
                required : true,
            },
            "txtDiagnosaKeperawatan" : {
                required : true,
            },
        }
    });

    $('#dtTanggalPemeriksaan').datetimepicker({
        format : "YYYY-MM-DD HH:mm:ss"
    });

    $('#btnSimpanAsuhan').click(function(){
        if($('#frm-asuhan').valid()){
            saveDataAsuhan();
        }
    });
});

function saveDataAsuhan(){
    $.ajax({
        url : global_url + "rwt_inap/Asuhan/saveData",
        type : "POST",
        data : $('#frm-asuhan').serialize(),
        dataType : "html",
        success : function msg(result){
            var data = jQuery.parseJSON(result);
            var status = data['status'];
            var message = data['message'];
            alertPopUp(status , message , "");
            getDataAsuhan();
            $('.bootbox').modal("hide");
            
        }
    });
}
</script>