<form class="form-horizontal clearfix" method="POST" action="?" id="frm-antrian-apotik">
    <?=$txtIdKunjungan?>
    <?=$txtIdRekmed?>
    <?=$txtIdRekmedDetail?>
    <?=$txtPengobatan?>
    <?=$bitJenisApotik?>
    <div class="col-md-12 text-center">
        <button id="btnAntrianObat" class="btn btn-primary" type="button"><i class="fa fa-save"></i> <?=$buttonLabel?></button>
    </div>
</form>

<script>
$(function(){
    $('#frm-antrian-apotik').validate({
        ignore : "",
        rules : {
            "txtPengobatan" : {
                required : true,
            }
        }
    });

    $('#btnAntrianObat').click(function(){
        if($('#frm-antrian-apotik').valid()){
            saveDataAntrian();
        }
    });
});

function saveDataAntrian(){
    $.ajax({
        url : global_url + "rawat-inap/save-antrian-apotik",
        type : "POST",
        data : $('#frm-antrian-apotik').serialize(),
        dataType : "html",
        success : function msg(result){
            var data = jQuery.parseJSON(result);
            var status = data['status'];
            var message = data['message'];
            alertPopUp(status , message , "");
            $('.bootbox').modal("hide");
        }
    });
}
</script>