<form class="form-horizontal clearfix" method="POST" action="?" id="frm-assesment-planning">
     <input name="mode" id = "modeTerapi" value="<?=$mode?>" type="hidden">
    <?=$txtIdRekmedDetail?>
    <?=$txtDiagnosis?>
    <?=$txtTerapi?>
    <div class="col-md-12 text-center">
        <button id="btnSimpanAssesment" class="btn btn-primary" type="button"><i class="fa fa-save"></i> <?=$buttonLabel?></button>
              <button onClick="cetakTerapi()" id="btnCetakDiagnosis" class="btn btn-warning" type="button"><i class="fa fa-print"></i> Cetak</button>
     
     
</form>

<script>
$(function(){
    $('#frm-assesment-planning').validate({
        ignore : "",
        rules : {
            "txtAssesment" : {
                required : true,
            }
        }
    });
    
    $('#btnSimpanAssesment').click(function(){
        if($('#frm-assesment-planning').valid()){
            saveDataAssesment();
        }
    });
    $('#btnCetakAl').click(function(){
        bootbox. alert({
          title : 'Peringatan',
          message : 'Data Belum Ada'
        });
    });
});

function saveDataAssesment(){
    $.ajax({
        url : global_url + "rawat-inap/save-assesment",
        type : "POST",
        data : $('#frm-assesment-planning').serialize(),
        dataType : "html",
        success : function msg(result){
            var data = jQuery.parseJSON(result);
            var status = data['status'];
            var message = data['message'];
            var id = data['id'];
            alertPopUp(status , message , "");
           $("#modeTerapi").val('2');
        }
    });
}
function cetakTerapi(){
    var urlDownload = "<?=$link_cetak?>";
    var mode =   $("#modeTerapi").val();  ;

    if(mode == 0){
        bootbox.alert({
          title : 'peringatan',
          message : 'data belum ada'
        });
    }else{
        window.open(urlDownload,'_blank');
         
    }
}
</script>