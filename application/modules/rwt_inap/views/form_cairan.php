<form class="form-horizontal clearfix" method="POST" action="?" id="frm-cairan">
    <?=$txtIdRekmedDetail?>
    <?=$intIdCairan?>
    <?=$dtTanggal?>
    <?=$txtJenisCairan?>
    <?=$txtDosis?>
    <?=$txtJumlahUrine?>
    <div class="col-md-12 text-center">
        <button id="btnSimpanCairan" class="btn btn-primary" type="button"><i class="fa fa-save"></i> <?=$buttonLabel?></button>
    </div>
</form>
<script>

$(function(){
///   $.fn.modal.Constructor.prototype.enforceFocus = function() {};
    $('#frm-cairan').validate({
        ignore : "",
        rules : {
            "txtJenisCairan" : {
                required : true,
            },
            "txtDosis" : {
                required : true,
            },
            "txtJumlahUrine" : {
                required : true,
            },
        }
    });

    $('#dtTanggal').datetimepicker({
        format : "YYYY-MM-DD HH:mm"
    });
    $('#dtJam').datetimepicker({
        format : "HH:mm"
    });

    $('#btnSimpanCairan').click(function(){
        if($('#frm-cairan').valid()){
            saveDataCairan();
        }
    });

});

function saveDataCairan(){
    $.ajax({
        url : global_url + "rawat-inap/save-cairan",
        type : "POST",
        data : $('#frm-cairan').serialize(),
        dataType : "html",
        success : function msg(result){
            var data = jQuery.parseJSON(result);
            var status = data['status'];
            var message = data['message'];
            alertPopUp(status , message , "");
            getDataCairan();
            $('.bootbox').modal("hide");
            
        }
    });
}
</script>