<div class="clearfix">
    <div class="col-md-12">
    <h4 class="">Resume Medik</h4>
        <form method="POST" action="?" class="form-horizontal" id="frm-resume-medis">
             <input name="mode" id = "modeResume" value="<?=$mode?>" type="hidden">
            <?=$frm_id_detail_rekmed?>
            <?=$dtKeluar?>
            <?=$intIdPegawai?>
            <?=$txtAnamnesis?>
            <?=$txtPeriksaFisik?>
            <?=$txtPeriksaPenunjang?>
            <!--Form Diagnosa Sekarang-->
            <?=$frm_rekmed_diagnosa?>
            <?=$frm_rekmed_diagnosa_keluar?>
            <?=$txtTerapi?>
            <?=$txtPrognosis?>
            <div class="form-group">
              <label class="col-sm-3 control-label" for="selectJamkes">Kondisi Keluar</label>
              <div class="col-sm-9">
                <?=$intKeadaanKeluar?>
                <?=$intIdLayananRujukan?>
              </div>
            </div>
            <?=$txtSaran?>
            <div class="col-md-12 text-center">
                <button type="button" class="btn btn-primary" id="simpanDataResume"><i class="fa fa-save"></i> <?=$buttonLabel?></button>
              <button onClick="cetakResume()" id="btnCetakDiagnosis" class="btn btn-warning" type="button"><i class="fa fa-print"></i> Cetak</button>
     
            </div>
        </form>
    </div>
</div>
<script>
var base_url_rekmed = global_url+'rekam-medis/';
var base_url_penyakit = global_url+"data-penyakit/";
$(function(){
$('#dtMasuk').datetimepicker({
    format : "YYYY-MM-DD HH:mm:ss"
});
$('#dtKeluar').datetimepicker({
    format : "YYYY-MM-DD HH:mm:ss"
});
$('#frm-resume-medis').validate({
    ignore : "",
    rules : {
        "txtAnamnesis" : {
            required : true,
        }
    }
});

$('#simpanDataResume').click(function(){
    if($('#frm-resume-medis').valid()){
        ///alert("Simpan Data Berhasil");
        saveDataResume();
    }
});

$('.select-diagnosa').select2({
        ajax: {
        url: base_url_penyakit+"get-list-penyakit/",
        dataType: 'json',
        delay: 250,
        data: function (params) {
            return {
              q: params.term, // search term
              page: params.page
            };
        },
        processResults: function (data, params) {
        params.page = params.page || 1;
        return {
          results: data.items,
          pagination: {
            more: (params.page * 20) < data.total_count
        }
        };
        },
        cache : true,
  },
  escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
  minimumInputLength: 1,
  templateResult: formatRepo, // omitted for brevity, see the source of this page
  templateSelection: formatRepoSelection, // omitted for brevity, see the source of this page
  placeholder: 'Ketikan Nama Penyakit / Kode ICD',
});
    
});

function saveDataResume(){
    $.ajax({
       url : global_url+"rawat-inap/save-resume/",
       type : "POST",
       data : $('#frm-resume-medis').serialize(),
       dataType : "html",
       success: function msg(res){
            var data = jQuery.parseJSON(res);            
            var status = data['status'];
            var message = data['message'];
           var id = data['id'];
            alertPopUp(status , message , "");
           $("#modeResume").val('2');   
       }
    });
}

function checkRujukan(){
    var intKeadaanKeluar = $('#intKeadaanKeluar').val();
    if(intKeadaanKeluar==4){
        $('#intIdLayananRujukan').removeAttr("disabled");
    }else{
        $('#intIdLayananRujukan').attr("disabled","");
    }
}
function cetakResume(){
    var urlDownload = "<?=$link_cetak?>";
    var mode =   $("#modeResume").val();

    if(mode == 0){
        bootbox.alert({
          title : 'peringatan',
          message : 'data belum ada'
        });
    }else{
        window.open(urlDownload,'_blank');
         
    }
}
</script>