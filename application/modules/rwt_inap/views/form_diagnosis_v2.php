<div class="clearfix">
    <div class="col-md-12">
    <h4 class="">Form Anamnesis</h4>
        <form method="POST" action="?" class="form-horizontal" id="frm-diagnosis-rawat-inap">
            <input name="mode" id = "modeDiagnosa" value="<?=$mode?>" type="hidden">
            <?=$txtIdRekmedDetail?>
            <!--Form Diagnosa Sekarang-->
            <?=$txtKeluhan?>
            <?=$txtRiwayatSekarang?>
            <?=$txtRiwayatDulu?>
            <?=$txtRiwayatKeluarga?>
            <?=$txtRiwayatSosial?>
            <div class="col-md-12 text-center">
                <button type="button" class="btn btn-primary" id="simpanDataDiagnosis"><i class="fa fa-save"></i> <?=$buttonLabel?></button>
                
                  <button onClick="cetakDiagnosa()" id="btnCetakDiagnosis" class="btn btn-warning" type="button"><i class="fa fa-print"></i> Cetak</button>
     
            </div>
        </form>
    </div>
</div>
<script>
$(function(){
     
     var mode = $('#mode').val();
    if (mode  == "") {
        $('#btnCetakDiagnosis').click(function(){
        
});
    }
  
$('#frm-diagnosis-rawat-inap').validate({
    ignore : "",
    rules : {
        "txtKeluhan" : {
            required : true,
        },
        "txtRiwayatSekarang" : {
            required : true,
        }
    }
});
$('#simpanDataDiagnosis').click(function(){
    if($('#frm-diagnosis-rawat-inap').valid()){
        ///alert("Simpan Data Berhasil");
        saveDataDiagnosis();
    }
});
});

function saveDataDiagnosis(){
    $.ajax({
       url : global_url+"rawat-inap/diagnosis/save-diagnosis/",
       type : "POST",
       data : $('#frm-diagnosis-rawat-inap').serialize(),
       dataType : "html",
       success: function msg(res){
            var data = jQuery.parseJSON(res);            
            var status = data['status'];
            var message = data['message'];
            var id = data['id'];
            alertPopUp(status , message , "");
             //$('#btnCetakDiagnosis').css('display','inline');
            $("#mode").val('3');
            $("#simpanDataDiagnosis").val('Update');     
       }
    });
}
function cetakDiagnosa(){
    var urlDownload = "<?=$link_cetak?>";
    var mode =   $("#modeDiagnosa").val();  ;

    if(mode == 0){
        bootbox.alert({
          title : 'peringatan',
          message : 'data belum ada'
        });
    }else{
        window.open(urlDownload,'_blank');
         
    }
}
</script>