<div class="clearfix" id="form-identitas">
<div class="alert alert-danger alert-dismissable" id="alert_message5" style="display: none;">
    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
    <h4><i class="icon fa fa-ban" id="alert_message5_icon"></i> Peringatan!</h4>
    <div id="alert_message5_content"></div>
</div>
<form method="POST" action="?" class="" id="frm-identitas-pasien">
    <div class="col-md-6">
        <?=$frm_id_pasien?>
        <?=$frm_nama_pasien?>
        <?=$frm_umur_pasien?>
        <?=$frm_jk_pasien?>
        <?=$frm_telpon_pasien?>
        <?=$frm_agama?>
        <?=$frm_pendidikan?>
    </div>
    <div class="col-md-6">
        <?=$frm_nama_keluarga?>
        <?=$frm_usia_keluarga?>
        <?=$frm_jk_keluarga?>
        <?=$frm_alamat_keluarga?>
        <?=$frm_telpon_keluarga?>
        <?=$frm_agama_keluarga?>
        <?=$frm_pendidikan_keluarga?>
        <?=$frm_hubungan_keluarga?>
    </div>
    <div class="text-center col-md-12">
        <button class="btn btn-success btn-flat" type="button" id="simpanDataIdentitas"><i class="fa fa-save"></i> <?=$buttonLabel?></button>
    </div>
</form>
<script>
    $(function(){
        $('#frm-identitas-pasien').validate({
            ignore : "",
            rules : {
                txtNamaKeluarga : {
                    required : true,
                },
                txtBeratBadan : {
                    required : true,
                }
            }
        });

        $('#simpanDataPelayananANC').click(function(){
            if ($('#frm-identitas-pasien').valid()) {
                //code
                saveDataPelayananANC();
            }

        });
    });

function saveDataPelayananANC() {
    //code
    $.ajax({
       url : global_url+"rekam-medis-ibu/save-data-pelayanan-anc-ibu/",
       type : "POST",
       data : $('#frm-pelayanan-anc').serialize(),
       dataType : "html",
       success: function msg(res){
            var data = jQuery.parseJSON(res);
            var status = data['status'];
            var message = data['message'];
            messageBox(status , message , 'alert_message5');
            if (status==true) {
             //code
             getDataPelayananANC();
             $('.bootbox').modal("hide");
            }
       }
    });

}
</script>
</div>
