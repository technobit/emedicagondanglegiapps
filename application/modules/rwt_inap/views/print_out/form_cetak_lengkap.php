<section class="content-header">
    <h1 class="title"><?=$title?></h1>
    <?=$breadcrumbs?>
</section>
<section class="content">
<div class="row"> 
  <div class="col-md-12 text-right no-print">    
    <?=form_button("btnCetak" , '<i class="fa fa-print"></i> Cetak' , 'onclick="cetak()" class="btn btn-default btn-flat"')?>
   </div>
</div>
<section class="invoice">
<!-- title row -->

<div class="row">
  <div class="col-xs-12">
    <center>
    <h2 class="page-header">
      <img src="<?=ASSETS_IMAGE_URL?>pkm_bll_black.png" height="50px">
     
    </h2>
    </center>
  </div>
  <!-- /.col -->
</div>
<div class="text-center">
  <h4>Data Pengguna Layanan</h4>
</div>
<div class="row invoice-info">
        <div class="col-sm-6 invoice-col">
          <address>
            <strong>Nama Pengguna Layanan</strong> <?=': '.$detailPasien['txtNamaPasien']?><br>
            <strong>No Rekam Medis</strong> <?=': '.$detailPasien['txtNoRekamMedis']?> <br>
            <strong>Kamar Pengguna Layanan</strong> <?=': '.$detailPasien['txtKamar']?> <br>
            <strong>Tanggal Masuk</strong> <?=': '.$tanggalKunjungan?> <br>
          </address>
        </div>
     
        <div class="col-sm-2 invoice-col">
        </div>
    
        <div class="col-sm-4 invoice-col">
          <b>Usia Pengguna Layanan </b><?=': '.$detailPasien['txtUsiaPasienKunjungan']?><br>
          <b>Jenis Kelamin  </b> <?=$jenisKelamin?><br>
          <b>Jaminan Kesehatan  </b> <?=': '.$detailPasien['txtNamaJaminan']?><br>
          <b>No Jaminan Kesehatan  </b> <?=': '.$detailPasien['txtNoIdJaminanKesehatan']?><br>
        </div>
       
      </div>
 <div class="text-center">
  <h4>Amanesis</h4>
</div>
<div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          <address>
            <strong>Keluhan</strong> 
            </address>
        </div>
     <div class="col-sm-8 invoice-col">
          <?=': '.$diagnosa['txtKeluhan']?><br>
          </div>
</div>

<div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          <address>
            <strong>Riwayat Sekarang</strong> 
            </address>
        </div>
     <div class="col-sm-8 invoice-col">
          <?=': '.$diagnosa['txtRiwayatSekarang']?><br>
          </div>
</div>
<div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          <address>
            <strong>Riwayat Lama</strong> 
            </address>
        </div>
     <div class="col-sm-8 invoice-col">
          <?=': '.$diagnosa['txtRiwayatDulu']?><br>
          </div>
</div>
<div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          <address>
            <strong>Riwayat Keluarga</strong> 
            </address>
        </div>
     <div class="col-sm-8 invoice-col">
          <?=': '.$diagnosa['txtRiwayatKeluarga']?><br>
          </div>
</div>
<div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          <address>
            <strong>Riwayat Sosial</strong> 
            </address>
        </div>
     <div class="col-sm-8 invoice-col">
          <?=': '.$diagnosa['txtRiwayatSosial']?><br>
          </div>
</div>
<div class="text-center">
  <h4>Pemeriksaan Fisik Dan Lanjut</h4>
</div>
<div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          <address>
            <strong>Keadaan Umum</strong> 
            </address>
        </div>
     <div class="col-sm-8 invoice-col">
          <?=': '.$detailData['txtKeadaanUmum']?><br>
          </div>
</div>

<div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          <address>
            <strong>Pemeriksaan Fisik</strong> 
            </address>
        </div>
     <div class="col-sm-2 invoice-col">
           <address>
            <strong>TD</strong><br>
            <strong>Temp</strong><br>
            <strong>Gizi</strong><br>
             <strong>Tinggi Badan</strong><br> 
            </address>
            </div>
     <div class="col-sm-2 invoice-col">
          <?=': '.$detailData['txtTD'].' <small>mmHg</small>'?><br>
          <?=': '.$detailData['txtTemp'].' <small>C</small>'?><br>
          <?=': '.$detailData['txtGizi']?><br>
          <?=': '.$detailData['txtTB'].' <small>cm</small>'?><br>
          </div>
     <div class="col-sm-2 invoice-col">
          <address>
            <strong>Nadi</strong><br>
            <strong>RR</strong><br>
            <strong>Berat Badan</strong><br>
             <strong>Kesadaran</strong> <br>
            </address>
     </div>
      <div class="col-sm-2 invoice-col">
          <?=': '.$detailData['txtNadi'].' <small>X/Menit</small>'?><br>
           <?=': '.$detailData['txtRR'].' <small>X/Menit</small>'?><br>
            <?=': '.$detailData['txtBB'].' <small>kg</small>'?><br>
            <?=': '.!empty($detailData['txtKesadaran'])?$kesadaran[$detailData['txtKesadaran']]:""?><br>
          </div>
</div>
<div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          <address>
            <strong>Kepala / Leher</strong> 
            </address>
        </div>
     <div class="col-sm-8 invoice-col">
          <?=': '.$detailData['txtKepala']?><br>
          </div>
</div>
<div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          <address>
            <strong>Thorax</strong> 
            </address>
        </div>
     <div class="col-sm-8 invoice-col">
          <?=': '.$detailData['txtThorax']?><br>
          </div>
</div>
<div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          <address>
            <strong>Abdomen</strong> 
            </address>
        </div>
     <div class="col-sm-8 invoice-col">
          <?=': '.$detailData['txtAbdomen']?><br>
          </div>
</div>
<div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          <address>
            <strong>Ekstrimitas</strong> 
            </address>
        </div>
     <div class="col-sm-8 invoice-col">
          <?=': '.$detailData['txtExtrimitas']?><br>
          </div>
</div>
<div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          <address>
            <strong>Laboratorium</strong> 
            </address>
        </div>
     <div class="col-sm-8 invoice-col">
          <?=': '.$detailData['txtLaboratorium']?><br>
          </div>
</div>
<div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          <address>
            <strong>ECG</strong> 
            </address>
        </div>
     <div class="col-sm-8 invoice-col">
          <?=': '.$detailData['txtECG']?><br>
          </div>
</div>
<div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          <address>
            <strong>Radiologi</strong> 
            </address>
        </div>
     <div class="col-sm-8 invoice-col">
          <?=': '.$detailData['txtRadiologi']?><br>
          </div>
</div>
<div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          <address>
            <strong>Lain-Lain</strong> 
            </address>
        </div>
     <div class="col-sm-8 invoice-col">
          <?=': '.$detailData['txtLainLain']?><br>
          </div>
</div>
 <div class="text-center">
  <h4>Diagnosis Dan Terapi</h4>
</div>
<div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          <address>
            <strong>Diagnosa</strong> 
            </address>
        </div>
     <div class="col-sm-8 invoice-col">
          <?=': '.$detailData_['txtDiagnosis']?><br>
          </div>
</div>
<div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          <address>
            <strong>Terapi</strong> 
            </address>
        </div>
     <div class="col-sm-8 invoice-col">
          <?=': '.$detailData_['txtTerapi']?><br>
          </div>
</div>

<!-- /.row -->    
</section>
</section>