<?php
class Status_absensi_model extends CI_Model{

    var $table = "mstatus_absensi";
    var $primary_key = "intStatus";
    
    function __construct() { 
        parent::__construct();
    }

    function insertUpdate($arrData , $id){
        
        if(!empty($id)) { 
            $this->db->where($this->primary_key , $id);
		    $query = $this->db->update($this->table, $arrData);
            $message = !$query ? "Data Gagal Di Simpan" : "Data Berhasil Di Simpan";
            
        }else{
            $this->db->set($arrData);
            $query = $this->db->insert($this->table);
            $message = !$query ? "Data Gagal Di Simpan" : "Data Berhasil Di Simpan";
            $id = $this->db->insert_id();
        }
        $retVal =array();
        $retVal['status'] = $query;
        $retVal['message'] = $message;
        $retVal['id'] = $id;
        return $retVal;
    }

    function checkPresensiPegawai($intIdPegawai , $date){
        $this->db->from($this->table);
        $this->db->join("pegawai" , "pegawai.intIdPegawai = ");
        $this->db->where("intIDPegawai" , $intIdPegawai);
        $this->db->like("dtAbsensiMasuk" , $date);
        $data = $this->db->get();
        return $data->row_array();
    }

    function detailAbsensiPegawai($intIDAbsensi) {
        $this->db->from($this->table);
        $this->db->where("intIDAbsensi" , $intIDAbsensi);
        $data = $this->db->get();
        return $data->row_array();
    }

    function getDataStatus(){
        $this->db->from($this->table);
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    function delete($id){
        $this->db->where($this->primary_key , $id);
		$query = $this->db->delete($this->table);
        $message = !$query ? "Data Gagal Di Hapus" : "Data Berhasil Di Hapus";
        $retVal['status'] = $query;
        $retVal['message'] = $message;
        return $retVal;
    }

    function detail($id) {
        $this->db->where($this->primary_key , $id);
        $data = $this->db->get($this->table);
        return $data->row_array();
    }
}