<?php
class Absensi_model extends CI_Model{

    var $table = "logabsensi_pegawai";
    var $primary_key = "intIDAbsensi";
    
    function __construct() { 
        parent::__construct();
    }

    function insertUpdate($arrData , $id){
        
        if(!empty($id)) { 
            $this->db->where($this->primary_key , $id);
		    $query = $this->db->update($this->table, $arrData);
            $message = !$query ? "Data Gagal Di Simpan" : "Data Berhasil Di Simpan";
            
        }else{
            $this->db->set($arrData);
            $query = $this->db->insert($this->table);
            $message = !$query ? "Data Gagal Di Simpan" : "Data Berhasil Di Simpan";
            $id = $this->db->insert_id();
        }
        $retVal =array();
        $retVal['status'] = $query;
        $retVal['message'] = $message;
        $retVal['id'] = $arrData['intIDPegawai'];
        return $retVal;
    }

    function checkPresensiPegawai($intIdPegawai , $date){
        $this->db->from($this->table);
        $this->db->join("pegawai" , "pegawai.intIdPegawai = logabsensi_pegawai.intIDPegawai");
        $this->db->where("logabsensi_pegawai.intIDPegawai" , $intIdPegawai);
        $this->db->like("logabsensi_pegawai.dtAbsensiMasuk" , $date);
        $data = $this->db->get();
        return $data->row_array();
    }

    function detailAbsensiPegawai($intIDAbsensi) {
        $this->db->from($this->table);
        $this->db->where("intIDAbsensi" , $intIDAbsensi);
        $data = $this->db->get();
        return $data->row_array();
    }

    function getDataAbsensi($dtStart , $dtEnd , $intIdStatus){
        $strSP = "CALL Absensi_GetDataAbsensi('".$dtStart." 00:00:00' , '".$dtEnd." 23:59:59' , '".$intIdStatus."')";
        
        $query = $this->db->query($strSP);
        $result = $query->result_array();
        return $result;
    }

    function delete($id){
        $this->db->where($this->primary_key , $id);
		$query = $this->db->delete($this->table);
        $message = !$query ? "Data Gagal Di Hapus" : "Data Berhasil Di Hapus";
        $retVal['status'] = $query;
        $retVal['message'] = $message;
        return $retVal;
    }

    function detail($id) {
        $this->db->where($this->primary_key , $id);
        $data = $this->db->get($this->table);
        return $data->row_array();
    }

    function getListStatus(){

        $data = $this->db->get("mstatus_absensi");
        return $data->result_array();

    }

    function getListStatusAbsensi(){
        $this->db->where("intStatus" , "!=1");
        $data = $this->db->get("mstatus_absensi");
        return $data->result_array();

    }
}