<?php
class Report_model extends CI_Model{

    var $table = "logabsensi_pegawai";
    var $primary_key = "intIDAbsensi";
    
    function __construct() { 
        parent::__construct();
    }

    function getRekapAbsensi($dtStart , $dtEnd){
        $query = "CALL Absensi_RptRekapDataAbsensi('".$dtStart."' , '".$dtEnd."')";
        $data = $this->db->query($query);
        $result = $data->result_array();
        return $result;
    }

    function getDetailRekapAbsensi($dtStart , $dtEnd , $intIdStatus = 0 , $intIdPegawai = 0){
        $query = "CALL Absensi_GetDetailRkpDataAbsensi('".$intIdPegawai."','".$dtStart."' , '".$dtEnd."' , '".$intIdStatus."')";
        $data = $this->db->query($query);
        $result = $data->result_array();
        return $result;
    }

    function getDetailAbsensi($dtStart , $dtEnd , $intIdStatus = 0 , $intIdPegawai = 0){
        $query = "CALL Absensi_RptDataAbsensiDateRange('".$dtStart."' , '".$dtEnd."' , '".$intIdStatus."' , '".$intIdPegawai."')";
        $data = $this->db->query($query);
        $result = $data->result_array();
        return $result;
    }

    function getDataHarian($dtInput){
        $dtStart = $dtInput." 00:00:00";
        $dtEnd = $dtInput." 23:59:00";
        $query = "
        SELECT pegawai.`txtNamaPegawai`, 
( SELECT `dtAbsensiMasuk` FROM `logabsensi_pegawai` WHERE `logabsensi_pegawai`.`intIDPegawai` = pegawai.`intIdPegawai` AND `logabsensi_pegawai`.`dtAbsensiMasuk` BETWEEN '".$dtStart."' AND '".$dtEnd."' AND `dtAbsensiMasuk` IS NOT NULL GROUP BY `intIDPegawai` ) AS tanggal_masuk, 
( SELECT `dtAbsensiKeluar` FROM `logabsensi_pegawai` WHERE `logabsensi_pegawai`.`intIDPegawai` = pegawai.`intIdPegawai` AND `logabsensi_pegawai`.`dtAbsensiMasuk` BETWEEN '".$dtStart."' AND '".$dtEnd."' AND `dtAbsensiMasuk` IS NOT NULL GROUP BY `intIDPegawai` ) AS tanggal_pulang, 
( SELECT txtStatus FROM `logabsensi_pegawai` LEFT JOIN mstatus_absensi ON logabsensi_pegawai.intStatus = mstatus_absensi.intStatus WHERE `logabsensi_pegawai`.`intIDPegawai` = pegawai.`intIdPegawai` AND `logabsensi_pegawai`.`dtAbsensiMasuk` BETWEEN '".$dtStart."' AND '".$dtEnd."' AND logabsensi_pegawai.`intStatus` IS NOT NULL GROUP BY `intIDPegawai` ) AS status,
( SELECT `logabsensi_pegawai`.`intStatus` FROM `logabsensi_pegawai` LEFT JOIN mstatus_absensi ON logabsensi_pegawai.intStatus = mstatus_absensi.intStatus WHERE `logabsensi_pegawai`.`intIDPegawai` = pegawai.`intIdPegawai` AND `logabsensi_pegawai`.`dtAbsensiMasuk` BETWEEN '".$dtStart."' AND '".$dtEnd."' AND logabsensi_pegawai.`intStatus` IS NOT NULL GROUP BY `intIDPegawai` ) AS STATUS_INT 

FROM `pegawai` ORDER BY `intIDPegawai` ASC
";
        $data = $this->db->query($query);
        $result = $data->result_array();
        return $result;
    }

    

    
}