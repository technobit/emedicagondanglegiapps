<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register_Model extends CI_Model {

	var $table = "register";
	var $primary_key = "txtIdKunjungan";
    
    public function __construct(){
        parent::__construct();    
    }
	
	public function getRegisterRawatJalan($date_start , $date_end , $id_pelayanan = 0 , $id_status = 0){
		$this->db->select("register.txtIdPasien,register.txtIdKunjungan,register.txtUsiaPasienKunjungan ,register.intNoAntri, pasien.txtNamaPasien,pasien.txtNoAnggota,pasien.dtTanggalLahir,pelayanan.txtNama as txtNamaPelayanan,register.bitStatusKunjungan,register.bitIsPoli");
		$this->db->from($this->table);
		$this->db->join("pasien" , "register.txtIdPasien =  pasien.txtIdPasien" , "left");
		$this->db->join("pelayanan" , "register.intIdPelayanan = pelayanan.intIdPelayanan" , "left");
		$this->db->join("jaminan_kesehatan" , "register.intIdJaminanKesehatan = jaminan_kesehatan.intIdJaminanKesehatan" , "left");
		
		$this->db->where("register.bitStatusKunjungan" , 1);
		$this->db->where("register.dtTanggalKunjungan BETWEEN '".$date_start."' AND '".$date_end."'");
		if($id_pelayanan!=0){
			$this->db->where("register.intIdPelayanan" , $id_pelayanan);
		}
		if($id_status!=0){
			$this->db->where("register.bitIsPoli" , $id_status);
		}
		$this->db->order_by("register.dtTanggalKunjungan" , "ASC");
		$data = $this->db->get();
        return $data->result_array();
	}
	
	public function getRegisterAntrianRawatJalan($date_start , $date_end){
		$this->db->select("register.txtIdPasien,register.txtIdKunjungan,register.txtUsiaPasienKunjungan ,register.intNoAntri, pasien.txtNamaPasien,pasien.txtNoAnggota,pasien.dtTanggalLahir,pelayanan.txtNama as txtNamaPelayanan,register.bitStatusKunjungan,register.bitIsPoli");
		$this->db->from($this->table);
		$this->db->join("pasien" , "register.txtIdPasien =  pasien.txtIdPasien" , "left");
		$this->db->join("pelayanan" , "register.intIdPelayanan = pelayanan.intIdPelayanan" , "left");
		$this->db->join("jaminan_kesehatan" , "register.intIdJaminanKesehatan = jaminan_kesehatan.intIdJaminanKesehatan" , "left");
		$this->db->where("register.bitStatusKunjungan" , 1);
		$this->db->where("register.bitIsPoli" , 1);
		$this->db->where("register.dtTanggalKunjungan BETWEEN '".$date_start."' AND '".$date_end."'");
		$this->db->order_by("register.dtTanggalKunjungan" , "ASC");
		$data = $this->db->get();
        return $data->result_array();
	}
	public function getLatestDataRegister($offset , $limit , $date_start="" , $date_end = "" , $idPelayanan = "" , $status="1"){
		if(empty($date_start)){
			$date_start = date("Y-m-d")." 00:00:00";	
		}
		
		if(empty($date_end)){
			$date_end = date("Y-m-d H:i:s");	
		}
		
		$this->db->select("
						register.txtIdPasien,
						register.txtIdKunjungan,
						register.txtUsiaPasienKunjungan,
						register.intNoAntri,
						register.txtKeteranganKunjungan,  
						pasien.txtNamaPasien,
						pasien.txtNoAnggota,
						pasien.dtTanggalLahir,
						pelayanan.txtNama as txtNamaPelayanan,
						pelayanan_referr.txtNama as txtNamaPelayananReferrer,
						register.bitStatusKunjungan,
						register.bitIsPoli");
		$this->db->from($this->table);
		$this->db->join("pasien" , "register.txtIdPasien =  pasien.txtIdPasien" , "left");
		$this->db->join("pelayanan" , "register.intIdPelayanan = pelayanan.intIdPelayanan" , "left");
		$this->db->join("pelayanan as pelayanan_referr" , "register.intRefIdPelayanan = pelayanan_referr.intIdPelayanan" , "left");
		$this->db->join("jaminan_kesehatan" , "register.intIdJaminanKesehatan = jaminan_kesehatan.intIdJaminanKesehatan" , "left");
		$this->db->where("register.bitIsPoli" , $status);

		if(!empty($idPelayanan)){
			$this->db->where("register.intIdPelayanan" , $idPelayanan);
		}
		
		$this->db->where("register.dtTanggalKunjungan BETWEEN '".$date_start."' AND '".$date_end."'");
		$this->db->order_by("register.dtTanggalKunjungan" , "ASC");
		$this->db->limit($limit);
		$this->db->offset($offset);
		$data = $this->db->get();
        return $data->result_array();
	}

	public function getDataRawatInap($idPelayanan = "" , $status="1" , $kamar=""){		
		$this->db->select("
						register.txtIdPasien,
						register.txtIdKunjungan,
						register.txtUsiaPasienKunjungan,
						register.txtKeteranganKunjungan,
						register.dtTanggalKunjungan,
						register.bitStatusKunjungan,  
						pasien.txtNamaPasien,
						pasien.txtNoAnggota,
						pasien.dtTanggalLahir,
						pelayanan.txtNama as txtNamaPelayanan,
						kamar.txtKamar,
						kelas_kamar.txtKelasKamar,
						register.bitStatusKunjungan,
						rekam_medis_detail.txtIdRekmedDetail,
						rekam_medis_detail.txtIdRekamMedis,
						register.bitIsPoli");
		$this->db->from($this->table);
		$this->db->join("pasien" , "register.txtIdPasien =  pasien.txtIdPasien" , "left");
		$this->db->join("rekam_medis_detail" , "rekam_medis_detail.txtIdKunjungan =  register.txtIdKunjungan" , "inner");
		$this->db->join("register_kamar" , "register.txtIdKunjungan =  register_kamar.txtIdKunjungan" , "inner");
		$this->db->join("kamar" , "register_kamar.intIdKamar =  kamar.intIdKamar" , "left");
		$this->db->join("kelas_kamar" , "kamar.intIdKelasKamar =  kelas_kamar.intIdKelasKamar" , "left");
		$this->db->join("pelayanan" , "register.intIdPelayanan = pelayanan.intIdPelayanan" , "left");
		$this->db->join("jaminan_kesehatan" , "register.intIdJaminanKesehatan = jaminan_kesehatan.intIdJaminanKesehatan" , "left");
		$this->db->where("register.bitStatusKunjungan" , 2);
		$this->db->where("register.bitIsPoli" , $status);

		if(!empty($idPelayanan)){
			$this->db->where("register.intIdPelayanan" , $idPelayanan);
		}
		if(!empty($kamar)){
			$this->db->where("register_kamar.intIdKamar" , $kamar);
		}
		
		$this->db->order_by("register.dtTanggalKunjungan" , "DESC");
		$data = $this->db->get();
        return $data->result_array();
	}

	public function getDataRegisterRawatInap($unit_poli , $status, $date_start , $date_end){
		$this->db->select("
						register.txtIdPasien,
						register.txtIdKunjungan,
						register.txtUsiaPasienKunjungan,
						register.txtKeteranganKunjungan,
						register.bitStatusKunjungan,  
						pasien.txtNamaPasien,
						pasien.txtNoAnggota,
						pasien.dtTanggalLahir,
						pelayanan.txtNama as txtNamaPelayanan,
						register.bitStatusKunjungan,
						register.bitIsPoli");
		$this->db->from($this->table);
		$this->db->join("pasien" , "register.txtIdPasien =  pasien.txtIdPasien" , "left");
		$this->db->join("pelayanan" , "register.intIdPelayanan = pelayanan.intIdPelayanan" , "left");
		$this->db->join("jaminan_kesehatan" , "register.intIdJaminanKesehatan = jaminan_kesehatan.intIdJaminanKesehatan" , "left");
		$this->db->where("register.bitStatusKunjungan" , 2);
		$this->db->where("register.bitIsPoli" , $status);
		
		if(!empty($unit_poli)){
			$this->db->where("register.intIdPelayanan" , $unit_poli);
		}

		$this->db->where("register.dtTanggalKunjungan BETWEEN '".$date_start."' AND '".$date_end."'");
		
		$this->db->order_by("register.dtTanggalKunjungan" , "DESC");
		$data = $this->db->get();
        return $data->result_array();
	}
	
	public function countRegisterByPelayanan($idPelayanan = "" , $date_start="" , $date_end = "" , $status = ""){
		if(empty($date_start)){
			$date_start = date("Y-m-d")." 00:00:00";	
		}
		
		if(empty($date_end)){
			$date_end = date("Y-m-d H:i:s");	
		}
		
		$this->db->select("register.*");
		$this->db->from($this->table);
		
		if(!empty($status)){
			$this->db->where("register.bitIsPoli" , $status);	
		}
		
		if(!empty($idPelayanan)){
			$this->db->where("register.intIdPelayanan" , $idPelayanan);
		}
		
		$this->db->where("register.dtTanggalKunjungan BETWEEN '".$date_start."' AND '".$date_end."'");
		$data = $this->db->count_all_results();
		return $data;
	}

	public function countRegisterByPelayananSum($date_start="" , $date_end = ""){
		$this->db->select("pelayanan.intIdPelayanan AS id, pelayanan.txtNama AS nama, COUNT(register.intIdPelayanan) AS jumlah");
		$this->db->from($this->table);
		$this->db->join("pelayanan" , "register.intIdPelayanan = pelayanan.intIdPelayanan" , "left");
		$this->db->where("register.dtTanggalKunjungan BETWEEN '".$date_start."' AND '".$date_end."'");
		$this->db->where("pelayanan.intIdJenisPelayanan!=8");	
		$this->db->group_by("register.intIdPelayanan");
		$data = $this->db->get();
        return $data->result_array();
	}

	public function countRegisterByDate($date_start="" , $date_end = ""){
		$this->db->select("register.dtTanggalKunjungan, COUNT(register.dtTanggalKunjungan) AS Jumlah");
		$this->db->from($this->table);
		$this->db->where("register.dtTanggalKunjungan BETWEEN '".$date_start."' AND '".$date_end."' GROUP BY DAY(register.dtTanggalKunjungan)");
		$data = $this->db->get();
        return $data->result_array();
	}

	public function getNoAntrianByIdPelayanan($idPelayanan){
		$date_start = date("Y-m-d")." 00:00:00";
		$date_end = date("Y-m-d H:i:s");
		$this->db->select("register.*");
		$this->db->from($this->table);
		$this->db->where("register.intIdPelayanan" , $idPelayanan);
		$this->db->where("register.dtTanggalKunjungan BETWEEN '".$date_start."' AND '".$date_end."'");
		$data = $this->db->count_all_results();
		return $data;
	}
	
	public function getDataAntrianApotik($status=1,$jenisApotik=1,$date , $offset , $limit){
		$date_start = "";
		if(!empty($date)){
			$date_start = $date." 00:00:00";	
		}
		$date_end = "";
		if(!empty($date)){
			$date_end = $date." 23:59:59";	
		}
		
		$this->db->select("register.txtIdPasien,register.txtIdKunjungan,register.txtUsiaPasienKunjungan ,register.intNoAntri, pasien.txtNamaPasien,pasien.txtNoAnggota,pasien.dtTanggalLahir,pelayanan.txtNama as txtNamaPelayanan,register.bitStatusKunjungan,register.bitIsPoli,register.bitIsApotik,register.bitIsPayment");
		$this->db->from($this->table);
		$this->db->join("pasien" , "register.txtIdPasien =  pasien.txtIdPasien" , "left");
		$this->db->join("pelayanan" , "register.intIdPelayanan = pelayanan.intIdPelayanan" , "left");
		$this->db->join("jaminan_kesehatan" , "register.intIdJaminanKesehatan = jaminan_kesehatan.intIdJaminanKesehatan" , "left");
		$this->db->where("register.bitIsApotik" , $status);
		$this->db->where("register.bitJenisApotik" , $jenisApotik);
		$this->db->where("register.dtTanggalKunjungan BETWEEN '".$date_start."' AND '".$date_end."'");
		$this->db->order_by("register.dtTanggalKunjungan" , "ASC");
		$this->db->limit($limit);
		$this->db->offset($offset);
		$data = $this->db->get();		
        return $data->result_array();
	}

	public function getRiwayatRawatInap($startDate , $endDate , $intIdKamar){
		$this->db->select("
						rekam_medis_detail.txtIdRekmedDetail,
						rekam_medis_detail.txtIdRekamMedis,
						register.txtIdPasien,
						register.txtIdKunjungan,
						pasien.txtNoAnggota,
						pasien.txtNamaPasien,
						register.txtUsiaPasienKunjungan,
						register.txtKeteranganKunjungan,
						register.dtTanggalKunjungan,
						register.dtSelesaiKunjungan,
						register.bitStatusKunjungan,  
						kamar.txtKamar,
						kelas_kamar.txtKelasKamar");
		$this->db->from($this->table);
		$this->db->join("pasien" , "register.txtIdPasien =  pasien.txtIdPasien" , "left");
		$this->db->join("rekam_medis_detail" , "rekam_medis_detail.txtIdKunjungan =  register.txtIdKunjungan" , "inner");
		$this->db->join("register_kamar" , "register.txtIdKunjungan =  register_kamar.txtIdKunjungan" , "inner");
		$this->db->join("kamar" , "register_kamar.intIdKamar =  kamar.intIdKamar" , "left");
		$this->db->join("kelas_kamar" , "kamar.intIdKelasKamar =  kelas_kamar.intIdKelasKamar" , "left");
		$this->db->where("register.bitStatusKunjungan" , 2);
		$this->db->where("register.bitIsPoli" , 3);

		if(!empty($intIdKamar)){
			$this->db->where("register_kamar.intIdKamar" , $intIdKamar);
		}
		$this->db->where("register.dtTanggalKunjungan BETWEEN '".$startDate." 00:00:00' AND '".$endDate." 23:59:59'");
		$this->db->order_by("register.dtTanggalKunjungan" , "DESC");
		$data = $this->db->get();
        return $data->result_array();
	}

	public function getAntrianApotik($status=1,$jenisApotik=1,$date){
		$date_start = "";
		if(!empty($date)){
			$date_start = $date." 00:00:00";	
		}
		$date_end = "";
		if(!empty($date)){
			$date_end = $date." 23:59:59";	
		}
		
		$this->db->select("register.txtIdPasien,register.txtIdKunjungan,register.txtUsiaPasienKunjungan ,register.intNoAntri, pasien.txtNamaPasien,pasien.txtNoAnggota,pasien.dtTanggalLahir,pelayanan.txtNama as txtNamaPelayanan,register.bitStatusKunjungan,register.bitIsPoli,register.bitIsApotik,register.bitIsPayment");
		$this->db->from($this->table);
		$this->db->join("pasien" , "register.txtIdPasien =  pasien.txtIdPasien" , "left");
		$this->db->join("pelayanan" , "register.intIdPelayanan = pelayanan.intIdPelayanan" , "left");
		$this->db->join("jaminan_kesehatan" , "register.intIdJaminanKesehatan = jaminan_kesehatan.intIdJaminanKesehatan" , "left");
		$this->db->where_in("register.bitIsApotik" , array(1,2));
		$this->db->where("register.bitJenisApotik" , $jenisApotik);
		$this->db->where("register.dtTanggalKunjungan BETWEEN '".$date_start."' AND '".$date_end."'");
		$this->db->order_by("register.dtTanggalKunjungan" , "ASC");
		$data = $this->db->get();		
        return $data->result_array();
	}

	public function getAntrianKasir($date){
		$date_start = "";
		if(!empty($date)){
			$date_start = $date." 00:00:00";	
		}
		$date_end = "";
		if(!empty($date)){
			$date_end = $date." 23:59:59";	
		}
		
		$this->db->select("register.txtIdPasien,register.txtIdKunjungan,register.txtUsiaPasienKunjungan ,register.intNoAntri, pasien.txtNamaPasien,pasien.txtNoAnggota,pasien.dtTanggalLahir,pelayanan.txtNama as txtNamaPelayanan,register.bitStatusKunjungan,register.bitIsPoli,register.bitIsApotik,register.bitIsPayment");
		$this->db->from($this->table);
		$this->db->join("pasien" , "register.txtIdPasien =  pasien.txtIdPasien" , "left");
		$this->db->join("pelayanan" , "register.intIdPelayanan = pelayanan.intIdPelayanan" , "left");
		$this->db->join("jaminan_kesehatan" , "register.intIdJaminanKesehatan = jaminan_kesehatan.intIdJaminanKesehatan" , "left");
		$this->db->where_in("register.bitIsPayment" , array(1,2));
		$this->db->where("register.dtTanggalKunjungan BETWEEN '".$date_start."' AND '".$date_end."'");
		$this->db->order_by("register.dtTanggalKunjungan" , "ASC");
		$data = $this->db->get();		
        return $data->result_array();
	}
	
	public function getCountDataAntrianApotik($status=1,$jenisApotik=1,$date){
		$date_start = "";
		if(!empty($date)){
			$date_start = $date." 00:00:00";	
		}
		$date_end = "";
		if(!empty($date)){
			$date_end = $date." 23:59:59";	
		}
		
		$this->db->from($this->table);
		$this->db->join("pasien" , "register.txtIdPasien =  pasien.txtIdPasien" , "left");
		$this->db->join("pelayanan" , "register.intIdPelayanan = pelayanan.intIdPelayanan" , "left");
		$this->db->join("jaminan_kesehatan" , "register.intIdJaminanKesehatan = jaminan_kesehatan.intIdJaminanKesehatan" , "left");
		$this->db->where("register.bitIsApotik" , $status);
		$this->db->where("register.bitJenisApotik" , $jenisApotik);
		if(!empty($idPelayanan)){
			$this->db->where("register.intIdPelayanan" , $idPelayanan);
		}
		
		$this->db->where("register.dtTanggalKunjungan BETWEEN '".$date_start."' AND '".$date_end."'");
		$data = $this->db->count_all_results();		
        return $data;
	}
	
	public function getDataAntrian($poli="poli" , $status=1, $id_pelayanan=1 , $date = "" ,$offset=0 , $limit=10){
		$date_start = "";
		if(!empty($date)){
			$date_start = $date." 00:00:00";	
		}
		$date_end = "";
		if(!empty($date)){
			$date_end = $date." 23:59:59";	
		}
		
		$this->db->select("register.txtIdPasien,register.txtIdKunjungan,register.txtUsiaPasienKunjungan ,register.intNoAntri, pasien.txtNamaPasien,pasien.txtNoAnggota,pasien.dtTanggalLahir,pelayanan.txtNama as txtNamaPelayanan,register.bitStatusKunjungan,register.bitIsPoli,register.bitIsApotik,register.bitIsPayment");
		$this->db->from($this->table);
		$this->db->join("pasien" , "register.txtIdPasien =  pasien.txtIdPasien" , "left");
		$this->db->join("pelayanan" , "register.intIdPelayanan = pelayanan.intIdPelayanan" , "left");
		$this->db->join("jaminan_kesehatan" , "register.intIdJaminanKesehatan = jaminan_kesehatan.intIdJaminanKesehatan" , "left");
		
		if($poli=="poli"){
			$field = "register.bitIsPoli";
		}else if($poli=="apotik"){
			$field = "register.bitIsApotik";
		}else if($poli=="payment"){
			$field = "register.bitIsPayment";
		}else{
			$field = "register.bitIsPoli";
		}
		
		$this->db->where($field , $status);
		
		if(!empty($idPelayanan)){
			$this->db->where("register.intIdPelayanan" , $idPelayanan);
		}
		
		$this->db->where("register.dtTanggalKunjungan BETWEEN '".$date_start."' AND '".$date_end."'");
		$this->db->order_by("register.dtTanggalKunjungan" , "ASC");
		$this->db->limit($limit);
		$this->db->offset($offset);
		$data = $this->db->get();		
        return $data->result_array();
	}

	public function getDataRujukan($date , $idPelayanan){
		$date_start = "";
		if(!empty($date)){
			$date_start = $date." 00:00:00";	
		}
		$date_end = "";
		if(!empty($date)){
			$date_end = $date." 23:59:59";	
		}

		$this->db->select("
						register.txtIdPasien,
						register.txtIdKunjungan,
						register.txtUsiaPasienKunjungan,
						register.intNoAntri,
						register.txtKeteranganKunjungan,
						register.bitPeriksaLanjut,  
						pasien.txtNamaPasien,
						pasien.txtNoAnggota,
						pasien.dtTanggalLahir,
						pelayanan.txtNama as txtNamaPelayanan,
						pelayanan_referr.txtNama as txtNamaPelayananReferrer,
						register.bitStatusKunjungan,
						register.bitIsPoli");
		
		$this->db->from($this->table);
		$this->db->join("pasien" , "register.txtIdPasien =  pasien.txtIdPasien" , "left");
		$this->db->join("pelayanan" , "register.intIdPelayanan = pelayanan.intIdPelayanan" , "left");
		$this->db->join("pelayanan as pelayanan_referr" , "register.intRefIdPelayanan = pelayanan_referr.intIdPelayanan" , "left");
		if(!empty($idPelayanan)){
			$this->db->where("register.intRefIdPelayanan" , $idPelayanan);
		}
		$this->db->where_in("register.bitIsPoli" , array("1" , "2" , "3"));
		$this->db->where("register.bitPeriksaLanjut!=",3);
		$this->db->where("register.dtTanggalKunjungan BETWEEN '".$date_start."' AND '".$date_end."'");
		$data = $this->db->get();		
        return $data->result_array();
	}
	
	public function getCountDataAntrian($poli="poli" , $status=1, $id_pelayanan=1 , $date = ""){
		$date_start = "";
		$date_end = "";
		if(!empty($date)){
			$date_start = $date." 00:00:00";	
		}
		
		if(!empty($date)){
			$date_end = $date." 23:59:59";	
		}
		
		$this->db->select("register.txtIdPasien,register.txtIdKunjungan,register.txtUsiaPasienKunjungan ,register.intNoAntri, pasien.txtNamaPasien,pasien.txtNoAnggota,pasien.dtTanggalLahir,pelayanan.txtNama as txtNamaPelayanan,register.bitStatusKunjungan,register.bitIsPoli,register.bitIsApotik,register.bitIsPayment");
		$this->db->from($this->table);
		
		if($poli=="poli"){
			$field = "register.bitIsPoli";
		}else if($poli=="apotik"){
			$field = "register.bitIsApotik";
		}else if($poli=="payment"){
			$field = "register.bitIsPayment";
		}else{
			$field = "register.bitIsPoli";
		}
		
		$this->db->where($field , $status);
		if(!empty($idPelayanan)){
			$this->db->where("register.intIdPelayanan" , $idPelayanan);
		}
		
		$this->db->where("register.dtTanggalKunjungan BETWEEN '".$date_start."' AND '".$date_end."'");
		$data = $this->db->count_all_results();
        return $data;
	}
    
    public function getDataIndex($offset = 0 , $limit = 10, $search = ""){
		
		if(!empty($search)){
			$this->db->where($search);
		}

        $this->db->select($this->table.'.* ');
		$this->db->from($this->table);
		$this->db->limit($limit);
		$this->db->offset($offset);
        $data = $this->db->get();
        return $data->result_array();
    }

    public function getCountDataIndex($search=""){
    	if(!empty($search)){
			$this->db->where($search);
		}
		$this->db->select($this->table.'.* ');	
		$this->db->from($this->table);
		$data = $this->db->count_all_results();	
        return $data;
    }
	
	public function getKunjunganReferral($idKunjungan){
		$this->db->from($this->table);
		$this->db->where("bitPeriksaLanjut!=" ,0);
		$this->db->where_in("bitIsPoli" ,array(1,2));
		$this->db->where("txtRefIdKunjungan" , $idKunjungan);
		///$this->db->where("bitIsPoli!=",3);
		$data = $this->db->get();
        return $data->row_array();
	}

	
	
	public function getDetail($id){
		$this->db->where('register.'.$this->primary_key , $id)
		->join('pasien', 'pasien.txtIdPasien = register.txtIdPasien')
		->join('jaminan_kesehatan', 'jaminan_kesehatan.intIdJaminanKesehatan = pasien.intIdJaminanKesehatan' , 'left')
		->join('pelayanan', 'pelayanan.IntIdPelayanan = register.IntIdPelayanan')
		->join('rekam_medis_detail', 'rekam_medis_detail.txtIdKunjungan = register.txtIdKunjungan' , 'left')
		->join('pegawai', 'pegawai.IntIdPegawai = rekam_medis_detail.IntIdPegawai' , 'left');
		$query = $this->db->get($this->table,1);
		$resVal = "";
		if($query->num_rows() > 0){
			$resVal = $query->row_array();	
		}else{
			$resVal = false;
		}
		return $resVal;
	}

	public function getDetailKeuangan($id){
		$this->db->where('register.'.$this->primary_key , $id)
		->join('pasien', 'pasien.txtIdPasien = register.txtIdPasien')
		->join('jaminan_kesehatan', 'jaminan_kesehatan.intIdJaminanKesehatan = pasien.intIdJaminanKesehatan')
		->join('pelayanan', 'pelayanan.IntIdPelayanan = register.IntIdPelayanan')
		->join('rekam_medis_detail', 'rekam_medis_detail.txtIdKunjungan = register.txtIdKunjungan')
		->join('pegawai', 'pegawai.IntIdPegawai = rekam_medis_detail.IntIdPegawai')
		->join('keuangan_rawat_jalan', 'keuangan_rawat_jalan.id_kunjungan = register.txtIdKunjungan', 'left');
		$query = $this->db->get($this->table,1);
		$resVal = "";
		if($query->num_rows() > 0){
			$resVal = $query->row_array();	
		}else{
			$resVal = false;
		}
		return $resVal;
	}

	public function getDetailKeuanganRawatInap($id){
		$this->db->where('register.'.$this->primary_key , $id)
		->join('pasien', 'pasien.txtIdPasien = register.txtIdPasien')
		->join('jaminan_kesehatan', 'jaminan_kesehatan.intIdJaminanKesehatan = pasien.intIdJaminanKesehatan')
		->join('pelayanan', 'pelayanan.IntIdPelayanan = register.IntIdPelayanan')
		->join('register_kamar', 'register_kamar.txtIdKunjungan = register.txtIdKunjungan')
        ->join('kamar', 'kamar.intIdKamar = register_kamar.intIdKamar');
		$query = $this->db->get($this->table,1);
		$resVal = "";
		if($query->num_rows() > 0){
			$resVal = $query->row_array();	
		}else{
			$resVal = false;
		}
		return $resVal;
	}

	public function findDetailKeuanganRawatInap($id_kunjungan, $id_keuangan){
		$this->db->where('register.'.$this->primary_key , $id_kunjungan)
		->where('keuangan_rawat_inap.id', $id_keuangan)
		->join('pasien', 'pasien.txtIdPasien = register.txtIdPasien')
		->join('jaminan_kesehatan', 'jaminan_kesehatan.intIdJaminanKesehatan = pasien.intIdJaminanKesehatan')
		->join('pelayanan', 'pelayanan.IntIdPelayanan = register.IntIdPelayanan')
		->join('register_kamar', 'register_kamar.txtIdKunjungan = register.txtIdKunjungan')
        ->join('kamar', 'kamar.intIdKamar = register_kamar.intIdKamar')
        ->join('keuangan_rawat_inap', 'keuangan_rawat_inap.id_kunjungan = register.txtIdKunjungan');		
		$query = $this->db->get($this->table,1);
		$resVal = "";
		if($query->num_rows() > 0){
			$resVal = $query->row_array();	
		}else{
			$resVal = false;
		}
		return $resVal;
	}
	
	public function getDetailRegister($id){
		$this->db->select("*");
		$this->db->where('register.'.$this->primary_key , $id);
		$this->db->join('pelayanan' , 'register.intIdPelayanan = pelayanan.intIdPelayanan');
		$query = $this->db->get($this->table,1);
		$resVal = "";
		if($query->num_rows() > 0){
			$resVal = $query->row_array();	
		}else{
			$resVal = false;
		}
		return $resVal;
	}

	public function getDetailRegisterPelayanan($id){
		$this->db->select("*");
		$this->db->where('register.'.$this->primary_key , $id);
		$this->db->join('pelayanan' , 'register.intIdPelayanan = pelayanan.intIdPelayanan');
		$this->db->join("jenis_pelayanan" , "pelayanan.intIdJenisPelayanan = jenis_pelayanan.intIdJenisPelayanan" ,"left");
		$this->db->join('pasien' , 'pasien.txtIdPasien = register.txtIdPasien' , "left");
		$this->db->join('kelurahan' , 'kelurahan.IDKelurahan = pasien.intIdKelurahan' , "left");
		$this->db->join('jaminan_kesehatan' , 'jaminan_kesehatan.intIdJaminanKesehatan = pasien.intIdJaminanKesehatan' , "left");
		$this->db->join('rekam_medis' , 'pasien.txtIdPasien = rekam_medis.txtIdPasien' , "left");
		$query = $this->db->get($this->table,1);
		$resVal = "";
		if($query->num_rows() > 0){
			$resVal = $query->row_array();	
		}else{
			$resVal = false;
		}
		return $resVal;
	}

	public function getAllDetailPasienRegistered($idKunjungan){
		$this->db->select("*");
		$this->db->where('register.'.$this->primary_key , $idKunjungan);
		$this->db->join('pasien' , 'pasien.txtIdPasien = register.txtIdPasien');
		$this->db->join('rekam_medis' , 'pasien.txtIdPasien = rekam_medis.txtIdPasien');
		$this->db->join('jaminan_kesehatan' , 'jaminan_kesehatan.intIdJaminanKesehatan = pasien.intIdJaminanKesehatan' , 'left');
		$this->db->join('pelayanan' , 'register.intIdPelayanan = pelayanan.intIdPelayanan');
		$this->db->join('jenis_pelayanan' , 'register.intIdPelayanan = pelayanan.intIdPelayanan');
		$query = $this->db->get($this->table,1);
		$resVal = "";
		if($query->num_rows() > 0){
			$resVal = $query->row_array();	
		}else{
			$resVal = false;
		}
		return $resVal;
	}
	
	public function saveData($arrData = array(),$debug=false){
		
		$this->db->set($arrData);
		if($debug){
			$retVal = $this->db->get_compiled_insert($this->table);
		}else{
			$res = $this->db->insert($this->table);
			if(!$res){
				$retVal['error_stat'] = "Data Gagal Di Simpan";
				$retVal['status'] = false;
			}else{
				$retVal['error_stat'] = "Data Berhasil Di Simpan";
				$retVal['status'] = true;
				$retVal['id'] = $this->db->insert_id();
			}
		}
		return $retVal;
	}
	
	public function update($array , $id){
		
		$this->db->where($this->primary_key , $id);
		$query = $this->db->update($this->table, $array);
		if(!$query){
			
			$retVal['error_stat'] = "Data Gagal Di Update";
			$retVal['status'] = false;
		}else{
			$retVal['error_stat'] = "Data Berhasil Di Update";
			$retVal['status'] = true;
			$retVal['id'] = $id;
		}
		
		return $retVal;
	}
	
	public function delete($id){
		$this->db->where($this->primary_key , $id);
		$q = $this->db->delete($this->table);
		$retVal = array();
		if(!$q){
			$retVal['message'] = "Data Gagal Di Hapus";
			$retVal['status'] = false;
		}else{
			$retVal['message'] = "Data Berhasil Di Hapus";
			$retVal['status'] = true;
		}
		
		return $retVal;
	}

	public function getCountRekapPengunjungRawatJalan($idPelayanan , $startDate , $endDate){

		$startDate = $startDate." 00:00:00";
		$endDate = $endDate." 23:59:00";
		
		$this->db->select("register.txtIdPasien,register.txtIdKunjungan,register.txtUsiaPasienKunjungan ,register.intNoAntri, pasien.txtNamaPasien,pasien.txtNoAnggota,pasien.dtTanggalLahir,pelayanan.txtNama as txtNamaPelayanan,register.bitStatusKunjungan,register.bitIsPoli");
		$this->db->from($this->table);
		$this->db->where("register.bitIsPoli" , 3);
		$this->db->where("register.bitStatusKunjungan" , 1);
		if(!empty($idPelayanan)){
			$this->db->where("register.intIdPelayanan" , $idPelayanan);
		}
		$this->db->where("register.dtTanggalKunjungan BETWEEN '".$startDate."' AND '".$endDate."'");
		$this->db->group_by("register.txtIdKunjungan");
		$data = $this->db->count_all_results();
        return $data;
	}

	public function getRekapPengunjungRawatJalan($idPelayanan , $startDate , $endDate , $offset = 0, $limit = 10){

		$startDate = $startDate." 00:00:00";
		$endDate = $endDate." 23:59:00";
		$this->db->select("register.txtIdPasien,
						   register.txtIdKunjungan,
						   register.txtUsiaPasienKunjungan,
						   register.bitIsBaru,
						   register.bitStatusKunjungan,
						   register.bitIsPoli,
						   register.txtKeteranganKunjungan,
						   pasien.txtNamaPasien,
						   pasien.charJkPasien,
						   pasien.txtNoAnggota,
						   pasien.txtWilayahPasien,
						   pasien.intIdKelurahan,
						   kelurahan.Nama as NamaKelurahan,
						   jaminan_kesehatan.txtNamaJaminan as txtJaminanKesehatan,
						   pelayanan.txtNama as txtNamaPelayanan,
						   rekam_medis_diagnosa.intIdDiagnosaPenyakit,
						   rekam_medis_diagnosa.bitIsKasusBaru,
						   rekam_medis_detail.txtPengobatan,
						   data_penyakit.txtIndonesianName,
						   data_penyakit.txtCategory,
						   data_penyakit.txtSubCategory
						   ");
		$this->db->from($this->table);
		$this->db->where("register.bitIsPoli" , 3);
		$this->db->where("register.bitStatusKunjungan" , 1);
		$this->db->join('pasien', 'pasien.txtIdPasien = register.txtIdPasien');
		$this->db->join('pelayanan', 'pelayanan.IntIdPelayanan = register.IntIdPelayanan');
		$this->db->join('jaminan_kesehatan', 'register.intIdJaminanKesehatan = jaminan_kesehatan.intIdJaminanKesehatan' , 'left');
		$this->db->join('rekam_medis_detail', 'rekam_medis_detail.txtIdKunjungan = register.txtIdKunjungan' , 'left');
		$this->db->join('rekam_medis_diagnosa', 'rekam_medis_diagnosa.txtIdRekmedDetail = rekam_medis_detail.txtIdRekmedDetail' , 'left');
		$this->db->join('data_penyakit', 'data_penyakit.intIdPenyakit = rekam_medis_diagnosa.intIdDiagnosaPenyakit' , 'left');
		$this->db->join('kelurahan', 'kelurahan.IDKelurahan = pasien.intIdKelurahan', 'left');

		if(!empty($idPelayanan)){
			$this->db->where("register.intIdPelayanan" , $idPelayanan);
		}

		$this->db->where("register.dtTanggalKunjungan BETWEEN '".$startDate."' AND '".$endDate."'");
		if($limit!='-1'){
			$this->db->offset($offset);
			$this->db->limit($limit);
		}

		$this->db->group_by("register.txtIdKunjungan");
		$data = $this->db->get();
		
        return $data->result_array();
	}

	public function getJumlahKunjungan($startDate , $endDate , $idPelayanan="0"){
		$startDate = $startDate." 00:00:00";
		$endDate = $endDate." 23:59:00";
		$strPelayanan = $idPelayanan!="0" ? " AND P.intIdPelayanan='".$idPelayanan."'" : "";
		$stringQuery = "
		SELECT p.`txtNama` AS NAMA_PELAYANAN, 
		(SELECT COUNT(R1.`txtIdKunjungan`) FROM register R1 WHERE R1.dtTanggalKunjungan BETWEEN '".$startDate."' AND '".$endDate."'  AND R1.intIdPelayanan = p.`intIdPelayanan`) AS jumlah,
		(SELECT COUNT(R1.`txtIdKunjungan`) FROM register R1 WHERE R1.bitIsPoli='3' AND R1.dtTanggalKunjungan BETWEEN '".$startDate."' AND '".$endDate."'  AND R1.intIdPelayanan = p.`intIdPelayanan`) AS jumlah_selesai,
		(SELECT COUNT(R1.`txtIdKunjungan`) FROM register R1 WHERE R1.bitIsPoli='2' AND R1.dtTanggalKunjungan BETWEEN '".$startDate."' AND '".$endDate."' AND R1.intIdPelayanan = p.`intIdPelayanan`) AS jumlah_sedang_dilayani,
		(SELECT COUNT(R1.`txtIdKunjungan`) FROM register R1 WHERE R1.bitIsPoli='1' AND R1.dtTanggalKunjungan BETWEEN '".$startDate."' AND '".$endDate."' AND R1.intIdPelayanan = p.`intIdPelayanan`) AS jumlah_antri,
		(SELECT COUNT(R1.`txtIdKunjungan`) FROM register R1 WHERE R1.bitIsPoli='4' AND R1.dtTanggalKunjungan BETWEEN '".$startDate."' AND '".$endDate."' AND R1.intIdPelayanan = p.`intIdPelayanan`) AS jumlah_rujukan
		FROM pelayanan P
		WHERE 1=1
		".$strPelayanan."
		AND P.intIdJenisPelayanan != 8
		GROUP BY p.`txtNama` ORDER BY NAMA_PELAYANAN DESC
	   	";
	   $data = $this->db->query($stringQuery);
	   return $data->result_array();	
	}

	public function getJumlahKunjunganByJaminanKesehatan($startDate , $endDate , $idPelayanan){
		$startDate = $startDate." 00:00:00";
		$endDate = $endDate." 23:59:00";
		$strPelayanan = $idPelayanan!="0" ? " AND R.intIdPelayanan='".$idPelayanan."'" : "";
		$stringQuery = "
		SELECT JK.`txtNamaJaminan` AS NamaJaminan, COUNT(R.txtIdKunjungan) AS jumlah
       	FROM jaminan_kesehatan JK
		LEFT JOIN register R ON R.intIdJaminanKesehatan = JK.intIdJaminanKesehatan
		WHERE 1=1
		AND R.dtTanggalKunjungan BETWEEN '".$startDate."' AND '".$endDate."'
		".$strPelayanan."
		GROUP BY JK.`txtNamaJaminan` ORDER BY NamaJaminan ASC
		";
		///echo $stringQuery;die;
		$data = $this->db->query($stringQuery);
	   	return $data->result_array();	
	}

	public function getRekapDataPenyakit($startDate , $endDate , $idPelayanan , $arrPenyakit = ""){
		$startDate = $startDate." 00:00:00";
		$endDate = $endDate." 23:59:00";
		///$strPelayanan = $idPelayanan!="0" ? " AND R.intIdPelayanan='".$idPelayanan."'" : "";
		$this->db->select("DP.txtCategory , DP.txtSubCategory , DP.txtIndonesianName, COUNT(DP.`intIdPenyakit`) AS jumlah");
		$this->db->from("data_penyakit DP");
		$this->db->join("rekam_medis_diagnosa RMDi" , "RMDi.intIdDiagnosaPenyakit = DP.intIdPenyakit");
		$this->db->join("rekam_medis_detail RMD" , "RMD.txtIdRekmedDetail = RMDi.txtIdRekmedDetail");
		$this->db->join("register R" , "R.txtIdKunjungan = RMD.txtIdKunjungan");
		$this->db->where("R.dtTanggalKunjungan BETWEEN '".$startDate."' AND '".$endDate."'");

		if($idPelayanan!=0){
			$this->db->where("R.intIdPelayanan" , $idPelayanan);
		}

		if(!empty($arrPenyakit)){
			$this->db->where_in("DP.intIdPenyakit" , $arrPenyakit);
		}

		$this->db->group_by("DP.intIdPenyakit");
		$this->db->order_by("jumlah DESC");
		$data = $this->db->get();
	   	return $data->result_array();	
	}

	public function getRekapTindakan($startDate , $endDate , $idPelayanan){
		$startDate = $startDate." 00:00:00";
		$endDate = $endDate." 23:59:00";
		///$strPelayanan = $idPelayanan!="0" ? " AND R.intIdPelayanan='".$idPelayanan."'" : "";
		$this->db->select("T.txtTindakan, T.txtDeskripsi , T.intIdTindakan, COUNT(T.intIdTindakan) AS jumlah");
		$this->db->from("tindakan T");
		$this->db->join("rekam_medis_tindakan RMDt" , "RMDt.intIdTindakan = T.intIdTindakan");
		$this->db->join("rekam_medis_detail RMD" , "RMD.txtIdRekmedDetail = RMDt.txtIdRekmedDetail");
		$this->db->join("register R" , "R.txtIdKunjungan = RMD.txtIdKunjungan");
		$this->db->where("R.dtTanggalKunjungan BETWEEN '".$startDate."' AND '".$endDate."'");

		if($idPelayanan!=0){
			$this->db->where("R.intIdPelayanan" , $idPelayanan);
		}

		$this->db->group_by("T.intIdTindakan");
		$this->db->order_by("jumlah DESC");
		$data = $this->db->get();
	   	return $data->result_array();
	}

	public function getRekapRujukan($startDate , $endDate){
		$startDate = $startDate." 00:00:00";
		$endDate = $endDate." 23:59:00";
		///$strPelayanan = $idPelayanan!="0" ? " AND R.intIdPelayanan='".$idPelayanan."'" : "";
		$this->db->select("P.txtNama ,COUNT(R.txtIdKunjungan) AS jumlah");
		$this->db->from("pelayanan P");
		$this->db->join("register R" , "R.intIdPelayanan = P.intIdPelayanan");
		$this->db->where("R.dtTanggalKunjungan BETWEEN '".$startDate."' AND '".$endDate."'");
		$this->db->where("P.intIdJenisPelayanan" , 8);
		$this->db->group_by("P.intIdPelayanan");
		$this->db->order_by("jumlah DESC");
		$data = $this->db->get();
	   	return $data->result_array();
	}

	public function getDataPasienRujukan($startDate , $endDate , $intIdPelayanan = 0){
		$startDate = $startDate." 00:00:00";
		$endDate = $endDate." 23:59:00";
		$idPelayananString = $intIdPelayanan!=0 ? " AND 
		PeReff.intIdPelayanan = '".$intIdPelayanan."' " : "";
		$data = $this->db->query("
			SELECT X.*,(SELECT P.txtIndonesianName FROM data_penyakit P WHERE P.intIdPenyakit 
			IN (SELECT RMD1.intIdDiagnosaPenyakit FROM rekam_medis_diagnosa RMD1 WHERE X.txtIdRekmedDetail = RMD1.txtIdRekmedDetail)  LIMIT 1 ) txtnamaIndo
 			FROM (SELECT DISTINCT Pa.txtNoAnggota,`R`.`txtIdKunjungan`, PeReff.txtNama as NamaPelayananAsal , RMD.`txtIdRekmedDetail`, `R`.`dtTanggalKunjungan`, `Pa`.`txtNamaPasien`, `R`.`txtUsiaPasienKunjungan`, `Pa`.`charJkPasien`, `JK`.`txtNamaJaminan`, `R`.`txtNoJaminanKesehatan`, `K`.`Nama` AS `Kecamatan`, 
			`Pe`.`txtNama` AS `NamaPelayanan`, DPL.txtPoliLuar
			FROM `register` `R`
			JOIN `pasien` `Pa` ON `R`.`txtIdPasien` = `Pa`.`txtIdPasien`
			JOIN `pelayanan` `Pe` ON `R`.`intIdPelayanan` = `Pe`.`intIdPelayanan`
			JOIN `pelayanan` `PeReff` ON `R`.`intRefIdPelayanan` = `PeReff`.`intIdPelayanan`
			LEFT JOIN `data_poli_luar` `DPL` ON `R`.`intIdPoliLuar` = `DPL`.`intIdPoliLuar`
			LEFT JOIN `kecamatan` `K` ON `K`.`IDKecamatan` = `Pa`.`intIdKecamatan`
			LEFT JOIN `jaminan_kesehatan` `JK` ON `R`.`intIdJaminanKesehatan` = `JK`.`intIdJaminanKesehatan`
			LEFT JOIN `rekam_medis_detail` `RMD` ON `R`.`txtRefIdKunjungan` = `RMD`.`txtIdKunjungan`
			WHERE 
			`Pe`.`intIdJenisPelayanan` IN ('8' , '13')
			AND `R`.`dtTanggalKunjungan` BETWEEN '".$startDate."' AND '".$endDate."'
			".$idPelayananString."
			ORDER BY `R`.`dtTanggalKunjungan` ASC
			) X 
		");
	   	return $data->result_array();
	}

	public function getDetailPasienRujukan($idKunjungan){
		$data = $this->db->query("
			SELECT X.*,(SELECT P.txtIndonesianName FROM data_penyakit P WHERE P.intIdPenyakit 
			IN (SELECT RMD1.intIdDiagnosaPenyakit FROM rekam_medis_diagnosa RMD1 WHERE X.txtIdRekmedDetail = RMD1.txtIdRekmedDetail)  LIMIT 1 ) txtnamaIndo
 			FROM (SELECT DISTINCT Pa.txtNoAnggota,Pe.intIdPelayanan , DPL.intIDPoliLuar,RMD.txtPemeriksaan,
			 `R`.`txtIdKunjungan`, 
			 PeReff.txtNama as NamaPelayananAsal , 
			 RMD.`txtIdRekmedDetail`, 
			 `R`.`dtTanggalKunjungan`,
			 `R`.bitPeriksaLanjut, 
			 `Pa`.`txtNamaPasien`, 
			 `R`.`txtUsiaPasienKunjungan`, `Pa`.`charJkPasien`, `JK`.`txtNamaJaminan`, `R`.`txtNoJaminanKesehatan`, `K`.`Nama` AS `Kecamatan`, 
			`Pe`.`txtNama` AS `NamaPelayanan`, 
			DPL.txtPoliLuar, 
			R.txtKeteranganKunjungan
			FROM `register` `R`
			JOIN `pasien` `Pa` ON `R`.`txtIdPasien` = `Pa`.`txtIdPasien`
			JOIN `pelayanan` `Pe` ON `R`.`intIdPelayanan` = `Pe`.`intIdPelayanan`
			JOIN `pelayanan` `PeReff` ON `R`.`intRefIdPelayanan` = `PeReff`.`intIdPelayanan`
			LEFT JOIN `data_poli_luar` `DPL` ON `R`.`intIdPoliLuar` = `DPL`.`intIdPoliLuar`
			LEFT JOIN `kecamatan` `K` ON `K`.`IDKecamatan` = `Pa`.`intIdKecamatan`
			LEFT JOIN `jaminan_kesehatan` `JK` ON `R`.`intIdJaminanKesehatan` = `JK`.`intIdJaminanKesehatan`
			LEFT JOIN `rekam_medis_detail` `RMD` ON `R`.`txtRefIdKunjungan` = `RMD`.`txtIdKunjungan`
			WHERE `R`.`txtIdKunjungan` = '".$idKunjungan."'
			) X 
		");
	   	return $data->row_array();
	}


	public function changeStatusApotik($id, $status) {
        $this->db->where('txtIdKunjungan', $id)
        ->set('bitIsApotik', $status)
        ->update('register');
    }
	
	public function changeStatusPayment($id, $status) {
         $this->db->where('txtIdKunjungan', $id)
         ->set('bitIsPayment', $status)
         ->update('register');
     }

	 public function checkKunjunganByIdPasien($idPasien){
		 $this->db->from("register");
		 $this->db->where("txtIdPasien" , $idPasien);
		 $this->db->like("dtTanggalKunjungan" , date("Y"));
		 $count = $this->db->count_all_results();
		 if($count < 1){
			 return true;
		 }else{
			 return false;
		 }
	 }

	 public function getDataAntrianRawatJalan($start_date , $end_date , $intIdPoli){
		 if(empty($date_start)){
			$date_start = date("Y-m-d")." 00:00:00";	
		}
		
		if(empty($date_end)){
			$date_end = date("Y-m-d H:i:s");	
		}
		
		$this->db->select("
						register.txtIdPasien,
						register.txtIdKunjungan,
						register.txtUsiaPasienKunjungan,
						register.intNoAntri,
						register.txtKeteranganKunjungan,  
						pasien.txtNamaPasien,
						pasien.txtNoAnggota,
						pasien.dtTanggalLahir,
						pelayanan.txtNama as txtNamaPelayanan,
						pelayanan_referr.txtNama as txtNamaPelayananReferrer,
						register.bitStatusKunjungan,
						register.bitIsPoli");
		$this->db->from($this->table);
		$this->db->join("pasien" , "register.txtIdPasien =  pasien.txtIdPasien" , "left");
		$this->db->join("pelayanan" , "register.intIdPelayanan = pelayanan.intIdPelayanan" , "left");
		$this->db->join("pelayanan as pelayanan_referr" , "register.intRefIdPelayanan = pelayanan_referr.intIdPelayanan" , "left");
		$this->db->join("jaminan_kesehatan" , "register.intIdJaminanKesehatan = jaminan_kesehatan.intIdJaminanKesehatan" , "left");
		$this->db->where_in("register.bitIsPoli" , array(1,2));

		if(!empty($intIdPoli)){
			$this->db->where("register.intIdPelayanan" , $intIdPoli);
		}
		
		$this->db->where("register.dtTanggalKunjungan BETWEEN '".$date_start."' AND '".$date_end."'");
		$this->db->order_by("register.dtTanggalKunjungan" , "ASC");
		$this->db->where("register.bitStatusKunjungan" , 1);
		$data = $this->db->get();
        return $data->result_array();
	 }


	 public function getJumlahAntrianPerPoli($date , $poli = ""){
		$date_start = $date." 00:00:00";
		$date_end = $date." ".date("H:i:s");
		
		 $this->db->from($this->table);
		 $this->db->where("register.dtTanggalKunjungan BETWEEN '".$date_start."' AND '".$date_end."'");
		 if(!empty($poli)) {
			 $this->db->where("intIdPelayanan" , $poli);
		 }
		 $this->db->where("bitIsPoli" , 1);
		 $data = $this->db->count_all_results();
		 return $data;
	 }

	 public function getNoAntrianDiLayani($date , $poli = ""){
		 $date_start = $date." 00:00:00";
		 $date_end = $date." ".date("H:i:s");
		 $this->db->select("intNoAntri");
		 $this->db->from($this->table);
		 $this->db->where("register.dtTanggalKunjungan BETWEEN '".$date_start."' AND '".$date_end."'");
		 
		 $this->db->where("intIdPelayanan" , $poli);
		 $this->db->where("bitIsPoli" , 2);
		 $this->db->order_by("register.dtTanggalKunjungan" , "DESC");
		 $data = $this->db->get();
		 return $data->row_array();
	 }
	
	public function getJumlahAntrianApotik($date){
		$date_start = $date." 00:00:00";
		$date_end = $date." ".date("H:i:s");
		
		 $this->db->from($this->table);
		 $this->db->where("register.dtTanggalKunjungan BETWEEN '".$date_start."' AND '".$date_end."'");
		 $this->db->where("bitIsApotik" , 1);
		 $data = $this->db->count_all_results();
		 return $data;
	 }

	 public function getNoAntrianApotik($date ){
		 $date_start = $date." 00:00:00";
		 $date_end = $date." ".date("H:i:s");
		 $this->db->select("intNoAntri");
		 $this->db->from($this->table);
		 $this->db->where("register.dtTanggalKunjungan BETWEEN '".$date_start."' AND '".$date_end."'");
		 $this->db->where("bitIsApotik" , 2);
		 $this->db->order_by("register.dtTanggalKunjungan" , "DESC");
		 $data = $this->db->get();
		 return $data->row_array();
	 }

	 public function deleteRegisterRawatInap($txtIdKunjungan){
		$query = $this->db->query("CALL deleteRegisterInap('".$txtIdKunjungan."')");
		$retVal = array();
		$retVal['status'] = $query;
		if($query){
			$retVal['message'] = "Data Loket berhasil Di Hapus";
		}else{
			$retVal['message'] = "Data Loket Gagal Di Hapus";
		}
		return $retVal;
	}


}

