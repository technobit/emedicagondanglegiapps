<?php
class Setting_model extends CI_Model{

    var $table = "mabsensi_hari";
    var $primary_key = "id";
    
    function __construct() { 
        parent::__construct();
    }

    function getListDay(){
        $this->db->order_by("intDay" , "ASC");
        $query = $this->db->get($this->table);
        return $query->result_array();
    }

    function deleteTable(){
        $ret = $this->db->truncate($this->table);
        return $ret;
    }

    function insertBatchDay($arrPost){

        $ret = false;
        if(!empty($arrPost)){
            $ret = $this->db->insert_batch($this->table , $arrPost);
        }
        
        $retVal['status'] = $ret!= false ? true : false;
        $retVal['message'] = $retVal['status']== true ? "Data Berhasil Di Simpan" : "Data Gagal Di Simpan";
        return $retVal;
    }

    function getJamByIntDay($intDay) {
        $this->db->where("intDay" , $intDay);
        $ret = $this->db->get($this->table);
        return $ret->row_array();
    }
    
}