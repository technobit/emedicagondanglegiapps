<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Rekam_medis_obat_m extends Base_m {

    public function getObat($id_apotik, $id) {
        $txtIdRekmedDetail = $this->getRekmedId($id);
        return $this->db->select('rekam_medis_obat.*, obat.id as id_obat, obat.kode_obat, obat.nama_obat, kemasan.id as id_kemasan, kemasan.kemasan')        
        ->join('obat', 'obat.id = rekam_medis_obat.id_obat')
        ->join('kemasan', 'kemasan.id = obat.id_kemasan', 'left')
        ->where('txtIdRekmedDetail', $txtIdRekmedDetail)
        ->where('id_apotik', $id_apotik)
        ->get('rekam_medis_obat');
    }

    public function createObat($id_apotik, $id, $record) {
        
        ///$id;die;            
        /// Hack Because It Shit
        $id_apotik_stok = 1;
        $this->deleteObat($this->getRekmedId($id));     
        $this->load->model('obat_m');
        $this->load->model('register_model');
        foreach ($record['obat'] as $row) {
            $this->db->insert('rekam_medis_obat', array(
                'txtIdRekmedDetail' => $this->getRekmedId($id),
                'id_apotik' => $id_apotik,
                'id_obat' => $row['id_obat'],
                'jml' => $row['jml'],
                'aturan_minum' => $row['aturan_minum']
            ));
            $this->db->where('id_apotik', $id_apotik)
            ->where('id_obat', $row['id_obat'])
            ->set('stok', 'stok - '.$row['jml'], false)->update('obat_apotik');
            $this->obat_m->update_stok_apotik_periode($id_apotik, $row['id_obat']); 
        }
        $this->register_model->changeStatusApotik($id, 3);
        return true;
    }

    public function deleteObat($id) {
        $rsObat = $this->db->where('txtIdRekmedDetail', $id)->get('rekam_medis_obat')->result();
        $this->load->model('obat_m');
        foreach ($rsObat as $obat) {
            $this->db->set('stok', 'stok + '.$obat->jml, false)
            ->where('id_obat', $obat->id_obat)
            ->where('id_apotik', $obat->id_apotik)
            ->update('obat_apotik');
            $this->db->where('id', $obat->id)->delete('rekam_medis_obat');
            $this->obat_m->update_stok_apotik_periode($obat->id_apotik, $obat->id_obat); 
        }
        $this->db->where('id', $id)->delete('pengambilan_obat');
        $this->register_model->changeStatusApotik($id, 1);
    }    

    public function getRekmedId($id) {
        $rekmed = $this->db->select('txtIdRekmedDetail')
        ->where('txtIdKunjungan', $id)
        ->get('rekam_medis_detail')
        ->row();
        return $rekmed->txtIdRekmedDetail;

    }

    public function getDetailObatByRekmedId($id){
        $data =  $this->db->from("rekam_medis_obat")
        ->where("txtIdRekmedDetail" , $id)
        ->join("obat" , "obat.id = rekam_medis_obat.id_obat")
        ->get();
        return $data->result_array();
        
    }

    public function getRekapitulasiPemakaianObat($id_apotik , $start_date , $end_date , $limit , $offset){
        $start_date = $start_date." 00:00:00";
		$end_date = $end_date." 23:59:00";

        $this->db->select("rekam_medis_obat.id_obat ,obat.kode_obat , obat.nama_obat, SUM(rekam_medis_obat.jml) AS jumlah ");
        $this->db->from("rekam_medis_obat");
        $this->db->join("obat" , "obat.id = rekam_medis_obat.id_obat");
        $this->db->join("rekam_medis_detail" , "rekam_medis_detail.txtIdRekmedDetail = rekam_medis_obat.txtIdRekmedDetail");
        $this->db->join("register" , "rekam_medis_detail.txtIdKunjungan = register.txtIdKunjungan");
        $this->db->where("rekam_medis_obat.id_apotik" , $id_apotik);
        $this->db->where("register.dtTanggalKunjungan BETWEEN '".$start_date."' AND '".$end_date."'");
        $this->db->group_by("rekam_medis_obat.id_obat");
        $this->db->order_by("jumlah DESC");
        if($limit!="-1"){
            $this->db->limit($limit);
            $this->db->offset($offset);
        }
        
        $data = $this->db->get();
	   	return $data->result_array();
    }

    public function getCountRekapPemakaianObat($id_apotik , $start_date , $end_date){
        $start_date = $start_date." 00:00:00";
		$end_date = $end_date." 23:59:00";
        $this->db->select("rekam_medis_obat.id_obat ,obat.kode_obat , obat.nama_obat, SUM(rekam_medis_obat.jml) AS jumlah ");
        $this->db->from("rekam_medis_obat");
        $this->db->join("obat" , "obat.id = rekam_medis_obat.id_obat");
        $this->db->join("rekam_medis_detail" , "rekam_medis_detail.txtIdRekmedDetail = rekam_medis_obat.txtIdRekmedDetail");
        $this->db->join("register" , "rekam_medis_detail.txtIdKunjungan = register.txtIdKunjungan");
        $this->db->where("rekam_medis_obat.id_apotik" , $id_apotik);
        $this->db->where("register.dtTanggalKunjungan BETWEEN '".$start_date."' AND '".$end_date."'");
        $this->db->group_by("rekam_medis_obat.id_obat");
        $this->db->order_by("jumlah DESC");
        $data = $this->db->count_all_results();
	   	return $data;
    }

}