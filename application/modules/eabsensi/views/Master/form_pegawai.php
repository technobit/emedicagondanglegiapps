<section class="content-header">
  <h1 class="title"><?=$title?></h1>
  <?=$breadcrumbs?>
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
    <div class="box box-success">
    <div class="box-header">
    <h3 class="box-title">Form Pegawai</h3>
    </div>
        <form class="form-horizontal" id="frm-input" method="POST" action="">
          <div class="box-body">
                <?=$this->form_builder->inputHidden("intIdPegawai" , "")?>
                <?=$this->form_builder->inputText("No Induk Pegawai" , "txtNoIndukPegawai" , "")?>
                <?=$this->form_builder->inputText("Nama Pegawai" , "txtNamaPegawai" , "")?>
                <?=$this->form_builder->inputText("Alamat Pegawai" , "txtAlamatPegawai" , "")?>
                <div class="form-group">
                  <label class="col-sm-3 control-label form-label">No. Handphone</label>
                  <div class="col-sm-9">
                    <div class="input-group">
                    <span class="input-group-addon">+62 / 0</span>
                    <?=form_input("txtNoHandphone" , "" , 'class="form-control" id="txtNoHandphone"')?>  
                    </div>
                  </div>
                </div>
                <?=$this->form_builder->inputDropdown("Jabatan" , "intJabatanPegawai" , "" , $jabatan_list)?>
                <?=$this->form_builder->inputDropdown("Status" , "bitStatusPegawai" , "" , $status_list)?>        
                <div class="col-sm-3 col-sm-offset-3">
                <button class="btn btn-primary btn-success" id="saveBtn" type="button"><i class="fa fa-save"></i> Simpan</button>
                <a href="<?=$link_index?>" class="btn btn-danger btn-flat" id="cancelButton"><i class="fa fa-ban"></i> Batal</a>
            </div>
          </div>
        </form>
    </div>
    </div>
  </div>
</section>