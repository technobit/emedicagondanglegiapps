<?php 
  $arrForm = array(
    "intIDPegawai",
    "intIDAbsensi",
    "dtAbsensiMasuk",
    "dtAbsensiKeluar",
    "intStatus",
  );

foreach($arrForm as $valForm) {
  $$valForm = isset($detailAbsensi[$valForm]) ? $detailAbsensi[$valForm] : ""; 
}
$dtTanggal = "";
$dtJamMasuk = "";
$dtJamKeluar = "";

if(!empty($dtAbsensiMasuk)){
  $dtAbsensiMasuk = explode(" " , $dtAbsensiMasuk);
  $dtTanggal = $dtAbsensiMasuk[0];
  $dtJamMasuk = $dtAbsensiMasuk[1];
}

if((!empty($dtAbsensiKeluar)) && ($dtAbsensiKeluar!="0000-00-00 00:00:00")){
  $dtAbsensiKeluar = explode(" " , $dtAbsensiKeluar);
  $dtTanggal = $dtAbsensiKeluar[0];
  $dtJamKeluar = $dtAbsensiKeluar[1];
}


?>
<section class="content-header">
  <h1 class="title"><?=$title?></h1>
  <?=$breadcrumbs?>
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
    <div class="box box-success">
    <div class="box-header">
    <h3 class="box-title">Form Absensi</h3>
    </div>
        <form class="form-horizontal" id="frm-input-absensi" method="POST" action="">
          <div class="box-body">
                <?=$this->form_builder->inputHidden("intIDAbsensi" , $intIDAbsensi)?>
                <?=$this->form_builder->inputDropdown("Pegawai" , "intIdPegawai" , $intIDPegawai , $list_pegawai)?>
                <?=$this->form_builder->inputText("Tanggal" , "dtTanggal" , $dtTanggal)?>
                <?=$this->form_builder->inputText("Jam Masuk" , "dtJamMasuk" , $dtJamMasuk)?>
                <?=$this->form_builder->inputText("Jam Keluar" , "dtJamKeluar" , $dtJamKeluar)?>
                <?=$this->form_builder->inputDropdown("Status" , "bitStatusAbsensi" , $intStatus , $status_list)?>        
                <div class="col-sm-3 col-sm-offset-3">
                <button class="btn btn-primary btn-success" id="saveBtn" type="button"><i class="fa fa-save"></i> Simpan</button>
            </div>
          </div>
        </form>
    </div>
    </div>
  </div>
</section>