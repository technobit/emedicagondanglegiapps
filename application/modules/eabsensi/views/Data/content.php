<section class="content-header">
  <h1 class="title"><?=$title?></h1>
  <?=$breadcrumbs?>
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
    <!-- Hasil Pencarian Data Pasien -->
    <div class="box box-success">
    <!-- form start -->
        <form class="form-horizontal" id="frm-filter">
          <div class="box-body">
            <div class="form-group">
                <label class="col-md-2 control-label">Tanggal</label>
                <div class="col-md-5">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        <input value="<?=date('Y-m-01')?>" id="filterTanggalAwal" class="form-control datepicker" type="text">
                    </div>
                </div>  
                <div class="col-md-5">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        <input value="<?=date('Y-m-d')?>" id="filterTanggalAkhir" class="form-control datepicker" type="text">
                    </div>
            </div>                           
            </div>
            <?=$list_status_absensi?>
            <?=$uniqueID?>
            <div class="col-sm-9 col-sm-offset-2" style="padding-left: 8px;">
                <button class="btn btn-primary btn-flat" id="searchBtn" type="button"><i class="fa fa-search"></i> Cari</button>
                <button class="btn btn-info btn-flat" id="refreshBtn" type="button" ><i class="fa fa-list"></i> Refresh</button>
            </div>
          </div>
        </form>
    </div>
      <div class="box box-primary">
        <!-- /.box-header -->
        <div class="box-header">
        <div class="box-tools">
            <a href="<?=$link_input?>" class="btn btn-primary"><i class="fa fa-plus"></i> Masukkan Absensi</a>
            <a href="<?=$link_cek_sidik?>" onclick="verifySidik()" class="btn btn-primary"><i class="fa fa-hand-o-up"></i> Presensi Sidik Jari</a>
        </div>
        </div>
        <div class="box-body">
          <table class="table table-hover" id="table-data">
                <thead>
                  <tr>
                    <th>Nama Pegawai</th>
                    <th>Tanggal</th>
                    <th>Jam Hadir</th>
                    <th>Jam Pulang</th>
                    <th>Status Kehadiran</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  
                </tbody>
              </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<?=html_alert("input_absensi")?>