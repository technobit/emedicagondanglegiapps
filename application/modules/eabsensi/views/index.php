<section class="content-header">
  <h1 class="title"><?=$title?></h1>
  <?=$breadcrumbs?>
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
    <div class="box box-success">
    <!-- form start -->
        <form class="form-horizontal" id="frm-filter">
          <div class="box-body">
            <?=$frm_search?>
            <?=$frm_jabatan?>
            <div class="col-sm-9 col-sm-offset-3" style="padding-left: 8px;">
                <button class="btn btn-primary btn-flat" id="searchBtn" type="button"><i class="fa fa-search"></i> Cari</button>
                <button class="btn btn-info btn-flat" id="refreshBtn" type="button" ><i class="fa fa-list"></i> Refresh</button>
            </div>
          </div>
        </form>
    </div>
      <div class="box box-primary">
        <!-- /.box-header -->
        <div class="box-header">
          <div class="box-tools">
            <a href="<?=$link_add?>" class="btn btn-success"><i class="fa fa-plus"></i> Tambah Pegawai</a>
          </div>
        </div>
        <div class="box-body">
          <table class="table table-hover" id="table-data">
                <thead>
                  <tr>
                    <th>NIP</th>
                    <th>Nama Pegawai</th>
                    <th>Jabatan Pegawai</th>
                    <th>Status</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  
                </tbody>
              </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>