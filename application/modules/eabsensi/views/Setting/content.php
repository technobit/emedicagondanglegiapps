<section class="content-header">
  <h1 class="title"><?=$title?></h1>
  <?=$breadcrumbs?>
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
    <!-- Hasil Pencarian Data Pasien -->
    <div class="box box-success">
    <!-- form start -->
    <div class="box-header">
        <h3 class="box-title">Pengaturan EAbsensi</h3>
    </div>
     <div class="box-body">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_1" data-toggle="tab">Pengaturan Jam</a></li>
              
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="tab_1">
              <form class="form-horizontal" id="frm-setting-day">
              <table class="table">
                <thead>
                    <tr>
                        <th>Hari</th>
                        <th>Jam Masuk</th>
                        <th>Jam Pulang</th>
                    </tr>
                </thead>
                <tbodu>
                <?php 
                    foreach($setting_day as $rowDay) : 
                ?>
                <tr>
                    <td><?=$list_day[$rowDay['intDay']]?></td>
                    <td><?=form_input("dtJamMasuk[".$rowDay['intDay']."]" , $rowDay['dtJamMasuk'],'class="form-control timepicker"' )?></td>
                    <td><?=form_input("dtJamPulang[".$rowDay['intDay']."]" , $rowDay['dtJamPulang'],'class="form-control timepicker"' )?></td>
                    </tr>
                <?php 
                    endforeach;
                ?>
                </tbody>
              </table>
              <div class="text-center ">
                  <button id="btnSimpanTime" class="btn btn-primary btn-flat" type="button"><i class="fa fa-send"></i> Kirim</button>
              </div>
              </form>
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
     </div>
    </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>