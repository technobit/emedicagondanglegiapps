<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Setting extends MY_Controller {

    var $meta_title = "Pengaturan";
    var $meta_desc = "Data - Data Absensi";
    var $main_title = "Data - Data Absensi";
    var $dtBreadcrumbs = array();  
    var $base_url = "";

    public function __construct() {
        parent::__construct();
        $this->base_url = base_url(). "e-absensi/";
        $this->dtBreadcrumbs = array(
            "Home" =>$this->base_url,            
            "E-Absensi" =>$this->base_url ."e-absensi/",
            "Data Absensi" => $this->base_url ."e-absensi/data-absensi/",
        );
        $this->load->model('setting_model');
    }

    public function index() {            
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "container" => $this->_build_setting(),
            "menu_key" => "setting_absensi",
			"akses_key" => "is_view",
            "custom_js" => array(
            ASSETS_JS_URL . "eabsensi/setting/index.js",
                ASSETS_URL."plugins/validate/jquery.validate_1.11.1.min.js",
                ASSETS_URL."plugins/datetimepicker/bootstrap-datetimepicker.min.js",
            ), 
            "custom_css" => array()            
        );  
        $this->_render("default",$dt);  

    }

    private function _build_setting() {        
        
        $list_day = $this->config->item("day_list");
        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['title'] = $this->meta_title;
        $dt['setting_day'] = $this->setting_model->getListDay();
        $dt['list_day'] = $list_day;
        return $this->load->view('Setting/content', $dt, true);
    }

    public function simpanWaktu(){
        if(!$this->input->is_ajax_request()){
            echo "Ilegall";die;
        }

        $post = $this->input->post();

        $retVal = array();
        $retVal['status'] = false;
        $retVal['message'] = "Data Gagal Di Simpan";

        if(!empty($post)) {
            $deleteTable = $this->setting_model->deleteTable();
            if($deleteTable) {
                /// Send
                $arrPost = array();
                $dtJamMasuk = $post['dtJamMasuk'];
                $dtJamPulang = $post['dtJamPulang'];
                $list_day = $this->config->item("day_list"); 
                foreach($list_day as $intDay => $strDay){
                    
                    $arrPost[] = array(
                        "intDay" => $intDay , 
                        "dtJamMasuk" => !empty($dtJamMasuk[$intDay]) ? $dtJamMasuk[$intDay] : "07:15:00",                         
                        "dtJamPulang" => !empty($dtJamPulang[$intDay]) ? $dtJamPulang[$intDay] : "13:15:00",
                    );
                }

                $resQuery = $this->setting_model->insertBatchDay($arrPost);
                $retVal = $resQuery;
            }
        }
        echo json_encode($retVal);
    }
}