<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Dashboard extends MY_Controller {

    var $meta_title = "Master Data - Pegawai";
    var $meta_desc = "Master Data - Pegawai";
    var $main_title = "Master Data - Pegawai";
    var $dtBreadcrumbs = array();  
    var $base_url = "";

    public function __construct() {
        parent::__construct();
        $this->base_url = base_url();
        $this->dtBreadcrumbs = array(
            "Home" =>$this->base_url,            
            "E-Absensi" =>$this->base_url ."e-absensi/",
            "Dashboard" => $this->base_url ."e-absensi/",
        );
        ///$this->load->model('jenis_obat_m');
    }

    public function index() {            
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "container" => $this->_index(),
            "menu_key" => "absensi_apps",
			"akses_key" => "is_view",
            "custom_js" => array(), 
            "custom_css" => array()            
        );  
        $this->_render("default",$dt);  
    }

    private function _index() {        
        
        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['title'] = $this->meta_title;
        $dt['rsJenisObat'] = $this->jenis_obat_m->get();   
        return $this->load->view('jenis_obat/index', $dt, true);
    }

    public function create() {
        if ($post = $this->input->post()) {
            $this->validation();
            $this->jenis_obat_m->insert($post);
            $this->redirect->with('successMessage', 'Data kemasan berhasil ditambahkan.')->to('data-jenis-obat');
        }
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "menu_key" => "gudang_apotik_data_kemasan",
			"akses_key" => "is_insert",
            "container" => $this->_form(),
            "custom_js" => array(),
            "custom_css" => array()            
        );  
        $this->_render("default",$dt);  
    }

    public function edit($id) {
        $result = $this->find($id);
        if ($post = $this->input->post()) {
            $this->validation();
            $this->jenis_obat_m->update($id, $post);
            $this->redirect->with('successMessage', 'Data jenis obat berhasil diperbarui.')->to('gudang_apotik/kemasan');
        }
        $this->form->setData($result);
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "menu_key" => "gudang_apotik_data_kemasan",
			"akses_key" => "is_update",
            "container" => $this->_form(),
            "custom_js" => array(),
            "custom_css" => array()            
        );  
        $this->_render("default",$dt);  
    }

    private function _form() {
        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['title'] = $this->meta_title;        
        return $this->load->view('jenis_obat/form', $dt, true);
    }

    public function delete($id) {
        $result = $this->find($id);
        $this->jenis_obat_m->delete($id);
        $this->redirect->with('successMessage', 'Data jenis obat berhasil dihapus.')->to('data-jenis-obat');
    }

    private function find($id) {
        $result = $this->jenis_obat_m->find($id);
        if (!$result) {
            $this->redirect->with('errorMessage', 'Data jenis obat tidak ditemukan.')->back();
        } else {
            return $result;
        }
    }

    private function validation() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('jenis_obat', 'Jenis Obat', 'required');
        if (!$this->form_validation->run()) {
            $this->redirect->withInput()->withValidation()->back();
        }        
    }

}