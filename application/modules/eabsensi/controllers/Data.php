<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Data extends MY_Controller {

    var $meta_title = "Data - Data Absensi";
    var $meta_desc = "Data - Data Absensi";
    var $main_title = "Data - Data Absensi";
    var $dtBreadcrumbs = array();  
    var $base_url = "";

    public function __construct() {
        parent::__construct();
        $this->base_url = base_url(). "e-absensi/";
        $this->dtBreadcrumbs = array(
            "Home" =>$this->base_url,            
            "E-Absensi" =>$this->base_url ."e-absensi/",
            "Data Absensi" => $this->base_url ."e-absensi/data-absensi/",
        );
        $this->load->model('pegawai_model');
        $this->load->model('absensi_model');
        $this->load->model("setting_model");
    }

    public function index() {            
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "container" => $this->_build_pegawai(),
            "menu_key" => "data_absensi",
			"akses_key" => "is_view",
            "custom_js" => array(
                ASSETS_JS_URL . "eabsensi/data/index.js",
                ASSETS_URL."plugins/moment/moment.min.js",
				ASSETS_URL."plugins/flexcode/js/jquery.timer.js",
            ), 
            "custom_css" => array()            
        );  
        $this->_render("default",$dt);  

    }

    private function _build_pegawai() {        
        
        $dataStatus= $this->absensi_model->getListStatus();

        $status_absensi = arrayDropdown($dataStatus , 'intStatus' , 'txtStatus' , array("0" => "-Semua-"));
        ////$status_absensi["0"] = "-Semua-";
        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['title'] = $this->meta_title;
        $dt['list_status_absensi'] = $this->form_builder->inputDropdown("Status" , "intIdStatus" , "0" , $status_absensi , array() , "col-sm-2");
        $uniqueID = getIdVerificationPegawai();
        $dt['link_cek_sidik'] = 'EmedicaAbsensi://open?id='.$uniqueID;
        $dt['uniqueID'] = $this->form_builder->inputHidden("uniqueID" , $uniqueID);
        $dt['link_input'] = $this->base_url . "input-absensi/";   
        return $this->load->view('Data/content', $dt, true);
    }

    public function insertPresensi(){
        
        if(!$this->input->is_ajax_request()){
            echo "Ilegall";die;
        }

        $dayNowIndex = date("w");
        $intIdPegawai = $this->input->post("intIdPegawai");
        $dateServer = date("Y-m-d");
        $timeServer = date("H:i:s");
        $dateTimeServer = $dateServer . " ".$timeServer;
        $timeStampInsert = strtotime($dateServer." ".$timeServer);

        $getDay = $this->setting_model->getJamByIntDay($dayNowIndex);
        $jamMasuk = strtotime($dateServer." ".$getDay['dtJamMasuk']);
        $jamPulang = strtotime($dateServer." ".$getDay['dtJamPulang']);

        $checkData = $this->absensi_model->checkPresensiPegawai($intIdPegawai , $dateServer);
        $intIdAbsensi = !empty($checkData) ? $checkData['intIDAbsensi'] : "";
        $arrData = array(
            "intIDPegawai" => $intIdPegawai,
        );
        $status = 1;
        $dtAbsensiMasuk = $dateTimeServer;
        $dtAbsensiKeluar = "0000-00-00 00:00:00";
        if($timeStampInsert <= $jamMasuk){
            //// Masuk
            $dtAbsensiMasuk = $dateTimeServer;
        }

        if(($timeStampInsert >= $jamMasuk) && ($timeStampInsert <= $jamPulang)){
            /// Insert Terlambat
            if(empty($intIdAbsensi)){
                $status = 6;
            }else{
                
                $intIdAbsensi = "";
                $dtAbsensiMasuk = $checkData['dtAbsensiMasuk'];
                $dtAbsensiKeluar = $dateTimeServer;
            }
        }else if($timeStampInsert >= $jamPulang){
            /// Pulang - Update
            
            if(!empty($intIdAbsensi)){
                $dtAbsensiMasuk = $checkData['dtAbsensiMasuk'];
                $dtAbsensiKeluar = $dateTimeServer;
            }else{    
                $status = 1; /// Undefined
                $dtAbsensiMasuk = $dateTimeServer;
                $dtAbsensiKeluar = $dateTimeServer;
            }            
        }

        $arrData['intStatus'] = $status;
        $arrData['dtAbsensiMasuk'] = $dtAbsensiMasuk;
        $arrData['dtAbsensiKeluar'] = $dtAbsensiKeluar;
        $resData = $this->absensi_model->insertUpdate($arrData , $intIdAbsensi);
        echo json_encode($resData);
    }

    public function createAbsensi($intIdAbsensi = ""){

        $post = $this->input->post();

        if(!empty($post)) {     
            $primary = $post['intIDAbsensi'];
            $dtAbsensiMasuk = !empty($post['dtJamMasuk']) ? $post['dtTanggal'] . " " . $post['dtJamMasuk'] : "0000-00-00 00:00:00";
            $dtAbsensiKeluar = !empty($post['dtJamKeluar']) ? $post['dtTanggal'] . " " . $post['dtJamKeluar'] : "0000-00-00 00:00:00";
            $arrData = array(
                "intIDPegawai" => $post['intIdPegawai'],
                "dtAbsensiMasuk" => $dtAbsensiMasuk,
                "dtAbsensiKeluar" => $dtAbsensiKeluar,
                "intStatus" => $post['bitStatusAbsensi']
            );

            $resData = $this->absensi_model->insertUpdate($arrData , $primary);
            $this->session->set_flashdata("input_absensi" , $resData);
            redirect($this->base_url);
        }

        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "container" => $this->_build_absensi($intIdAbsensi),
            "menu_key" => "data_absensi",
			"akses_key" => "is_insert",
            "custom_js" => array(
                ASSETS_URL."plugins/select2/select2.js",
				ASSETS_URL."plugins/validate/jquery.validate_1.11.1.min.js",
                ASSETS_URL."plugins/datetimepicker/bootstrap-datetimepicker.min.js",
                ASSETS_JS_URL . "eabsensi/data/form.js"
            ), 
            "custom_css" => array(
                ASSETS_URL."plugins/select2/select2.css",
                ASSETS_URL."plugins/datetimepicker/bootstrap-datetimepicker.css",
            )            
        );  
        $this->_render("default",$dt);  
    }

    private function _build_absensi($intIdAbsensi){

        $dataAbsensi = array();
        if(!empty($intIdAbsensi)){
            $dataAbsensi = $this->absensi_model->detailAbsensiPegawai($intIdAbsensi);
        }

        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['title'] = $this->meta_title;
        $dataListPegawai = $this->pegawai_model->getListPegawai();
        $dt['list_pegawai'] = arrayDropdown($dataListPegawai , "intIdPegawai" , "txtNamaPegawai" , array("" => "-Pilih Pegawai-"));
        $dt['detailAbsensi'] = $dataAbsensi;
        $dt['status_list'] = $this->config->item("status_absensi");
        return $this->load->view('Data/form', $dt, true);
    }

    /// Ajax Function 
    public function deleteDataAbsensi(){
        if(!$this->input->is_ajax_request()){
            echo "Ilegall"; die;
        }

        $intIdAbsensi = $this->input->post("intIdAbsensi");
        $retVal = $this->absensi_model->delete($intIdAbsensi);
        echo json_encode($retVal);
    }

    public function getDataAbsensi(){
        if(!$this->input->is_ajax_request()){
            echo "Ilegall"; die;
        }

        $dtStart = $this->input->post("dtStart");
        $dtEnd = $this->input->post("dtEnd") ;
        $intIdStatus = $this->input->post("intIdStatus");
        $dataAbsensi = $this->absensi_model->getDataAbsensi($dtStart , $dtEnd , $intIdStatus);
        
        $retVal['data'] = array();
        if(!empty($dataAbsensi)){ 
            foreach($dataAbsensi as $rowAbsensi) {
                $tanggalMasuk = explode(" " , $rowAbsensi['dtAbsensiMasuk']);
                $tanggalKeluar = explode(" " , $rowAbsensi['dtAbsensikeluar']);
                $date = !empty($tanggalMasuk[0]) ? $tanggalMasuk[0] : (!empty($tanggalKeluar[0])) ? $tanggalKeluar[0] : "0000-00-00";
                $jamMasuk = !empty($tanggalMasuk[1]) ? $tanggalMasuk[1] : "";
                $jamKeluar = !empty($tanggalKeluar[1]) ? $tanggalKeluar[1] : "";

                $intIdAbsensi = $rowAbsensi['intIDAbsensi'];
                $btnHapus = '<button class="btn btn-primary btn-flat btn-xs" onclick="deleteAbsensi('.$rowAbsensi['intIDAbsensi'].')"><i class="fa fa-trash"></i> Hapus</button>';
                $btnEdit = '<a class="btn btn-warning btn-flat btn-xs" href="'.$this->base_url_site.'e-absensi/edit-absensi/'.$intIdAbsensi.'"><i class="fa fa-edit"></i> Edit</a>';
                $btnAksi = $btnEdit. $btnHapus;
                $arrData = array(
                    $rowAbsensi['txtNamaPegawai'],
                    $date,
                    $jamMasuk,
                    $jamKeluar,
                    $rowAbsensi['txtStatus'],
                    $btnAksi
                );
                $retVal['data'][] = $arrData;
            }
        }
        echo json_encode($retVal);
    }

}