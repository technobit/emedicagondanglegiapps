<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Report extends MY_Controller {

    var $meta_title = "Data - Data Absensi";
    var $meta_desc = "Data - Data Absensi";
    var $main_title = "Data - Data Absensi";
    var $dtBreadcrumbs = array();  
    var $base_url = "";

    public function __construct() {
        parent::__construct();
        $this->_checkValidateUser();
        $this->base_url = base_url(). "e-absensi/";
        $this->dtBreadcrumbs = array(
            "Home" =>$this->base_url,            
            "E-Absensi" =>$this->base_url ."e-absensi/",
            "Report" => $this->base_url ."e-absensi/report/",
        );
        $this->load->model('pegawai_model');
        $this->load->model('report_model');
        $this->load->model('status_absensi_model');
        $this->load->library("Excel");
    }

    public function harian(){
        $dt = array(
            "title" => "Report - Rekap Harian Absensi",
            "description" => $this->meta_desc,
            "container" => $this->_build_harian(),
            "menu_key" => "rekap_harian_eabsensi",
			"akses_key" => "is_view",
            "custom_js" => array(
                ASSETS_JS_URL . "eabsensi/report/harian.js"
            ), 
            "custom_css" => array()            
        );  
        $this->_render("default",$dt);  
    }

    public function _build_harian(){
        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['title'] = "Report - Rekap Harian Absensi";
        return $this->load->view('Report/harian', $dt, true);
    }

    /// Report Absensi
    public function detail($startDate="" , $endDate = "" , $intIdPegawai = "" , $intIdStatus = "") {            
        $dt = array(
            "title" => "Report - Detail Report Absensi",
            "description" => $this->meta_desc,
            "container" => $this->__build_detail($startDate , $endDate , $intIdPegawai , $intIdStatus),
            "menu_key" => "rekap_kehadiran_eabsensi",
			"akses_key" => "is_view",
            "custom_js" => array(
                ASSETS_JS_URL . "eabsensi/report/detail.js"
            ), 
            "custom_css" => array()            
        );  
        $this->_render("default",$dt);  

    }

    private function __build_detail($startDate , $endDate , $intIdPegawai , $intIdStatus) {        
        $dataStatus = $this->status_absensi_model->getDataStatus();
        $dataPegawai = $this->pegawai_model->getListPegawai();
        
        $listStatus = arrayDropdown($dataStatus , "intStatus" , "txtStatus" , array("0" => "-Semua-"));
        $listPegawai = arrayDropdown($dataPegawai , "intIdPegawai" , "txtNamaPegawai");
        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['list_status_absensi'] = $this->form_builder->inputDropdown("Status" , "intIdStatus" , $intIdStatus , $listStatus , array() , "col-sm-2");
        $dt['list_pegawai'] = $this->form_builder->inputDropdown("Pegawai" , "intIdPegawai" , $intIdPegawai , $listPegawai , array() , "col-sm-2");
        $dt['startDate'] = $startDate;
        $dt['endDate'] = $endDate;
        $dt['title'] = "Report - Detail Absensi";
        return $this->load->view('Report/detail', $dt, true);
    }

    public function rekap() {            
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "container" => $this->__build_rekap(),
            "menu_key" => "rekap_status_absensi",
			"akses_key" => "is_view",
            "custom_js" => array(
                ASSETS_JS_URL . "eabsensi/report/rekap.js"
            ), 
            "custom_css" => array()            
        );  
        $this->_render("default",$dt);  

    }

    private function __build_rekap() {        
        $dataStatus = $this->status_absensi_model->getDataStatus();
        $listStatus = arrayDropdown($dataStatus , "intStatus" , "txtStatus" , array("0" => "-Semua-"));
        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['list_status_absensi'] = $this->form_builder->inputDropdown("Status" , "intIdStatus" , "" , $listStatus , array() , "col-sm-2");
        $dt['title'] = "Report - Rekap Data Absensi";
        return $this->load->view('Report/rekap_status', $dt, true);
    }

    /// Ajax Function 
    public function getDataDetail(){
        if(!$this->input->is_ajax_request()){
            echo "Ilegal";die;
        }

        $post = $this->input->post();
        $dtStart = $post['dtStart'];
        $dtEnd = $post['dtEnd'];
        $intIdStatus = $post['intIdStatus'];
        $intIdPegawai = $post['intIdPegawai'];

        $data = $this->report_model->getDetailRekapAbsensi($dtStart , $dtEnd , $intIdStatus , $intIdPegawai);
        ///echopre($data);
        $retVal['data'] = array();
        if(!empty($data)){
            foreach($data as $rowRekap) {
                $tanggalMasuk = explode(" " , $rowRekap['dtAbsensiMasuk']);
                $tanggalKeluar = explode(" " , $rowRekap['dtAbsensiKeluar']);
                $date = $tanggalMasuk[0];
                $jamMasuk = $tanggalMasuk[1];
                $jamKeluar = $tanggalKeluar[1];
                $arrData = array(
                    $rowRekap['txtNamaPegawai'],
                    $date,
                    $jamMasuk,
                    $jamKeluar,
                    $rowRekap['txtStatus'],
                );
                $retVal['data'][] = $arrData; 
            }
        }

        echo json_encode($retVal);

    }

    public function getDataRekap(){
        if(!$this->input->is_ajax_request()){
            echo "Ilegal";die;
        }

        $post = $this->input->post();
        $dtStart = $post['dtStart'];
        $dtEnd = $post['dtEnd'];

        $data = $this->report_model->getRekapAbsensi($dtStart , $dtEnd);
        
        $retVal['data'] = array();
        if(!empty($data)){
            foreach($data as $rowRekap) {

                $linkStatus = $this->base_url."report-detail/".$dtStart."/".$dtEnd."/".$rowRekap['intIDPegawai'];
                $arrData = array(
                    '<a target="_blank" href="'.$linkStatus.'/">'.$rowRekap['txtNamaPegawai'].'</a>',
                    '<a target="_blank" href="'.$linkStatus.'/1/">'.$rowRekap['intHadir']."</a>",
                    '<a target="_blank" href="'.$linkStatus.'/2/">'.$rowRekap['intIzin']."</a>",
                    '<a target="_blank" href="'.$linkStatus.'/3/">'.$rowRekap['intAlpa']."</a>",
                    '<a target="_blank" href="'.$linkStatus.'/4/">'.$rowRekap['intCuti']."</a>",
                    '<a target="_blank" href="'.$linkStatus.'/5/">'.$rowRekap['intDinasLuar']."</a>",
                    '<a target="_blank" href="'.$linkStatus.'/6/">'.$rowRekap['intTerlambat']."</a>",
                    '<a target="_blank" href="'.$linkStatus.'/7/">'.$rowRekap['intPulangAwal']."</a>",
                );
                $retVal['data'][] = $arrData; 
            }
        }

        echo json_encode($retVal);
    }

    public function getDataHarian(){
        if(!$this->input->is_ajax_request()){
            echo "Ilegal";die;
        }

        $post = $this->input->post();
        $dtStart = $post['dtSend'];
        $dataHarian = $this->report_model->getDataHarian($dtStart);
        $retVal['data'] = array();
        foreach($dataHarian as $row) {
            $tanggalMasuk = explode(" " , $row['tanggal_masuk']);
            $tanggalKeluar = explode(" " , $row['tanggal_pulang']);
            $date = isset($tanggalMasuk[0]) ? $tanggalMasuk[0] : "";
            $jamMasuk = isset($tanggalMasuk[1]) ? $tanggalMasuk[1] : "";
            $jamKeluar = isset($tanggalKeluar[1]) ? $tanggalKeluar[1] : "";
            $retVal['data'][] = array(
                $row['txtNamaPegawai'],
                $date,
                $jamMasuk,
                $jamKeluar,
                $row['status']
            );
        }
        echo json_encode($retVal);
    }

    //// Download Excel

    public function DownloadExcelHarian($dtStart){
        if(empty($dtStart)){
            redirect($this->base_url."report-harian/");
        }
        $this->excel->setActiveSheetIndex(0);
        $this->excel->getActiveSheet()->setTitle('Sheet 1');
        $this->excel->getActiveSheet()->setCellValue('A1', 'Puskesmas '.APP_KELURAHAN);
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(20);
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->mergeCells('A1:N1');
        $this->excel->getActiveSheet()->setCellValue('A2', "Laporan Rekap Harian Kehadiran Pegawai");
        $this->excel->getActiveSheet()->setCellValue('A3', 'Tanggal');
        $this->excel->getActiveSheet()->setCellValue('B3', $dtStart);

        $dataHarian = $this->report_model->getDataHarian($dtStart);
        ////echopre($dataHarian);die;
        $this->excel->getActiveSheet()->setCellValue('A6', "No");
        $this->excel->getActiveSheet()->setCellValue('B6', "Nama");
        $this->excel->getActiveSheet()->setCellValue('C6', "Tanggal");
        $this->excel->getActiveSheet()->setCellValue('D6', "Jam Hadir");
        $this->excel->getActiveSheet()->setCellValue('E6', "Jam Pulang");
        $this->excel->getActiveSheet()->setCellValue('F6', "Status");
        
        $indexNo = 7;
        $no = 1;
        foreach($dataHarian as $rowCellValue) {
            $tanggalMasuk = explode(" " , $rowCellValue['tanggal_masuk']);
            $tanggalKeluar = explode(" " , $rowCellValue['tanggal_pulang']);
            $date = isset($tanggalMasuk[0]) ? $tanggalMasuk[0] : "";
            $jamMasuk = isset($tanggalMasuk[1]) ? $tanggalMasuk[1] : "";
            $jamKeluar = isset($tanggalKeluar[1]) ? $tanggalKeluar[1] : "";

            $this->excel->getActiveSheet()->setCellValue('A'.$indexNo, $no);
            
            $this->excel->getActiveSheet()->setCellValue('B'.$indexNo, $rowCellValue['txtNamaPegawai']);
            
            $this->excel->getActiveSheet()->setCellValue('C'.$indexNo, $date);
            
            $this->excel->getActiveSheet()->setCellValue('D'.$indexNo, $jamMasuk);
            
            $this->excel->getActiveSheet()->setCellValue('E'.$indexNo, $jamKeluar);
        
            $this->excel->getActiveSheet()->setCellValue('F'.$indexNo, $rowCellValue['status']);
            
            $no++;
            $indexNo++;
        }
        $fileName = "Rekap_Harian_Kehadiran_Pegawai";
        
        $this->getOutput($fileName);
    }

    public function DownloadExcelRekap($dtStart, $dtEnd)
    {
        # code...
        if(empty($dtStart)){
            redirect($this->base_url."report-status/");
        }
        $this->excel->setActiveSheetIndex(0);
        $this->excel->getActiveSheet()->setTitle('Sheet 1');
        $this->excel->getActiveSheet()->setCellValue('A1', 'Puskesmas '.APP_KELURAHAN);
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(20);
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->mergeCells('A1:N1');
        $this->excel->getActiveSheet()->setCellValue('A2', "Laporan Rekap Status Kehadiran Pegawai");
        $this->excel->getActiveSheet()->setCellValue('A3', 'Tanggal Awal');
        $this->excel->getActiveSheet()->setCellValue('A4', 'Tanggal Akhir');
        $this->excel->getActiveSheet()->setCellValue('B3', $dtStart);
        $this->excel->getActiveSheet()->setCellValue('B4', $dtEnd);

        

        $this->excel->getActiveSheet()->setCellValue('A6', "No");
        $this->excel->getActiveSheet()->mergeCells('A6:A7');
        $this->excel->getActiveSheet()->setCellValue('B6', "Nama Pegawai");
        $this->excel->getActiveSheet()->mergeCells('B6:B7');
        $this->excel->getActiveSheet()->setCellValue('C6', "Status Kehadiran");
        $this->excel->getActiveSheet()->mergeCells('C6:G6');
        $this->excel->getActiveSheet()->setCellValue('C7', "Hadir");
        $this->excel->getActiveSheet()->setCellValue('D7', "Izin");
        $this->excel->getActiveSheet()->setCellValue('E7', "Alpa / Tanpa Keterangan");
        $this->excel->getActiveSheet()->setCellValue('F7', "Cuti");
        $this->excel->getActiveSheet()->setCellValue('G7', "Dinas Luar");

        $data = $this->report_model->getRekapAbsensi($dtStart , $dtEnd);

        $indexNo = 8;
        $no = 1;
        foreach($data as $rowCellValue) {
            $this->excel->getActiveSheet()->setCellValue('A'.$indexNo, $no);
            $this->excel->getActiveSheet()->setCellValue('B'.$indexNo, $rowCellValue['txtNamaPegawai']);
            $this->excel->getActiveSheet()->setCellValue('C'.$indexNo, $rowCellValue['intHadir']);
            $this->excel->getActiveSheet()->setCellValue('D'.$indexNo, $rowCellValue['intIzin']);
            $this->excel->getActiveSheet()->setCellValue('E'.$indexNo, $rowCellValue['intAlpa']);
            $this->excel->getActiveSheet()->setCellValue('F'.$indexNo, $rowCellValue['intCuti']);
            $this->excel->getActiveSheet()->setCellValue('G'.$indexNo, $rowCellValue['intDinasLuar']);
            $no++;
            $indexNo++;
        }
        $fileName = "Rekap_Kehadiran_Pegawai";
        $this->getOutput($fileName);

    }

    public function DownloadExcelDetail($dtStart, $dtEnd , $intIdStatus , $intIdPegawai)
    {
        # code...
        if(empty($dtStart)){
            redirect($this->base_url."report-detail/");
        }

        $this->excel->setActiveSheetIndex(0);
        $this->excel->getActiveSheet()->setTitle('Sheet 1');
        $this->excel->getActiveSheet()->setCellValue('A1', 'Puskesmas '.APP_KELURAHAN . " - Detail Laporan Kehadiran Pegawai");
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(20);
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        $this->excel->getActiveSheet()->mergeCells('A1:N1');
        $this->excel->getActiveSheet()->setCellValue('A3', 'Tanggal Awal');
        $this->excel->getActiveSheet()->setCellValue('A4', 'Tanggal Akhir');
        $this->excel->getActiveSheet()->setCellValue('B3', $dtStart);
        $this->excel->getActiveSheet()->setCellValue('B4', $dtEnd);        

        $this->excel->getActiveSheet()->setCellValue('A6', "No");
        $this->excel->getActiveSheet()->setCellValue('B6', "Nama Pegawai");
        $this->excel->getActiveSheet()->setCellValue('C6', "Tanggal");
        $this->excel->getActiveSheet()->setCellValue('D6', "Status");
        $this->excel->getActiveSheet()->setCellValue('E6', "Jam Hadir");
        $this->excel->getActiveSheet()->setCellValue('F6', "Jam Pulang");

        $data = $this->report_model->getDetailRekapAbsensi($dtStart , $dtEnd , $intIdStatus , $intIdPegawai);

        $indexNo = 7;
        $no = 1;
        foreach($data as $rowCellValue) {
            $tanggalMasuk = explode(" " , $rowCellValue['dtAbsensiMasuk']);
            $tanggalKeluar = explode(" " , $rowCellValue['dtAbsensiKeluar']);
            $date = $tanggalMasuk[0];
            $jamMasuk = $tanggalMasuk[1];
            $jamKeluar = $tanggalKeluar[1];
            $this->excel->getActiveSheet()->setCellValue('A'.$indexNo, $no);
            $this->excel->getActiveSheet()->setCellValue('B'.$indexNo, $rowCellValue['txtNamaPegawai']);
            $this->excel->getActiveSheet()->setCellValue('C'.$indexNo, $date);
            $this->excel->getActiveSheet()->setCellValue('D'.$indexNo, $rowCellValue['txtStatus']);
            $this->excel->getActiveSheet()->setCellValue('E'.$indexNo, $jamMasuk);
            $this->excel->getActiveSheet()->setCellValue('F'.$indexNo, $jamKeluar);
            $no++;
            $indexNo++;
        }
        $fileName = "Detail_Kehadiran_Pegawai";
        $this->getOutputHarian($fileName);

    }

    private function getOutputHarian( $filename){
        ///
        $filename = date('YmdHis')."-".$filename;
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'.xls"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $objWriter->save('php://output');
    }

    

    



    

}