<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Master extends MY_Controller {

    var $meta_title = "Master Data - Pegawai";
    var $meta_desc = "Master Data - Pegawai";
    var $main_title = "Master Data - Pegawai";
    var $dtBreadcrumbs = array();  
    var $base_url = "";

    public function __construct() {
        parent::__construct();
        $this->base_url = base_url(). "e-absensi/";
        $this->dtBreadcrumbs = array(
            "Home" =>$this->base_url,            
            "E-Absensi" =>$this->base_url ."e-absensi/",
            "Data Pegawai" => $this->base_url ."e-absensi/data-pegawai/",
        );
        $this->load->model('pegawai_model');
    }

    public function pegawai() {            
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "container" => $this->_build_pegawai(),
            "menu_key" => "absensi_master_pegawai",
			"akses_key" => "is_view",
            "custom_js" => array(
                ASSETS_JS_URL . "eabsensi/pegawai/form.js"
            ), 
            "custom_css" => array()            
        );  
        $this->_render("default",$dt);  
    }

    private function _build_pegawai() {        
        
        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['frm_search'] = $this->form_builder->inputText("Nama Pegawai / No Pegawai" , "txtNmaPegawai" , "");
        $dt['title'] = $this->meta_title;
        $dt['link_add'] = $this->base_url."pegawai/tambah-data/";
        return $this->load->view('Master/content_pegawai', $dt, true);
    }

    /// Ajax Function 
    public function getDataPegawai(){
        if(!$this->input->is_ajax_request()){
            
        }


    }

    public function saveDataPegawai(){
        if(!$this->input->is_ajax_request()){
            
        }

        
    }

    public function createPegawai() {
        if ($post = $this->input->post()) {
            //$this->validation();
            echopre($post);die;
            $retVal = $this->pegawai_model->saveData($post);
        }

        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "menu_key" => "absensi_master_pegawai",
			"akses_key" => "is_insert",
            "container" => $this->_form(),
            "custom_js" => array(
                ASSETS_URL."plugins/select2/select2.js",
				ASSETS_URL."plugins/validate/jquery.validate_1.11.1.min.js",
                ASSETS_JS_URL . "eabsensi/pegawai/form.js"
            ),
            "custom_css" => array()            
        );  
        $this->_render("default",$dt);  
    }

    public function edit($id) {
        $result = $this->find($id);
        if ($post = $this->input->post()) {
            $this->validation();
            $this->jenis_obat_m->update($id, $post);
            $this->redirect->with('successMessage', 'Data jenis obat berhasil diperbarui.')->to('gudang_apotik/kemasan');
        }
        $this->form->setData($result);
        $dt = array(
            "title" => $this->meta_title,
            "description" => $this->meta_desc,
            "menu_key" => "gudang_apotik_data_kemasan",
			"akses_key" => "is_update",
            "container" => $this->_form(),
            "custom_js" => array(),
            "custom_css" => array()            
        );  
        $this->_render("default",$dt);  
    }

    private function _form() {
        $dt['breadcrumbs'] = $this->setBreadcrumbs($this->dtBreadcrumbs);
        $dt['title'] = $this->meta_title;        
        $dt['jabatan_list'] = $this->config->item("jabatan_list");
        $dt['status_list'] = $this->config->item("status_list");
        $dt['link_index'] = $this->base_url . "pegawai/";
        return $this->load->view('Master/form_pegawai', $dt, true);
    }

    public function delete($id) {
        $result = $this->find($id);
        $this->jenis_obat_m->delete($id);
        $this->redirect->with('successMessage', 'Data jenis obat berhasil dihapus.')->to('data-jenis-obat');
    }

    private function find($id) {
        $result = $this->jenis_obat_m->find($id);
        if (!$result) {
            $this->redirect->with('errorMessage', 'Data jenis obat tidak ditemukan.')->back();
        } else {
            return $result;
        }
    }

    private function validation() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('jenis_obat', 'Jenis Obat', 'required');
        if (!$this->form_validation->run()) {
            $this->redirect->withInput()->withValidation()->back();
        }           
    }

}