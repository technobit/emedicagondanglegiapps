var base_url_register = global_url+'loket/';
var data_grid = "";
$( document ).ready(function() {

   ///getDataRegister();

});


function hapus_antrian(idKunjungan){
    bootbox.confirm({
        title : "Peringatan",
        message : "Apakah Anda Akan Menghapus Antrian Ini?",
        callback : function(result){
            if(result==true){
                $.ajax({
                    type : "POST",
                    url : global_url + "loket/hapusDataAntrianLoket/",
                    data : "idKunjungan="+idKunjungan,
                    success : function msg(response){
                        var data = jQuery.parseJSON(response);
                        var status = data['status'];
                        var message = data['message'];
                        alertPopUp(status, message , "");
                        if(status==true){
                            var id_jenis_loket = $('input[name=id_jenis_loket]').val();
                            window.location = base_url_register + 'loket/' + id_jenis_loket;
                        }
                    }
                })
            }
        }
    });
} 

function selesai_antrian(){
    $.ajax({
        url : global_url+"loket/updateDataLoket/",
        type : "POST",
        data : $('#form-detail-loket').serialize(),
        dataType : "html",
        success : function(response){
            var data = $.parseJSON(response);
            var status = data['status'];
            var message = data['message'];
            var base_url = data['base_url'];
            alertPopUp(status, message , base_url);
        }
    });
}


function panggil_antrian(id){
    ////$.playSound(global_url+'suara/antrian_loket/'+id+'');
    $.ajax({
        url : global_url+'suara/antrian_loket/'+id,
        type : 'GET',
        success : function msg(response){
            console.log(response);
        }
    });
}

function printNoAntrian(id){
    $.ajax({
        url : global_url+"cetak/cetak-no-antrian/",
        type : "POST",
        data : "idKunjungan="+id,
        dataType : "html",
        success : function(response){
            ///var data = jQuery.parseJSON(response);
        }
    });
}