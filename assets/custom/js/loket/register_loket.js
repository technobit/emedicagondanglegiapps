var base_url_register = global_url+'loket/';
var data_grid = "";
$( document ).ready(function() {

   ///getDataRegister();
data_grid = $('#table-data').DataTable({
    "paging": true,
    "lengthChange": false,
    "searching": false,
    "ordering": false,
    "info": false,
    "autoWidth": false,
    "processing" : false,
    ajax : {
      url : base_url_register+"getDataRegisterLoket/",
      type : "POST",
      data : function(p) {
          p.txtDate = $('#txtDate').val();
          p.intIdStatus = $('#intIdStatus').val();
          p.intIdJenisLoket = $('input[name=id_jenis_loket]').val();
      }
    }

    
});
setInterval(function () {
    data_grid.ajax.reload();
  }, 3000);
   $('#searchRegister').click(function(){
      if ($('#txtDate').val()!='') {
        //code
        ///getDataRegister();
        data_grid.ajax.reload();
      }
   });
   $(window).keypress(function(e) {
       var key = e.which;
       if (key==13) {
        //code
        e.preventDefault();
        ///getDataRegister();
        data_grid.ajax.reload();
       }
   });
   $('#txtDate').datepicker({
      format : 'yyyy-mm-dd', 
   });
});

function hapus_antrian(idKunjungan){
    bootbox.confirm({
        title : "Peringatan",
        message : "Apakah Anda Akan Menghapus Antrian Ini?",
        callback : function(result){
            if(result==true){
                $.ajax({
                    type : "POST",
                    url : global_url + "loket/hapusDataAntrianLoket/",
                    data : "idKunjungan="+idKunjungan,
                    success : function msg(response){
                        var data = jQuery.parseJSON(response);
                        var status = data['status'];
                        var message = data['message'];
                        alertPopUp(status, message , "");
                        if(status==true){
                            data_grid.ajax.reload();
                        }
                    }
                })
            }
        }
    });
} 



function panggil_antrian(id){
    ////$.playSound(global_url+'suara/antrian_loket/'+id+'');
    $.ajax({
        url : global_url+'suara/antrian_loket/'+id,
        type : 'GET',
        success : function msg(response){
            console.log(response);
        }
    });
}

function printNoAntrian(id){
    $.ajax({
        url : global_url+"cetak/cetak-no-antrian/",
        type : "POST",
        data : "idKunjungan="+id,
        dataType : "html",
        success : function(response){
            ///var data = jQuery.parseJSON(response);
        }
    });
}