var infoTable = "";
var data_grid ="";
var base_url = global_url+"mpengaturan/setting_profil/";
$( document ).ready(function(){
  
    $('#selectProvinsi').select2();
    $('#selectKebupaten').select2();
    $('#selectKecamatan').select2();
    $('#saveBtn').click(function(){
    var formData = new FormData($('#frm-pengaturan-profil')[0]);
    $.ajax({
        url : global_url+"mpengaturan/setting_profil/simpanDataProfil/",
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,
        dataType: "JSON",
         success: function msg(data){
                  
                 var status = data.status;
                 var message = data.message; 
                 alertNoRedirect(status,message);
                 $("#image").attr('src',data.img);   
        }
    });
    });
    
});


function getKabupaten() {
    //code
    $.ajax({
      url : base_url+"getDataKabupaten/",
      type : "POST",
      data : "idProvinsi="+$('#selectProvinsi').val(),
      dataType : "html",
      success: function msg(res){
           var data = jQuery.parseJSON(res);
           var status = data['status'];
           var resHtml = data['html'];
           $('#selectKebupaten').html(resHtml);
      }
    });
}
function getKecamatan() {
    //code
    $.ajax({
      url : base_url+"getDataKecamatan/",
      type : "POST",
      data : "idKabupaten="+$('#selectKebupaten').val(),
      dataType : "html",
      success: function msg(res){
           var data = jQuery.parseJSON(res);
           var status = data['status'];
           var resHtml = data['html'];
           $('#selectKecamatan').html(resHtml);
      }
    });
}