var infoTable = "";
var data_grid ="";
var base_url = global_url + 'mpengaturan/Pengaturan_profil_printer/';
$( document ).ready(function(){
  
    
    $('#saveBtn').click(function(){
         $.ajax({
            url : global_url + 'mpengaturan/setting_bpjs/simpanData',
            type : "POST",
            data : $('#frm-pengaturan-bpjs').serialize(),
            dataType : "html",
            success: function msg(res){
                 var data = jQuery.parseJSON(res);    
                 var status = data['status'];
                 var message = data['message']; 
                 alertNoRedirect(status,message);   
        }
    });
    });
});

function hapusProfile(id){
  var url_hapus = base_url+"deleteData/"+id;
  bootbox.confirm({
        size : "small",
        title : "Konfirmasi",
        message : "Apakah Anda Akan Menghapus Data Ini?",
        callback : function(response){
            if(response==true){
                $.ajax({
                url :  url_hapus,
                type : "POST",
                success : function msg(data){
                             var status = data.status;
                             var message = data.message;
                            alertNoRedirect(status,message);
                            data_grid.ajax.reload();
                            getlistDropdown();
                            }
                    });              
             }
        }
    });
}
function cancel_modal(){
    $('.bootbox').modal('hide');
}
