var infoTable = "";
var data_grid ="";
var base_url = global_url + 'mpengaturan/Pengaturan_profil_printer/';
$( document ).ready(function(){
   data_grid = $('#data-grid').DataTable({
          "paging": false,
          "lengthChange": true,
          "searching": false,
          "ordering": false,
          "info": false,
          "autoWidth": true,
          "processing" : true,
          "serverSide": true,
          "aLengthMenu": [
                [25, 50, 100, 200, -1],
                [25, 50, 100, 200, "All"]
          ],
          "ajax" : {
            "url" : global_url+"mpengaturan/pengaturan_profil_printer/getdata/",
            "type" : "POST",
            "data" : function (postParams) {
                 
            }
          }
    });
    
    $('#saveBtn').click(function(){
         $.ajax({
            url : global_url + 'mpengaturan/Pengaturan_profil_printer/simpanDataPengaturan',
            type : "POST",
            data : $('#frm-pengaturan-print').serialize(),
            dataType : "html",
            success: function msg(res){
                 var data = jQuery.parseJSON(res);    
                 var status = data['status'];
                 var message = data['message']; 
                 alertNoRedirect(status,message);   
        }
    });
    });
});
function getlistDropdown() {
    //code
    $.ajax({
      url : base_url+"getlistDropdown/",
      type : "POST",
      dataType : "html",
      success: function msg(res){
           var data = jQuery.parseJSON(res);
           var status = data['status'];
           var resHtml = data['html'];
           $('#printer_loket').html(resHtml);
           $('#printer_self_service').html(resHtml);
           $('#printer_self_kasir').html(resHtml);
      }
    });
}
function addProfile(id = ""){
     $.ajax({
        url :  global_url + 'mpengaturan/Pengaturan_profil_printer/Form_add_profile/'+id,
        type : "POST",
        success : function msg(result){
            bootbox.dialog({
                title : "Form Profile Printer" , 
                message : result,
            });
        }
    });
        
}
function hapusProfile(id){
  var url_hapus = base_url+"deleteData/"+id;
  bootbox.confirm({
        size : "small",
        title : "Konfirmasi",
        message : "Apakah Anda Akan Menghapus Data Ini?",
        callback : function(response){
            if(response==true){
                $.ajax({
                url :  url_hapus,
                type : "POST",
                success : function msg(data){
                             var status = data.status;
                             var message = data.message;
                            alertNoRedirect(status,message);
                            data_grid.ajax.reload();
                            getlistDropdown();
                            }
                    });              
             }
        }
    });
}
function saveDataPasien() {
    //code
  
    $.ajax({
            url : global_url + 'mpengaturan/Pengaturan_profil_printer/simpanData',
            type : "POST",
            data : $('#frm-profile-printer').serialize(),
            dataType : "html",
            success: function msg(res){
                 cancel_modal();
                data_grid.ajax.reload();
                getlistDropdown();
            }
    });
}
function cancel_modal(){
    $('.bootbox').modal('hide');
}
