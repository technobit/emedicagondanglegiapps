var infoTable = "";
var data_grid ="";
var base_url = global_url + 'mpengaturan/setting_finger/';
$( document ).ready(function(){
   data_grid = $('#data-grid').DataTable({
          "paging": false,
          "lengthChange": true,
          "searching": false,
          "ordering": false,
          "info": false,
          "autoWidth": true,
          "processing" : true,
          "serverSide": true,
          "aLengthMenu": [
                [25, 50, 100, 200, -1],
                [25, 50, 100, 200, "All"]
          ],
          "ajax" : {
            "url" : base_url+"getdata/",
            "type" : "POST",
            "data" : function (postParams) {
                 
            }
          }
    });
    
   
});
function getlistDropdown() {
    //code
    $.ajax({
      url : base_url+"getlistDropdown/",
      type : "POST",
      dataType : "html",
      success: function msg(res){
           var data = jQuery.parseJSON(res);
           var status = data['status'];
           var resHtml = data['html'];
           $('#printer_loket').html(resHtml);
           $('#printer_self_service').html(resHtml);
           $('#printer_self_kasir').html(resHtml);
      }
    });
}
function addDevice(id = ""){
     $.ajax({
        url :  base_url + 'Form_device/'+id,
        type : "POST",
        success : function msg(result){
            bootbox.dialog({
                title : "Form Fingerspot Device" , 
                message : result,
            });
        }
    });
        
}
function hapusDevice(id){
  var url_hapus = base_url+"deleteData/"+id;
  bootbox.confirm({
        size : "small",
        title : "Konfirmasi",
        message : "Apakah Anda Akan Menghapus Data Ini?",
        callback : function(response){
            if(response==true){
                $.ajax({
                url :  url_hapus,
                type : "POST",
                success : function msg(data){
                             var status = data.status;
                             var message = data.message;
                            alertNoRedirect(status,message);
                            data_grid.ajax.reload();
                           
                            }
                    });              
             }
        }
    });
}
function saveData() {
    //code
  
    $.ajax({
            url : base_url + 'simpanData',
            type : "POST",
            data : $('#frm-finger').serialize(),
            dataType : "html",
            success: function msg(res){
                cancel_modal();
                data_grid.ajax.reload();
               
            }
    });
}
function cancel_modal(){
    $('.bootbox').modal('hide');
}
